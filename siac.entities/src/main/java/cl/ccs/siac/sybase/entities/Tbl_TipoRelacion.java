package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoRelacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoRelacion.findAll", query="SELECT t FROM Tbl_TipoRelacion t")
public class Tbl_TipoRelacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoRelacion")
	private String fld_GlosaTipoRelacion;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoRelacion")
	private byte fld_TipoRelacion;

	public Tbl_TipoRelacion() {
	}

	public String getFld_GlosaTipoRelacion() {
		return this.fld_GlosaTipoRelacion;
	}

	public void setFld_GlosaTipoRelacion(String fld_GlosaTipoRelacion) {
		this.fld_GlosaTipoRelacion = fld_GlosaTipoRelacion;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoRelacion() {
		return this.fld_TipoRelacion;
	}

	public void setFld_TipoRelacion(byte fld_TipoRelacion) {
		this.fld_TipoRelacion = fld_TipoRelacion;
	}

}