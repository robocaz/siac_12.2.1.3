package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ConsultaRangoBolFacDTO;
import cl.exe.ccs.dto.FacturaDTO;
import cl.exe.ccs.dto.ReemplazaBoletaFacturaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteDTO;

@Named("facturaDao")
@Stateless
public class FacturaDaoImpl implements FacturaDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(FacturaDaoImpl.class);

	public static String getTipoPersonaRut(String rut) {
		rut = rut.trim();
		if (rut.contains(".")) {
			rut = rut.replace(".", "");
		}
		if (rut.contains("-")) {
			rut = rut.substring(0, rut.lastIndexOf("-"));
		} else {
			rut = rut.substring(0, rut.length() - 1) + "-" + rut.charAt(rut.length() - 1);
		}
		return Integer.parseInt(rut) < 50000000 ? "N" : "J";
	}

	public static String getRutConCeros(String rut) {
		rut = rut.trim();
		if (rut.contains(".")) {
			rut = rut.replace(".", "");
		}
		if (!rut.contains("-")) {
			rut = rut.substring(0, rut.length() - 1) + "-" + rut.charAt(rut.length() - 1);
		}
		while (rut.length() < 11) {
			rut = "0" + rut;
		}
		return rut;
	}

	public FacturaDTO consultaFacturas(FacturaDTO facturaDTO) {

		try {

			log.info("Ejecutando SQ_FacturaTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_FacturaTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoFacturacion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", Integer.parseInt(facturaDTO.getCorrCaja()));

			storedProcedure.execute();

			log.info(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());

			facturaDTO.setTipoFacturacion(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TipoFacturacion").toString()) == 1? "Rut" : "Tercero");
			facturaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			facturaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			if (facturaDTO.getIdControl() == 0) {
				List<FacturaDTO> lista = new ArrayList<FacturaDTO>();
				List<Object[]> listaSP = storedProcedure.getResultList();

				for (Object item : listaSP) {

					Object[] datos = (Object[]) item;
					FacturaDTO grilla = new FacturaDTO();
					TramitanteDTO tramitante = new TramitanteDTO();

					grilla.setIdCentroCosto(Integer.parseInt(datos[0].toString()));
					grilla.setCodRegion(Integer.parseInt(datos[1].toString()));
					grilla.setCodComuna(Integer.parseInt(datos[2].toString()));
					grilla.setDireccion(datos[3].toString());

					grilla.setGiro(datos[5].toString());
					grilla.setMontoBruto(java.math.BigDecimal.valueOf(Double.parseDouble(datos[6].toString())));
					grilla.setNumeroFactura(Long.parseLong(datos[7].toString()));
					grilla.setOrdenCompra(datos[8].toString().trim());
					grilla.setRazonSocial(datos[9].toString());
					tramitante.setApellidoPaterno(datos[10].toString());
					tramitante.setApellidoMaterno(datos[11].toString());
					tramitante.setNombres(datos[12].toString());
					grilla.setTramitante(tramitante);
					grilla.setRutAfectado(datos[13].toString());
					grilla.setRutFacturacion(datos[14].toString());

					grilla.setTipoFacturacion(grilla.getRutAfectado().compareTo("000000000-0") == 0 ? "Tercero" : "Rut");

					lista.add(grilla);
				}

				facturaDTO.setFacturas(lista);
			}

			return facturaDTO;
		} catch (Exception e) {
			log.error("Error en el SP SQ_FacturaTemp_SW2 " + e.getMessage());
			facturaDTO.setIdControl(1);
			return facturaDTO;
		}

	}

	public FacturaDTO grabaFactura(FacturaDTO facturaDTO) {

		try {
			

			log.info("Ejecutando SI_TempFactura_SW2");

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_TempFactura_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutFacturacion", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RazonSocial", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroFactura", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoBruto", java.math.BigDecimal.class,ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOrdenCompra", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp timestamp = new java.sql.Timestamp(facturaDTO.getFecha());

			storedProcedure.setParameter("Fld_CorrCaja", Integer.parseInt(facturaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_CodCCosto", facturaDTO.getIdCentroCosto());
			storedProcedure.setParameter("Fld_FecEmision", timestamp);
			storedProcedure.setParameter("Fld_RutAfectado", facturaDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_RutFacturacion", facturaDTO.getRutFacturacion());
			if(facturaDTO.getRazonSocial() != null){
				storedProcedure.setParameter("Fld_RazonSocial", facturaDTO.getRazonSocial());
			}
			else{
				storedProcedure.setParameter("Fld_RazonSocial", facturaDTO.getNombres()+" "+facturaDTO.getApellidoPaterno()+" "+facturaDTO.getApellidoMaterno());
			}
			storedProcedure.setParameter("Fld_Nombre_ApPat", facturaDTO.getTramitante().getApellidoPaterno());
			storedProcedure.setParameter("Fld_Nombre_ApMat", facturaDTO.getTramitante().getApellidoMaterno());
			storedProcedure.setParameter("Fld_Nombre_Nombres", facturaDTO.getTramitante().getNombres());
			storedProcedure.setParameter("Fld_Direccion", facturaDTO.getDireccion());
			storedProcedure.setParameter("Fld_CodComuna", facturaDTO.getCodComuna());
			storedProcedure.setParameter("Fld_NroFactura", facturaDTO.getNumeroFactura());
			storedProcedure.setParameter("Fld_MontoBruto", facturaDTO.getTotal());
			storedProcedure.setParameter("Fld_NroOrdenCompra", facturaDTO.getOrdenCompra());
			storedProcedure.setParameter("Fld_Giro", facturaDTO.getGiro());

			storedProcedure.execute();

			log.info(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());

			facturaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			facturaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return facturaDTO;

		} catch (Exception e) {
			log.error("Error en el SP SI_TempFactura_SW2 " + e.getMessage());
			e.printStackTrace(); 
			facturaDTO.setIdControl(1);
			return facturaDTO;
		}

	}

	public FacturaDTO eliminaFactura(FacturaDTO facturaDTO) {

		try {
			
			log.info("Ejecutando SD_EliFacturaTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_EliFacturaTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoFacturacion", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutFacturacion", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", Integer.parseInt(facturaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_TipoFacturacion",facturaDTO.getRutAfectado().compareTo("000000000-0") == 0 ? 2 : 1);
			storedProcedure.setParameter("Fld_RutAfectado", facturaDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_RutFacturacion", facturaDTO.getRutFacturacion());

			storedProcedure.execute();

			facturaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			facturaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return facturaDTO;
		} catch (Exception e) {
			log.error("Error en el SP SD_EliFacturaTemp_SW2 " + e.getMessage());
			facturaDTO.setIdControl(1);
			return facturaDTO;
		}

	}

	public FacturaDTO actualizarTransaccion(FacturaDTO facturaDTO) {
		try {
			
			log.info("Ejecutando SU_ActBoletaFacturaTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_ActBoletaFacturaTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_ATerceros", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", Integer.parseInt(facturaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_BoletaFactura", "F");
			storedProcedure.setParameter("Fld_NroBoletaFactura",Integer.parseInt(facturaDTO.getNumeroFactura().toString()));
			storedProcedure.setParameter("Fld_ATerceros",facturaDTO.getRutAfectado().compareTo("000000000-0") == 0 ? 1 : 0);
			storedProcedure.setParameter("Fld_RutAfectado", facturaDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_RutTramitante", facturaDTO.getTramitante().getRut());

			storedProcedure.execute();

			facturaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			facturaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return facturaDTO;
		} catch (Exception e) {
			log.error("Error en el SP SU_ActBoletaFacturaTmp_SW2 " + e.getMessage());
			facturaDTO.setIdControl(1);
			return facturaDTO;
		}
	}

	public FacturaDTO folioFactura(FacturaDTO facturaDTO) {
		try {
			
			
			log.info("Ejecutando SP_ObtieneFolioDTE2_SW2");
			
			//StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_ObtieneFolioDTE_SW2");
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_ObtieneFolioDTE2_SW2");
			

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDTE", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTempTransac", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FolioActual", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CodCCosto", facturaDTO.getIdCentroCosto());
			storedProcedure.setParameter("Fld_CorrCaja", Integer.parseInt(facturaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_CodUsuario", facturaDTO.getUsuario());
			storedProcedure.setParameter("Fld_TipoDTE", 33);
			storedProcedure.setParameter("Fld_CorrTempTransac", facturaDTO.getTransacciones().get(0).getCorrelativoTransaccion());

			storedProcedure.execute();

			facturaDTO.setNumeroFactura(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_FolioActual").toString()));
			facturaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			facturaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			
			log.info("Folio: " + facturaDTO.getNumeroFactura() + " Codigo: " + facturaDTO.getIdControl() + "Descripcion: " + facturaDTO.getMsgControl());
			
			return facturaDTO;
		} catch (Exception e) {
			log.error("Error en el SP SP_ObtieneFolioDTE_SW2 " + e.getMessage());
			facturaDTO.setIdControl(1);
			facturaDTO.setMsgControl(e.getCause().getMessage());
			return facturaDTO;
		}
	}

	public FacturaDTO consultaRut(FacturaDTO facturaDTO) {
		try {
			
			
			log.info("Ejecutando SQ_Tramitante_SW2");			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_Tramitante_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomTram", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Telefono", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Email", String.class, ParameterMode.OUT);

			log.info("Rut = " + FacturaDaoImpl.getRutConCeros(facturaDTO.getRutFacturacion()));
			storedProcedure.setParameter("Fld_RutTramitante",FacturaDaoImpl.getRutConCeros(facturaDTO.getRutFacturacion()));

			storedProcedure.execute();

			TramitanteDTO tramitante = facturaDTO.getTramitante();

			if (FacturaDaoImpl.getTipoPersonaRut(facturaDTO.getRutFacturacion()).compareTo("N") == 0) {
				tramitante.setNombres(storedProcedure.getOutputParameterValue("Fld_NomTram_Nombres").toString().trim());
				tramitante.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_NomTram_ApPat").toString().trim());
				tramitante.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_NomTram_ApMat").toString().trim());
			} else {
				facturaDTO.setRazonSocial((storedProcedure.getOutputParameterValue("Fld_NomTram_ApPat").toString()+
											storedProcedure.getOutputParameterValue("Fld_NomTram_ApMat").toString()+
											storedProcedure.getOutputParameterValue("Fld_NomTram_Nombres").toString()).trim());
			}

			facturaDTO.setDireccion(storedProcedure.getOutputParameterValue("Fld_Direccion").toString().trim());
			facturaDTO.setGiro(storedProcedure.getOutputParameterValue("Fld_Giro").toString().trim());
			facturaDTO.setCodComuna(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodComuna").toString()));
			facturaDTO.setCodRegion(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodRegion").toString()));
			tramitante.setTelefono(storedProcedure.getOutputParameterValue("Fld_Telefono").toString().trim());
			tramitante.setEmail(storedProcedure.getOutputParameterValue("Fld_Email").toString().trim());
			facturaDTO.setTramitante(tramitante);

			facturaDTO.setIdControl(0);

			return facturaDTO;
		} catch (Exception e) {
			log.error("Error en el SP SQ_Tramitante_SW2 " + e.getMessage());
			facturaDTO.setIdControl(1);
			facturaDTO.setMsgControl(e.getMessage());
			return facturaDTO;
		}
	}

	public ConsultaRangoBolFacDTO consultaRangoBF(ConsultaRangoBolFacDTO consultaRango) {
		try {
			
			log.info("Ejecutando SQ_RangoBFCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_RangoBFCaja_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RangoDesde", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RangoHasta", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", consultaRango.getCorrCaja());
			storedProcedure.setParameter("Fld_BoletaFactura", consultaRango.getTipoDoc());

			storedProcedure.execute();

			consultaRango.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			consultaRango.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			if (consultaRango.getIdControl() == 0) {
				consultaRango.setCodCCosto(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodCCosto").toString()));
				consultaRango.setRangoDesde(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_RangoDesde").toString()));
				consultaRango.setRangoHasta(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_RangoHasta").toString()));
			}

			return consultaRango;
		} catch (Exception e) {
			log.error("Error en el SP SQ_RangoBFCaja_SW2 " + e.getMessage());
			consultaRango.setIdControl(1);
			consultaRango.setMsgControl(e.getMessage());
			return consultaRango;
		}
	}

	public ResponseDTO reemplazarBoleta(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO) {
		ResponseDTO response = new ResponseDTO();

		try {
			
			log.info("Ejecutando SP_ReemplazaFactura2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_ReemplazaFactura2_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoBruto", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOrdenCompra", String.class, ParameterMode.IN);

			storedProcedure.setParameter("Fld_CodCCosto", reemplazaBoletaFacturaDTO.getCodCCosto().intValue());
			storedProcedure.setParameter("Fld_CorrCaja", BigDecimal.valueOf(reemplazaBoletaFacturaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_NroBoletaFactura", reemplazaBoletaFacturaDTO.getNumeroBoleta());
			storedProcedure.setParameter("Fld_FecEmision",new java.sql.Timestamp(reemplazaBoletaFacturaDTO.getFecEmision()));
			storedProcedure.setParameter("Fld_RutAfectado", reemplazaBoletaFacturaDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrNombre",BigDecimal.valueOf(reemplazaBoletaFacturaDTO.getCorrNombre()));
			storedProcedure.setParameter("Fld_MontoBruto", reemplazaBoletaFacturaDTO.getMontoBruto());
			storedProcedure.setParameter("Fld_NroFactura", reemplazaBoletaFacturaDTO.getNumeroFactura());
			storedProcedure.setParameter("Fld_CodUsuario", reemplazaBoletaFacturaDTO.getCodUsuario());
			storedProcedure.setParameter("Fld_CodOrigen", reemplazaBoletaFacturaDTO.getCodOrigen());
			storedProcedure.setParameter("Fld_NroOrdenCompra", reemplazaBoletaFacturaDTO.getOrdenCompra());

			storedProcedure.execute();

			return response;
		} catch (Exception e) {
			log.error("Error en el SP SP_ReemplazaFactura2_SW2 " + e.getMessage());
			response.setIdControl(1);
			response.setMsgControl(e.getMessage());
			return response;
		}
	}

	public ResponseDTO reemplazarBoletaCero(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO) {
		ResponseDTO response = new ResponseDTO();

		try {
			
			log.info("Ejecutando SP_ReempFactBolCero2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_ReempFactBolCero2_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoBruto", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOrdenCompra", String.class, ParameterMode.IN);

			storedProcedure.setParameter("Fld_CodCCosto", reemplazaBoletaFacturaDTO.getCodCCosto().intValue());
			storedProcedure.setParameter("Fld_CorrTransaccion",BigDecimal.valueOf(reemplazaBoletaFacturaDTO.getCorrTransaccion()));
			storedProcedure.setParameter("Fld_FecEmision",new java.sql.Timestamp(reemplazaBoletaFacturaDTO.getFecEmision()));
			storedProcedure.setParameter("Fld_RutAfectado", reemplazaBoletaFacturaDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrNombre",BigDecimal.valueOf(reemplazaBoletaFacturaDTO.getCorrNombre()));
			storedProcedure.setParameter("Fld_MontoBruto", reemplazaBoletaFacturaDTO.getMontoBruto());
			storedProcedure.setParameter("Fld_NroFactura", reemplazaBoletaFacturaDTO.getNumeroFactura());
			storedProcedure.setParameter("Fld_CodUsuario", reemplazaBoletaFacturaDTO.getCodUsuario());
			storedProcedure.setParameter("Fld_CodOrigen", reemplazaBoletaFacturaDTO.getCodOrigen());
			storedProcedure.setParameter("Fld_NroOrdenCompra", reemplazaBoletaFacturaDTO.getOrdenCompra());

			storedProcedure.execute();


			return response;
		} catch (Exception e) {
			log.error("Error en el SP SP_ReempFactBolCero2_SW2 " + e.getMessage());
			response.setIdControl(1);
			response.setMsgControl(e.getMessage());
			return response;
		}
	}

	public ResponseDTO obtenerIVA() {
		ResponseDTO response = new ResponseDTO();

		try {
			
			log.info("Ejecutando SQ_PorcentajeIVA_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_PorcentajeIVA_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_PorcentajeIVA", Float.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.execute();

			response.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				response.setMsgControl(storedProcedure.getOutputParameterValue("Fld_PorcentajeIVA").toString());
			}
		} catch (Exception e) {
			log.error("Error en el SP SQ_PorcentajeIVA_SW2 " + e.getMessage());
			response.setIdControl(1);
			response.setMsgControl(e.getMessage());
		}
		return response;
	}

}
