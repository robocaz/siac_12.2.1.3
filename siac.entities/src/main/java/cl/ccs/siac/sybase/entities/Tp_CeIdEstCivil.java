package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdEstCivil database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdEstCivil.findAll", query="SELECT t FROM Tp_CeIdEstCivil t")
public class Tp_CeIdEstCivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstCivil")
	private String fld_CodEstCivil;

	@Column(name="Fld_GlosaEstCivil")
	private String fld_GlosaEstCivil;

	public Tp_CeIdEstCivil() {
	}

	public String getFld_CodEstCivil() {
		return this.fld_CodEstCivil;
	}

	public void setFld_CodEstCivil(String fld_CodEstCivil) {
		this.fld_CodEstCivil = fld_CodEstCivil;
	}

	public String getFld_GlosaEstCivil() {
		return this.fld_GlosaEstCivil;
	}

	public void setFld_GlosaEstCivil(String fld_GlosaEstCivil) {
		this.fld_GlosaEstCivil = fld_GlosaEstCivil;
	}

}