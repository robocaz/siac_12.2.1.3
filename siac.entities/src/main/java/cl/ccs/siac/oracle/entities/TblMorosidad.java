package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_MOROSIDAD database table.
 * 
 */
@Entity
@Table(name="TBL_MOROSIDAD")
@NamedQuery(name="TblMorosidad.findAll", query="SELECT t FROM TblMorosidad t")
public class TblMorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_MOROSIDAD_FLDCORRMOROSIDADID_GENERATOR", sequenceName="FLD_CORRMOROSIDAD_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_MOROSIDAD_FLDCORRMOROSIDADID_GENERATOR")
	@Column(name="FLD_CORRMOROSIDAD_ID")
	private long fldCorrmorosidadId;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_CORRENVIO")
	private BigDecimal fldCorrenvio;

	@Column(name="FLD_CORRENVIOBAJA")
	private BigDecimal fldCorrenviobaja;

	@Column(name="FLD_CORRENVIOCARGA")
	private BigDecimal fldCorrenviocarga;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FECHACREACION")
	private Timestamp fldFechacreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOCREACION")
	private String fldUsuariocreacion;

	//bi-directional many-to-one association to TblBloqueo
	@OneToMany(mappedBy="tblMorosidad")
	private List<TblBloqueo> tblBloqueos;

	//bi-directional many-to-one association to TblSucursal
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FLD_CODEMISOR", referencedColumnName="FLD_CODEMISOR"),
		@JoinColumn(name="FLD_CODSUCURSAL", referencedColumnName="FLD_CODSUCURSAL"),
		@JoinColumn(name="FLD_TIPOEMISOR", referencedColumnName="FLD_TIPOEMISOR")
		})
	private TblSucursal tblSucursal;

	//bi-directional many-to-one association to TblRutmorosidad
	@OneToMany(mappedBy="tblMorosidad")
	private List<TblRutmorosidad> tblRutmorosidads;

	public TblMorosidad() {
	}

	public long getFldCorrmorosidadId() {
		return this.fldCorrmorosidadId;
	}

	public void setFldCorrmorosidadId(long fldCorrmorosidadId) {
		this.fldCorrmorosidadId = fldCorrmorosidadId;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public BigDecimal getFldCorrenvio() {
		return this.fldCorrenvio;
	}

	public void setFldCorrenvio(BigDecimal fldCorrenvio) {
		this.fldCorrenvio = fldCorrenvio;
	}

	public BigDecimal getFldCorrenviobaja() {
		return this.fldCorrenviobaja;
	}

	public void setFldCorrenviobaja(BigDecimal fldCorrenviobaja) {
		this.fldCorrenviobaja = fldCorrenviobaja;
	}

	public BigDecimal getFldCorrenviocarga() {
		return this.fldCorrenviocarga;
	}

	public void setFldCorrenviocarga(BigDecimal fldCorrenviocarga) {
		this.fldCorrenviocarga = fldCorrenviocarga;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Timestamp getFldFechacreacion() {
		return this.fldFechacreacion;
	}

	public void setFldFechacreacion(Timestamp fldFechacreacion) {
		this.fldFechacreacion = fldFechacreacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuariocreacion() {
		return this.fldUsuariocreacion;
	}

	public void setFldUsuariocreacion(String fldUsuariocreacion) {
		this.fldUsuariocreacion = fldUsuariocreacion;
	}

	public List<TblBloqueo> getTblBloqueos() {
		return this.tblBloqueos;
	}

	public void setTblBloqueos(List<TblBloqueo> tblBloqueos) {
		this.tblBloqueos = tblBloqueos;
	}

	public TblBloqueo addTblBloqueo(TblBloqueo tblBloqueo) {
		getTblBloqueos().add(tblBloqueo);
		tblBloqueo.setTblMorosidad(this);

		return tblBloqueo;
	}

	public TblBloqueo removeTblBloqueo(TblBloqueo tblBloqueo) {
		getTblBloqueos().remove(tblBloqueo);
		tblBloqueo.setTblMorosidad(null);

		return tblBloqueo;
	}

	public TblSucursal getTblSucursal() {
		return this.tblSucursal;
	}

	public void setTblSucursal(TblSucursal tblSucursal) {
		this.tblSucursal = tblSucursal;
	}

	public List<TblRutmorosidad> getTblRutmorosidads() {
		return this.tblRutmorosidads;
	}

	public void setTblRutmorosidads(List<TblRutmorosidad> tblRutmorosidads) {
		this.tblRutmorosidads = tblRutmorosidads;
	}

	public TblRutmorosidad addTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().add(tblRutmorosidad);
		tblRutmorosidad.setTblMorosidad(this);

		return tblRutmorosidad;
	}

	public TblRutmorosidad removeTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().remove(tblRutmorosidad);
		tblRutmorosidad.setTblMorosidad(null);

		return tblRutmorosidad;
	}

}