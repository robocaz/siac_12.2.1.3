package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TramoAclLP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TramoAclLP.findAll", query="SELECT t FROM Tbl_TramoAclLP t")
public class Tbl_TramoAclLP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CostoTramo")
	private BigDecimal fld_CostoTramo;

	@Column(name="Fld_MontoDesde")
	private double fld_MontoDesde;

	@Column(name="Fld_MontoHasta")
	private double fld_MontoHasta;

	public Tbl_TramoAclLP() {
	}

	public BigDecimal getFld_CostoTramo() {
		return this.fld_CostoTramo;
	}

	public void setFld_CostoTramo(BigDecimal fld_CostoTramo) {
		this.fld_CostoTramo = fld_CostoTramo;
	}

	public double getFld_MontoDesde() {
		return this.fld_MontoDesde;
	}

	public void setFld_MontoDesde(double fld_MontoDesde) {
		this.fld_MontoDesde = fld_MontoDesde;
	}

	public double getFld_MontoHasta() {
		return this.fld_MontoHasta;
	}

	public void setFld_MontoHasta(double fld_MontoHasta) {
		this.fld_MontoHasta = fld_MontoHasta;
	}

}