package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class DetalleCentroCostoDTO implements Serializable{
	
	private String fechaDesde;
	private String fechaHasta;
	private Integer centroCosto;
	private List<DetalleCajaDTO> cajasDetalle;
	private Long montoCalculadoBF;
	private Long montoCalculadoDet;
	private Long montoPagado;
	private Long diferenciaBF;
	private Long diferenciaDet;
	private String glosaCC;
	
	
	
	public String getGlosaCC() {
		return glosaCC;
	}
	public void setGlosaCC(String glosaCC) {
		this.glosaCC = glosaCC;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public List<DetalleCajaDTO> getCajasDetalle() {
		return cajasDetalle;
	}
	public void setCajasDetalle(List<DetalleCajaDTO> cajasDetalle) {
		this.cajasDetalle = cajasDetalle;
	}
	public Long getMontoCalculadoBF() {
		return montoCalculadoBF;
	}
	public void setMontoCalculadoBF(Long montoCalculadoBF) {
		this.montoCalculadoBF = montoCalculadoBF;
	}
	public Long getMontoCalculadoDet() {
		return montoCalculadoDet;
	}
	public void setMontoCalculadoDet(Long montoCalculadoDet) {
		this.montoCalculadoDet = montoCalculadoDet;
	}
	public Long getMontoPagado() {
		return montoPagado;
	}
	public void setMontoPagado(Long montoPagado) {
		this.montoPagado = montoPagado;
	}
	public Long getDiferenciaBF() {
		return diferenciaBF;
	}
	public void setDiferenciaBF(Long diferenciaBF) {
		this.diferenciaBF = diferenciaBF;
	}
	public Long getDiferenciaDet() {
		return diferenciaDet;
	}
	public void setDiferenciaDet(Long diferenciaDet) {
		this.diferenciaDet = diferenciaDet;
	}
	
	
	
	
	
	
	
}
