package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LibradoresNombre database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LibradoresNombre.findAll", query="SELECT t FROM Tbl_LibradoresNombre t")
public class Tbl_LibradoresNombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_LibradoresNombrePK id;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	public Tbl_LibradoresNombre() {
	}

	public Tbl_LibradoresNombrePK getId() {
		return this.id;
	}

	public void setId(Tbl_LibradoresNombrePK id) {
		this.id = id;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

}