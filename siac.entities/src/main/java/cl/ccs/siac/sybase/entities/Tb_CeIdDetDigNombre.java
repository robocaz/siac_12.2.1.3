package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetDigNombres database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetDigNombres")
@NamedQuery(name="Tb_CeIdDetDigNombre.findAll", query="SELECT t FROM Tb_CeIdDetDigNombre t")
public class Tb_CeIdDetDigNombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETDIGNOMBRES_FLD_CORRDIGNOM_ID_GENERATOR", sequenceName="FLD_CORRDIGNOM_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETDIGNOMBRES_FLD_CORRDIGNOM_ID_GENERATOR")
	@Column(name="Fld_CorrDigNom_Id")
	private long fld_CorrDigNom_Id;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdDetErrDigNom
	@OneToMany(mappedBy="tbCeIdDetDigNombre")
	private List<Tb_CeIdDetErrDigNom> tbCeIdDetErrDigNoms;

	public Tb_CeIdDetDigNombre() {
	}

	public long getFld_CorrDigNom_Id() {
		return this.fld_CorrDigNom_Id;
	}

	public void setFld_CorrDigNom_Id(long fld_CorrDigNom_Id) {
		this.fld_CorrDigNom_Id = fld_CorrDigNom_Id;
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public List<Tb_CeIdDetErrDigNom> getTbCeIdDetErrDigNoms() {
		return this.tbCeIdDetErrDigNoms;
	}

	public void setTbCeIdDetErrDigNoms(List<Tb_CeIdDetErrDigNom> tbCeIdDetErrDigNoms) {
		this.tbCeIdDetErrDigNoms = tbCeIdDetErrDigNoms;
	}

	public Tb_CeIdDetErrDigNom addTbCeIdDetErrDigNom(Tb_CeIdDetErrDigNom tbCeIdDetErrDigNom) {
		getTbCeIdDetErrDigNoms().add(tbCeIdDetErrDigNom);
		tbCeIdDetErrDigNom.setTbCeIdDetDigNombre(this);

		return tbCeIdDetErrDigNom;
	}

	public Tb_CeIdDetErrDigNom removeTbCeIdDetErrDigNom(Tb_CeIdDetErrDigNom tbCeIdDetErrDigNom) {
		getTbCeIdDetErrDigNoms().remove(tbCeIdDetErrDigNom);
		tbCeIdDetErrDigNom.setTbCeIdDetDigNombre(null);

		return tbCeIdDetErrDigNom;
	}

}