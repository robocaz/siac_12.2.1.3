package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_EmisoresPub database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EmisoresPub.findAll", query="SELECT t FROM Tbl_EmisoresPub t")
public class Tbl_EmisoresPub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_EmisoresPub() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}