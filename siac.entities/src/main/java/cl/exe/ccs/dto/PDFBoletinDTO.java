package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PDFBoletinDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 2063502091023566495L;

	private String fecUltimaMulta;
	private String fecUltimaDeuda;
	private Integer nroMultasLaboral;
	private Integer nroDeudasLaboral;
	private BigDecimal montoMultaLaboral;
	private BigDecimal montoDeudaLaboral;
	private Integer nroAcreedores;
	private Integer nroTrabajadores;
	private String nombreInfractor;
	private String codAutenticBic;
	private List<BoletinData> lista;

	public String getFecUltimaMulta() {
		return fecUltimaMulta;
	}

	public void setFecUltimaMulta(String fecUltimaMulta) {
		this.fecUltimaMulta = fecUltimaMulta;
	}

	public String getFecUltimaDeuda() {
		return fecUltimaDeuda;
	}

	public void setFecUltimaDeuda(String fecUltimaDeuda) {
		this.fecUltimaDeuda = fecUltimaDeuda;
	}

	public Integer getNroMultasLaboral() {
		return nroMultasLaboral;
	}

	public void setNroMultasLaboral(Integer nroMultasLaboral) {
		this.nroMultasLaboral = nroMultasLaboral;
	}

	public Integer getNroDeudasLaboral() {
		return nroDeudasLaboral;
	}

	public void setNroDeudasLaboral(Integer nroDeudasLaboral) {
		this.nroDeudasLaboral = nroDeudasLaboral;
	}

	public BigDecimal getMontoMultaLaboral() {
		return montoMultaLaboral;
	}

	public void setMontoMultaLaboral(BigDecimal montoMultaLaboral) {
		this.montoMultaLaboral = montoMultaLaboral;
	}

	public BigDecimal getMontoDeudaLaboral() {
		return montoDeudaLaboral;
	}

	public void setMontoDeudaLaboral(BigDecimal montoDeudaLaboral) {
		this.montoDeudaLaboral = montoDeudaLaboral;
	}

	public Integer getNroAcreedores() {
		return nroAcreedores;
	}

	public void setNroAcreedores(Integer nroAcreedores) {
		this.nroAcreedores = nroAcreedores;
	}

	public Integer getNroTrabajadores() {
		return nroTrabajadores;
	}

	public void setNroTrabajadores(Integer nroTrabajadores) {
		this.nroTrabajadores = nroTrabajadores;
	}

	public String getNombreInfractor() {
		return nombreInfractor;
	}

	public void setNombreInfractor(String nombreInfractor) {
		this.nombreInfractor = nombreInfractor;
	}

	public String getCodAutenticBic() {
		return codAutenticBic;
	}

	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}

	public List<BoletinData> getLista() {
		return lista;
	}

	public void setLista(List<BoletinData> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n fecUltimaMulta: " + fecUltimaMulta);
		str.append(" ,fecUltimaDeuda: " + fecUltimaDeuda);
		str.append(" ,nroMultasLaboral: " + nroMultasLaboral);
		str.append(" ,nroDeudasLaboral: " + nroDeudasLaboral);
		str.append(" ,montoMultaLaboral: " + montoMultaLaboral);
		str.append(" ,montoDeudaLaboral: " + montoDeudaLaboral);
		str.append(" ,nroAcreedores: " + nroAcreedores);
		str.append(" ,nroTrabajadores: " + nroTrabajadores);
		str.append(" ,nombreInfractor: " + nombreInfractor);
		str.append(" ,codAutenticBic: " + codAutenticBic);
		str.append(" ,lista: " + lista);
		return str.toString();
	}

}
