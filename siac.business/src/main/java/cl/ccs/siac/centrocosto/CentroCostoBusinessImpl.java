package cl.ccs.siac.centrocosto;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.CentroCostoDao;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("centroCostoBusiness")
public class CentroCostoBusinessImpl implements CentroCostoBusiness {
	
	@Inject
	private CentroCostoDao centroCostoDao;
	
	@Override
	public ResponseListDTO<ComboDTO> listar(String username) throws Exception {
		ResponseListDTO<ComboDTO> respuesta = centroCostoDao.listar(username);
		return respuesta;
	}

	@Override
	public ResponseDTO modificar(String username, String codigoCentroCosto) throws Exception {
		ResponseDTO respuesta = centroCostoDao.modificar(username, codigoCentroCosto);
		return respuesta;
	}
}
