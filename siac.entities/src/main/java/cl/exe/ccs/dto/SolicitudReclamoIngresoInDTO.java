package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoIngresoInDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 1832131004031763633L;

	private Integer corrCaja;
	private String rutTitular;
	private Integer corrNombre;
	private String email;
	private Integer codComuna;
	private String direccion;
	private String telefono;
	private Integer cantDoc;
	private Integer tipoSolicitud;
	private Integer codCategoria;
	private String usuario;
	private String texto;

	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(String rutTitular) {
		this.rutTitular = rutTitular;
	}
	public Integer getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Integer corrNombre) {
		this.corrNombre = corrNombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getCodComuna() {
		return codComuna;
	}
	public void setCodComuna(Integer codComuna) {
		this.codComuna = codComuna;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Integer getCantDoc() {
		return cantDoc;
	}
	public void setCantDoc(Integer cantDoc) {
		this.cantDoc = cantDoc;
	}
	public Integer getTipoSolicitud() {
		return tipoSolicitud;
	}
	public void setTipoSolicitud(Integer tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	public Integer getCodCategoria() {
		return codCategoria;
	}
	public void setCodCategoria(Integer codCategoria) {
		this.codCategoria = codCategoria;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudReclamoIngresoInDTO [corrCaja=");
		builder.append(corrCaja);
		builder.append(", rutTitular=");
		builder.append(rutTitular);
		builder.append(", corrNombre=");
		builder.append(corrNombre);
		builder.append(", email=");
		builder.append(email);
		builder.append(", codComuna=");
		builder.append(codComuna);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", cantDoc=");
		builder.append(cantDoc);
		builder.append(", tipoSolicitud=");
		builder.append(tipoSolicitud);
		builder.append(", codCategoria=");
		builder.append(codCategoria);
		builder.append(", usuario=");
		builder.append(usuario);
		builder.append(", texto=");
		builder.append(texto);
		builder.append("]");
		return builder.toString();
	}

}
