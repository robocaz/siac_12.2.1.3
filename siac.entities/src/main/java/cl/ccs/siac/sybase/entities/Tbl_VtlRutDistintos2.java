package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_VtlRutDistintos2 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VtlRutDistintos2.findAll", query="SELECT t FROM Tbl_VtlRutDistintos2 t")
public class Tbl_VtlRutDistintos2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_RutBueno")
	private String fld_RutBueno;

	public Tbl_VtlRutDistintos2() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_RutBueno() {
		return this.fld_RutBueno;
	}

	public void setFld_RutBueno(String fld_RutBueno) {
		this.fld_RutBueno = fld_RutBueno;
	}

}