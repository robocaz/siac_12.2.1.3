package cl.exe.ccs.dto;

import java.io.Serializable;

public class BoletinDTO implements Serializable {

	private static final long serialVersionUID = 2279090875954902029L;

	private String rutAfectado;
	private Long corrMovimiento;
	private String codServicio;
	private String codUsuario;
	
	private String centroCosto;

	public String getRutAfectado() {
		return rutAfectado;
	}

	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}

	public Long getCorrMovimiento() {
		return corrMovimiento;
	}

	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}

	public String getCodServicio() {
		return codServicio;
	}

	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("rutAfectado: " + rutAfectado);
		str.append(" ,corrMovimiento: " + corrMovimiento);
		str.append(" ,codServicio: " + codServicio);
		str.append(" ,codUsuario: " + codUsuario);
		str.append(" ,centroCosto: " + centroCosto);
		return str.toString();
	}
	
}
