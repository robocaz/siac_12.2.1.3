package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ErrorProtesto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ErrorProtesto.findAll", query="SELECT t FROM Tbl_ErrorProtesto t")
public class Tbl_ErrorProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CodRechazo")
	private int fld_CodRechazo;

	@Column(name="Fld_Flag_Advertencia")
	private boolean fld_Flag_Advertencia;

	@Column(name="Fld_Flag_Digitacion")
	private boolean fld_Flag_Digitacion;

	@Column(name="Fld_Flag_Error")
	private boolean fld_Flag_Error;

	@Column(name="Fld_Flag_Verificar")
	private boolean fld_Flag_Verificar;

	@Column(name="Fld_GlosaAccion")
	private String fld_GlosaAccion;

	@Column(name="Fld_GlosaEstadoCampo")
	private String fld_GlosaEstadoCampo;

	@Column(name="Fld_GlosaSituacion")
	private String fld_GlosaSituacion;

	@Column(name="Fld_MensajeError")
	private String fld_MensajeError;

	@Column(name="Fld_Prioridad")
	private short fld_Prioridad;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_ErrorProtesto() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public int getFld_CodRechazo() {
		return this.fld_CodRechazo;
	}

	public void setFld_CodRechazo(int fld_CodRechazo) {
		this.fld_CodRechazo = fld_CodRechazo;
	}

	public boolean getFld_Flag_Advertencia() {
		return this.fld_Flag_Advertencia;
	}

	public void setFld_Flag_Advertencia(boolean fld_Flag_Advertencia) {
		this.fld_Flag_Advertencia = fld_Flag_Advertencia;
	}

	public boolean getFld_Flag_Digitacion() {
		return this.fld_Flag_Digitacion;
	}

	public void setFld_Flag_Digitacion(boolean fld_Flag_Digitacion) {
		this.fld_Flag_Digitacion = fld_Flag_Digitacion;
	}

	public boolean getFld_Flag_Error() {
		return this.fld_Flag_Error;
	}

	public void setFld_Flag_Error(boolean fld_Flag_Error) {
		this.fld_Flag_Error = fld_Flag_Error;
	}

	public boolean getFld_Flag_Verificar() {
		return this.fld_Flag_Verificar;
	}

	public void setFld_Flag_Verificar(boolean fld_Flag_Verificar) {
		this.fld_Flag_Verificar = fld_Flag_Verificar;
	}

	public String getFld_GlosaAccion() {
		return this.fld_GlosaAccion;
	}

	public void setFld_GlosaAccion(String fld_GlosaAccion) {
		this.fld_GlosaAccion = fld_GlosaAccion;
	}

	public String getFld_GlosaEstadoCampo() {
		return this.fld_GlosaEstadoCampo;
	}

	public void setFld_GlosaEstadoCampo(String fld_GlosaEstadoCampo) {
		this.fld_GlosaEstadoCampo = fld_GlosaEstadoCampo;
	}

	public String getFld_GlosaSituacion() {
		return this.fld_GlosaSituacion;
	}

	public void setFld_GlosaSituacion(String fld_GlosaSituacion) {
		this.fld_GlosaSituacion = fld_GlosaSituacion;
	}

	public String getFld_MensajeError() {
		return this.fld_MensajeError;
	}

	public void setFld_MensajeError(String fld_MensajeError) {
		this.fld_MensajeError = fld_MensajeError;
	}

	public short getFld_Prioridad() {
		return this.fld_Prioridad;
	}

	public void setFld_Prioridad(short fld_Prioridad) {
		this.fld_Prioridad = fld_Prioridad;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}