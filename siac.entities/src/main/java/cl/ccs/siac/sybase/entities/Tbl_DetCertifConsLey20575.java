package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetCertifConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetCertifConsLey20575.findAll", query="SELECT t FROM Tbl_DetCertifConsLey20575 t")
public class Tbl_DetCertifConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMotivo")
	private short fld_CodMotivo;

	@Column(name="Fld_CodResponsable")
	private short fld_CodResponsable;

	@Column(name="Fld_CorrConsulta")
	private BigDecimal fld_CorrConsulta;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_RutRequirente")
	private String fld_RutRequirente;

	public Tbl_DetCertifConsLey20575() {
	}

	public short getFld_CodMotivo() {
		return this.fld_CodMotivo;
	}

	public void setFld_CodMotivo(short fld_CodMotivo) {
		this.fld_CodMotivo = fld_CodMotivo;
	}

	public short getFld_CodResponsable() {
		return this.fld_CodResponsable;
	}

	public void setFld_CodResponsable(short fld_CodResponsable) {
		this.fld_CodResponsable = fld_CodResponsable;
	}

	public BigDecimal getFld_CorrConsulta() {
		return this.fld_CorrConsulta;
	}

	public void setFld_CorrConsulta(BigDecimal fld_CorrConsulta) {
		this.fld_CorrConsulta = fld_CorrConsulta;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_RutRequirente() {
		return this.fld_RutRequirente;
	}

	public void setFld_RutRequirente(String fld_RutRequirente) {
		this.fld_RutRequirente = fld_RutRequirente;
	}

}