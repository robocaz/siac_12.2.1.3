package cl.ccs.siac.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class declaration
 *
 *
 * @author
 * @version 1.10, 08/04/00
 */
public class Impresora {
	// Variables de acceso al dispositivo
	private FileWriter fw;
	private BufferedWriter bw;
	private PrintWriter pw;
	private String dispositivo = "";

	/** Esta funcion inicia el dispositivo donde se va a imprimir **/

	public void setDispositivo(String texto) {
		dispositivo = texto;
		try {
			fw = new FileWriter("COM4:");
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void escribir(String texto) {
		try {

			pw.println(texto);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void cortar() {
		try {

			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'm' };
			pw.write(ESC_CUT_PAPER);
			

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void avanza_pagina() {
		try {
			pw.write(0x0C);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void setRojo() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 1 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void setNegro() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 0 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void setTipoCaracterLatino() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'R', 18 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void setFormato(int formato) {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, '!', (char) formato };
			pw.write(ESC_CUT_PAPER);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void correr(int fin) {
		try {
			int i = 0;
			for (i = 1; i <= fin; i++) {
				this.salto();
			}

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void salto() {
		try {

			pw.println("");

		} catch (Exception e) {
			System.out.print(e);

		}

	}

	public void dividir() {
		escribir("—————————————-");

	}

	public void cerrarDispositivo() {
		try {

			pw.close();
			if (this.dispositivo.trim().equals("pantalla.txt")) {

				java.io.File archivo = new java.io.File("pantalla.txt");
				java.awt.Desktop.getDesktop().open(archivo);
			}

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public static void main(String args[]) {
		
		String numeroBoleta = String.format("%07d", 78453);
		String corrCaja = String.format("%09d", 568222);
		String centroCosto = String.format("%04d", 2530);
		String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
		StringBuffer stb = new StringBuffer();

		
	
		stb.append("\n\n\n\n\n\n\n");
		stb.append("\n");
		stb.append("\n");
		stb.append("   " + corrCaja + " " + centroCosto + "  " + date);
		
		
		Impresora p = new Impresora();
		p.setDispositivo("");
		p.escribir((char) 27 + "@");
		p.setTipoCaracterLatino();
		p.setNegro();
		p.setFormato(10);
		p.escribir(stb.toString());
		p.escribir("\n");
		//iteracion
		for (int i = 0; i < 9; i++) {
			p.escribir("  " + "1 " + "   " +"descripcion qw" + "       "+ "4000");
		}
		//p.escribir("\n");
		p.escribir("                 " + "Total $36000");
		
		
		p.correr(10);
		p.cortar();
		p.cerrarDispositivo();
	}
}