package cl.ccs.siac.protestos;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.ProtestosDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ProtestoAclaracionDTO;
import cl.exe.ccs.dto.ProtestoData;
import cl.exe.ccs.dto.ProtestoDetalleDTO;
import cl.exe.ccs.dto.ProtestoOutDTO;
import cl.exe.ccs.dto.ProtestoTramitanteDTO;
import cl.exe.ccs.dto.ProtestoInDTO;

@Named("protestosBusiness")
public class ProtestosBusinessImpl implements ProtestosBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private ProtestosDao protestosDao;
	
	@Override
	public ProtestoOutDTO buscar(ProtestoInDTO params) {
		
		params.setRut(utils.formatZeroRut(params.getRut()));
		ProtestoOutDTO out = protestosDao.buscar(params);
		
		for ( ProtestoData prot : out.getLista() ) {
			prot.setRutAfectado(prot.getRutAfectado().replaceFirst("^0*", ""));
			prot.setFecProt(utils.checkTimestampToStrFechaVista(prot.getFecProt()));
			prot.setFecPago(utils.checkTimestampToStrFechaVista(prot.getFecPago()));
			prot.setFecVcto(utils.checkTimestampToStrFechaVista(prot.getFecVcto()));
		}
		
		return out;
	}
	
	@Override
	public ProtestoDetalleDTO detalle(Integer corrProt) {
		
		ProtestoDetalleDTO out = protestosDao.detalle(corrProt);
		
		out.setFecProt(utils.checkTimestampToStrFechaVista(out.getFecProt()));
		out.setFecVcto(utils.checkTimestampToStrFechaVista(out.getFecVcto()));
		out.setFecDigitacion(utils.checkTimestampToStrFechaHoraVista(out.getFecDigitacion()));
		out.setFecModificacion(utils.checkTimestampToStrFechaHoraVista(out.getFecModificacion()));
		out.setFecPublicacion(utils.checkTimestampToStrFechaVista(out.getFecPublicacion()));
		out.setFecMarcaAclaracion(utils.checkTimestampToStrFechaVista(out.getFecMarcaAclaracion()));
		out.setFecMarcaAclRectificacion(utils.checkTimestampToStrFechaVista(out.getFecMarcaAclRectificacion()));
		out.setFecMarcaAclEspAnulada(utils.checkTimestampToStrFechaVista(out.getFecMarcaAclEspAnulada()));
		out.setFecMarcaAclEspecial(utils.checkTimestampToStrFechaVista(out.getFecMarcaAclEspecial()));
		
		return out;
	}
	
	@Override
	public ProtestoAclaracionDTO aclaracion(Integer corrAcl) {
		
		ProtestoAclaracionDTO out = protestosDao.aclaracion(corrAcl);
		out.setFecDigitacion(utils.checkTimestampToStrFechaHoraVista(out.getFecDigitacion()));
		out.setFecPago(utils.checkTimestampToStrFechaHoraVista(out.getFecPago()));
		
		return out;
	}
	
	@Override
	public ProtestoTramitanteDTO tramitante(String rut) {
		
		return protestosDao.tramitante(rut);
	}

}
