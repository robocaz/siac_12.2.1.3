package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_HorarioAP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_HorarioAP.findAll", query="SELECT t FROM Tbl_HorarioAP t")
public class Tbl_HorarioAP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuarioIngreso")
	private String fld_CodUsuarioIngreso;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_Corr_Id")
	private BigDecimal fld_Corr_Id;

	@Column(name="Fld_DiaSemana")
	private byte fld_DiaSemana;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_HoraInicio")
	private String fld_HoraInicio;

	@Column(name="Fld_HoraTermino")
	private String fld_HoraTermino;

	@Column(name="Fld_Vigente")
	private boolean fld_Vigente;

	public Tbl_HorarioAP() {
	}

	public String getFld_CodUsuarioIngreso() {
		return this.fld_CodUsuarioIngreso;
	}

	public void setFld_CodUsuarioIngreso(String fld_CodUsuarioIngreso) {
		this.fld_CodUsuarioIngreso = fld_CodUsuarioIngreso;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_Corr_Id() {
		return this.fld_Corr_Id;
	}

	public void setFld_Corr_Id(BigDecimal fld_Corr_Id) {
		this.fld_Corr_Id = fld_Corr_Id;
	}

	public byte getFld_DiaSemana() {
		return this.fld_DiaSemana;
	}

	public void setFld_DiaSemana(byte fld_DiaSemana) {
		this.fld_DiaSemana = fld_DiaSemana;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public String getFld_HoraInicio() {
		return this.fld_HoraInicio;
	}

	public void setFld_HoraInicio(String fld_HoraInicio) {
		this.fld_HoraInicio = fld_HoraInicio;
	}

	public String getFld_HoraTermino() {
		return this.fld_HoraTermino;
	}

	public void setFld_HoraTermino(String fld_HoraTermino) {
		this.fld_HoraTermino = fld_HoraTermino;
	}

	public boolean getFld_Vigente() {
		return this.fld_Vigente;
	}

	public void setFld_Vigente(boolean fld_Vigente) {
		this.fld_Vigente = fld_Vigente;
	}

}