package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetAutenticFnq database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetAutenticFnq.findAll", query="SELECT t FROM Tbl_DetAutenticFnq t")
public class Tbl_DetAutenticFnq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAutenticacion")
	private BigDecimal fld_CorrAutenticacion;

	@Column(name="Fld_FecAux")
	private Timestamp fld_FecAux;

	@Column(name="Fld_FecFinContrato")
	private Timestamp fld_FecFinContrato;

	@Column(name="Fld_FecIniContrato")
	private Timestamp fld_FecIniContrato;

	@Column(name="Fld_Nombre_ApMat_Emp")
	private String fld_Nombre_ApMat_Emp;

	@Column(name="Fld_Nombre_ApPat_Emp")
	private String fld_Nombre_ApPat_Emp;

	@Column(name="Fld_Nombre_Nombres_Emp")
	private String fld_Nombre_Nombres_Emp;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutEmpresa")
	private String fld_RutEmpresa;

	public Tbl_DetAutenticFnq() {
	}

	public BigDecimal getFld_CorrAutenticacion() {
		return this.fld_CorrAutenticacion;
	}

	public void setFld_CorrAutenticacion(BigDecimal fld_CorrAutenticacion) {
		this.fld_CorrAutenticacion = fld_CorrAutenticacion;
	}

	public Timestamp getFld_FecAux() {
		return this.fld_FecAux;
	}

	public void setFld_FecAux(Timestamp fld_FecAux) {
		this.fld_FecAux = fld_FecAux;
	}

	public Timestamp getFld_FecFinContrato() {
		return this.fld_FecFinContrato;
	}

	public void setFld_FecFinContrato(Timestamp fld_FecFinContrato) {
		this.fld_FecFinContrato = fld_FecFinContrato;
	}

	public Timestamp getFld_FecIniContrato() {
		return this.fld_FecIniContrato;
	}

	public void setFld_FecIniContrato(Timestamp fld_FecIniContrato) {
		this.fld_FecIniContrato = fld_FecIniContrato;
	}

	public String getFld_Nombre_ApMat_Emp() {
		return this.fld_Nombre_ApMat_Emp;
	}

	public void setFld_Nombre_ApMat_Emp(String fld_Nombre_ApMat_Emp) {
		this.fld_Nombre_ApMat_Emp = fld_Nombre_ApMat_Emp;
	}

	public String getFld_Nombre_ApPat_Emp() {
		return this.fld_Nombre_ApPat_Emp;
	}

	public void setFld_Nombre_ApPat_Emp(String fld_Nombre_ApPat_Emp) {
		this.fld_Nombre_ApPat_Emp = fld_Nombre_ApPat_Emp;
	}

	public String getFld_Nombre_Nombres_Emp() {
		return this.fld_Nombre_Nombres_Emp;
	}

	public void setFld_Nombre_Nombres_Emp(String fld_Nombre_Nombres_Emp) {
		this.fld_Nombre_Nombres_Emp = fld_Nombre_Nombres_Emp;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutEmpresa() {
		return this.fld_RutEmpresa;
	}

	public void setFld_RutEmpresa(String fld_RutEmpresa) {
		this.fld_RutEmpresa = fld_RutEmpresa;
	}

}