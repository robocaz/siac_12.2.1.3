package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdDetEstEnvios database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetEstEnvios")
@NamedQuery(name="Tb_CeIdDetEstEnvio.findAll", query="SELECT t FROM Tb_CeIdDetEstEnvio t")
public class Tb_CeIdDetEstEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodUsuarioResp")
	private String fld_CodUsuarioResp;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	public Tb_CeIdDetEstEnvio() {
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioResp() {
		return this.fld_CodUsuarioResp;
	}

	public void setFld_CodUsuarioResp(String fld_CodUsuarioResp) {
		this.fld_CodUsuarioResp = fld_CodUsuarioResp;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

}