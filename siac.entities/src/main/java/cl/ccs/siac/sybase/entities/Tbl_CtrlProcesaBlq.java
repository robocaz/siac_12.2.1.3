package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtrlProcesaBlq database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CtrlProcesaBlq.findAll", query="SELECT t FROM Tbl_CtrlProcesaBlq t")
public class Tbl_CtrlProcesaBlq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrBlq_Id")
	private BigDecimal fld_CorrBlq_Id;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FolioArch")
	private short fld_FolioArch;

	@Column(name="Fld_TotRegistros")
	private int fld_TotRegistros;

	public Tbl_CtrlProcesaBlq() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrBlq_Id() {
		return this.fld_CorrBlq_Id;
	}

	public void setFld_CorrBlq_Id(BigDecimal fld_CorrBlq_Id) {
		this.fld_CorrBlq_Id = fld_CorrBlq_Id;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public short getFld_FolioArch() {
		return this.fld_FolioArch;
	}

	public void setFld_FolioArch(short fld_FolioArch) {
		this.fld_FolioArch = fld_FolioArch;
	}

	public int getFld_TotRegistros() {
		return this.fld_TotRegistros;
	}

	public void setFld_TotRegistros(int fld_TotRegistros) {
		this.fld_TotRegistros = fld_TotRegistros;
	}

}