package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdSucBancosSAC database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdSucBancosSAC.findAll", query="SELECT t FROM Tb_CeIdSucBancosSAC t")
public class Tb_CeIdSucBancosSAC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoBanco")
	private int fld_CodigoBanco;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	public Tb_CeIdSucBancosSAC() {
	}

	public int getFld_CodigoBanco() {
		return this.fld_CodigoBanco;
	}

	public void setFld_CodigoBanco(int fld_CodigoBanco) {
		this.fld_CodigoBanco = fld_CodigoBanco;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

}