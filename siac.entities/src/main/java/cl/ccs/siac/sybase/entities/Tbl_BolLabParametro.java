package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_BolLabParametros database table.
 * 
 */
@Entity
@Table(name="Tbl_BolLabParametros")
@NamedQuery(name="Tbl_BolLabParametro.findAll", query="SELECT t FROM Tbl_BolLabParametro t")
public class Tbl_BolLabParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodLaboral")
	private String fld_CodLaboral;

	@Column(name="Fld_Descripcion")
	private String fld_Descripcion;

	@Column(name="Fld_RubroLaboral")
	private byte fld_RubroLaboral;

	public Tbl_BolLabParametro() {
	}

	public String getFld_CodLaboral() {
		return this.fld_CodLaboral;
	}

	public void setFld_CodLaboral(String fld_CodLaboral) {
		this.fld_CodLaboral = fld_CodLaboral;
	}

	public String getFld_Descripcion() {
		return this.fld_Descripcion;
	}

	public void setFld_Descripcion(String fld_Descripcion) {
		this.fld_Descripcion = fld_Descripcion;
	}

	public byte getFld_RubroLaboral() {
		return this.fld_RubroLaboral;
	}

	public void setFld_RubroLaboral(byte fld_RubroLaboral) {
		this.fld_RubroLaboral = fld_RubroLaboral;
	}

}