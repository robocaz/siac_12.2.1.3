package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONSOLIDADOMAESTRO database table.
 * 
 */
@Entity
@Table(name="TBL_CONSOLIDADOMAESTRO")
@NamedQuery(name="TblConsolidadomaestro.findAll", query="SELECT t FROM TblConsolidadomaestro t")
public class TblConsolidadomaestro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAACTUALIZACION")
	private Date fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVENCIMIENTO")
	private Date fldFechavencimiento;

	@Column(name="FLD_MONEDA")
	private String fldMoneda;

	@Column(name="FLD_MONTO")
	private BigDecimal fldMonto;

	@Column(name="FLD_NOMBREDEUDOR")
	private String fldNombredeudor;

	@Column(name="FLD_RUTDEUDOR")
	private String fldRutdeudor;

	@Column(name="FLD_TOTALDOCUMENTOS")
	private BigDecimal fldTotaldocumentos;

	public TblConsolidadomaestro() {
	}

	public Date getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Date fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechavencimiento() {
		return this.fldFechavencimiento;
	}

	public void setFldFechavencimiento(Date fldFechavencimiento) {
		this.fldFechavencimiento = fldFechavencimiento;
	}

	public String getFldMoneda() {
		return this.fldMoneda;
	}

	public void setFldMoneda(String fldMoneda) {
		this.fldMoneda = fldMoneda;
	}

	public BigDecimal getFldMonto() {
		return this.fldMonto;
	}

	public void setFldMonto(BigDecimal fldMonto) {
		this.fldMonto = fldMonto;
	}

	public String getFldNombredeudor() {
		return this.fldNombredeudor;
	}

	public void setFldNombredeudor(String fldNombredeudor) {
		this.fldNombredeudor = fldNombredeudor;
	}

	public String getFldRutdeudor() {
		return this.fldRutdeudor;
	}

	public void setFldRutdeudor(String fldRutdeudor) {
		this.fldRutdeudor = fldRutdeudor;
	}

	public BigDecimal getFldTotaldocumentos() {
		return this.fldTotaldocumentos;
	}

	public void setFldTotaldocumentos(BigDecimal fldTotaldocumentos) {
		this.fldTotaldocumentos = fldTotaldocumentos;
	}

}