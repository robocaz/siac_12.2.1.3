package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutcp database table.
 * 
 */
@Entity
@Table(name="tmp_rutcp")
@NamedQuery(name="TmpRutcp.findAll", query="SELECT t FROM TmpRutcp t")
public class TmpRutcp implements Serializable {
	private static final long serialVersionUID = 1L;

	private String glosa;

	public TmpRutcp() {
	}

	public String getGlosa() {
		return this.glosa;
	}

	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

}