package cl.ccs.siac.printer;

import java.io.File;
import java.io.FileReader;

import javax.ejb.Stateless;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import cl.exe.ccs.dto.InicioConfigDTO;

@Named("cargaConfigImpl")
@Stateless
public class CargaConfigImpl implements CargaConfig {

	private static final Logger LOG = LogManager.getLogger(CargaConfigImpl.class);

	public InicioConfigDTO cargaConfiguracion() {

		InicioConfigDTO config = new InicioConfigDTO();
		try {

			Gson gson = new Gson();
			if (new File("./configuracion.json").exists()) {
				JsonReader reader = new JsonReader(new FileReader("./configuracion.json"));
				config = gson.fromJson(reader, InicioConfigDTO.class);
				config.setIdControl(0);
			} else {
				config.setIdControl(1);
			}

		} catch (Exception e) {
			config.setIdControl(1);
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

		return config;

	}
}
