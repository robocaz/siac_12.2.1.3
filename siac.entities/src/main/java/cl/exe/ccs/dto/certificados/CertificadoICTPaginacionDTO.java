package cl.exe.ccs.dto.certificados;

import java.util.ArrayList;
import java.util.List;

public class CertificadoICTPaginacionDTO {
    private Integer pagina;
    private boolean bc;
    private int bc_inicio;
    private int bc_termino;
    private int acl_inicio;
    private int acl_termino;
    private int mc_inicio;
    private int mc_termino;
    private List<CertificadoICTbcDTO> bc_coleccion;
    private boolean acl;
    private List<CertificadoICTbcDTO> acl_coleccion;
    private boolean mc;
    private List<CertificadoICTmcDTO> mc_coleccion;

    public CertificadoICTPaginacionDTO() {
        this.pagina = 0;
        this.bc = false;
        this.bc_coleccion = new ArrayList<CertificadoICTbcDTO>();
        this.acl = false;
        this.acl_coleccion = new ArrayList<CertificadoICTbcDTO>();
        this.mc = false;
        this.mc_coleccion = new ArrayList<CertificadoICTmcDTO>();
    }

    public Integer getPagina() {
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina = pagina;
    }

    public boolean isBc() {
        return bc;
    }

    public boolean getBc() {
        return bc;
    }

    public void setBc(boolean bc) {
        this.bc = bc;
    }

    public int getBc_inicio() {
        return bc_inicio;
    }

    public void setBc_inicio(int bc_inicio) {
        this.bc_inicio = bc_inicio;
    }

    public int getBc_termino() {
        return bc_termino;
    }

    public void setBc_termino(int bc_termino) {
        this.bc_termino = bc_termino;
    }

    public int getAcl_inicio() {
        return acl_inicio;
    }

    public void setAcl_inicio(int acl_inicio) {
        this.acl_inicio = acl_inicio;
    }

    public int getAcl_termino() {
        return acl_termino;
    }

    public void setAcl_termino(int acl_termino) {
        this.acl_termino = acl_termino;
    }

    public int getMc_inicio() {
        return mc_inicio;
    }

    public void setMc_inicio(int mc_inicio) {
        this.mc_inicio = mc_inicio;
    }

    public int getMc_termino() {
        return mc_termino;
    }

    public void setMc_termino(int mc_termino) {
        this.mc_termino = mc_termino;
    }

    public List<CertificadoICTbcDTO> getBc_coleccion() {
        return bc_coleccion;
    }

    public void setBc_coleccion(List<CertificadoICTbcDTO> bc_coleccion) {
        this.bc_coleccion = bc_coleccion;
    }

    public boolean isAcl() {
        return acl;
    }

    public void setAcl(boolean acl) {
        this.acl = acl;
    }

    public List<CertificadoICTbcDTO> getAcl_coleccion() {
        return acl_coleccion;
    }

    public void setAcl_coleccion(List<CertificadoICTbcDTO> acl_coleccion) {
        this.acl_coleccion = acl_coleccion;
    }

    public boolean isMc() {
        return mc;
    }

    public void setMc(boolean mc) {
        this.mc = mc;
    }

    public List<CertificadoICTmcDTO> getMc_coleccion() {
        return mc_coleccion;
    }

    public void setMc_coleccion(List<CertificadoICTmcDTO> mc_coleccion) {
        this.mc_coleccion = mc_coleccion;
    }

    @Override
    public String toString() {
        return "CertificadoICTPaginacionDTO{" +
                "pagina=" + pagina +
                ", bc=" + bc +
                ", bc_inicio=" + bc_inicio +
                ", bc_termino=" + bc_termino +
                ", bc_coleccion=" + bc_coleccion.size() +
                ", acl=" + acl +
                ", acl_inicio=" + acl_inicio +
                ", acl_termino=" + acl_termino +
                ", acl_coleccion=" + acl_coleccion.size() +
                ", mc=" + mc +
                ", mc_inicio=" + mc_inicio +
                ", mc_termino=" + mc_termino +
                ", mc_coleccion=" + mc_coleccion.size() +
                '}';
    }
}
