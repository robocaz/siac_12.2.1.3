package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysusermessages database table.
 * 
 */
@Entity
@Table(name="sysusermessages")
@NamedQuery(name="Sysusermessage.findAll", query="SELECT s FROM Sysusermessage s")
public class Sysusermessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private String description;

	private short dlevel;

	private int error;

	private short langid;

	private int uid;

	public Sysusermessage() {
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public short getDlevel() {
		return this.dlevel;
	}

	public void setDlevel(short dlevel) {
		this.dlevel = dlevel;
	}

	public int getError() {
		return this.error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public short getLangid() {
		return this.langid;
	}

	public void setLangid(short langid) {
		this.langid = langid;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}