package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalAclPend database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalAclPend.findAll", query="SELECT t FROM Tbl_CausalAclPend t")
public class Tbl_CausalAclPend implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalPend")
	private byte fld_CodCausalPend;

	@Column(name="Fld_GlosaCausalPend")
	private String fld_GlosaCausalPend;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CausalAclPend() {
	}

	public byte getFld_CodCausalPend() {
		return this.fld_CodCausalPend;
	}

	public void setFld_CodCausalPend(byte fld_CodCausalPend) {
		this.fld_CodCausalPend = fld_CodCausalPend;
	}

	public String getFld_GlosaCausalPend() {
		return this.fld_GlosaCausalPend;
	}

	public void setFld_GlosaCausalPend(String fld_GlosaCausalPend) {
		this.fld_GlosaCausalPend = fld_GlosaCausalPend;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}