package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RADetalle database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RADetalle.findAll", query="SELECT t FROM Tbl_RADetalle t")
public class Tbl_RADetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrDetalleSolic_Id")
	private BigDecimal fld_CorrDetalleSolic_Id;

	@Column(name="Fld_CorrSolicArriendo")
	private BigDecimal fld_CorrSolicArriendo;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_Monto")
	private BigDecimal fld_Monto;

	public Tbl_RADetalle() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrDetalleSolic_Id() {
		return this.fld_CorrDetalleSolic_Id;
	}

	public void setFld_CorrDetalleSolic_Id(BigDecimal fld_CorrDetalleSolic_Id) {
		this.fld_CorrDetalleSolic_Id = fld_CorrDetalleSolic_Id;
	}

	public BigDecimal getFld_CorrSolicArriendo() {
		return this.fld_CorrSolicArriendo;
	}

	public void setFld_CorrSolicArriendo(BigDecimal fld_CorrSolicArriendo) {
		this.fld_CorrSolicArriendo = fld_CorrSolicArriendo;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public BigDecimal getFld_Monto() {
		return this.fld_Monto;
	}

	public void setFld_Monto(BigDecimal fld_Monto) {
		this.fld_Monto = fld_Monto;
	}

}