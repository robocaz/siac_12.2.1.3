package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_FnqAnuladosProrrogados database table.
 * 
 */
@Entity
@Table(name="Tbl_FnqAnuladosProrrogados")
@NamedQuery(name="Tbl_FnqAnuladosProrrogado.findAll", query="SELECT t FROM Tbl_FnqAnuladosProrrogado t")
public class Tbl_FnqAnuladosProrrogado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecFiniquito")
	private Timestamp fld_FecFiniquito;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_MarcaAnulado")
	private boolean fld_MarcaAnulado;

	@Column(name="Fld_MarcaProrroga")
	private boolean fld_MarcaProrroga;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutEmpleador")
	private String fld_RutEmpleador;

	public Tbl_FnqAnuladosProrrogado() {
	}

	public Timestamp getFld_FecFiniquito() {
		return this.fld_FecFiniquito;
	}

	public void setFld_FecFiniquito(Timestamp fld_FecFiniquito) {
		this.fld_FecFiniquito = fld_FecFiniquito;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public boolean getFld_MarcaAnulado() {
		return this.fld_MarcaAnulado;
	}

	public void setFld_MarcaAnulado(boolean fld_MarcaAnulado) {
		this.fld_MarcaAnulado = fld_MarcaAnulado;
	}

	public boolean getFld_MarcaProrroga() {
		return this.fld_MarcaProrroga;
	}

	public void setFld_MarcaProrroga(boolean fld_MarcaProrroga) {
		this.fld_MarcaProrroga = fld_MarcaProrroga;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutEmpleador() {
		return this.fld_RutEmpleador;
	}

	public void setFld_RutEmpleador(String fld_RutEmpleador) {
		this.fld_RutEmpleador = fld_RutEmpleador;
	}

}