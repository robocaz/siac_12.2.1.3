package cl.exe.ccs.dto;

import java.io.Serializable;

public class ArqueoCajaTipoPagoData implements Serializable{

	private String glosa;
	private Integer trxs;
	private Integer mntoIngresado;
	private Integer mntoCobrado;
	private Integer mntoDiff;

	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public Integer getTrxs() {
		return trxs;
	}
	public void setTrxs(Integer trxs) {
		this.trxs = trxs;
	}
	public Integer getMntoIngresado() {
		return mntoIngresado;
	}
	public void setMntoIngresado(Integer mntoIngresado) {
		this.mntoIngresado = mntoIngresado;
	}
	public Integer getMntoCobrado() {
		return mntoCobrado;
	}
	public void setMntoCobrado(Integer mntoCobrado) {
		this.mntoCobrado = mntoCobrado;
	}
	public Integer getMntoDiff() {
		return mntoDiff;
	}
	public void setMntoDiff(Integer mntoDiff) {
		this.mntoDiff = mntoDiff;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaTipoPagoData [glosa=");
		builder.append(glosa);
		builder.append(", trxs=");
		builder.append(trxs);
		builder.append(", mntoIngresado=");
		builder.append(mntoIngresado);
		builder.append(", mntoCobrado=");
		builder.append(mntoCobrado);
		builder.append(", mntoDiff=");
		builder.append(mntoDiff);
		builder.append("]");
		return builder.toString();
	}

}
