package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TETipoDocAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TETipoDocAclOnline.findAll", query="SELECT t FROM Tbl_TETipoDocAclOnline t")
public class Tbl_TETipoDocAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_TETipoDocAclOnline() {
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}