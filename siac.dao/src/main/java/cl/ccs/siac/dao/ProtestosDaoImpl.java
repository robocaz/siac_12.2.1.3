package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ProtestoOutDTO;
import cl.exe.ccs.dto.ProtestoTramitanteDTO;
import cl.exe.ccs.dto.ProtestoAclaracionDTO;
import cl.exe.ccs.dto.ProtestoData;
import cl.exe.ccs.dto.ProtestoDetalleDTO;
import cl.exe.ccs.dto.ProtestoInDTO;

@Named("protestosDao")
@Stateless
public class ProtestosDaoImpl implements ProtestosDao {
	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;
	
	private static final Logger LOG = LogManager.getLogger(ProtestosDaoImpl.class);
	
	@Override
	public ProtestoOutDTO buscar(ProtestoInDTO params) {
		LOG.debug("> DAO: buscar - SP: SQ_BusProtestos_SW2");
		
		ProtestoOutDTO out = new ProtestoOutDTO();
		List<ProtestoData> lista = new ArrayList<ProtestoData>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_BusProtestos_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Boolean.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, Boolean.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, Boolean.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getRut());
			spq.setParameter(2, params.getOpcAcl());
			spq.setParameter(3, params.getOpcPrt());
			spq.setParameter(4, params.getOpcOmi());
			spq.setParameter(5, params.getCodUser());
			LOG.debug("> Params: " + params.toString());
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			if ( result != null && !result.isEmpty() ) {
				
				for ( Object row : result ) {
					Object[] data = (Object[]) row;
					ProtestoData prot = new ProtestoData();
					
					prot.setRutAfectado(data[0].toString());
					prot.setGlosaCorta(data[1].toString());
					prot.setTipoDocumento(data[2].toString());
					prot.setGlosaMoneda(data[3].toString());
					prot.setMontoProt((BigDecimal) data[4]);
					prot.setFecProt(data[5].toString());
					prot.setFecPago(data[6].toString());
					prot.setNroOper4Dig((Integer) data[7]);
					prot.setNroBoletin((Integer) data[8]);
					prot.setGlosaEstado(data[9].toString());
					prot.setCorrProt((BigDecimal) data[10]);
					prot.setCorrAclId((BigDecimal) data[11]);
					prot.setFecVcto(data[12].toString());
					
					lista.add(prot);
				}
				
			}
			
			out.setLista(lista);
			
			out.setNombreAfectado(spq.getOutputParameterValue(6).toString().trim());
			out.setTotReg((Integer) spq.getOutputParameterValue(7));
			
			out.setIdControl((Integer) spq.getOutputParameterValue(8));
			out.setMsgControl(spq.getOutputParameterValue(9).toString());
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ProtestoDetalleDTO detalle(Integer corrProt) {
		LOG.debug("> DAO: detalle - SP: SQ_DetProtesto_SW2");
		
		ProtestoDetalleDTO out = new ProtestoDetalleDTO();
		List<String[]> lista = new ArrayList<String[]>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_DetProtesto_SW2");
			
			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(4, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(12, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(13, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(14, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(15, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(16, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(17, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(18, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(19, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(20, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(21, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(22, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(23, Boolean.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(24, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(25, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(26, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(27, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(28, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(29, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(30, Boolean.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(31, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(32, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(33, Boolean.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(34, Float.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(35, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(36, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(37, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(38, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, new BigDecimal(corrProt));
			LOG.debug("> Params: " + corrProt);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			if ( result != null && !result.isEmpty() ) {
				
				for ( Object row : result ) {
					Object[] data = (Object[]) row;
					String[] fila = new String[4];
					fila[0] = data[0].toString();
					fila[1] = data[1].toString();
					fila[2] = (data[2] != null) ? data[2].toString() : new String();
					fila[3] = (data[3] != null) ? data[3].toString() : new String();
					lista.add(fila);
				}
				
			}
			
			out.setLista(lista);
			
			out.setGlosaTipoDocumento(spq.getOutputParameterValue(2).toString());
			out.setGlosaCorta(spq.getOutputParameterValue(3).toString().trim());
			out.setMontoProt((BigDecimal) spq.getOutputParameterValue(4));
			out.setFecProt(spq.getOutputParameterValue(5).toString());
			out.setNroOperacion(spq.getOutputParameterValue(6).toString().trim());
			out.setGlosaCausalProt(spq.getOutputParameterValue(7).toString().trim());
			out.setGlosaMarcaAclOmi(spq.getOutputParameterValue(8).toString().trim());
			out.setFecPublicacion(spq.getOutputParameterValue(9).toString());
			out.setPagBoletin((Integer) spq.getOutputParameterValue(10));
			out.setNroBoletin((Integer) spq.getOutputParameterValue(11));
			out.setGlosaTipoEmisor(spq.getOutputParameterValue(12).toString());
			out.setGlosaEmisor(spq.getOutputParameterValue(13).toString());
			out.setGlosaSucursal(spq.getOutputParameterValue(14).toString());
			out.setFecModificacion(spq.getOutputParameterValue(15).toString());
			out.setCodUsuarioModif(spq.getOutputParameterValue(16).toString().trim());
			out.setFecDigitacion(spq.getOutputParameterValue(17).toString());
			out.setCodUsuarioDigitacion(spq.getOutputParameterValue(18).toString());
			out.setFecVcto(spq.getOutputParameterValue(19).toString());
			out.setGlosaTipoDocumentoImpago(spq.getOutputParameterValue(20).toString().trim());
			out.setNroOper4Dig((Integer) spq.getOutputParameterValue(21));
			out.setGlosaCortaAlias((spq.getOutputParameterValue(22) != null) ? spq.getOutputParameterValue(22).toString().trim() : new String());
			out.setMarcaAclRectificacion((Boolean) spq.getOutputParameterValue(23));
			out.setFecMarcaAclEspecial(spq.getOutputParameterValue(24).toString());
			out.setFecMarcaAclaracion(spq.getOutputParameterValue(25).toString());
			out.setGlosaTipoRelacion((spq.getOutputParameterValue(26) != null) ? spq.getOutputParameterValue(26).toString().trim() : new String());
			out.setMontoProtOriginal((BigDecimal) spq.getOutputParameterValue(27));
			out.setGlosaEstado(spq.getOutputParameterValue(28).toString().trim());
			out.setCorrEnvio((BigDecimal) spq.getOutputParameterValue(29));
			out.setMarcaAclEspAnulada((Boolean) spq.getOutputParameterValue(30));
			out.setFecMarcaAclEspAnulada(spq.getOutputParameterValue(31).toString());
			out.setFecMarcaAclRectificacion(spq.getOutputParameterValue(32).toString());
			out.setMarcaAclEspecial((Boolean) spq.getOutputParameterValue(33));
			out.setNroLote((Double) spq.getOutputParameterValue(34));
			out.setNroBoletinAcl((Integer) spq.getOutputParameterValue(35));
			out.setPagBoletinAcl((Integer) spq.getOutputParameterValue(36));
			out.setGlosaLocPub(spq.getOutputParameterValue(37).toString().trim());
			out.setNombreLibrador(spq.getOutputParameterValue(38).toString().trim());
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ProtestoAclaracionDTO aclaracion(Integer corrAcl) {
		LOG.debug("> DAO: aclaracion - SP: SQ_DetAclaracion_SW2");
		
		ProtestoAclaracionDTO out = new ProtestoAclaracionDTO();
		List<String[]> lista = new ArrayList<String[]>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_DetAclaracion_SW2");
			
			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(4, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, new BigDecimal(corrAcl));
			LOG.debug("> Params: " + corrAcl);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			if ( result != null && !result.isEmpty() ) {
				
				for ( Object row : result ) {
					Object[] data = (Object[]) row;
					String[] fila = new String[4];
					fila[0] = data[0].toString();
					fila[1] = data[1].toString();
					fila[2] = data[2].toString();
					fila[3] = data[3].toString();
					lista.add(fila);
				}
				
			}
			
			out.setLista(lista);
			
			out.setFecDigitacion(spq.getOutputParameterValue(2).toString());
			out.setCorrProt((BigDecimal) spq.getOutputParameterValue(3));
			out.setCorrCaja((BigDecimal) spq.getOutputParameterValue(4));
			out.setCodUsuario(spq.getOutputParameterValue(5).toString());
			out.setFecPago(spq.getOutputParameterValue(6).toString());
			out.setCodCCosto((Integer) spq.getOutputParameterValue(7));
			out.setGlosaCCosto((spq.getOutputParameterValue(8) != null) ? spq.getOutputParameterValue(8).toString().trim() : new String());
			out.setRutTramitante(spq.getOutputParameterValue(9).toString().trim());
			out.setGlosaTipoDocAclaracion(spq.getOutputParameterValue(10).toString().trim());
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ProtestoTramitanteDTO tramitante(String rut) {
		LOG.debug("> DAO: tramitante - SP: SQ_DetTramitante_SW2");
		
		ProtestoTramitanteDTO out = new ProtestoTramitanteDTO();
		
		try {
			
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_DetTramitante_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, rut);
			
			LOG.debug("> Params: " + rut);
			
			spq.execute();
			
			out.setCorrelativo((BigDecimal) spq.getOutputParameterValue(2));
			out.setApellidoPaterno(spq.getOutputParameterValue(3).toString());
			out.setApellidoMaterno(spq.getOutputParameterValue(4).toString());
			out.setNombres(spq.getOutputParameterValue(5).toString());
			out.setDireccion(spq.getOutputParameterValue(6).toString());
			out.setTelefono(spq.getOutputParameterValue(7).toString());
			out.setGiro(spq.getOutputParameterValue(8).toString());
			out.setComuna(spq.getOutputParameterValue(9).toString());
			out.setRegion(spq.getOutputParameterValue(10).toString());
			out.setEmail((spq.getOutputParameterValue(11) != null) ? spq.getOutputParameterValue(11).toString() : new String());
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
}
