angular.module('ccs').directive('mayusculas', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
                this.value = this.value.toUpperCase();
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();
            });
        }
    };
});
