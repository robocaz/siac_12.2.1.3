package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_VCEstDetEnvio database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VCEstDetEnvio.findAll", query="SELECT t FROM Tbl_VCEstDetEnvio t")
public class Tbl_VCEstDetEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrEnvioVC")
	private BigDecimal fld_CorrEnvioVC;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	public Tbl_VCEstDetEnvio() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrEnvioVC() {
		return this.fld_CorrEnvioVC;
	}

	public void setFld_CorrEnvioVC(BigDecimal fld_CorrEnvioVC) {
		this.fld_CorrEnvioVC = fld_CorrEnvioVC;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

}