package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_SolVtaDetalle database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolVtaDetalle.findAll", query="SELECT t FROM Tbl_SolVtaDetalle t")
public class Tbl_SolVtaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_ConTA")
	private byte fld_ConTA;

	@Column(name="Fld_Correlativo")
	private BigDecimal fld_Correlativo;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrNombreContratante")
	private BigDecimal fld_CorrNombreContratante;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_DireccionCompleta")
	private String fld_DireccionCompleta;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_MontoMovimiento")
	private BigDecimal fld_MontoMovimiento;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutContratante")
	private String fld_RutContratante;

	@Column(name="Fld_TelefonoCelular")
	private String fld_TelefonoCelular;

	@Column(name="Fld_TelefonoDomicilio")
	private String fld_TelefonoDomicilio;

	public Tbl_SolVtaDetalle() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public byte getFld_ConTA() {
		return this.fld_ConTA;
	}

	public void setFld_ConTA(byte fld_ConTA) {
		this.fld_ConTA = fld_ConTA;
	}

	public BigDecimal getFld_Correlativo() {
		return this.fld_Correlativo;
	}

	public void setFld_Correlativo(BigDecimal fld_Correlativo) {
		this.fld_Correlativo = fld_Correlativo;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrNombreContratante() {
		return this.fld_CorrNombreContratante;
	}

	public void setFld_CorrNombreContratante(BigDecimal fld_CorrNombreContratante) {
		this.fld_CorrNombreContratante = fld_CorrNombreContratante;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public String getFld_DireccionCompleta() {
		return this.fld_DireccionCompleta;
	}

	public void setFld_DireccionCompleta(String fld_DireccionCompleta) {
		this.fld_DireccionCompleta = fld_DireccionCompleta;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public BigDecimal getFld_MontoMovimiento() {
		return this.fld_MontoMovimiento;
	}

	public void setFld_MontoMovimiento(BigDecimal fld_MontoMovimiento) {
		this.fld_MontoMovimiento = fld_MontoMovimiento;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutContratante() {
		return this.fld_RutContratante;
	}

	public void setFld_RutContratante(String fld_RutContratante) {
		this.fld_RutContratante = fld_RutContratante;
	}

	public String getFld_TelefonoCelular() {
		return this.fld_TelefonoCelular;
	}

	public void setFld_TelefonoCelular(String fld_TelefonoCelular) {
		this.fld_TelefonoCelular = fld_TelefonoCelular;
	}

	public String getFld_TelefonoDomicilio() {
		return this.fld_TelefonoDomicilio;
	}

	public void setFld_TelefonoDomicilio(String fld_TelefonoDomicilio) {
		this.fld_TelefonoDomicilio = fld_TelefonoDomicilio;
	}

}