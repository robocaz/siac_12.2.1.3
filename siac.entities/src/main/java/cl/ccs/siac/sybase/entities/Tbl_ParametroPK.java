package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_Parametros database table.
 * 
 */
@Embeddable
public class Tbl_ParametroPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_TipoParametro")
	private String fld_TipoParametro;

	@Column(name="Fld_Codigo")
	private short fld_Codigo;

	public Tbl_ParametroPK() {
	}
	public String getFld_TipoParametro() {
		return this.fld_TipoParametro;
	}
	public void setFld_TipoParametro(String fld_TipoParametro) {
		this.fld_TipoParametro = fld_TipoParametro;
	}
	public short getFld_Codigo() {
		return this.fld_Codigo;
	}
	public void setFld_Codigo(short fld_Codigo) {
		this.fld_Codigo = fld_Codigo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_ParametroPK)) {
			return false;
		}
		Tbl_ParametroPK castOther = (Tbl_ParametroPK)other;
		return 
			this.fld_TipoParametro.equals(castOther.fld_TipoParametro)
			&& (this.fld_Codigo == castOther.fld_Codigo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fld_TipoParametro.hashCode();
		hash = hash * prime + ((int) this.fld_Codigo);
		
		return hash;
	}
}