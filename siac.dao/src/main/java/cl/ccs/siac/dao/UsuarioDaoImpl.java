package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Named("usuarioDao")
@Stateless
public class UsuarioDaoImpl implements UsuarioDao {

	@PersistenceContext(unitName = "oracle")
	private EntityManager em;

	@PersistenceContext(unitName = "orcl-link")
	private EntityManager em_oracle_link;

	private static final Logger log = LogManager.getLogger(UsuarioDaoImpl.class);
	
	public UsuarioDTO obtenerUsuario(String username) {
		
		
		try{
			
			log.info("Ejecutando MOL.PKG_SIAC.SQ_DatosUsuario");
			
			StoredProcedureQuery storedProcedure = em_oracle_link.createStoredProcedureQuery("MOL.PKG_SIAC.SQ_DatosUsuario");
			storedProcedure.registerStoredProcedureParameter("PUSUARIO", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("PNOMBRE", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("PCCOSTO", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("PGLOSACCOSTO", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pClasificacionCCosto", Integer.class, ParameterMode.OUT);
			
			storedProcedure.registerStoredProcedureParameter("PEMAIL", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("PMENSAJE", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("PRETURN", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pRegistro", void.class, ParameterMode.REF_CURSOR);
			
	
			if(username != null){
				storedProcedure.setParameter("PUSUARIO", username.trim());
			}
			else{
				storedProcedure.setParameter("PUSUARIO", "");
			}
			// execute SP
			storedProcedure.execute();
			// get result
			UsuarioDTO usuarioDTO = new UsuarioDTO();
	
			if (Integer.parseInt(storedProcedure.getOutputParameterValue("PRETURN").toString()) != 0) {
				usuarioDTO.setMsgControl(storedProcedure.getOutputParameterValue("PMENSAJE").toString());
				usuarioDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("PRETURN").toString()));
				return usuarioDTO;
			} else {
				usuarioDTO.setUsername(username);
				usuarioDTO.setNombre(storedProcedure.getOutputParameterValue("PNOMBRE").toString());
				CentroCostoDTO centroCostoActivo = new CentroCostoDTO();
				centroCostoActivo.setCodigo(storedProcedure.getOutputParameterValue("PCCOSTO").toString());
				centroCostoActivo.setDescripcion(storedProcedure.getOutputParameterValue("PGLOSACCOSTO").toString());
				centroCostoActivo.setClasificacion(Integer.parseInt(storedProcedure.getOutputParameterValue("pClasificacionCCosto").toString()));
				usuarioDTO.setCentroCostoActivo(centroCostoActivo);
				usuarioDTO.setEmail(storedProcedure.getOutputParameterValue("PEMAIL").toString());
				usuarioDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("PRETURN").toString()));
				
				List<String> lista = new ArrayList<String>();
				List<Object[]> listaCentroCosto = storedProcedure.getResultList();
				for (Object item : listaCentroCosto) {
					Object[] datos = (Object[]) item;
					lista.add(datos[0].toString());
				}
				usuarioDTO.setPrivilegios(lista);
				
				return usuarioDTO;
			}
		} 
		catch (Exception e) {
			log.error("Error en el SP MOL.PKG_SIAC.SQ_DatosUsuario " + e.getMessage());
			return null;
		}


	}

	public UsuarioDTO esSupervisor(String usuario, String password) {
		
		try{
			
			log.info("Ejecutando MOL.PKG_SIAC.SQ_SupervisorSiac");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("MOL.PKG_SIAC.SQ_SupervisorSiac");
			storedProcedure.registerStoredProcedureParameter("pUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pPassword", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pReturn", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pMensaje", String.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("pUsuario", usuario);
			storedProcedure.setParameter("pPassword", password);
	
			storedProcedure.execute();
			
			UsuarioDTO usuarioDTO =  new UsuarioDTO();
			usuarioDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("pReturn").toString()));
			usuarioDTO.setMsgControl(storedProcedure.getOutputParameterValue("pMensaje").toString());
			
	
			return usuarioDTO;
		}
		catch (Exception e) {
			log.error("Error en el SP MOL.PKG_SIAC.SQ_SupervisorSiac " + e.getMessage());
			return null;
		}		
		
	}
	
	public ResponseDTO cambiarClave(String usuario, String password, String passwordAux) {

		
		try{
			
			log.info("Ejecutando MOL.PKG_USUARIOS.SU_NVAPASSWORD");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("MOL.PKG_USUARIOS.SU_NVAPASSWORD");
			storedProcedure.registerStoredProcedureParameter("pUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pPassword", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pPasswordAux", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pMensaje", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pReturn", Integer.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("pUsuario", usuario);
			storedProcedure.setParameter("pPassword", password);
			storedProcedure.setParameter("pPasswordAux", passwordAux);
	
			storedProcedure.execute();
			
			ResponseDTO responseDTO =  new ResponseDTO();
			responseDTO.setMsgControl(storedProcedure.getOutputParameterValue("pMensaje").toString());
			responseDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("pReturn").toString()));
	
			return responseDTO;
		}
		catch (Exception e) {
			log.error("Error en el SP MOL.PKG_USUARIOS.SU_NVAPASSWORD" + e.getMessage());
			return null;
		}			
	}

}
