package cl.ccs.siac.pdf;


import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PdfUtil {

    private static int fontSize;

    public PdfUtil() {
        super();
    }

    public PdfPCell setDisableBorderTRBL(PdfPCell cell) {
        cell.setBorder(0);
        return cell;
    }

    public PdfPCell setDisableBorderTRB(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        return cell;
    }

    public PdfPCell setDisableBorderTR(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthRight(0);
        return cell;
    }

    public PdfPCell setDisableBorderT(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        return cell;
    }

    public PdfPCell setDisableBorderTBL(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public PdfPCell setDisableBorderTB(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        ;
        return cell;
    }

    public PdfPCell setDisableBorderTRL(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public PdfPCell setDisableBorderTL(PdfPCell cell) {
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    /////////////////////////////////////////////

    public PdfPCell setDisableBorderRBL(PdfPCell cell) {
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public PdfPCell setDisableBorderRL(PdfPCell cell) {
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public PdfPCell setDisableBorderRB(PdfPCell cell) {
        cell.setBorderWidthRight(0);
        cell.setBorderWidthBottom(0);
        return cell;
    }

    public PdfPCell setDisableBorderR(PdfPCell cell) {
        cell.setBorderWidthRight(0);
        return cell;
    }

    //////////////////////////////////////////////////

    public PdfPCell setDisableBorderBL(PdfPCell cell) {
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public PdfPCell setDisableBorderB(PdfPCell cell) {
        cell.setBorderWidthBottom(0);
        return cell;
    }

    /////////////////////////////////////////////////

    public PdfPCell setDisableBorderL(PdfPCell cell) {
        cell.setBorderWidthLeft(0);
        return cell;
    }

    public static PdfPCell getCell(int text) {
        return getCell(String.valueOf(text), Element.ALIGN_RIGHT);
    }

    public static PdfPCell getCell(double text) {
        return getCell(String.valueOf(text), Element.ALIGN_RIGHT);
    }

    public static PdfPCell getCell(Date text) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        return getCell(format.format(text), Element.ALIGN_LEFT);
    }

    public static PdfPCell getCell(String text) {
        return getCell(text, Element.ALIGN_LEFT);
    }


    public static PdfPCell getCell(Date text, int align) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return getCell(format.format(text), align, Font.NORMAL);
    }

    public static PdfPCell getCell(String text, int align) {
        return getCell(text, align, Font.NORMAL);
    }

    public static PdfPCell getCell(String text, int align, int border) {
        text = text == null ? "" : text;
        Font font = new Font(Font.FontFamily.COURIER, fontSize, border);
        Chunk chunkText = new Chunk(text, font);
        Paragraph paragraph = new Paragraph(chunkText);

        PdfPCell cell = new PdfPCell(paragraph);
        cell.setHorizontalAlignment(align);
        return cell;
    }
    
    public static PdfPCell getCell(String text, int align, int border, Font font) {
        text = text == null ? "" : text;
        Chunk chunkText = new Chunk(text, font);
        Paragraph paragraph = new Paragraph(chunkText);

        PdfPCell cell = new PdfPCell(paragraph);
        cell.setHorizontalAlignment(align);
        cell.setBorder(border);
        return cell;
    }

    public static void setFontSize(int fontSize) {
        PdfUtil.fontSize = fontSize;
    }

    public static int getFontSize() {
        return fontSize;
    }

    public void writeTextPosition(PdfWriter writer, Chunk chunk, int posX, int posY) {
        PdfPTable table = new PdfPTable(1);
        try {
            table.setWidths(new int[] { 24 });
            table.setTotalWidth(500);
            PdfPCell cell = new PdfPCell();
            cell.addElement(chunk);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            table.writeSelectedRows(0, -1, posX, posY, writer.getDirectContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void writeTextPosition(PdfWriter writer, String text, int posX, int posY) {
        PdfPTable table = new PdfPTable(1);
        try {
            table.setWidths(new int[] { 24 });
            table.setTotalWidth(500);
            PdfPCell cell = new PdfPCell();
            Chunk chunk = new Chunk(text);
            cell.addElement(chunk);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            table.writeSelectedRows(0, -1, posX, posY, writer.getDirectContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void writeTextPositionEditable(PdfWriter writer, String text, int posX, int posY, Font f) {
        PdfPTable table = new PdfPTable(1);
        try {
            table.setWidths(new int[] { 24 });
            table.setTotalWidth(600);
            PdfPCell cell = new PdfPCell();
            Chunk chunk = new Chunk(text,f);
            cell.addElement(chunk);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            table.writeSelectedRows(0, -1, posX, posY, writer.getDirectContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
