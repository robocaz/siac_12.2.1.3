package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_RESUMENTRANSAC database table.
 * 
 */
@Entity
@Table(name="TBL_RESUMENTRANSAC")
@NamedQuery(name="TblResumentransac.findAll", query="SELECT t FROM TblResumentransac t")
public class TblResumentransac implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CANTIDADTRANSAC")
	private BigDecimal fldCantidadtransac;

	@Column(name="FLD_CODTRANSACCION")
	private BigDecimal fldCodtransaccion;

	@Column(name="FLD_CORRPERFIL")
	private BigDecimal fldCorrperfil;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATRANSAC")
	private Date fldFechatransac;

	@Column(name="FLD_FLAGBATCH")
	private BigDecimal fldFlagbatch;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	public TblResumentransac() {
	}

	public BigDecimal getFldCantidadtransac() {
		return this.fldCantidadtransac;
	}

	public void setFldCantidadtransac(BigDecimal fldCantidadtransac) {
		this.fldCantidadtransac = fldCantidadtransac;
	}

	public BigDecimal getFldCodtransaccion() {
		return this.fldCodtransaccion;
	}

	public void setFldCodtransaccion(BigDecimal fldCodtransaccion) {
		this.fldCodtransaccion = fldCodtransaccion;
	}

	public BigDecimal getFldCorrperfil() {
		return this.fldCorrperfil;
	}

	public void setFldCorrperfil(BigDecimal fldCorrperfil) {
		this.fldCorrperfil = fldCorrperfil;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechatransac() {
		return this.fldFechatransac;
	}

	public void setFldFechatransac(Date fldFechatransac) {
		this.fldFechatransac = fldFechatransac;
	}

	public BigDecimal getFldFlagbatch() {
		return this.fldFlagbatch;
	}

	public void setFldFlagbatch(BigDecimal fldFlagbatch) {
		this.fldFlagbatch = fldFlagbatch;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

}