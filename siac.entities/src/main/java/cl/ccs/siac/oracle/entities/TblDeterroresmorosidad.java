package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_DETERRORESMOROSIDAD database table.
 * 
 */
@Entity
@Table(name="TBL_DETERRORESMOROSIDAD")
@NamedQuery(name="TblDeterroresmorosidad.findAll", query="SELECT t FROM TblDeterroresmorosidad t")
public class TblDeterroresmorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_APMATERNO")
	private String fldApmaterno;

	@Column(name="FLD_APPATERNO")
	private String fldAppaterno;

	@Column(name="FLD_CODCAMPO")
	private BigDecimal fldCodcampo;

	@Column(name="FLD_CODERROR")
	private BigDecimal fldCoderror;

	@Column(name="FLD_CODMONEDA")
	private String fldCodmoneda;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	@Column(name="FLD_COMUNA")
	private String fldComuna;

	@Column(name="FLD_CORRENVIODIG")
	private BigDecimal fldCorrenviodig;

	@Column(name="FLD_CORRMORDIG")
	private BigDecimal fldCorrmordig;

	@Column(name="FLD_DIRECCION")
	private String fldDireccion;

	@Column(name="FLD_FECHAVCTO")
	private String fldFechavcto;

	@Column(name="FLD_FOLIOMOR")
	private BigDecimal fldFoliomor;

	@Column(name="FLD_MONTO")
	private String fldMonto;

	@Column(name="FLD_NOMBRES")
	private String fldNombres;

	@Column(name="FLD_NRODOCUMENTO")
	private String fldNrodocumento;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_RUTRELACIONADO")
	private String fldRutrelacionado;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_TIPORELACION")
	private String fldTiporelacion;

	public TblDeterroresmorosidad() {
	}

	public String getFldApmaterno() {
		return this.fldApmaterno;
	}

	public void setFldApmaterno(String fldApmaterno) {
		this.fldApmaterno = fldApmaterno;
	}

	public String getFldAppaterno() {
		return this.fldAppaterno;
	}

	public void setFldAppaterno(String fldAppaterno) {
		this.fldAppaterno = fldAppaterno;
	}

	public BigDecimal getFldCodcampo() {
		return this.fldCodcampo;
	}

	public void setFldCodcampo(BigDecimal fldCodcampo) {
		this.fldCodcampo = fldCodcampo;
	}

	public BigDecimal getFldCoderror() {
		return this.fldCoderror;
	}

	public void setFldCoderror(BigDecimal fldCoderror) {
		this.fldCoderror = fldCoderror;
	}

	public String getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(String fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}

	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public String getFldComuna() {
		return this.fldComuna;
	}

	public void setFldComuna(String fldComuna) {
		this.fldComuna = fldComuna;
	}

	public BigDecimal getFldCorrenviodig() {
		return this.fldCorrenviodig;
	}

	public void setFldCorrenviodig(BigDecimal fldCorrenviodig) {
		this.fldCorrenviodig = fldCorrenviodig;
	}

	public BigDecimal getFldCorrmordig() {
		return this.fldCorrmordig;
	}

	public void setFldCorrmordig(BigDecimal fldCorrmordig) {
		this.fldCorrmordig = fldCorrmordig;
	}

	public String getFldDireccion() {
		return this.fldDireccion;
	}

	public void setFldDireccion(String fldDireccion) {
		this.fldDireccion = fldDireccion;
	}

	public String getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(String fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldFoliomor() {
		return this.fldFoliomor;
	}

	public void setFldFoliomor(BigDecimal fldFoliomor) {
		this.fldFoliomor = fldFoliomor;
	}

	public String getFldMonto() {
		return this.fldMonto;
	}

	public void setFldMonto(String fldMonto) {
		this.fldMonto = fldMonto;
	}

	public String getFldNombres() {
		return this.fldNombres;
	}

	public void setFldNombres(String fldNombres) {
		this.fldNombres = fldNombres;
	}

	public String getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(String fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldRutrelacionado() {
		return this.fldRutrelacionado;
	}

	public void setFldRutrelacionado(String fldRutrelacionado) {
		this.fldRutrelacionado = fldRutrelacionado;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldTiporelacion() {
		return this.fldTiporelacion;
	}

	public void setFldTiporelacion(String fldTiporelacion) {
		this.fldTiporelacion = fldTiporelacion;
	}

}