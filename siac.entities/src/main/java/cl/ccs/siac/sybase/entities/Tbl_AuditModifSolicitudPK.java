package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_AuditModifSolicitud database table.
 * 
 */
@Embeddable
public class Tbl_AuditModifSolicitudPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRegistro")
	private long fld_CorrRegistro;

	@Column(name="Fld_RutUsuario")
	private String fld_RutUsuario;

	public Tbl_AuditModifSolicitudPK() {
	}
	public long getFld_CorrRegistro() {
		return this.fld_CorrRegistro;
	}
	public void setFld_CorrRegistro(long fld_CorrRegistro) {
		this.fld_CorrRegistro = fld_CorrRegistro;
	}
	public String getFld_RutUsuario() {
		return this.fld_RutUsuario;
	}
	public void setFld_RutUsuario(String fld_RutUsuario) {
		this.fld_RutUsuario = fld_RutUsuario;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_AuditModifSolicitudPK)) {
			return false;
		}
		Tbl_AuditModifSolicitudPK castOther = (Tbl_AuditModifSolicitudPK)other;
		return 
			(this.fld_CorrRegistro == castOther.fld_CorrRegistro)
			&& this.fld_RutUsuario.equals(castOther.fld_RutUsuario);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_CorrRegistro ^ (this.fld_CorrRegistro >>> 32)));
		hash = hash * prime + this.fld_RutUsuario.hashCode();
		
		return hash;
	}
}