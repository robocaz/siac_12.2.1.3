angular.module('ccs').controller('CertificadoAclaracionProceso', function ($scope, $window,TransaccionResource ,AclaracionResource, DetectorFraudeResource, TramitanteResource, PersonaResource, PagosResource ,$location,RutHelper,
		 $rootScope, $uibModal, $document, $cookies, $timeout, $filter,$q){


	$rootScope.tramitante;
	$rootScope.usuario;
	$scope.afectado = {};
	$scope.listaAclaraciones = [];
	$scope.listaSeleccionadas = [];
	$scope.validasupervisor = false;
	$scope.tramitanteCarga = {};
	$scope.ruts = [];
	$scope.costoCAR = {};
	$scope.esrutjuridico = false;
	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);
	$scope.valorCAR = {};
	$scope.valorICT = {};
	$scope.cargaValor = function(servicio){
		
		var successCallback = function(data,responseHeaders){
			console.log("---Costo Servicio--- " + servicio);
			console.log(data);
			if(servicio == "ICT"){
				$scope.valorICT = data;
			}	
			if(servicio == "CAR"){
				$scope.valorCAR = data;
			}	
			
		}
		PagosResource.valorServicio(servicio, successCallback, $scope.errorCallback);
		
	}

	$rootScope.$on('callEvent', function(event, data, afectado) {

		var deferred = $q.defer();
		var loopPromises = [];
		$scope.listaAclaraciones = data;
		$scope.respuestaAct = TramitanteResource.carga({rut: $rootScope.tramitante.rut});
		$scope.respuestaAct.$promise.then(function(tramitanteRut) {
			console.log(tramitanteRut);
			$scope.tramitanteCarga = tramitanteRut;
			deferred.resolve(tramitanteRut);
		});

		deferred.promise;

	    //if(loopPromises.length > 0){
			$q.all(deferred).then(function () {
				$scope.afectado = afectado;
				$scope.iniciaListado();
			});
		//}
	});


	$scope.cargaTramitante = function(tramitante){
		var successCallback = function(data,responseHeaders){
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
				$scope.tramitanteCarga = data;
			}

		};
        TramitanteResource.carga({rut: $rootScope.tramitante.rut}, successCallback, $scope.errorCallback);
	};


	$scope.iniciaListado = function(){

		//$scope.existe = $scope.listaAclaraciones.filter((subject) => subject.rut !== $rootScope.tramitante.rut)[0];
		
		
		var keepGoing = true;
		angular.forEach($scope.listaAclaraciones, function(acl){
		  if(keepGoing) {
			if(acl.rut != $scope.tramitanteCarga.rut){
			  var esjuridico = $scope.esRutJuridicoFirmante(acl.rut);	
		      if(!esjuridico){
				  $scope.validasupervisor = true;
				  acl.validasupervisor = true;
			      keepGoing = false
		      }
		      else{
				  $scope.validasupervisor = false;
				  acl.validasupervisor = false;
			      keepGoing = false		    	  
		      }
		    }
		    else{
		    	$scope.validasupervisor = false;
		    }
		  }
		});
		$scope.cargaValor("ICT");
		$scope.cargaValor("CAR");
		angular.element("#informeconsolidadotramite1").modal();
	};





	$scope.validaSupervisorModal = function(metodo, div){

		if($scope.listaSeleccionadas.length == 0){
			 swal('Alerta',"Seleccione algún registro",'error');
			 return false;
		}
		
		//if($scope.validasupervisor == true){
			
		//	$scope.authSupervisor(metodo, div);
		//}
		//else{
			if(metodo === "aceptaICL"){
				$scope.aceptaICL(div);
			}
			else if(metodo === "aceptaCar"){
				$scope.aceptaCar(div);
			}
			//metodo
		//}
		console.log();
	};

	
	$scope.cantPromesasTerminadas = 0;
	$scope.deferred = $q.defer();

	$scope.loopPromises = [];
    
	$scope.aceptaICL = function(div){


		if($scope.listaSeleccionadas.length == 0){
			 swal('Alerta',"Seleccione algún registro",'error');
			 return false;
		}


	    angular.forEach($scope.listaSeleccionadas, function (aclaracionItem) {
	    	
	    	
	    	if(aclaracionItem.validasupervisor){

	    		
	    		var modalInstance = $uibModal.open({
	    			templateUrl: 'views/modal/modalloginsupervisor.html',
	    			controller: 'ModalLoginSupervisorController',
	    			appendTo: angular.element($document[0].querySelector('#'+ div)),
	    			size: 'sm'
	    		  });
	    		  modalInstance.result.then(function (respuesta) {
	    			  if(respuesta){
	    			    	if(aclaracionItem.cantidad === undefined){
	    			    		swal('Alerta',"Ingrese las cantidades",'error');
	    			    		return false;
	    			    	}

	    			    	for(i = 0; i < aclaracionItem.cantidad; i++){

	    						$scope.transaccion = {
	    							codServicio : 'ICT',
	    							rutAfectado : aclaracionItem.rut,
	    							corrNombre: aclaracionItem.correlativo
	    						}


	    						$scope.respuestaAct = TransaccionResource.agregarservicio($scope.transaccion);
	    						$scope.respuestaAct.$promise.then(function(data) {
	    							console.log(data);
	    							$scope.deferred.resolve(data);
	    						});

	    						$scope.loopPromises.push($scope.deferred.promise);

	    			    	}
	    			    	$scope.cantPromesasTerminadas = $scope.cantPromesasTerminadas + 1;
	    			    	aclaracionItem.cantidad = "";
	    			    	aclaracionItem.checked = false;	 
	    			    	$scope.validaPromesas();
	    			  }
	    		  });
	    		  
	    	}
	    	else{
		    	if(aclaracionItem.cantidad === undefined){
		    		swal('Alerta',"Ingrese las cantidades",'error');
		    		return false;
		    	}

		    	for(i = 0; i < aclaracionItem.cantidad; i++){

					$scope.transaccion = {
						codServicio : 'ICT',
						rutAfectado : aclaracionItem.rut,
						corrNombre: aclaracionItem.correlativo
					}


					$scope.respuestaAct = TransaccionResource.agregarservicio($scope.transaccion);
					$scope.respuestaAct.$promise.then(function(data) {
						console.log(data);
						$scope.deferred.resolve(data);
					});

					$scope.loopPromises.push($scope.deferred.promise);

		    	}
		    	
		    	$scope.cantPromesasTerminadas = $scope.cantPromesasTerminadas + 1;
		    	aclaracionItem.cantidad = "";
		    	aclaracionItem.checked = false;	    	    		
		    	$scope.validaPromesas();
	    	}

	    	
	    });
	    
	    
	    
//	    if(loopPromises.length > 0 && $scope.cantPromesasTerminadas === $scope.listaSeleccionadas.length){
//			$q.all(loopPromises).then(function () {
//			//Cambiar al aceptar
//			swal({
//				  title: 'Operación Exitosa',
//				  text: "Actualización realizada exitosamente",
//				  type: 'success',
//				  confirmButtonColor: '#3085d6',
//				  cancelButtonColor: '#d33',
//				  confirmButtonText: 'Continuar'
//				}).then(function () {
//					if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
//						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
//					}
//				})
//			});
//		}

	};


	
	$scope.validaPromesas = function(){
		
	    if($scope.loopPromises.length > 0 && $scope.cantPromesasTerminadas === $scope.listaSeleccionadas.length){
			$q.all($scope.loopPromises).then(function () {
			//Cambiar al aceptar
			swal({
				  title: 'Operación Exitosa',
				  text: "Actualización realizada exitosamente",
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Continuar'
				}).then(function () {
					if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
						$scope.loopPromises = [];
						$scope.cantPromesasTerminadas = 0;
					}
				})
			});
		}		
		
	}
	
	

	$scope.aceptaCar = function(div){

		if($scope.listaSeleccionadas.length == 0){
			 swal('Alerta',"Seleccione algún registro",'error');
			 return false;
		}


		//var deferred = $q.defer();

	    //var loopPromises = [];
	    angular.forEach($scope.listaSeleccionadas, function (aclaracionItem) {

			
	    	
	    	if(aclaracionItem.validasupervisor){
	    	
	    		var modalInstance = $uibModal.open({
	    			templateUrl: 'views/modal/modalloginsupervisor.html',
	    			controller: 'ModalLoginSupervisorController',
	    			appendTo: angular.element($document[0].querySelector('#'+ div)),
	    			size: 'sm'
	    		  });
	    		  modalInstance.result.then(function (respuesta) {
	    			  if(respuesta){
	  			    	if($scope.listaSeleccionadas.length == 0){
							 swal('Alerta',"Seleccione algún registro",'error');
							 return false;
						}
			
						if(aclaracionItem.cantidad === undefined){
				    		swal('Alerta',"Ingrese las cantidades",'error');
				    		return false;
				    	}
				    	for(i = 0; i < aclaracionItem.cantidad; i++){
			
							$scope.transaccion = {
								codServicio : 'CAR',
								rutAfectado : aclaracionItem.rut,
								corrNombre  : aclaracionItem.correlativo
							}
			
			
							$scope.respuestaAct = TransaccionResource.agregarservicio($scope.transaccion);
							$scope.respuestaAct.$promise.then(function(data) {
								console.log(data);
								$scope.deferred.resolve(data);
							});
			
							$scope.loopPromises.push($scope.deferred.promise);
			
				    	}
				    	$scope.cantPromesasTerminadas = $scope.cantPromesasTerminadas + 1;
				    	aclaracionItem.cantidad = "";
				    	aclaracionItem.checked = false;
				    	$scope.validaPromesas();

	    			  }
	    		  });	    		
	    	}
	    	else{
			    	if($scope.listaSeleccionadas.length == 0){
						 swal('Alerta',"Seleccione algún registro",'error');
						 return false;
					}
		
					if(aclaracionItem.cantidad === undefined){
			    		swal('Alerta',"Ingrese las cantidades",'error');
			    		return false;
			    	}
			    	for(i = 0; i < aclaracionItem.cantidad; i++){
		
						$scope.transaccion = {
							codServicio : 'CAR',
							rutAfectado : aclaracionItem.rut,
							corrNombre  : aclaracionItem.correlativo
						}
		
		
						$scope.respuestaAct = TransaccionResource.agregarservicio($scope.transaccion);
						$scope.respuestaAct.$promise.then(function(data) {
							console.log(data);
							$scope.deferred.resolve(data);
						});
		
						$scope.loopPromises.push($scope.deferred.promise);
		
			    	}
			    	$scope.cantPromesasTerminadas = $scope.cantPromesasTerminadas + 1;
			    	aclaracionItem.cantidad = "";
			    	aclaracionItem.checked = false;
			    	$scope.validaPromesas();
	    	}

	    });

//	    if(loopPromises.length > 0){
//			$q.all(loopPromises).then(function () {
//			//Cambiar al aceptar
//			swal({
//				  title: 'Operación Exitosa',
//				  text: "Actualización realizada exitosamente",
//				  type: 'success',
//				  confirmButtonColor: '#3085d6',
//				  cancelButtonColor: '#d33',
//				  confirmButtonText: 'Continuar'
//				}).then(function () {
//					if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
//						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
//					}
//				})
//			});
//		}
	};


    $scope.updateSelection = function(position, entities) {
	    angular.forEach(entities, function(acl, index) {
	      $scope.selectedAclaraciones()
	    });
    };

	$scope.selectedAclaraciones = function () {
	   $scope.listaSeleccionadas = $filter('filter')($scope.listaAclaraciones, {checked: true});
	   $scope.listaDeSeleccionadas = $filter('filter')($scope.listaAclaraciones, {checked: false});
	   angular.forEach($scope.listaSeleccionadas, function(acl, index) {
	      acl.cantidad = 1;
	    });

	   angular.forEach($scope.listaDeSeleccionadas, function(acl, index) {
	      acl.cantidad = "";
	    });
	};


	$scope.guardarServicio = function(){
		$scope.transaccion = {
			codServicio : 'ICT',
			rutAfectado : $scope.afectado.rut,
			corrNombre: $scope.afectado.corrNombre
		}
		var successCallback = function(data,responseHeaders){

		};
	    TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
	};


	$scope.cargaCar = function(){
		console.log($scope.listaAclaraciones);
		$scope.valor = 0;
		angular.forEach($scope.listaAclaraciones, function(acl, index) {
		  acl.checked = false;
		  acl.cantidad = "";
	    });
		
		angular.element("#informeconsolidadotramite1").modal('hide');
		angular.element("#informeconsolidadotramite2").modal();
	};


	$scope.actualizaCarro = function(){
		$scope.listarServicioTramitante();
		$scope.ruts = [];
		$scope.validasupervisor = false;
		angular.element("#informeconsolidadotramite2").modal('hide');
	};


	$scope.authSupervisor = function(metodo, div) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#'+ div)),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){

				if(metodo === "aceptaICL"){
					$scope.aceptaICL();
				}
				else if(metodo === "aceptaCar"){
					$scope.aceptaCar();
				}
			  }
		  });
	};

	$scope.esRutJuridicoFirmante = function (rut) {
		var rutNumerico	= rut==undefined ?  0 :  parseInt( rut.slice(0, rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};
	
	
});
