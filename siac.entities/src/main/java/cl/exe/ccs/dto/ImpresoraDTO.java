package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImpresoraDTO extends ResponseDTO implements Serializable{
	
	
	private Integer codCCosto;
	private String idImpresora;
	private String modeloImpresora;
	
	public Integer getCodCCosto() {
		return codCCosto;
	}
	public void setCodCCosto(Integer codCCosto) {
		this.codCCosto = codCCosto;
	}
	public String getIdImpresora() {
		return idImpresora;
	}
	public void setIdImpresora(String idImpresora) {
		this.idImpresora = idImpresora;
	}
	public String getModeloImpresora() {
		return modeloImpresora;
	}
	public void setModeloImpresora(String modeloImpresora) {
		this.modeloImpresora = modeloImpresora;
	}
	
	
    
}
