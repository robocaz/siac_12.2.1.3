package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PBC_Control database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PBC_Control.findAll", query="SELECT t FROM Tbl_PBC_Control t")
public class Tbl_PBC_Control implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Cantidad")
	private short fld_Cantidad;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrControlPBC_Id")
	private BigDecimal fld_CorrControlPBC_Id;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_PBC_Control() {
	}

	public short getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(short fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrControlPBC_Id() {
		return this.fld_CorrControlPBC_Id;
	}

	public void setFld_CorrControlPBC_Id(BigDecimal fld_CorrControlPBC_Id) {
		this.fld_CorrControlPBC_Id = fld_CorrControlPBC_Id;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}