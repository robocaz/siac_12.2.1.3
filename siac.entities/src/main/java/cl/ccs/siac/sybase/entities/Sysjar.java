package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysjars database table.
 * 
 */
@Entity
@Table(name="sysjars")
@NamedQuery(name="Sysjar.findAll", query="SELECT s FROM Sysjar s")
public class Sysjar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private byte[] jbinary;

	private int jid;

	private String jname;

	private short jstatus;

	public Sysjar() {
	}

	public byte[] getJbinary() {
		return this.jbinary;
	}

	public void setJbinary(byte[] jbinary) {
		this.jbinary = jbinary;
	}

	public int getJid() {
		return this.jid;
	}

	public void setJid(int jid) {
		this.jid = jid;
	}

	public String getJname() {
		return this.jname;
	}

	public void setJname(String jname) {
		this.jname = jname;
	}

	public short getJstatus() {
		return this.jstatus;
	}

	public void setJstatus(short jstatus) {
		this.jstatus = jstatus;
	}

}