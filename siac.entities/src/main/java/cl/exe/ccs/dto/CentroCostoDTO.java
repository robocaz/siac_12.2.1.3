package cl.exe.ccs.dto;

import java.io.Serializable;

public class CentroCostoDTO extends TipoDTO implements Serializable{
	
	private String codigo;
	private String descripcion;
	private Integer clasificacion;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getClasificacion() {
		return clasificacion;
	}
	public void setClasificacion(Integer clasificacion) {
		this.clasificacion = clasificacion;
	}
}
