package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdCtrExtRC database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdCtrExtRC.findAll", query="SELECT t FROM Tb_CeIdCtrExtRC t")
public class Tb_CeIdCtrExtRC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FecExtraccion")
	private Timestamp fld_FecExtraccion;

	@Column(name="Fld_FecMenosDias")
	private Timestamp fld_FecMenosDias;

	public Tb_CeIdCtrExtRC() {
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public Timestamp getFld_FecExtraccion() {
		return this.fld_FecExtraccion;
	}

	public void setFld_FecExtraccion(Timestamp fld_FecExtraccion) {
		this.fld_FecExtraccion = fld_FecExtraccion;
	}

	public Timestamp getFld_FecMenosDias() {
		return this.fld_FecMenosDias;
	}

	public void setFld_FecMenosDias(Timestamp fld_FecMenosDias) {
		this.fld_FecMenosDias = fld_FecMenosDias;
	}

}