package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ABIVendedor database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIVendedor.findAll", query="SELECT t FROM Tbl_ABIVendedor t")
public class Tbl_ABIVendedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Activo")
	private boolean fld_Activo;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CorrVendedor")
	private BigDecimal fld_CorrVendedor;

	@Column(name="Fld_Nombre_Vendedor")
	private String fld_Nombre_Vendedor;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_ABIVendedor() {
	}

	public boolean getFld_Activo() {
		return this.fld_Activo;
	}

	public void setFld_Activo(boolean fld_Activo) {
		this.fld_Activo = fld_Activo;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public BigDecimal getFld_CorrVendedor() {
		return this.fld_CorrVendedor;
	}

	public void setFld_CorrVendedor(BigDecimal fld_CorrVendedor) {
		this.fld_CorrVendedor = fld_CorrVendedor;
	}

	public String getFld_Nombre_Vendedor() {
		return this.fld_Nombre_Vendedor;
	}

	public void setFld_Nombre_Vendedor(String fld_Nombre_Vendedor) {
		this.fld_Nombre_Vendedor = fld_Nombre_Vendedor;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}