package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_RutUnicos database table.
 * 
 */
@Entity
@Table(name="Ley_RutUnicos")
@NamedQuery(name="Ley_RutUnico.findAll", query="SELECT l FROM Ley_RutUnico l")
public class Ley_RutUnico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Correlativo")
	private BigDecimal correlativo;

	@Column(name="Flag")
	private byte flag;

	@Column(name="Rut")
	private String rut;

	public Ley_RutUnico() {
	}

	public BigDecimal getCorrelativo() {
		return this.correlativo;
	}

	public void setCorrelativo(BigDecimal correlativo) {
		this.correlativo = correlativo;
	}

	public byte getFlag() {
		return this.flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}