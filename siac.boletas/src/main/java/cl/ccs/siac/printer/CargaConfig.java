package cl.ccs.siac.printer;

import cl.exe.ccs.dto.InicioConfigDTO;

public interface CargaConfig {

	public InicioConfigDTO cargaConfiguracion();
}
