angular.module('ccs').controller('CajaController', function($scope, CajaResource, $rootScope, $location) {

    $scope.seleccionado = "listado";

    $scope.aceptarApertura = function(){
    	$rootScope.tramitante = {};
    	 $rootScope.$emit('limpiaTramitante');
    	if($scope.seleccionado == ""){
    		swal('Alerta','Seleccione una opción','error');
    		return false;
    	}
        if($scope.seleccionado == "nuevo"){
            var successCallback = function(data){
                $rootScope.usuario.caja = data.corrCaja;
                $scope.tramitante = {};
                angular.element('#modalaperturacaja').modal("hide");
                $location.path("/atencion");
                $scope.cantidadesabiertasActualiza();
            };
            CajaResource.aperturacaja( successCallback, $scope.errorCallback);
        }else{
            angular.element('#modalaperturacaja').modal("hide");
            angular.element('#listacajas').modal();
        }
       
    }
    
    $scope.cantidadesabiertasActualiza = function() {
        var successCallback = function(data){

                $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
                $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
        };
        CajaResource.cantidadabiertas( successCallback, $scope.errorCallback);
      
    };
    
    $scope.cerrar = function(){
    	
    	$rootScope.tramitante = {};
    	$scope.tramitante = {};
    	angular.element('#modalaperturacaja').modal("hide");
    	$location.path("/inicio");
    	$scope.seleccionado = "";
    	
    }

    $("#modalaperturacaja").on('shown.bs.modal', function () {
    	$scope.seleccionado = "";
		//$("#radio_apertura_2").focus();
	});
	angular.element("#modalaperturacaja").on('hide.bs.modal', function () {
		$scope.seleccionado = "";
    });

});
