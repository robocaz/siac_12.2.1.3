angular.module('ccs').factory('BoletaResource', function ($resource) {
    var resource = $resource('rest/boleta',{
        nocache : new Date().getTime()
    },{
        'obtenerBoleta': { method: 'POST', isArray: false, url: 'rest/boleta/obtenerBoleta' },
        'obtenerBoletaTransaccion': { method: 'POST', isArray: false, url: 'rest/boleta/obtenerBoletaTransaccion' },
        'obtenerBoletasFolioCero': { method: 'POST', isArray: false, url: 'rest/boleta/obtenerBoletasFolioCero' },
        'consultaBoleta': {  method: 'POST', isArray: false, url: 'rest/boleta/consulta', nocache : new Date().getTime() },
        'inicializaBoleta': {  method: 'POST', isArray: false, url: 'rest/boleta/iniciar' },
        'anularBoleta' : {  method: 'POST', isArray: false, url: 'rest/boleta/anular' }
    });
    return resource;
});
