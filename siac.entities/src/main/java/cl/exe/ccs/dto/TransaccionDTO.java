package cl.exe.ccs.dto;

import java.io.Serializable;

public class TransaccionDTO extends ResponseDTO implements Serializable{
	
	private Long corrCaja;
	private String codServicio;
	private String rutTramitante;
	private String rutAfectado;
	private Long corrProtesto;
	private Long corrNombre; 
	private Long corrAclaracion;
	
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public Long getCorrProtesto() {
		return corrProtesto;
	}
	public void setCorrProtesto(Long corrProtesto) {
		this.corrProtesto = corrProtesto;
	}
	public Long getCorrAclaracion() {
		return corrAclaracion;
	}
	public void setCorrAclaracion(Long corrAclaracion) {
		this.corrAclaracion = corrAclaracion;
	}
	public Long getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Long corrNombre) {
		this.corrNombre = corrNombre;
	}
	
}
