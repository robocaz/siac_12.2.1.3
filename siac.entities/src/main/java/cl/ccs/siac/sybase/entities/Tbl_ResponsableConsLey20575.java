package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ResponsableConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ResponsableConsLey20575.findAll", query="SELECT t FROM Tbl_ResponsableConsLey20575 t")
public class Tbl_ResponsableConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodResponsable")
	private short fld_CodResponsable;

	@Column(name="Fld_GlosaResponsable")
	private String fld_GlosaResponsable;

	@Column(name="Fld_RutResponsable")
	private String fld_RutResponsable;

	public Tbl_ResponsableConsLey20575() {
	}

	public short getFld_CodResponsable() {
		return this.fld_CodResponsable;
	}

	public void setFld_CodResponsable(short fld_CodResponsable) {
		this.fld_CodResponsable = fld_CodResponsable;
	}

	public String getFld_GlosaResponsable() {
		return this.fld_GlosaResponsable;
	}

	public void setFld_GlosaResponsable(String fld_GlosaResponsable) {
		this.fld_GlosaResponsable = fld_GlosaResponsable;
	}

	public String getFld_RutResponsable() {
		return this.fld_RutResponsable;
	}

	public void setFld_RutResponsable(String fld_RutResponsable) {
		this.fld_RutResponsable = fld_RutResponsable;
	}

}