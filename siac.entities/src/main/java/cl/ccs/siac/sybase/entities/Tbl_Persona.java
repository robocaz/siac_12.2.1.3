package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Personas database table.
 * 
 */
@Entity
@Table(name="Tbl_Personas")
@NamedQuery(name="Tbl_Persona.findAll", query="SELECT t FROM Tbl_Persona t")
public class Tbl_Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalidadJuridica")
	private String fld_CalidadJuridica;

	@Column(name="Fld_CorrNombre_Id")
	private BigDecimal fld_CorrNombre_Id;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_Fuente")
	private byte fld_Fuente;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoNombre")
	private boolean fld_TipoNombre;

	public Tbl_Persona() {
	}

	public String getFld_CalidadJuridica() {
		return this.fld_CalidadJuridica;
	}

	public void setFld_CalidadJuridica(String fld_CalidadJuridica) {
		this.fld_CalidadJuridica = fld_CalidadJuridica;
	}

	public BigDecimal getFld_CorrNombre_Id() {
		return this.fld_CorrNombre_Id;
	}

	public void setFld_CorrNombre_Id(BigDecimal fld_CorrNombre_Id) {
		this.fld_CorrNombre_Id = fld_CorrNombre_Id;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public byte getFld_Fuente() {
		return this.fld_Fuente;
	}

	public void setFld_Fuente(byte fld_Fuente) {
		this.fld_Fuente = fld_Fuente;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public boolean getFld_TipoNombre() {
		return this.fld_TipoNombre;
	}

	public void setFld_TipoNombre(boolean fld_TipoNombre) {
		this.fld_TipoNombre = fld_TipoNombre;
	}

}