package cl.ccs.siac.exceptions;

public class ReportException extends Exception {

	private static final long serialVersionUID = 4866063202133217219L;

	/**
	 * Constructor.
	 */
	public ReportException() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 */
	public ReportException(final String message) {
		super(message);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportException(final Throwable throwable) {
		super(throwable);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportException(final String message, final Throwable throwable) {
		super(message, throwable);
	}

}
