package cl.ccs.siac.pos.printer;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Scanner;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class TwoWaySerialComm {

    
    public static void main(String[] args) throws PortInUseException, IOException, UnsupportedCommOperationException {
        Enumeration comport= CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier myCPI = null;
        Scanner mySC;
        PrintStream myPS;
        while(comport.hasMoreElements())
        {
            myCPI = (CommPortIdentifier) comport.nextElement();
            if(myCPI.getName().equals("COM1"))
                break;
        }
        CommPort  puerto= myCPI.open("COM1", 2000);
        SerialPort mySP= (SerialPort)puerto;
        mySP.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1 ,SerialPort.PARITY_NONE );

        myPS = new PrintStream (mySP.getOutputStream());


		String numeroBoleta = String.format("%07d", 78453);
		String corrCaja = String.format("%09d", 568222);
		String centroCosto = String.format("%04d", 2530);
		String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
		StringBuffer stb = new StringBuffer();
		stb.append("\n\n\n\n\n\n\n");
		stb.append("\n");
		stb.append("                                           #" + numeroBoleta);
		stb.append("   " + corrCaja + " " + centroCosto + "  " + date);

      
        myPS.println(stb.toString());
        myPS.println("\n");
		//iteracion
		for (int i = 0; i < 9; i++) {
			 myPS.println("  " + "1 " + "   " +"descripcion qw" + "       "+ "4000");
		}
		//p.escribir("\n");
		myPS.println("                 " + "Total $36000");
		for (int i = 1; i <= 10; i++) {
			 myPS.println("");
		}
        
        
        myPS.close();
        myPS.flush();
        puerto.close();
    }
}