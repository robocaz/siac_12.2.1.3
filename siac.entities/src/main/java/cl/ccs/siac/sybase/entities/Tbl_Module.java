package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Module database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Module.findAll", query="SELECT t FROM Tbl_Module t")
public class Tbl_Module implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	@Column(name="Fld_ModuleDescription")
	private String fld_ModuleDescription;

	@Column(name="Fld_ModuleName")
	private String fld_ModuleName;

	@Column(name="Fld_ModuleTimeStamp")
	private byte[] fld_ModuleTimeStamp;

	public Tbl_Module() {
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

	public String getFld_ModuleDescription() {
		return this.fld_ModuleDescription;
	}

	public void setFld_ModuleDescription(String fld_ModuleDescription) {
		this.fld_ModuleDescription = fld_ModuleDescription;
	}

	public String getFld_ModuleName() {
		return this.fld_ModuleName;
	}

	public void setFld_ModuleName(String fld_ModuleName) {
		this.fld_ModuleName = fld_ModuleName;
	}

	public byte[] getFld_ModuleTimeStamp() {
		return this.fld_ModuleTimeStamp;
	}

	public void setFld_ModuleTimeStamp(byte[] fld_ModuleTimeStamp) {
		this.fld_ModuleTimeStamp = fld_ModuleTimeStamp;
	}

}