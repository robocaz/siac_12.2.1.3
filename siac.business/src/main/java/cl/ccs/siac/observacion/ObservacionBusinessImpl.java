package cl.ccs.siac.observacion;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.ObservacionDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ObservacionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("observacionBusiness")
public class ObservacionBusinessImpl implements ObservacionBusiness {
	
	@Inject
	private ObservacionDao observacionDao;
	
	@Inject
	private Utils utils;
	
	@Override
	public ResponseListDTO<ObservacionDTO> listar(String rutTramitante) {
		String rutParse = "";
		if(rutTramitante != null && !rutTramitante.equals("")){

			rutParse = utils.formatearRUT(rutTramitante);
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			rutTramitante = rutParse;
		}
		return observacionDao.listar(rutTramitante);
	}

	@Override
	public ResponseDTO crear(ObservacionDTO observacionDTO) {
		
		String rutParse = "";
		if(observacionDTO.getRutTramitante() != null && !observacionDTO.getRutTramitante().equals("")){

			rutParse = utils.formatearRUT(observacionDTO.getRutTramitante());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			observacionDTO.setRutTramitante(rutParse);
		}
		return observacionDao.crear(observacionDTO);
	}
	
}
