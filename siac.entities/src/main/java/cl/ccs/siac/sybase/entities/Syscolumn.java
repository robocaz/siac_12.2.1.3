package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the syscolumns database table.
 * 
 */
@Entity
@Table(name="syscolumns")
@NamedQuery(name="Syscolumn.findAll", query="SELECT s FROM Syscolumn s")
public class Syscolumn implements Serializable {
	private static final long serialVersionUID = 1L;

	private int accessrule;

	private int cdefault;

	private short colid;

	private int computedcol;

	private int domain;

	private Timestamp encrdate;

	private String encrkeydb;

	private int encrkeyid;

	private int encrlen;

	private int encrtype;

	private int id;

	private short inrowlen;

	private int length;

	@Column(name="lobcomp_lvl")
	private byte lobcompLvl;

	private String name;

	private short number;

	private short offset;

	private byte prec;

	private String printfmt;

	@Column(name="remote_name")
	private String remoteName;

	@Column(name="remote_type")
	private int remoteType;

	private byte scale;

	private byte status;

	private int status2;

	private short status3;

	private byte type;

	private short usertype;

	private int xdbid;

	private int xstatus;

	private int xtype;

	public Syscolumn() {
	}

	public int getAccessrule() {
		return this.accessrule;
	}

	public void setAccessrule(int accessrule) {
		this.accessrule = accessrule;
	}

	public int getCdefault() {
		return this.cdefault;
	}

	public void setCdefault(int cdefault) {
		this.cdefault = cdefault;
	}

	public short getColid() {
		return this.colid;
	}

	public void setColid(short colid) {
		this.colid = colid;
	}

	public int getComputedcol() {
		return this.computedcol;
	}

	public void setComputedcol(int computedcol) {
		this.computedcol = computedcol;
	}

	public int getDomain() {
		return this.domain;
	}

	public void setDomain(int domain) {
		this.domain = domain;
	}

	public Timestamp getEncrdate() {
		return this.encrdate;
	}

	public void setEncrdate(Timestamp encrdate) {
		this.encrdate = encrdate;
	}

	public String getEncrkeydb() {
		return this.encrkeydb;
	}

	public void setEncrkeydb(String encrkeydb) {
		this.encrkeydb = encrkeydb;
	}

	public int getEncrkeyid() {
		return this.encrkeyid;
	}

	public void setEncrkeyid(int encrkeyid) {
		this.encrkeyid = encrkeyid;
	}

	public int getEncrlen() {
		return this.encrlen;
	}

	public void setEncrlen(int encrlen) {
		this.encrlen = encrlen;
	}

	public int getEncrtype() {
		return this.encrtype;
	}

	public void setEncrtype(int encrtype) {
		this.encrtype = encrtype;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getInrowlen() {
		return this.inrowlen;
	}

	public void setInrowlen(short inrowlen) {
		this.inrowlen = inrowlen;
	}

	public int getLength() {
		return this.length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public byte getLobcompLvl() {
		return this.lobcompLvl;
	}

	public void setLobcompLvl(byte lobcompLvl) {
		this.lobcompLvl = lobcompLvl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getNumber() {
		return this.number;
	}

	public void setNumber(short number) {
		this.number = number;
	}

	public short getOffset() {
		return this.offset;
	}

	public void setOffset(short offset) {
		this.offset = offset;
	}

	public byte getPrec() {
		return this.prec;
	}

	public void setPrec(byte prec) {
		this.prec = prec;
	}

	public String getPrintfmt() {
		return this.printfmt;
	}

	public void setPrintfmt(String printfmt) {
		this.printfmt = printfmt;
	}

	public String getRemoteName() {
		return this.remoteName;
	}

	public void setRemoteName(String remoteName) {
		this.remoteName = remoteName;
	}

	public int getRemoteType() {
		return this.remoteType;
	}

	public void setRemoteType(int remoteType) {
		this.remoteType = remoteType;
	}

	public byte getScale() {
		return this.scale;
	}

	public void setScale(byte scale) {
		this.scale = scale;
	}

	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public int getStatus2() {
		return this.status2;
	}

	public void setStatus2(int status2) {
		this.status2 = status2;
	}

	public short getStatus3() {
		return this.status3;
	}

	public void setStatus3(short status3) {
		this.status3 = status3;
	}

	public byte getType() {
		return this.type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public short getUsertype() {
		return this.usertype;
	}

	public void setUsertype(short usertype) {
		this.usertype = usertype;
	}

	public int getXdbid() {
		return this.xdbid;
	}

	public void setXdbid(int xdbid) {
		this.xdbid = xdbid;
	}

	public int getXstatus() {
		return this.xstatus;
	}

	public void setXstatus(int xstatus) {
		this.xstatus = xstatus;
	}

	public int getXtype() {
		return this.xtype;
	}

	public void setXtype(int xtype) {
		this.xtype = xtype;
	}

}