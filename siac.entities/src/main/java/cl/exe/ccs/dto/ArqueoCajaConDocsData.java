package cl.exe.ccs.dto;

import java.io.Serializable;

public class ArqueoCajaConDocsData implements Serializable{

	private String numero;
	private Integer mntoTrx;
	private String tipoPago;
	private String glosa;

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Integer getMntoTrx() {
		return mntoTrx;
	}
	public void setMntoTrx(Integer mntoTrx) {
		this.mntoTrx = mntoTrx;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaConDocsData [numero=");
		builder.append(numero);
		builder.append(", mntoTrx=");
		builder.append(mntoTrx);
		builder.append(", tipoPago=");
		builder.append(tipoPago);
		builder.append(", glosa=");
		builder.append(glosa);
		builder.append("]");
		return builder.toString();
	}

}
