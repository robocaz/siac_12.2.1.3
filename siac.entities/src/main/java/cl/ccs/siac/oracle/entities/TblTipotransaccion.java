package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_TIPOTRANSACCION database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOTRANSACCION")
@NamedQuery(name="TblTipotransaccion.findAll", query="SELECT t FROM TblTipotransaccion t")
public class TblTipotransaccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOTRANSACCION_FLDCODTRANSACCION_GENERATOR", sequenceName="FLD_CODTRANSACCION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOTRANSACCION_FLDCODTRANSACCION_GENERATOR")
	@Column(name="FLD_CODTRANSACCION")
	private long fldCodtransaccion;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATRANSACCION")
	private String fldGlosatransaccion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblLogaclaracion
	@OneToMany(mappedBy="tblTipotransaccion")
	private List<TblLogaclaracion> tblLogaclaracions;

	//bi-directional many-to-one association to TblLogconsulta
	@OneToMany(mappedBy="tblTipotransaccion")
	private List<TblLogconsulta> tblLogconsultas;

	public TblTipotransaccion() {
	}

	public long getFldCodtransaccion() {
		return this.fldCodtransaccion;
	}

	public void setFldCodtransaccion(long fldCodtransaccion) {
		this.fldCodtransaccion = fldCodtransaccion;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatransaccion() {
		return this.fldGlosatransaccion;
	}

	public void setFldGlosatransaccion(String fldGlosatransaccion) {
		this.fldGlosatransaccion = fldGlosatransaccion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblLogaclaracion> getTblLogaclaracions() {
		return this.tblLogaclaracions;
	}

	public void setTblLogaclaracions(List<TblLogaclaracion> tblLogaclaracions) {
		this.tblLogaclaracions = tblLogaclaracions;
	}

	public TblLogaclaracion addTblLogaclaracion(TblLogaclaracion tblLogaclaracion) {
		getTblLogaclaracions().add(tblLogaclaracion);
		tblLogaclaracion.setTblTipotransaccion(this);

		return tblLogaclaracion;
	}

	public TblLogaclaracion removeTblLogaclaracion(TblLogaclaracion tblLogaclaracion) {
		getTblLogaclaracions().remove(tblLogaclaracion);
		tblLogaclaracion.setTblTipotransaccion(null);

		return tblLogaclaracion;
	}

	public List<TblLogconsulta> getTblLogconsultas() {
		return this.tblLogconsultas;
	}

	public void setTblLogconsultas(List<TblLogconsulta> tblLogconsultas) {
		this.tblLogconsultas = tblLogconsultas;
	}

	public TblLogconsulta addTblLogconsulta(TblLogconsulta tblLogconsulta) {
		getTblLogconsultas().add(tblLogconsulta);
		tblLogconsulta.setTblTipotransaccion(this);

		return tblLogconsulta;
	}

	public TblLogconsulta removeTblLogconsulta(TblLogconsulta tblLogconsulta) {
		getTblLogconsultas().remove(tblLogconsulta);
		tblLogconsulta.setTblTipotransaccion(null);

		return tblLogconsulta;
	}

}