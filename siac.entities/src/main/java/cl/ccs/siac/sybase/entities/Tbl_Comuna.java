package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Comunas database table.
 * 
 */
@Entity
@Table(name="Tbl_Comunas")
@NamedQuery(name="Tbl_Comuna.findAll", query="SELECT t FROM Tbl_Comuna t")
public class Tbl_Comuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Ciudad")
	private String fld_Ciudad;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_GlosaComuna")
	private String fld_GlosaComuna;

	@Column(name="Fld_Provincia")
	private String fld_Provincia;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_Comuna() {
	}

	public String getFld_Ciudad() {
		return this.fld_Ciudad;
	}

	public void setFld_Ciudad(String fld_Ciudad) {
		this.fld_Ciudad = fld_Ciudad;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_GlosaComuna() {
		return this.fld_GlosaComuna;
	}

	public void setFld_GlosaComuna(String fld_GlosaComuna) {
		this.fld_GlosaComuna = fld_GlosaComuna;
	}

	public String getFld_Provincia() {
		return this.fld_Provincia;
	}

	public void setFld_Provincia(String fld_Provincia) {
		this.fld_Provincia = fld_Provincia;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}