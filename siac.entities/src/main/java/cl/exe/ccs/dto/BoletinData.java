package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class BoletinData implements Serializable, Comparable<BoletinData> {

	private static final long serialVersionUID = -4523202866074832919L;

	private String tipoDeudaLaboral;
	private String glosaTipoAcreedor;
	private String nombreInstitucion;
	private Integer nroBolLaboral;
	private Integer pagBolLaboral;
	private String fecBolLaboral;
	private String motivoInfraccion;
	private String rut;
	private String nombre;
	private Integer periodo;
	private BigDecimal montoLaboral;
	private BigDecimal montoLaboralUTM;
	private Integer codRegionInspeccion;
	private Integer anoResolMulta;
	private Integer nroResolMulta;
	private String tipoMulta;
	private Integer meses;
	private Integer nroCotizaciones;
	private BigDecimal montoAdeudado;

	@Override
	public int compareTo(BoletinData o) {
		final int val = nombreInstitucion.compareTo(o.nombreInstitucion);
		if ( val < 0 ) {
			return -1;
		}
		if ( val > 0 ) {
			return 1;
		}
		if ( periodo < o.periodo ) {
			return -1;
		}
		if ( periodo > o.periodo ) {
			return 1;
		}
		return 0;
	}

	public String getTipoDeudaLaboral() {
		return tipoDeudaLaboral;
	}

	public void setTipoDeudaLaboral(String tipoDeudaLaboral) {
		this.tipoDeudaLaboral = tipoDeudaLaboral;
	}

	public String getGlosaTipoAcreedor() {
		return glosaTipoAcreedor;
	}

	public void setGlosaTipoAcreedor(String glosaTipoAcreedor) {
		this.glosaTipoAcreedor = glosaTipoAcreedor;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public Integer getNroBolLaboral() {
		return nroBolLaboral;
	}

	public void setNroBolLaboral(Integer nroBolLaboral) {
		this.nroBolLaboral = nroBolLaboral;
	}

	public Integer getPagBolLaboral() {
		return pagBolLaboral;
	}

	public void setPagBolLaboral(Integer pagBolLaboral) {
		this.pagBolLaboral = pagBolLaboral;
	}

	public String getFecBolLaboral() {
		return fecBolLaboral;
	}

	public void setFecBolLaboral(String fecBolLaboral) {
		this.fecBolLaboral = fecBolLaboral;
	}

	public String getMotivoInfraccion() {
		return motivoInfraccion;
	}

	public void setMotivoInfraccion(String motivoInfraccion) {
		this.motivoInfraccion = motivoInfraccion;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public BigDecimal getMontoLaboral() {
		return montoLaboral;
	}

	public void setMontoLaboral(BigDecimal montoLaboral) {
		this.montoLaboral = montoLaboral;
	}

	public BigDecimal getMontoLaboralUTM() {
		return montoLaboralUTM;
	}

	public void setMontoLaboralUTM(BigDecimal montoLaboralUTM) {
		this.montoLaboralUTM = montoLaboralUTM;
	}

	public Integer getCodRegionInspeccion() {
		return codRegionInspeccion;
	}

	public void setCodRegionInspeccion(Integer codRegionInspeccion) {
		this.codRegionInspeccion = codRegionInspeccion;
	}

	public Integer getAnoResolMulta() {
		return anoResolMulta;
	}

	public void setAnoResolMulta(Integer anoResolMulta) {
		this.anoResolMulta = anoResolMulta;
	}

	public Integer getNroResolMulta() {
		return nroResolMulta;
	}

	public void setNroResolMulta(Integer nroResolMulta) {
		this.nroResolMulta = nroResolMulta;
	}

	public String getTipoMulta() {
		return tipoMulta;
	}

	public void setTipoMulta(String tipoMulta) {
		this.tipoMulta = tipoMulta;
	}

	public Integer getMeses() {
		return meses;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public Integer getNroCotizaciones() {
		return nroCotizaciones;
	}

	public void setNroCotizaciones(Integer nroCotizaciones) {
		this.nroCotizaciones = nroCotizaciones;
	}

	public BigDecimal getMontoAdeudado() {
		return montoAdeudado;
	}

	public void setMontoAdeudado(BigDecimal montoAdeudado) {
		this.montoAdeudado = montoAdeudado;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n tipoDeudaLaboral: " + tipoDeudaLaboral);
		str.append(" ,glosaTipoAcreedor: " + glosaTipoAcreedor);
		str.append(" ,nombreInstitucion: " + nombreInstitucion);
		str.append(" ,nroBolLaboral: " + nroBolLaboral);
		str.append(" ,pagBolLaboral: " + pagBolLaboral);
		str.append(" ,fecBolLaboral: " + fecBolLaboral);
		str.append(" ,motivoInfraccion: " + motivoInfraccion);
		str.append(" ,rut: " + rut);
		str.append(" ,nombre: " + nombre);
		str.append(" ,periodo: " + periodo);
		str.append(" ,montoLaboral: " + montoLaboral);
		str.append(" ,montoLaboralUTM: " + montoLaboralUTM);
		str.append(" ,codRegionInspeccion: " + codRegionInspeccion);
		str.append(" ,anoResolMulta: " + anoResolMulta);
		str.append(" ,nroResolMulta: " + nroResolMulta);
		str.append(" ,tipoMulta: " + tipoMulta);
		str.append(" ,meses: " + meses);
		str.append(" ,nroCotizaciones: " + nroCotizaciones);
		str.append(" ,montoAdeudado: " + montoAdeudado);
		return str.toString();
	}

}
