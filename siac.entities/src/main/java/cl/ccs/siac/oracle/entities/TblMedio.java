package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_MEDIOS database table.
 * 
 */
@Entity
@Table(name="TBL_MEDIOS")
@NamedQuery(name="TblMedio.findAll", query="SELECT t FROM TblMedio t")
public class TblMedio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_MEDIOS_FLDCODMEDIOS_GENERATOR", sequenceName="FLD_CODMEDIOS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_MEDIOS_FLDCODMEDIOS_GENERATOR")
	@Column(name="FLD_CODMEDIOS")
	private String fldCodmedios;

	@Column(name="FLD_GLOSAMEDIOS")
	private String fldGlosamedios;

	public TblMedio() {
	}

	public String getFldCodmedios() {
		return this.fldCodmedios;
	}

	public void setFldCodmedios(String fldCodmedios) {
		this.fldCodmedios = fldCodmedios;
	}

	public String getFldGlosamedios() {
		return this.fldGlosamedios;
	}

	public void setFldGlosamedios(String fldGlosamedios) {
		this.fldGlosamedios = fldGlosamedios;
	}

}