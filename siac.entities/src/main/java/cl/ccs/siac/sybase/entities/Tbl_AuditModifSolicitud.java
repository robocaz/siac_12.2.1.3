package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditModifSolicitud database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditModifSolicitud.findAll", query="SELECT t FROM Tbl_AuditModifSolicitud t")
public class Tbl_AuditModifSolicitud implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_AuditModifSolicitudPK id;

	@Column(name="Fld_CodEstado")
	private String fld_CodEstado;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	public Tbl_AuditModifSolicitud() {
	}

	public Tbl_AuditModifSolicitudPK getId() {
		return this.id;
	}

	public void setId(Tbl_AuditModifSolicitudPK id) {
		this.id = id;
	}

	public String getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(String fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

}