package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RectifAclEsp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RectifAclEsp.findAll", query="SELECT t FROM Tbl_RectifAclEsp t")
public class Tbl_RectifAclEsp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEstadoSolicAER")
	private short fld_CodEstadoSolicAER;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrAER_Id")
	private BigDecimal fld_CorrAER_Id;

	@Column(name="Fld_CorrDocum")
	private BigDecimal fld_CorrDocum;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_MarcaProceso")
	private boolean fld_MarcaProceso;

	@Column(name="Fld_MarcaRectifCCosto")
	private boolean fld_MarcaRectifCCosto;

	@Column(name="Fld_MarcaRectifMontoProtesto")
	private boolean fld_MarcaRectifMontoProtesto;

	@Column(name="Fld_MarcaVisada")
	private byte fld_MarcaVisada;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	@Column(name="Fld_TomoBoletin")
	private short fld_TomoBoletin;

	public Tbl_RectifAclEsp() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEstadoSolicAER() {
		return this.fld_CodEstadoSolicAER;
	}

	public void setFld_CodEstadoSolicAER(short fld_CodEstadoSolicAER) {
		this.fld_CodEstadoSolicAER = fld_CodEstadoSolicAER;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrAER_Id() {
		return this.fld_CorrAER_Id;
	}

	public void setFld_CorrAER_Id(BigDecimal fld_CorrAER_Id) {
		this.fld_CorrAER_Id = fld_CorrAER_Id;
	}

	public BigDecimal getFld_CorrDocum() {
		return this.fld_CorrDocum;
	}

	public void setFld_CorrDocum(BigDecimal fld_CorrDocum) {
		this.fld_CorrDocum = fld_CorrDocum;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_MarcaProceso() {
		return this.fld_MarcaProceso;
	}

	public void setFld_MarcaProceso(boolean fld_MarcaProceso) {
		this.fld_MarcaProceso = fld_MarcaProceso;
	}

	public boolean getFld_MarcaRectifCCosto() {
		return this.fld_MarcaRectifCCosto;
	}

	public void setFld_MarcaRectifCCosto(boolean fld_MarcaRectifCCosto) {
		this.fld_MarcaRectifCCosto = fld_MarcaRectifCCosto;
	}

	public boolean getFld_MarcaRectifMontoProtesto() {
		return this.fld_MarcaRectifMontoProtesto;
	}

	public void setFld_MarcaRectifMontoProtesto(boolean fld_MarcaRectifMontoProtesto) {
		this.fld_MarcaRectifMontoProtesto = fld_MarcaRectifMontoProtesto;
	}

	public byte getFld_MarcaVisada() {
		return this.fld_MarcaVisada;
	}

	public void setFld_MarcaVisada(byte fld_MarcaVisada) {
		this.fld_MarcaVisada = fld_MarcaVisada;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

	public short getFld_TomoBoletin() {
		return this.fld_TomoBoletin;
	}

	public void setFld_TomoBoletin(short fld_TomoBoletin) {
		this.fld_TomoBoletin = fld_TomoBoletin;
	}

}