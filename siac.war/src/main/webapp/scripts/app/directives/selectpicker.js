angular.module('ccs').directive('selectpicker', ['$parse', function($parse){
    return {
        restrict: 'A',
        priority: 1000,
        link: function (scope, element, attrs) {
    
            scope.$watch(attrs.ngModel, function(n, o){
              element.selectpicker('maxOptions', '3');
              element.selectpicker('val', $parse(n)());
              element.selectpicker('refresh');
            });
         
        }
    };
}]);