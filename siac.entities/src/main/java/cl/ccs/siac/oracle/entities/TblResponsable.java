package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_RESPONSABLE database table.
 * 
 */
@Entity
@Table(name="TBL_RESPONSABLE")
@NamedQuery(name="TblResponsable.findAll", query="SELECT t FROM TblResponsable t")
public class TblResponsable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODRESPONSABLE")
	private BigDecimal fldCodresponsable;

	@Column(name="FLD_GLOSARESPONSABLE")
	private String fldGlosaresponsable;

	@Column(name="FLD_RUTRESPONSABLE")
	private String fldRutresponsable;

	public TblResponsable() {
	}

	public BigDecimal getFldCodresponsable() {
		return this.fldCodresponsable;
	}

	public void setFldCodresponsable(BigDecimal fldCodresponsable) {
		this.fldCodresponsable = fldCodresponsable;
	}

	public String getFldGlosaresponsable() {
		return this.fldGlosaresponsable;
	}

	public void setFldGlosaresponsable(String fldGlosaresponsable) {
		this.fldGlosaresponsable = fldGlosaresponsable;
	}

	public String getFldRutresponsable() {
		return this.fldRutresponsable;
	}

	public void setFldRutresponsable(String fldRutresponsable) {
		this.fldRutresponsable = fldRutresponsable;
	}

}