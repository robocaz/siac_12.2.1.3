angular.module('ccs').controller('ConfirmarCierreCajaSesionController', function($scope, CajaResource, $rootScope, $location, LoginResource) {

    $scope.seleccionado = "listado";

    var cantidadsolicitudes = 0;
    var cantidadexitosos = 0;

    $scope.aceptar = function(){
    	
        angular.element('#confirmarcierrecajasesion').modal("hide");
        angular.element('#listacierrecajasesion').modal();
    }

    $scope.cierretemporal = function(corrCaja){
        var successCallback = function(data){
            cantidadexitosos = cantidadexitosos + 1;
            if(cantidadsolicitudes==cantidadexitosos){
                $scope.cerrarsesion();
            }
        };
        var errorCallback = function(data){
            console.log("ERROR");
        };
        CajaResource.cerrarcajatemporal(corrCaja ,  successCallback, errorCallback);
    }

    $scope.cancelar = function(){
        var successCallback = function(data){
        	$scope.blockBack(0);
            $scope.listaCajasAbiertas = data;
            for(x=0; x<$scope.listaCajasAbiertas.length; x++){
                if($scope.listaCajasAbiertas[x].estado==1){
                    console.log("cantidadsolicitudes: " + cantidadsolicitudes + ", cantidadexitosos: "+cantidadexitosos);
                    cantidadsolicitudes = cantidadsolicitudes + 1;
                    $scope.cierretemporal($scope.listaCajasAbiertas[x].corrCaja);
                }
            }
            if(cantidadsolicitudes==0){
                $scope.cerrarsesion();
            }
        };
        CajaResource.listarcajastodas(successCallback, $scope.errorCallback);
    }

    $scope.cerrarsesion = function(){
        var successCallbackLogOut = function(data){
            $rootScope.usuario = {};
            $rootScope.disableTram = false;
            $('.modal-backdrop').remove();
            $location.url('/login')
        };
        LoginResource.logout(successCallbackLogOut, $scope.errorCallback);
    };
});
