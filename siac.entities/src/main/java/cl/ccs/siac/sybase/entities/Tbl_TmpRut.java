package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TmpRut database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TmpRut.findAll", query="SELECT t FROM Tbl_TmpRut t")
public class Tbl_TmpRut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrValid_Id")
	private BigDecimal fld_CorrValid_Id;

	@Column(name="Fld_FolioProt")
	private int fld_FolioProt;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_TmpRut() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrValid_Id() {
		return this.fld_CorrValid_Id;
	}

	public void setFld_CorrValid_Id(BigDecimal fld_CorrValid_Id) {
		this.fld_CorrValid_Id = fld_CorrValid_Id;
	}

	public int getFld_FolioProt() {
		return this.fld_FolioProt;
	}

	public void setFld_FolioProt(int fld_FolioProt) {
		this.fld_FolioProt = fld_FolioProt;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}