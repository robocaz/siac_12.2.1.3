package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABIContrato database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIContrato.findAll", query="SELECT t FROM Tbl_ABIContrato t")
public class Tbl_ABIContrato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_AvisoBienvenida")
	private byte fld_AvisoBienvenida;

	@Column(name="Fld_AvisoBienvMovil")
	private byte fld_AvisoBienvMovil;

	@Column(name="Fld_AvisoRenovacion")
	private byte fld_AvisoRenovacion;

	@Column(name="Fld_AvisoTermino")
	private byte fld_AvisoTermino;

	@Column(name="Fld_CantidadRenovacion")
	private byte fld_CantidadRenovacion;

	@Column(name="Fld_CantidadRut")
	private short fld_CantidadRut;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrContrato_Id")
	private BigDecimal fld_CorrContrato_Id;

	@Column(name="Fld_CorrNombreContratante")
	private BigDecimal fld_CorrNombreContratante;

	@Column(name="Fld_DireccionCompleta")
	private String fld_DireccionCompleta;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecAvisoBienvMail")
	private Timestamp fld_FecAvisoBienvMail;

	@Column(name="Fld_FecAvisoBienvSMS")
	private Timestamp fld_FecAvisoBienvSMS;

	@Column(name="Fld_FecAvisoRenovacion")
	private Timestamp fld_FecAvisoRenovacion;

	@Column(name="Fld_FecInicioContrato")
	private Timestamp fld_FecInicioContrato;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_FecUltRenovacion")
	private Timestamp fld_FecUltRenovacion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_Flag_Revisado")
	private boolean fld_Flag_Revisado;

	@Column(name="Fld_FlagBoletin")
	private boolean fld_FlagBoletin;

	@Column(name="Fld_FlagListaPublico")
	private boolean fld_FlagListaPublico;

	@Column(name="Fld_FlagMorosidad")
	private boolean fld_FlagMorosidad;

	@Column(name="Fld_NoEnviarMail")
	private boolean fld_NoEnviarMail;

	@Column(name="Fld_Password")
	private String fld_Password;

	@Column(name="Fld_RutContratante")
	private String fld_RutContratante;

	@Column(name="Fld_TelefonoCelular")
	private String fld_TelefonoCelular;

	@Column(name="Fld_TelefonoDomicilio")
	private String fld_TelefonoDomicilio;

	public Tbl_ABIContrato() {
	}

	public byte getFld_AvisoBienvenida() {
		return this.fld_AvisoBienvenida;
	}

	public void setFld_AvisoBienvenida(byte fld_AvisoBienvenida) {
		this.fld_AvisoBienvenida = fld_AvisoBienvenida;
	}

	public byte getFld_AvisoBienvMovil() {
		return this.fld_AvisoBienvMovil;
	}

	public void setFld_AvisoBienvMovil(byte fld_AvisoBienvMovil) {
		this.fld_AvisoBienvMovil = fld_AvisoBienvMovil;
	}

	public byte getFld_AvisoRenovacion() {
		return this.fld_AvisoRenovacion;
	}

	public void setFld_AvisoRenovacion(byte fld_AvisoRenovacion) {
		this.fld_AvisoRenovacion = fld_AvisoRenovacion;
	}

	public byte getFld_AvisoTermino() {
		return this.fld_AvisoTermino;
	}

	public void setFld_AvisoTermino(byte fld_AvisoTermino) {
		this.fld_AvisoTermino = fld_AvisoTermino;
	}

	public byte getFld_CantidadRenovacion() {
		return this.fld_CantidadRenovacion;
	}

	public void setFld_CantidadRenovacion(byte fld_CantidadRenovacion) {
		this.fld_CantidadRenovacion = fld_CantidadRenovacion;
	}

	public short getFld_CantidadRut() {
		return this.fld_CantidadRut;
	}

	public void setFld_CantidadRut(short fld_CantidadRut) {
		this.fld_CantidadRut = fld_CantidadRut;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrContrato_Id() {
		return this.fld_CorrContrato_Id;
	}

	public void setFld_CorrContrato_Id(BigDecimal fld_CorrContrato_Id) {
		this.fld_CorrContrato_Id = fld_CorrContrato_Id;
	}

	public BigDecimal getFld_CorrNombreContratante() {
		return this.fld_CorrNombreContratante;
	}

	public void setFld_CorrNombreContratante(BigDecimal fld_CorrNombreContratante) {
		this.fld_CorrNombreContratante = fld_CorrNombreContratante;
	}

	public String getFld_DireccionCompleta() {
		return this.fld_DireccionCompleta;
	}

	public void setFld_DireccionCompleta(String fld_DireccionCompleta) {
		this.fld_DireccionCompleta = fld_DireccionCompleta;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecAvisoBienvMail() {
		return this.fld_FecAvisoBienvMail;
	}

	public void setFld_FecAvisoBienvMail(Timestamp fld_FecAvisoBienvMail) {
		this.fld_FecAvisoBienvMail = fld_FecAvisoBienvMail;
	}

	public Timestamp getFld_FecAvisoBienvSMS() {
		return this.fld_FecAvisoBienvSMS;
	}

	public void setFld_FecAvisoBienvSMS(Timestamp fld_FecAvisoBienvSMS) {
		this.fld_FecAvisoBienvSMS = fld_FecAvisoBienvSMS;
	}

	public Timestamp getFld_FecAvisoRenovacion() {
		return this.fld_FecAvisoRenovacion;
	}

	public void setFld_FecAvisoRenovacion(Timestamp fld_FecAvisoRenovacion) {
		this.fld_FecAvisoRenovacion = fld_FecAvisoRenovacion;
	}

	public Timestamp getFld_FecInicioContrato() {
		return this.fld_FecInicioContrato;
	}

	public void setFld_FecInicioContrato(Timestamp fld_FecInicioContrato) {
		this.fld_FecInicioContrato = fld_FecInicioContrato;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public Timestamp getFld_FecUltRenovacion() {
		return this.fld_FecUltRenovacion;
	}

	public void setFld_FecUltRenovacion(Timestamp fld_FecUltRenovacion) {
		this.fld_FecUltRenovacion = fld_FecUltRenovacion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_Flag_Revisado() {
		return this.fld_Flag_Revisado;
	}

	public void setFld_Flag_Revisado(boolean fld_Flag_Revisado) {
		this.fld_Flag_Revisado = fld_Flag_Revisado;
	}

	public boolean getFld_FlagBoletin() {
		return this.fld_FlagBoletin;
	}

	public void setFld_FlagBoletin(boolean fld_FlagBoletin) {
		this.fld_FlagBoletin = fld_FlagBoletin;
	}

	public boolean getFld_FlagListaPublico() {
		return this.fld_FlagListaPublico;
	}

	public void setFld_FlagListaPublico(boolean fld_FlagListaPublico) {
		this.fld_FlagListaPublico = fld_FlagListaPublico;
	}

	public boolean getFld_FlagMorosidad() {
		return this.fld_FlagMorosidad;
	}

	public void setFld_FlagMorosidad(boolean fld_FlagMorosidad) {
		this.fld_FlagMorosidad = fld_FlagMorosidad;
	}

	public boolean getFld_NoEnviarMail() {
		return this.fld_NoEnviarMail;
	}

	public void setFld_NoEnviarMail(boolean fld_NoEnviarMail) {
		this.fld_NoEnviarMail = fld_NoEnviarMail;
	}

	public String getFld_Password() {
		return this.fld_Password;
	}

	public void setFld_Password(String fld_Password) {
		this.fld_Password = fld_Password;
	}

	public String getFld_RutContratante() {
		return this.fld_RutContratante;
	}

	public void setFld_RutContratante(String fld_RutContratante) {
		this.fld_RutContratante = fld_RutContratante;
	}

	public String getFld_TelefonoCelular() {
		return this.fld_TelefonoCelular;
	}

	public void setFld_TelefonoCelular(String fld_TelefonoCelular) {
		this.fld_TelefonoCelular = fld_TelefonoCelular;
	}

	public String getFld_TelefonoDomicilio() {
		return this.fld_TelefonoDomicilio;
	}

	public void setFld_TelefonoDomicilio(String fld_TelefonoDomicilio) {
		this.fld_TelefonoDomicilio = fld_TelefonoDomicilio;
	}

}