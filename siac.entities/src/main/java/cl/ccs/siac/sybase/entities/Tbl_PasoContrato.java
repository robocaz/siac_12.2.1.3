package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PasoContrato database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PasoContrato.findAll", query="SELECT t FROM Tbl_PasoContrato t")
public class Tbl_PasoContrato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrNombreTAvisa")
	private BigDecimal fld_CorrNombreTAvisa;

	@Column(name="Fld_CorrNomContratante")
	private BigDecimal fld_CorrNomContratante;

	@Column(name="Fld_DuracionContrato")
	private byte fld_DuracionContrato;

	@Column(name="Fld_FecAvisoRenovacion")
	private Timestamp fld_FecAvisoRenovacion;

	@Column(name="Fld_FecInicioContrato")
	private Timestamp fld_FecInicioContrato;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_RutContratante")
	private String fld_RutContratante;

	public Tbl_PasoContrato() {
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrNombreTAvisa() {
		return this.fld_CorrNombreTAvisa;
	}

	public void setFld_CorrNombreTAvisa(BigDecimal fld_CorrNombreTAvisa) {
		this.fld_CorrNombreTAvisa = fld_CorrNombreTAvisa;
	}

	public BigDecimal getFld_CorrNomContratante() {
		return this.fld_CorrNomContratante;
	}

	public void setFld_CorrNomContratante(BigDecimal fld_CorrNomContratante) {
		this.fld_CorrNomContratante = fld_CorrNomContratante;
	}

	public byte getFld_DuracionContrato() {
		return this.fld_DuracionContrato;
	}

	public void setFld_DuracionContrato(byte fld_DuracionContrato) {
		this.fld_DuracionContrato = fld_DuracionContrato;
	}

	public Timestamp getFld_FecAvisoRenovacion() {
		return this.fld_FecAvisoRenovacion;
	}

	public void setFld_FecAvisoRenovacion(Timestamp fld_FecAvisoRenovacion) {
		this.fld_FecAvisoRenovacion = fld_FecAvisoRenovacion;
	}

	public Timestamp getFld_FecInicioContrato() {
		return this.fld_FecInicioContrato;
	}

	public void setFld_FecInicioContrato(Timestamp fld_FecInicioContrato) {
		this.fld_FecInicioContrato = fld_FecInicioContrato;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public String getFld_RutContratante() {
		return this.fld_RutContratante;
	}

	public void setFld_RutContratante(String fld_RutContratante) {
		this.fld_RutContratante = fld_RutContratante;
	}

}