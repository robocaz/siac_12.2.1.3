package cl.ccs.siac.teavisa;

import java.util.List;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TeAvisaDTO;

public interface TeAvisaBusiness {

	public TeAvisaDTO contrataTeAvisa(TeAvisaDTO contrato);
	
	public ResponseDTO validaContratoTeAvisa(String rutAvisa);
	
	public List<TeAvisaDTO> buscaContratos(String rut);
	
	public List<TeAvisaDTO> buscaContratosRenovacion(String rut, Integer criterio, Long corrContrato);
	
	public Long costoServicioUpgrade(Long corrContrato, String codServicio);
}
