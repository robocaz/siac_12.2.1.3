package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 4493355690606795750L;
	protected String msgControl;
    protected Integer idControl;
    
    public enum IdRespuestaBinaria {
    	SI(1), NO(0);
    	
    	public Integer value;
    	
    	IdRespuestaBinaria(Integer value) {
    		this.value = value;
    	}
    }
    
    public void setMsgControl(String msgControl) {
        this.msgControl = msgControl;
    }

    public String getMsgControl() {
        return msgControl;
    }

    public void setIdControl(Integer idControl) {
        this.idControl = idControl;
    }

    public Integer getIdControl() {
        return idControl;
    }
}
