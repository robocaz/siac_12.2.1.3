package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_LogTarifaAclBat database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LogTarifaAclBat.findAll", query="SELECT t FROM Tbl_LogTarifaAclBat t")
public class Tbl_LogTarifaAclBat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_CostoTramo")
	private BigDecimal fld_CostoTramo;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_MontoEnPesos")
	private BigDecimal fld_MontoEnPesos;

	@Column(name="Fld_MontoMovimiento")
	private BigDecimal fld_MontoMovimiento;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TotFaltante")
	private BigDecimal fld_TotFaltante;

	@Column(name="Fld_TotSobrante")
	private BigDecimal fld_TotSobrante;

	public Tbl_LogTarifaAclBat() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public BigDecimal getFld_CostoTramo() {
		return this.fld_CostoTramo;
	}

	public void setFld_CostoTramo(BigDecimal fld_CostoTramo) {
		this.fld_CostoTramo = fld_CostoTramo;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public BigDecimal getFld_MontoEnPesos() {
		return this.fld_MontoEnPesos;
	}

	public void setFld_MontoEnPesos(BigDecimal fld_MontoEnPesos) {
		this.fld_MontoEnPesos = fld_MontoEnPesos;
	}

	public BigDecimal getFld_MontoMovimiento() {
		return this.fld_MontoMovimiento;
	}

	public void setFld_MontoMovimiento(BigDecimal fld_MontoMovimiento) {
		this.fld_MontoMovimiento = fld_MontoMovimiento;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public BigDecimal getFld_TotFaltante() {
		return this.fld_TotFaltante;
	}

	public void setFld_TotFaltante(BigDecimal fld_TotFaltante) {
		this.fld_TotFaltante = fld_TotFaltante;
	}

	public BigDecimal getFld_TotSobrante() {
		return this.fld_TotSobrante;
	}

	public void setFld_TotSobrante(BigDecimal fld_TotSobrante) {
		this.fld_TotSobrante = fld_TotSobrante;
	}

}