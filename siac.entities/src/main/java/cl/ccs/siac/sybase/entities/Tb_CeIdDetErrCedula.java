package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdDetErrCedulas database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetErrCedulas")
@NamedQuery(name="Tb_CeIdDetErrCedula.findAll", query="SELECT t FROM Tb_CeIdDetErrCedula t")
public class Tb_CeIdDetErrCedula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CorrErrCedula_Id")
	private BigDecimal fld_CorrErrCedula_Id;

	//bi-directional many-to-one association to Tb_CeIdDetCedula
	@ManyToOne
	@JoinColumn(name="Fld_CorrCedula")
	private Tb_CeIdDetCedula tbCeIdDetCedula;

	public Tb_CeIdDetErrCedula() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public BigDecimal getFld_CorrErrCedula_Id() {
		return this.fld_CorrErrCedula_Id;
	}

	public void setFld_CorrErrCedula_Id(BigDecimal fld_CorrErrCedula_Id) {
		this.fld_CorrErrCedula_Id = fld_CorrErrCedula_Id;
	}

	public Tb_CeIdDetCedula getTbCeIdDetCedula() {
		return this.tbCeIdDetCedula;
	}

	public void setTbCeIdDetCedula(Tb_CeIdDetCedula tbCeIdDetCedula) {
		this.tbCeIdDetCedula = tbCeIdDetCedula;
	}

}