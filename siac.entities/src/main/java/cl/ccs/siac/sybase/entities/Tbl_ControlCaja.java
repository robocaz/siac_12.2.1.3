package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlCaja database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlCaja.findAll", query="SELECT t FROM Tbl_ControlCaja t")
public class Tbl_ControlCaja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodOrigen")
	private String fld_CodOrigen;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioCierre")
	private String fld_CodUsuarioCierre;

	@Column(name="Fld_CodUsuarioContable")
	private String fld_CodUsuarioContable;

	@Column(name="Fld_CorrAsiento")
	private BigDecimal fld_CorrAsiento;

	@Column(name="Fld_CorrCaja_Id")
	private BigDecimal fld_CorrCaja_Id;

	@Column(name="Fld_EstCaja")
	private String fld_EstCaja;

	@Column(name="Fld_FecCierreCaja")
	private Timestamp fld_FecCierreCaja;

	@Column(name="Fld_FecCierreTemp")
	private Timestamp fld_FecCierreTemp;

	@Column(name="Fld_FecContabilizacion")
	private Timestamp fld_FecContabilizacion;

	@Column(name="Fld_FecContableTemp")
	private Timestamp fld_FecContableTemp;

	@Column(name="Fld_FecFinCaja")
	private Timestamp fld_FecFinCaja;

	@Column(name="Fld_FecInicioCaja")
	private Timestamp fld_FecInicioCaja;

	@Column(name="Fld_FecValidacion")
	private Timestamp fld_FecValidacion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_FlagCierreTemp")
	private boolean fld_FlagCierreTemp;

	@Column(name="Fld_FlagRegistroValido")
	private boolean fld_FlagRegistroValido;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TotFaltante")
	private BigDecimal fld_TotFaltante;

	@Column(name="Fld_TotSobrante")
	private BigDecimal fld_TotSobrante;

	public Tbl_ControlCaja() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodOrigen() {
		return this.fld_CodOrigen;
	}

	public void setFld_CodOrigen(String fld_CodOrigen) {
		this.fld_CodOrigen = fld_CodOrigen;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioCierre() {
		return this.fld_CodUsuarioCierre;
	}

	public void setFld_CodUsuarioCierre(String fld_CodUsuarioCierre) {
		this.fld_CodUsuarioCierre = fld_CodUsuarioCierre;
	}

	public String getFld_CodUsuarioContable() {
		return this.fld_CodUsuarioContable;
	}

	public void setFld_CodUsuarioContable(String fld_CodUsuarioContable) {
		this.fld_CodUsuarioContable = fld_CodUsuarioContable;
	}

	public BigDecimal getFld_CorrAsiento() {
		return this.fld_CorrAsiento;
	}

	public void setFld_CorrAsiento(BigDecimal fld_CorrAsiento) {
		this.fld_CorrAsiento = fld_CorrAsiento;
	}

	public BigDecimal getFld_CorrCaja_Id() {
		return this.fld_CorrCaja_Id;
	}

	public void setFld_CorrCaja_Id(BigDecimal fld_CorrCaja_Id) {
		this.fld_CorrCaja_Id = fld_CorrCaja_Id;
	}

	public String getFld_EstCaja() {
		return this.fld_EstCaja;
	}

	public void setFld_EstCaja(String fld_EstCaja) {
		this.fld_EstCaja = fld_EstCaja;
	}

	public Timestamp getFld_FecCierreCaja() {
		return this.fld_FecCierreCaja;
	}

	public void setFld_FecCierreCaja(Timestamp fld_FecCierreCaja) {
		this.fld_FecCierreCaja = fld_FecCierreCaja;
	}

	public Timestamp getFld_FecCierreTemp() {
		return this.fld_FecCierreTemp;
	}

	public void setFld_FecCierreTemp(Timestamp fld_FecCierreTemp) {
		this.fld_FecCierreTemp = fld_FecCierreTemp;
	}

	public Timestamp getFld_FecContabilizacion() {
		return this.fld_FecContabilizacion;
	}

	public void setFld_FecContabilizacion(Timestamp fld_FecContabilizacion) {
		this.fld_FecContabilizacion = fld_FecContabilizacion;
	}

	public Timestamp getFld_FecContableTemp() {
		return this.fld_FecContableTemp;
	}

	public void setFld_FecContableTemp(Timestamp fld_FecContableTemp) {
		this.fld_FecContableTemp = fld_FecContableTemp;
	}

	public Timestamp getFld_FecFinCaja() {
		return this.fld_FecFinCaja;
	}

	public void setFld_FecFinCaja(Timestamp fld_FecFinCaja) {
		this.fld_FecFinCaja = fld_FecFinCaja;
	}

	public Timestamp getFld_FecInicioCaja() {
		return this.fld_FecInicioCaja;
	}

	public void setFld_FecInicioCaja(Timestamp fld_FecInicioCaja) {
		this.fld_FecInicioCaja = fld_FecInicioCaja;
	}

	public Timestamp getFld_FecValidacion() {
		return this.fld_FecValidacion;
	}

	public void setFld_FecValidacion(Timestamp fld_FecValidacion) {
		this.fld_FecValidacion = fld_FecValidacion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_FlagCierreTemp() {
		return this.fld_FlagCierreTemp;
	}

	public void setFld_FlagCierreTemp(boolean fld_FlagCierreTemp) {
		this.fld_FlagCierreTemp = fld_FlagCierreTemp;
	}

	public boolean getFld_FlagRegistroValido() {
		return this.fld_FlagRegistroValido;
	}

	public void setFld_FlagRegistroValido(boolean fld_FlagRegistroValido) {
		this.fld_FlagRegistroValido = fld_FlagRegistroValido;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_TotFaltante() {
		return this.fld_TotFaltante;
	}

	public void setFld_TotFaltante(BigDecimal fld_TotFaltante) {
		this.fld_TotFaltante = fld_TotFaltante;
	}

	public BigDecimal getFld_TotSobrante() {
		return this.fld_TotSobrante;
	}

	public void setFld_TotSobrante(BigDecimal fld_TotSobrante) {
		this.fld_TotSobrante = fld_TotSobrante;
	}

}