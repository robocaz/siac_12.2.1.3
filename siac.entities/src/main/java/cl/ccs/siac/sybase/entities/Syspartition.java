package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the syspartitions database table.
 * 
 */
@Entity
@Table(name="syspartitions")
@NamedQuery(name="Syspartition.findAll", query="SELECT s FROM Syspartition s")
public class Syspartition implements Serializable {
	private static final long serialVersionUID = 1L;

	private String cdataptnname;

	private Timestamp crdate;

	@Column(name="data_partitionid")
	private int dataPartitionid;

	private int datoampage;

	private int firstpage;

	private int id;

	private short indid;

	private int indoampage;

	@Column(name="lobcomp_lvl")
	private byte lobcompLvl;

	private String name;

	private int partitionid;

	private byte ptndcompver;

	private int rootpage;

	private short segment;

	private int status;

	public Syspartition() {
	}

	public String getCdataptnname() {
		return this.cdataptnname;
	}

	public void setCdataptnname(String cdataptnname) {
		this.cdataptnname = cdataptnname;
	}

	public Timestamp getCrdate() {
		return this.crdate;
	}

	public void setCrdate(Timestamp crdate) {
		this.crdate = crdate;
	}

	public int getDataPartitionid() {
		return this.dataPartitionid;
	}

	public void setDataPartitionid(int dataPartitionid) {
		this.dataPartitionid = dataPartitionid;
	}

	public int getDatoampage() {
		return this.datoampage;
	}

	public void setDatoampage(int datoampage) {
		this.datoampage = datoampage;
	}

	public int getFirstpage() {
		return this.firstpage;
	}

	public void setFirstpage(int firstpage) {
		this.firstpage = firstpage;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getIndid() {
		return this.indid;
	}

	public void setIndid(short indid) {
		this.indid = indid;
	}

	public int getIndoampage() {
		return this.indoampage;
	}

	public void setIndoampage(int indoampage) {
		this.indoampage = indoampage;
	}

	public byte getLobcompLvl() {
		return this.lobcompLvl;
	}

	public void setLobcompLvl(byte lobcompLvl) {
		this.lobcompLvl = lobcompLvl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPartitionid() {
		return this.partitionid;
	}

	public void setPartitionid(int partitionid) {
		this.partitionid = partitionid;
	}

	public byte getPtndcompver() {
		return this.ptndcompver;
	}

	public void setPtndcompver(byte ptndcompver) {
		this.ptndcompver = ptndcompver;
	}

	public int getRootpage() {
		return this.rootpage;
	}

	public void setRootpage(int rootpage) {
		this.rootpage = rootpage;
	}

	public short getSegment() {
		return this.segment;
	}

	public void setSegment(short segment) {
		this.segment = segment;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}