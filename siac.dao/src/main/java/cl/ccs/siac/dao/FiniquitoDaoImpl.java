package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.FiniquitoDTO;

@Named("finiquitoDao")
@Stateless
public class FiniquitoDaoImpl implements FiniquitoDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;

	private static final Logger log = LogManager.getLogger(FiniquitoDaoImpl.class);

	@Override
	public FiniquitoDTO consultaFiniquito(final String rut) {

		FiniquitoDTO finiquito = new FiniquitoDTO();

		try {
			
			log.info("Ejecutando SQ_DetFiniquito2_SW2");
			
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_DetFiniquito2_SW2");

			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(4, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(12, Boolean.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(13, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(14, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(15, String.class, ParameterMode.OUT);

			spq.setParameter(1, rut);

			spq.execute();

			finiquito.setRutAfectado(spq.getOutputParameterValue(1).toString());
			finiquito.setCorrFiniquito((BigDecimal) spq.getOutputParameterValue(2));
			finiquito.setFecIniContrato(String.valueOf(spq.getOutputParameterValue(3)));
			finiquito.setFecFinContrato(String.valueOf(spq.getOutputParameterValue(4)));
			finiquito.setFecFiniquito(String.valueOf(spq.getOutputParameterValue(5)));
			finiquito.setRutEmpresa(spq.getOutputParameterValue(6).toString());
			finiquito.setNombreApPatEmp(spq.getOutputParameterValue(7).toString());
			finiquito.setNombreApMatEmp(spq.getOutputParameterValue(8).toString());
			finiquito.setNombreNombresEmp(spq.getOutputParameterValue(9).toString());
			finiquito.setCodAfp((Integer) spq.getOutputParameterValue(10));
			finiquito.setDocPresentadoFnq((Integer) spq.getOutputParameterValue(11));
			finiquito.setMarcaProrroga((Boolean) spq.getOutputParameterValue(12));
			finiquito.setFecDecJurada(String.valueOf(spq.getOutputParameterValue(13)));
			finiquito.setRetorno((Integer) spq.getOutputParameterValue(14));
			finiquito.setMensaje(spq.getOutputParameterValue(15).toString());

			finiquito.setMsgControl("OK");

		} catch (Exception e) {
			log.error("Error en el SP SQ_DetFiniquito2_SW2 " + e.getMessage());
			finiquito.setEstadoRespuesta(-1);
			finiquito.setMsgControl("ERR");
		}

		return finiquito;
	}

	public FiniquitoDTO guardaFiniquito(FiniquitoDTO finiquito) {

		try {

			log.info("Ejecutando SI_CertFiniquito3_SW2");
			
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SI_CertFiniquito3_SW2");

			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(10, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(11, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(12, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(13, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(14, Boolean.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(15, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(16, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(17, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(18, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(19, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(20, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(21, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(22, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(23, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(24, String.class, ParameterMode.OUT);

			spq.setParameter(1, finiquito.getCorrFiniquito());
			spq.setParameter(2, finiquito.getCorrCaja());
			spq.setParameter(3, finiquito.getRutAfectado());
			spq.setParameter(4, finiquito.getNombreApPat());
			spq.setParameter(5, finiquito.getNombreApMat());
			spq.setParameter(6, finiquito.getNombreNombres());
			spq.setParameter(7, finiquito.getRutEmpresa());
			spq.setParameter(8, finiquito.getNombreApPatEmp());
			spq.setParameter(9, finiquito.getNombreApMatEmp());
			spq.setParameter(10, finiquito.getNombreNombresEmp());
			spq.setParameter(11, Timestamp.valueOf(finiquito.getFecIniContrato()));
			spq.setParameter(12, Timestamp.valueOf(finiquito.getFecFinContrato()));
			spq.setParameter(13, Timestamp.valueOf(finiquito.getFecFiniquito()));
			spq.setParameter(14, finiquito.getMarcaProrroga());
			spq.setParameter(15, Timestamp.valueOf(finiquito.getFecDecJurada()));
			spq.setParameter(16, finiquito.getDocPresentadoFnq());
			spq.setParameter(17, finiquito.getRutTramitante());
			spq.setParameter(18, finiquito.getCorrNomTram());
			spq.setParameter(19, finiquito.getCodAfp());
			spq.setParameter(20, Timestamp.valueOf(finiquito.getFecUltCotizacion()));
			spq.setParameter(21, Timestamp.valueOf(finiquito.getFecCertCotizacion()));

			spq.execute();

			finiquito.setEstadoRespuesta((Integer) spq.getOutputParameterValue(22));
			finiquito.setRetorno((Integer) spq.getOutputParameterValue(23));
			finiquito.setMensaje(spq.getOutputParameterValue(24).toString());

			System.out.println("EstResp: " + finiquito.getEstadoRespuesta());
			System.out.println("Retorno: " + finiquito.getRetorno());
			System.out.println("Mensaje: " + finiquito.getMensaje());

			finiquito.setMsgControl("OK");

		} catch (Exception e) {
			log.error("Error en el SP SI_CertFiniquito3_SW2 " + e.getMessage());
			finiquito.setEstadoRespuesta(-1);
			finiquito.setMsgControl("ERR");
		}

		return finiquito;
	}

}
