package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_AUDITAPERFILCONSULTA database table.
 * 
 */
@Entity
@Table(name="TBL_AUDITAPERFILCONSULTA")
@NamedQuery(name="TblAuditaperfilconsulta.findAll", query="SELECT t FROM TblAuditaperfilconsulta t")
public class TblAuditaperfilconsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ACCION")
	private String fldAccion;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CORRPERFIL")
	private BigDecimal fldCorrperfil;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblAuditaperfilconsulta() {
	}

	public String getFldAccion() {
		return this.fldAccion;
	}

	public void setFldAccion(String fldAccion) {
		this.fldAccion = fldAccion;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCorrperfil() {
		return this.fldCorrperfil;
	}

	public void setFldCorrperfil(BigDecimal fldCorrperfil) {
		this.fldCorrperfil = fldCorrperfil;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}