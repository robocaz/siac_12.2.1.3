package cl.ccs.siac.observacion;

import cl.exe.ccs.dto.ObservacionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface ObservacionBusiness {
	public ResponseListDTO<ObservacionDTO> listar(String rutTramitante);
	public ResponseDTO crear(ObservacionDTO observacionDTO);
}
