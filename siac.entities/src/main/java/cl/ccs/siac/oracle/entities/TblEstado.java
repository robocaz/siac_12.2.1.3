package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_ESTADO database table.
 * 
 */
@Entity
@Table(name="TBL_ESTADO")
@NamedQuery(name="TblEstado.findAll", query="SELECT t FROM TblEstado t")
public class TblEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ESTADO_FLDCODESTADO_GENERATOR", sequenceName="FLD_CODESTADO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ESTADO_FLDCODESTADO_GENERATOR")
	@Column(name="FLD_CODESTADO")
	private long fldCodestado;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSAESTADO")
	private String fldGlosaestado;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblEstado() {
	}

	public long getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(long fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosaestado() {
		return this.fldGlosaestado;
	}

	public void setFldGlosaestado(String fldGlosaestado) {
		this.fldGlosaestado = fldGlosaestado;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}