package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdTipoFuente database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdTipoFuente.findAll", query="SELECT t FROM Tp_CeIdTipoFuente t")
public class Tp_CeIdTipoFuente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoFuente")
	private String fld_GlosaTipoFuente;

	@Column(name="Fld_TipoFuente")
	private byte fld_TipoFuente;

	public Tp_CeIdTipoFuente() {
	}

	public String getFld_GlosaTipoFuente() {
		return this.fld_GlosaTipoFuente;
	}

	public void setFld_GlosaTipoFuente(String fld_GlosaTipoFuente) {
		this.fld_GlosaTipoFuente = fld_GlosaTipoFuente;
	}

	public byte getFld_TipoFuente() {
		return this.fld_TipoFuente;
	}

	public void setFld_TipoFuente(byte fld_TipoFuente) {
		this.fld_TipoFuente = fld_TipoFuente;
	}

}