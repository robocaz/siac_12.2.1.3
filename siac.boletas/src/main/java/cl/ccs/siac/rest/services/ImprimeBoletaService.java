package cl.ccs.siac.rest.services;

import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import javax.inject.Inject;
import javax.print.PrintException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import cl.ccs.siac.dto.BoletaTransaccionDTO;
import cl.ccs.siac.dto.CertificadoDTO;
import cl.ccs.siac.printer.Impresion;

@Path("/siac")
public class ImprimeBoletaService {
 
	@Inject
	private Impresion utilImpresion;
	
	private static final Logger LOG = LogManager.getLogger(ImprimeBoletaService.class);


	@POST
	@Path("/impresion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response imprimeBoleta(String data) throws UnknownHostException {
		
		String respuesta = "";
		int status = 0;
		Gson gs = new Gson();
		BoletaTransaccionDTO boletaTransaccionDTO =  gs.fromJson(data, BoletaTransaccionDTO.class);

		respuesta = utilImpresion.imprimeBoletaPortCom(boletaTransaccionDTO);
		
		String result = "Boleta impresa : " + boletaTransaccionDTO.getNumeroBoleta();
		
		if(respuesta.equals("0")){
			status = 200;
		}
		else{
			status = 404;
		}
	    return Response.ok().entity(respuesta)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS")
	            .header("Access-Control-Allow-Headers", "X-Requested-With, X-Codingpedia,Authorization")
	            .build();

	}
	
	
	@POST
	@Path("/imprimereporte")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response imprimeReporte(byte[] reporte) throws IOException, PrinterException, PrintException, InterruptedException {
		
		
		
		
		
		FileOutputStream fos = new FileOutputStream("out.pdf");
		fos.write(reporte);
		fos.close();

		Runtime rs = Runtime.getRuntime();
		 
	    try {
	      rs.exec("java -jar siac.impresora.jar out.pdf" );
	    }
	    catch (IOException e) {
	      System.out.println(e);
	    }   
	    finally {
	    	FileUtils.forceDeleteOnExit(new File("out.txt"));
		}
	    


		return Response.ok().entity(Response.Status.OK)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS")
	            .header("Access-Control-Allow-Headers", "Content-Type, X-Requested-With, X-Codingpedia,Authorization")
	            .build();

	}	
	
	@POST
	@Path("/imprimereportearreglo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response imprimeReporteArreglo(List<CertificadoDTO> reporte, @Context HttpServletRequest request) throws IOException, InterruptedException  {
		
		for(CertificadoDTO cert: reporte){

			FileUtils.writeByteArrayToFile(new File("temp/"+cert.getNombre()+".pdf"), cert.getArchivopdf());
			LOG.info("temp/"+cert.getNombre()+".pdf");
			Runtime rs = Runtime.getRuntime();
			 
		    try {
		      rs.exec("java -jar siac.impresora.jar temp/"+cert.getNombre()+".pdf");
		      Thread.sleep(3000);  //ROP Se agrega dealy para tratar que imprima ordenado
		    }
		    catch (IOException e) {
		      LOG.error(e.getStackTrace());
		      System.out.println(e);
		    }   
		    
		}

		return Response.ok().entity(Response.Status.OK).build();

	}	
	
	
	@POST
	@Path("/borratemporales")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response borraTemporales() throws IOException {
		
		File dir = new File("temp");
		System.out.println(dir.getAbsolutePath());
		FileUtils.cleanDirectory(dir);
		return Response.ok().entity(Response.Status.OK).build();
	}
	
	
	@POST
	@Path("/prueba")
	@Produces("application/json")
	@Consumes("application/json")
	public Response imprimePrueba() throws UnknownHostException {

		System.out.println("+++++++++++++++++++++++++++++++++++++");
		 return Response.ok().entity(new Long(0))
	    			.header("Access-Control-Allow-Origin", "*")
	    			.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
	    			.allow("OPTIONS").build();

	}
}
