angular.module('ccs').controller('ConsolidadoAclaracionTramiteController', function($scope, TransaccionResource ,PersonaResource, TramitanteResource ,$location,RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.afectado = {};



	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;

	$scope.getTramitante = function(){
		$scope.limpiarAfectado();
	};

	$scope.esRutValido = function(){

		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ){
			return true;
		}else{
			return false;
		}
	};

	$scope.esRutJuridico = function (rut) {
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};

	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		$scope.buscaAfectado("tramitante");

	}

	$scope.limpiarAfectado = function(){
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
        $scope.afectado.nombreafectado = null;
        $scope.esrutjuridico = false;
        $scope.editable = false;
	};

	$scope.buscaAfectado = function (tipoPersona) {
        $scope.limpiarAfectado();
		$scope.esvalido = false;

		if($scope.afectado.rut === undefined ){
			return;
		}
        if($scope.afectado.rut === null ){
			$scope.afectado.rut  = undefined;
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
  				function () {
					$("#rutAfectadoInforme").focus();
				});
			return;
		}

		if($scope.esRutValido()){
			if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
				swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
			}else{
				if(tipoPersona == "tramitante"){
					$scope.obtenerAfectadoTramitante();
				}
				else{
					$scope.obtenerAfectado();
				}
			}
		}else{
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'});
			$scope.afectado.rut == null;
		}
	}

	$scope.obtenerAfectado = function(){
		var successCallback = function(data,responseHeaders){
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
                $scope.editable = false;
				if($scope.esrutjuridico){
					$scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}
				$('#ap_pat_pag').focus();
			}
		};
        PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
	};


	$scope.obtenerAfectadoTramitante = function(){
		var successCallback = function(data,responseHeaders){
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
                $scope.editable = false;
				if($scope.esrutjuridico){
					$scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}
				$('#ap_pat_pag').focus();
			}
		};
        TramitanteResource.carga({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
	};


	$scope.validacionCertificado = function () {
		
		$scope.$emit('logout');
		
		// Rut
		if( typeof($scope.afectado.rut) === 'undefined' || $scope.afectado.rut === null || $scope.afectado.rut.trim() === '' || !$scope.esRutValido() ) {
	      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
	    // Declaración de Uso
	    if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
	      swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
	      return;
	    }
		// Apellido Paterno
		if( !$scope.esRutJuridico() && ( typeof($scope.afectado.apellidoPaterno) === 'undefined' || $scope.afectado.apellidoPaterno === null || $scope.afectado.apellidoPaterno.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Apellido Paterno', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
		// Nombres
		if( !$scope.esRutJuridico() && ( typeof($scope.afectado.nombres) === 'undefined' || $scope.afectado.nombres === null || $scope.afectado.nombres.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
		// Razon Social
		if( $scope.esRutJuridico() && ( typeof($scope.afectado.nombreafectado) === 'undefined' || $scope.afectado.nombreafectado === null || $scope.afectado.nombreafectado.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Nombre Afectado', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }

        if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso && $scope.afectado.rut != $rootScope.tramitante.rut){
            $scope.authSupervisor();
        }else{
            $scope.guardar();
        }
	};

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#consolidadoaclaraciontramite')),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){
				  $scope.guardar();
			  }
		  });
	};


    $scope.guardar = function(){
    		// if($scope.editable){
			$scope.guardarPersona();
			//	}else{
			//$scope.guardarServicio();
			//	}
    }

	$scope.guardarServicio = function(){
    var esJuridico = $scope.esrutjuridico;
		$scope.transaccion = {
			codServicio : 'ICT',
			rutAfectado : $scope.afectado.rut,
			corrNombre: $scope.afectado.corrNombre
		}
		var successCallback = function(data,responseHeaders){
			$rootScope.pendienteFlag = true;
			$scope.listarServicioTramitante();
			//$('#consolidadoaclaraciontramite').modal('hide');
      $scope.afectado.rut = undefined;
      $scope.getTramitante();

			swal({
				  title: 'Operación Exitosa',
				  text: "Actualización realizada exitosamente",
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Continuar'
				}).then(function () {
					if(!esJuridico){
						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
					}
				});





		};
        TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
	}

	$scope.guardarPersona = function(){

		if($scope.esrutjuridico){
			$scope.afectado.apellidoPaterno = $scope.afectado.nombreafectado.substring(0, 25);
			$scope.afectado.apellidoMaterno = $scope.afectado.nombreafectado.substring(25, 50);
			$scope.afectado.nombres 		= $scope.afectado.nombreafectado.substring(50, 75);
		}

		var successCallback = function(data,responseHeaders){

			if(data.correlativo == "0"){
                swal('Alerta','No se ha actualizado el tramitante','error');
			} else{
				$scope.afectado.corrNombre = data.correlativo;
				$scope.guardarServicio();
			}
		};
        PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};

	$('#consolidadoaclaraciontramite').on('show.bs.modal', function (e) {
        $timeout(function(){
            $scope.afectado.rut = undefined;
            $scope.getTramitante();

			console.log("netre focus");
        });
    });
	$("#consolidadoaclaraciontramite").on('shown.bs.modal', function () {
		$("#rutAfectadoInforme").focus();
	});
});
