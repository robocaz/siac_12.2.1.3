angular.module('ccs').controller('AclaracionController', function ($scope, $window,TransaccionResource ,AclaracionResource, DetectorFraudeResource, PersonaResource, PagosResource ,$location,RutHelper,
			 $rootScope, $uibModal, $document, $cookies, $timeout, $filter,$q) {

  $scope.model = {
    name: 'Tabs'
  };


	$scope.tiposEmisores = [];
	$scope.listadocumentos = [];
	$scope.afectado = {};
	$scope.sucursales = [];
	$scope.aclaracion = {};
	$scope.aclaracion.sucursal = {};
	$scope.aclaracion.emisor = {};
	$scope.aclaracion.docpresentada = {};
	$scope.aclaracion.tipoemisor = {};
	$scope.numerofirmas = {};
	$scope.tiposDocumentos = [];
	$scope.tab = 0;
	$scope.tiposMonedas = [];
	$scope.aclaracion.firmante = {};
	$scope.detalleFirma = {};
	$scope.listafirmas = [];
	$scope.listafirmantes = [];
	$scope.bipersonalError = [];
	$scope.avanza = false;
	$scope.confirmatorio = false;
	$scope.habilitado = false;
	$scope.habilitadoBoton = false;
	$scope.numeroconfirmatorio = {};
	$scope.usafirma = 0;
	$scope.usaconfirmatorio = 0;
	$scope.disableSucursal = false;
	$scope.disableBiper = false;
	$scope.cuotasel = [];

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;
	$scope.estadoHabi = false;
	$scope.docaclarar = {};
	$scope.bipersonal = {};
	$scope.cartera = {};
	$scope.botonAceptar = false;
	$scope.botonSiguiente = false;
	$scope.valueBack = 1;
	$scope.editableAcl = true;
	$scope.biperCamposBloq = false;
	$scope.codEmisorAux = {};
	$scope.tipoEmisorAux = {};
	$scope.tiposEmisoresAll = [];
	$scope.habilitadoParaFraude = false;
	$scope.rutFraude='00000000-0';

	$scope.aclaracionBase = {};
	
    $scope.estadosCuota = [{ label: 'Pagada', value: 1},{ label:'Regularizada', value: 6}];
    
    
    
    

	$scope.getTramitante = function(){
		$scope.limpiarAfectado();
	};

	$scope.esRutValido = function(){

		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ){
			return true;
		}else{
			return false;
		}
	};

	$scope.esRutJuridicoAfec = function (rut) {
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};

	$scope.esRutJuridicoFirmante = function (rut) {
		var rutNumerico	= $scope.aclaracion.firmante.rut==undefined ?  0 :  parseInt( $scope.aclaracion.firmante.rut.slice(0, $scope.aclaracion.firmante.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};
	
	
	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		$scope.buscaAfectado("tramitante");
		$scope.validaFraude();

	}

	$scope.limpiarAfectado = function(){
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
        $scope.afectado.nombreafectado = null;
        $scope.esrutjuridico = false;
        $scope.editable = false;
	};

	$scope.buscaAfectado = function (tipoPersona) {
        $scope.limpiarAfectado();
		$scope.esvalido = false;


		if($scope.afectado.rut === undefined ){
			return;
		}
        if($scope.afectado.rut === null ){
			$scope.afectado.rut  = undefined;
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
  				function () {
  					$("rutAfectadoAclaracion").focus();
  					return false;
				});
			return;
		}

		if($scope.esRutValido()){
			if($scope.esRutJuridicoAfec() && $rootScope.tramitante.declaracionUso){
				swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
			}else{
				if(tipoPersona == "tramitante"){
					$scope.obtenerAfectadoTramitante();
				}
				else{
					$scope.obtenerAfectado();
				}
			}
		}else{
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'});
			$scope.afectado.rut == null;
		}
	}

	$scope.obtenerAfectado = function(){
		swal({
		  position: 'top-end',
		  type: 'success',
		  title: 'Cargando ...',
		  showConfirmButton: false
		})
		var successCallback = function(data,responseHeaders){
			swal.close();
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Nombre no encontrado','error');

			} else {
                $scope.editable = false;
				if($scope.esrutjuridico){
					$scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
					angular.element("#apPaterno").focus();

				}
				$scope.validaFraude2();

			}
			
		};
        PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
	};

	$scope.focusON = function(){
		$('#ap_pat_pag').focus();
	}

	$scope.obtenerAfectadoTramitante = function(){
		swal({
		  position: 'top-end',
		  type: 'success',
		  title: 'Cargando...',
		  showConfirmButton: false
		})
		var successCallback = function(data,responseHeaders){
			swal.close();
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
                $scope.editable = false;
				if($scope.esrutjuridico){
					$scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
					angular.element("#apPaterno").focus();

				}
				$scope.validaFraude();
			}
		};
        PersonaResource.obtenerTramitante($scope.afectado, successCallback, $scope.errorCallback);
	};


	$scope.validacionCertificado = function () {
		$scope.$emit('logout');
        if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso && $scope.afectado.rut != $rootScope.tramitante.rut){
            $scope.authSupervisor();
        }else{
            $scope.guardar();
        }
	};

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#informecertificado')),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){
				  $scope.guardar();
			  }
		  });
	};


    $scope.guardar = function(){
    		// if($scope.editable){
			$scope.guardarPersona();
			//	}else{
			//$scope.guardarServicio();
			//	}
    }



	$scope.guardarServicio = function(){
		$scope.transaccion = {
			codServicio : 'ISA',
			rutAfectado : $scope.afectado.rut
		}
		var successCallback = function(data,responseHeaders){
			$scope.listarServicioTramitante();
			$('#informecertificado').modal('hide');


			swal({
				  title: 'Operación Exitosa',
				  text: "Actualización realizada exitosamente",
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Continuar',
				  allowOutsideClick: false
				}).then(function () {
					if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
					}
				})





		};
        TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
	}

	$scope.guardarPersona = function(){

		if($scope.esrutjuridico){
			$scope.afectado.apellidoPaterno = $scope.afectado.nombreafectado.substring(0, 25);
			$scope.afectado.apellidoMaterno = $scope.afectado.nombreafectado.substring(25, 50);
			$scope.afectado.nombres 		= $scope.afectado.nombreafectado.substring(50, 75);
		}

		var successCallback = function(data,responseHeaders){

			if(data.correlativo == "0"){
                swal('Alerta','No se ha actualizado el tramitante','error');
			} else{
				$scope.guardarServicio();
			}
		};
        PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};

	$scope.siguiente = function(){
		if($scope.afectado.rut === undefined || $scope.afectado.rut == "" || $scope.afectado.rut == null){
			swal('Alerta','Debe ingresar Rut.','error');
			$("rutAfectadoAclaracion").focus();
			return false;
		}
		if($scope.afectado.apellidoPaterno === undefined || $scope.afectado.apellidoPaterno == "" || $scope.afectado.apellidoPaterno == null){
			swal('Alerta','Debe ingresar Apellido Paterno.','error');
			$("#apPaterno").focus();
			return false;
		}

		if($scope.afectado.nombres === undefined || $scope.afectado.nombres == "" || $scope.afectado.nombres == null){
			swal('Alerta','Debe ingresar Nombres.','error');
			$("#nombres").focus();
			return false;
		}
		if($scope.esrutjuridico && ($scope.afectado.nombreafectado === undefined || $scope.afectado.nombreafectado == "" || $scope.afectado.nombreafectado == null)){
				swal('Alerta','Debe ingresar Nombre.','error');
				$("#nombres").focus();
				return false;
		}

		else{
			var successCallback = function(data,responseHeaders){

				if(data.correlativo == "0"){
	                swal('Alerta','No se ha actualizado el tramitante','error');
				} else{
					$scope.afectado.correlativo = data.correlativo;
					$scope.editableAcl = false;
					$scope.habilitado = true;
					$scope.habilitadoBoton = true;
					if(undefined==$scope.aclaracion.tipodocumento || null==$scope.aclaracion.tipodocumento)
						$scope.$broadcast('UiSelectTipoDoc');
					else{
						
					}
				}
			};

	        PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	    }


	}

	 $rootScope.$on("LimpiaAclaracion", function(event, data) {
		 $scope.resetForm();
     });

	$scope.validaFraude = function(){


		//Revisar fraude usuario
		var successCallbackFraude = function(dataFraude, responseHeadersFraude){
			var poseeFraude = !!(dataFraude.idControl);

			if (poseeFraude) {
				var persona = $scope.afectado;
				persona.rut = $scope.afectado.rut;
	        	persona.tieneDeclaracionUso = false;

				$rootScope.fraude = {
						persona: persona,
						dataFraude : dataFraude.lista
				}
				//$scope.limpiaForm();
				angular.element('#detectorFraudeAclaracion').modal();
				angular.element('#detectorFraudeAclaracion').css('z-index', '999999');

			}
		}

		var errorCallbackFraude = function(responseFraude) {
			$scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
		}

		DetectorFraudeResource.detallesFraudePorRut($scope.afectado, successCallbackFraude, errorCallbackFraude);


	}

	$scope.validaFraude2 = function(){


		//Revisar fraude usuario
		var successCallbackFraude = function(dataFraude, responseHeadersFraude){
			var poseeFraude = !!(dataFraude.idControl);
			
			if (poseeFraude) {
				var persona = $scope.afectado;
				persona.rut = $scope.afectado.rut;
	        	persona.tieneDeclaracionUso = false;

				$rootScope.fraude = {
						persona: persona,
						dataFraude : dataFraude.lista
				}
				if($rootScope.rutFraude!=$scope.afectado.rut) $rootScope.habilitadoParaFraude=false; 
				//$scope.limpiaForm();
				if(!$rootScope.habilitadoParaFraude && $rootScope.rutFraude!=$scope.afectado.rut){
					angular.element('#detectorFraudeAclaracion').modal();
					angular.element('#detectorFraudeAclaracion').css('z-index', '999999');
					$scope.habilitadoParaFraude=true;
					$rootScope.rutFraude = $scope.afectado.rut;
				}
			}else{
				$rootScope.habilitadoParaFraude=false;
				$rootScope.rutFraude='000000000';
			}
		}

		var errorCallbackFraude = function(responseFraude) {
			$scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
		}

		DetectorFraudeResource.detallesFraudePorRut($scope.afectado, successCallbackFraude, errorCallbackFraude);


	}



	$scope.obtieneTipoEmisor = function(){
		$scope.aclaracion.tipoemisor = {};
		$scope.aclaracion.tipoemisor.descripcion = "";
		$scope.aclaracion.emisor = [];
		$scope.aclaracion.emisor.descripcion = "";
		$scope.listadocumentos = [];

		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.tiposEmisores = data;
			}
		};

		AclaracionResource.tiposEmisores({tipoDoc: $scope.aclaracion.tipodocumento.codigo},successCallback, $scope.errorCallback);
	}

	$scope.obtieneTipoEmisorTodos = function(){
		

		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.tiposEmisoresAll = data;
			}
		};

		AclaracionResource.tiposEmisoresTodos(successCallback, $scope.errorCallback);
	}
	
	
	$scope.obtieneEmisores = function(tipoEmisor){

		$scope.aclaracion.emisor = [];
		$scope.aclaracion.emisor.descripcion = "";
		angular.element("#cargando").modal();
		var successCallback = function(data,responseHeaders){
			angular.element("#cargando").modal('hide');
			if(data != null){
				$scope.emisores = data;
				$scope.$broadcast('UiSelectEmisor');
			}
		};

		AclaracionResource.emisores({tipoEmisor: tipoEmisor},successCallback, $scope.errorCallback);
	}

	$scope.documentosAcl = function(){

		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.listadocumentos = data;
			}
		};

		AclaracionResource.documentos({tipoDoc: $scope.aclaracion.tipodocumento.codigo},successCallback, $scope.errorCallback);
	}


	$scope.cargaSucursal = function(){
		$scope.sucursales = [];
		$scope.aclaracion.sucursal = {};
		
		$scope.aclaracion.docpresentada = {};
		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.sucursales = data;
				$scope.$broadcast('UiSelectDocPresentada');
			}
		};
		AclaracionResource.cargaSucursales({tipoEmisor: $scope.aclaracion.tipoemisor.codigo, codEmisor: $scope.aclaracion.emisor.codigo}, successCallback, $scope.errorCallback);

	}

	$scope.tipoDocumentos = function(){
		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.tiposDocumentos = data;
			}
		};
		AclaracionResource.tiposDocumentos(successCallback, $scope.errorCallback);
	}


	$scope.tipoMonedas = function(){
		var successCallback = function(data,responseHeaders){
			if(data != null){
				$scope.tiposMonedas = data;
			}
		};
		AclaracionResource.tiposMoneda(successCallback, $scope.errorCallback);
	}






	$scope.setTab = function(){
		// validacion
		
		
		$timeout(function () {$("#botAnterior").focus();});
		
		if($scope.aclaracion.tipoemisor.codigo !== undefined && $scope.aclaracion.emisor.codigo !== undefined ){
			$scope.avanza = true;
			
			
			
			if($scope.aclaracion.docpresentada.codigo == "CE" || $scope.aclaracion.docpresentada.codigo == "CT"){
				//valido si requiero numero confirmatorio
				$scope.numerofirmas.tipoEmisor = $scope.aclaracion.tipoemisor.codigo;
				$scope.numerofirmas.codEmisor = $scope.aclaracion.emisor.codigo;
				$scope.numerofirmas.tipoDocAclaracion = $scope.aclaracion.docpresentada.codigo;
				$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo;


				var successCallback = function(data,responseHeaders){
					if(data != null){
						$scope.numerofirmas = data;
						$scope.tab = 1;
						//$scope.usafirma = $scope.numerofirmas.flagFirmaNroConf;
						$scope.usafirma = 1;
						$scope.usaconfirmatorio = $scope.numerofirmas.usaNrosConfirmatorios;
						if($scope.aclaracion.docpresentada.codigo == "CT"){
							$scope.disableSucursal = true;
						}
						else{
							$scope.disableSucursal = false;
						}
						console.log("usaconfirmatorio: "+$scope.usaconfirmatorio);
						console.log("numerofirmas: "+$scope.numerofirmas);

					}
				};

				AclaracionResource.reqConfirmatorio($scope.numerofirmas ,successCallback, $scope.errorCallback);

			}
			else{
				$scope.tab = 2;
				$scope.numerofirmas = {};
			}

		}
		else{
			$scope.avanza = false;
			swal('Alerta','Para continuar debe seleccionar todos los campos','warning');
		}

		$scope.aclaracion.sucursal = {};
	}



	$scope.confirmatorioOpcional = function(){

		if($scope.active == 0 && $scope.aclaracion.docpresentada.codigo == "CE"){
			if($scope.numerofirmas.flagUsoOpcional == 1 && ($scope.numeroconfirmatorio.codSucursal === undefined || $scope.numeroconfirmatorio.codSucursal == null || $scope.numeroconfirmatorio.codSucursal === "")){

				swal({
				  title: 'Atención',
				  text: "Para este Emisor el número confirmatorio es opcional",
				  type: 'warning',
				  showCancelButton: false,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Aceptar',
				  allowOutsideClick: false

				}).then(function (){
				})

			}
		}

	}





	$scope.siguienteTab = function(){


		if($scope.active == 0){
			if($scope.aclaracion.tipoemisor.codigo === undefined || $scope.aclaracion.tipoemisor.codigo == ""){
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.aclaracion.fechapago = $scope.fechaPagoString;
				swal('Alerta',"Seleccione tipo de emisor",'error');
				return false;
			}



			if($scope.aclaracion.emisor.codigo === undefined || $scope.aclaracion.emisor.codigo == ""){
				swal('Alerta',"Seleccione el emisor",'error');
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.aclaracion.fechapago = $scope.fechaPagoString;
				return false;
			}

			if($scope.aclaracion.docpresentada.codigo === undefined || $scope.aclaracion.docpresentada.codigo == ""){
				swal('Alerta',"Seleccione documentación presentada",'error');
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.aclaracion.fechapago = $scope.fechaPagoString;
				return false;
			}
		}



		if($scope.aclaracion.docpresentada.codigo == "CE" || $scope.aclaracion.docpresentada.codigo == "CT"){



			if($scope.active == 1){



				if($scope.usafirma == 1 && $scope.listafirmantes.length == 0 && $scope.numerofirmas.nroFirmas > 0 && $scope.aclaracion.docpresentada.codigo != "CT"){
					swal('Alerta','Debe seleccionar firmantes','error');
					return false;
				}

				else if($scope.usaconfirmatorio == 1 && $scope.numerofirmas.flagUsoOpcional == 0 &&
					($scope.numeroconfirmatorio.codSucursal === undefined || 
					$scope.numeroconfirmatorio.codSucursal == null || 
					$scope.numeroconfirmatorio.codSucursal === "") && $scope.aclaracion.docpresentada.codigo != "CT"){
					swal('Alerta','Debe ingresar numero confirmatorio','error');
					return false;
				}

				else if($scope.usafirma == 1 && $scope.aclaracion.docpresentada.codigo == "CT" && $scope.listafirmantes.length > 0 &&  $scope.usaconfirmatorio == 1 && $scope.numerofirmas.flagUsoOpcional == 0 &&
					($scope.numeroconfirmatorio.codSucursal === undefined || $scope.numeroconfirmatorio.codSucursal == null || $scope.numeroconfirmatorio.codSucursal === "")){
					swal('Alerta','Debe ingresar numero confirmatorio','error');
					return false;
				}

				else{
					$scope.active = $scope.active + 1;
					$timeout(function () {$scope.$broadcast('UIsucursal');});
				}
			}
			else{
				$scope.active = $scope.active + 1;
				$timeout(function () {$scope.$broadcast('UIsucursal');});
			}

		}else{
          $scope.active = $scope.active + 2;
          $timeout(function () {$("#numoperacion").focus();});

		}
	    //$scope.active = $scope.active + 1;

		if($scope.active == 2){
			$scope.botonAceptar = true;
			$scope.avanza = false;
			$timeout(function () {$("#numoperacion").focus();});
		}
		else{
			$scope.tab = $scope.tab+1;
		}
	}

	$scope.anteriorTab = function(){

		if($scope.active == 0){
			$scope.habilitado = false;
			$scope.editableAcl = true;
			$scope.avanza = false;

			$scope.habilitadoBoton = false;
			$scope.aclaracion = {};

			return false;
		}

		if($scope.aclaracion.docpresentada.codigo == "CE" || $scope.aclaracion.docpresentada.codigo == "CT"){
			$scope.active = $scope.active - 1;
		}else{
			$scope.active = $scope.active - 2;
		}

     	//$scope.active = $scope.active - 1;

		if($scope.active < 2){
			$scope.botonAceptar = false;
			$scope.avanza = true;
		}
		//else if($scope.active == 0){
		//	$scope.habilitado = false;
		//}
	}


	$scope.onTabSelect=function(){

		if($scope.active == 2){
			$scope.botonAceptar = true;
			$scope.avanza = false;
		}
		else{
			$scope.botonAceptar = false;
			$scope.avanza = true;
		}
	}


	$scope.cambiaFocoMonto = function(){
        $timeout(function () {
           $("#monto_acl").focus();
        });
	}

	$scope.cambiaFocoVolver = function(){
        $timeout(function () {
           $("#botAnterior").focus();
        });
	}

	$scope.cambiaFocoFecha = function(){
        $timeout(function () {
           $("#fecha_pago_acl").focus();
        });
	}
	$scope.validaFirmante = function(){

		if($scope.aclaracion.firmante.rut !== undefined && $scope.aclaracion.firmante.rut != ""){
			var rutNumerico	= $scope.aclaracion.firmante.rut==undefined ?  0 :  parseInt($scope.aclaracion.firmante.rut.slice(0, $scope.aclaracion.firmante.rut.length-1).trim());
			if(RutHelper.validate($scope.aclaracion.firmante.rut) && rutNumerico > rutMinimo ){
				
				var esjuridico = $scope.esRutJuridicoFirmante($scope.aclaracion.firmante.rut);
				if(esjuridico){
					swal({ title: 'Error', text: 'Rut firmante no puede ser Jurídico',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
							function () {
								$("#rutfirmante").focus();
								$scope.aclaracion.firmante.rut = "";
								return false;
					});					
				}
				else{
					return true;
				}
				
				
			}else{
				swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () {
					$("#rutfirmante").focus();
				});
				return false;
			}
		}
	};

	$scope.cerrarFirmas = function(){
		console.log($scope.aclaracion);
		console.log($scope.listafirmantes);

		if($scope.aclaracion.docpresentada.codigo == "CT" && $scope.listafirmantes.length > 0){

			$scope.codEmisorAux = $scope.listafirmantes[0].codEmisorAux;
			$scope.tipoEmisorAux = $scope.listafirmantes[0].tipoEmisorAux;

			$scope.usaconfirmatorio = 1;

			$scope.numerofirmas.tipoEmisor = $scope.tipoEmisorAux;
			$scope.numerofirmas.codEmisor = $scope.codEmisorAux;
			$scope.numerofirmas.tipoDocAclaracion = $scope.aclaracion.docpresentada.codigo;
			$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo;


			var successCallback = function(data,responseHeaders){
				if(data != null){
					$scope.numerofirmas = data;
					$scope.tab = 1;
					//$scope.usafirma = $scope.numerofirmas.flagFirmaNroConf;
					$scope.usafirma = 1;
					$scope.usaconfirmatorio = $scope.numerofirmas.usaNrosConfirmatorios;
					if($scope.aclaracion.docpresentada.codigo == "CT"){
						$scope.disableSucursal = true;
						if($scope.numerofirmas.usaNrosConfirmatorios== 1 && $scope.numerofirmas.flagUsoOpcional == 1 && ($scope.numeroconfirmatorio.codSucursal === undefined || $scope.numeroconfirmatorio.codSucursal == null || $scope.numeroconfirmatorio.codSucursal === "")){

							swal({
							  title: 'Atención',
							  text: "Para este Emisor el número confirmatorio es opcional",
							  type: 'warning',
							  showCancelButton: false,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'Aceptar',
							  allowOutsideClick: false

							}).then(function (){
							})

						}
					}
					else{
						$scope.disableSucursal = false;
					}
					$scope.confirmatorioOpcional();

				}
			};

			AclaracionResource.reqConfirmatorio($scope.numerofirmas ,successCallback, $scope.errorCallback);


		}


		angular.element("#verfirma").modal("hide");
	}



	$scope.consultaConfirmatorio = function(){

		if($scope.aclaracion.docpresentada.codigo == "CT"){
			//valido si requiero numero confirmatorio
			$scope.numerofirmas.tipoEmisor = $scope.aclaracion.tipoemisor.codigo;
			$scope.numerofirmas.codEmisor = $scope.aclaracion.emisor.codigo;
			$scope.numerofirmas.tipoDocAclaracion = $scope.aclaracion.docpresentada.codigo;
			$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo;


			var successCallback = function(data,responseHeaders){
				if(data != null){
					$scope.numerofirmas = data;
					$scope.tab = 1;
					//$scope.usafirma = $scope.numerofirmas.flagFirmaNroConf;
					$scope.usafirma = 1;
					$scope.usaconfirmatorio = $scope.numerofirmas.usaNrosConfirmatorios;
					if($scope.aclaracion.docpresentada.codigo == "CT"){
						$scope.disableSucursal = true;
					}
					else{
						$scope.disableSucursal = false;
					}
					console.log($scope.numerofirmas);

				}
			};

			AclaracionResource.reqConfirmatorio($scope.numerofirmas ,successCallback, $scope.errorCallback);

		}

	}


	$scope.buscaFirmas = function(){



		$scope.detalleFirma.firmante = $scope.aclaracion.firmante;
		$scope.detalleFirma.codSucursalAux = $scope.aclaracion.sucursal.codSucursal;
		$scope.detalleFirma.tipoEmisorAux = $scope.aclaracion.tipoemisor.codigo;
		$scope.detalleFirma.codEmisorAux = $scope.aclaracion.emisor.codigo;
		$scope.detalleFirma.codTipoDoc = $scope.aclaracion.docpresentada.codigo;
		console.log($scope.detalleFirma);

		var successCallback = function(data,responseHeaders){
			if(data.lista.length > 0){
				
				if(data.lista.length > 1){
					$scope.listafirmas = data.lista;
					console.log($scope.listafirmas)
					angular.element("#verfirma").modal();
				}
				if(data.lista.length === 1){
					$scope.cargaImagenFirma(data.lista[0]);
				}

			}
			else{
				swal('Alerta','No hay resultados para estos criterios de busqueda','warning');
			}
		};

		AclaracionResource.buscarFirma($scope.detalleFirma, successCallback, $scope.errorCallback);

	}


	$scope.cargaImagenFirma = function(detalleFirma){

		$scope.detalleFirma = detalleFirma;
		var successCallback = function(data,responseHeaders){
			if(data.idControl == 0){

				$scope.detalleFirma = data;
				$scope.b64encoded = $scope.detalleFirma.imageBinary;
				angular.element("#imgfirma").modal();
			}
			else{
				swal('Alerta',data.msgControl,'warning');
			}
		};

		AclaracionResource.cargaImagen($scope.detalleFirma, successCallback, $scope.errorCallback);

	}

	$scope.cerrarImg = function(){
		angular.element("#imgfirma").modal("hide");
	}


	$scope.agregarFirmante = function(firmante){
		console.log(firmante);
		
		$scope.firmanteAdd = angular.copy(firmante);
		if(!firmante.flagCertifAcl){
			swal('Alerta','El firmante seleccionado no está autorizado para emitir certificados de aclaración.','error');
		}
		else if(firmante.estado == 1){
			swal('Alerta','Firma no vigente, Debe solicitar autorización del supervisor para este certificado.','error');
			$scope.abrirModalTenedor($scope.firmanteAdd);

		}
		else if(firmante.estado == 0){

			if($scope.listafirmantes.length > 0){
			    //$scope.existe = $scope.listafirmantes.filter((firmante) => firmante.firmante.rut !== $scope.firmanteAdd.firmante.rut)[0];
			    //$scope.existe = $filter('filter')($scope.listafirmantes, {checked: true});

			    $scope.ruts = $filter('filter')($scope.listafirmantes, { firmante: $scope.firmanteAdd.firmante });

			    if($scope.ruts.length == 0){
					$scope.listafirmantes.push($scope.firmanteAdd);
					$scope.cerrarImg();
				}
				else{
					swal('Alerta','Firma ya agregada','error');
				}
			}
			else{
				
				if($scope.aclaracion.docpresentada.codigo == "CT"){
					//valido si requiero numero confirmatorio
					$scope.numerofirmas.tipoEmisor = $scope.firmanteAdd.tipoEmisorAux; //firmante
					$scope.numerofirmas.codEmisor = $scope.firmanteAdd.codEmisorAux; //firmante
					$scope.numerofirmas.tipoDocAclaracion = $scope.aclaracion.docpresentada.codigo; //firmante
					$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo; //firmante
					
					console.log("numerofirmas.tipoEmisor:"+$scope.numerofirmas.tipoEmisor);
					console.log("numerofirmas.codEmisor:"+$scope.numerofirmas.codEmisor);
					console.log("numerofirmas.tipoDocAclaracion:"+$scope.numerofirmas.tipoDocAclaracion);
					console.log("numerofirmas.tipoDocumento:"+$scope.numerofirmas.tipoDocumento);
					
					var successCallback = function(data,responseHeaders){
						if(data != null){
							$scope.numerofirmas = data;
							$scope.tab = 1;
							//$scope.usafirma = $scope.numerofirmas.flagFirmaNroConf;
							$scope.usafirma = 1;
							$scope.usaconfirmatorio = $scope.numerofirmas.usaNrosConfirmatorios;
							if($scope.aclaracion.docpresentada.codigo == "CT"){
								$scope.disableSucursal = true;
							}
							else{
								$scope.disableSucursal = false;
							}
							console.log($scope.numerofirmas);
						}
					};

					AclaracionResource.reqConfirmatorio($scope.numerofirmas ,successCallback, $scope.errorCallback); //firmante

				}
				
				$scope.listafirmantes.push($scope.firmanteAdd);
				$scope.cerrarImg();
			}
		}


	}



	$scope.validaConfirmatorio = function(){


		if($scope.numeroconfirmatorio.serie === undefined || $scope.numeroconfirmatorio.serie == ""){
			swal('Alerta','Ingrese número de serie','error');
			$("#compos_serie").focus();
			return false
		}

		if($scope.numeroconfirmatorio.folioNro === undefined || $scope.numeroconfirmatorio.folioNro == ""){
			swal('Alerta','Ingrese folio','error');
			$("#compos_folio").focus();
			return false;
		}

		if($scope.numeroconfirmatorio.nroConfirmatorio === undefined || $scope.numeroconfirmatorio.nroConfirmatorio == ""){
			swal('Alerta','Ingrese número confirmatorio','error');
			$("#compos_confirmatorio").focus();
			return false;
		}


		console.log($scope.numeroconfirmatorio);
		console.log($scope.numerofirmas);

		if($scope.aclaracion.docpresentada.codigo == "CT" && $scope.listafirmantes.length > 0){

			$scope.numeroconfirmatorio.codEmisor = $scope.listafirmantes[0].codEmisorAux;
			$scope.numeroconfirmatorio.tipoEmisor = $scope.listafirmantes[0].tipoEmisorAux;


		}
		else{
			$scope.numeroconfirmatorio.tipoEmisor = $scope.aclaracion.tipoemisor.codigo;
			$scope.numeroconfirmatorio.codEmisor = $scope.aclaracion.emisor.codigo;

		}

		var successCallback = function(data,responseHeaders){
			if(data.idControl == 0){
				
				$scope.numeroconfirmatorio = data;
   				var keepGoing = true;
				angular.forEach($scope.listafirmantes, function (item) {
					if(keepGoing) {
						if(($scope.numerofirmas.flagFirmaNroConf==1 && $scope.aclaracion.docpresentada.codigo == "CT") && ($scope.numeroconfirmatorio.codSucursal !== item.codSucursalAux)){
							keepGoing = false;
							swal({
							  title: 'Alerta',
							  text: "La sucursal del firmante es distinta a la sucursal del nro confirmatorio. No es posible ingresar aclaraciones con el certificado que tiene en su poder",
							  type: 'error',
							  showCancelButton: false,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'SI',
							  allowOutsideClick: false
							}).then(function () {
								$scope.numeroconfirmatorio.serie = "";
								$scope.numeroconfirmatorio.folioNro ="";
								$scope.numeroconfirmatorio.nroConfirmatorio = "";
								$scope.numeroconfirmatorio.codSucursal  = "";
								$scope.$apply();
							})
						}					

					}
		        });

				if(keepGoing){
					swal('Alerta',"Número Confirmatorio OK",'success');
				}

				console.log(data);
			}
			else{
				$scope.numeroconfirmatorio.serie = "";
				$scope.numeroconfirmatorio.folioNro ="";
				$scope.numeroconfirmatorio.nroConfirmatorio = "";
				swal('Alerta',data.msgControl,'error');
			}
		};
		AclaracionResource.numConfirmatorio($scope.numeroconfirmatorio, successCallback, $scope.errorCallback);

	}




	$scope.habilitaEstado = function(){
		if($scope.aclaracion.tipodocumento.codigo == "CM"){
			$scope.estadoHabi = true;
		}
		else{
			$scope.estadoHabi = false;
		}
		$scope.$broadcast('UiSelectTipoEmisor');
	}


	$scope.aceptaAclaracion = function(){


		//validar si esta en el carro
		//
		$scope.fechaProtestoString = $scope.aclaracion.fechaprotesto;
		$scope.fechaPagoString = $scope.aclaracion.fechapago;
		
		$scope.aclaracion.tramitante = $rootScope.tramitante;
		if(!angular.equals({}, $scope.bipersonal) && ($scope.bipersonal !== undefined && $scope.bipersonal !== null)){
			$scope.aclaracion.afectado = $scope.bipersonal;
		}
		else{
			$scope.aclaracion.afectado = $scope.afectado;
		}		

		$scope.fechaProtestoString = $scope.aclaracion.fechaprotesto;
		$scope.fechaPagoString = $scope.aclaracion.fechapago;

		if($scope.aclaracion.numoperacion === undefined || $scope.aclaracion.numoperacion == ""){
			$scope.aclaracion.numoperacion = 0;
			//swal('Alerta',"Ingrese Nro. operación.",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			//return false
		}

		if($scope.aclaracion.tipomoneda === undefined){
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			swal('Alerta',"Seleccione tipo de moneda",'error');
			return false;
		}

		if($scope.aclaracion.monto === undefined){
			swal('Alerta',"Ingrese Monto.",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			return false
		}
		else{
			$scope.aclaracion.monto = $scope.aclaracion.monto.replace(/,/g , "");
		}

		if($scope.aclaracion.monto !== undefined && $scope.aclaracion.monto == 0){
			$scope.aclaracion.monto = "";
			swal('Alerta',"Ingrese Monto mayor a 0 .",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			$("#monto_acl").focus();
			return false
		}



		if($scope.aclaracion.tipoemisor.codigo === undefined || $scope.aclaracion.tipoemisor.codigo == ""){
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			swal('Alerta',"Seleccione tipo de emisor",'error');
			return false;
		}



		if($scope.aclaracion.emisor.codigo === undefined || $scope.aclaracion.emisor.codigo == ""){
			swal('Alerta',"Seleccione el emisor",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			return false;
		}

		if($scope.aclaracion.docpresentada.codigo === undefined || $scope.aclaracion.docpresentada.codigo == ""){
			swal('Alerta',"Seleccione documentación presentada",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			return false;
		}


//		if($scope.aclaracion.tipodocumento.codigo == "CM" && ($scope.aclaracion.estadocuota === undefined || $scope.aclaracion.estadocuota == "")){
//			swal('Alerta',"Seleccione estado cuota",'error');
//			return false;
//		}
//
		if($scope.listafirmantes.length == 0 && $scope.numerofirmas.nroFirmas > 0 && $scope.aclaracion.docpresentada.codigo != "CT"){
			swal('Alerta',"No ha validado la firma para este certificado",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			$("#rutfirmante").focus();
			return false;
		}

		if($scope.listafirmantes.length <  $scope.numerofirmas.nroFirmas && $scope.aclaracion.docpresentada.codigo != "CT"){
			swal('Alerta',"Este Emisor requiere "+ $scope.numerofirmas.nroFirmas +" firmas para validación de certificados.",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			$("#rutfirmante").focus();
			return false;
		}

		if(!angular.equals({}, $scope.numeroconfirmatorio) && ($scope.numeroconfirmatorio.codSucursal === undefined || $scope.numeroconfirmatorio.codSucursal === "") && $scope.usaconfirmatorio == 0){
			swal('Alerta',"No ha Validado el Nro. Confirmatorio.",'error');
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			$("#compos_serie").focus();
			return false;
		}

		if(($scope.numeroconfirmatorio.codSucursal === undefined || $scope.numeroconfirmatorio.codSucursal === "")
			&& $scope.numerofirmas.flagUsoOpcional == 1 ){
			swal({
				  title: 'Alerta',
				  text: "No validado el Nro. Confirmatorio, pero para este emisor es opcional. ¿Desea validar Nro Confirmatorio?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'SI',
				  cancelButtonText: 'NO'
				}).then(function () {
					$("#compos_serie").focus();
					return false;
				})


		}


		var pattern = /(\d{2})(\d{2})(\d{4})/;
		if($scope.aclaracion.fechaprotesto === undefined){
			swal('Alerta',"Debe ingresar fecha de protesto",'error');
			return false;
		}


		if($scope.aclaracion.fechaprotesto !== undefined){

			$scope.fechaprotesto = new Date($scope.aclaracion.fechaprotesto.replace(pattern, '$3-$2-$1'));
			$scope.aclaracion.fechaprotesto = $scope.fechaprotesto;
		}
		if(!angular.isUndefined($scope.aclaracion.fechapago) && $scope.aclaracion.fechapago !== null && !angular.equals({}, $scope.aclaracion.fechapago) ){
			$scope.fechapago = new Date($scope.aclaracion.fechapago.replace(pattern, '$3-$2-$1'));
			$scope.aclaracion.fechapago = $scope.fechapago;
		}


		if($scope.aclaracion.fechapago !== undefined &&  $scope.aclaracion.fechapago !== null &&  $scope.aclaracion.fechapago !== ""){
			if($scope.aclaracion.fechaprotesto > $scope.aclaracion.fechapago){
				swal('Alerta',"La fecha de protesto no puede ser mayor a la fecha de pago",'error');
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.aclaracion.fechapago = $scope.fechaPagoString;
				return false;
			}
		}


		if($scope.aclaracion.numboletin !== undefined && $scope.aclaracion.numboletin == 0){
			swal('Alerta',"Número de Boletín no debe ser 0",'error');
			$scope.aclaracion.numboletin = "";
			$("#nboletin_acl").focus();
			$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
			$scope.aclaracion.fechapago = $scope.fechaPagoString;
			return false;
		}

		$scope.aclaracion.numeroconfirmatorio = $scope.numeroconfirmatorio;
		$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo;
		$scope.numerofirmas.tipoDocAclaracion =$scope.aclaracion.docpresentada.codigo;
		$scope.aclaracion.numerofirmas = $scope.numerofirmas;
		$scope.aclaracion.listafirmantes = $scope.listafirmantes;
		$scope.aclaracion.tramitante = $rootScope.tramitante;
		if(!angular.equals({}, $scope.bipersonal) && ($scope.bipersonal !== undefined && $scope.bipersonal !== null)){
			$scope.aclaracion.afectado = $scope.bipersonal;
		}
		else{
			$scope.aclaracion.afectado = $scope.afectado;
		}

		console.log($scope.aclaracion);
		
		var successCallback = function(data,responseHeaders){
			
			if(data.idControl === 0){
			

			
					var successCallback = function(data,responseHeaders){
						$scope.aclaracion = data;
						console.log($scope.aclaracion)
						$rootScope.pendienteFlag = true;
						//$scope.tieneVentaCartera($scope.aclaracion);
						var promise = $scope.tieneVentaCartera($scope.aclaracion);
						promise.then(function(resultado) {
							$scope.aclaracion = resultado;
							console.log(resultado);
			
							if($scope.aclaracion.codRetAcl == 0){
			
								//swal('Alerta',$scope.aclaracion.glosaRetAcl,'success');
								$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
								$scope.aclaracion.fechapago = $scope.fechaPagoString;
								swal({
								  title: 'Atencion',
								  text: $scope.aclaracion.glosaRetAcl,
								  type: 'success',
								  showCancelButton: false,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Aceptar',
								  allowOutsideClick: false
								}).then(function (){
									if($scope.aclaracion.ventaCartera.flagVentaCartera == 1){
			
										//swal('Alerta',$scope.aclaracion.ventaCartera.mensaje,'warning');
			
										swal({
										  title: 'Alerta',
										  text: $scope.aclaracion.ventaCartera.mensaje,
										  type: 'warning',
										  showCancelButton: false,
										  confirmButtonColor: '#3085d6',
										  cancelButtonColor: '#d33',
										  confirmButtonText: 'Aceptar',
										  allowOutsideClick: false
										}).then(function (){
											$scope.tipoProtesto = "SINCAND";
											
											if($scope.aclaracion.ventaCartera.flagDespliegue == 1){
												$scope.obtieneTipoEmisorTodos();
												angular.element("#datosventacartera").modal();
											}
											else if($scope.aclaracion.ventaCartera.flagDespliegue == 0 && (
													$scope.aclaracion.docpresentada.codigo !== "PJ" && $scope.aclaracion.docpresentada.codigo !== "PC" && $scope.aclaracion.docpresentada.codigo !== "DO") ){
												$scope.eliminaAclaracion($scope.aclaracion.corrProt);
											}
											else if($scope.aclaracion.ventaCartera.flagDespliegue == 0  && (
													$scope.aclaracion.docpresentada.codigo === "PJ" || $scope.aclaracion.docpresentada.codigo === "PC") ){
												$scope.resetForm();
												
											}
											else{
												$scope.cerrar();
											}
											return false;
			
										})
			
			
									}
			
									else if($scope.aclaracion.codRetAcl == 0 && $scope.aclaracion.tipoPareo != 4 && $scope.aclaracion.cantFilas == 1 && $scope.aclaracion.emisor.codigo == 12 && $scope.aclaracion.tipodocumento.codigo == "CM"){

										swal('Atención'," Se buscaran cuotas anteriores del mismo crédito para ser Aclaradas",'warning');
			
			
										swal({
										  title: 'Atención',
										  text: "Se buscaran cuotas anteriores del mismo crédito para ser Aclaradas",
										  type: 'warning',
										  confirmButtonColor: '#3085d6',
										  cancelButtonColor: '#d33',
										  confirmButtonText: 'Continuar',
										  allowOutsideClick: false
										}).then(function () {
										//busco cuotas anteriores
											var successCallbackBco = function(data,responseHeaders){
												
												$scope.aclaracioncuotas = data;
												$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
												$scope.aclaracion.fechapago = $scope.fechaPagoString;
												angular.forEach($scope.aclaracioncuotas.candidatos, function (item) {
										            item.checked = true;
										        });
												if($scope.aclaracioncuotas.candidatos.length > 0){
													$scope.selectedCuotas();
													angular.element("#cuotasmorosasanteriores").modal();
													console.log(data);
												}else{
													swal('Atención'," No hay cuotas anteriores",'warning');
													$scope.resetForm();
												}
			
											}
											AclaracionResource.buscaCuotasBcoEstado($scope.aclaracion, successCallbackBco, $scope.errorCallback);
			
										})	
										
			
									}
									else{
										$scope.limpiaForm();
										$timeout(function () {$("#rutAfectadoAclaracion").focus();});
									}
								})
			
							}
							else{
								if($scope.aclaracion.codRetAcl == 10 && $scope.aclaracion.candidatos.length > 0){
									$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
									$scope.aclaracion.fechapago = $scope.fechaPagoString;
									$scope.aclaracionBase = angular.copy($scope.aclaracion) ;
									$scope.aclaracionBase.monto = parseInt($scope.aclaracion.monto);
									$scope.aclaracionBase.fechaprotesto = $scope.fechaProtestoString;

									$scope.aclaracionBase.fechaprotesto = $scope.fechaProtestoString.replace(pattern, '$3-$2-$1');

									$('#listaprotestosencontrados').modal();
			
								}
			
								else if($scope.aclaracion.codRetAcl == 10 && $scope.aclaracion.candidatos.length == 0){
									swal('Alerta',$scope.aclaracion.glosaRetAcl,'error');
			
									if(!angular.equals({}, $scope.bipersonal) && ($scope.bipersonal !== undefined && $scope.bipersonal !== null)){
			
										$scope.bipersonalAdd = angular.copy($scope.bipersonal);
										$scope.bipersonalError.push($scope.bipersonalAdd);
										$scope.aclaracion.bipersonales = $scope.bipersonalError;
			
									}else{
										$scope.bipersonal.rut = $scope.afectado.rut;
										$scope.bipersonal.correlativo = $scope.afectado.correlativo;
										$scope.bipersonalError.push($scope.bipersonal);
										$scope.aclaracion.bipersonales = $scope.bipersonalError;
			
									}
			
									$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
									$scope.aclaracion.fechapago = $scope.fechaPagoString;
									console.log($scope.bipersonalError);
			
									swal({
										  title: 'Alerta',
										  text: $scope.aclaracion.glosaRetAcl + "¿Desea agregar Bipersonales?",
										  type: 'warning',
										  showCancelButton: true,
										  confirmButtonColor: '#3085d6',
										  cancelButtonColor: '#d33',
										  confirmButtonText: 'SI',
										  cancelButtonText: 'NO',
										  allowOutsideClick: false
										}).then(function () {
											$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
											$scope.aclaracion.fechapago = $scope.fechaPagoString;
											$scope.disableBiper = true;
											$scope.biperCamposBloq = true;
											$scope.bipersonal = {};
											$scope.habilitadoBoton = false;
											$scope.biperCamposBloq = true;
											$scope.$digest();
											$("#rut_bi").focus();
										}, function (dismiss) {
										  if (dismiss === 'cancel') {
										  	$scope.resetForm();
										  }
										})
									}
			
									else{
										swal('Alerta',$scope.aclaracion.glosaRetAcl,'error');
											$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
											$scope.aclaracion.fechapago = $scope.fechaPagoString;
									}
							}
			
						}, function(error) {
							$scope.mensaje = "Se ha producido un error al obtener el dato:" + error;
							$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
							$scope.aclaracion.fechapago = $scope.fechaPagoString;
						});
			
			
					};
					AclaracionResource.aceptaAclaracion($scope.aclaracion, successCallback, $scope.errorCallback);
			}
			else{
				swal('Atención','El protesto ya se encuentra en el carro de compra','error');
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.aclaracion.fechapago = $scope.fechaPagoString;				
				return false	
			}

		}
		AclaracionResource.validaCarro($scope.aclaracion, successCallback, $scope.errorCallback);

	}



	$scope.cerrar = function(){

		swal({
			  title: 'Alerta',
			  text: "¿Desea salir de la pantalla de aclaración?. Se limpiará el formulario.",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'SI',
			  cancelButtonText: 'NO',
			  allowOutsideClick: false
			}).then(function () {
				$scope.listarServicioTramitante();
				$scope.afectado = {};
				$scope.aclaracion = {};
				$scope.aclaracion.sucursal = {};
				$scope.aclaracion.firmante = {};
				$scope.bipersonal ={};
				$scope.bipersonalError = [];

				$scope.numerofirmas = {};
				$scope.tab = 0;
				$scope.detalleFirma = {};
				$scope.listafirmas = [];
				$scope.listafirmantes = [];
				$scope.avanza = false;
				$scope.confirmatorio = false;
				$scope.habilitado = false;
				$scope.habilitadoBoton = false;
				$scope.numeroconfirmatorio = {};
				$scope.usafirma = 0;
				$scope.usaconfirmatorio = 0;
				$scope.disableSucursal = false;
				$scope.disableBiper = false;


				$scope.esvalido = false;
				$scope.editable = false;
				$scope.esrutjuridico = false;
				$scope.estadoHabi = false;
				$scope.docaclarar = {};
				$scope.bipersonal = {};
				$scope.cartera = {};
				$scope.botonAceptar = false;
				$scope.botonSiguiente = false;
				$scope.valueBack = 1;
				$scope.editableAcl = true;
				$scope.biperCamposBloq = false;
				$scope.active = 0;
				//$('#aclaracion').modal('hide');
				angular.element("#aclaracion").modal('hide');

				$scope.consultaAclaracionProceso();
				$scope.$digest();

			}, function (dismiss) {
			  if (dismiss === 'cancel') {
			  }
			})

	}


	$scope.consultaAclaracionProceso = function(){

		var successCallback = function(data,responseHeaders){
			if(data.idControl == 0){
				$rootScope.$emit('callEvent', data.lista, $scope.afectado);
				$scope.$digest();

			}
		};
		AclaracionResource.consultaAclaracionProceso({rutTramitante: $rootScope.tramitante.rut, corrCaja: $rootScope.usuario.caja}, successCallback, $scope.errorCallback);
	}


	$scope.resetForm = function(){

		$scope.afectado = {};
		if($scope.aclaracion===undefined){
			$scope.aclaracion = {};
			//$scope.habilitado = false;
			$scope.botonSiguiente = false;
		}
		if($scope.usafirma != 1) $scope.usafirma = 0;
		if($scope.usaconfirmatorio != 1) $scope.usaconfirmatorio = 0;
		$scope.aclaracion.sucursal = {};
		$scope.aclaracion.firmante = {};
		$scope.aclaracion.numoperacion = "";
		$scope.aclaracion.tipomoneda = {};
		$scope.aclaracion.monto = "";
		$scope.aclaracion.fechaprotesto = {};
		$scope.aclaracion.bipersonales = [];
		$scope.bipersonalError = [];
		
		$scope.numerofirmas = {};
		$scope.tab = 0;
		$scope.detalleFirma = {};
		$scope.listafirmas = [];
		$scope.listafirmantes = [];
		$scope.avanza = true;
		$scope.confirmatorio = false;
		$scope.habilitado = false;
		$scope.habilitadoBoton = false;
		$scope.numeroconfirmatorio = {};
		
		
		$scope.disableSucursal = false;
		$scope.disableBiper = false;


		$scope.esvalido = false;
		$scope.editable = false;
		$scope.esrutjuridico = false;
		$scope.estadoHabi = false;
		$scope.docaclarar = {};
		$scope.bipersonal = {};
		$scope.cartera = {};
		$scope.botonAceptar = false;
		$scope.botonSiguiente = true;
		$scope.valueBack = 1;
		$scope.editableAcl = true;
		$scope.biperCamposBloq = false;
		$scope.active = 0;
		//$scope.$digest();

        $timeout(function () {$("#rutAfectadoAclaracion").focus();});

	}
	
	$scope.resetForm2 = function(){

		$scope.afectado = {};
		$scope.aclaracion = {};
		
		$scope.numerofirmas = {};
		$scope.tab = 0;
		$scope.detalleFirma = {};
		$scope.listafirmas = [];
		$scope.listafirmantes = [];
		$scope.avanza = true;
		$scope.confirmatorio = false;
		$scope.habilitado = false;
		$scope.habilitadoBoton = false;
		$scope.numeroconfirmatorio = {};
		$scope.usafirma = 0;
		$scope.usaconfirmatorio = 0;
		$scope.disableSucursal = false;
		$scope.disableBiper = false;


		$scope.esvalido = false;
		$scope.editable = false;
		$scope.esrutjuridico = false;
		$scope.estadoHabi = false;
		$scope.docaclarar = {};
		$scope.bipersonal = {};
		$scope.bipersonalError = [];
		$scope.cartera = {};
		$scope.botonAceptar = false;
		$scope.botonSiguiente = false;
		$scope.valueBack = 1;
		$scope.editableAcl = true;
		$scope.biperCamposBloq = false;
		$scope.active = 0;
		//$scope.$digest();

        $timeout(function () {$("#rutAfectadoAclaracion").focus();});

	}

    $scope.abrirModal = function(elemento){
        var modalInstance = $uibModal.open({
          templateUrl: 'views/modal/modalloginsupervisor.html',
          controller: 'ModalLoginSupervisorController',
          appendTo: angular.element($document[0].querySelector('#imgfirma')),
          size: 'sm'
        });
        modalInstance.result.then(function (respuesta) {
            if(respuesta){
                console.log(respuesta);
                if(elemento != null){
                	$scope.listafirmantes.push(elemento);
                	$scope.cerrarImg();
                	return true;
                }

            }
            if(!respuesta){
				$scope.cerrarImg();
				return false;
            }
        });
    }
    
    $scope.abrirModalTenedor = function(elemento){
        var modalInstance = $uibModal.open({
          templateUrl: 'views/modal/modalloginsupervisor.html',
          controller: 'ModalLoginSupervisorController',
          appendTo: angular.element($document[0].querySelector('#imgfirma')),
          size: 'sm'
        });
        modalInstance.result.then(function (respuesta) {
            if(respuesta){
                console.log(respuesta);
                if(elemento != null){
                	
                	if($scope.listafirmantes.length > 0){
    				    //$scope.existe = $scope.listafirmantes.filter((firmante) => firmante.firmante.rut !== $scope.firmanteAdd.firmante.rut)[0];
    				    //$scope.existe = $filter('filter')($scope.listafirmantes, {checked: true});
    	
    				    $scope.ruts = $filter('filter')($scope.listafirmantes, { firmante: $scope.firmanteAdd.firmante });
    	
    				    if($scope.ruts.length == 0){
    						$scope.listafirmantes.push($scope.firmanteAdd);
    						$scope.cerrarImg();
    					}
    					else{
    						swal('Alerta','Firma ya agregada','error');
    					}
    				}
    				else{
    					
    					if($scope.aclaracion.docpresentada.codigo == "CT"){
    						//valido si requiero numero confirmatorio
    						$scope.numerofirmas.tipoEmisor = $scope.firmanteAdd.tipoEmisorAux; //firmante
    						$scope.numerofirmas.codEmisor = $scope.firmanteAdd.codEmisorAux; //firmante
    						$scope.numerofirmas.tipoDocAclaracion = $scope.aclaracion.docpresentada.codigo; //firmante
    						$scope.numerofirmas.tipoDocumento = $scope.aclaracion.tipodocumento.codigo; //firmante
    						
    						console.log("numerofirmas.tipoEmisor:"+$scope.numerofirmas.tipoEmisor);
    						console.log("numerofirmas.codEmisor:"+$scope.numerofirmas.codEmisor);
    						console.log("numerofirmas.tipoDocAclaracion:"+$scope.numerofirmas.tipoDocAclaracion);
    						console.log("numerofirmas.tipoDocumento:"+$scope.numerofirmas.tipoDocumento);
    						
    						var successCallback = function(data,responseHeaders){
    							if(data != null){
    								$scope.numerofirmas = data;
    								$scope.tab = 1;
    								//$scope.usafirma = $scope.numerofirmas.flagFirmaNroConf;
    								$scope.usafirma = 1;
    								$scope.usaconfirmatorio = $scope.numerofirmas.usaNrosConfirmatorios;
    								if($scope.aclaracion.docpresentada.codigo == "CT"){
    									$scope.disableSucursal = true;
    								}
    								else{
    									$scope.disableSucursal = false;
    								}
    								console.log($scope.numerofirmas);
    							}
    						};
    	
    						AclaracionResource.reqConfirmatorio($scope.numerofirmas ,successCallback, $scope.errorCallback); //firmante
    	
    					}
    					
    					$scope.listafirmantes.push($scope.firmanteAdd);
    					$scope.cerrarImg();
    				}

                	return true;
                }

            }
            if(!respuesta){
				$scope.cerrarImg();
				return false;
            }
        });
    }








	$scope.eliminarTramitante = function(rut){
		console.log(rut);
	}

	$scope.guardarServicio = function(){
		$scope.transaccion = {
			codServicio : 'ACL',
			rutAfectado : $scope.afectado.rut,
			corrNombre: $scope.afectado.corrNombre
		}
		var successCallback = function(data,responseHeaders){
			$scope.listarServicioTramitante();
			//$('#informecertificado').modal('hide');

		};
        TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
	}

	$scope.selCandidato = function(correlativoPro){

		if(correlativoPro === undefined || correlativoPro === null){
			swal('Atención','No se han seleccionado protesto(s)','error');
			return false
		}
		else{

				$scope.candidatosel = $filter('filter')($scope.aclaracion.candidatos, {corrProt: correlativoPro})[0];
				if($scope.candidatosel.marcaAclaracion > 0){
						swal('Atención','El protesto seleccionado ya fue aclarado','error');
						return false
				}
				swal({
					  title: 'Atención',
					  text: "El protesto seleccionado tiene diferencias con respecto a los datos ingresados.¿Desea continuar?.",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'SI',
					  cancelButtonText: 'NO',
					  allowOutsideClick: false
					}).then(function () {


							//$scope.candidatosel = $scope.aclaracion.candidatos.filter((subject) => subject.rut !== $rootScope.tramitante.rut)[0];
							$scope.candidatosel = $filter('filter')($scope.aclaracion.candidatos, {corrProt: correlativoPro})[0];

							$scope.candidatoSeleccionado = $scope.aclaracion;
							//$scope.candidatoParse  = JSON.parse($scope.candidatosel);

							var pattern = /(\d{2})(\d{2})(\d{4})/;
							if($scope.candidatosel.fechaprotesto !== undefined){
								$scope.fechaprotesto = new Date($scope.candidatosel.fechaprotesto.replace(pattern, '$3-$2-$1'));
								$scope.candidatoSeleccionado.fechaprotesto = $scope.fechaprotesto;
							}
							if(!angular.isUndefined($scope.candidatosel.fechapago) && $scope.candidatosel.fechapago !== null ){
								$scope.fechapago = new Date($scope.candidatosel.replace(pattern, '$3-$2-$1'));
								$scope.candidatoSeleccionado.fechapago = $scope.fechapago;
							}

							$scope.candidatoSeleccionado.corrProt = $scope.candidatosel.corrProt;
							$scope.candidatoSeleccionado.nroBoletin = $scope.candidatosel.nroBoletin;
							$scope.candidatoSeleccionado.pagBoletin = $scope.candidatosel.pagBoletin;
							$scope.candidatoSeleccionado.costoAclaracion = $scope.candidatosel.costoAclaracion;
							$scope.candidatoSeleccionado.nombreLibrador = $scope.candidatosel.nombreLibrador;
							$scope.candidatoSeleccionado.codLocalidad = $scope.candidatosel.codLocalidad;
							$scope.candidatoSeleccionado.corrProt = $scope.candidatosel.corrProt;
							$scope.candidatoSeleccionado.codEstado = $scope.candidatosel.codEstado;
							$scope.candidatoSeleccionado.ptjePareo = $scope.candidatosel.ptjePareo;
							$scope.candidatoSeleccionado.tipomoneda.codMoneda = $scope.candidatosel.tipomoneda.codMoneda;
							$scope.candidatoSeleccionado.tipoDocumentoImpago = $scope.candidatosel.tipoDocumentoImpago;
							$scope.candidatoSeleccionado.fecPublicacion = $scope.candidatosel.fecPublicacion;
							$scope.candidatoSeleccionado.marcaAclaracion = $scope.candidatosel.marcaAclaracion;
							$scope.candidatoSeleccionado.tipoPareo = 2;
							$scope.candidatoSeleccionado.criterio = 2;

							var promise = $scope.tieneVentaCartera($scope.candidatoSeleccionado);
							promise.then(function(resultado) {

								$scope.aclaracion = resultado;

								if($scope.aclaracion.ventaCartera.flagVentaCartera == 1 && $scope.aclaracion.emisor.codigo == 12){

									swal('Alerta',$scope.aclaracion.ventaCartera.mensaje,'warning');
									$scope.tipoProtesto = "CAND";
									if($scope.aclaracion.ventaCartera.flagDespliegue == 1 ){
										angular.element("#datosventacartera").modal();
									}
									return false;
								}
								else{
									$scope.aclaraCandidato($scope.candidatoSeleccionado);
								}
							}, function(error) {
									swal('Atención',error,'error');
							});
									
					}, function (dismiss) {
					  if (dismiss === 'cancel') {

					  }
					})

				}
			

	}


	$scope.aclaraCandidato = function(candidato){


		var successCallback = function(data,responseHeaders){
			if(data.idControl == 0){
				swal('Alerta',"Protesto fue aclarado",'success');
				$('#listaprotestosencontrados').modal("hide");

				swal({
				  title: 'Atención',
				  text: "Protesto fue aclarado",
				  type: 'success',
				  showCancelButton: false,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Aceptar',
				  allowOutsideClick: false

				}).then(function (){
					if(data.codRetAcl == 0 && data.tipoPareo != 4 && data.cantFilas == 1 && data.emisor.codigo == 12  && $scope.aclaracion.tipodocumento.codigo == "CM"){
						

						swal({
							  title: 'Atención',
							  text: "Se buscaran cuotas anteriores del mismo crédito para ser Aclaradas",
							  type: 'warning',
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'Continuar',
							  allowOutsideClick: false
							}).then(function () {
								 var aux = data.fechaprotesto.split(/[- : T]/);
								 data.fechaprotesto = aux[2]+aux[1]+aux[0];

								//busco cuotas anteriores
								var successCallbackBco = function(data,responseHeaders){
									$scope.aclaracioncuotas = data;
									$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
									$scope.aclaracion.fechapago = $scope.fechaPagoString;
									angular.forEach($scope.aclaracioncuotas.candidatos, function (item) {
							            item.checked = true;
							        });
									if($scope.aclaracioncuotas.candidatos.length > 0){
										$scope.selectedCuotas();
										angular.element("#cuotasmorosasanteriores").modal();
										console.log(data);
									}else{
										swal('Atención'," No hay cuotas anteriores",'warning');
									}

								}
								AclaracionResource.buscaCuotasBcoEstado(data, successCallbackBco, $scope.errorCallback);

							})						


					}
					else{
						$scope.limpiaForm();
					}					
				})
			}
		};
		AclaracionResource.aclaraCandidato(candidato, successCallback, $scope.errorCallback);
	}


	$scope.aclaraCuotas = function(){

		$scope.aclaracion.candidatos = $scope.cuotasel;
		
		if($scope.aclaracion.candidatos.length > 0){
			
			var successCallback = function(data,responseHeaders){
				if(data.idControl == 0){
					swal('Alerta',"Aclaración Terminada",'success');
					angular.element("#cuotasmorosasanteriores").modal("hide");
					$scope.limpiaForm();
				}
			};
			AclaracionResource.aclaraCuotasBcoEstado($scope.aclaracion, successCallback, $scope.errorCallback);
			
		}
		else{
			swal({
				  title: 'Alerta',
				  text: "Seleccione cuotas para continuar",
				  type: 'warning',
				  showCancelButton: false,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Aceptar',
				  allowOutsideClick: false
				}).then(function () {
				})

		}

	}

	$scope.tieneVentaCartera = function(aclaracionSel){
		console.log("tieneVentaCartera");

		var defered = $q.defer();
      	var promise = defered.promise;

		var successCallback = function(data,responseHeaders){
			console.log(data);
			defered.resolve(data);

		}
		AclaracionResource.tieneVentaCartera(aclaracionSel, successCallback, $scope.errorCallback);
	    return promise;
	}


	$scope.validaVentaCartera = function(protesto, tipoProtesto){

		var successCallback = function(data,responseHeaders){
			if(data.flagVentaCartera == 1){
				swal('Alerta',data.mensaje,'warning');
				swal({
					  title: 'Alerta',
					  html: data.mensaje + "<br />" + "¿Desea consultar nuevamente? Verifique los datos ingresados",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'SI',
					  cancelButtonText: 'NO',
					  allowOutsideClick: false
					}).then(function () {

					}, function (dismiss) {
					  if (dismiss === 'cancel') {
					  	//borrar aclaracion
						var successCallback = function(data,responseHeaders){
								console.log(data);
								if(data.idControl == 0){
									swal('Alerta',"Eliminación Exitosa",'warning');
									angular.element("#datosventacartera").modal("hide");
									$scope.limpiaForm();
								}
								else{
									swal('Alerta',data.mensaje,'warning');
								}
							}
					  	AclaracionResource.eliminaProtestoTemp({corrProt: protesto.corrProt, corrCaja: protesto.corrcaja} ,successCallback, $scope.errorCallback);
					  }
					})

			}
			else if(data.flagVentaCartera == 0 ){
				//swal('Alerta',"Validación Exitosa a Protesto con Venta de Cartera.",'success');
				swal({
					  title: 'Operación Exitosa',
					  text: "Validación Exitosa a Protesto con Venta de Cartera.",
					  type: 'success',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Continuar',
					  allowOutsideClick: false
					}).then(function () {
						if(tipoProtesto === "CAND"){
							$scope.aclaraCandidato(protesto);
						}
						$scope.limpiaForm();
						angular.element("#datosventacartera").modal("hide");
					})

				//$scope.cerrarCartera();

			}
		};
		AclaracionResource.validaVentaCartera({tipoEmisor: $scope.cartera.tipoemisor.codigo,
										    codEmisor: $scope.cartera.emisor.codigo,
										    corrProt: protesto.corrProt}, successCallback, $scope.errorCallback);
	}



	$scope.eliminaAclaracion = function(corrProt){

		var successCallback = function(data,responseHeaders){
			console.log(data);
			if(data.idControl == 0){
				swal('Alerta',"Eliminación Exitosa",'warning');
				angular.element("#datosventacartera").modal("hide");
				$scope.limpiaForm();
			}
			else{
				swal('Alerta',data.mensaje,'warning');
			}
		}
		AclaracionResource.eliminaProtestoTemp({corrProt: corrProt, corrCaja: $rootScope.usuario.caja} ,successCallback, $scope.errorCallback);
	}

	$scope.cerrarCartera = function(protesto, tipoProtesto){

		//borrar aclaracion
		swal({
			  title: 'Alerta',
			  html: "<br />" + "¿Desea reingresar datos de Venta de Cartera? Presione: SI: para corregir, NO: para eliminar transacción",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'SI',
			  cancelButtonText: 'NO',
			  allowOutsideClick: false
			}).then(function () {
			 	angular.element("#datosventacartera").modal("hide");
			}, function (dismiss) {
			  if (dismiss === 'cancel') {
			  	//borrar aclaracion
				var successCallback = function(data,responseHeaders){
						console.log(data);
						if(data.idControl == 0){
							swal('Alerta',"Eliminación Exitosa",'warning');
							angular.element("#datosventacartera").modal("hide");
							$scope.limpiaForm();
						}
						else{
							swal('Alerta',data.mensaje,'warning');
						}
					}
			  	AclaracionResource.eliminaProtestoTemp({corrProt: protesto.corrProt, corrCaja: protesto.corrcaja} ,successCallback, $scope.errorCallback);
			  }
			})

	}


    $scope.aclaraBipersonal = function(){

    	console.log($scope.bipersonal);
    	if(angular.equals({}, $scope.bipersonal)){
    		swal('Alerta','Ingrese datos bipersonal','error');
    		$('#rut_bi').focus();
    		return false;
    	}
    	if($scope.bipersonal.rut === undefined || $scope.bipersonal.rut == null || $scope.bipersonal.rut == ""){
    		swal('Alerta','Ingrese rut bipersonal','error');
    		$('#rut_bi').focus();
    		return false;
    	}
    	if(!$scope.esrutjuridico && ($scope.bipersonal.apellidoPaterno === undefined || $scope.bipersonal.apellidoPaterno == "")){
    		swal('Alerta','Ingrese apellido paterno bipersonal','error');
    		$('#ap_pat_bi').focus();
    		return false;
    	}
    	if($scope.bipersonal.nombres === undefined || $scope.bipersonal.nombres == ""){
    		swal('Alerta','Ingrese nombre bipersonal','error');
    		$('#nombre_bi').focus();
    		return false;
    	}
    	$scope.aceptaAclaracion();
    }


	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);
	$scope.esrutjuridico = false;

    $scope.esRutJuridico = function () {
		var rutNumerico	= $scope.bipersonal.rut==undefined ?  0 :  parseInt($scope.bipersonal.rut.slice(0, $scope.bipersonal.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};

	$scope.obtenerBipersonal = function(){

		var res = $scope.afectado.rut.replace(".", "");
		res = res.replace("-", "");
		res = res.replace(/^0+/, '');
		if(res == $scope.bipersonal.rut){
			swal('Alerta','El afectado y el bipersonal no pueden ser el mismo','error');
			$scope.bipersonal = {};
			return false;
		}



		if(!angular.equals({}, $scope.bipersonal)){
			 $scope.esRutJuridico();
			if($scope.bipersonal.rut !== undefined && $scope.bipersonal.rut !== null && $scope.bipersonal.rut !== "" ){
				var successCallback = function(data,responseHeaders){
		            $scope.esvalido = true;
					if(data.correlativo == "0"){
		                $scope.editable = true;
		                swal('Alerta','Persona no encontrada','error');

					} else {
		                $scope.editable = false;
						if($scope.esrutjuridico){
							$scope.bipersonal.nombres = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
							$scope.bipersonal.correlativo   = data.correlativo;
						}else{
							$scope.bipersonal.apellidoPaterno = data.apellidoPaterno;
							$scope.bipersonal.apellidoMaterno = data.apellidoMaterno;
							$scope.bipersonal.nombres 		= data.nombres;
							$scope.bipersonal.correlativo   = data.correlativo;
						}
						$('#ap_pat_bi').focus();
					}
				};
		        PersonaResource.obtenerPersona($scope.bipersonal, successCallback, $scope.errorCallback);
		    }
			else{
				swal('Alerta','Rut Invalido','error');
				$scope.bipersonal = {};
			}
		}
	};

	$scope.setEstadoCuota = function(){

		if($scope.aclaracion.tipodocumento.codigo == "CM"){
			$scope.aclaracion.estadocuota = "1";
		}else{
			$scope.aclaracion.estadocuota = "0";
		}
	}




	$scope.selectedCuotas = function () {

       $scope.cuotasel = $filter('filter')($scope.aclaracioncuotas.candidatos, {checked: true});
	}

	$scope.cerrarCandidatos = function(){


		swal({
			  title: 'Alerta',
			  text: $scope.aclaracion.glosaRetAcl + "¿Desea agregar Bipersonales?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'SI',
			  cancelButtonText: 'NO',
			  allowOutsideClick: false
			}).then(function () {

				angular.element("#listaprotestosencontrados").modal("hide");
				$scope.aclaracion.fechaprotesto  =  $scope.fechaProtestoString;
				$scope.disableBiper = true;
				$scope.biperCamposBloq = true;
				$scope.bipersonal = {};
				$scope.habilitadoBoton = false;
				$scope.$digest();
				$("#rut_bi").focus();
			}, function (dismiss) {
			  if (dismiss === 'cancel') {
			  	angular.element("#listaprotestosencontrados").modal("hide");
			  	$scope.resetForm();
			  }
			})
	}




	$scope.cerrarCuotasBco = function(){
		angular.element("#cuotasmorosasanteriores").modal("hide");
		$scope.resetForm();
	}


	$scope.limpiaForm = function(){

		if($scope.aclaracion.docpresentada.codigo === "CT" || $scope.aclaracion.docpresentada.codigo === "CE" ){
			swal({
				  title: 'Atención',
				  text: "¿Desea ingresar otro documento para el mismo certificado?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'SI',
				  cancelButtonText: 'No',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  allowOutsideClick: false
				}).then(function () {
					
					$scope.aclaracion.sucursal = {};
					//$scope.aclaracion.firmante = {};
					$scope.aclaracion.numoperacion = "";
					$scope.aclaracion.tipomoneda = {};
					$scope.aclaracion.monto = "";
					$scope.aclaracion.fechaprotesto = "";
					//$scope.aclaracion.estadocuota = "";
					$scope.aclaracion.fechapago = null;
					$scope.aclaracion.numboletin = undefined;
					$scope.bipersonal = {};
					$scope.disableBiper = false;
					$scope.bipersonal = {};
					$scope.habilitadoBoton = true;
					$scope.estadoHabi = true;
					$scope.biperCamposBloq = false;
					
					$scope.setEstadoCuota();
					$scope.$digest();
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
						$scope.resetForm2();
				  }
			})
		}else{

			$scope.resetForm();

		}
	}

	$scope.isValidDate =  function(date, tipofecha) {

		var dateParts = date.split('/');
		var dateString = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]
	    var d = new Date(dateString);

	    var fechaIni = new Date("01-01-1900");
	    if(d < fechaIni){
	    	if(tipofecha == "protesto"){
	    		swal('Alerta','Fecha Protesto invalida','error');
 				$scope.aclaracion.fechaprotesto = "";
 				$("#fecha_prot_acl").focus();	    		
	    		return false;
	    	}
	    	else if(tipofecha == "pago"){
	    		swal('Alerta','Fecha de Pago invalida','error');
 				$scope.aclaracion.fechapago = "";
 				$("#fecha_pago_acl").focus();	    		
	    		return false;
	    	}
	    }
	    else{

	    $scope.current = new Date();
	    if(d > $scope.current){
	    	if(tipofecha == "protesto"){
	    		swal('Alerta','Fecha Protesto no puede ser mayor a la fecha actual','error');
 				$scope.aclaracion.fechaprotesto = "";
 				$("#fecha_prot_acl").focus();	    		
	    	}
	    	else if(tipofecha == "pago"){
	    		swal('Alerta','Fecha de Pago no puede ser mayor a la fecha actual','error');
 				$scope.aclaracion.fechapago = "";
 				$("#fecha_pago_acl").focus();	    		
	    	}
	    }
	    return (d &&  d.getDate() == Number(dateParts[0])  && (d.getMonth() + 1) == dateParts[1] && d.getFullYear() == Number(dateParts[2]));
		}
	}



	$scope.esfecha = function(fecha, tipofecha){

		if(fecha !== ""){
			dateArray = fecha.split("");
	        var fechaString = dateArray[0] + dateArray[1] + "/" + dateArray[2] + dateArray[3] + "/" + dateArray[4] + dateArray[5] + dateArray[6] + dateArray[7];

	 		var valor = $scope.isValidDate(fechaString, tipofecha);
	 		if(!valor){

	 			if(tipofecha == "protesto"){
	 				$scope.aclaracion.fechaprotesto = "";
	 				swal('Alerta','Fecha Protesto Invalida','error');
	 				$("#fecha_prot_acl").focus();
	 			}
	 			else if(tipofecha == "pago"){
					$scope.aclaracion.fechapago = "";
					swal('Alerta','Fecha Pago Invalida','error');
					$("#fecha_pago_acl").focus();
	 			}

	 		}
	 	}

	}

	$rootScope.$on("LimpiarAfectado", function(event, data) {
		 $scope.resetForm();
		 rutSearch = '';
    });

	$scope.tipoMonedas();
	$scope.tipoDocumentos();
	$("rutAfectadoAclaracion").focus();



	$scope.validaDecimal = function(valor){

		$scope.floatValue = valor.replace(",", '');
		$scope.floatValue = parseFloat($scope.floatValue);

		$scope.floatValue  = $filter('currency')($scope.floatValue, "");


		var monto = $scope.floatValue.split(".");
		if(monto[1].length > 2){
			swal('Alerta','Monto invalido, maximo 2 decimales','error');
			$scope.aclaracion.monto = "";
			$("#monto_acl").focus();
		}else{
			$scope.montoString = $scope.floatValue.toString()
			$scope.aclaracion.monto = $scope.montoString; //.replace(/,/g , "");
			//$scope.aclaracion.monto = $filter('currency')($scope.aclaracion.monto);
		}
	
	}


    $scope.quitarFirma = function(index) {


      		swal({
			  title: 'Alerta',
			  text: "¿Desea eliminar la firma seleccionada?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'SI',
			  cancelButtonText: 'NO',
			  allowOutsideClick: false
			}).then(function () {
					$scope.listafirmantes.splice(index, 1);
					$scope.$digest();

			}, function (dismiss) {
			  if (dismiss === 'cancel') {
			  }
			})



    };

    $scope.limpiaFirmantes = function(){
    	$scope.listafirmantes = [];
    }

    $("#aclaracion").on('shown.bs.modal', function () {
		$("#rutAfectadoAclaracion").focus();
	});
	/**
	$document.on('keydown', function(e){
	      if(e.which === 8 && ( e.target.nodeName === "INPUT" || e.target.nodeName === "SELECT" ) ){ // you can add others here inside brackets.
	          e.preventDefault();
	      }
	});
	**/
});
