angular.module('ccs').controller('CentroCostoController', function($rootScope, $scope, CentroCostoResource, $location, $timeout) {

	$scope.centroCosto = {};
	$scope.centros = [];
	$scope.solo1CC = false;

	$("#centrocosto").on('shown.bs.modal', function () {
		console.log("-> Seleccionar CC..");
		$timeout(function(){
			init();
		});
	});

	function init() {
		$scope.centros = [];
		$scope.listar();
		$scope.centroCosto = {};
		$scope.centroCosto.model = $scope.usuario.centroCostoActivo;
	}
	
    $scope.listar = function() {
        var successCallback = function(data,responseHeaders){
        	console.log("> listar data:");
        	console.log(data);
            $scope.centros = data;
            if ( $scope.centros.length == 1 ) {
            	$scope.solo1CC = true;
            }
        };
        CentroCostoResource.listar( successCallback, $scope.errorCallback);
    };

    $scope.modificarCentroCosto = function() {
    	
    	if($rootScope.transacciones.length == 0){
            if($scope.centroCosto.model.codigo == $rootScope.usuario.centroCostoActivo.codigo){
                swal({ title: 'Error', text: 'Centro de Costo seleccionado, ya se encuentra en estado Activo',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
                    function () { $("#centrocosto .ui-select-focusser").focus(); });
            }else{
				swal({
					title: 'Alerta',
					text: 'Confirmar cambio centro de Costo',
					type: 'question',
					confirmButtonText: 'Aceptar',
					showCancelButton: true,
					cancelButtonText: 'Cancelar'
				}).then( function() {
						modificarCC();
					}
				);
            }
        }
        else{
            swal('Alerta','Para cambiar de centro de costo debe finiquitar transacciones pendientes.','error');
            return false;  
        }
    };

	function modificarCC() {
		var successCallback = function(data, responseHeaders) {
			if ( data.idControl == 0) {
				var centroCosto_aux = {};
				centroCosto_aux.codigo = $scope.centroCosto.model.codigo;
				centroCosto_aux.descripcion = $scope.centroCosto.model.descripcion;
				$rootScope.usuario.caja = 0;
				$rootScope.usuario.esAgencia = parseInt($scope.centroCosto.model.codigo) > 2300 && parseInt($scope.centroCosto.model.codigo) < 3000;
				$scope.setCentroCosto( centroCosto_aux );
				$("#centrocosto").modal("hide");
				$location.path("/inicio");
			} else {
				console.log("NOOK");
			}
		};
		CentroCostoResource.modificar($scope.centroCosto.model, successCallback, $scope.errorCallback);
	}

    $("#centrocosto").on('hidden.bs.modal', function () {
        $scope.centroCosto = {};
        $scope.centros = [];
        $scope.solo1CC = false;
        $scope.$apply();
    });

    $scope.seleccionarCentroCosto = function(){
        if($scope.centroCosto.model.codigo != $rootScope.usuario.centroCostoActivo.codigo){
            $scope.$broadcast("btnAceptarCambiarCentroCosto");
        }else{
            $scope.$broadcast("btnCancelarCambiarCentroCosto");
        }
    };



});
