package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BolLabInfractores database table.
 * 
 */
@Entity
@Table(name="Tbl_BolLabInfractores")
@NamedQuery(name="Tbl_BolLabInfractore.findAll", query="SELECT t FROM Tbl_BolLabInfractore t")
public class Tbl_BolLabInfractore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_MontoDeudaLaboral")
	private BigDecimal fld_MontoDeudaLaboral;

	@Column(name="Fld_MontoMultaLaboral")
	private BigDecimal fld_MontoMultaLaboral;

	@Column(name="Fld_NombreInfractor")
	private String fld_NombreInfractor;

	@Column(name="Fld_NroDeudasLaboral")
	private int fld_NroDeudasLaboral;

	@Column(name="Fld_NroMultasLaboral")
	private int fld_NroMultasLaboral;

	@Column(name="Fld_RutInfractor")
	private String fld_RutInfractor;

	public Tbl_BolLabInfractore() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public BigDecimal getFld_MontoDeudaLaboral() {
		return this.fld_MontoDeudaLaboral;
	}

	public void setFld_MontoDeudaLaboral(BigDecimal fld_MontoDeudaLaboral) {
		this.fld_MontoDeudaLaboral = fld_MontoDeudaLaboral;
	}

	public BigDecimal getFld_MontoMultaLaboral() {
		return this.fld_MontoMultaLaboral;
	}

	public void setFld_MontoMultaLaboral(BigDecimal fld_MontoMultaLaboral) {
		this.fld_MontoMultaLaboral = fld_MontoMultaLaboral;
	}

	public String getFld_NombreInfractor() {
		return this.fld_NombreInfractor;
	}

	public void setFld_NombreInfractor(String fld_NombreInfractor) {
		this.fld_NombreInfractor = fld_NombreInfractor;
	}

	public int getFld_NroDeudasLaboral() {
		return this.fld_NroDeudasLaboral;
	}

	public void setFld_NroDeudasLaboral(int fld_NroDeudasLaboral) {
		this.fld_NroDeudasLaboral = fld_NroDeudasLaboral;
	}

	public int getFld_NroMultasLaboral() {
		return this.fld_NroMultasLaboral;
	}

	public void setFld_NroMultasLaboral(int fld_NroMultasLaboral) {
		this.fld_NroMultasLaboral = fld_NroMultasLaboral;
	}

	public String getFld_RutInfractor() {
		return this.fld_RutInfractor;
	}

	public void setFld_RutInfractor(String fld_RutInfractor) {
		this.fld_RutInfractor = fld_RutInfractor;
	}

}