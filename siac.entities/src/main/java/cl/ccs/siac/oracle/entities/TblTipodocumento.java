package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_TIPODOCUMENTO database table.
 * 
 */
@Entity
@Table(name="TBL_TIPODOCUMENTO")
@NamedQuery(name="TblTipodocumento.findAll", query="SELECT t FROM TblTipodocumento t")
public class TblTipodocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPODOCUMENTO_FLDTIPODOCUMENTO_GENERATOR", sequenceName="FLD_TIPODOCUMENTO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPODOCUMENTO_FLDTIPODOCUMENTO_GENERATOR")
	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATIPODOCUMENTO")
	private String fldGlosatipodocumento;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblTipodocumento() {
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatipodocumento() {
		return this.fldGlosatipodocumento;
	}

	public void setFldGlosatipodocumento(String fldGlosatipodocumento) {
		this.fldGlosatipodocumento = fldGlosatipodocumento;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}