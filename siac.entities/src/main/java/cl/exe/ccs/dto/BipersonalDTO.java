package cl.exe.ccs.dto;

import java.io.Serializable;

public class BipersonalDTO extends ResponseDTO implements Serializable{
	
	private String rut;
	private Long corrAclTemp;
	private Long correlativo;
	
	

	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public Long getCorrAclTemp() {
		return corrAclTemp;
	}
	public void setCorrAclTemp(Long corrAclTemp) {
		this.corrAclTemp = corrAclTemp;
	}
	public Long getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Long correlativo) {
		this.correlativo = correlativo;
	}

	
	
	
	

}
