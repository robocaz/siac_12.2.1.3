package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProtestoAclaracionDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = -6466135674013074112L;

	private List<String[]> lista;
	
	private String fecDigitacion;
	private BigDecimal corrProt;
	private BigDecimal corrCaja;
	private String codUsuario;
	private String fecPago;
	private Integer codCCosto;
	private String glosaCCosto;
	private String rutTramitante;
	private String glosaTipoDocAclaracion;
	
	public List<String[]> getLista() {
		return lista;
	}

	public void setLista(List<String[]> lista) {
		this.lista = lista;
	}

	public String getFecDigitacion() {
		return fecDigitacion;
	}

	public void setFecDigitacion(String fecDigitacion) {
		this.fecDigitacion = fecDigitacion;
	}

	public BigDecimal getCorrProt() {
		return corrProt;
	}

	public void setCorrProt(BigDecimal corrProt) {
		this.corrProt = corrProt;
	}

	public BigDecimal getCorrCaja() {
		return corrCaja;
	}

	public void setCorrCaja(BigDecimal corrCaja) {
		this.corrCaja = corrCaja;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getFecPago() {
		return fecPago;
	}

	public void setFecPago(String fecPago) {
		this.fecPago = fecPago;
	}

	public Integer getCodCCosto() {
		return codCCosto;
	}

	public void setCodCCosto(Integer codCCosto) {
		this.codCCosto = codCCosto;
	}

	public String getGlosaCCosto() {
		return glosaCCosto;
	}

	public void setGlosaCCosto(String glosaCCosto) {
		this.glosaCCosto = glosaCCosto;
	}

	public String getRutTramitante() {
		return rutTramitante;
	}

	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}

	public String getGlosaTipoDocAclaracion() {
		return glosaTipoDocAclaracion;
	}

	public void setGlosaTipoDocAclaracion(String glosaTipoDocAclaracion) {
		this.glosaTipoDocAclaracion = glosaTipoDocAclaracion;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n fecDigitacion: " + fecDigitacion);
		str.append(", corrProt: " + corrProt);
		str.append(", corrCaja: " + corrCaja);
		str.append(", codUsuario: " + codUsuario);
		str.append(", fecPago: " + fecPago);
		str.append(", codCCosto: " + codCCosto);
		str.append(", glosaCCosto: " + glosaCCosto);
		str.append(", rutTramitante: " + rutTramitante);
		str.append(", glosaTipoDocAclaracion: " + glosaTipoDocAclaracion);
		str.append("\n lista: " + lista);
		return str.toString();
	}

}
