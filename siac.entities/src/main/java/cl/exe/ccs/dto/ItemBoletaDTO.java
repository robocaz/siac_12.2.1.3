package cl.exe.ccs.dto;

import java.io.Serializable;

public class ItemBoletaDTO implements Serializable{

	private Integer id;
	private Integer corrCaja;
	private String tipoDocumento;
	private String estado;
	private Integer numBoletaFactura;
	private String tipoPago;
	private String numeroCheque;
	private Double montoCobradoSistema;
	private Double montoIngresadoCajera;
	private Double montoDiferencia;
	private String rutAfectado;
	private String rutTramitante;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getNumBoletaFactura() {
		return numBoletaFactura;
	}
	public void setNumBoletaFactura(Integer numBoletaFactura) {
		this.numBoletaFactura = numBoletaFactura;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getNumeroCheque() {
		return numeroCheque;
	}
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	public Double getMontoCobradoSistema() {
		return montoCobradoSistema;
	}
	public void setMontoCobradoSistema(Double montoCobradoSistema) {
		this.montoCobradoSistema = montoCobradoSistema;
	}
	public Double getMontoIngresadoCajera() {
		return montoIngresadoCajera;
	}
	public void setMontoIngresadoCajera(Double montoIngresadoCajera) {
		this.montoIngresadoCajera = montoIngresadoCajera;
	}
	public Double getMontoDiferencia() {
		return montoDiferencia;
	}
	public void setMontoDiferencia(Double montoDiferencia) {
		this.montoDiferencia = montoDiferencia;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	
	
	
	
	
	
}
