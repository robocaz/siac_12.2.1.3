package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class BoletinDataDetalle extends BoletinDataGeneric implements Serializable{

	protected String glosaTipoAcreedor;
	protected Integer nroBolLaboral;
	protected Integer pagBolLaboral;
	protected String fecBolLaboral;
	protected String motivoInfraccion;
	protected BigDecimal montoLaboral;
	protected Integer codRegionInspeccion;
	protected Integer anoResolMulta;
	protected Integer nroResolMulta;
	protected String tipoMulta;
	protected Integer meses;
	protected Integer nroCotizaciones;

	public String getGlosaTipoAcreedor() {
		return glosaTipoAcreedor;
	}

	public void setGlosaTipoAcreedor(String glosaTipoAcreedor) {
		this.glosaTipoAcreedor = glosaTipoAcreedor;
	}

	public Integer getNroBolLaboral() {
		return nroBolLaboral;
	}

	public void setNroBolLaboral(Integer nroBolLaboral) {
		this.nroBolLaboral = nroBolLaboral;
	}

	public Integer getPagBolLaboral() {
		return pagBolLaboral;
	}

	public void setPagBolLaboral(Integer pagBolLaboral) {
		this.pagBolLaboral = pagBolLaboral;
	}

	public String getFecBolLaboral() {
		return fecBolLaboral;
	}

	public void setFecBolLaboral(String fecBolLaboral) {
		this.fecBolLaboral = fecBolLaboral;
	}

	public String getMotivoInfraccion() {
		return motivoInfraccion;
	}

	public void setMotivoInfraccion(String motivoInfraccion) {
		this.motivoInfraccion = motivoInfraccion;
	}

	public BigDecimal getMontoLaboral() {
		return montoLaboral;
	}

	public void setMontoLaboral(BigDecimal montoLaboral) {
		this.montoLaboral = montoLaboral;
	}

	public Integer getCodRegionInspeccion() {
		return codRegionInspeccion;
	}

	public void setCodRegionInspeccion(Integer codRegionInspeccion) {
		this.codRegionInspeccion = codRegionInspeccion;
	}

	public Integer getAnoResolMulta() {
		return anoResolMulta;
	}

	public void setAnoResolMulta(Integer anoResolMulta) {
		this.anoResolMulta = anoResolMulta;
	}

	public Integer getNroResolMulta() {
		return nroResolMulta;
	}

	public void setNroResolMulta(Integer nroResolMulta) {
		this.nroResolMulta = nroResolMulta;
	}

	public String getTipoMulta() {
		return tipoMulta;
	}

	public void setTipoMulta(String tipoMulta) {
		this.tipoMulta = tipoMulta;
	}

	public Integer getMeses() {
		return meses;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public Integer getNroCotizaciones() {
		return nroCotizaciones;
	}

	public void setNroCotizaciones(Integer nroCotizaciones) {
		this.nroCotizaciones = nroCotizaciones;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n tipoDeudaLaboral: " + tipoDeudaLaboral);
		str.append(" ,glosaTipoAcreedor: " + glosaTipoAcreedor);
		str.append(" ,nombreInstitucion: " + nombreInstitucion);
		str.append(" ,nroBolLaboral: " + nroBolLaboral);
		str.append(" ,pagBolLaboral: " + pagBolLaboral);
		str.append(" ,fecBolLaboral: " + fecBolLaboral);
		str.append(" ,motivoInfraccion: " + motivoInfraccion);
		str.append(" ,rut: " + rut);
		str.append(" ,nombre: " + nombre);
		str.append(" ,periodo: " + periodo);
		str.append(" ,montoLaboral: " + montoLaboral);
		str.append(" ,montoLaboralUTM: " + montoLaboralUTM);
		str.append(" ,codRegionInspeccion: " + codRegionInspeccion);
		str.append(" ,anoResolMulta: " + anoResolMulta);
		str.append(" ,nroResolMulta: " + nroResolMulta);
		str.append(" ,tipoMulta: " + tipoMulta);
		str.append(" ,meses: " + meses);
		str.append(" ,nroCotizaciones: " + nroCotizaciones);
		str.append(" ,montoAdeudado: " + montoAdeudado);
		return str.toString();
	}

}
