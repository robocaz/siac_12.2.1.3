package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginDTO implements Serializable{
    
    private String username;
    private String password;
    private String passwordAux;


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
    
    public void setPasswordAux(String passwordAux) {
        this.passwordAux = passwordAux;
    }

    public String getPasswordAux() {
        return passwordAux;
    }

}

