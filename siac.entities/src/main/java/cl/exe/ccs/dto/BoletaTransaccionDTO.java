package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BoletaTransaccionDTO extends ResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3153751350879728298L;
	private Long numeroBoleta;
	private Long idCaja;
	private Integer idCentroCosto;
	private String fecha;
	private ComboDTO medioPago;
	private ComboDTO banco;
	private SucursalDTO sucursal;
	private String rutTramitante;
	private String rutAfectado;
	private String numerocheque;
	private Long montoingresado;
	private Long total;
	private String nombreMaquina;
	private String tipoDocumento;
	private List<TransaccionServicioDTO> transacciones;
	private String corrTransaccion;
	private String boletaNula;
	private String tercero; 

	
	
	
	public String getTercero() {
		return tercero;
	}

	public void setTercero(String tercero) {
		this.tercero = tercero;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public Long getNumeroBoleta() {
		return numeroBoleta;
	}

	public void setNumeroBoleta(Long numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}

	public Long getIdCaja() {
		return idCaja;
	}

	public void setIdCaja(Long idCaja) {
		this.idCaja = idCaja;
	}

	public Integer getIdCentroCosto() {
		return idCentroCosto;
	}

	public void setIdCentroCosto(Integer idCentroCosto) {
		this.idCentroCosto = idCentroCosto;
	}



	public ComboDTO getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(ComboDTO medioPago) {
		this.medioPago = medioPago;
	}

	public ComboDTO getBanco() {
		return banco;
	}

	public void setBanco(ComboDTO banco) {
		this.banco = banco;
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	public String getRutTramitante() {
		return rutTramitante;
	}

	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}

	public String getRutAfectado() {
		return rutAfectado;
	}

	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}

	public String getNumerocheque() {
		return numerocheque;
	}

	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}

	public Long getMontoingresado() {
		return montoingresado;
	}

	public void setMontoingresado(Long montoingresado) {
		this.montoingresado = montoingresado;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getNombreMaquina() {
		return nombreMaquina;
	}

	public void setNombreMaquina(String nombreMaquina) {
		this.nombreMaquina = nombreMaquina;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public List<TransaccionServicioDTO> getTransacciones() {
		return transacciones;
	}

	public void setTransacciones(List<TransaccionServicioDTO> transacciones) {
		this.transacciones = transacciones;
	}

	public String getCorrTransaccion() {
		return corrTransaccion;
	}

	public void setCorrTransaccion(String corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}

	public String getBoletaNula() {
		return boletaNula;
	}

	public void setBoletaNula(String boletaNula) {
		this.boletaNula = boletaNula;
	}

}
