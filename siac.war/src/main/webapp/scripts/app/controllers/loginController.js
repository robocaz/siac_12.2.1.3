angular.module('ccs').controller('LoginController',function($scope, LoginResource, $location,$localStorage, $http ) {

				  $localStorage.$reset();
                  $scope.login = {};
                  
                  
                  $("#username").focus();

                  $scope.ingresar = function() {

                    var successCallback = function(data, responseHeaders) {

                      $scope.usuario.msgControl = $scope.validarIdControl(data);
                      $scope.blockBack(1);
                      $scope.StartTimer();
                    };
                    LoginResource.login($scope.login, successCallback,$scope.errorCallback);
                  };

                  $scope.logout = function() {

                    var successCallback = function(data, responseHeaders) {

                      $scope.blockBack(0);
                      $scope.usuario.msgControl = $scope.validarIdControl(data);
                      $scope.StopTimer();
                      
                    };

                    LoginResource.logout($scope.login, successCallback,$scope.errorCallback);
                  };

                  $scope.validarIdControl = function(data) {

                    if (data.idControl == 0) {
                    	data.esAgencia = Number(data.centroCostoActivo.codigo) > 2300 && Number(data.centroCostoActivo.codigo) < 3000;
                    	
                      $scope.setUsuario(data);
                      
                      $location.path("/inicio");
                      var url = "http://localhost:8080/terminal/siac/borratemporales";
                      $http({
                          method: 'POST',
                          url: url,
                          timeout: 4000,
                          headers : {
                              'Content-Type' : 'application/json'
                           }
                        }).then(function(response) {
                          console.log(response);
                        });  
                    } else if (data.idControl == 1) {
                      return "Usuario no Registrado";
                    } else if (data.idControl == 2) {
                      return "Error al Buscar Nombre del Usuario";
                    } else if (data.idControl == 3) {
                      return "Usuario no tiene Centro de Costo Activo, consulte con administrador";
                    } else if (data.idControl == 4) {
                      return "Usuario no Registrado";
                    } else if (data.idControl == 5) {
                      return "Nombre de usuario y/o password no son válidos";
                    } else if (data.idControl == 6) {
                      return "No se identifica perfil de usuario, consulte con administrador";
                    }
                  }

                });
