package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutFalabella database table.
 * 
 */
@Entity
@Table(name="tmp_rutFalabella")
@NamedQuery(name="Tmp_rutFalabella.findAll", query="SELECT t FROM Tmp_rutFalabella t")
public class Tmp_rutFalabella implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tmp_rutFalabella() {
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}