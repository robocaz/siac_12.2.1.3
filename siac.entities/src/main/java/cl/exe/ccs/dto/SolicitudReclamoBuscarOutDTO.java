package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class SolicitudReclamoBuscarOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 2215465607903677721L;

	private List<SolicitudReclamoData> lista;

	public List<SolicitudReclamoData> getLista() {
		return lista;
	}

	public void setLista(List<SolicitudReclamoData> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudReclamoBuscarOutDTO [lista=");
		builder.append(lista);
		builder.append(", msgControl=");
		builder.append(msgControl);
		builder.append(", idControl=");
		builder.append(idControl);
		builder.append("]");
		return builder.toString();
	}

}
