package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_VCDetRechazados database table.
 * 
 */
@Entity
@Table(name="Tbl_VCDetRechazados")
@NamedQuery(name="Tbl_VCDetRechazado.findAll", query="SELECT t FROM Tbl_VCDetRechazado t")
public class Tbl_VCDetRechazado implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_VCDetRechazadoPK id;

	@Column(name="Fld_CodEmisorVen")
	private String fld_CodEmisorVen;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisorVen")
	private String fld_TipoEmisorVen;

	@Column(name="Fld_TipoRechazo")
	private byte fld_TipoRechazo;

	public Tbl_VCDetRechazado() {
	}

	public Tbl_VCDetRechazadoPK getId() {
		return this.id;
	}

	public void setId(Tbl_VCDetRechazadoPK id) {
		this.id = id;
	}

	public String getFld_CodEmisorVen() {
		return this.fld_CodEmisorVen;
	}

	public void setFld_CodEmisorVen(String fld_CodEmisorVen) {
		this.fld_CodEmisorVen = fld_CodEmisorVen;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisorVen() {
		return this.fld_TipoEmisorVen;
	}

	public void setFld_TipoEmisorVen(String fld_TipoEmisorVen) {
		this.fld_TipoEmisorVen = fld_TipoEmisorVen;
	}

	public byte getFld_TipoRechazo() {
		return this.fld_TipoRechazo;
	}

	public void setFld_TipoRechazo(byte fld_TipoRechazo) {
		this.fld_TipoRechazo = fld_TipoRechazo;
	}

}