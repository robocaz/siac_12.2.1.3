package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_CONSULTARUT database table.
 * 
 */
@Entity
@Table(name="TBL_CONSULTARUT")
@NamedQuery(name="TblConsultarut.findAll", query="SELECT t FROM TblConsultarut t")
public class TblConsultarut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_BURO")
	private BigDecimal fldBuro;

	@Column(name="FLD_FECHATRN")
	private BigDecimal fldFechatrn;

	@Column(name="FLD_HORATRN")
	private BigDecimal fldHoratrn;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	public TblConsultarut() {
	}

	public BigDecimal getFldBuro() {
		return this.fldBuro;
	}

	public void setFldBuro(BigDecimal fldBuro) {
		this.fldBuro = fldBuro;
	}

	public BigDecimal getFldFechatrn() {
		return this.fldFechatrn;
	}

	public void setFldFechatrn(BigDecimal fldFechatrn) {
		this.fldFechatrn = fldFechatrn;
	}

	public BigDecimal getFldHoratrn() {
		return this.fldHoratrn;
	}

	public void setFldHoratrn(BigDecimal fldHoratrn) {
		this.fldHoratrn = fldHoratrn;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

}