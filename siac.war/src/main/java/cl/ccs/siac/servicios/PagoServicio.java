package cl.ccs.siac.servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.boleta.BoletaBusiness;
import cl.ccs.siac.boletin.BoletinBusiness;
import cl.ccs.siac.certificados.CertificadosBusiness;
import cl.ccs.siac.dao.CombosDao;
import cl.ccs.siac.pago.PagoBusiness;
import cl.ccs.siac.persona.PersonaBusiness;
import cl.ccs.siac.transaccion.TransaccionBusiness;
import cl.ccs.siac.utils.BuildPDF;
import cl.ccs.siac.utils.CertificadosUtil;
import cl.ccs.siac.utils.FacturaUtil;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.ccs.siac.utils.Utils;
import cl.ccs.siac.utils.certificados.CertificadoICMUtils;
import cl.ccs.siac.utils.certificados.CertificadoILPUtils;
import cl.exe.ccs.dto.AceptaTransaccionDTO;
import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.BoletinDataDetalle;
import cl.exe.ccs.dto.BoletinDataEmpleador;
import cl.exe.ccs.dto.BoletinDataTrabajador;
import cl.exe.ccs.dto.BoletinInDTO;
import cl.exe.ccs.dto.BoletinOutDTO;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SucursalDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.TransaccionServicioDTO;
import cl.exe.ccs.dto.UsuarioDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;
import cl.exe.ccs.enums.TipoCertificado;
import cl.exe.ccs.resources.Constants;

/**
 * @author rbocaz
 *
 */
@Path("/pagos")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class PagoServicio {

	private static final Logger LOG = LogManager.getLogger(PagoServicio.class);

	@Inject
	private CombosDao combosDao;

	@Inject
	private Utils utils;

	@Inject
	private PersonaBusiness personaBusiness;

	@Inject
	private PagoBusiness pagoBusiness;

	@Inject
	private BoletaBusiness boletaBusiness;


	@Inject
	private TransaccionBusiness transaccionBusiness;

	@Inject
	private CertificadosBusiness certificadosBusiness;

	@Inject
	private BoletinBusiness boletinBusiness;
	


	/**
	 * Metodo que obtiene el listado de los bancos disponibles en el sistema.
	 *
	 * @param request
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/combobancos")
	public Response obtenerBancos(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<ComboDTO> bancos = combosDao.obtenerEmisores("BCO");

			if (bancos == null || bancos.size() == 0) {
				LOG.error("Ha ocurrido un error al cargar barcos ");
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(bancos).build();
			}
		} catch (Exception ex) {
			LOG.error("Metodo obtenerBancos Cargar Bancos, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Obtiene un listado de las sucursales de CCS
	 *
	 * @param request
	 * @param banco
	 * @return
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/combosucursales")
	public Response obtenerSucursales(@Context HttpServletRequest request, ComboDTO banco) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<SucursalDTO> sucursales = combosDao.obtenerSucursales(banco);

			if (sucursales == null || sucursales.size() == 0) {
				LOG.error("Ha ocurrido un error al cargar sucursales. Metodo obtenerSucursales Cargar Sucursales ");
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(sucursales).build();
			}
		} catch (Exception ex) {
			LOG.error("Metodo obtenerSucursales Cargar Sucursales, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Metodo que imprime una boleta asociada a una transaccion.
	 *
	 * @param request
	 * @param boletaTransaccion
	 * @return
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/imprimirboleta")
	public Response imprimeBoleta(@Context HttpServletRequest request, BoletaTransaccionDTO boletaTransaccion) {

		// guardo en bd SU_ActBoletaFacturaTmp_SW2
		ResponseDTO respuestaDTO = null;
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: imprimeBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try{
			Long respConsulta = pagoBusiness.consultaBoletaFacturaTemp(boletaTransaccion);
			// si numero boleta es 0 es una impresion
			// y actualizo en la tabla con numero de boleta y medios de pago
			if (respConsulta == 0) {
				// sp para actualizar boleta
				respuestaDTO = pagoBusiness.actualizaBoletaFacturaTemp(boletaTransaccion);
				if (respuestaDTO.getIdControl() != 0) {
					LOG.error("Ha ocurrido un error al actualizar boleta ");
					return Response.ok(respuestaDTO).build();
				}
			}
	
			return Response.status(Response.Status.OK).build();
		} catch (Exception ex) {
			LOG.error("Metodo imprimeBoleta , Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Metodo utilizado para la reimpresion de una boleta asociada a una
	 * transaccion.
	 *
	 * @param request
	 * @param boletaTransaccion
	 * @return
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/reimprimirboleta")
	public Response reimprimirBoleta(@Context HttpServletRequest request, BoletaTransaccionDTO boletaTransaccion) {

		// guardo en bd SU_ActBoletaFacturaTmp_SW2
		ResponseDTO respuestaDTO = new ResponseDTO();
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: reimprimirBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// armaBoleta.imprimeBoletaJar(boletaTransaccion);

		respuestaDTO.setIdControl(0);
		return Response.ok(respuestaDTO).build();
	}

	/**
	 * Metodo utilizado para anular una boleta asociada a una transaccion.
	 *
	 * @param request
	 * @param boletaTransaccion
	 * @return
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/anularboleta")
	public Response anularBoleta(@Context HttpServletRequest request, BoletaTransaccionDTO boletaTransaccion) {

		Principal userPrincipal = request.getUserPrincipal();
		ResponseDTO respuesta = null;
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: anularBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		try{
			for (TransaccionServicioDTO transaccion : boletaTransaccion.getTransacciones()) {
				respuesta = pagoBusiness.anulacionRegistroNuevo(boletaTransaccion.getIdCaja(),Long.parseLong(transaccion.getCorrelativoTransaccion()));
				if (respuesta.getIdControl() != 0) {
					LOG.error("Ha ocurrido un error al actualizar rango boleta ");
					return Response.ok(respuesta).build();
				}
			}
	
			return Response.status(Response.Status.OK).build();
		}catch (Exception ex) {
			LOG.error("Metodo anularBoleta , Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Metodo que actualiza los rangos de boletas que utiliza una sucursal
	 *
	 * @param request
	 * @param boletaTransaccion
	 * @return
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/actboletarango")
	public Response actualizaRangoBoleta(@Context HttpServletRequest request, BoletaTransaccionDTO boletaTransaccion) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: actualizaRangoBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// envio a imprimir
		ResponseDTO respuesta = boletaBusiness.actualizaRangoBoleta(boletaTransaccion);
		if (respuesta.getIdControl() != 0) {
			LOG.error("Ha ocurrido un error al actualizar rango boleta ");
			return Response.ok(respuesta).build();
		}

		return Response.status(Response.Status.OK).build();
	}

	/**
	 * Metodo utilizado para finalizar las transacciones del sistema.
	 *
	 * @param request
	 * @param aceptaTransaccion
	 * @return
	 * @throws InterruptedException
	 */
	@SuppressWarnings("rawtypes")
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/aceptatransaccion")
	public Response aceptaTransaccion(@Context HttpServletRequest request, AceptaTransaccionDTO aceptaTransaccion) throws InterruptedException {

		ResponseDTO respuesta = new ResponseDTO();
		ResponseListDTO<CertificadoDTO> certificados = new ResponseListDTO<CertificadoDTO>();
		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: aceptaTransaccion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		respuesta = transaccionBusiness.aceptarTransaccion(aceptaTransaccion);
		LOG.info("Valor respuesta aceptarTransaccion " + respuesta.getIdControl());
		if (respuesta.getIdControl() != 0) {
			LOG.error("Ha ocurrido un error al aceptar transaccion");
			return Response.ok(respuesta).build();
		}

		certificados = transaccionBusiness.certificadosTransaccion(aceptaTransaccion);

		List<CertificadoDTO> certs = new ArrayList<CertificadoDTO>();
		
		for (CertificadoDTO c : certificados.getLista()) {
			
			switch (TipoCertificado.valueOf(c.getCodServicio())) {
			case CT1:
				String[] tipo1 = new String[2];
				tipo1[0] = "ISA";
				tipo1[1] = "ILP";
				certs.addAll(generaCertificados(tipo1, c));
				break;
			case CT2:
				String[] tipo2 = new String[4];
				tipo2[0] = "ISA";
				tipo2[1] = "ILP";
				tipo2[2] = "BLT";
				tipo2[3] = "BLI";
				certs.addAll(generaCertificados(tipo2, c));
				break;
			case CT3:
				String[] tipo3 = new String[3];
				tipo3[0] = "ISA";
				tipo3[1] = "ILP";
				tipo3[2] = "BLI";
				certs.addAll(generaCertificados(tipo3, c));
				break;
			case ADM:
				String[] tipo4 = new String[2];
				tipo4[0] = "ADM";
				tipo4[1] = "ILP";
				certs.addAll(generaCertificados(tipo4, c));	
				break;
			case ARM:
				String[] tipo5 = new String[2];
				tipo5[0] = "ARM";
				tipo5[1] = "ILP";
				certs.addAll(generaCertificados(tipo5, c));	
				break;	
			case AMM:
				String[] tipo6 = new String[2];
				tipo6[0] = "AMM";
				tipo6[1] = "ILP";
				certs.addAll(generaCertificados(tipo6, c));	
				break;					
			case RAM:
				String[] tipo7 = new String[1];
				tipo7[0] = "RAM";
				certs.addAll(generaCertificados(tipo7, c));	
				break;
			case RMM:
			case RDM:
				String[] tipo8 = new String[1];
				tipo8[0] = "RAM";
				certs.addAll(generaCertificados(tipo8, c));	
				break;				
			default:
				certs.add(c);
				break;
			}

			if (!certs.isEmpty()) {
				certificados.setLista(certs);
			}

		}

		LOG.info("Valor respuesta certificadosTransaccion " + respuesta.getIdControl());
		if (certificados.getIdControl() != 0) {
			LOG.error("Ha ocurrido un error al aceptar transaccion ");
			return Response.ok(respuesta).build();
		}

		GeneradorPDF generador = new GeneradorPDF();

		for (CertificadoDTO certificado : certificados.getLista()) {
			switch (TipoCertificado.valueOf(certificado.getCodServicio())) {
			case ISA:
				ConsultaBICDTO consultaBIC = certificadosBusiness.generaCOACBIC(userPrincipal.getName(), certificado);
				String codAutenticBic = consultaBIC.getCodAutenticBic();
				consultaBIC.setRut(certificado.getRutAfectado());
				ConsultaMOLDTO consultaMOLDTO = certificadosBusiness.generaCOACMOL(userPrincipal.getName(), certificado,codAutenticBic);
				
					//create new report ISA
					String codVerificacion = consultaBIC.getCodAutenticBic().trim() + " - " + consultaMOLDTO.getCodAutenticMol().trim();
					
					Map<String, Object> parametrosISA = new HashMap<String, Object>();
					
					try {
						byte[] archivo;
						if(consultaBIC.getProtVigentes().size()==0 && consultaMOLDTO.getListMorosidadVig().size()==0){
							if(FacturaUtil.getTipoPersonaRut(consultaBIC.getRut()).equalsIgnoreCase("J")){
								parametrosISA.put("pNombre",(consultaBIC.getApPat().trim()+""+consultaBIC.getApMat().trim()+""+consultaBIC.getNombres().trim()).trim());
								parametrosISA.put("pFlag", 1);
							}else{
								parametrosISA.put("pNombre",(consultaBIC.getNombres().trim()+" "+consultaBIC.getApPat().trim()+" "+consultaBIC.getApMat().trim()).trim());
								parametrosISA.put("pFlag", 0);
							}
							parametrosISA.put("pRut",CertificadosUtil.getRut(consultaBIC.getRut()));
							parametrosISA.put("user",usuarioDTO.getUsername());
							parametrosISA.put("sucursal",usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
							parametrosISA.put("codVerificacion",codVerificacion);
							archivo = generador.create(certificado.getCodServicio()+"nodata", new HashSet(), parametrosISA);
						}else{
							parametrosISA = CertificadosUtil.getParametrosISA(consultaBIC, consultaMOLDTO,usuarioDTO, 26);
							archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosISA);
							

							
						}

						String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(codVerificacion);
						OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
						stream.write(archivo);
						stream.close();
						
						FileInputStream fis = new FileInputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
						byte[] reporte = IOUtils.toByteArray(fis);
						


						
						certificado.setReporte(archivo);
						certificado.setRutaPDF(nombreArchivo);
						
						    
						    
					} catch (Exception ex) {
						LOG.error("Ha ocurrido un error al generar certificado ISA");
						certificado.setIdControl(1);
					}

					break;

			case ICT:
			  certificado.setTipoConsulta(8);
				ConsultaBICDTO consultaBICICT = certificadosBusiness.generaCOACBIC(userPrincipal.getName(),certificado);
				consultaBICICT.setRut(certificado.getRutAfectado());
				ConsultaMOLDTO consultaMOLICT = certificadosBusiness.generaCOACMOL(userPrincipal.getName(), certificado,consultaBICICT.getCodAutenticBic());
				String codigoVerificacion = consultaBICICT.getCodAutenticBic().trim() + " - " + consultaMOLICT.getCodAutenticMol().trim();

				Map<String, Object> parametrosICT = CertificadosUtil.getParametrosICT(consultaBICICT, consultaMOLICT,usuarioDTO, 26);

				try {
					byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosICT);
					String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(codigoVerificacion);
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception ex) {
					LOG.error("Ha ocurrido un error al generar certificado ICT");
					certificado.setIdControl(1);
				}
				break;

			case CAC:
				ConsultaCerAclDTO consultaCAC = new ConsultaCerAclDTO();
				consultaCAC.setRutAfectado(certificado.getRutAfectado());
				consultaCAC.setCorrMovimiento(certificado.getCorrMovimiento());
				consultaCAC.setCodUsuario(usuarioDTO.getUsername());

				CertificadoAclDTO certificadoCAC = certificadosBusiness.generaCAC(consultaCAC);
				Map<String, Object> parametrosCAC = CertificadosUtil.getParametrosCAC(certificadoCAC, usuarioDTO);

				try {
					byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosCAC);
					String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(certificadoCAC.getCodAutenticBic());
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception ex) {
					ex.printStackTrace();
					LOG.error("Ha ocurrido un error al generar certificado CAC");
					certificado.setIdControl(1);
				}
				break;

			case CAA:
				ConsultaCerAclDTO consultaCertificadoCAA = new ConsultaCerAclDTO();

				consultaCertificadoCAA.setCorrCaja(aceptaTransaccion.getIdCaja());
				consultaCertificadoCAA.setCorrTransac(certificado.getCorrTransaccion());
				consultaCertificadoCAA.setRutAfectado(certificado.getRutAfectado());
				consultaCertificadoCAA.setCorrNombre(aceptaTransaccion.getCorrNombre());
				consultaCertificadoCAA.setCodUsuario(usuarioDTO.getUsername());
				consultaCertificadoCAA.setCorrMovimiento(certificado.getCorrMovimiento());
				consultaCertificadoCAA.setCodServicio(certificado.getCodServicio());

				PersonaDTO personaCAA = personaBusiness.obtener(certificado.getRutAfectado());

				ConsultaCerAclDTO certificadoCAA = certificadosBusiness.generaCAA(consultaCertificadoCAA);
				Map<String, Object> parametrosCAA = CertificadosUtil.getParametrosCAA(certificadoCAA, usuarioDTO,personaCAA);

				try {
					byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosCAA);
					String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(certificadoCAA.getCodAutenticBic());
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception ex) {
					ex.printStackTrace();
					LOG.error("Ha ocurrido un error al generar certificado CAC");
					certificado.setIdControl(1);
				}
				break;
			case CAR:
				ConsultaCerAclDTO consultaCertificadoCAR = new ConsultaCerAclDTO();

				consultaCertificadoCAR.setCorrCaja(aceptaTransaccion.getIdCaja());
				consultaCertificadoCAR.setCorrTransac(certificado.getCorrTransaccion());
				consultaCertificadoCAR.setRutAfectado(certificado.getRutAfectado());
				consultaCertificadoCAR.setCorrNombre(aceptaTransaccion.getCorrNombre());
				consultaCertificadoCAR.setCodUsuario(usuarioDTO.getUsername());
				consultaCertificadoCAR.setCorrMovimiento(certificado.getCorrMovimiento());
				consultaCertificadoCAR.setCodServicio(certificado.getCodServicio());

				PersonaDTO personaCAR = personaBusiness.obtener(certificado.getRutAfectado());

				ConsultaCerAclDTO certificadoCAR = certificadosBusiness.generaCAA(consultaCertificadoCAR);
				Map<String, Object> parametrosCAR = CertificadosUtil.getParametrosCAR(certificadoCAR, usuarioDTO,personaCAR);

				try {
					byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosCAR);
					String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(certificadoCAR.getCodAutenticBic());
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception ex) {
					ex.printStackTrace();
					LOG.error("Ha ocurrido un error al generar certificado CAC");
					certificado.setIdControl(1);
				}
				break;
			case ILP:
				generaILP(usuarioDTO, generador, certificado);
				break;
			case ADM:
				generalADM(usuarioDTO, generador, certificado);
				break;
			case AMM:
				generalADM(usuarioDTO, generador, certificado);
				break;				
			case ARM:
				generalADM(usuarioDTO, generador, certificado);
				break;
			case RAM:
			case RMM:
			case RDM:
				generalRAM(usuarioDTO, generador, certificado);
				break;
			case AUM:
				generalAUM(usuarioDTO, generador, certificado);
				break;				
			case BLT:
				LOG.info("> Creacion Certificado: " + certificado.getCodServicio());
				final BoletinInDTO paramsBLT = new BoletinInDTO();
				
				paramsBLT.setRutAfectado(certificado.getRutAfectado());
				paramsBLT.setCorrMovimiento(certificado.getCorrMovimiento());
				paramsBLT.setCodServicio(certificado.getCodServicio());
				paramsBLT.setCodUsuario(usuarioDTO.getUsername());
				
				paramsBLT.setCentroCosto(usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
				
				LOG.info("> Obteniendo Data RUT: " + certificado.getRutAfectado());
				final BoletinOutDTO<BoletinDataTrabajador> dataBLT = boletinBusiness.consultaTrabajador(paramsBLT);
				LOG.info("> Mapeando Data..");
				final Map<String, Object> mapaBLT = boletinBusiness.obtenerMapaBLT(paramsBLT, dataBLT);
				
				try {
					LOG.info("> Generando PDF..");
					final byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), mapaBLT);
					final String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(dataBLT.getCodAutenticBic() + "-0");
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception e) {
					LOG.error("Error al generar certificado {0}: {1}", certificado.getCodServicio(), e.getMessage());
					certificado.setIdControl(1);
				}
				break;
			case BLI:
				LOG.info("> Creacion Certificado: " + certificado.getCodServicio());
				final BoletinInDTO paramsBLI = new BoletinInDTO();
				
				paramsBLI.setRutAfectado(certificado.getRutAfectado());
				paramsBLI.setCorrMovimiento(certificado.getCorrMovimiento());
				paramsBLI.setCodServicio(certificado.getCodServicio());
				paramsBLI.setCodUsuario(usuarioDTO.getUsername());
				
				paramsBLI.setCentroCosto(usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
				
				LOG.info("> Obteniendo Data RUT: " + certificado.getRutAfectado());
				final BoletinOutDTO<BoletinDataDetalle> dataBLI = boletinBusiness.consultaInfractor(paramsBLI);
				LOG.info("> Mapeando Data..");
				final Map<String, Object> mapaBLI = boletinBusiness.obtenerMapaBLI(paramsBLI, dataBLI);
				
				try {
					LOG.info("> Generando PDF..");
					final byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), mapaBLI);
					final String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(dataBLI.getCodAutenticBic() + "-0");
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception e) {
					LOG.error("Error al generar certificado {0}: {1}", certificado.getCodServicio(), e.getMessage());
					certificado.setIdControl(1);
				}
				break;
			case BLR:
				LOG.info("> Creacion Certificado: " + certificado.getCodServicio());
				final BoletinInDTO paramsBLR = new BoletinInDTO();
				
				paramsBLR.setRutAfectado(certificado.getRutAfectado());
				paramsBLR.setCorrMovimiento(certificado.getCorrMovimiento());
				paramsBLR.setCodServicio(certificado.getCodServicio());
				paramsBLR.setCodUsuario(usuarioDTO.getUsername());
				
				paramsBLR.setCentroCosto(usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
				
				LOG.info("> Obteniendo Data RUT: " + certificado.getRutAfectado());
				final BoletinOutDTO<BoletinDataEmpleador> dataBLR = boletinBusiness.consultaResumen(paramsBLR);
				LOG.info("> Mapeando Data..");
				final Map<String, Object> mapaBLR = boletinBusiness.obtenerMapaBLR(paramsBLR, dataBLR);
				
				try {
					LOG.info("> Generando PDF..");
					final byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), mapaBLR);
					final String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(dataBLR.getCodAutenticBic() + "-0");
					OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
					stream.write(archivo);
					stream.close();
					certificado.setReporte(archivo);
					certificado.setRutaPDF(nombreArchivo);
				} catch (Exception e) {
					LOG.error("Error al generar certificado {0}: {1}", certificado.getCodServicio(), e.getMessage());
					certificado.setIdControl(1);
				}
				break;
			case ICM:
				generaICM(usuarioDTO, generador, certificado);
				break;
			case ILG:
				ConsultaBICDTO consultaBICILG = certificadosBusiness.generaCOACBIC(userPrincipal.getName(), certificado);
				LOG.error("consultaBICILG");
				String codAutenticBicILG = consultaBICILG.getCodAutenticBic();
				LOG.error("codAutenticBicILG");
				consultaBICILG.setRut(certificado.getRutAfectado());
				LOG.error("setRut");
				ConsultaMOLDTO consultaMOLDTOILG = certificadosBusiness.generaCOACMOL(userPrincipal.getName(), certificado,codAutenticBicILG);
				LOG.error("consultaMOLDTOILG");
					//create new report ISA
					String codVerificacionILG = consultaBICILG.getCodAutenticBic().trim() + " - " + consultaMOLDTOILG.getCodAutenticMol().trim();
					LOG.error("codVerificacionILG");
					Map<String, Object> parametrosILG = new HashMap<String, Object>();
					LOG.error("parametrosILG");
					try {
						LOG.error("inicio try");
						byte[] archivo;
						LOG.error("despues byte");
						if(consultaBICILG.getProtVigentes().size()==0 && consultaMOLDTOILG.getListMorosidadVig().size()==0){
							LOG.error("consultaBICILG: "+consultaBICILG.getProtVigentes().size());
							LOG.error("consultaMOLDTOILG: "+consultaMOLDTOILG.getListMorosidadVig().size());
							LOG.error("despues if");
							parametrosILG.put("pNombre",(consultaBICILG.getNombres().trim()+" "+consultaBICILG.getApPat().trim()+" "+consultaBICILG.getApMat().trim()).trim());
							LOG.error("pNombre: "+consultaBICILG.getNombres()+" "+consultaBICILG.getApPat()+" "+consultaBICILG.getApMat());
							parametrosILG.put("pFlag", 0);
							LOG.error("pFlag: "+0);
							parametrosILG.put("pRut",CertificadosUtil.getRut(consultaBICILG.getRut()));
							LOG.error("pRut: "+CertificadosUtil.getRut(consultaBICILG.getRut()));
							parametrosILG.put("user",usuarioDTO.getUsername());
							LOG.error("user: "+usuarioDTO.getUsername());
							parametrosILG.put("sucursal",usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
							LOG.error("sucursal: "+usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
							parametrosILG.put("codVerificacion",codVerificacionILG);
							LOG.error("codVerificacion: "+codVerificacionILG);
							archivo = generador.create(certificado.getCodServicio()+"nodata", new HashSet(), parametrosILG);
							LOG.error("se gener� con jasper");
						}else{
							LOG.error("consultaBICILG: "+consultaBICILG.getProtVigentes().size());
							LOG.error("consultaMOLDTOILG: "+consultaMOLDTOILG.getListMorosidadVig().size());
							LOG.error("inicio else");
							parametrosILG = CertificadosUtil.getParametrosILG(consultaBICILG, consultaMOLDTOILG,usuarioDTO, 26);
							LOG.error("medio else");
							archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosILG);
							LOG.error("final else");
						}
						
						String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(codVerificacionILG);
						LOG.error("nombreArchivo: "+nombreArchivo);
						OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
						stream.write(archivo);
						stream.close();
						certificado.setReporte(archivo);
						LOG.error("set reporte");
						certificado.setRutaPDF(nombreArchivo);
						LOG.info("setRuta");
					} catch (Exception ex) {
						LOG.error("Ha ocurrido un error al generar certificado ILG");
						certificado.setIdControl(1);
					}
										

				break;
			default:
				break;
			}

		}

		return Response.ok(certificados).build();

	}

	private List<CertificadoDTO> generaCertificados(String[] tipo, CertificadoDTO c) {
		List<CertificadoDTO> certificados = new ArrayList<CertificadoDTO>();

		for (int i = 0; i < tipo.length; i++) {
			CertificadoDTO certificado = new CertificadoDTO();
			certificado.setCodServicio(tipo[i]);
			certificado.setCorrMovimiento(c.getCorrMovimiento());
			certificado.setCorrTransaccion(c.getCorrTransaccion());
			certificado.setGlosaServicio(getGlosaPorTipo(tipo[i]));
			certificado.setIdControl(c.getIdControl());
			certificado.setMsgControl(c.getMsgControl());
			certificado.setNumeroBoleta(c.getNumeroBoleta());
			certificado.setRutAfectado(c.getRutAfectado());
			certificado.setRutaPDF(c.getRutAfectado());
			certificado.setTipoDocumento(c.getTipoDocumento());
			certificado.setFlagImprime(c.getFlagImprime());

			certificados.add(certificado);
		}

		return certificados;

	}

	private String getGlosaPorTipo(String tipo) {
		if ("ISA".equals(tipo)) {
			return "Inf. Consolidado";
		} else if ("ILP".equals(tipo)) {
			return "Inf. Lista Publico";
		} else if ("BLT".equals(tipo)) {
			return "Certificado Laboral Trabajador";
		} else if ("BLR".equals(tipo)) {
			return "Certificado Laboral Empleador";
		} else if ("BLI".equals(tipo)) {
			return "Certificado Laboral Detalle";
		}else if("ADM".equals(tipo)){
			return "BC Te Avisa Total Meson";
		}else if("ARM".equals(tipo)){
			return "BC Te Avisa Email Meson";
		}else if("AMM".equals(tipo)){
			return "BC Te Avisa Movil Meson";
		}else if("RAM".equals(tipo)){
			return "Renovación BC Te Avisa Email Meson";
		}else if("RMM".equals(tipo)){
			return "Renovación BC Te Avisa Movil Meson";
		}else if("AUM".equals(tipo)){
			return "BC Upgrade Te Avisa Mesón";
		}		
		return "";
	}

	private void generaISA(Principal userPrincipal, UsuarioDTO usuarioDTO, CertificadoDTO certificado) {
	  certificado.setTipoConsulta(2);
		ConsultaBICDTO consultaBIC = certificadosBusiness.generaCOACBIC(userPrincipal.getName(), certificado);
		String codAutenticBic = consultaBIC.getCodAutenticBic();
		consultaBIC.setRut(certificado.getRutAfectado());
		ConsultaMOLDTO consultaMOLDTO = certificadosBusiness.generaCOACMOL(userPrincipal.getName(), certificado,codAutenticBic);
		BuildPDF pdf = new BuildPDF();
		String codigo = pdf.buildPDF(consultaBIC, consultaMOLDTO, usuarioDTO.getUsername(),usuarioDTO.getCentroCostoActivo().getDescripcion());
		System.out.println(pdf.getRutaPDF());
		certificado.setRutaPDF(pdf.getFilename());
	}

	private void generaICM(UsuarioDTO usuarioDTO, GeneradorPDF generador, CertificadoDTO certificado) {
		ConsCertificadoICMDTO consCertificadoICM = new ConsCertificadoICMDTO();
		consCertificadoICM.setSucursal(usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
		consCertificadoICM.setCodUsuario(usuarioDTO.getUsername());
		consCertificadoICM.setRutAfectado(certificado.getRutAfectado());
		consCertificadoICM.setCorrMovimiento(certificado.getCorrMovimiento());
		consCertificadoICM.setTipoConsulta(2);
		ConsCertificadoICMDTO certificadoICM = certificadosBusiness.generaICM(consCertificadoICM);
		Map<String, Object> parametrosICM = CertificadoICMUtils.getParametrosICM(certificadoICM);
		try {
			byte[] archivo = generador.create(certificado.getCodServicio(), new HashSet(), parametrosICM);
			String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(certificadoICM.getCodAutenticBic());
			OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
			stream.write(archivo);
			stream.close();
			certificado.setReporte(archivo);
			certificado.setRutaPDF(nombreArchivo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Ha ocurrido un error al generar certificado ICM");
			certificado.setIdControl(1);
		}
	}

	private void generaILP(UsuarioDTO usuarioDTO, GeneradorPDF generador, CertificadoDTO certificado) {
		ConsCertificadoILPDTO consCertificado = new ConsCertificadoILPDTO();
		consCertificado.setSucursal(usuarioDTO.getCentroCostoActivo().getDescripcion().trim());
		consCertificado.setCodUsuario(usuarioDTO.getUsername());
		consCertificado.setRutAfectado(certificado.getRutAfectado());
		consCertificado.setCorrMovimiento(certificado.getCorrMovimiento());
		ConsCertificadoILPDTO certificadoILP = certificadosBusiness.generaILP(consCertificado);
		Map<String, Object> parametros = CertificadoILPUtils.getParametrosILP(certificadoILP);
		try {
			byte[] archivo = generador.create("ILP", new HashSet(), parametros);
			String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(certificadoILP.getCodAutenticBic());
			OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
			stream.write(archivo);
			stream.close();
			certificado.setReporte(archivo);
			certificado.setRutaPDF(nombreArchivo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Ha ocurrido un error al generar certificado ILP");
			certificado.setIdControl(1);
		}
	}
	
	private void generalADM(UsuarioDTO usuarioDTO, GeneradorPDF generador, CertificadoDTO certificado) {
		TeAvisaDTO contrato = new TeAvisaDTO();
		
		contrato.setCorrCaja(Integer.parseInt(usuarioDTO.getCaja()));
		contrato.setCorrMovimiento(certificado.getCorrMovimiento());
		contrato.setCorrTransaccion(certificado.getCorrTransaccion());
		
		contrato = certificadosBusiness.getDatosContratoTeAvisa(contrato);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		SimpleDateFormat of = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		parametros.put("RUT", CertificadosUtil.getRut(contrato.getContratante().getRut().trim()));
		parametros.put("NOMBRE", contrato.getContratante().getNombres().trim() + " " + contrato.getContratante().getApellidoPaterno().trim() + " " + contrato.getContratante().getApellidoMaterno().trim());
		parametros.put("FECHA", df.format(new Date()));
		parametros.put("DIRECCION", contrato.getDireccion());
		parametros.put("TELEFONO", contrato.getTelfijo());
		parametros.put("CELULAR", contrato.getTelcel());
		parametros.put("RUT_AVISA", CertificadosUtil.getRut(contrato.getPersTeAvisa().getRut().trim()));
		parametros.put("EMAIL", contrato.getEmail());
		parametros.put("COD_AUTENTIC", String.format("%011d",contrato.getCorrContrato()));
		parametros.put("USUARIO_CC", usuarioDTO.getUsername() + " - " + usuarioDTO.getCentroCostoActivo().getDescripcion());
		
		String valorServicioDTA = pagoBusiness.valorServicio("DTA").getMsgControl();
		parametros.put("COSTO_DTA", valorServicioDTA);

		String valorServicioARM = pagoBusiness.valorServicio("ARM").getMsgControl();
		parametros.put("COSTO_SERV_1", valorServicioARM + " " + utils.getTextoValor(valorServicioARM));
		
		String valorServicioADM = pagoBusiness.valorServicio("ADM").getMsgControl();
		parametros.put("COSTO_SERV_2", valorServicioADM  + " " + utils.getTextoValor(valorServicioADM));

		String valorServicioAMM = pagoBusiness.valorServicio("AMM").getMsgControl();
		parametros.put("COSTO_SERV_3", valorServicioAMM   + " " + utils.getTextoValor(valorServicioAMM));
		
		String tipoContrato = "";
		if(contrato.getServcorreo() && !contrato.getServsms()){
			tipoContrato = "CORREO ELECTRÓNICO";
		}
		if(!contrato.getServcorreo() && contrato.getServsms()){
			tipoContrato = "MÓVIL";
		}		
		if(contrato.getServcorreo() && contrato.getServsms()){
			tipoContrato = "TOTAL";
		}
		parametros.put("TIPO_CONTRATO", tipoContrato);
		
		
		try {
			byte[] archivo = generador.create("contratoTeAvisa", new HashSet(), parametros);
			
			String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(contrato.getCorrContrato().toString());
			OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
			stream.write(archivo);
			stream.close();
			certificado.setRutaPDF(nombreArchivo);
			certificado.setReporte(archivo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Ha ocurrido un error al generar contratoTeAvisa");
			certificado.setIdControl(1);
		}
		
		
	}

	private void generalRAM(UsuarioDTO usuarioDTO, GeneradorPDF generador, CertificadoDTO certificado) {
		TeAvisaDTO contrato = new TeAvisaDTO();
		
		contrato.setCorrCaja(Integer.parseInt(usuarioDTO.getCaja()));
		contrato.setCorrMovimiento(certificado.getCorrMovimiento());
		contrato.setCorrTransaccion(certificado.getCorrTransaccion());
		
		contrato = certificadosBusiness.getDatosContratoTeAvisa(contrato);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		SimpleDateFormat of = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		parametros.put("RUT", CertificadosUtil.getRut(contrato.getContratante().getRut().trim()));
		parametros.put("NOMBRE", contrato.getContratante().getNombres().trim() + " " + contrato.getContratante().getApellidoPaterno().trim() + " " + contrato.getContratante().getApellidoMaterno().trim());
		parametros.put("FECHA", df.format(new Date()));
		parametros.put("DIRECCION", contrato.getDireccion());
		parametros.put("TELEFONO", contrato.getTelfijo());
		parametros.put("CELULAR", contrato.getTelcel());
		parametros.put("RUT_AVISA", CertificadosUtil.getRut(contrato.getPersTeAvisa().getRut().trim()));
		parametros.put("EMAIL", contrato.getEmail());
		parametros.put("COD_AUTENTIC", String.format("%011d",contrato.getCorrContrato()));
		parametros.put("USUARIO_CC", usuarioDTO.getUsername() + " - " + usuarioDTO.getCentroCostoActivo().getDescripcion());
		parametros.put("NUM_CONTRATO", contrato.getCorrContrato().toString());		

		String tipoContrato = "";
		if(contrato.getServcorreo() && !contrato.getServsms()){
			tipoContrato = "CORREO ELECTRÓNICO";
		}
		if(!contrato.getServcorreo() && contrato.getServsms()){
			tipoContrato = "MÓVIL";
		}		
		if(contrato.getServcorreo() && contrato.getServsms()){
			tipoContrato = "TOTAL";
		}
		parametros.put("TIPO_CONTRATO", tipoContrato);
		
		
		try {
			byte[] archivo = generador.create("renovacionTeAvisa", new HashSet(), parametros);
			
			String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(contrato.getCorrContrato().toString()+"-"+contrato.getCorrMovimiento());
			OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
			stream.write(archivo);
			stream.close();
			certificado.setReporte(archivo);
			certificado.setRutaPDF(nombreArchivo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Ha ocurrido un error al generar renovacionTeAvisa");
			certificado.setIdControl(1);
		}
		
		
	}
	
	private void generalAUM(UsuarioDTO usuarioDTO, GeneradorPDF generador, CertificadoDTO certificado) {
		TeAvisaDTO contrato = new TeAvisaDTO();
		
		contrato.setCorrCaja(Integer.parseInt(usuarioDTO.getCaja()));
		contrato.setCorrMovimiento(certificado.getCorrMovimiento());
		contrato.setCorrTransaccion(certificado.getCorrTransaccion());
		
		contrato = certificadosBusiness.getDatosContratoTeAvisa(contrato);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		SimpleDateFormat of = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		parametros.put("RUT", CertificadosUtil.getRut(contrato.getContratante().getRut().trim()));
		parametros.put("NOMBRE", contrato.getContratante().getNombres().trim() + " " + contrato.getContratante().getApellidoPaterno().trim() + " " + contrato.getContratante().getApellidoMaterno().trim());
		parametros.put("FECHA", df.format(new Date()));
		parametros.put("DIRECCION", contrato.getDireccion());
		parametros.put("TELEFONO", contrato.getTelfijo());
		parametros.put("CELULAR", contrato.getTelcel());
		parametros.put("RUT_AVISA", CertificadosUtil.getRut(contrato.getPersTeAvisa().getRut().trim()));
		parametros.put("EMAIL", contrato.getEmail());
		parametros.put("COD_AUTENTIC", String.format("%011d",contrato.getCorrContrato()));
		parametros.put("USUARIO_CC", usuarioDTO.getUsername() + " - " + usuarioDTO.getCentroCostoActivo().getDescripcion());
		parametros.put("NUM_CONTRATO", contrato.getCorrContrato().toString());		

		
		try {
			byte[] archivo = generador.create("upgradeTeAvisa", new HashSet(), parametros);
			
			String nombreArchivo = CertificadosUtil.getNombreArchivoCertificado(contrato.getCorrContrato().toString());
			OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + nombreArchivo + ".pdf");
			stream.write(archivo);
			stream.close();
			certificado.setReporte(archivo);
			certificado.setRutaPDF(nombreArchivo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Ha ocurrido un error al generar upgradeTeAvisa");
			certificado.setIdControl(1);
		}
		
		
	}	
	/**
	 * Metodo utilizado para cancelar las transacciones del sistema.
	 *
	 * @param request
	 * @param aceptaTransaccion
	 * @return
	 * @throws InterruptedException
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/eliminatransaccion")
	public Response cancelarTransaccion(@Context HttpServletRequest request, AceptaTransaccionDTO aceptaTransaccion) throws InterruptedException {

		ResponseDTO respuesta = new ResponseDTO();
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: cancelarTransaccion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
        
		try{
			respuesta = transaccionBusiness.cancelarTransaccion(aceptaTransaccion);
			if (respuesta.getIdControl() != 0) {
				LOG.error("Ha ocurrido un error al actualizar rango boleta ");
				return Response.ok(respuesta).build();
			}
	
			return Response.ok(respuesta).build();
		}
		catch(Exception e){
			LOG.error("Metodo cancelarTransaccion  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Metodo utilizado para actualizar una transaccion antes de aceptar o
	 * finalizar.
	 *
	 * @param request
	 * @param boletaTransaccion
	 * @return
	 * @throws InterruptedException
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/actualizar")
	public Response actualizaAntesAceptar(@Context HttpServletRequest request, BoletaTransaccionDTO boletaTransaccion)
			throws InterruptedException {

		// guardo en bd SU_ActBoletaFacturaTmp_SW2
		ResponseDTO respuestaDTO = null;
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: actualizaAntesAceptar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		try{
			respuestaDTO = pagoBusiness.actualizaMedioPagoTemp(boletaTransaccion);
			LOG.info("Valor respuesta actualizaMedioPagoTemp " + respuestaDTO.getIdControl());
			if (respuestaDTO.getIdControl() != 0) {
				LOG.error("Ha ocurrido un error al actualizar medio de pago ");
				return Response.ok(respuestaDTO).build();
			}
			return Response.ok(respuestaDTO).build();
		}
		catch(Exception e){
			LOG.error("Metodo actualizaAntesAceptar  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Metodo utilizado para la descarga de los certificados comprados en el
	 * sistema
	 *
	 * @param request
	 * @param nombreArchivo
	 * @return
	 * @throws Exception
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/descargacertificado/{fileName}")
	public Response descargaCertificado(@Context HttpServletRequest request,@PathParam("fileName") String nombreArchivo) throws Exception {

		File file = new File(Constants.DIR_CERTIFICATES + "/" + nombreArchivo + ".pdf");
		ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment;filename=" + file.getName());
		return response.build();

	}

	/**
	 * M�todo utilizado para obtener el valor ($) del servicio
	 *
	 * @param request
	 * @param nombreArchivo
	 * @return
	 * @throws Exception
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/valorServicio")
	public Response valorServicio(@Context HttpServletRequest request, String codServicio) throws Exception {

		ResponseDTO respuestaDTO = null;
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			LOG.error("Error en el metodo: valorServicio - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try{
			respuestaDTO = pagoBusiness.valorServicio(codServicio);
			LOG.info("Valor respuesta valorServicio " + respuestaDTO.getIdControl());
	
			if (respuestaDTO.getIdControl() != 0) {
				LOG.error("Ha ocurrido un error al obtener el valor del servicio" + codServicio);
			}
			return Response.ok(respuestaDTO).build();
		}
		catch(Exception e){
			LOG.error("Ha ocurrido un error al obtener el valor del servicio" + codServicio);
			LOG.error("Metodo valorServicio  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
}
