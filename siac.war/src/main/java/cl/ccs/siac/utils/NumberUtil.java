package cl.ccs.siac.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import cl.exe.ccs.resources.Constants;

public class NumberUtil {
    
    private final static NumberUtil INSTANCE = new NumberUtil();
    
    public NumberUtil() {
        super();
    }
    
    public static NumberUtil getInstance() {
        return INSTANCE;
    }
    
    public static String numberWithSeparator(Long number) {
        NumberFormat numberFormat = DecimalFormat.getIntegerInstance(Constants.LOCALE);
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.applyPattern("###,###.###");
        return decimalFormat.format(number);
    }

    public static String numberWithSeparator(String numberStr) {
        Long number = Long.parseLong(numberStr);
        return numberWithSeparator(number);
    }

    public static String numberWithSeparatorWithotD(Long number) {
        NumberFormat numberFormat = DecimalFormat.getIntegerInstance(Constants.LOCALE);
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.applyPattern("###,###");
        return decimalFormat.format(number);
    }

    public String amountPdf(Float number) {
            NumberFormat numberFormat = DecimalFormat.getIntegerInstance(Constants.LOCALE);
            DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
            decimalFormat.applyPattern("###,##0.00");
            return decimalFormat.format(number);
        }
        
        public String amountPdf(Double number) {
            NumberFormat numberFormat = DecimalFormat.getIntegerInstance(Constants.LOCALE);
            DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
            decimalFormat.applyPattern("###,##0.00");
            return decimalFormat.format(number);
        }


    public static String fillWithZeroPdf(Integer number) {
        return String.format("%04d", number);
    }

    public static String formatRut(String rut) {
        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        rut = String.valueOf(Long.parseLong(rut.substring(0, rut.length() - 1))) + rut.substring(rut.length() - 1);

        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }
    
    public static String roundAndFormat(Float fNumber){
        return formatNumber(roundFloat(fNumber));
    }
    
    
    /* Round 1.5 to 2 and 1.4 to 1 */
    public static float roundFloat(Float fNumber){
        String strNumber = formatNumber(fNumber);
        BigDecimal bdNumber = new BigDecimal(strNumber).setScale(0, BigDecimal.ROUND_HALF_UP);
        return bdNumber.floatValue();
    }
    
    
    /**
     * Convert float number to a string representation with the first two decimals
     * in case of it have decimals.
     * Examples:
     * 85.1234 is converted to 85.12
     * 85.0 is converted to 85
     * @param number
     * @return
     */
    public static String formatNumber(Float number) {
        
        String formmatedIva;
        DecimalFormat format = new DecimalFormat();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Constants.LOCALE);
        symbols.setDecimalSeparator('.'); /* use dot as decimal separator */
        format.setGroupingSize(0);
        format.setDecimalFormatSymbols(symbols);
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);
        formmatedIva = format.format(number);
        return formmatedIva;
    }
    
    /**
     *
     * @param gross
     * @param iva example: 19
     * @return
     */
    public static strictfp float calcNeto(Float gross, Float iva) {
        float fNneto = gross / (iva / 100f + 1);  
        return fNneto;
    }

}
