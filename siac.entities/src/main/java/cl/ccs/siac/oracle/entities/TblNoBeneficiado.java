package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_NO_BENEFICIADO database table.
 * 
 */
@Entity
@Table(name="TBL_NO_BENEFICIADO")
@NamedQuery(name="TblNoBeneficiado.findAll", query="SELECT t FROM TblNoBeneficiado t")
public class TblNoBeneficiado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_RUT")
	private String fldRut;

	public TblNoBeneficiado() {
	}

	public String getFldRut() {
		return this.fldRut;
	}

	public void setFldRut(String fldRut) {
		this.fldRut = fldRut;
	}

}