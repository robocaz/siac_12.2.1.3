package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Emisor_TipoDocumento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Emisor_TipoDocumento.findAll", query="SELECT t FROM Tbl_Emisor_TipoDocumento t")
public class Tbl_Emisor_TipoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Calidad")
	private BigDecimal fld_Calidad;

	@Column(name="Fld_CargoContacto")
	private String fld_CargoContacto;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodFrecuenciaEnvio")
	private short fld_CodFrecuenciaEnvio;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CodUsuarioMod")
	private String fld_CodUsuarioMod;

	@Column(name="Fld_CorrEmiTipoDoc_Id")
	private BigDecimal fld_CorrEmiTipoDoc_Id;

	@Column(name="Fld_DiaTope1")
	private byte fld_DiaTope1;

	@Column(name="Fld_DiaTope2")
	private byte fld_DiaTope2;

	@Column(name="Fld_DireccionContacto")
	private String fld_DireccionContacto;

	@Column(name="Fld_Estado")
	private byte fld_Estado;

	@Column(name="Fld_FaxContacto")
	private double fld_FaxContacto;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecMod")
	private Timestamp fld_FecMod;

	@Column(name="Fld_FlagPrincipal")
	private boolean fld_FlagPrincipal;

	@Column(name="Fld_FonoContacto")
	private String fld_FonoContacto;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_GradoDificultad")
	private BigDecimal fld_GradoDificultad;

	@Column(name="Fld_MailContacto")
	private String fld_MailContacto;

	@Column(name="Fld_NomContacto_ApMat")
	private String fld_NomContacto_ApMat;

	@Column(name="Fld_NomContacto_ApPat")
	private String fld_NomContacto_ApPat;

	@Column(name="Fld_NomContacto_Nombres")
	private String fld_NomContacto_Nombres;

	@Column(name="Fld_RutContacto")
	private String fld_RutContacto;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_VolumenEsperado")
	private int fld_VolumenEsperado;

	public Tbl_Emisor_TipoDocumento() {
	}

	public BigDecimal getFld_Calidad() {
		return this.fld_Calidad;
	}

	public void setFld_Calidad(BigDecimal fld_Calidad) {
		this.fld_Calidad = fld_Calidad;
	}

	public String getFld_CargoContacto() {
		return this.fld_CargoContacto;
	}

	public void setFld_CargoContacto(String fld_CargoContacto) {
		this.fld_CargoContacto = fld_CargoContacto;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodFrecuenciaEnvio() {
		return this.fld_CodFrecuenciaEnvio;
	}

	public void setFld_CodFrecuenciaEnvio(short fld_CodFrecuenciaEnvio) {
		this.fld_CodFrecuenciaEnvio = fld_CodFrecuenciaEnvio;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public String getFld_CodUsuarioMod() {
		return this.fld_CodUsuarioMod;
	}

	public void setFld_CodUsuarioMod(String fld_CodUsuarioMod) {
		this.fld_CodUsuarioMod = fld_CodUsuarioMod;
	}

	public BigDecimal getFld_CorrEmiTipoDoc_Id() {
		return this.fld_CorrEmiTipoDoc_Id;
	}

	public void setFld_CorrEmiTipoDoc_Id(BigDecimal fld_CorrEmiTipoDoc_Id) {
		this.fld_CorrEmiTipoDoc_Id = fld_CorrEmiTipoDoc_Id;
	}

	public byte getFld_DiaTope1() {
		return this.fld_DiaTope1;
	}

	public void setFld_DiaTope1(byte fld_DiaTope1) {
		this.fld_DiaTope1 = fld_DiaTope1;
	}

	public byte getFld_DiaTope2() {
		return this.fld_DiaTope2;
	}

	public void setFld_DiaTope2(byte fld_DiaTope2) {
		this.fld_DiaTope2 = fld_DiaTope2;
	}

	public String getFld_DireccionContacto() {
		return this.fld_DireccionContacto;
	}

	public void setFld_DireccionContacto(String fld_DireccionContacto) {
		this.fld_DireccionContacto = fld_DireccionContacto;
	}

	public byte getFld_Estado() {
		return this.fld_Estado;
	}

	public void setFld_Estado(byte fld_Estado) {
		this.fld_Estado = fld_Estado;
	}

	public double getFld_FaxContacto() {
		return this.fld_FaxContacto;
	}

	public void setFld_FaxContacto(double fld_FaxContacto) {
		this.fld_FaxContacto = fld_FaxContacto;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecMod() {
		return this.fld_FecMod;
	}

	public void setFld_FecMod(Timestamp fld_FecMod) {
		this.fld_FecMod = fld_FecMod;
	}

	public boolean getFld_FlagPrincipal() {
		return this.fld_FlagPrincipal;
	}

	public void setFld_FlagPrincipal(boolean fld_FlagPrincipal) {
		this.fld_FlagPrincipal = fld_FlagPrincipal;
	}

	public String getFld_FonoContacto() {
		return this.fld_FonoContacto;
	}

	public void setFld_FonoContacto(String fld_FonoContacto) {
		this.fld_FonoContacto = fld_FonoContacto;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public BigDecimal getFld_GradoDificultad() {
		return this.fld_GradoDificultad;
	}

	public void setFld_GradoDificultad(BigDecimal fld_GradoDificultad) {
		this.fld_GradoDificultad = fld_GradoDificultad;
	}

	public String getFld_MailContacto() {
		return this.fld_MailContacto;
	}

	public void setFld_MailContacto(String fld_MailContacto) {
		this.fld_MailContacto = fld_MailContacto;
	}

	public String getFld_NomContacto_ApMat() {
		return this.fld_NomContacto_ApMat;
	}

	public void setFld_NomContacto_ApMat(String fld_NomContacto_ApMat) {
		this.fld_NomContacto_ApMat = fld_NomContacto_ApMat;
	}

	public String getFld_NomContacto_ApPat() {
		return this.fld_NomContacto_ApPat;
	}

	public void setFld_NomContacto_ApPat(String fld_NomContacto_ApPat) {
		this.fld_NomContacto_ApPat = fld_NomContacto_ApPat;
	}

	public String getFld_NomContacto_Nombres() {
		return this.fld_NomContacto_Nombres;
	}

	public void setFld_NomContacto_Nombres(String fld_NomContacto_Nombres) {
		this.fld_NomContacto_Nombres = fld_NomContacto_Nombres;
	}

	public String getFld_RutContacto() {
		return this.fld_RutContacto;
	}

	public void setFld_RutContacto(String fld_RutContacto) {
		this.fld_RutContacto = fld_RutContacto;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public int getFld_VolumenEsperado() {
		return this.fld_VolumenEsperado;
	}

	public void setFld_VolumenEsperado(int fld_VolumenEsperado) {
		this.fld_VolumenEsperado = fld_VolumenEsperado;
	}

}