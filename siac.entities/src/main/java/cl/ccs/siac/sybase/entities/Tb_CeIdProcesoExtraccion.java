package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdProcesoExtraccion database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdProcesoExtraccion.findAll", query="SELECT t FROM Tb_CeIdProcesoExtraccion t")
public class Tb_CeIdProcesoExtraccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_TipoExtraccion")
	private byte fld_TipoExtraccion;

	public Tb_CeIdProcesoExtraccion() {
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public byte getFld_TipoExtraccion() {
		return this.fld_TipoExtraccion;
	}

	public void setFld_TipoExtraccion(byte fld_TipoExtraccion) {
		this.fld_TipoExtraccion = fld_TipoExtraccion;
	}

}