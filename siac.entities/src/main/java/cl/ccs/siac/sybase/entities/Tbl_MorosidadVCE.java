package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_MorosidadVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_MorosidadVCE.findAll", query="SELECT t FROM Tbl_MorosidadVCE t")
public class Tbl_MorosidadVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecVencimiento")
	private Timestamp fld_FecVencimiento;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_MontoDeuda")
	private BigDecimal fld_MontoDeuda;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutInfractor")
	private String fld_RutInfractor;

	@Column(name="Fld_TipoDocInfractor")
	private String fld_TipoDocInfractor;

	@Column(name="Fld_TipoMoneda")
	private String fld_TipoMoneda;

	public Tbl_MorosidadVCE() {
	}

	public Timestamp getFld_FecVencimiento() {
		return this.fld_FecVencimiento;
	}

	public void setFld_FecVencimiento(Timestamp fld_FecVencimiento) {
		this.fld_FecVencimiento = fld_FecVencimiento;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public BigDecimal getFld_MontoDeuda() {
		return this.fld_MontoDeuda;
	}

	public void setFld_MontoDeuda(BigDecimal fld_MontoDeuda) {
		this.fld_MontoDeuda = fld_MontoDeuda;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutInfractor() {
		return this.fld_RutInfractor;
	}

	public void setFld_RutInfractor(String fld_RutInfractor) {
		this.fld_RutInfractor = fld_RutInfractor;
	}

	public String getFld_TipoDocInfractor() {
		return this.fld_TipoDocInfractor;
	}

	public void setFld_TipoDocInfractor(String fld_TipoDocInfractor) {
		this.fld_TipoDocInfractor = fld_TipoDocInfractor;
	}

	public String getFld_TipoMoneda() {
		return this.fld_TipoMoneda;
	}

	public void setFld_TipoMoneda(String fld_TipoMoneda) {
		this.fld_TipoMoneda = fld_TipoMoneda;
	}

}