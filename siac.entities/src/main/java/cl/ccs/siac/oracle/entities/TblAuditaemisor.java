package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_AUDITAEMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_AUDITAEMISOR")
@NamedQuery(name="TblAuditaemisor.findAll", query="SELECT t FROM TblAuditaemisor t")
public class TblAuditaemisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ACCION")
	private String fldAccion;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CORRDIRECCION")
	private BigDecimal fldCorrdireccion;

	@Column(name="FLD_EMAIL")
	private String fldEmail;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAFINVIGENCIA")
	private Date fldFechafinvigencia;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIOVIGENCIA")
	private Date fldFechainiciovigencia;

	@Column(name="FLD_FLAGVIGENCIA")
	private BigDecimal fldFlagvigencia;

	@Column(name="FLD_GLOSAEMISOR")
	private String fldGlosaemisor;

	@Column(name="FLD_RAZONSOCIAL")
	private String fldRazonsocial;

	@Column(name="FLD_SITIOWEB")
	private String fldSitioweb;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOASIGNADO")
	private String fldUsuarioasignado;

	@Column(name="FLD_USUARIOELIMINACION")
	private String fldUsuarioeliminacion;

	public TblAuditaemisor() {
	}

	public String getFldAccion() {
		return this.fldAccion;
	}

	public void setFldAccion(String fldAccion) {
		this.fldAccion = fldAccion;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCorrdireccion() {
		return this.fldCorrdireccion;
	}

	public void setFldCorrdireccion(BigDecimal fldCorrdireccion) {
		this.fldCorrdireccion = fldCorrdireccion;
	}

	public String getFldEmail() {
		return this.fldEmail;
	}

	public void setFldEmail(String fldEmail) {
		this.fldEmail = fldEmail;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechafinvigencia() {
		return this.fldFechafinvigencia;
	}

	public void setFldFechafinvigencia(Date fldFechafinvigencia) {
		this.fldFechafinvigencia = fldFechafinvigencia;
	}

	public Date getFldFechainiciovigencia() {
		return this.fldFechainiciovigencia;
	}

	public void setFldFechainiciovigencia(Date fldFechainiciovigencia) {
		this.fldFechainiciovigencia = fldFechainiciovigencia;
	}

	public BigDecimal getFldFlagvigencia() {
		return this.fldFlagvigencia;
	}

	public void setFldFlagvigencia(BigDecimal fldFlagvigencia) {
		this.fldFlagvigencia = fldFlagvigencia;
	}

	public String getFldGlosaemisor() {
		return this.fldGlosaemisor;
	}

	public void setFldGlosaemisor(String fldGlosaemisor) {
		this.fldGlosaemisor = fldGlosaemisor;
	}

	public String getFldRazonsocial() {
		return this.fldRazonsocial;
	}

	public void setFldRazonsocial(String fldRazonsocial) {
		this.fldRazonsocial = fldRazonsocial;
	}

	public String getFldSitioweb() {
		return this.fldSitioweb;
	}

	public void setFldSitioweb(String fldSitioweb) {
		this.fldSitioweb = fldSitioweb;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuarioasignado() {
		return this.fldUsuarioasignado;
	}

	public void setFldUsuarioasignado(String fldUsuarioasignado) {
		this.fldUsuarioasignado = fldUsuarioasignado;
	}

	public String getFldUsuarioeliminacion() {
		return this.fldUsuarioeliminacion;
	}

	public void setFldUsuarioeliminacion(String fldUsuarioeliminacion) {
		this.fldUsuarioeliminacion = fldUsuarioeliminacion;
	}

}