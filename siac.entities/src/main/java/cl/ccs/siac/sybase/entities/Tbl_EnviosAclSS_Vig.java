package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_EnviosAclSS_Vig database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosAclSS_Vig.findAll", query="SELECT t FROM Tbl_EnviosAclSS_Vig t")
public class Tbl_EnviosAclSS_Vig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CorrEnvioAcl")
	private BigDecimal fld_CorrEnvioAcl;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_Bol")
	private boolean fld_Flag_Bol;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_EnviosAclSS_Vig() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public BigDecimal getFld_CorrEnvioAcl() {
		return this.fld_CorrEnvioAcl;
	}

	public void setFld_CorrEnvioAcl(BigDecimal fld_CorrEnvioAcl) {
		this.fld_CorrEnvioAcl = fld_CorrEnvioAcl;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_Bol() {
		return this.fld_Flag_Bol;
	}

	public void setFld_Flag_Bol(boolean fld_Flag_Bol) {
		this.fld_Flag_Bol = fld_Flag_Bol;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}