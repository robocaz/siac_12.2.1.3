package cl.ccs.siac.persona;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.PersonaDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("personaBusiness")
public class PersonaBusinessImpl implements PersonaBusiness {

	@Inject
	private PersonaDao personaDao;

	@Inject
	private Utils utils;

	private Logger log = LogManager.getLogger(PersonaBusinessImpl.class);

	@Override
	public PersonaDTO obtener(String rut) {
		String rutParse = "";
		if (rut != null && !rut.equals("")) {

			rutParse = utils.formatearRUT(rut);
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');

			log.info(rutParse);
		}
		return personaDao.obtener(rutParse);
	}

	@Override
	public PersonaDTO actualizar(PersonaDTO personaDTO) {

		String rutParse = "";
		if (personaDTO.getRut() != null && !personaDTO.getRut().equals("")) {

			rutParse = utils.formatearRUT(personaDTO.getRut());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');

			personaDTO.setRut(rutParse);
		}

		return personaDao.actualizar(personaDTO);
	}
	
	@Override
	public ResponseDTO verificar(String rut) {

		String rutParse = "";
		if (rut != null && !rut.equals("")) {

			rutParse = utils.formatearRUT(rut);
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
		}

		return personaDao.verificar(rutParse);
	}

	public PersonaDTO getNombre(String rut, Long correlativo){
		return personaDao.getNombre(rut, correlativo);
	}
}
