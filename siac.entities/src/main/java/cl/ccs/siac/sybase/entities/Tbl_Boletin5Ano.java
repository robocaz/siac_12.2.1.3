package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Boletin5Anos database table.
 * 
 */
@Entity
@Table(name="Tbl_Boletin5Anos")
@NamedQuery(name="Tbl_Boletin5Ano.findAll", query="SELECT t FROM Tbl_Boletin5Ano t")
public class Tbl_Boletin5Ano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletin5Anos")
	private short fld_NroBoletin5Anos;

	public Tbl_Boletin5Ano() {
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletin5Anos() {
		return this.fld_NroBoletin5Anos;
	}

	public void setFld_NroBoletin5Anos(short fld_NroBoletin5Anos) {
		this.fld_NroBoletin5Anos = fld_NroBoletin5Anos;
	}

}