angular.module('ccs').controller('MedioPagoController', function($scope, MediosResource, LoginResource, $rootScope, $location,$uibModal, $document) {

    $scope.mediospago = [];
    $scope.cajas = [];
    $scope.mediopago = {};
    $scope.mediopago.numboletafac = "";
    $scope.mediospago = [];
    $scope.sucursal = [];
    
    $scope.usuario = $rootScope.usuario;
    
    
    
    $scope.cargaMedios = function() {

        var successCallback = function(data,responseHeaders){
        	
        	console.log(data);
        	$scope.mediospago = data;
        	$scope.showMedios(1);
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        MediosResource.cargaMedios(successCallback, errorCallback2);
    };

    $scope.showMedios = function(value) {
        if (value == 1){
        	$scope.medioshow = true;
        }
        else{ 
        	$scope.medioshow = false;
        }
    };
      


      
    $scope.cargacajas = function() {

        var successCallback = function(data,responseHeaders){
        	
        	console.log(data);
        	$scope.cajas = data;

        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        MediosResource.cajasabiertas(successCallback, errorCallback2);
    };
    
    $scope.abrir = function(){
		angular.element("#modificarmediopago").modal();
	
    }

  	angular.element("#modificarmediopago").on('show.bs.modal', function () {
	    $scope.cargacajas();
  	});	



    $scope.buscarBoleta = function() {
    	
    	$scope.mediopago.idCCosto = $scope.usuario.centroCostoActivo.codigo;
    	
    	if(angular.equals({}, $scope.mediopago.caja) || $scope.mediopago.caja === undefined){
    		$scope.mediopago.msgControl = "";
    		swal({ title: 'Error', text: "Seleccione Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
    		return false;
    	}
    	if($scope.mediopago.numboletafac == null || $scope.mediopago.numboletafac == ""){
    		$scope.mediopago.msgControl = "";
    		swal({ title: 'Error', text: "Debe Ingresar n° boleta/factura", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
    		return false;
    	}
    	else{
	    	$scope.mediopago.idCaja = $scope.mediopago.caja.corrCaja;
	        var successCallback = function(data,responseHeaders){
	        	
	        	console.log(data);
	        	if(data.idControl == 1){
	        		$scope.mediopago.msgControl = "N\u00BA Documento no corresponde a caja seleccionada";
	        		return false;
	        	}
	        	else{
		        	$scope.boleta = data;
		        	angular.element("#modificarmediopago").modal('hide');
		        	angular.element("#tblresuldocumentos").modal();
		        	$scope.mediopago.msgControl = "";
	        	}
	        };
	        var errorCallback2 = function(response) {
	            $scope.error = "Servicio de Aplicación no se encuentra disponible";
	        };
	        MediosResource.cargaBoletas($scope.mediopago,successCallback, errorCallback2);
	        $scope.mediopago.msgControl = "";
	    	console.log($scope.mediopago);
    	}
    }
    
    
    
    $scope.actualizaMedio = function(idDiv) {
    	
    	var flag = false;
    	if($scope.mediopago.mediospago.codigo == "CHE" || $scope.mediopago.mediospago.codigo == "VVS"){
	    	if($scope.mediopago.numerocheque == null || $scope.mediopago.numerocheque == ""){
	    		$scope.mediopago.msgControl = "Debe ingresar número de documento";
	    		return false;
	    	}
	    	if($scope.mediopago.emisor == null || $scope.mediopago.emisor.codigo == ""){
	    		$scope.mediopago.msgControl = "Debe Seleccionar Banco";
	    		return false;
	    	}
	    	if($scope.mediopago.sucursal == null || $scope.mediopago.sucursal == ""){
	    		$scope.mediopago.msgControl = "Debe Seleccionar Sucursal";
	    		return false;
	    	}
	    	else{
	    		$scope.mediopago.msgControl = "";
	    		flag = true;
	    	}
    	}
    	
    	if($scope.mediopago.mediospago.codigo == "EFT" ){
    		$scope.mediopago.msgControl = "";
    		flag = true;
    	}
    	
    	if($scope.mediopago.mediospago.codigo == "TCD"){
	    	if($scope.mediopago.numerocheque == null || $scope.mediopago.numerocheque == ""){
	    		$scope.mediopago.msgControl = "Debe ingresar número de comprobante";
	    		return false;
	    	}
	    	if($scope.mediopago.emisor == null || $scope.mediopago.emisor.codigo == ""){
	    		$scope.mediopago.msgControl = "Debe Seleccionar Banco";
	    		return false;
	    	}
	    	else{
	    		$scope.mediopago.msgControl = "";
	    		flag = true;
	    	}
    	}
    	
    	if($scope.mediopago.mediospago.codigo == "RC"){
	    	if($scope.mediopago.autorizacion == null || $scope.mediopago.autorizacion == ""){
	    		$scope.mediopago.msgControl = "Debe ingresar código de autorización";
	    		return false;
	    	}
	    	if($scope.mediopago.operacion == null || $scope.mediopago.operacion == ""){
	    		$scope.mediopago.msgControl = "Debe ingresar número de operación";
	    		return false;
	    	}
	    	if($scope.mediopago.emisor == null || $scope.mediopago.emisor.codigo == ""){
	    		$scope.mediopago.msgControl = "Debe Seleccionar Banco";
	    		return false;
	    	}
	    	else{
	    		$scope.mediopago.msgControl = "";
	    		
	    		$scope.mediopago.autorizacion = $scope.mediopago.autorizacion.padStart(6, "0");
	    		$scope.mediopago.operacion = $scope.mediopago.operacion.padStart(6, "0");
	    		$scope.mediopago.numerocheque = $scope.mediopago.autorizacion + $scope.mediopago.operacion;
	    		flag = true;
	    	}
    	}
    	
    	if(flag){
	        var successCallback = function(data,responseHeaders){
	        	
	        	console.log(data);
	        	if(data.idControl == 0){
		        	$scope.boleta = data;
		        	swal('Exito','Medio de Pago modificado exitosamente.','success');
		        	angular.element("#tblresuldocumentos").modal('hide');
		        	$scope.mediopago.numboletafac = "";
		        	if(idDiv != null || idDiv != ""){
		        		angular.element("#"+idDiv).modal('hide');
		        	}
		        	angular.element("#modificarmediopago").modal();
		        	$scope.mediopago = {};
		        	$scope.medioshow = false;
	        	}
	        	else{
	        		swal('Error','Error al actualizar medio de pago','error');
	        	}
	        	
	        };
	        var errorCallback2 = function(response) {
	            $scope.error = "Servicio de Aplicación no se encuentra disponible";
	        };
	        
	        MediosResource.actualizaMetodo($scope.mediopago,successCallback, errorCallback2);
	    	console.log($scope.mediopago);
    	}
    }
    
    
    $scope.abrirModal = function(){
    	if($scope.mediopago.mediospago.codigo == $scope.boleta.mediospago.codigo){
    		swal('Error','Boleta/Factura consultada, ya posee este tipo de pago.','error');
    		return false;
    	}
    	else{
	        var modalInstance = $uibModal.open({
	          templateUrl: 'views/modal/modalloginsupervisor.html',
	          controller: 'ModalLoginSupervisorController',
	          appendTo: angular.element($document[0].querySelector('#tblresuldocumentos')),
	          size: 'sm'
	        });
	        modalInstance.result.then(function (respuesta) {
	            if(respuesta){
	            	
	                if($scope.mediopago.mediospago.codigo == "EFT"){
	                	console.log("medio efectivo");
	                	$scope.mediopago.numerocheque = "";
	                	$scope.actualizaMedio();
	                }
	                if($scope.mediopago.mediospago.codigo == "CHE"){
	                	console.log("medio cheque");
	                	angular.element("#modpagocheque").modal();
	                }
	                if($scope.mediopago.mediospago.codigo == "VVS"){
	                	console.log("medio vale vista");
	                	angular.element("#modpagovalevista").modal();
	                }
	                if($scope.mediopago.mediospago.codigo == "TCD"){
	                	console.log("medio tarjeta credito");
	                	angular.element("#modpagotarjetacredito").modal();
	                }
	                if($scope.mediopago.mediospago.codigo == "RC"){
	                	console.log("medio red compra");
	                	angular.element("#modpagoredcompra").modal();
	                }	                
	            }
	        });
    	}
    }
    
    
    $scope.cargaBancos = function() {

        var successCallback = function(data,responseHeaders){
        	
        	console.log(data);
        	$scope.bancos = data;
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        MediosResource.cargaBancos(successCallback, errorCallback2);
    };
    
    $scope.cargaSucursales = function() {


 		angular.element("#cargando").modal();
        var successCallback = function(data,responseHeaders){
        	angular.element("#cargando").modal("hide");
        	console.log(data);
        	
        	$scope.sucursales = data;
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        MediosResource.cargaSucursales($scope.mediopago, successCallback, errorCallback2);
    };
    
    
    
	$scope.noCeros = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.mediopago.numerocheque = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Número Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}

	$scope.noCerosAutorizacion = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.mediopago.autorizacion = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Código de Autorización Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}

	$scope.noCerosOperacion = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.mediopago.operacion = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Número de Operación Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}
    
    $scope.limpiar = function(){
    	$scope.mediopago.numboletafac = "";
    	$scope.mediopago.caja = {};
    }

    
    $scope.cerralimpiaDatos = function(){
    	$scope.mediopago.numboletafac = "";
    	$scope.mediopago.caja = {};   
    	$scope.mediopago.msgControl = "";
    	angular.element("#modificarmediopago").modal("hide");
    }	
    
    $scope.limpiaDatos = function(){
    	$scope.mediopago.numerocheque = "";
    	$scope.mediopago.emisor = {};
    	$scope.mediopago.sucursal = {};
    	
    }
    
    $scope.reset = function(){
    	    $scope.cajas = [];
    }
});