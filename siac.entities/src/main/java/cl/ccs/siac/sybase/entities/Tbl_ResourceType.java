package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ResourceType database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ResourceType.findAll", query="SELECT t FROM Tbl_ResourceType t")
public class Tbl_ResourceType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	@Column(name="Fld_ResourceType")
	private byte fld_ResourceType;

	@Column(name="Fld_ResourceTypeDescription")
	private String fld_ResourceTypeDescription;

	public Tbl_ResourceType() {
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

	public byte getFld_ResourceType() {
		return this.fld_ResourceType;
	}

	public void setFld_ResourceType(byte fld_ResourceType) {
		this.fld_ResourceType = fld_ResourceType;
	}

	public String getFld_ResourceTypeDescription() {
		return this.fld_ResourceTypeDescription;
	}

	public void setFld_ResourceTypeDescription(String fld_ResourceTypeDescription) {
		this.fld_ResourceTypeDescription = fld_ResourceTypeDescription;
	}

}