package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_NrosUnicos database table.
 * 
 */
@Entity
@Table(name="Tbl_NrosUnicos")
@NamedQuery(name="Tbl_NrosUnico.findAll", query="SELECT t FROM Tbl_NrosUnico t")
public class Tbl_NrosUnico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_IdRectif")
	private int fld_IdRectif;

	public Tbl_NrosUnico() {
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public int getFld_IdRectif() {
		return this.fld_IdRectif;
	}

	public void setFld_IdRectif(int fld_IdRectif) {
		this.fld_IdRectif = fld_IdRectif;
	}

}