package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetCausalDevEnvProt database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetCausalDevEnvProt.findAll", query="SELECT t FROM Tbl_DetCausalDevEnvProt t")
public class Tbl_DetCausalDevEnvProt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalDev")
	private byte fld_CodCausalDev;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_DetCausalDevEnvProt() {
	}

	public byte getFld_CodCausalDev() {
		return this.fld_CodCausalDev;
	}

	public void setFld_CodCausalDev(byte fld_CodCausalDev) {
		this.fld_CodCausalDev = fld_CodCausalDev;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}