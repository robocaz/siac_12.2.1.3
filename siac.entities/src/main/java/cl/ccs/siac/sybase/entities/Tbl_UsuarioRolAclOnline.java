package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsuarioRolAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsuarioRolAclOnline.findAll", query="SELECT t FROM Tbl_UsuarioRolAclOnline t")
public class Tbl_UsuarioRolAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoRol")
	private String fld_CodigoRol;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	public Tbl_UsuarioRolAclOnline() {
	}

	public String getFld_CodigoRol() {
		return this.fld_CodigoRol;
	}

	public void setFld_CodigoRol(String fld_CodigoRol) {
		this.fld_CodigoRol = fld_CodigoRol;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

}