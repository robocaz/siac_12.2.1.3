package cl.ccs.siac.teavisa;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.TeAvisaDao;
import cl.ccs.siac.tramitante.TramitanteBusinessImpl;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TeAvisaDTO;

@Named("teAvisaBusiness")
public class TeAvisaBusinessImpl implements TeAvisaBusiness {
	
	@Inject
	private TeAvisaDao teAvisaDao;

	@Inject
	private Utils utils;

	private Logger log = LogManager.getLogger(TramitanteBusinessImpl.class);
	
	
	public TeAvisaDTO contrataTeAvisa(TeAvisaDTO contrato){
		
		String rutTramitante = contrato.getTramitante().getRut();
		rutTramitante = utils.formatearRUT(rutTramitante);
		rutTramitante = rutTramitante.replace(".", "");
		rutTramitante = String.format("%11s", rutTramitante).replace(' ', '0');
		contrato.getTramitante().setRut(rutTramitante);
		
		String rutAfectado = contrato.getAfectado().getRut();
		rutAfectado = utils.formatearRUT(rutAfectado);
		rutAfectado = rutAfectado.replace(".", "");
		rutAfectado = String.format("%11s", rutAfectado).replace(' ', '0');
		contrato.getAfectado().setRut(rutAfectado);
		
		return teAvisaDao.contrataTeAvisa(contrato);
	}
	
	
	public ResponseDTO validaContratoTeAvisa(String rutAvisa){
		rutAvisa = utils.formatearRUT(rutAvisa);
		rutAvisa = rutAvisa.replace(".", "");
		rutAvisa = String.format("%11s", rutAvisa).replace(' ', '0');
		return teAvisaDao.validaContratoTeAvisa(rutAvisa);
	}
	
	
	public List<TeAvisaDTO> buscaContratos(String rut){
		rut = utils.formatearRUT(rut);
		rut = rut.replace(".", "");
		rut = String.format("%11s", rut).replace(' ', '0');
		return teAvisaDao.buscaContratos(rut);
	}

	public List<TeAvisaDTO> buscaContratosRenovacion(String rut, Integer criterio, Long corrContrato){
		if(rut != null && !rut.equals("")){
			rut = utils.formatearRUT(rut);
			rut = rut.replace(".", "");
			rut = String.format("%11s", rut).replace(' ', '0');
		}
		return teAvisaDao.buscaContratosRenovacion(rut, criterio, corrContrato);
	}
	
	public Long costoServicioUpgrade(Long corrContrato, String codServicio){
		return teAvisaDao.costoServicioUpgrade(corrContrato, codServicio);
	}
}
