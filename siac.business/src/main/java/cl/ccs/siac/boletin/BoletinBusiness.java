package cl.ccs.siac.boletin;

import java.util.Map;

import cl.exe.ccs.dto.BoletinInDTO;
import cl.exe.ccs.dto.BoletinDataDetalle;
import cl.exe.ccs.dto.BoletinDataEmpleador;
import cl.exe.ccs.dto.BoletinDataTrabajador;
import cl.exe.ccs.dto.BoletinOutDTO;

public interface BoletinBusiness {

	BoletinOutDTO<BoletinDataTrabajador> consultaTrabajador(BoletinInDTO params);

	Map<String, Object> obtenerMapaBLT(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataTrabajador> dataOut);

	BoletinOutDTO<BoletinDataDetalle> consultaInfractor(BoletinInDTO params);

	Map<String, Object> obtenerMapaBLI(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataDetalle> dataOut);

	BoletinOutDTO<BoletinDataEmpleador> consultaResumen(BoletinInDTO params);

	Map<String, Object> obtenerMapaBLR(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataEmpleador> dataOut);

}
