package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditElimProtestos database table.
 * 
 */
@Entity
@Table(name="Tbl_AuditElimProtestos")
@NamedQuery(name="Tbl_AuditElimProtesto.findAll", query="SELECT t FROM Tbl_AuditElimProtesto t")
public class Tbl_AuditElimProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodMonedaOriginal")
	private byte fld_CodMonedaOriginal;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioDigitacion")
	private String fld_CodUsuarioDigitacion;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecEliminacion")
	private Timestamp fld_FecEliminacion;

	@Column(name="Fld_FecMarcaAclaracion")
	private Timestamp fld_FecMarcaAclaracion;

	@Column(name="Fld_FecMarcaAclEspecial")
	private Timestamp fld_FecMarcaAclEspecial;

	@Column(name="Fld_FecMarcaAclEspecialAnulada")
	private Timestamp fld_FecMarcaAclEspecialAnulada;

	@Column(name="Fld_FecMarcaAclRectificacion")
	private Timestamp fld_FecMarcaAclRectificacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_Flag_6")
	private boolean fld_Flag_6;

	@Column(name="Fld_Flag_7")
	private boolean fld_Flag_7;

	@Column(name="Fld_Flag_8")
	private boolean fld_Flag_8;

	@Column(name="Fld_Flag_Ing3100")
	private boolean fld_Flag_Ing3100;

	@Column(name="Fld_Flag_MFecProtesto")
	private boolean fld_Flag_MFecProtesto;

	@Column(name="Fld_Flag_MMonto")
	private boolean fld_Flag_MMonto;

	@Column(name="Fld_Flag_MNroBolPub")
	private boolean fld_Flag_MNroBolPub;

	@Column(name="Fld_Flag_MNroOperacion")
	private boolean fld_Flag_MNroOperacion;

	@Column(name="Fld_Flag_MTipDoc")
	private boolean fld_Flag_MTipDoc;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_Marca3Dig")
	private boolean fld_Marca3Dig;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MarcaAclEspecial")
	private boolean fld_MarcaAclEspecial;

	@Column(name="Fld_MarcaAclEspecialAnulada")
	private boolean fld_MarcaAclEspecialAnulada;

	@Column(name="Fld_MarcaAclRectificacion")
	private boolean fld_MarcaAclRectificacion;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroLote")
	private double fld_NroLote;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_SdoCapital")
	private BigDecimal fld_SdoCapital;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoRelacion")
	private byte fld_TipoRelacion;

	@Column(name="Fld_TomoBoletin")
	private short fld_TomoBoletin;

	public Tbl_AuditElimProtesto() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_CodMonedaOriginal() {
		return this.fld_CodMonedaOriginal;
	}

	public void setFld_CodMonedaOriginal(byte fld_CodMonedaOriginal) {
		this.fld_CodMonedaOriginal = fld_CodMonedaOriginal;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioDigitacion() {
		return this.fld_CodUsuarioDigitacion;
	}

	public void setFld_CodUsuarioDigitacion(String fld_CodUsuarioDigitacion) {
		this.fld_CodUsuarioDigitacion = fld_CodUsuarioDigitacion;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecEliminacion() {
		return this.fld_FecEliminacion;
	}

	public void setFld_FecEliminacion(Timestamp fld_FecEliminacion) {
		this.fld_FecEliminacion = fld_FecEliminacion;
	}

	public Timestamp getFld_FecMarcaAclaracion() {
		return this.fld_FecMarcaAclaracion;
	}

	public void setFld_FecMarcaAclaracion(Timestamp fld_FecMarcaAclaracion) {
		this.fld_FecMarcaAclaracion = fld_FecMarcaAclaracion;
	}

	public Timestamp getFld_FecMarcaAclEspecial() {
		return this.fld_FecMarcaAclEspecial;
	}

	public void setFld_FecMarcaAclEspecial(Timestamp fld_FecMarcaAclEspecial) {
		this.fld_FecMarcaAclEspecial = fld_FecMarcaAclEspecial;
	}

	public Timestamp getFld_FecMarcaAclEspecialAnulada() {
		return this.fld_FecMarcaAclEspecialAnulada;
	}

	public void setFld_FecMarcaAclEspecialAnulada(Timestamp fld_FecMarcaAclEspecialAnulada) {
		this.fld_FecMarcaAclEspecialAnulada = fld_FecMarcaAclEspecialAnulada;
	}

	public Timestamp getFld_FecMarcaAclRectificacion() {
		return this.fld_FecMarcaAclRectificacion;
	}

	public void setFld_FecMarcaAclRectificacion(Timestamp fld_FecMarcaAclRectificacion) {
		this.fld_FecMarcaAclRectificacion = fld_FecMarcaAclRectificacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public boolean getFld_Flag_6() {
		return this.fld_Flag_6;
	}

	public void setFld_Flag_6(boolean fld_Flag_6) {
		this.fld_Flag_6 = fld_Flag_6;
	}

	public boolean getFld_Flag_7() {
		return this.fld_Flag_7;
	}

	public void setFld_Flag_7(boolean fld_Flag_7) {
		this.fld_Flag_7 = fld_Flag_7;
	}

	public boolean getFld_Flag_8() {
		return this.fld_Flag_8;
	}

	public void setFld_Flag_8(boolean fld_Flag_8) {
		this.fld_Flag_8 = fld_Flag_8;
	}

	public boolean getFld_Flag_Ing3100() {
		return this.fld_Flag_Ing3100;
	}

	public void setFld_Flag_Ing3100(boolean fld_Flag_Ing3100) {
		this.fld_Flag_Ing3100 = fld_Flag_Ing3100;
	}

	public boolean getFld_Flag_MFecProtesto() {
		return this.fld_Flag_MFecProtesto;
	}

	public void setFld_Flag_MFecProtesto(boolean fld_Flag_MFecProtesto) {
		this.fld_Flag_MFecProtesto = fld_Flag_MFecProtesto;
	}

	public boolean getFld_Flag_MMonto() {
		return this.fld_Flag_MMonto;
	}

	public void setFld_Flag_MMonto(boolean fld_Flag_MMonto) {
		this.fld_Flag_MMonto = fld_Flag_MMonto;
	}

	public boolean getFld_Flag_MNroBolPub() {
		return this.fld_Flag_MNroBolPub;
	}

	public void setFld_Flag_MNroBolPub(boolean fld_Flag_MNroBolPub) {
		this.fld_Flag_MNroBolPub = fld_Flag_MNroBolPub;
	}

	public boolean getFld_Flag_MNroOperacion() {
		return this.fld_Flag_MNroOperacion;
	}

	public void setFld_Flag_MNroOperacion(boolean fld_Flag_MNroOperacion) {
		this.fld_Flag_MNroOperacion = fld_Flag_MNroOperacion;
	}

	public boolean getFld_Flag_MTipDoc() {
		return this.fld_Flag_MTipDoc;
	}

	public void setFld_Flag_MTipDoc(boolean fld_Flag_MTipDoc) {
		this.fld_Flag_MTipDoc = fld_Flag_MTipDoc;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public boolean getFld_Marca3Dig() {
		return this.fld_Marca3Dig;
	}

	public void setFld_Marca3Dig(boolean fld_Marca3Dig) {
		this.fld_Marca3Dig = fld_Marca3Dig;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public boolean getFld_MarcaAclEspecial() {
		return this.fld_MarcaAclEspecial;
	}

	public void setFld_MarcaAclEspecial(boolean fld_MarcaAclEspecial) {
		this.fld_MarcaAclEspecial = fld_MarcaAclEspecial;
	}

	public boolean getFld_MarcaAclEspecialAnulada() {
		return this.fld_MarcaAclEspecialAnulada;
	}

	public void setFld_MarcaAclEspecialAnulada(boolean fld_MarcaAclEspecialAnulada) {
		this.fld_MarcaAclEspecialAnulada = fld_MarcaAclEspecialAnulada;
	}

	public boolean getFld_MarcaAclRectificacion() {
		return this.fld_MarcaAclRectificacion;
	}

	public void setFld_MarcaAclRectificacion(boolean fld_MarcaAclRectificacion) {
		this.fld_MarcaAclRectificacion = fld_MarcaAclRectificacion;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public double getFld_NroLote() {
		return this.fld_NroLote;
	}

	public void setFld_NroLote(double fld_NroLote) {
		this.fld_NroLote = fld_NroLote;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public BigDecimal getFld_SdoCapital() {
		return this.fld_SdoCapital;
	}

	public void setFld_SdoCapital(BigDecimal fld_SdoCapital) {
		this.fld_SdoCapital = fld_SdoCapital;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoRelacion() {
		return this.fld_TipoRelacion;
	}

	public void setFld_TipoRelacion(byte fld_TipoRelacion) {
		this.fld_TipoRelacion = fld_TipoRelacion;
	}

	public short getFld_TomoBoletin() {
		return this.fld_TomoBoletin;
	}

	public void setFld_TomoBoletin(short fld_TomoBoletin) {
		this.fld_TomoBoletin = fld_TomoBoletin;
	}

}