package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.tramitante.TramitanteBusiness;
import cl.ccs.siac.transaccion.TransaccionBusiness;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ServicioDTO;
import cl.exe.ccs.dto.TramitanteDTO;
import cl.exe.ccs.dto.TransaccionGrillaDTO;

@Path("/carrocompra")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class CarroCompraServicio {

	private static final Logger log = LogManager.getLogger(CarroCompraServicio.class);

	@Inject
	private TramitanteBusiness tramitanteBusiness;

	@Inject
	private CombosBusiness combosBusiness;

	@Inject
	private TransaccionBusiness transaccionBusiness;

	@GET
	@Path("/cargaservicios")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cargaTramitante(@Context HttpServletRequest request, TramitanteDTO tramitanteDTO) throws ServletException {
		try {

			Principal userPrincipal = request.getUserPrincipal();
			if (userPrincipal == null || userPrincipal.getName() == null) {
				log.error("Error en el metodo: cargaTramitante - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}


			List<ServicioDTO> servicios = combosBusiness.obtenerServicios();
			return Response.ok(servicios).build();

		} catch (Exception ex) {
			log.error("Metodo cargaTramitante - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/eliminaservicios")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response eliminaServicio(@Context HttpServletRequest request, TransaccionGrillaDTO servicio) throws ServletException {
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: eliminaServicio - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			System.out.println("servicios");

			ResponseDTO respuesta = transaccionBusiness.eliminarServicio(servicio.getCorrCaja(), Long.parseLong(servicio.getCorrelativoMovimiento()));

			return Response.ok(respuesta).build();

		} catch (Exception ex) {
			log.error("Metodo eliminaServicio - Corr Caja:" + servicio.getCorrCaja() + "Corr Movimiento: " + servicio.getCorrelativoMovimiento());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
