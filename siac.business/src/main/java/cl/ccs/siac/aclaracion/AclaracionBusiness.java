package cl.ccs.siac.aclaracion;

import java.util.List;

import cl.ccs.siac.sybase.entities.Tbl_Firma;
import cl.exe.ccs.dto.AclaracionDTO;
import cl.exe.ccs.dto.BipersonalDTO;
import cl.exe.ccs.dto.ConsultaCAADTO;
import cl.exe.ccs.dto.DetalleFirmaDTO;
import cl.exe.ccs.dto.NumeroConfirmatorioDTO;
import cl.exe.ccs.dto.NumeroFirmasDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.VentaCarteraDTO;

public interface AclaracionBusiness {

	
	public ResponseListDTO<DetalleFirmaDTO> consultaFirmas(DetalleFirmaDTO detalle);
	
	public Tbl_Firma obtieneFirma(Long corrFirma);
	
	public NumeroConfirmatorioDTO confimaNumero(NumeroConfirmatorioDTO numero);
	
	public  NumeroFirmasDTO obtieneConfirmatorio(NumeroFirmasDTO numero);
	
	public  AclaracionDTO aceptaAclaracion(AclaracionDTO aclaracion);
	
	public  AclaracionDTO aclaracionTemp(AclaracionDTO aclaracion);
	
	public  List<AclaracionDTO> listaCandidatos(AclaracionDTO aclaracion);
	
	public  AclaracionDTO desDuplica(AclaracionDTO aclaracion);
	
	public  List<AclaracionDTO> buscaCuotasBancoEstado(AclaracionDTO aclaracion);
	
	public AclaracionDTO tieneVentaCartera(AclaracionDTO aclaracion);
	
	public VentaCarteraDTO buscaProtestoVentaCartera(Long corrProt, String tipoEmisor, String emisor);
	
	public ResponseDTO eliminaProtestoTemp(Long corrProt, Long corrCaja);
	
	public ResponseDTO guardaBipersonalTemp(BipersonalDTO bipersonal);
	
	public ResponseListDTO<PersonaDTO> listaAclaraciones(Long corrCaja, String rutTramitante);

	public ResponseListDTO<ConsultaCAADTO> consultaRutFecha(ConsultaCAADTO consulta);
	
	public AclaracionDTO aclaracionesParaCertificado(AclaracionDTO aclaracion);
	
	public ResponseDTO validaAclaracionCarro(AclaracionDTO aclaracion);
}
