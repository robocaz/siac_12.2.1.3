package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NumeroConfirmatorioDTO extends ResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3555552094449323477L;
	private Long serie;
	private Long nroConfirmatorio;
	private Long folioNro;
	private String tipoEmisor;
	private String codEmisor;
	private Long codSucursal;
	private Long corrVigNC;
	
	
	
	public Long getNroConfirmatorio() {
		return nroConfirmatorio;
	}
	public void setNroConfirmatorio(Long nroConfirmatorio) {
		this.nroConfirmatorio = nroConfirmatorio;
	}
	public Long getFolioNro() {
		return folioNro;
	}
	public void setFolioNro(Long folioNro) {
		this.folioNro = folioNro;
	}
	public String getTipoEmisor() {
		return tipoEmisor;
	}
	public void setTipoEmisor(String tipoEmisor) {
		this.tipoEmisor = tipoEmisor;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public Long getCodSucursal() {
		return codSucursal;
	}
	public void setCodSucursal(Long codSucursal) {
		this.codSucursal = codSucursal;
	}
	public Long getCorrVigNC() {
		return corrVigNC;
	}
	public void setCorrVigNC(Long corrVigNC) {
		this.corrVigNC = corrVigNC;
	}
	public Long getSerie() {
		return serie;
	}
	public void setSerie(Long serie) {
		this.serie = serie;
	}
	
	
	
	

	
}
