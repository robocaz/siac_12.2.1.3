package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the SAC_corr database table.
 * 
 */
@Entity
@NamedQuery(name="SAC_corr.findAll", query="SELECT s FROM SAC_corr s")
public class SAC_corr implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal corr;

	private short tr;

	public SAC_corr() {
	}

	public BigDecimal getCorr() {
		return this.corr;
	}

	public void setCorr(BigDecimal corr) {
		this.corr = corr;
	}

	public short getTr() {
		return this.tr;
	}

	public void setTr(short tr) {
		this.tr = tr;
	}

}