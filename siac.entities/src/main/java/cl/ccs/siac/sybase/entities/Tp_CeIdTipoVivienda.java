package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdTipoViviendas database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdTipoViviendas")
@NamedQuery(name="Tp_CeIdTipoVivienda.findAll", query="SELECT t FROM Tp_CeIdTipoVivienda t")
public class Tp_CeIdTipoVivienda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoVivienda")
	private String fld_GlosaTipoVivienda;

	@Column(name="Fld_TipoVivienda")
	private short fld_TipoVivienda;

	public Tp_CeIdTipoVivienda() {
	}

	public String getFld_GlosaTipoVivienda() {
		return this.fld_GlosaTipoVivienda;
	}

	public void setFld_GlosaTipoVivienda(String fld_GlosaTipoVivienda) {
		this.fld_GlosaTipoVivienda = fld_GlosaTipoVivienda;
	}

	public short getFld_TipoVivienda() {
		return this.fld_TipoVivienda;
	}

	public void setFld_TipoVivienda(short fld_TipoVivienda) {
		this.fld_TipoVivienda = fld_TipoVivienda;
	}

}