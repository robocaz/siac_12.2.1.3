package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_FusionEmisores database table.
 * 
 */
@Entity
@Table(name="Tbl_FusionEmisores")
@NamedQuery(name="Tbl_FusionEmisore.findAll", query="SELECT t FROM Tbl_FusionEmisore t")
public class Tbl_FusionEmisore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisorHijo")
	private String fld_CodEmisorHijo;

	@Column(name="Fld_CodEmisorPadre")
	private String fld_CodEmisorPadre;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisorHijo")
	private String fld_TipoEmisorHijo;

	@Column(name="Fld_TipoEmisorPadre")
	private String fld_TipoEmisorPadre;

	public Tbl_FusionEmisore() {
	}

	public String getFld_CodEmisorHijo() {
		return this.fld_CodEmisorHijo;
	}

	public void setFld_CodEmisorHijo(String fld_CodEmisorHijo) {
		this.fld_CodEmisorHijo = fld_CodEmisorHijo;
	}

	public String getFld_CodEmisorPadre() {
		return this.fld_CodEmisorPadre;
	}

	public void setFld_CodEmisorPadre(String fld_CodEmisorPadre) {
		this.fld_CodEmisorPadre = fld_CodEmisorPadre;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisorHijo() {
		return this.fld_TipoEmisorHijo;
	}

	public void setFld_TipoEmisorHijo(String fld_TipoEmisorHijo) {
		this.fld_TipoEmisorHijo = fld_TipoEmisorHijo;
	}

	public String getFld_TipoEmisorPadre() {
		return this.fld_TipoEmisorPadre;
	}

	public void setFld_TipoEmisorPadre(String fld_TipoEmisorPadre) {
		this.fld_TipoEmisorPadre = fld_TipoEmisorPadre;
	}

}