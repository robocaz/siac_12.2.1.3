package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CCosto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CCosto.findAll", query="SELECT t FROM Tbl_CCosto t")
public class Tbl_CCosto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CargoContacto")
	private String fld_CargoContacto;

	@Column(name="Fld_CCostoERP")
	private short fld_CCostoERP;

	@Column(name="Fld_ClasificacionCCosto")
	private boolean fld_ClasificacionCCosto;

	@Column(name="Fld_ClasificacionRetencion")
	private boolean fld_ClasificacionRetencion;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CostoOperacional")
	private BigDecimal fld_CostoOperacional;

	@Column(name="Fld_DireccionContacto")
	private String fld_DireccionContacto;

	@Column(name="Fld_FonoContacto")
	private String fld_FonoContacto;

	@Column(name="Fld_GlosaCCosto")
	private String fld_GlosaCCosto;

	@Column(name="Fld_MarcaAfectoIva")
	private boolean fld_MarcaAfectoIva;

	@Column(name="Fld_NomContacto_ApMat")
	private String fld_NomContacto_ApMat;

	@Column(name="Fld_NomContacto_ApPat")
	private String fld_NomContacto_ApPat;

	@Column(name="Fld_NomContacto_Nombres")
	private String fld_NomContacto_Nombres;

	@Column(name="Fld_RazonSocialCCosto")
	private String fld_RazonSocialCCosto;

	@Column(name="Fld_RutAgente")
	private String fld_RutAgente;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorMinimoCheque")
	private BigDecimal fld_ValorMinimoCheque;

	public Tbl_CCosto() {
	}

	public String getFld_CargoContacto() {
		return this.fld_CargoContacto;
	}

	public void setFld_CargoContacto(String fld_CargoContacto) {
		this.fld_CargoContacto = fld_CargoContacto;
	}

	public short getFld_CCostoERP() {
		return this.fld_CCostoERP;
	}

	public void setFld_CCostoERP(short fld_CCostoERP) {
		this.fld_CCostoERP = fld_CCostoERP;
	}

	public boolean getFld_ClasificacionCCosto() {
		return this.fld_ClasificacionCCosto;
	}

	public void setFld_ClasificacionCCosto(boolean fld_ClasificacionCCosto) {
		this.fld_ClasificacionCCosto = fld_ClasificacionCCosto;
	}

	public boolean getFld_ClasificacionRetencion() {
		return this.fld_ClasificacionRetencion;
	}

	public void setFld_ClasificacionRetencion(boolean fld_ClasificacionRetencion) {
		this.fld_ClasificacionRetencion = fld_ClasificacionRetencion;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public BigDecimal getFld_CostoOperacional() {
		return this.fld_CostoOperacional;
	}

	public void setFld_CostoOperacional(BigDecimal fld_CostoOperacional) {
		this.fld_CostoOperacional = fld_CostoOperacional;
	}

	public String getFld_DireccionContacto() {
		return this.fld_DireccionContacto;
	}

	public void setFld_DireccionContacto(String fld_DireccionContacto) {
		this.fld_DireccionContacto = fld_DireccionContacto;
	}

	public String getFld_FonoContacto() {
		return this.fld_FonoContacto;
	}

	public void setFld_FonoContacto(String fld_FonoContacto) {
		this.fld_FonoContacto = fld_FonoContacto;
	}

	public String getFld_GlosaCCosto() {
		return this.fld_GlosaCCosto;
	}

	public void setFld_GlosaCCosto(String fld_GlosaCCosto) {
		this.fld_GlosaCCosto = fld_GlosaCCosto;
	}

	public boolean getFld_MarcaAfectoIva() {
		return this.fld_MarcaAfectoIva;
	}

	public void setFld_MarcaAfectoIva(boolean fld_MarcaAfectoIva) {
		this.fld_MarcaAfectoIva = fld_MarcaAfectoIva;
	}

	public String getFld_NomContacto_ApMat() {
		return this.fld_NomContacto_ApMat;
	}

	public void setFld_NomContacto_ApMat(String fld_NomContacto_ApMat) {
		this.fld_NomContacto_ApMat = fld_NomContacto_ApMat;
	}

	public String getFld_NomContacto_ApPat() {
		return this.fld_NomContacto_ApPat;
	}

	public void setFld_NomContacto_ApPat(String fld_NomContacto_ApPat) {
		this.fld_NomContacto_ApPat = fld_NomContacto_ApPat;
	}

	public String getFld_NomContacto_Nombres() {
		return this.fld_NomContacto_Nombres;
	}

	public void setFld_NomContacto_Nombres(String fld_NomContacto_Nombres) {
		this.fld_NomContacto_Nombres = fld_NomContacto_Nombres;
	}

	public String getFld_RazonSocialCCosto() {
		return this.fld_RazonSocialCCosto;
	}

	public void setFld_RazonSocialCCosto(String fld_RazonSocialCCosto) {
		this.fld_RazonSocialCCosto = fld_RazonSocialCCosto;
	}

	public String getFld_RutAgente() {
		return this.fld_RutAgente;
	}

	public void setFld_RutAgente(String fld_RutAgente) {
		this.fld_RutAgente = fld_RutAgente;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorMinimoCheque() {
		return this.fld_ValorMinimoCheque;
	}

	public void setFld_ValorMinimoCheque(BigDecimal fld_ValorMinimoCheque) {
		this.fld_ValorMinimoCheque = fld_ValorMinimoCheque;
	}

}