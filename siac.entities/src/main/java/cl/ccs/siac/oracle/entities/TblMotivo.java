package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_MOTIVO database table.
 * 
 */
@Entity
@Table(name="TBL_MOTIVO")
@NamedQuery(name="TblMotivo.findAll", query="SELECT t FROM TblMotivo t")
public class TblMotivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODMOTIVO")
	private BigDecimal fldCodmotivo;

	@Column(name="FLD_CODNEMOTECNICO")
	private String fldCodnemotecnico;

	@Column(name="FLD_GLOSAMOTIVO")
	private String fldGlosamotivo;

	public TblMotivo() {
	}

	public BigDecimal getFldCodmotivo() {
		return this.fldCodmotivo;
	}

	public void setFldCodmotivo(BigDecimal fldCodmotivo) {
		this.fldCodmotivo = fldCodmotivo;
	}

	public String getFldCodnemotecnico() {
		return this.fldCodnemotecnico;
	}

	public void setFldCodnemotecnico(String fldCodnemotecnico) {
		this.fldCodnemotecnico = fldCodnemotecnico;
	}

	public String getFldGlosamotivo() {
		return this.fldGlosamotivo;
	}

	public void setFldGlosamotivo(String fldGlosamotivo) {
		this.fldGlosamotivo = fldGlosamotivo;
	}

}