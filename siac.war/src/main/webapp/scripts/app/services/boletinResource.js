angular.module('ccs').factory('BoletinResource', function ($resource) {
	var resource = $resource('rest/boletin/', { nocache : new Date().getTime()},
	{
		'pdf' : { method:'GET', isArray:false, url:'rest/boletin/pdf' }
	});
	return resource;
});
