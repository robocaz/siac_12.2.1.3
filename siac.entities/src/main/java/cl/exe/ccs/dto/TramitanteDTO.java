package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class TramitanteDTO extends ResponseDTO implements Serializable{
    
    private String rut;
    private String apellidoPaterno;
    private String apellidoMaterno ; 
    private String nombres ;
    private Boolean declaracionUso;
    private String telefono;
    private String direccion;
    private String giro;
    private Integer correlativo;
    private ComboDTO region;
    private ComboDTO comuna;
    private String email;
    private String usuario;
    private Long corrCaja;
    private Boolean tieneDeclaracionUso;
    
	public ComboDTO getRegion() {
		return region;
	}
	public void setRegion(ComboDTO region) {
		this.region = region;
	}
	public ComboDTO getComuna() {
		return comuna;
	}
	public void setComuna(ComboDTO comuna) {
		this.comuna = comuna;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public Boolean getDeclaracionUso() {
		return declaracionUso;
	}
	public void setDeclaracionUso(Boolean declaracionUso) {
		this.declaracionUso = declaracionUso;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getGiro() {
		return giro;
	}
	public Integer getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Boolean getTieneDeclaracionUso() {
		return tieneDeclaracionUso;
	}
	public void setTieneDeclaracionUso(Boolean tieneDeclaracionUso) {
		this.tieneDeclaracionUso = tieneDeclaracionUso;
	}
    
 
}

