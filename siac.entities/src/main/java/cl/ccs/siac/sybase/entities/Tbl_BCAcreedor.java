package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCAcreedor database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCAcreedor.findAll", query="SELECT t FROM Tbl_BCAcreedor t")
public class Tbl_BCAcreedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrAcreedor_Id")
	private BigDecimal fld_CorrAcreedor_Id;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProcedimiento")
	private BigDecimal fld_CorrProcedimiento;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecVigencia")
	private Timestamp fld_FecVigencia;

	@Column(name="Fld_MontoDeuda")
	private BigDecimal fld_MontoDeuda;

	@Column(name="Fld_NaturalezaCredito")
	private String fld_NaturalezaCredito;

	@Column(name="Fld_PorcentajeMonto")
	private BigDecimal fld_PorcentajeMonto;

	@Column(name="Fld_RutAcreedor")
	private String fld_RutAcreedor;

	@Column(name="Fld_Vigencia")
	private boolean fld_Vigencia;

	public Tbl_BCAcreedor() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrAcreedor_Id() {
		return this.fld_CorrAcreedor_Id;
	}

	public void setFld_CorrAcreedor_Id(BigDecimal fld_CorrAcreedor_Id) {
		this.fld_CorrAcreedor_Id = fld_CorrAcreedor_Id;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProcedimiento() {
		return this.fld_CorrProcedimiento;
	}

	public void setFld_CorrProcedimiento(BigDecimal fld_CorrProcedimiento) {
		this.fld_CorrProcedimiento = fld_CorrProcedimiento;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecVigencia() {
		return this.fld_FecVigencia;
	}

	public void setFld_FecVigencia(Timestamp fld_FecVigencia) {
		this.fld_FecVigencia = fld_FecVigencia;
	}

	public BigDecimal getFld_MontoDeuda() {
		return this.fld_MontoDeuda;
	}

	public void setFld_MontoDeuda(BigDecimal fld_MontoDeuda) {
		this.fld_MontoDeuda = fld_MontoDeuda;
	}

	public String getFld_NaturalezaCredito() {
		return this.fld_NaturalezaCredito;
	}

	public void setFld_NaturalezaCredito(String fld_NaturalezaCredito) {
		this.fld_NaturalezaCredito = fld_NaturalezaCredito;
	}

	public BigDecimal getFld_PorcentajeMonto() {
		return this.fld_PorcentajeMonto;
	}

	public void setFld_PorcentajeMonto(BigDecimal fld_PorcentajeMonto) {
		this.fld_PorcentajeMonto = fld_PorcentajeMonto;
	}

	public String getFld_RutAcreedor() {
		return this.fld_RutAcreedor;
	}

	public void setFld_RutAcreedor(String fld_RutAcreedor) {
		this.fld_RutAcreedor = fld_RutAcreedor;
	}

	public boolean getFld_Vigencia() {
		return this.fld_Vigencia;
	}

	public void setFld_Vigencia(boolean fld_Vigencia) {
		this.fld_Vigencia = fld_Vigencia;
	}

}