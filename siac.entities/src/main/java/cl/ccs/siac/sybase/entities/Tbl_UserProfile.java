package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UserProfile database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UserProfile.findAll", query="SELECT t FROM Tbl_UserProfile t")
public class Tbl_UserProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ProfileName")
	private String fld_ProfileName;

	@Column(name="Fld_UserCode")
	private String fld_UserCode;

	public Tbl_UserProfile() {
	}

	public String getFld_ProfileName() {
		return this.fld_ProfileName;
	}

	public void setFld_ProfileName(String fld_ProfileName) {
		this.fld_ProfileName = fld_ProfileName;
	}

	public String getFld_UserCode() {
		return this.fld_UserCode;
	}

	public void setFld_UserCode(String fld_UserCode) {
		this.fld_UserCode = fld_UserCode;
	}

}