package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_FnqProrroga database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FnqProrroga.findAll", query="SELECT t FROM Tbl_FnqProrroga t")
public class Tbl_FnqProrroga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CorrFiniquito")
	private BigDecimal fld_CorrFiniquito;

	@Column(name="Fld_CorrNomTram")
	private BigDecimal fld_CorrNomTram;

	@Column(name="Fld_FecDecJurada")
	private Timestamp fld_FecDecJurada;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_RutTramitante")
	private String fld_RutTramitante;

	public Tbl_FnqProrroga() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrFiniquito() {
		return this.fld_CorrFiniquito;
	}

	public void setFld_CorrFiniquito(BigDecimal fld_CorrFiniquito) {
		this.fld_CorrFiniquito = fld_CorrFiniquito;
	}

	public BigDecimal getFld_CorrNomTram() {
		return this.fld_CorrNomTram;
	}

	public void setFld_CorrNomTram(BigDecimal fld_CorrNomTram) {
		this.fld_CorrNomTram = fld_CorrNomTram;
	}

	public Timestamp getFld_FecDecJurada() {
		return this.fld_FecDecJurada;
	}

	public void setFld_FecDecJurada(Timestamp fld_FecDecJurada) {
		this.fld_FecDecJurada = fld_FecDecJurada;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public String getFld_RutTramitante() {
		return this.fld_RutTramitante;
	}

	public void setFld_RutTramitante(String fld_RutTramitante) {
		this.fld_RutTramitante = fld_RutTramitante;
	}

}