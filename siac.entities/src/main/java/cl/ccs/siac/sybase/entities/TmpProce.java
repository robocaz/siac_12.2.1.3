package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_proce database table.
 * 
 */
@Entity
@Table(name="tmp_proce")
@NamedQuery(name="TmpProce.findAll", query="SELECT t FROM TmpProce t")
public class TmpProce implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;

	public TmpProce() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}