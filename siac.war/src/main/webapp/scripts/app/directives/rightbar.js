'use strict';
angular.module('ccs').directive('rightbar',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/rightbar.html?nocache='+ new Date().getTime()
    };
});
