package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_PERFIL database table.
 * 
 */
@Entity
@Table(name="TBL_PERFIL")
@NamedQuery(name="TblPerfil.findAll", query="SELECT t FROM TblPerfil t")
public class TblPerfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_PERFIL_FLDCORRPERFILID_GENERATOR", sequenceName="FLD_CORRPERFIL_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_PERFIL_FLDCORRPERFILID_GENERATOR")
	@Column(name="FLD_CORRPERFIL_ID")
	private long fldCorrperfilId;

	@Column(name="FLD_DESCRIPCION")
	private String fldDescripcion;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_PERFILCONSULTA")
	private String fldPerfilconsulta;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblTipoperfil
	@ManyToOne
	@JoinColumn(name="FLD_TIPOPERFIL")
	private TblTipoperfil tblTipoperfil;

	//bi-directional many-to-one association to TblPerfilconsulta
	@OneToMany(mappedBy="tblPerfil")
	private List<TblPerfilconsulta> tblPerfilconsultas;

	//bi-directional many-to-one association to TblUsuariomol
	@OneToMany(mappedBy="tblPerfil")
	private List<TblUsuariomol> tblUsuariomols;

	public TblPerfil() {
	}

	public long getFldCorrperfilId() {
		return this.fldCorrperfilId;
	}

	public void setFldCorrperfilId(long fldCorrperfilId) {
		this.fldCorrperfilId = fldCorrperfilId;
	}

	public String getFldDescripcion() {
		return this.fldDescripcion;
	}

	public void setFldDescripcion(String fldDescripcion) {
		this.fldDescripcion = fldDescripcion;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldPerfilconsulta() {
		return this.fldPerfilconsulta;
	}

	public void setFldPerfilconsulta(String fldPerfilconsulta) {
		this.fldPerfilconsulta = fldPerfilconsulta;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public TblTipoperfil getTblTipoperfil() {
		return this.tblTipoperfil;
	}

	public void setTblTipoperfil(TblTipoperfil tblTipoperfil) {
		this.tblTipoperfil = tblTipoperfil;
	}

	public List<TblPerfilconsulta> getTblPerfilconsultas() {
		return this.tblPerfilconsultas;
	}

	public void setTblPerfilconsultas(List<TblPerfilconsulta> tblPerfilconsultas) {
		this.tblPerfilconsultas = tblPerfilconsultas;
	}

	public TblPerfilconsulta addTblPerfilconsulta(TblPerfilconsulta tblPerfilconsulta) {
		getTblPerfilconsultas().add(tblPerfilconsulta);
		tblPerfilconsulta.setTblPerfil(this);

		return tblPerfilconsulta;
	}

	public TblPerfilconsulta removeTblPerfilconsulta(TblPerfilconsulta tblPerfilconsulta) {
		getTblPerfilconsultas().remove(tblPerfilconsulta);
		tblPerfilconsulta.setTblPerfil(null);

		return tblPerfilconsulta;
	}

	public List<TblUsuariomol> getTblUsuariomols() {
		return this.tblUsuariomols;
	}

	public void setTblUsuariomols(List<TblUsuariomol> tblUsuariomols) {
		this.tblUsuariomols = tblUsuariomols;
	}

	public TblUsuariomol addTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().add(tblUsuariomol);
		tblUsuariomol.setTblPerfil(this);

		return tblUsuariomol;
	}

	public TblUsuariomol removeTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().remove(tblUsuariomol);
		tblUsuariomol.setTblPerfil(null);

		return tblUsuariomol;
	}

}