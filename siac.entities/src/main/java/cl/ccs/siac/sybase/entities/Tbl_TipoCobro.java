package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoCobro database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoCobro.findAll", query="SELECT t FROM Tbl_TipoCobro t")
public class Tbl_TipoCobro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoCobro")
	private String fld_GlosaTipoCobro;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoCobro")
	private String fld_TipoCobro;

	public Tbl_TipoCobro() {
	}

	public String getFld_GlosaTipoCobro() {
		return this.fld_GlosaTipoCobro;
	}

	public void setFld_GlosaTipoCobro(String fld_GlosaTipoCobro) {
		this.fld_GlosaTipoCobro = fld_GlosaTipoCobro;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoCobro() {
		return this.fld_TipoCobro;
	}

	public void setFld_TipoCobro(String fld_TipoCobro) {
		this.fld_TipoCobro = fld_TipoCobro;
	}

}