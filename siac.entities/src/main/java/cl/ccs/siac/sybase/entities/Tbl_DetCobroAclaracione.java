package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetCobroAclaraciones database table.
 * 
 */
@Entity
@Table(name="Tbl_DetCobroAclaraciones")
@NamedQuery(name="Tbl_DetCobroAclaracione.findAll", query="SELECT t FROM Tbl_DetCobroAclaracione t")
public class Tbl_DetCobroAclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrTablaAcl")
	private BigDecimal fld_CorrTablaAcl;

	@Column(name="Fld_CostoTramo")
	private BigDecimal fld_CostoTramo;

	@Column(name="Fld_MontoDesde")
	private double fld_MontoDesde;

	@Column(name="Fld_MontoHasta")
	private double fld_MontoHasta;

	@Column(name="Fld_PorcentajeCobro")
	private BigDecimal fld_PorcentajeCobro;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoTramo")
	private int fld_TipoTramo;

	public Tbl_DetCobroAclaracione() {
	}

	public BigDecimal getFld_CorrTablaAcl() {
		return this.fld_CorrTablaAcl;
	}

	public void setFld_CorrTablaAcl(BigDecimal fld_CorrTablaAcl) {
		this.fld_CorrTablaAcl = fld_CorrTablaAcl;
	}

	public BigDecimal getFld_CostoTramo() {
		return this.fld_CostoTramo;
	}

	public void setFld_CostoTramo(BigDecimal fld_CostoTramo) {
		this.fld_CostoTramo = fld_CostoTramo;
	}

	public double getFld_MontoDesde() {
		return this.fld_MontoDesde;
	}

	public void setFld_MontoDesde(double fld_MontoDesde) {
		this.fld_MontoDesde = fld_MontoDesde;
	}

	public double getFld_MontoHasta() {
		return this.fld_MontoHasta;
	}

	public void setFld_MontoHasta(double fld_MontoHasta) {
		this.fld_MontoHasta = fld_MontoHasta;
	}

	public BigDecimal getFld_PorcentajeCobro() {
		return this.fld_PorcentajeCobro;
	}

	public void setFld_PorcentajeCobro(BigDecimal fld_PorcentajeCobro) {
		this.fld_PorcentajeCobro = fld_PorcentajeCobro;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public int getFld_TipoTramo() {
		return this.fld_TipoTramo;
	}

	public void setFld_TipoTramo(int fld_TipoTramo) {
		this.fld_TipoTramo = fld_TipoTramo;
	}

}