package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.exe.ccs.dto.ImpresoraDTO;
import cl.exe.ccs.dto.TarifaDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/listado")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class ListadosServicio {

	private static final Logger log = LogManager.getLogger(ListadosServicio.class);

	@Inject
	private CombosBusiness combosBusiness;

	@GET
	@Path("/tarifas")
	@Produces("application/json")
	@Consumes("application/json")
	public Response obtenerTarifas(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			List<TarifaDTO> tarifas = combosBusiness.obtenerTarifas();
			if (tarifas != null && tarifas.size() > 0) {
				return Response.ok(tarifas).build();
			} else {

				log.warn("Listar Tarifas - Message: Error del Sistema - Problemas al cargar listado");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Listar Tarifas - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("/impresoras")
	@Produces("application/json")
	@Consumes("application/json")
	public Response obtenerImpresoras(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			List<ImpresoraDTO> impresoras = combosBusiness
					.obtenerImpresoras(Integer.parseInt(dto.getCentroCostoActivo().getCodigo()));
			if (impresoras != null && impresoras.size() > 0) {
				return Response.ok(impresoras).build();
			} else {

				log.warn("Listar Impresoras - Message: Error del Sistema - Problemas al cargar listado");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Listar Impresoras - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
