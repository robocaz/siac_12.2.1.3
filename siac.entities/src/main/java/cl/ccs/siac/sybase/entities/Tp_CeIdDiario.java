package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdDiarios database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdDiarios")
@NamedQuery(name="Tp_CeIdDiario.findAll", query="SELECT t FROM Tp_CeIdDiario t")
public class Tp_CeIdDiario implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tp_CeIdDiarioPK id;

	@Column(name="Fld_GlosaDiario")
	private String fld_GlosaDiario;

	public Tp_CeIdDiario() {
	}

	public Tp_CeIdDiarioPK getId() {
		return this.id;
	}

	public void setId(Tp_CeIdDiarioPK id) {
		this.id = id;
	}

	public String getFld_GlosaDiario() {
		return this.fld_GlosaDiario;
	}

	public void setFld_GlosaDiario(String fld_GlosaDiario) {
		this.fld_GlosaDiario = fld_GlosaDiario;
	}

}