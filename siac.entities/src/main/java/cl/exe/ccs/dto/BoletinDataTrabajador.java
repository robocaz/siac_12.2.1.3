package cl.exe.ccs.dto;

import java.io.Serializable;

public class BoletinDataTrabajador extends BoletinDataGeneric implements Comparable<BoletinDataTrabajador>,Serializable {

	@Override
	public int compareTo(BoletinDataTrabajador o) {
		final int val = super.nombreInstitucion.compareTo(o.nombreInstitucion);
		if ( val < 0 ) {
			return -1;
		}
		if ( val > 0 ) {
			return 1;
		}
		if ( periodo < o.periodo ) {
			return -1;
		}
		if ( periodo > o.periodo ) {
			return 1;
		}
		return 0;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n rut: " + rut);
		str.append(" ,nombre: " + nombre);
		str.append(" ,periodo: " + periodo);
		str.append(" ,montoAdeudado: " + montoAdeudado);
		str.append(" ,montoLaboralUTM: " + montoLaboralUTM);
		str.append(" ,nombreInstitucion: " + nombreInstitucion);
		str.append(" ,tipoDeudaLaboral: " + tipoDeudaLaboral);
		return str.toString();
	}

}
