package cl.ccs.siac.servicios;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.depositos.DepositosBusiness;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.DepositoDTO;
import cl.exe.ccs.dto.PDFDepositosDTO;
import cl.exe.ccs.dto.PDFResponseDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Path("/depositos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class DepositosServicio {

	private static final Logger LOG = LogManager.getLogger(DepositosServicio.class);

	@Inject
	private DepositosBusiness depositosBusiness;

	@Inject
	private CombosBusiness combosBusiness;

	@Inject
	private Utils utils;

	private boolean validaUsuario(HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if ( userPrincipal == null || userPrincipal.getName() == null ) {
			return false;
		}
		return true;
	}

	@GET
	@Path("/cmbbancos")
	public Response cmbbancos(@Context HttpServletRequest request) {
		LOG.info("-> cmbbancos..");
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			List<ComboDTO> comboBox = combosBusiness.cmbBancos();
			
			if ( comboBox == null || comboBox.size() == 0 ) {
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(comboBox).build();
			}
		} catch (Exception e) {
			LOG.error("Metodos cmbbancos - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@GET
	@Path("/cmbctasctes")
	public Response cmbctasctes(@Context HttpServletRequest request, @QueryParam("codEmisor") String codEmisor) {
		LOG.info("-> cmbctasctes :" + codEmisor);
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			List<ComboDTO> comboBox = combosBusiness.cmbCtasCtes(codEmisor);
			
			if ( comboBox == null || comboBox.size() == 0 ) {
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(comboBox).build();
			}
		} catch (Exception e) {
			LOG.error("Metodo cmbctasctes - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/consulta")
	public Response consulta(@Context HttpServletRequest request, DepositoDTO depo) {
		LOG.info("-> Consulta Deposito.. " + depo.getCodigoCC() + " " + depo.getFechaDesde() + " " + depo.getFechaHasta());
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			List<DepositoDTO> response = depositosBusiness.consultaDepositos(depo.getCodigoCC(), depo.getFechaDesde(), depo.getFechaHasta());
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Metodo consulta - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/guarda")
	public Response guarda(@Context HttpServletRequest request, DepositoDTO depo) {
		LOG.info("-> Guarda Deposito.. " + depo.getFecDepositoAux());
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ResponseDTO response = depositosBusiness.guardaDeposito(depo);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Metodo guarda - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/cerrar")
	public Response cerrar(@Context HttpServletRequest request, BigDecimal corrCtaId) {
		LOG.info("-> Cerrar Deposito.. " + corrCtaId);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ResponseDTO response = depositosBusiness.cerrarDeposito(corrCtaId);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Metodo cerrar - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/pdf")
	public Response pdf(@Context HttpServletRequest request, PDFDepositosDTO dto) {
		LOG.info("-> Rest Depositos PDF.." + dto.toString());
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		GeneradorPDF generador = new GeneradorPDF();
		Map<String, Object> map = obtenerMapDepositos(dto);
		
		try {
			byte[] archivo = generador.create("Depositos", dto.getDepositos(), map);
			
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);
			pdf.setIdControl(1);
			pdf.setMsgControl("OK");
			
			return Response.ok(pdf).build();
			
		} catch (Exception e) {
			LOG.error("Depositos - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	private Map<String, Object> obtenerMapDepositos(PDFDepositosDTO dto) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("FECHA_DESDE", dto.getFechaDesde());
		params.put("FECHA_HASTA", dto.getFechaHasta());
		params.put("FECHA", utils.formatearFecha(new Date()));
		params.put("USUARIO", dto.getUsuario());
		params.put("C_COSTO", dto.getCentroCosto());
		params.put("TOT_DEPOS", dto.getTotalDepos());
		params.put("TOT_MONTOS", dto.calTotalMontos());
		
		return params;
	}

}
