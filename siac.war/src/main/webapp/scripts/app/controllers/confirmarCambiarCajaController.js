angular.module('ccs').controller('ConfirmarCambiarCajaController', function($scope, CajaResource, $rootScope, $location) {

    $scope.seleccionado = "listado";

    $scope.aceptarCambiarCaja = function(){
        var successCallback = function(data){
            $rootScope.usuario.caja = data.caja;
            angular.element('#confirmarcambiarcaja').modal("hide");
            angular.element('#modalaperturacaja').modal();
            //$location.path("/inicio");
        };
        CajaResource.cerrarcajatemporal($rootScope.usuario.caja,  successCallback, $scope.errorCallback);
    }
});
