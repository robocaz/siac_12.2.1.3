package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_FrecuenciaEnvio database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FrecuenciaEnvio.findAll", query="SELECT t FROM Tbl_FrecuenciaEnvio t")
public class Tbl_FrecuenciaEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodFrecuenciaEnvio")
	private short fld_CodFrecuenciaEnvio;

	@Column(name="Fld_GlosaFrecuenciaEnvio")
	private String fld_GlosaFrecuenciaEnvio;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_FrecuenciaEnvio() {
	}

	public short getFld_CodFrecuenciaEnvio() {
		return this.fld_CodFrecuenciaEnvio;
	}

	public void setFld_CodFrecuenciaEnvio(short fld_CodFrecuenciaEnvio) {
		this.fld_CodFrecuenciaEnvio = fld_CodFrecuenciaEnvio;
	}

	public String getFld_GlosaFrecuenciaEnvio() {
		return this.fld_GlosaFrecuenciaEnvio;
	}

	public void setFld_GlosaFrecuenciaEnvio(String fld_GlosaFrecuenciaEnvio) {
		this.fld_GlosaFrecuenciaEnvio = fld_GlosaFrecuenciaEnvio;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}