package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Libradores database table.
 * 
 */
@Entity
@Table(name="Tbl_Libradores")
@NamedQuery(name="Tbl_Libradore.findAll", query="SELECT t FROM Tbl_Libradore t")
public class Tbl_Libradore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_LIBRADORES_FLD_CORRLIB_ID_GENERATOR", sequenceName="FLD_CORRLIB_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_LIBRADORES_FLD_CORRLIB_ID_GENERATOR")
	@Column(name="Fld_CorrLib_Id")
	private long fld_CorrLib_Id;

	@Column(name="Fld_CargoRepresentante")
	private String fld_CargoRepresentante;

	@Column(name="Fld_CodEstadoLibrador")
	private short fld_CodEstadoLibrador;

	@Column(name="Fld_CodEstadoRegimen")
	private short fld_CodEstadoRegimen;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_Comuna")
	private String fld_Comuna;

	@Column(name="Fld_DireccionLibrador")
	private String fld_DireccionLibrador;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FechaActualizacion")
	private Timestamp fld_FechaActualizacion;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_Representante")
	private String fld_Representante;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	public Tbl_Libradore() {
	}

	public long getFld_CorrLib_Id() {
		return this.fld_CorrLib_Id;
	}

	public void setFld_CorrLib_Id(long fld_CorrLib_Id) {
		this.fld_CorrLib_Id = fld_CorrLib_Id;
	}

	public String getFld_CargoRepresentante() {
		return this.fld_CargoRepresentante;
	}

	public void setFld_CargoRepresentante(String fld_CargoRepresentante) {
		this.fld_CargoRepresentante = fld_CargoRepresentante;
	}

	public short getFld_CodEstadoLibrador() {
		return this.fld_CodEstadoLibrador;
	}

	public void setFld_CodEstadoLibrador(short fld_CodEstadoLibrador) {
		this.fld_CodEstadoLibrador = fld_CodEstadoLibrador;
	}

	public short getFld_CodEstadoRegimen() {
		return this.fld_CodEstadoRegimen;
	}

	public void setFld_CodEstadoRegimen(short fld_CodEstadoRegimen) {
		this.fld_CodEstadoRegimen = fld_CodEstadoRegimen;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_Comuna() {
		return this.fld_Comuna;
	}

	public void setFld_Comuna(String fld_Comuna) {
		this.fld_Comuna = fld_Comuna;
	}

	public String getFld_DireccionLibrador() {
		return this.fld_DireccionLibrador;
	}

	public void setFld_DireccionLibrador(String fld_DireccionLibrador) {
		this.fld_DireccionLibrador = fld_DireccionLibrador;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FechaActualizacion() {
		return this.fld_FechaActualizacion;
	}

	public void setFld_FechaActualizacion(Timestamp fld_FechaActualizacion) {
		this.fld_FechaActualizacion = fld_FechaActualizacion;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public String getFld_Representante() {
		return this.fld_Representante;
	}

	public void setFld_Representante(String fld_Representante) {
		this.fld_Representante = fld_Representante;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

}