package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_VIGENCIAEMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_VIGENCIAEMISOR")
@NamedQuery(name="TblVigenciaemisor.findAll", query="SELECT t FROM TblVigenciaemisor t")
public class TblVigenciaemisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODVIGENCIA")
	private BigDecimal fldCodvigencia;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSAVIGENCIA")
	private String fldGlosavigencia;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblVigenciaemisor() {
	}

	public BigDecimal getFldCodvigencia() {
		return this.fldCodvigencia;
	}

	public void setFldCodvigencia(BigDecimal fldCodvigencia) {
		this.fldCodvigencia = fldCodvigencia;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosavigencia() {
		return this.fldGlosavigencia;
	}

	public void setFldGlosavigencia(String fldGlosavigencia) {
		this.fldGlosavigencia = fldGlosavigencia;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}