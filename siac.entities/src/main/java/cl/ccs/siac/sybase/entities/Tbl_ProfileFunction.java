package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ProfileFunction database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ProfileFunction.findAll", query="SELECT t FROM Tbl_ProfileFunction t")
public class Tbl_ProfileFunction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FunctionName")
	private String fld_FunctionName;

	@Column(name="Fld_ProfileName")
	private String fld_ProfileName;

	public Tbl_ProfileFunction() {
	}

	public String getFld_FunctionName() {
		return this.fld_FunctionName;
	}

	public void setFld_FunctionName(String fld_FunctionName) {
		this.fld_FunctionName = fld_FunctionName;
	}

	public String getFld_ProfileName() {
		return this.fld_ProfileName;
	}

	public void setFld_ProfileName(String fld_ProfileName) {
		this.fld_ProfileName = fld_ProfileName;
	}

}