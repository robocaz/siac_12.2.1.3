package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_MORHISTO database table.
 * 
 */
@Entity
@Table(name="TBL_MORHISTO")
@NamedQuery(name="TblMorhisto.findAll", query="SELECT t FROM TblMorhisto t")
public class TblMorhisto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_MORHISTO_FLDCORRMOROSIDADID_GENERATOR", sequenceName="FLD_CORRMOROSIDAD_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_MORHISTO_FLDCORRMOROSIDADID_GENERATOR")
	@Column(name="FLD_CORRMOROSIDAD_ID")
	private long fldCorrmorosidadId;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_CORRENVIO")
	private BigDecimal fldCorrenvio;

	@Column(name="FLD_CORRENVIOBAJA")
	private BigDecimal fldCorrenviobaja;

	@Column(name="FLD_CORRENVIOCARGA")
	private BigDecimal fldCorrenviocarga;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FECHACREACION")
	private Timestamp fldFechacreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOCREACION")
	private String fldUsuariocreacion;

	//bi-directional many-to-one association to TblAclhisto
	@OneToMany(mappedBy="tblMorhisto")
	private List<TblAclhisto> tblAclhistos;

	//bi-directional many-to-one association to TblSucursal
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FLD_CODEMISOR", referencedColumnName="FLD_CODEMISOR"),
		@JoinColumn(name="FLD_CODSUCURSAL", referencedColumnName="FLD_CODSUCURSAL"),
		@JoinColumn(name="FLD_TIPOEMISOR", referencedColumnName="FLD_TIPOEMISOR")
		})
	private TblSucursal tblSucursal;

	//bi-directional many-to-one association to TblRutmorhisto
	@OneToMany(mappedBy="tblMorhisto")
	private List<TblRutmorhisto> tblRutmorhistos;

	public TblMorhisto() {
	}

	public long getFldCorrmorosidadId() {
		return this.fldCorrmorosidadId;
	}

	public void setFldCorrmorosidadId(long fldCorrmorosidadId) {
		this.fldCorrmorosidadId = fldCorrmorosidadId;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public BigDecimal getFldCorrenvio() {
		return this.fldCorrenvio;
	}

	public void setFldCorrenvio(BigDecimal fldCorrenvio) {
		this.fldCorrenvio = fldCorrenvio;
	}

	public BigDecimal getFldCorrenviobaja() {
		return this.fldCorrenviobaja;
	}

	public void setFldCorrenviobaja(BigDecimal fldCorrenviobaja) {
		this.fldCorrenviobaja = fldCorrenviobaja;
	}

	public BigDecimal getFldCorrenviocarga() {
		return this.fldCorrenviocarga;
	}

	public void setFldCorrenviocarga(BigDecimal fldCorrenviocarga) {
		this.fldCorrenviocarga = fldCorrenviocarga;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Timestamp getFldFechacreacion() {
		return this.fldFechacreacion;
	}

	public void setFldFechacreacion(Timestamp fldFechacreacion) {
		this.fldFechacreacion = fldFechacreacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuariocreacion() {
		return this.fldUsuariocreacion;
	}

	public void setFldUsuariocreacion(String fldUsuariocreacion) {
		this.fldUsuariocreacion = fldUsuariocreacion;
	}

	public List<TblAclhisto> getTblAclhistos() {
		return this.tblAclhistos;
	}

	public void setTblAclhistos(List<TblAclhisto> tblAclhistos) {
		this.tblAclhistos = tblAclhistos;
	}

	public TblAclhisto addTblAclhisto(TblAclhisto tblAclhisto) {
		getTblAclhistos().add(tblAclhisto);
		tblAclhisto.setTblMorhisto(this);

		return tblAclhisto;
	}

	public TblAclhisto removeTblAclhisto(TblAclhisto tblAclhisto) {
		getTblAclhistos().remove(tblAclhisto);
		tblAclhisto.setTblMorhisto(null);

		return tblAclhisto;
	}

	public TblSucursal getTblSucursal() {
		return this.tblSucursal;
	}

	public void setTblSucursal(TblSucursal tblSucursal) {
		this.tblSucursal = tblSucursal;
	}

	public List<TblRutmorhisto> getTblRutmorhistos() {
		return this.tblRutmorhistos;
	}

	public void setTblRutmorhistos(List<TblRutmorhisto> tblRutmorhistos) {
		this.tblRutmorhistos = tblRutmorhistos;
	}

	public TblRutmorhisto addTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().add(tblRutmorhisto);
		tblRutmorhisto.setTblMorhisto(this);

		return tblRutmorhisto;
	}

	public TblRutmorhisto removeTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().remove(tblRutmorhisto);
		tblRutmorhisto.setTblMorhisto(null);

		return tblRutmorhisto;
	}

}