package cl.ccs.siac.finiquito;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.FiniquitoDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.FiniquitoDTO;

@Named("finiquitoBusiness")
public class FiniquitoBusinessImpl implements FiniquitoBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private FiniquitoDao finiquitoDao;
	
	@Override
	public FiniquitoDTO consultaFiniquito(String rut) {
		String rutFormateado = utils.formatZeroRut(rut);
		FiniquitoDTO finq = finiquitoDao.consultaFiniquito(rutFormateado);
		
		finq.setFecIniContrato(utils.checkTimestampToStrFecha(finq.getFecIniContrato()));
		finq.setFecFinContrato(utils.checkTimestampToStrFecha(finq.getFecFinContrato()));
		finq.setFecFiniquito(utils.checkTimestampToStrFecha(finq.getFecFiniquito()));
		finq.setFecDecJurada(utils.checkTimestampToStrFecha(finq.getFecDecJurada()));
		
		return finq;
	}
	
	@Override
	public FiniquitoDTO guardaFiniquito(FiniquitoDTO finq) {
		
		System.out.println("1->" + finq.toString());
		
		finq.setRutTramitante(utils.formatZeroRut(finq.getRutTramitante()));
		finq.setRutAfectado(utils.formatZeroRut(finq.getRutAfectado()));
		finq.setRutEmpresa(utils.formatZeroRut(finq.getRutEmpresa()));
		finq.setNombreApMatEmp((finq.getNombreApMatEmp() != null) ? finq.getNombreApMatEmp() : "");
		finq.setNombreNombresEmp((finq.getNombreNombresEmp() != null) ? finq.getNombreNombresEmp() : "");
		finq.setFecIniContrato(checkFechaToStrTimestamp(finq.getFecIniContrato()));
		finq.setFecFinContrato(checkFechaToStrTimestamp(finq.getFecFinContrato()));
		finq.setFecFiniquito(checkFechaToStrTimestamp(finq.getFecFiniquito()));
		
		if ( finq.getMarcaProrroga() ) {
			finq.setFecDecJurada(checkFechaToStrTimestamp(finq.getFecDecJurada()));
		} else {
			finq.setFecDecJurada(utils.getDefaultTimestampStr());
		}
		
		finq.setCodAfp(0);
		finq.setFecUltCotizacion(utils.getDefaultTimestampStr());
		finq.setFecCertCotizacion(utils.getDefaultTimestampStr());
		
		return finiquitoDao.guardaFiniquito(finq);
	}
	
	/*
	 * Revisa la fecha ingresada para retornar un String con formato Timestamp
	 * Si no aplica, retorna un String Timestamp con la fecha por defecto de la BD
	 * "yyyy-MM-dd 00:00:00" || "1900-01-01 00:00:00"
	 */
	public String checkFechaToStrTimestamp(String strFecha) {
		if ( strFecha != null && !strFecha.isEmpty() ) {
			return utils.addTimeStrFormat(strFecha);
		} else {
			return utils.getDefaultTimestampStr();
		}
	}
	
}
