package cl.exe.ccs.dto;

import java.io.File;
import java.io.Serializable;

public class PDFResponseDTO extends ResponseDTO implements Serializable{

	
	private byte[] reporte;
	private File archivo;
	

	public byte[] getReporte() {
		return reporte;
	}

	public void setReporte(byte[] reporte) {
		this.reporte = reporte;
	}

	public File getArchivo() {
		return archivo;
	}

	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	
	
	
}
