angular.module('ccs').factory('DetectorFraudeResource', function ($resource) {
    var resource = $resource('rest/detectorfraude/',  { nocache : new Date().getTime()},
    {
    	'detallesFraudePorRut' :  { method : 'POST' , url : 'rest/detectorfraude/detallesFraudePorRut', isArray : false},
    	'validaAutorizacion': {method : 'POST', url : 'rest/detectorfraude/validaAutorizacion', isArray : false}
    });
    return resource;
});
