package cl.ccs.siac.printer;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dto.BoletaTransaccionDTO;
import cl.ccs.siac.dto.TransaccionServicioDTO;
import cl.ccs.siac.pos.printer.Impresora;
import cl.exe.ccs.dto.InicioConfigDTO;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

@Named("utilImpresion")
@Stateless
public class ImpresionImpl implements Impresion {

	@Inject
	private Impresora impresora;
	@Inject
	private CargaConfig cargaConfigImpl;

	private static final Logger LOG = LogManager.getLogger(ImpresionImpl.class);

	public String imprimeBoleta(BoletaTransaccionDTO boletaTransaccionDTO) {

		String resultado = "";
		try {

			// Cargo puerto Impresora

			InicioConfigDTO configuracion = new InicioConfigDTO();

			configuracion = cargaConfigImpl.cargaConfiguracion();
			if (configuracion.getIdControl() == 0) {
				impresora.setDispositivo(configuracion.getPuertoImpresora());
			}

			String numeroBoleta = boletaTransaccionDTO.getNumeroBoleta().toString();
			String corrCaja = String.format("%09d", boletaTransaccionDTO.getIdCaja());
			String centroCosto = String.format("%04d", boletaTransaccionDTO.getIdCentroCosto());
			String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
			StringBuffer stb = new StringBuffer();

			stb.append("\n\n\n\n");
			stb.append("                                                      #" + numeroBoleta + "\n");
			stb.append("\n\n");
			stb.append("  " + corrCaja + "  " + centroCosto + "   " + date);

			// impresora.setDispositivo(configuracion.getPuertoImpresora());
			impresora.escribir((char) 27 + "@");
			impresora.setTipoCaracterLatino();
			impresora.setNegro();
			impresora.setFormato(10);
			impresora.escribir(stb.toString());
			impresora.escribir("\n");
			// iteracion
			Long total = new Long(0);
			int maxReg = 9;
			int inicial = 1;

			String codServicio = "";

			List<TransaccionServicioDTO> allServices = boletaTransaccionDTO.getTransacciones(); // your
																								// list
																								// of
																								// all
																								// people
			Map<String, List<TransaccionServicioDTO>> map = new HashMap<String, List<TransaccionServicioDTO>>();
			for (TransaccionServicioDTO servicios : allServices) {
				String key = servicios.getCodServicio();
				if (map.get(key) == null) {
					map.put(key, new ArrayList<TransaccionServicioDTO>());
				}
				map.get(key).add(servicios);
			}

			for (String key : map.keySet()) {
				int cantidad = map.get(key).size();
				int posicion = ((TransaccionServicioDTO) map.get(key).get(0)).getCostoServicio().indexOf(".");
				Long costo = Long.parseLong(((TransaccionServicioDTO) map.get(key).get(0)).getCostoServicio().substring(0, posicion)) * cantidad;
				String nombreServicio = ((TransaccionServicioDTO) map.get(key).get(0)).getTipoServicio();
				System.out.println(key + map.get(key).size() + " " + ((TransaccionServicioDTO) map.get(key).get(0)).getTipoServicio() + " " + costo);
				impresora.escribir("  " + cantidad + "    " + nombreServicio.trim() + "    " + costo);
				total = total + costo;
				inicial = inicial + 1;
			}

			impresora.correr(maxReg - inicial);
			impresora.correr(1);
			impresora.escribir("                 " + "Total $" + total);

			impresora.correr(11);
			impresora.cortar();
			impresora.cerrarDispositivo();
			resultado = "0";

		} catch (Exception e) {
			resultado = "1";
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

		return resultado;

	}

	public String imprimeBoletaPortCom(BoletaTransaccionDTO boletaTransaccionDTO) {

		String resultado = "";
		String saltos = "";
		InicioConfigDTO configuracion = new InicioConfigDTO();
		SerialPort mySP;
		CommPort puerto = null;
		PrintStream myPS = null;
		configuracion = cargaConfigImpl.cargaConfiguracion();

		try {

			Enumeration comport = CommPortIdentifier.getPortIdentifiers();
			CommPortIdentifier myCPI = null;

			while (comport.hasMoreElements()) {
				myCPI = (CommPortIdentifier) comport.nextElement();
				System.out.println("En uso ? " + myCPI.isCurrentlyOwned());
				if (myCPI.getName().equals(configuracion.getPuertoImpresora()))
					break;
			}

			puerto = myCPI.open(configuracion.getPuertoImpresora(), 2000);
			mySP = (SerialPort) puerto;

			mySP.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			myPS = new PrintStream(mySP.getOutputStream());

			String numeroBoleta = boletaTransaccionDTO.getNumeroBoleta().toString();
			String corrCaja = String.format("%09d", boletaTransaccionDTO.getIdCaja());
			String centroCosto = String.format("%04d", boletaTransaccionDTO.getIdCentroCosto());
			String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
			StringBuffer stb = new StringBuffer();
			stb.append("\n\n\n\n\n\n\n");
			stb.append("                                                                      #" + numeroBoleta
					+ "\n");
			stb.append("\n");
			stb.append("   " + corrCaja + "   " + centroCosto + "     " + date);
			myPS.println(stb.toString());
			myPS.println("\n");

			// iteracion
			Long total = new Long(0);
			int maxReg = 9;
			int inicial = 1;

			String codServicio = "";

			List<TransaccionServicioDTO> allServices = boletaTransaccionDTO.getTransacciones(); // your
																								// list
																								// of
																								// all
																								// people
			Map<String, List<TransaccionServicioDTO>> map = new HashMap<String, List<TransaccionServicioDTO>>();
			for (TransaccionServicioDTO servicios : allServices) {
				String key = servicios.getCodServicio();
				if (map.get(key) == null) {
					map.put(key, new ArrayList<TransaccionServicioDTO>());
				}
				map.get(key).add(servicios);
			}

			for (String key : map.keySet()) {
				int cantidad = map.get(key).size();
				int posicion = ((TransaccionServicioDTO) map.get(key).get(0)).getCostoServicio().indexOf(".");
				Long costo = Long.parseLong(((TransaccionServicioDTO) map.get(key).get(0)).getCostoServicio().substring(0, posicion)) * cantidad;
				String nombreServicio = ((TransaccionServicioDTO) map.get(key).get(0)).getTipoServicio();
				System.out.println(key + map.get(key).size() + " " + ((TransaccionServicioDTO) map.get(key).get(0)).getTipoServicio() + " " + costo);
				myPS.println("   " + cantidad + "     " + nombreServicio.trim() + "       " + costo);
				total = total + costo;
				inicial = inicial + 1;
			}

			saltos = this.correr(maxReg - inicial);
			myPS.println(saltos);
			saltos = this.correr(1);

			myPS.println(saltos);
			myPS.println("                    " + "Total $" + total);

			saltos = "";
			saltos = this.correr(7);
			myPS.println(saltos);

			myPS.close();
			myPS.flush();
			puerto.close();
			resultado = "0";

		} catch (Exception e) {
			resultado = "1";
			LOG.error(e.getMessage());
			e.printStackTrace();
			myPS.close();
			myPS.flush();
			puerto.close();

		}

		return resultado;

	}

	
	public String imprimeBoletaPrueba() {

		String resultado = "";
		String saltos = "";
		InicioConfigDTO configuracion = new InicioConfigDTO();
		SerialPort mySP;
		CommPort puerto = null;
		PrintStream myPS = null;
		configuracion = cargaConfigImpl.cargaConfiguracion();

		try {

			Enumeration comport = CommPortIdentifier.getPortIdentifiers();
			CommPortIdentifier myCPI = null;

			while (comport.hasMoreElements()) {
				myCPI = (CommPortIdentifier) comport.nextElement();
				System.out.println("En uso ? " + myCPI.isCurrentlyOwned());
				if (myCPI.getName().equals(configuracion.getPuertoImpresora()))
					break;
			}

			puerto = myCPI.open(configuracion.getPuertoImpresora(), 2000);
			mySP = (SerialPort) puerto;

			mySP.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			myPS = new PrintStream(mySP.getOutputStream());

			StringBuffer stb = new StringBuffer();
			stb.append("PRUEBA IMPRESION BOLETA " + new Date());
			saltos = this.correr(1);

			myPS.println(saltos);
			myPS.println(saltos);

			myPS.close();
			myPS.flush();
			puerto.close();
			resultado = "0";

		} catch (Exception e) {
			resultado = "1";
			LOG.error(e.getMessage());
			e.printStackTrace();
			myPS.close();
			myPS.flush();
			puerto.close();

		}

		return resultado;

	}	
	
	public String correr(int fin) {
		String salto = "";
		try {
			int i = 0;
			for (i = 1; i <= fin; i++) {
				salto = salto + "\n";
			}

		} catch (Exception e) {
			System.out.print(e);
		}
		return salto;
	}
}
