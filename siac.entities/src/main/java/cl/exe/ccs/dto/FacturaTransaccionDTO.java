package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FacturaTransaccionDTO implements Serializable{
	
	private Integer nroLinea;
	private Integer cantidadServicio;
	private String codServicio;
	private String codExterno;
	private String tipoServicio;
	private BigDecimal costoServicio;
	private Integer correlativoTransaccion;
	
	public Integer getNroLinea() {
		return nroLinea;
	}
	public void setNroLinea(Integer nroLinea) {
		this.nroLinea = nroLinea;
	}
	public Integer getCantidadServicio() {
		return cantidadServicio;
	}
	public void setCantidadServicio(Integer cantidadServicio) {
		this.cantidadServicio = cantidadServicio;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getCodExterno() {
		return codExterno;
	}
	public void setCodExterno(String codExterno) {
		this.codExterno = codExterno;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public BigDecimal getCostoServicio() {
		return costoServicio;
	}
	public void setCostoServicio(BigDecimal costoServicio) {
		this.costoServicio = costoServicio;
	}
	public Integer getCorrelativoTransaccion() {
		return correlativoTransaccion;
	}
	public void setCorrelativoTransaccion(Integer correlativoTransaccion) {
		this.correlativoTransaccion = correlativoTransaccion;
	}
	
}
