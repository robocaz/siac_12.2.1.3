package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CampaniaVtaInternet database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CampaniaVtaInternet.findAll", query="SELECT t FROM Tbl_CampaniaVtaInternet t")
public class Tbl_CampaniaVtaInternet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmpresaPago")
	private String fld_CodEmpresaPago;

	@Column(name="Fld_CorrUnico_id")
	private BigDecimal fld_CorrUnico_id;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	public Tbl_CampaniaVtaInternet() {
	}

	public String getFld_CodEmpresaPago() {
		return this.fld_CodEmpresaPago;
	}

	public void setFld_CodEmpresaPago(String fld_CodEmpresaPago) {
		this.fld_CodEmpresaPago = fld_CodEmpresaPago;
	}

	public BigDecimal getFld_CorrUnico_id() {
		return this.fld_CorrUnico_id;
	}

	public void setFld_CorrUnico_id(BigDecimal fld_CorrUnico_id) {
		this.fld_CorrUnico_id = fld_CorrUnico_id;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

}