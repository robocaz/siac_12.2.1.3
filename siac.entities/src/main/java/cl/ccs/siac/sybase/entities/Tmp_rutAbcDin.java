package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutAbcDin database table.
 * 
 */
@Entity
@Table(name="tmp_rutAbcDin")
@NamedQuery(name="Tmp_rutAbcDin.findAll", query="SELECT t FROM Tmp_rutAbcDin t")
public class Tmp_rutAbcDin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tmp_rutAbcDin() {
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}