package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_AvancePareo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AvancePareo.findAll", query="SELECT t FROM Tbl_AvancePareo t")
public class Tbl_AvancePareo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrMov")
	private BigDecimal fld_CorrMov;

	@Column(name="Fld_FlagIntentos")
	private short fld_FlagIntentos;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_AvancePareo() {
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrMov() {
		return this.fld_CorrMov;
	}

	public void setFld_CorrMov(BigDecimal fld_CorrMov) {
		this.fld_CorrMov = fld_CorrMov;
	}

	public short getFld_FlagIntentos() {
		return this.fld_FlagIntentos;
	}

	public void setFld_FlagIntentos(short fld_FlagIntentos) {
		this.fld_FlagIntentos = fld_FlagIntentos;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}