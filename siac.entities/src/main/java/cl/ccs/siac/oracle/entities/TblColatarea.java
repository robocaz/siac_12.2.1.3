package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_COLATAREA database table.
 * 
 */
@Entity
@Table(name="TBL_COLATAREA")
@NamedQuery(name="TblColatarea.findAll", query="SELECT t FROM TblColatarea t")
public class TblColatarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODPROCESO")
	private BigDecimal fldCodproceso;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_CORR")
	private BigDecimal fldCorr;

	@Column(name="FLD_CORRENVIO")
	private BigDecimal fldCorrenvio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECINICIO")
	private Date fldFecinicio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECTAREA")
	private Date fldFectarea;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECTERMINO")
	private Date fldFectermino;

	@Column(name="FLD_FLAGPROCESO")
	private BigDecimal fldFlagproceso;

	@Column(name="FLD_NOMBREPROCESO")
	private String fldNombreproceso;

	@Column(name="FLD_SERVIDOR")
	private BigDecimal fldServidor;

	@Column(name="FLD_TAREA")
	private String fldTarea;

	public TblColatarea() {
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodproceso() {
		return this.fldCodproceso;
	}

	public void setFldCodproceso(BigDecimal fldCodproceso) {
		this.fldCodproceso = fldCodproceso;
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public BigDecimal getFldCorr() {
		return this.fldCorr;
	}

	public void setFldCorr(BigDecimal fldCorr) {
		this.fldCorr = fldCorr;
	}

	public BigDecimal getFldCorrenvio() {
		return this.fldCorrenvio;
	}

	public void setFldCorrenvio(BigDecimal fldCorrenvio) {
		this.fldCorrenvio = fldCorrenvio;
	}

	public Date getFldFecinicio() {
		return this.fldFecinicio;
	}

	public void setFldFecinicio(Date fldFecinicio) {
		this.fldFecinicio = fldFecinicio;
	}

	public Date getFldFectarea() {
		return this.fldFectarea;
	}

	public void setFldFectarea(Date fldFectarea) {
		this.fldFectarea = fldFectarea;
	}

	public Date getFldFectermino() {
		return this.fldFectermino;
	}

	public void setFldFectermino(Date fldFectermino) {
		this.fldFectermino = fldFectermino;
	}

	public BigDecimal getFldFlagproceso() {
		return this.fldFlagproceso;
	}

	public void setFldFlagproceso(BigDecimal fldFlagproceso) {
		this.fldFlagproceso = fldFlagproceso;
	}

	public String getFldNombreproceso() {
		return this.fldNombreproceso;
	}

	public void setFldNombreproceso(String fldNombreproceso) {
		this.fldNombreproceso = fldNombreproceso;
	}

	public BigDecimal getFldServidor() {
		return this.fldServidor;
	}

	public void setFldServidor(BigDecimal fldServidor) {
		this.fldServidor = fldServidor;
	}

	public String getFldTarea() {
		return this.fldTarea;
	}

	public void setFldTarea(String fldTarea) {
		this.fldTarea = fldTarea;
	}

}