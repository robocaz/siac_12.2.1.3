package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ValorMonedaLP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ValorMonedaLP.findAll", query="SELECT t FROM Tbl_ValorMonedaLP t")
public class Tbl_ValorMonedaLP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_GlosaMoneda")
	private String fld_GlosaMoneda;

	@Column(name="Fld_ValorMoneda")
	private BigDecimal fld_ValorMoneda;

	public Tbl_ValorMonedaLP() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public String getFld_GlosaMoneda() {
		return this.fld_GlosaMoneda;
	}

	public void setFld_GlosaMoneda(String fld_GlosaMoneda) {
		this.fld_GlosaMoneda = fld_GlosaMoneda;
	}

	public BigDecimal getFld_ValorMoneda() {
		return this.fld_ValorMoneda;
	}

	public void setFld_ValorMoneda(BigDecimal fld_ValorMoneda) {
		this.fld_ValorMoneda = fld_ValorMoneda;
	}

}