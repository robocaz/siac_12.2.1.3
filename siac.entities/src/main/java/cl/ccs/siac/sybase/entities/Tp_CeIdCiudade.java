package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdCiudades database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdCiudades")
@NamedQuery(name="Tp_CeIdCiudade.findAll", query="SELECT t FROM Tp_CeIdCiudade t")
public class Tp_CeIdCiudade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCiudad")
	private short fld_CodCiudad;

	@Column(name="Fld_GlosaCiudad")
	private String fld_GlosaCiudad;

	public Tp_CeIdCiudade() {
	}

	public short getFld_CodCiudad() {
		return this.fld_CodCiudad;
	}

	public void setFld_CodCiudad(short fld_CodCiudad) {
		this.fld_CodCiudad = fld_CodCiudad;
	}

	public String getFld_GlosaCiudad() {
		return this.fld_GlosaCiudad;
	}

	public void setFld_GlosaCiudad(String fld_GlosaCiudad) {
		this.fld_GlosaCiudad = fld_GlosaCiudad;
	}

}