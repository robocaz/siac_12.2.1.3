package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CertifConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CertifConsLey20575.findAll", query="SELECT t FROM Tbl_CertifConsLey20575 t")
public class Tbl_CertifConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrConsulta_Id")
	private BigDecimal fld_CorrConsulta_Id;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	public Tbl_CertifConsLey20575() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrConsulta_Id() {
		return this.fld_CorrConsulta_Id;
	}

	public void setFld_CorrConsulta_Id(BigDecimal fld_CorrConsulta_Id) {
		this.fld_CorrConsulta_Id = fld_CorrConsulta_Id;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

}