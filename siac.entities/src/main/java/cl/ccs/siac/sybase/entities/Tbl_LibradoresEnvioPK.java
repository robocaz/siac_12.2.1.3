package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_LibradoresEnvios database table.
 * 
 */
@Embeddable
public class Tbl_LibradoresEnvioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrLib")
	private long fld_CorrLib;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	public Tbl_LibradoresEnvioPK() {
	}
	public long getFld_CorrLib() {
		return this.fld_CorrLib;
	}
	public void setFld_CorrLib(long fld_CorrLib) {
		this.fld_CorrLib = fld_CorrLib;
	}
	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}
	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_LibradoresEnvioPK)) {
			return false;
		}
		Tbl_LibradoresEnvioPK castOther = (Tbl_LibradoresEnvioPK)other;
		return 
			(this.fld_CorrLib == castOther.fld_CorrLib)
			&& (this.fld_NroBoletin == castOther.fld_NroBoletin);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_CorrLib ^ (this.fld_CorrLib >>> 32)));
		hash = hash * prime + ((int) this.fld_NroBoletin);
		
		return hash;
	}
}