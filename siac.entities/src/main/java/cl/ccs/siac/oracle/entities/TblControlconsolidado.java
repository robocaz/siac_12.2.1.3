package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONTROLCONSOLIDADO database table.
 * 
 */
@Entity
@Table(name="TBL_CONTROLCONSOLIDADO")
@NamedQuery(name="TblControlconsolidado.findAll", query="SELECT t FROM TblControlconsolidado t")
public class TblControlconsolidado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ARCHIVO")
	private String fldArchivo;

	@Column(name="FLD_CORRCONTROLCONS_ID")
	private BigDecimal fldCorrcontrolconsId;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA")
	private Date fldFecha;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAACTUALIZACION")
	private Date fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIO")
	private Date fldFechainicio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATERMINO")
	private Date fldFechatermino;

	@Column(name="FLD_FLAGPROCESO")
	private BigDecimal fldFlagproceso;

	@Column(name="FLD_REGISTROSDETALLE")
	private BigDecimal fldRegistrosdetalle;

	@Column(name="FLD_REGISTROSMAESTRO")
	private BigDecimal fldRegistrosmaestro;

	public TblControlconsolidado() {
	}

	public String getFldArchivo() {
		return this.fldArchivo;
	}

	public void setFldArchivo(String fldArchivo) {
		this.fldArchivo = fldArchivo;
	}

	public BigDecimal getFldCorrcontrolconsId() {
		return this.fldCorrcontrolconsId;
	}

	public void setFldCorrcontrolconsId(BigDecimal fldCorrcontrolconsId) {
		this.fldCorrcontrolconsId = fldCorrcontrolconsId;
	}

	public Date getFldFecha() {
		return this.fldFecha;
	}

	public void setFldFecha(Date fldFecha) {
		this.fldFecha = fldFecha;
	}

	public Date getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Date fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechainicio() {
		return this.fldFechainicio;
	}

	public void setFldFechainicio(Date fldFechainicio) {
		this.fldFechainicio = fldFechainicio;
	}

	public Date getFldFechatermino() {
		return this.fldFechatermino;
	}

	public void setFldFechatermino(Date fldFechatermino) {
		this.fldFechatermino = fldFechatermino;
	}

	public BigDecimal getFldFlagproceso() {
		return this.fldFlagproceso;
	}

	public void setFldFlagproceso(BigDecimal fldFlagproceso) {
		this.fldFlagproceso = fldFlagproceso;
	}

	public BigDecimal getFldRegistrosdetalle() {
		return this.fldRegistrosdetalle;
	}

	public void setFldRegistrosdetalle(BigDecimal fldRegistrosdetalle) {
		this.fldRegistrosdetalle = fldRegistrosdetalle;
	}

	public BigDecimal getFldRegistrosmaestro() {
		return this.fldRegistrosmaestro;
	}

	public void setFldRegistrosmaestro(BigDecimal fldRegistrosmaestro) {
		this.fldRegistrosmaestro = fldRegistrosmaestro;
	}

}