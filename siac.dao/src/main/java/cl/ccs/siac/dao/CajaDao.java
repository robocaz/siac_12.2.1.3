package cl.ccs.siac.dao;

import cl.exe.ccs.dto.CajaDTO;
import cl.exe.ccs.dto.CajaPendienteDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface CajaDao {
	
	public CajaDTO cantidadAbiertas(String usuario, String codCentroCosto);
	
	public ResponseListDTO<CajaDTO> listarCajasAbiertas(String usuario, String codCentroCosto);
	
	public CajaDTO aperturaCaja(String usuario, String codCentroCosto);

	public String tomarCaja(String CorrCaja, byte[] timeStamp, String username);
	
	public Boolean cerrarCajaTemporal(String CorrCaja, String usuario);
	
	public Boolean cerrarCajaDefinitivo(String CorrCaja, String usuario);
	
	public ResponseListDTO<CajaDTO> listarCajasTodas(String usuario);

	ResponseListDTO<CajaDTO> listarCajasDelDiaPorPrivilegio(String codCentroCosto, String usuario, int tipoUsuario);
	
	public CajaPendienteDTO cajasAbiertaPendiente(String usuario);
	
	public CajaDTO numeroCajasAbiertas(String usuario, String codCentroCosto);
}
