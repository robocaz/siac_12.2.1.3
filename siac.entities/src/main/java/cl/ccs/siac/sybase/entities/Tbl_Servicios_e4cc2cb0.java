package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Servicios_e4cc2cb0 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Servicios_e4cc2cb0.findAll", query="SELECT t FROM Tbl_Servicios_e4cc2cb0 t")
public class Tbl_Servicios_e4cc2cb0 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodExterno")
	private int fld_CodExterno;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_GlosaServicio")
	private String fld_GlosaServicio;

	@Column(name="Fld_OrigenProd")
	private byte fld_OrigenProd;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_Tipo")
	private byte fld_Tipo;

	public Tbl_Servicios_e4cc2cb0() {
	}

	public int getFld_CodExterno() {
		return this.fld_CodExterno;
	}

	public void setFld_CodExterno(int fld_CodExterno) {
		this.fld_CodExterno = fld_CodExterno;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public String getFld_GlosaServicio() {
		return this.fld_GlosaServicio;
	}

	public void setFld_GlosaServicio(String fld_GlosaServicio) {
		this.fld_GlosaServicio = fld_GlosaServicio;
	}

	public byte getFld_OrigenProd() {
		return this.fld_OrigenProd;
	}

	public void setFld_OrigenProd(byte fld_OrigenProd) {
		this.fld_OrigenProd = fld_OrigenProd;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_Tipo() {
		return this.fld_Tipo;
	}

	public void setFld_Tipo(byte fld_Tipo) {
		this.fld_Tipo = fld_Tipo;
	}

}