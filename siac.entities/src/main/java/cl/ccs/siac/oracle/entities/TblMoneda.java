package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_MONEDA database table.
 * 
 */
@Entity
@Table(name="TBL_MONEDA")
@NamedQuery(name="TblMoneda.findAll", query="SELECT t FROM TblMoneda t")
public class TblMoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_MONEDA_FLDCODMONEDA_GENERATOR", sequenceName="FLD_CODMONEDA")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_MONEDA_FLDCODMONEDA_GENERATOR")
	@Column(name="FLD_CODMONEDA")
	private long fldCodmoneda;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FLAGVIGENCIA")
	private BigDecimal fldFlagvigencia;

	@Column(name="FLD_GLOSACORTA")
	private String fldGlosacorta;

	@Column(name="FLD_GLOSAMONEDA")
	private String fldGlosamoneda;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblValormoneda
	@OneToMany(mappedBy="tblMoneda")
	private List<TblValormoneda> tblValormonedas;

	public TblMoneda() {
	}

	public long getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(long fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public BigDecimal getFldFlagvigencia() {
		return this.fldFlagvigencia;
	}

	public void setFldFlagvigencia(BigDecimal fldFlagvigencia) {
		this.fldFlagvigencia = fldFlagvigencia;
	}

	public String getFldGlosacorta() {
		return this.fldGlosacorta;
	}

	public void setFldGlosacorta(String fldGlosacorta) {
		this.fldGlosacorta = fldGlosacorta;
	}

	public String getFldGlosamoneda() {
		return this.fldGlosamoneda;
	}

	public void setFldGlosamoneda(String fldGlosamoneda) {
		this.fldGlosamoneda = fldGlosamoneda;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblValormoneda> getTblValormonedas() {
		return this.tblValormonedas;
	}

	public void setTblValormonedas(List<TblValormoneda> tblValormonedas) {
		this.tblValormonedas = tblValormonedas;
	}

	public TblValormoneda addTblValormoneda(TblValormoneda tblValormoneda) {
		getTblValormonedas().add(tblValormoneda);
		tblValormoneda.setTblMoneda(this);

		return tblValormoneda;
	}

	public TblValormoneda removeTblValormoneda(TblValormoneda tblValormoneda) {
		getTblValormonedas().remove(tblValormoneda);
		tblValormoneda.setTblMoneda(null);

		return tblValormoneda;
	}

}