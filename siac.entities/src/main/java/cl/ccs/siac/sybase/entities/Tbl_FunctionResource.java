package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_FunctionResource database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FunctionResource.findAll", query="SELECT t FROM Tbl_FunctionResource t")
public class Tbl_FunctionResource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FunctionName")
	private String fld_FunctionName;

	@Column(name="Fld_ResourceName")
	private String fld_ResourceName;

	public Tbl_FunctionResource() {
	}

	public String getFld_FunctionName() {
		return this.fld_FunctionName;
	}

	public void setFld_FunctionName(String fld_FunctionName) {
		this.fld_FunctionName = fld_FunctionName;
	}

	public String getFld_ResourceName() {
		return this.fld_ResourceName;
	}

	public void setFld_ResourceName(String fld_ResourceName) {
		this.fld_ResourceName = fld_ResourceName;
	}

}