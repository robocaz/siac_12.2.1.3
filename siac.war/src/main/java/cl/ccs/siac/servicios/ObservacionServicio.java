package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.observacion.ObservacionBusiness;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ObservacionDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.TramitanteDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/observacion")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class ObservacionServicio {

	private static final Logger log = LogManager.getLogger(ObservacionServicio.class);

	@Inject
	private ObservacionBusiness observacionBusiness;

	@Inject
	private CombosBusiness combosBusiness;

	@POST
	@Path("/crear")
	public Response crear(@Context HttpServletRequest request, Integer codigoObservacion) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			TramitanteDTO tramitanteDTO = (TramitanteDTO) request.getSession().getAttribute("tramitante");
			ObservacionDTO observacionDTO = new ObservacionDTO();
			observacionDTO.setCodigoCentroCosto(new Integer(usuarioDTO.getCentroCostoActivo().getCodigo()));
			observacionDTO.setCodigoObservacion(codigoObservacion);
			observacionDTO.setRutTramitante(tramitanteDTO.getRut());
			observacionDTO.setUsuario(usuarioDTO.getUsername());
			observacionBusiness.crear(observacionDTO);

			log.info("crear observacion tramitante, Usuario " + request.getUserPrincipal() + " - tramitante " + tramitanteDTO.getRut());
			return Response.ok(usuarioDTO).build();

		} catch (Exception ex) {
			log.error("crear observacion tramitante, Usuario " + request.getUserPrincipal()+ " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/listarobservacionestramitante")
	public Response listarObservacionesTramitante(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			TramitanteDTO tramitanteDTO = (TramitanteDTO) request.getSession().getAttribute("tramitante");
			ResponseListDTO<ObservacionDTO> respuesta = observacionBusiness.listar(tramitanteDTO.getRut());
			GenericEntity<List<ObservacionDTO>> list = new GenericEntity<List<ObservacionDTO>>(respuesta.getLista()) {
			};
			log.info("Listar Observaciones Tramitante, Usuario " + request.getUserPrincipal() + "  - Message: " + respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
			return Response.ok(list).build();

		} catch (Exception ex) {
			log.error("Listar Observaciones Tramitante, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/listarobservaciones")
	public Response listarObservaciones(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			List<ComboDTO> respuesta = combosBusiness.obtenerObservaciones();
			GenericEntity<List<ComboDTO>> list = new GenericEntity<List<ComboDTO>>(respuesta) {
			};

			log.info("Listar Observaciones, Usuario " + request.getUserPrincipal());
			return Response.ok(list).build();

		} catch (Exception ex) {
			log.error("Listar Observaciones, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
