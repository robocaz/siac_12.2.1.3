package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditoriaBoleta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditoriaBoleta.findAll", query="SELECT t FROM Tbl_AuditoriaBoleta t")
public class Tbl_AuditoriaBoleta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_BoletaFactura")
	private String fld_BoletaFactura;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_NroBoletaActual")
	private int fld_NroBoletaActual;

	@Column(name="Fld_NroBoletaFactura")
	private int fld_NroBoletaFactura;

	public Tbl_AuditoriaBoleta() {
	}

	public String getFld_BoletaFactura() {
		return this.fld_BoletaFactura;
	}

	public void setFld_BoletaFactura(String fld_BoletaFactura) {
		this.fld_BoletaFactura = fld_BoletaFactura;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public int getFld_NroBoletaActual() {
		return this.fld_NroBoletaActual;
	}

	public void setFld_NroBoletaActual(int fld_NroBoletaActual) {
		this.fld_NroBoletaActual = fld_NroBoletaActual;
	}

	public int getFld_NroBoletaFactura() {
		return this.fld_NroBoletaFactura;
	}

	public void setFld_NroBoletaFactura(int fld_NroBoletaFactura) {
		this.fld_NroBoletaFactura = fld_NroBoletaFactura;
	}

}