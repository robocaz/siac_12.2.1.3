package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CartasEmitidas database table.
 * 
 */
@Entity
@Table(name="Tbl_CartasEmitidas")
@NamedQuery(name="Tbl_CartasEmitida.findAll", query="SELECT t FROM Tbl_CartasEmitida t")
public class Tbl_CartasEmitida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuarioDigitacion")
	private String fld_CodUsuarioDigitacion;

	@Column(name="Fld_CorrCarta_Id")
	private BigDecimal fld_CorrCarta_Id;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoCarta")
	private short fld_TipoCarta;

	public Tbl_CartasEmitida() {
	}

	public String getFld_CodUsuarioDigitacion() {
		return this.fld_CodUsuarioDigitacion;
	}

	public void setFld_CodUsuarioDigitacion(String fld_CodUsuarioDigitacion) {
		this.fld_CodUsuarioDigitacion = fld_CodUsuarioDigitacion;
	}

	public BigDecimal getFld_CorrCarta_Id() {
		return this.fld_CorrCarta_Id;
	}

	public void setFld_CorrCarta_Id(BigDecimal fld_CorrCarta_Id) {
		this.fld_CorrCarta_Id = fld_CorrCarta_Id;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public short getFld_TipoCarta() {
		return this.fld_TipoCarta;
	}

	public void setFld_TipoCarta(short fld_TipoCarta) {
		this.fld_TipoCarta = fld_TipoCarta;
	}

}