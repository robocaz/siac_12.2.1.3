package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Finiquitos_old database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Finiquitos_old.findAll", query="SELECT t FROM Tbl_Finiquitos_old t")
public class Tbl_Finiquitos_old implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Articulo")
	private byte fld_Articulo;

	@Column(name="Fld_Cargo")
	private String fld_Cargo;

	@Column(name="Fld_CodAfp")
	private short fld_CodAfp;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodIsapre")
	private short fld_CodIsapre;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrDirAfectado")
	private BigDecimal fld_CorrDirAfectado;

	@Column(name="Fld_CorrDirEmpresa")
	private BigDecimal fld_CorrDirEmpresa;

	@Column(name="Fld_CorrFiniquito_Id")
	private BigDecimal fld_CorrFiniquito_Id;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_DiasTrabajados")
	private byte fld_DiasTrabajados;

	@Column(name="Fld_DocPresentado")
	private boolean fld_DocPresentado;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_Fax")
	private String fld_Fax;

	@Column(name="Fld_Fax_Emp")
	private String fld_Fax_Emp;

	@Column(name="Fld_FecCertCotizacion")
	private Timestamp fld_FecCertCotizacion;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecFinContrato")
	private Timestamp fld_FecFinContrato;

	@Column(name="Fld_FecIniContrato")
	private Timestamp fld_FecIniContrato;

	@Column(name="Fld_FecUltCotizacion")
	private Timestamp fld_FecUltCotizacion;

	@Column(name="Fld_Fono")
	private String fld_Fono;

	@Column(name="Fld_Fono_Emp")
	private String fld_Fono_Emp;

	@Column(name="Fld_MarcaAnulado")
	private boolean fld_MarcaAnulado;

	@Column(name="Fld_Nombre_ApMat_Emp")
	private String fld_Nombre_ApMat_Emp;

	@Column(name="Fld_Nombre_ApPat_Emp")
	private String fld_Nombre_ApPat_Emp;

	@Column(name="Fld_Nombre_Nombres_Emp")
	private String fld_Nombre_Nombres_Emp;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutEmpresa")
	private String fld_RutEmpresa;

	@Column(name="Fld_SueldoBase")
	private BigDecimal fld_SueldoBase;

	@Column(name="Fld_TotPagado")
	private BigDecimal fld_TotPagado;

	public Tbl_Finiquitos_old() {
	}

	public byte getFld_Articulo() {
		return this.fld_Articulo;
	}

	public void setFld_Articulo(byte fld_Articulo) {
		this.fld_Articulo = fld_Articulo;
	}

	public String getFld_Cargo() {
		return this.fld_Cargo;
	}

	public void setFld_Cargo(String fld_Cargo) {
		this.fld_Cargo = fld_Cargo;
	}

	public short getFld_CodAfp() {
		return this.fld_CodAfp;
	}

	public void setFld_CodAfp(short fld_CodAfp) {
		this.fld_CodAfp = fld_CodAfp;
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodIsapre() {
		return this.fld_CodIsapre;
	}

	public void setFld_CodIsapre(short fld_CodIsapre) {
		this.fld_CodIsapre = fld_CodIsapre;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrDirAfectado() {
		return this.fld_CorrDirAfectado;
	}

	public void setFld_CorrDirAfectado(BigDecimal fld_CorrDirAfectado) {
		this.fld_CorrDirAfectado = fld_CorrDirAfectado;
	}

	public BigDecimal getFld_CorrDirEmpresa() {
		return this.fld_CorrDirEmpresa;
	}

	public void setFld_CorrDirEmpresa(BigDecimal fld_CorrDirEmpresa) {
		this.fld_CorrDirEmpresa = fld_CorrDirEmpresa;
	}

	public BigDecimal getFld_CorrFiniquito_Id() {
		return this.fld_CorrFiniquito_Id;
	}

	public void setFld_CorrFiniquito_Id(BigDecimal fld_CorrFiniquito_Id) {
		this.fld_CorrFiniquito_Id = fld_CorrFiniquito_Id;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public byte getFld_DiasTrabajados() {
		return this.fld_DiasTrabajados;
	}

	public void setFld_DiasTrabajados(byte fld_DiasTrabajados) {
		this.fld_DiasTrabajados = fld_DiasTrabajados;
	}

	public boolean getFld_DocPresentado() {
		return this.fld_DocPresentado;
	}

	public void setFld_DocPresentado(boolean fld_DocPresentado) {
		this.fld_DocPresentado = fld_DocPresentado;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public String getFld_Fax() {
		return this.fld_Fax;
	}

	public void setFld_Fax(String fld_Fax) {
		this.fld_Fax = fld_Fax;
	}

	public String getFld_Fax_Emp() {
		return this.fld_Fax_Emp;
	}

	public void setFld_Fax_Emp(String fld_Fax_Emp) {
		this.fld_Fax_Emp = fld_Fax_Emp;
	}

	public Timestamp getFld_FecCertCotizacion() {
		return this.fld_FecCertCotizacion;
	}

	public void setFld_FecCertCotizacion(Timestamp fld_FecCertCotizacion) {
		this.fld_FecCertCotizacion = fld_FecCertCotizacion;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecFinContrato() {
		return this.fld_FecFinContrato;
	}

	public void setFld_FecFinContrato(Timestamp fld_FecFinContrato) {
		this.fld_FecFinContrato = fld_FecFinContrato;
	}

	public Timestamp getFld_FecIniContrato() {
		return this.fld_FecIniContrato;
	}

	public void setFld_FecIniContrato(Timestamp fld_FecIniContrato) {
		this.fld_FecIniContrato = fld_FecIniContrato;
	}

	public Timestamp getFld_FecUltCotizacion() {
		return this.fld_FecUltCotizacion;
	}

	public void setFld_FecUltCotizacion(Timestamp fld_FecUltCotizacion) {
		this.fld_FecUltCotizacion = fld_FecUltCotizacion;
	}

	public String getFld_Fono() {
		return this.fld_Fono;
	}

	public void setFld_Fono(String fld_Fono) {
		this.fld_Fono = fld_Fono;
	}

	public String getFld_Fono_Emp() {
		return this.fld_Fono_Emp;
	}

	public void setFld_Fono_Emp(String fld_Fono_Emp) {
		this.fld_Fono_Emp = fld_Fono_Emp;
	}

	public boolean getFld_MarcaAnulado() {
		return this.fld_MarcaAnulado;
	}

	public void setFld_MarcaAnulado(boolean fld_MarcaAnulado) {
		this.fld_MarcaAnulado = fld_MarcaAnulado;
	}

	public String getFld_Nombre_ApMat_Emp() {
		return this.fld_Nombre_ApMat_Emp;
	}

	public void setFld_Nombre_ApMat_Emp(String fld_Nombre_ApMat_Emp) {
		this.fld_Nombre_ApMat_Emp = fld_Nombre_ApMat_Emp;
	}

	public String getFld_Nombre_ApPat_Emp() {
		return this.fld_Nombre_ApPat_Emp;
	}

	public void setFld_Nombre_ApPat_Emp(String fld_Nombre_ApPat_Emp) {
		this.fld_Nombre_ApPat_Emp = fld_Nombre_ApPat_Emp;
	}

	public String getFld_Nombre_Nombres_Emp() {
		return this.fld_Nombre_Nombres_Emp;
	}

	public void setFld_Nombre_Nombres_Emp(String fld_Nombre_Nombres_Emp) {
		this.fld_Nombre_Nombres_Emp = fld_Nombre_Nombres_Emp;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutEmpresa() {
		return this.fld_RutEmpresa;
	}

	public void setFld_RutEmpresa(String fld_RutEmpresa) {
		this.fld_RutEmpresa = fld_RutEmpresa;
	}

	public BigDecimal getFld_SueldoBase() {
		return this.fld_SueldoBase;
	}

	public void setFld_SueldoBase(BigDecimal fld_SueldoBase) {
		this.fld_SueldoBase = fld_SueldoBase;
	}

	public BigDecimal getFld_TotPagado() {
		return this.fld_TotPagado;
	}

	public void setFld_TotPagado(BigDecimal fld_TotPagado) {
		this.fld_TotPagado = fld_TotPagado;
	}

}