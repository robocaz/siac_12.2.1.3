package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Parametros database table.
 * 
 */
@Entity
@Table(name="Tbl_Parametros")
@NamedQuery(name="Tbl_Parametro.findAll", query="SELECT t FROM Tbl_Parametro t")
public class Tbl_Parametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_ParametroPK id;

	@Column(name="Fld_Columna")
	private String fld_Columna;

	@Column(name="Fld_Glosa")
	private String fld_Glosa;

	@Column(name="Fld_NombreParametro")
	private String fld_NombreParametro;

	@Column(name="Fld_Tabla")
	private String fld_Tabla;

	@Column(name="Fld_Valor")
	private BigDecimal fld_Valor;

	public Tbl_Parametro() {
	}

	public Tbl_ParametroPK getId() {
		return this.id;
	}

	public void setId(Tbl_ParametroPK id) {
		this.id = id;
	}

	public String getFld_Columna() {
		return this.fld_Columna;
	}

	public void setFld_Columna(String fld_Columna) {
		this.fld_Columna = fld_Columna;
	}

	public String getFld_Glosa() {
		return this.fld_Glosa;
	}

	public void setFld_Glosa(String fld_Glosa) {
		this.fld_Glosa = fld_Glosa;
	}

	public String getFld_NombreParametro() {
		return this.fld_NombreParametro;
	}

	public void setFld_NombreParametro(String fld_NombreParametro) {
		this.fld_NombreParametro = fld_NombreParametro;
	}

	public String getFld_Tabla() {
		return this.fld_Tabla;
	}

	public void setFld_Tabla(String fld_Tabla) {
		this.fld_Tabla = fld_Tabla;
	}

	public BigDecimal getFld_Valor() {
		return this.fld_Valor;
	}

	public void setFld_Valor(BigDecimal fld_Valor) {
		this.fld_Valor = fld_Valor;
	}

}