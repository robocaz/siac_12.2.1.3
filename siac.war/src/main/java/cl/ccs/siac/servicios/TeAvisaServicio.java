package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.pago.PagoBusiness;
import cl.ccs.siac.persona.PersonaBusiness;
import cl.ccs.siac.teavisa.TeAvisaBusiness;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/teavisa")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class TeAvisaServicio {
	
	private static final Logger log = LogManager.getLogger(TeAvisaServicio.class);

	@Inject
	private TeAvisaBusiness teAvisaBusiness;
	
	@Inject
	private PagoBusiness pagoBusiness;
	
	@Inject
	private PersonaBusiness personaBusiness;
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/contratar")
	public Response contratar(@Context HttpServletRequest request, TeAvisaDTO teavisa) {
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		
		PersonaDTO personaActualizada = personaBusiness.actualizar(teavisa.getAfectado());
		teavisa.getAfectado().setCorrelativo(personaActualizada.getCorrelativo());
		
		try{
			teavisa.setCorrCaja(Integer.parseInt(usuarioDTO.getCaja()));
			teavisa.setCentroCosto(Integer.parseInt(usuarioDTO.getCentroCostoActivo().getCodigo()));
			
			String codigoServicio = "";
			
				if(teavisa.getServcorreo() && !teavisa.getServsms()){
					codigoServicio = "ARM";
				}
				if(!teavisa.getServcorreo() && teavisa.getServsms()){
					codigoServicio = "AMM";
				}		
				if(teavisa.getServcorreo() && teavisa.getServsms()){
					codigoServicio = "ADM";
				}
				teavisa.setTipoServicio(codigoServicio);
	
			if(!teavisa.isUpgrade() && teavisa.getCorrContrato() != null){
			
				String[] ram = {"ABI","ABW","ARW","ARM"};
				String[] rpm = {"PVU","PVW","PRM","PRW"};
				String[] rrm = {"CTP","CRM"};
				String[] rdm = {"ADW","ADM"};
				String[] rmm = {"AMM","AMW"};
				
				
				if(Arrays.asList(ram).contains(teavisa.getTipoServicio())){
					teavisa.setTipoServicio("RAM");
				}
				
				if(Arrays.asList(rdm).contains(teavisa.getTipoServicio())){
					teavisa.setTipoServicio("RDM");
				}		
				if(Arrays.asList(rmm).contains(teavisa.getTipoServicio())){
					teavisa.setTipoServicio("RMM");
				}				
			}
			
			if(teavisa.isUpgrade() && teavisa.getCorrContrato() != null){
				teavisa.setTipoServicio("AUM");
			}
			
			String valorServicio = pagoBusiness.valorServicio(teavisa.getTipoServicio()).getMsgControl();
			teavisa.setMontoServicio(Integer.parseInt(valorServicio));
			
			if(teavisa.getTipoServicio().equals("AUM")){
				Long costoServicio = teAvisaBusiness.costoServicioUpgrade(teavisa.getCorrContrato(), teavisa.getTipoServicio());
				teavisa.setMontoServicio(costoServicio.intValue());
			}
			
			
			teavisa = teAvisaBusiness.contrataTeAvisa(teavisa);
			
			return Response.ok(teavisa).build();
		}
		catch(Exception e){
			log.error("Metodo contratar  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/validacontrato")
	public Response validaContrato(@Context HttpServletRequest request, @QueryParam("rut") String rut) {
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try{
			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			
			ResponseDTO respuesta = teAvisaBusiness.validaContratoTeAvisa(rut);
			
			return Response.ok(respuesta).build();
		}
		catch(Exception e){
			log.error("Metodo validaContrato - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/buscacontratos")
	public Response buscaContratos(@Context HttpServletRequest request, @QueryParam("rut") String rut) {
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		try{
			List<TeAvisaDTO> respuesta = teAvisaBusiness.buscaContratos(rut);
			
			return Response.ok(respuesta).build();
		}
		catch(Exception e){
			log.error("Metodo buscaContratos - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}		
	}
	
	
	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/buscacontratosrenovacion")
	public Response buscaContratosRenovacion(@Context HttpServletRequest request, 
			@QueryParam("rut") String rut,
			@QueryParam("criterio") String criterio,
			@QueryParam("correlativo") String correlativo) {
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try{
			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
	
			List<TeAvisaDTO> respuesta = teAvisaBusiness.buscaContratosRenovacion(rut, Integer.parseInt(criterio), Long.parseLong(correlativo));
			
			return Response.ok(respuesta).build();
		}
		catch(Exception e){
			log.error("Metodo buscaContratosRenovacion - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}	
	}

}
