package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ley_RutMol database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_RutMol.findAll", query="SELECT l FROM Ley_RutMol l")
public class Ley_RutMol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Rut")
	private String rut;

	public Ley_RutMol() {
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}