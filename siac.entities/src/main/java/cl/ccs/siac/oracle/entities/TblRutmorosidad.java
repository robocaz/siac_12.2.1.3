package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_RUTMOROSIDAD database table.
 * 
 */
@Entity
@Table(name="TBL_RUTMOROSIDAD")
@NamedQuery(name="TblRutmorosidad.findAll", query="SELECT t FROM TblRutmorosidad t")
public class TblRutmorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TblRutmorosidadPK id;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_TIPORELACION")
	private String fldTiporelacion;

	//bi-directional many-to-one association to TblMorosidad
	@ManyToOne
	@JoinColumn(name="FLD_CORRMOROSIDAD")
	private TblMorosidad tblMorosidad;

	//bi-directional many-to-one association to TblNombre
	@ManyToOne
	@JoinColumn(name="FLD_CORRNOMBRE")
	private TblNombre tblNombre;

	//bi-directional many-to-one association to TblDireccion
	@ManyToOne
	@JoinColumn(name="FLD_CORRDIRECCION")
	private TblDireccion tblDireccion;

	public TblRutmorosidad() {
	}

	public TblRutmorosidadPK getId() {
		return this.id;
	}

	public void setId(TblRutmorosidadPK id) {
		this.id = id;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public String getFldTiporelacion() {
		return this.fldTiporelacion;
	}

	public void setFldTiporelacion(String fldTiporelacion) {
		this.fldTiporelacion = fldTiporelacion;
	}

	public TblMorosidad getTblMorosidad() {
		return this.tblMorosidad;
	}

	public void setTblMorosidad(TblMorosidad tblMorosidad) {
		this.tblMorosidad = tblMorosidad;
	}

	public TblNombre getTblNombre() {
		return this.tblNombre;
	}

	public void setTblNombre(TblNombre tblNombre) {
		this.tblNombre = tblNombre;
	}

	public TblDireccion getTblDireccion() {
		return this.tblDireccion;
	}

	public void setTblDireccion(TblDireccion tblDireccion) {
		this.tblDireccion = tblDireccion;
	}

}