package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tb_CeIdNombres database table.
 * 
 */
@Embeddable
public class Tb_CeIdNombrePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_CorrNombre_Id")
	private long fld_CorrNombre_Id;

	public Tb_CeIdNombrePK() {
	}
	public String getFld_Rut() {
		return this.fld_Rut;
	}
	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}
	public long getFld_CorrNombre_Id() {
		return this.fld_CorrNombre_Id;
	}
	public void setFld_CorrNombre_Id(long fld_CorrNombre_Id) {
		this.fld_CorrNombre_Id = fld_CorrNombre_Id;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tb_CeIdNombrePK)) {
			return false;
		}
		Tb_CeIdNombrePK castOther = (Tb_CeIdNombrePK)other;
		return 
			this.fld_Rut.equals(castOther.fld_Rut)
			&& (this.fld_CorrNombre_Id == castOther.fld_CorrNombre_Id);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fld_Rut.hashCode();
		hash = hash * prime + ((int) (this.fld_CorrNombre_Id ^ (this.fld_CorrNombre_Id >>> 32)));
		
		return hash;
	}
}