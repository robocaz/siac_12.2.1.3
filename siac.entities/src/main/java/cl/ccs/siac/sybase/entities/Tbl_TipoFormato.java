package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoFormato database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoFormato.findAll", query="SELECT t FROM Tbl_TipoFormato t")
public class Tbl_TipoFormato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Flag_AWK")
	private boolean fld_Flag_AWK;

	@Column(name="Fld_Flag_CortaReg")
	private boolean fld_Flag_CortaReg;

	@Column(name="Fld_Flag_EliCR")
	private boolean fld_Flag_EliCR;

	@Column(name="Fld_Flag_Filtro")
	private boolean fld_Flag_Filtro;

	@Column(name="Fld_Flag_SepNom")
	private boolean fld_Flag_SepNom;

	@Column(name="Fld_FormatoProtesto")
	private short fld_FormatoProtesto;

	@Column(name="Fld_GlosaFormato")
	private String fld_GlosaFormato;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_TipoFormato() {
	}

	public boolean getFld_Flag_AWK() {
		return this.fld_Flag_AWK;
	}

	public void setFld_Flag_AWK(boolean fld_Flag_AWK) {
		this.fld_Flag_AWK = fld_Flag_AWK;
	}

	public boolean getFld_Flag_CortaReg() {
		return this.fld_Flag_CortaReg;
	}

	public void setFld_Flag_CortaReg(boolean fld_Flag_CortaReg) {
		this.fld_Flag_CortaReg = fld_Flag_CortaReg;
	}

	public boolean getFld_Flag_EliCR() {
		return this.fld_Flag_EliCR;
	}

	public void setFld_Flag_EliCR(boolean fld_Flag_EliCR) {
		this.fld_Flag_EliCR = fld_Flag_EliCR;
	}

	public boolean getFld_Flag_Filtro() {
		return this.fld_Flag_Filtro;
	}

	public void setFld_Flag_Filtro(boolean fld_Flag_Filtro) {
		this.fld_Flag_Filtro = fld_Flag_Filtro;
	}

	public boolean getFld_Flag_SepNom() {
		return this.fld_Flag_SepNom;
	}

	public void setFld_Flag_SepNom(boolean fld_Flag_SepNom) {
		this.fld_Flag_SepNom = fld_Flag_SepNom;
	}

	public short getFld_FormatoProtesto() {
		return this.fld_FormatoProtesto;
	}

	public void setFld_FormatoProtesto(short fld_FormatoProtesto) {
		this.fld_FormatoProtesto = fld_FormatoProtesto;
	}

	public String getFld_GlosaFormato() {
		return this.fld_GlosaFormato;
	}

	public void setFld_GlosaFormato(String fld_GlosaFormato) {
		this.fld_GlosaFormato = fld_GlosaFormato;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}