package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_EMISORCONSOLIDADOAUX database table.
 * 
 */
@Entity
@Table(name="TBL_EMISORCONSOLIDADOAUX")
@NamedQuery(name="TblEmisorconsolidadoaux.findAll", query="SELECT t FROM TblEmisorconsolidadoaux t")
public class TblEmisorconsolidadoaux implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODEMISORAUX")
	private String fldCodemisoraux;

	@Column(name="FLD_ORDEN")
	private BigDecimal fldOrden;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblEmisorconsolidadoaux() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public String getFldCodemisoraux() {
		return this.fldCodemisoraux;
	}

	public void setFldCodemisoraux(String fldCodemisoraux) {
		this.fldCodemisoraux = fldCodemisoraux;
	}

	public BigDecimal getFldOrden() {
		return this.fldOrden;
	}

	public void setFldOrden(BigDecimal fldOrden) {
		this.fldOrden = fldOrden;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}