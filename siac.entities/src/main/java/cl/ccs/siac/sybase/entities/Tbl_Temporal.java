package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Temporal database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Temporal.findAll", query="SELECT t FROM Tbl_Temporal t")
public class Tbl_Temporal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FechaHora")
	private Timestamp fld_FechaHora;

	@Column(name="Fld_Texto")
	private String fld_Texto;

	public Tbl_Temporal() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FechaHora() {
		return this.fld_FechaHora;
	}

	public void setFld_FechaHora(Timestamp fld_FechaHora) {
		this.fld_FechaHora = fld_FechaHora;
	}

	public String getFld_Texto() {
		return this.fld_Texto;
	}

	public void setFld_Texto(String fld_Texto) {
		this.fld_Texto = fld_Texto;
	}

}