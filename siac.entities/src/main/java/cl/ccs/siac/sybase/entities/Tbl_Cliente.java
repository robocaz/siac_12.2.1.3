package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Clientes database table.
 * 
 */
@Entity
@Table(name="Tbl_Clientes")
@NamedQuery(name="Tbl_Cliente.findAll", query="SELECT t FROM Tbl_Cliente t")
public class Tbl_Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalidadJuridica")
	private String fld_CalidadJuridica;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_DireccionCliente")
	private String fld_DireccionCliente;

	@Column(name="Fld_FaxCliente")
	private String fld_FaxCliente;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FonoCliente")
	private String fld_FonoCliente;

	@Column(name="Fld_RazonSocialCli")
	private String fld_RazonSocialCli;

	@Column(name="Fld_RutCliente")
	private String fld_RutCliente;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Cliente() {
	}

	public String getFld_CalidadJuridica() {
		return this.fld_CalidadJuridica;
	}

	public void setFld_CalidadJuridica(String fld_CalidadJuridica) {
		this.fld_CalidadJuridica = fld_CalidadJuridica;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_DireccionCliente() {
		return this.fld_DireccionCliente;
	}

	public void setFld_DireccionCliente(String fld_DireccionCliente) {
		this.fld_DireccionCliente = fld_DireccionCliente;
	}

	public String getFld_FaxCliente() {
		return this.fld_FaxCliente;
	}

	public void setFld_FaxCliente(String fld_FaxCliente) {
		this.fld_FaxCliente = fld_FaxCliente;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public String getFld_FonoCliente() {
		return this.fld_FonoCliente;
	}

	public void setFld_FonoCliente(String fld_FonoCliente) {
		this.fld_FonoCliente = fld_FonoCliente;
	}

	public String getFld_RazonSocialCli() {
		return this.fld_RazonSocialCli;
	}

	public void setFld_RazonSocialCli(String fld_RazonSocialCli) {
		this.fld_RazonSocialCli = fld_RazonSocialCli;
	}

	public String getFld_RutCliente() {
		return this.fld_RutCliente;
	}

	public void setFld_RutCliente(String fld_RutCliente) {
		this.fld_RutCliente = fld_RutCliente;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}