package cl.ccs.siac.dao;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("personaDao")
@Stateless
public class PersonaDaoImpl implements PersonaDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	private static final Logger log = LogManager.getLogger(PersonaDaoImpl.class);
	
	public PersonaDTO obtener(String rut) {
		
		PersonaDTO personaDTO = new PersonaDTO();
		
		try {
			log.info("Ejecutando SQ_Nombre_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_Nombre_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutPersona", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RowCount", Integer.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("Fld_RutPersona", rut);

			storedProcedure.execute();
			
			personaDTO.setRut(rut);
			personaDTO.setNombres(storedProcedure.getOutputParameterValue("Fld_Nombre_Nombres").toString());
			personaDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApPat").toString());
			personaDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApMat").toString());
			personaDTO.setCorrelativo(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CorrNombre").toString()));
			personaDTO.setCantidadFila((Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_RowCount").toString())));
			personaDTO.setIdControl(0);
			return personaDTO;
			
		} catch (Exception e) {
			log.error("Error en el SP SQ_Nombre_SW2 " + e.getMessage());
			return null;
		}
	}
	
	public PersonaDTO getNombre(String rut, Long correlativo) {
		
		PersonaDTO personaDTO = new PersonaDTO();
		
		try {
			log.info("Ejecutando SQ_TraeNombre_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TraeNombre_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_ApPaterno", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_ApMaterno", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("Fld_RutAfectado", rut);
			storedProcedure.setParameter("Fld_CorrNombre", correlativo);

			storedProcedure.execute();
			
			personaDTO.setRut(rut);
			personaDTO.setNombres(storedProcedure.getOutputParameterValue("Fld_Nombre").toString());
			personaDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_ApPaterno").toString());
			personaDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_ApMaterno").toString());
			personaDTO.setCorrelativo(correlativo);
			personaDTO.setIdControl(0);
			return personaDTO;
			
		} catch (Exception e) {
			log.error("Error en el SP SQ_TraeNombre_SW2 " + e.getMessage());
			return null;
		}
	}
	public PersonaDTO actualizar(PersonaDTO personaDTO){
		
		try {
			
			log.info("Ejecutando SP_Nombre_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_Nombre_SW2");
			
			storedProcedure.registerStoredProcedureParameter("Fld_RutPersona", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_RutPersona", personaDTO.getRut());
			if(personaDTO.getApellidoPaterno() != null){
				storedProcedure.setParameter("Fld_Nombre_ApPat", personaDTO.getApellidoPaterno());
			}
			else{
				storedProcedure.setParameter("Fld_Nombre_ApPat", "");
			}
			if(personaDTO.getApellidoMaterno() != null){
				storedProcedure.setParameter("Fld_Nombre_ApMat", personaDTO.getApellidoMaterno());
			}
			else{
				storedProcedure.setParameter("Fld_Nombre_ApMat", "");
			}
			storedProcedure.setParameter("Fld_Nombre_Nombres", personaDTO.getNombres());
			
			storedProcedure.execute();
			
			personaDTO.setCorrelativo(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CorrNombre").toString()));
		
		} catch (Exception e) {
			log.error("Error en el SP SP_Nombre_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return personaDTO;
	
	}
	
public ResponseDTO verificar(String rut){
		ResponseDTO resp = new ResponseDTO();
		
		try {
			log.info("Ejecutando SQ_ValEmiCertLegal_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValEmiCertLegal_SW2");
			
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_RutAfectado", rut);
			
			storedProcedure.execute();
			
			String verif = storedProcedure.getOutputParameterValue("Fld_Retorno").toString();
			if(verif.equalsIgnoreCase("0")) resp.setIdControl(0);
			else {
				resp.setIdControl(1);
				resp.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_ValEmiCertLegal_SW2 " + e.getMessage());
			return null;
		}	
		return resp;
	}
	
}
