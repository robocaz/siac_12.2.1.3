angular.module('ccs').factory('AclaracionResource', function ($resource) {
    var resource = $resource('rest/aclaracion',{
        nocache : new Date().getTime()
    },{
        'tiposEmisores' :   {  method : 'GET',params:{tipoDoc:'tipoDoc'},  isArray : true, url: 'rest/aclaracion/tiposEmisores'},
        'tiposEmisoresTodos' :   {  method : 'GET',isArray : true, url: 'rest/aclaracion/tiposEmisoresTodos'},
        'emisores' :   {  method : 'GET', params:{tipoEmisor:'tipoEmisor'}, isArray : true, url: 'rest/aclaracion/emisores'},
        'documentos' :   {  method : 'GET', isArray : true, params:{tipoDoc:'tipoDoc'}, url: 'rest/aclaracion/docsaclaracion'},
        'cargaSucursales' :   {  method : 'GET', params:{tipoEmisor:'tipoEmisor', codEmisor:'codEmisor'}, isArray : true, url: 'rest/aclaracion/sucursales'},
        'tiposDocumentos' :   {  method : 'GET', isArray : true, url: 'rest/aclaracion/tiposdocumentos'},
        'tiposMoneda' :   {  method : 'GET', isArray : true, url: 'rest/aclaracion/tipomoneda'},
        'buscarFirma' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/buscafirmas'},
        'cargaImagen' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/cargaimagen'},
        'reqConfirmatorio' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/reqconfirmatorio'},
        'numConfirmatorio' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/numconfirmatorio'},
        'aceptaAclaracion' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/aceptaaclaracion'},
        'aclaraCandidato' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/aclaracandidato'},
        'buscaCuotasBcoEstado' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/cuotasbcoestado'},
        'aclaraCuotasBcoEstado' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/aclaracuotas'},
        'tieneVentaCartera' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/ventacartera'},
        'validaVentaCartera' :   {  method : 'GET', params:{tipoEmisor:'tipoEmisor', codEmisor:'codEmisor', corrProt:'corrProt'}, isArray : false, url: 'rest/aclaracion/validaprotestocartera'},
        'eliminaProtestoTemp' :   {  method : 'GET', params:{corrProt:'corrProt', corrCaja:'corrCaja'}, isArray : false, url: 'rest/aclaracion/eliminaprotesto'},
        'guardaBipersonal' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/guardabipersonal'},
        'consultaAclaracionProceso' :   {  method : 'GET', params:{rutTramitante:'rutTramitante', corrCaja:'corrCaja'}, isArray : false, url: 'rest/aclaracion/consultaproceso'},
        'consultaRutFecha' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/consultaRutFecha'},
        'consultaAclsCertificado' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/consultaAclsCertificado'},
        'validaCarro' :   {  method : 'POST', isArray : false, url: 'rest/aclaracion/consultacarro'}
    });
    return resource;
});
