package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclBatch.findAll", query="SELECT t FROM Tbl_AclBatch t")
public class Tbl_AclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodRetAcl")
	private byte fld_CodRetAcl;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrAclBat_Id")
	private BigDecimal fld_CorrAclBat_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tbl_AclBatch() {
	}

	public byte getFld_CodRetAcl() {
		return this.fld_CodRetAcl;
	}

	public void setFld_CodRetAcl(byte fld_CodRetAcl) {
		this.fld_CodRetAcl = fld_CodRetAcl;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrAclBat_Id() {
		return this.fld_CorrAclBat_Id;
	}

	public void setFld_CorrAclBat_Id(BigDecimal fld_CorrAclBat_Id) {
		this.fld_CorrAclBat_Id = fld_CorrAclBat_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}