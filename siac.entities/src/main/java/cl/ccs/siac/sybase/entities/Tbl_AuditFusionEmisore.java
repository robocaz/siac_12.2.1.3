package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditFusionEmisores database table.
 * 
 */
@Entity
@Table(name="Tbl_AuditFusionEmisores")
@NamedQuery(name="Tbl_AuditFusionEmisore.findAll", query="SELECT t FROM Tbl_AuditFusionEmisore t")
public class Tbl_AuditFusionEmisore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisorHijo")
	private String fld_CodEmisorHijo;

	@Column(name="Fld_CodEmisorPadre")
	private String fld_CodEmisorPadre;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisorHijo")
	private String fld_TipoEmisorHijo;

	@Column(name="Fld_TipoEmisorPadre")
	private String fld_TipoEmisorPadre;

	public Tbl_AuditFusionEmisore() {
	}

	public String getFld_CodEmisorHijo() {
		return this.fld_CodEmisorHijo;
	}

	public void setFld_CodEmisorHijo(String fld_CodEmisorHijo) {
		this.fld_CodEmisorHijo = fld_CodEmisorHijo;
	}

	public String getFld_CodEmisorPadre() {
		return this.fld_CodEmisorPadre;
	}

	public void setFld_CodEmisorPadre(String fld_CodEmisorPadre) {
		this.fld_CodEmisorPadre = fld_CodEmisorPadre;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisorHijo() {
		return this.fld_TipoEmisorHijo;
	}

	public void setFld_TipoEmisorHijo(String fld_TipoEmisorHijo) {
		this.fld_TipoEmisorHijo = fld_TipoEmisorHijo;
	}

	public String getFld_TipoEmisorPadre() {
		return this.fld_TipoEmisorPadre;
	}

	public void setFld_TipoEmisorPadre(String fld_TipoEmisorPadre) {
		this.fld_TipoEmisorPadre = fld_TipoEmisorPadre;
	}

}