package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.BoletinDataDetalle;
import cl.exe.ccs.dto.BoletinDataEmpleador;
import cl.exe.ccs.dto.BoletinDataTrabajador;
import cl.exe.ccs.dto.BoletinInDTO;
import cl.exe.ccs.dto.BoletinOutDTO;

@Named("boletinDao")
@Stateless
public class BoletinDaoImpl implements BoletinDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;

	private static final Logger LOG = LogManager.getLogger(BoletinDaoImpl.class);

	@Override
	public BoletinOutDTO<BoletinDataTrabajador> consultaTrabajador(BoletinInDTO params) {
		LOG.info("Ejecutando el SP: SQ_BolLabTrabajador_SW2");
		
		BoletinOutDTO<BoletinDataTrabajador> obj = new BoletinOutDTO<BoletinDataTrabajador>();
		
		try {
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_BolLabTrabajador_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getRutAfectado());
			spq.setParameter(2, params.getCorrMovimiento());
			spq.setParameter(3, params.getCodServicio());
			spq.setParameter(4, params.getCodUsuario());
			
			LOG.debug("> Params: " + params.toString());
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			List<BoletinDataTrabajador> lista = new ArrayList<BoletinDataTrabajador>();
			
			for ( Object item : result ) {
				Object[] datos = (Object[]) item;
				BoletinDataTrabajador fila = new BoletinDataTrabajador();
				
				fila.setRut(datos[0].toString());
				fila.setNombre(datos[1].toString());
				fila.setPeriodo((Integer) datos[2]);
				fila.setMontoAdeudado((BigDecimal) datos[3]);
				fila.setMontoLaboralUTM((BigDecimal) datos[4]);
				fila.setNombreInstitucion(datos[5].toString().trim());
				fila.setTipoDeudaLaboral(datos[6].toString().trim());
				
				lista.add(fila);
			}
			
			obj.setLista(lista);
			
			obj.setNroDeudasLaboral((Integer) spq.getOutputParameterValue(5));
			obj.setMontoDeudaLaboral((BigDecimal) spq.getOutputParameterValue(6));
			obj.setNroTrabajadores((Integer) spq.getOutputParameterValue(7));
			obj.setNombreInfractor(spq.getOutputParameterValue(8).toString());
			obj.setCodAutenticBic(spq.getOutputParameterValue(9).toString());
			
			obj.setIdControl((Integer) spq.getOutputParameterValue(10));
			obj.setMsgControl((spq.getOutputParameterValue(11) != null) ? spq.getOutputParameterValue(11).toString() : new String());
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_BolLabTrabajador_SW2 " + e.getMessage());
			obj.setMsgControl("ERR");
		}
		
		return obj;
	}

	@Override
	public BoletinOutDTO<BoletinDataDetalle> consultaInfractor(BoletinInDTO params) {

		LOG.info("Ejecutando el SP: SQ_BolLabInfractor_SW2");
		
		BoletinOutDTO<BoletinDataDetalle> obj = new BoletinOutDTO<BoletinDataDetalle>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_BolLabInfractor_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(5, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(12, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(13, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(14, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(15, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(16, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getRutAfectado());
			spq.setParameter(2, params.getCorrMovimiento());
			spq.setParameter(3, params.getCodServicio());
			spq.setParameter(4, params.getCodUsuario());
			
			LOG.debug("> Params: " + params.toString());
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			List<BoletinDataDetalle> lista = new ArrayList<BoletinDataDetalle>();
			
			for ( Object item : result ) {
				Object[] datos = (Object[]) item;
				BoletinDataDetalle fila = new BoletinDataDetalle();
				
				fila.setTipoDeudaLaboral(datos[0].toString());
				fila.setGlosaTipoAcreedor(datos[1].toString());
				fila.setNombreInstitucion(datos[2].toString().trim());
				fila.setNroBolLaboral((Integer) datos[3]);
				fila.setPagBolLaboral((Integer) datos[4]);
				fila.setFecBolLaboral(String.valueOf(datos[5]));
				fila.setMotivoInfraccion(datos[6].toString());
				fila.setRut(datos[7].toString());
				fila.setNombre(datos[8].toString());
				fila.setPeriodo((Integer) datos[9]);
				fila.setMontoLaboral((BigDecimal) datos[10]);
				fila.setMontoLaboralUTM((BigDecimal) datos[11]);
				fila.setCodRegionInspeccion((Integer) datos[12]);
				fila.setAnoResolMulta((Integer) datos[13]);
				fila.setNroResolMulta((Integer) datos[14]);
				fila.setTipoMulta(datos[15].toString());
				fila.setMeses((Integer) datos[16]);
				fila.setNroCotizaciones((Integer) datos[17]);
				fila.setMontoAdeudado((BigDecimal) datos[18]);
				
				lista.add(fila);
			}
			
			obj.setLista(lista);
			
			obj.setFecUltimaMulta(String.valueOf(spq.getOutputParameterValue(5)));
			obj.setFecUltimaDeuda(String.valueOf(spq.getOutputParameterValue(6)));
			obj.setNroMultasLaboral((Integer) spq.getOutputParameterValue(7));
			obj.setNroDeudasLaboral((Integer) spq.getOutputParameterValue(8));
			obj.setMontoMultaLaboral((BigDecimal) spq.getOutputParameterValue(9));
			obj.setMontoDeudaLaboral((BigDecimal) spq.getOutputParameterValue(10));
			obj.setNroAcreedores((Integer) spq.getOutputParameterValue(11));
			obj.setNroTrabajadores((Integer) spq.getOutputParameterValue(12));
			obj.setNombreInfractor(spq.getOutputParameterValue(13).toString());
			obj.setCodAutenticBic(spq.getOutputParameterValue(14).toString());
			
			obj.setIdControl((Integer) spq.getOutputParameterValue(15));
			obj.setMsgControl((spq.getOutputParameterValue(16) != null) ? spq.getOutputParameterValue(16).toString() : new String());
			
		} catch (Exception e) {
			LOG.error("Error en la ejecucion del SP SQ_BolLabInfractor_SW2 " + e.getMessage());
			obj.setMsgControl("ERR");
		}
		
		return obj;
	}

	@Override
	public BoletinOutDTO<BoletinDataEmpleador> consultaResumen(BoletinInDTO params) {
		
		LOG.info("Ejecutando el SP: SQ_BolLabResumen_SW2");
		
		BoletinOutDTO<BoletinDataEmpleador> obj = new BoletinOutDTO<BoletinDataEmpleador>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_BolLabResumen_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(5, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, Timestamp.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(10, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(12, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(13, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(14, String.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(15, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(16, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getRutAfectado());
			spq.setParameter(2, params.getCorrMovimiento());
			spq.setParameter(3, params.getCodServicio());
			spq.setParameter(4, params.getCodUsuario());
			
			LOG.debug("> Params: " + params.toString());
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			List<BoletinDataEmpleador> lista = new ArrayList<BoletinDataEmpleador>();
			
			for ( Object item : result ) {
				Object[] datos = (Object[]) item;
				BoletinDataEmpleador fila = new BoletinDataEmpleador();
				
				fila.setTipoDeudaLaboral(datos[0].toString());
				fila.setGlosaTipoAcreedor(datos[1].toString());
				fila.setNombreInstitucion(datos[2].toString().trim());
				fila.setNroBolLaboral((Integer) datos[3]);
				fila.setPagBolLaboral((Integer) datos[4]);
				fila.setFecBolLaboral(String.valueOf(datos[5]));
				fila.setMotivoInfraccion(datos[6].toString());
				fila.setRut(datos[7].toString());
				fila.setNombre(datos[8].toString());
				fila.setPeriodo((Integer) datos[9]);
				fila.setMontoLaboral((BigDecimal) datos[10]);
				fila.setMontoLaboralUTM((BigDecimal) datos[11]);
				fila.setCodRegionInspeccion((Integer) datos[12]);
				fila.setAnoResolMulta((Integer) datos[13]);
				fila.setNroResolMulta((Integer) datos[14]);
				fila.setTipoMulta(datos[15].toString());
				fila.setMeses((Integer) datos[16]);
				fila.setNroCotizaciones((Integer) datos[17]);
				fila.setMontoAdeudado((BigDecimal) datos[18]);
				
				lista.add(fila);
			}
			
			obj.setLista(lista);
			
			obj.setFecUltimaMulta(String.valueOf(spq.getOutputParameterValue(5)));
			obj.setFecUltimaDeuda(String.valueOf(spq.getOutputParameterValue(6)));
			obj.setNroMultasLaboral((Integer) spq.getOutputParameterValue(7));
			obj.setNroDeudasLaboral((Integer) spq.getOutputParameterValue(8));
			obj.setMontoMultaLaboral((BigDecimal) spq.getOutputParameterValue(9));
			obj.setMontoDeudaLaboral((BigDecimal) spq.getOutputParameterValue(10));
			obj.setNroAcreedores((Integer) spq.getOutputParameterValue(11));
			obj.setNroTrabajadores((Integer) spq.getOutputParameterValue(12));
			obj.setNombreInfractor(spq.getOutputParameterValue(13).toString());
			obj.setCodAutenticBic(spq.getOutputParameterValue(14).toString());
			
			obj.setIdControl((Integer) spq.getOutputParameterValue(15));
			obj.setMsgControl((spq.getOutputParameterValue(16) != null) ? spq.getOutputParameterValue(16).toString() : new String());
			
		} catch (Exception e) {
			LOG.error("Error en la ejecucion del SP SQ_BolLabResumen_SW2 " + e.getMessage());
			obj.setMsgControl("ERR");
		}
		
		return obj;
	}

}
