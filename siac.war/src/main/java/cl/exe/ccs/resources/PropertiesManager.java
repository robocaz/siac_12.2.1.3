package cl.exe.ccs.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
@Startup
public class PropertiesManager implements ServletContextListener  {
	private static final Logger logger = LogManager.getLogger(PropertiesManager.class);

	private static String filename;

	private Properties propsMessages = new Properties();
	private Properties propsConstants = new Properties();

	public PropertiesManager()  {
		super();
		logger.info("PropertiesManager:-->Inicio----------------");

		InputStream inputConst = null;
		InputStream inputMsg = null;
		try {
			
		    Context ctx = new InitialContext();
		    Context env = (Context) ctx.lookup("java:comp/env");
		    final String fileName = (String) env.lookup("properties-file");
		    //System.out.println(fileName);
			
			inputConst = getClass().getClassLoader().getResourceAsStream("/resources/"+fileName+".properties");
			propsConstants.load(inputConst);

			inputMsg = getClass().getClassLoader().getResourceAsStream("/resources/messages.properties");
			propsMessages.load(inputMsg);

		} catch (IOException e) {
			e.printStackTrace();
		}
		catch (NamingException e) {
			e.printStackTrace();
		}


		logger.info("PropertiesManager:-->Fin");
	}

	public Properties getPropertiesMessages() {
		return propsMessages;
	}

	public Properties getPropertiesConstants() {
		return propsConstants;
	}

	public static void setFilename(String initFile) {
		filename = initFile;
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}
}
