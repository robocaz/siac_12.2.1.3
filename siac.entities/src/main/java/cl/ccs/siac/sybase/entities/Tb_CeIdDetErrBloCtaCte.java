package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tb_CeIdDetErrBloCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetErrBloCtaCte.findAll", query="SELECT t FROM Tb_CeIdDetErrBloCtaCte t")
public class Tb_CeIdDetErrBloCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	//bi-directional many-to-one association to Tb_CeIdSegtoBloqueoCtaCte
	@ManyToOne
	@JoinColumn(name="Fld_CorrBloqueoCta")
	private Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte;

	public Tb_CeIdDetErrBloCtaCte() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public Tb_CeIdSegtoBloqueoCtaCte getTbCeIdSegtoBloqueoCtaCte() {
		return this.tbCeIdSegtoBloqueoCtaCte;
	}

	public void setTbCeIdSegtoBloqueoCtaCte(Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte) {
		this.tbCeIdSegtoBloqueoCtaCte = tbCeIdSegtoBloqueoCtaCte;
	}

}