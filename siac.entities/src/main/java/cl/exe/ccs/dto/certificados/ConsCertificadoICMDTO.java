/**
 * 
 */
package cl.exe.ccs.dto.certificados;

import java.io.Serializable;
import java.util.List;

/**
 * @author emiranda
 *
 */
public class ConsCertificadoICMDTO implements Serializable {

	private static final long serialVersionUID = 8477919542594438837L;

	private String codUsuario;
	private String sucursal;
	private String rutAfectado;
	private Integer tipoConsulta;
	private Long corrMovimiento;
	private String appPat;
	private String appMat;
	private String nombres;
	private Boolean tipoNombre;
	private int numeroAcls;
	private String fechaPubAcls;
	private String fechaPubAcls2;
	private String codAutenticBic;
	private Integer filas;
	private int resultado;
	private String mensaje;
	private List<CertificadoICMDTO> certICMResult;

	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}

	/**
	 * @param codUsuario
	 *            the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the rutAfectado
	 */
	public String getRutAfectado() {
		return rutAfectado;
	}

	/**
	 * @param rutAfectado
	 *            the rutAfectado to set
	 */
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}

	/**
	 * @return the tipoConsulta
	 */
	public Integer getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipoConsulta
	 *            the tipoConsulta to set
	 */
	public void setTipoConsulta(Integer tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	/**
	 * @return the corrMovimiento
	 */
	public Long getCorrMovimiento() {
		return corrMovimiento;
	}

	/**
	 * @param corrMovimiento
	 *            the corrMovimiento to set
	 */
	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}

	/**
	 * @return the appPat
	 */
	public String getAppPat() {
		return appPat;
	}

	/**
	 * @param appPat
	 *            the appPat to set
	 */
	public void setAppPat(String appPat) {
		this.appPat = appPat;
	}

	/**
	 * @return the appMat
	 */
	public String getAppMat() {
		return appMat;
	}

	/**
	 * @param appMat
	 *            the appMat to set
	 */
	public void setAppMat(String appMat) {
		this.appMat = appMat;
	}

	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}

	/**
	 * @param nombres
	 *            the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
	 * @return the tipoNombre
	 */
	public Boolean getTipoNombre() {
		return tipoNombre;
	}

	/**
	 * @param tipoNombre
	 *            the tipoNombre to set
	 */
	public void setTipoNombre(Boolean tipoNombre) {
		this.tipoNombre = tipoNombre;
	}

	/**
	 * @return the numeroAcls
	 */
	public int getNumeroAcls() {
		return numeroAcls;
	}

	/**
	 * @param numeroAcls
	 *            the numeroBoletin to set
	 */
	public void setNumeroAcls(int numeroAcls) {
		this.numeroAcls = numeroAcls;
	}

	/**
	 * @return the fechaPubAcls
	 */
	public String getFechaPubAcls() {
		return fechaPubAcls;
	}

	/**
	 * @param fechaPubAcls
	 *            the fechaPubAcls to set
	 */
	public void setFechaPubAcls(String fechaPubAcls) {
		this.fechaPubAcls = fechaPubAcls;
	}

	/**
	 * @return the fechaPubAcls2
	 */
	public String getFechaPubAcls2() {
		return fechaPubAcls2;
	}

	/**
	 * @param fechaPubAcls2
	 *            the fechaPubAcls2 to set
	 */
	public void setFechaPubAcls2(String fechaPubAcls2) {
		this.fechaPubAcls2 = fechaPubAcls2;
	}

	/**
	 * @return the codAutenticBic
	 */
	public String getCodAutenticBic() {
		return codAutenticBic;
	}

	/**
	 * @param codAutenticBic
	 *            the codAutenticBic to set
	 */
	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}

	/**
	 * @return the filas
	 */
	public Integer getFilas() {
		return filas;
	}

	/**
	 * @param filas
	 *            the filas to set
	 */
	public void setFilas(Integer filas) {
		this.filas = filas;
	}

	/**
	 * @return the resultado
	 */
	public int getResultado() {
		return resultado;
	}

	/**
	 * @param resultado
	 *            the resultado to set
	 */
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje
	 *            the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the certICMResult
	 */
	public List<CertificadoICMDTO> getCertICMResult() {
		return certICMResult;
	}

	/**
	 * @param certICMResult
	 *            the certICMResult to set
	 */
	public void setCertICMResult(List<CertificadoICMDTO> certICMResult) {
		this.certICMResult = certICMResult;
	}

}
