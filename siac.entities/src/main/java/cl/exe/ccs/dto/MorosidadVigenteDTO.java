package cl.exe.ccs.dto;

import java.io.Serializable;

public class MorosidadVigenteDTO implements Serializable {
    
    private String tipoCredito;
    private String moneda;
    private Float montoDeuda;
    private String fechaVcto;
    private String nombreEmisor;
    
    
	public String getTipoCredito() {
		return tipoCredito;
	}
	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public Float getMontoDeuda() {
		return montoDeuda;
	}
	public void setMontoDeuda(Float montoDeuda) {
		this.montoDeuda = montoDeuda;
	}
	public String getFechaVcto() {
		return fechaVcto;
	}
	public void setFechaVcto(String fechaVcto) {
		this.fechaVcto = fechaVcto;
	}
	public String getNombreEmisor() {
		return nombreEmisor;
	}
	public void setNombreEmisor(String nombreEmisor) {
		this.nombreEmisor = nombreEmisor;
	}
    
    

}
