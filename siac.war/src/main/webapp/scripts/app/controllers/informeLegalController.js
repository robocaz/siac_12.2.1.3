angular.module('ccs').controller('InformeLegalController', function($scope, TransaccionResource ,PersonaResource, TramitanteResource ,$location,RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.afectado = {};



	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;

	$scope.getTramitante = function(){
		console.log("getTramitante");
		console.log($scope.afectado.rut);
		$scope.limpiarAfectado();
	};

	
	
	
	$rootScope.$on('eventoLegal', function(event) {
		console.log("eventoLegal");
		var successCallback = function(data,responseHeaders){
			console.log("successCallback verificar");
			console.log(data);
			console.log(data.idControl);
			if(data.idControl==0){
				$scope.afectado.rut 			= $rootScope.tramitante.rut;
				$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
				$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
				$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
				$scope.esvalido = true;
				//$scope.buscaAfectado("tramitante");
				
				console.log($rootScope.tramitante.rut);
				console.log($rootScope.tramitante.apellidoPaterno);
				console.log($rootScope.tramitante.apellidoMaterno);
				console.log($rootScope.tramitante.nombres);
				
				console.log($scope.afectado.rut);
				
				console.log("evento legal");
				$('#informelegal').modal('show');				
			} else {
				swal('Alerta',data.msgControl,'error');
			}
		};
		console.log("PersonaResource.verificar");
        PersonaResource.verificar($rootScope.tramitante.rut, successCallback, $scope.errorCallback);
        
	});
	
	/*$scope.esRutValido = function(){

		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ){
			return true;
			console.log("RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo");
		}else{
			console.log("RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ELSE");
			return false;
		}
	};*/

	/*$scope.esRutJuridico = function (rut) {
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			//Es Juridico
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};*/

	$scope.cargaTramitante = function() {
		$scope.afectado.rut 			= $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		console.log("cargaTramitante");
		$scope.buscaAfectado("tramitante");

	}

	$scope.limpiarAfectado = function(){
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
        $scope.afectado.nombreafectado = null;
        //$scope.esrutjuridico = false;
        $scope.editable = false;
        console.log("limpiarAfectado");
	};

	$scope.buscaAfectado = function (tipoPersona) {
		console.log("buscaAfectado")
        $scope.limpiarAfectado();
		$scope.esvalido = false;

		/*if($scope.afectado.rut === undefined ){
			return;
		}
        if($scope.afectado.rut === null ){
        	console.log("$scope.afectado.rut === null");
			$scope.afectado.rut  = undefined;
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
  				function () {
					$("#rutAfectadoInforme").focus();
				});
			return;
		}*/

		/*if($scope.esRutValido()){
			console.log("$scope.esRutValido()");
			$scope.obtenerAfectadoTramitante();
		}else{
			console.log("$scope.esRutValido() ELSE");
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'});
			$scope.afectado.rut == null;
		}*/
	}

	$scope.obtenerAfectado = function(){
		console.log("obtenerAfectado");
		var successCallback = function(data,responseHeaders){
			console.log("successCallback ");
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
                $scope.editable = false;
				$scope.afectado.apellidoPaterno = data.apellidoPaterno;
				$scope.afectado.apellidoMaterno = data.apellidoMaterno;
				$scope.afectado.nombres 		= data.nombres;
				console.log("successCallback ELSE");
				$('#ap_pat_pag').focus();
			}
		};
		console.log("PersonaResource.obtenerPersona");
        PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
	};

/*
	$scope.obtenerAfectadoTramitante = function(){
		console.log("obtenerAfectadoTramitante");
		var successCallback = function(data,responseHeaders){
            $scope.esvalido = true;
			if(data.correlativo == "0"){
				console.log("successCallback data.correlativo == '0'");
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
				console.log("data.correlativo == '0' ELSE");
                $scope.editable = false;
				
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				
				$('#ap_pat_pag').focus();
			}
		};
		console.log("TramitanteResource.carga");
		console.log($scope.afectado.rut);
        TramitanteResource.carga({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
	};
*/

	$scope.validacionCertificado = function () {
      // Declaración de Uso
		console.log("validacionCertificado");
      /*if($rootScope.tramitante.declaracionUso){
    	  console.log("$rootScope.tramitante.declaracionUso");
        swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
        return;
      }*/
      
		$scope.$emit('logout');
        $scope.authSupervisor();
        
	};

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#informelegal')),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){
				  $scope.guardar();
			  }
		  });
	};


    $scope.guardar = function(){
    		// if($scope.editable){
    	console.log("guardar");
			$scope.guardarPersona();
			//	}else{
			$scope.guardarServicio();
			//	}
    }

	$scope.guardarServicio = function(){
		console.log("guardarServicio");
		$scope.transaccion = {
			codServicio : 'ILG',
			rutAfectado : $scope.afectado.rut,
			corrNombre: $scope.afectado.corrNombre
		}
		var successCallback = function(data,responseHeaders){
			$scope.listarServicioTramitante();
			$('#informelegal').modal('hide');

			console.log("successCallback $('#informelegal').modal('hide');");
			swal({
				  title: 'Operación Exitosa',
				  text: "Actualización realizada exitosamente",
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Continuar'
				}).then(function () {
					if(!$rootScope.tramitante.declaracionUso){
						swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
					}
				})





		};
		console.log("TransaccionResource.agregarservicio");
		console.log($scope.afectado.rut);
        TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
	}

	$scope.guardarPersona = function(){

		var successCallback = function(data,responseHeaders){
			console.log("guardarPersona");
			if(data.correlativo == "0"){
                swal('Alerta','No se ha actualizado el tramitante','error');
			} else{
				$scope.afectado.corrNombre = data.correlativo;
				$scope.guardarServicio();
			}
		};
		console.log("PersonaResource.actualizar");
        PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};

	$('#informelegal').on('show.bs.modal', function (e) {
        $timeout(function(){
        	
            //$scope.afectado.rut = undefined;
            //$scope.getTramitante();
            console.log($scope.afectado.rut);
			console.log("netre focus");
        });
    });
	$("#informelegal").on('shown.bs.modal', function () {
		console.log("hown.bs.modal");
		console.log($scope.afectado.rut);
		$("#rutAfectadoInforme").focus();
	});
});
