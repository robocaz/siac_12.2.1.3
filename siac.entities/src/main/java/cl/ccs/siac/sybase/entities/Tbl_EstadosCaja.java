package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EstadosCaja database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadosCaja.findAll", query="SELECT t FROM Tbl_EstadosCaja t")
public class Tbl_EstadosCaja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_EstCaja")
	private String fld_EstCaja;

	@Column(name="Fld_GlosaEstCaja")
	private String fld_GlosaEstCaja;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_EstadosCaja() {
	}

	public String getFld_EstCaja() {
		return this.fld_EstCaja;
	}

	public void setFld_EstCaja(String fld_EstCaja) {
		this.fld_EstCaja = fld_EstCaja;
	}

	public String getFld_GlosaEstCaja() {
		return this.fld_GlosaEstCaja;
	}

	public void setFld_GlosaEstCaja(String fld_GlosaEstCaja) {
		this.fld_GlosaEstCaja = fld_GlosaEstCaja;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}