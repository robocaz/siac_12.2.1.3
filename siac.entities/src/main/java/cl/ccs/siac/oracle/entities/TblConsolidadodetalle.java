package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONSOLIDADODETALLE database table.
 * 
 */
@Entity
@Table(name="TBL_CONSOLIDADODETALLE")
@NamedQuery(name="TblConsolidadodetalle.findAll", query="SELECT t FROM TblConsolidadodetalle t")
public class TblConsolidadodetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_DOCUMENTOS")
	private BigDecimal fldDocumentos;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA")
	private Date fldFecha;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAACTUALIZACION")
	private Date fldFechaactualizacion;

	@Column(name="FLD_MONTO")
	private BigDecimal fldMonto;

	@Column(name="FLD_RUTDEUDOR")
	private String fldRutdeudor;

	public TblConsolidadodetalle() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldDocumentos() {
		return this.fldDocumentos;
	}

	public void setFldDocumentos(BigDecimal fldDocumentos) {
		this.fldDocumentos = fldDocumentos;
	}

	public Date getFldFecha() {
		return this.fldFecha;
	}

	public void setFldFecha(Date fldFecha) {
		this.fldFecha = fldFecha;
	}

	public Date getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Date fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public BigDecimal getFldMonto() {
		return this.fldMonto;
	}

	public void setFldMonto(BigDecimal fldMonto) {
		this.fldMonto = fldMonto;
	}

	public String getFldRutdeudor() {
		return this.fldRutdeudor;
	}

	public void setFldRutdeudor(String fldRutdeudor) {
		this.fldRutdeudor = fldRutdeudor;
	}

}