package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdNacionalidad database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdNacionalidad.findAll", query="SELECT t FROM Tp_CeIdNacionalidad t")
public class Tp_CeIdNacionalidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodNacionalidad")
	private short fld_CodNacionalidad;

	@Column(name="Fld_GlosaNacionalidad")
	private String fld_GlosaNacionalidad;

	public Tp_CeIdNacionalidad() {
	}

	public short getFld_CodNacionalidad() {
		return this.fld_CodNacionalidad;
	}

	public void setFld_CodNacionalidad(short fld_CodNacionalidad) {
		this.fld_CodNacionalidad = fld_CodNacionalidad;
	}

	public String getFld_GlosaNacionalidad() {
		return this.fld_GlosaNacionalidad;
	}

	public void setFld_GlosaNacionalidad(String fld_GlosaNacionalidad) {
		this.fld_GlosaNacionalidad = fld_GlosaNacionalidad;
	}

}