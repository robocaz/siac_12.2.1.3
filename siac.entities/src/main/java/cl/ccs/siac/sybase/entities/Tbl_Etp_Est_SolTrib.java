package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Etp_Est_SolTrib database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Etp_Est_SolTrib.findAll", query="SELECT t FROM Tbl_Etp_Est_SolTrib t")
public class Tbl_Etp_Est_SolTrib implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaDestino")
	private String fld_CodAreaDestino;

	@Column(name="Fld_CodAreaOrigen")
	private String fld_CodAreaOrigen;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEtapa")
	private short fld_CodEtapa;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Etp_Est_SolTrib() {
	}

	public String getFld_CodAreaDestino() {
		return this.fld_CodAreaDestino;
	}

	public void setFld_CodAreaDestino(String fld_CodAreaDestino) {
		this.fld_CodAreaDestino = fld_CodAreaDestino;
	}

	public String getFld_CodAreaOrigen() {
		return this.fld_CodAreaOrigen;
	}

	public void setFld_CodAreaOrigen(String fld_CodAreaOrigen) {
		this.fld_CodAreaOrigen = fld_CodAreaOrigen;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEtapa() {
		return this.fld_CodEtapa;
	}

	public void setFld_CodEtapa(short fld_CodEtapa) {
		this.fld_CodEtapa = fld_CodEtapa;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}