package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_LOGCONSULTAWS database table.
 * 
 */
@Entity
@Table(name="TBL_LOGCONSULTAWS")
@NamedQuery(name="TblLogconsultaw.findAll", query="SELECT t FROM TblLogconsultaw t")
public class TblLogconsultaw implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODMOTIVO")
	private BigDecimal fldCodmotivo;

	@Column(name="FLD_CODSISTEMA")
	private BigDecimal fldCodsistema;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_CORRCONSULTA_ID")
	private BigDecimal fldCorrconsultaId;

	@Column(name="FLD_FECHA")
	private Timestamp fldFecha;

	@Column(name="FLD_NROREGISTROS")
	private BigDecimal fldNroregistros;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_RUTSOLICITANTE")
	private String fldRutsolicitante;

	@Column(name="FLD_RUTUSUARIO")
	private String fldRutusuario;

	@Column(name="FLD_WEBSERVICE")
	private String fldWebservice;

	public TblLogconsultaw() {
	}

	public BigDecimal getFldCodmotivo() {
		return this.fldCodmotivo;
	}

	public void setFldCodmotivo(BigDecimal fldCodmotivo) {
		this.fldCodmotivo = fldCodmotivo;
	}

	public BigDecimal getFldCodsistema() {
		return this.fldCodsistema;
	}

	public void setFldCodsistema(BigDecimal fldCodsistema) {
		this.fldCodsistema = fldCodsistema;
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public BigDecimal getFldCorrconsultaId() {
		return this.fldCorrconsultaId;
	}

	public void setFldCorrconsultaId(BigDecimal fldCorrconsultaId) {
		this.fldCorrconsultaId = fldCorrconsultaId;
	}

	public Timestamp getFldFecha() {
		return this.fldFecha;
	}

	public void setFldFecha(Timestamp fldFecha) {
		this.fldFecha = fldFecha;
	}

	public BigDecimal getFldNroregistros() {
		return this.fldNroregistros;
	}

	public void setFldNroregistros(BigDecimal fldNroregistros) {
		this.fldNroregistros = fldNroregistros;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldRutsolicitante() {
		return this.fldRutsolicitante;
	}

	public void setFldRutsolicitante(String fldRutsolicitante) {
		this.fldRutsolicitante = fldRutsolicitante;
	}

	public String getFldRutusuario() {
		return this.fldRutusuario;
	}

	public void setFldRutusuario(String fldRutusuario) {
		this.fldRutusuario = fldRutusuario;
	}

	public String getFldWebservice() {
		return this.fldWebservice;
	}

	public void setFldWebservice(String fldWebservice) {
		this.fldWebservice = fldWebservice;
	}

}