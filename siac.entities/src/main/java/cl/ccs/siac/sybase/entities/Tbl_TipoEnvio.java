package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoEnvio database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoEnvio.findAll", query="SELECT t FROM Tbl_TipoEnvio t")
public class Tbl_TipoEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaEnvio")
	private String fld_GlosaEnvio;

	@Column(name="Fld_NroDias")
	private byte fld_NroDias;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tbl_TipoEnvio() {
	}

	public String getFld_GlosaEnvio() {
		return this.fld_GlosaEnvio;
	}

	public void setFld_GlosaEnvio(String fld_GlosaEnvio) {
		this.fld_GlosaEnvio = fld_GlosaEnvio;
	}

	public byte getFld_NroDias() {
		return this.fld_NroDias;
	}

	public void setFld_NroDias(byte fld_NroDias) {
		this.fld_NroDias = fld_NroDias;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}