package cl.ccs.siac.mediopago;

import java.util.List;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.MedioPagoDTO;

public interface MediosPagoBusiness {

	public List<ComboDTO> obtieneMediosPagos();
	
	public MedioPagoDTO obtenerBoletas(MedioPagoDTO medioPagoDTO);
	
	public MedioPagoDTO actualizaMedioDePago(MedioPagoDTO medioPagoDTO);
}
