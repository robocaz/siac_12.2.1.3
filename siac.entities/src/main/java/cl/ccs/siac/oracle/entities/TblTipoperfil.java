package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_TIPOPERFIL database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOPERFIL")
@NamedQuery(name="TblTipoperfil.findAll", query="SELECT t FROM TblTipoperfil t")
public class TblTipoperfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOPERFIL_FLDTIPOPERFIL_GENERATOR", sequenceName="FLD_TIPOPERFIL")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOPERFIL_FLDTIPOPERFIL_GENERATOR")
	@Column(name="FLD_TIPOPERFIL")
	private String fldTipoperfil;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATIPOPERFIL")
	private String fldGlosatipoperfil;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblPerfil
	@OneToMany(mappedBy="tblTipoperfil")
	private List<TblPerfil> tblPerfils;

	public TblTipoperfil() {
	}

	public String getFldTipoperfil() {
		return this.fldTipoperfil;
	}

	public void setFldTipoperfil(String fldTipoperfil) {
		this.fldTipoperfil = fldTipoperfil;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatipoperfil() {
		return this.fldGlosatipoperfil;
	}

	public void setFldGlosatipoperfil(String fldGlosatipoperfil) {
		this.fldGlosatipoperfil = fldGlosatipoperfil;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblPerfil> getTblPerfils() {
		return this.tblPerfils;
	}

	public void setTblPerfils(List<TblPerfil> tblPerfils) {
		this.tblPerfils = tblPerfils;
	}

	public TblPerfil addTblPerfil(TblPerfil tblPerfil) {
		getTblPerfils().add(tblPerfil);
		tblPerfil.setTblTipoperfil(this);

		return tblPerfil;
	}

	public TblPerfil removeTblPerfil(TblPerfil tblPerfil) {
		getTblPerfils().remove(tblPerfil);
		tblPerfil.setTblTipoperfil(null);

		return tblPerfil;
	}

}