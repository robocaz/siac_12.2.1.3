package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tp_CeIdDiarios database table.
 * 
 */
@Embeddable
public class Tp_CeIdDiarioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodDiario")
	private byte fld_CodDiario;

	@Column(name="Fld_CodDiarioReal")
	private byte fld_CodDiarioReal;

	public Tp_CeIdDiarioPK() {
	}
	public byte getFld_CodDiario() {
		return this.fld_CodDiario;
	}
	public void setFld_CodDiario(byte fld_CodDiario) {
		this.fld_CodDiario = fld_CodDiario;
	}
	public byte getFld_CodDiarioReal() {
		return this.fld_CodDiarioReal;
	}
	public void setFld_CodDiarioReal(byte fld_CodDiarioReal) {
		this.fld_CodDiarioReal = fld_CodDiarioReal;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tp_CeIdDiarioPK)) {
			return false;
		}
		Tp_CeIdDiarioPK castOther = (Tp_CeIdDiarioPK)other;
		return 
			(this.fld_CodDiario == castOther.fld_CodDiario)
			&& (this.fld_CodDiarioReal == castOther.fld_CodDiarioReal);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) this.fld_CodDiario);
		hash = hash * prime + ((int) this.fld_CodDiarioReal);
		
		return hash;
	}
}