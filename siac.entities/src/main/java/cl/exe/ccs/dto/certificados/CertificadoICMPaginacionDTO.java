package cl.exe.ccs.dto.certificados;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CertificadoICMPaginacionDTO implements Serializable {

	private static final long serialVersionUID = 2285479121703512410L;
	private Integer pagina;
	private int bc_inicio;
	private int bc_termino;
	private List<CertificadoICMDTO> bc_coleccion;

	public CertificadoICMPaginacionDTO() {
		this.pagina = 0;
		this.bc_coleccion = new ArrayList<CertificadoICMDTO>();
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public int getBc_inicio() {
		return bc_inicio;
	}

	public void setBc_inicio(int bc_inicio) {
		this.bc_inicio = bc_inicio;
	}

	public int getBc_termino() {
		return bc_termino;
	}

	public void setBc_termino(int bc_termino) {
		this.bc_termino = bc_termino;
	}

	public List<CertificadoICMDTO> getBc_coleccion() {
		return bc_coleccion;
	}

	public void setBc_coleccion(List<CertificadoICMDTO> bc_coleccion) {
		this.bc_coleccion = bc_coleccion;
	}

	@Override
	public String toString() {
		return "CertificadoICTPaginacionDTO{" + "pagina=" + pagina + ", bc_inicio=" + bc_inicio + ", bc_termino="
				+ bc_termino + ", bc_coleccion=" + bc_coleccion.size() + '}';
	}

}
