package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BolLabDetInfractores database table.
 * 
 */
@Entity
@Table(name="Tbl_BolLabDetInfractores")
@NamedQuery(name="Tbl_BolLabDetInfractore.findAll", query="SELECT t FROM Tbl_BolLabDetInfractore t")
public class Tbl_BolLabDetInfractore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_AnoResolMulta")
	private short fld_AnoResolMulta;

	@Column(name="Fld_CodAcreedor")
	private String fld_CodAcreedor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodInfraccion")
	private String fld_CodInfraccion;

	@Column(name="Fld_CodRegionInspeccion")
	private short fld_CodRegionInspeccion;

	@Column(name="Fld_CorrDeudaLaboral")
	private int fld_CorrDeudaLaboral;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecBolLaboral")
	private Timestamp fld_FecBolLaboral;

	@Column(name="Fld_MontoDeudaLaboral")
	private BigDecimal fld_MontoDeudaLaboral;

	@Column(name="Fld_NroBolLaboral")
	private short fld_NroBolLaboral;

	@Column(name="Fld_NroCotizDeuda")
	private int fld_NroCotizDeuda;

	@Column(name="Fld_NroMesesDeuda")
	private short fld_NroMesesDeuda;

	@Column(name="Fld_NroResolMulta")
	private int fld_NroResolMulta;

	@Column(name="Fld_PagBolLaboral")
	private int fld_PagBolLaboral;

	@Column(name="Fld_RutInfractor")
	private String fld_RutInfractor;

	@Column(name="Fld_TipoDeudaLaboral")
	private String fld_TipoDeudaLaboral;

	public Tbl_BolLabDetInfractore() {
	}

	public short getFld_AnoResolMulta() {
		return this.fld_AnoResolMulta;
	}

	public void setFld_AnoResolMulta(short fld_AnoResolMulta) {
		this.fld_AnoResolMulta = fld_AnoResolMulta;
	}

	public String getFld_CodAcreedor() {
		return this.fld_CodAcreedor;
	}

	public void setFld_CodAcreedor(String fld_CodAcreedor) {
		this.fld_CodAcreedor = fld_CodAcreedor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodInfraccion() {
		return this.fld_CodInfraccion;
	}

	public void setFld_CodInfraccion(String fld_CodInfraccion) {
		this.fld_CodInfraccion = fld_CodInfraccion;
	}

	public short getFld_CodRegionInspeccion() {
		return this.fld_CodRegionInspeccion;
	}

	public void setFld_CodRegionInspeccion(short fld_CodRegionInspeccion) {
		this.fld_CodRegionInspeccion = fld_CodRegionInspeccion;
	}

	public int getFld_CorrDeudaLaboral() {
		return this.fld_CorrDeudaLaboral;
	}

	public void setFld_CorrDeudaLaboral(int fld_CorrDeudaLaboral) {
		this.fld_CorrDeudaLaboral = fld_CorrDeudaLaboral;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecBolLaboral() {
		return this.fld_FecBolLaboral;
	}

	public void setFld_FecBolLaboral(Timestamp fld_FecBolLaboral) {
		this.fld_FecBolLaboral = fld_FecBolLaboral;
	}

	public BigDecimal getFld_MontoDeudaLaboral() {
		return this.fld_MontoDeudaLaboral;
	}

	public void setFld_MontoDeudaLaboral(BigDecimal fld_MontoDeudaLaboral) {
		this.fld_MontoDeudaLaboral = fld_MontoDeudaLaboral;
	}

	public short getFld_NroBolLaboral() {
		return this.fld_NroBolLaboral;
	}

	public void setFld_NroBolLaboral(short fld_NroBolLaboral) {
		this.fld_NroBolLaboral = fld_NroBolLaboral;
	}

	public int getFld_NroCotizDeuda() {
		return this.fld_NroCotizDeuda;
	}

	public void setFld_NroCotizDeuda(int fld_NroCotizDeuda) {
		this.fld_NroCotizDeuda = fld_NroCotizDeuda;
	}

	public short getFld_NroMesesDeuda() {
		return this.fld_NroMesesDeuda;
	}

	public void setFld_NroMesesDeuda(short fld_NroMesesDeuda) {
		this.fld_NroMesesDeuda = fld_NroMesesDeuda;
	}

	public int getFld_NroResolMulta() {
		return this.fld_NroResolMulta;
	}

	public void setFld_NroResolMulta(int fld_NroResolMulta) {
		this.fld_NroResolMulta = fld_NroResolMulta;
	}

	public int getFld_PagBolLaboral() {
		return this.fld_PagBolLaboral;
	}

	public void setFld_PagBolLaboral(int fld_PagBolLaboral) {
		this.fld_PagBolLaboral = fld_PagBolLaboral;
	}

	public String getFld_RutInfractor() {
		return this.fld_RutInfractor;
	}

	public void setFld_RutInfractor(String fld_RutInfractor) {
		this.fld_RutInfractor = fld_RutInfractor;
	}

	public String getFld_TipoDeudaLaboral() {
		return this.fld_TipoDeudaLaboral;
	}

	public void setFld_TipoDeudaLaboral(String fld_TipoDeudaLaboral) {
		this.fld_TipoDeudaLaboral = fld_TipoDeudaLaboral;
	}

}