package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Boletin database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Boletin.findAll", query="SELECT t FROM Tbl_Boletin t")
public class Tbl_Boletin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrGen_Id")
	private BigDecimal fld_CorrGen_Id;

	@Column(name="Fld_FecCumplimiento5Anos")
	private Timestamp fld_FecCumplimiento5Anos;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_FlagGeneracion")
	private byte fld_FlagGeneracion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Boletin() {
	}

	public BigDecimal getFld_CorrGen_Id() {
		return this.fld_CorrGen_Id;
	}

	public void setFld_CorrGen_Id(BigDecimal fld_CorrGen_Id) {
		this.fld_CorrGen_Id = fld_CorrGen_Id;
	}

	public Timestamp getFld_FecCumplimiento5Anos() {
		return this.fld_FecCumplimiento5Anos;
	}

	public void setFld_FecCumplimiento5Anos(Timestamp fld_FecCumplimiento5Anos) {
		this.fld_FecCumplimiento5Anos = fld_FecCumplimiento5Anos;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public byte getFld_FlagGeneracion() {
		return this.fld_FlagGeneracion;
	}

	public void setFld_FlagGeneracion(byte fld_FlagGeneracion) {
		this.fld_FlagGeneracion = fld_FlagGeneracion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}