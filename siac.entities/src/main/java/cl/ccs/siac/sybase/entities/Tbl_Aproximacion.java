package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Aproximacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Aproximacion.findAll", query="SELECT t FROM Tbl_Aproximacion t")
public class Tbl_Aproximacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_Flag_MMonto")
	private boolean fld_Flag_MMonto;

	@Column(name="Fld_Flag_MNroBolPub")
	private boolean fld_Flag_MNroBolPub;

	@Column(name="Fld_Flag_MNroOperacion")
	private boolean fld_Flag_MNroOperacion;

	@Column(name="Fld_Flag_MTipDoc")
	private boolean fld_Flag_MTipDoc;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_Librador_ApMat")
	private String fld_Librador_ApMat;

	@Column(name="Fld_Librador_ApPat")
	private String fld_Librador_ApPat;

	@Column(name="Fld_Librador_Nombres")
	private String fld_Librador_Nombres;

	@Column(name="Fld_Marca3Dig")
	private boolean fld_Marca3Dig;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MarcaAclEspecial")
	private boolean fld_MarcaAclEspecial;

	@Column(name="Fld_MarcaDigVerErroneo")
	private boolean fld_MarcaDigVerErroneo;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_PtjeEmisor")
	private BigDecimal fld_PtjeEmisor;

	@Column(name="Fld_PtjeFecProt")
	private BigDecimal fld_PtjeFecProt;

	@Column(name="Fld_PtjeMoneda")
	private BigDecimal fld_PtjeMoneda;

	@Column(name="Fld_PtjeMonto")
	private BigDecimal fld_PtjeMonto;

	@Column(name="Fld_PtjeNroOper")
	private BigDecimal fld_PtjeNroOper;

	@Column(name="Fld_PtjeTipoDoc")
	private BigDecimal fld_PtjeTipoDoc;

	@Column(name="Fld_PtjeTotal")
	private BigDecimal fld_PtjeTotal;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_Aproximacion() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public boolean getFld_Flag_MMonto() {
		return this.fld_Flag_MMonto;
	}

	public void setFld_Flag_MMonto(boolean fld_Flag_MMonto) {
		this.fld_Flag_MMonto = fld_Flag_MMonto;
	}

	public boolean getFld_Flag_MNroBolPub() {
		return this.fld_Flag_MNroBolPub;
	}

	public void setFld_Flag_MNroBolPub(boolean fld_Flag_MNroBolPub) {
		this.fld_Flag_MNroBolPub = fld_Flag_MNroBolPub;
	}

	public boolean getFld_Flag_MNroOperacion() {
		return this.fld_Flag_MNroOperacion;
	}

	public void setFld_Flag_MNroOperacion(boolean fld_Flag_MNroOperacion) {
		this.fld_Flag_MNroOperacion = fld_Flag_MNroOperacion;
	}

	public boolean getFld_Flag_MTipDoc() {
		return this.fld_Flag_MTipDoc;
	}

	public void setFld_Flag_MTipDoc(boolean fld_Flag_MTipDoc) {
		this.fld_Flag_MTipDoc = fld_Flag_MTipDoc;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public String getFld_Librador_ApMat() {
		return this.fld_Librador_ApMat;
	}

	public void setFld_Librador_ApMat(String fld_Librador_ApMat) {
		this.fld_Librador_ApMat = fld_Librador_ApMat;
	}

	public String getFld_Librador_ApPat() {
		return this.fld_Librador_ApPat;
	}

	public void setFld_Librador_ApPat(String fld_Librador_ApPat) {
		this.fld_Librador_ApPat = fld_Librador_ApPat;
	}

	public String getFld_Librador_Nombres() {
		return this.fld_Librador_Nombres;
	}

	public void setFld_Librador_Nombres(String fld_Librador_Nombres) {
		this.fld_Librador_Nombres = fld_Librador_Nombres;
	}

	public boolean getFld_Marca3Dig() {
		return this.fld_Marca3Dig;
	}

	public void setFld_Marca3Dig(boolean fld_Marca3Dig) {
		this.fld_Marca3Dig = fld_Marca3Dig;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public boolean getFld_MarcaAclEspecial() {
		return this.fld_MarcaAclEspecial;
	}

	public void setFld_MarcaAclEspecial(boolean fld_MarcaAclEspecial) {
		this.fld_MarcaAclEspecial = fld_MarcaAclEspecial;
	}

	public boolean getFld_MarcaDigVerErroneo() {
		return this.fld_MarcaDigVerErroneo;
	}

	public void setFld_MarcaDigVerErroneo(boolean fld_MarcaDigVerErroneo) {
		this.fld_MarcaDigVerErroneo = fld_MarcaDigVerErroneo;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public BigDecimal getFld_PtjeEmisor() {
		return this.fld_PtjeEmisor;
	}

	public void setFld_PtjeEmisor(BigDecimal fld_PtjeEmisor) {
		this.fld_PtjeEmisor = fld_PtjeEmisor;
	}

	public BigDecimal getFld_PtjeFecProt() {
		return this.fld_PtjeFecProt;
	}

	public void setFld_PtjeFecProt(BigDecimal fld_PtjeFecProt) {
		this.fld_PtjeFecProt = fld_PtjeFecProt;
	}

	public BigDecimal getFld_PtjeMoneda() {
		return this.fld_PtjeMoneda;
	}

	public void setFld_PtjeMoneda(BigDecimal fld_PtjeMoneda) {
		this.fld_PtjeMoneda = fld_PtjeMoneda;
	}

	public BigDecimal getFld_PtjeMonto() {
		return this.fld_PtjeMonto;
	}

	public void setFld_PtjeMonto(BigDecimal fld_PtjeMonto) {
		this.fld_PtjeMonto = fld_PtjeMonto;
	}

	public BigDecimal getFld_PtjeNroOper() {
		return this.fld_PtjeNroOper;
	}

	public void setFld_PtjeNroOper(BigDecimal fld_PtjeNroOper) {
		this.fld_PtjeNroOper = fld_PtjeNroOper;
	}

	public BigDecimal getFld_PtjeTipoDoc() {
		return this.fld_PtjeTipoDoc;
	}

	public void setFld_PtjeTipoDoc(BigDecimal fld_PtjeTipoDoc) {
		this.fld_PtjeTipoDoc = fld_PtjeTipoDoc;
	}

	public BigDecimal getFld_PtjeTotal() {
		return this.fld_PtjeTotal;
	}

	public void setFld_PtjeTotal(BigDecimal fld_PtjeTotal) {
		this.fld_PtjeTotal = fld_PtjeTotal;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}