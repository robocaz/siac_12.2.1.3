package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BeneficioCesantia database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BeneficioCesantia.findAll", query="SELECT t FROM Tbl_BeneficioCesantia t")
public class Tbl_BeneficioCesantia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutEmpleador")
	private String fld_RutEmpleador;

	public Tbl_BeneficioCesantia() {
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutEmpleador() {
		return this.fld_RutEmpleador;
	}

	public void setFld_RutEmpleador(String fld_RutEmpleador) {
		this.fld_RutEmpleador = fld_RutEmpleador;
	}

}