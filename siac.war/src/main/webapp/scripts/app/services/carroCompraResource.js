angular.module('ccs').factory('CarroCompraResource', function ($resource) {
    var resource = $resource('rest/carrocompra/',  { nocache : new Date().getTime()},
    {
        'cargaServicios' :  { method : 'GET' ,url: 'rest/carrocompra/cargaservicios', isArray: true},
        'eliminaServicios' :  { method : 'POST' ,url: 'rest/carrocompra/eliminaservicios', isArray: false}
    });
    return resource;
});