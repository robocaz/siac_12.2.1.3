package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_PRT_Descripcion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PRT_Descripcion.findAll", query="SELECT t FROM Tbl_PRT_Descripcion t")
public class Tbl_PRT_Descripcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodPta")
	private String fld_CodPta;

	@Column(name="Fld_Comuna")
	private String fld_Comuna;

	@Column(name="Fld_Concesionario")
	private String fld_Concesionario;

	@Column(name="Fld_Domicilio")
	private String fld_Domicilio;

	@Column(name="Fld_Region")
	private short fld_Region;

	@Column(name="Fld_Tipo")
	private String fld_Tipo;

	public Tbl_PRT_Descripcion() {
	}

	public String getFld_CodPta() {
		return this.fld_CodPta;
	}

	public void setFld_CodPta(String fld_CodPta) {
		this.fld_CodPta = fld_CodPta;
	}

	public String getFld_Comuna() {
		return this.fld_Comuna;
	}

	public void setFld_Comuna(String fld_Comuna) {
		this.fld_Comuna = fld_Comuna;
	}

	public String getFld_Concesionario() {
		return this.fld_Concesionario;
	}

	public void setFld_Concesionario(String fld_Concesionario) {
		this.fld_Concesionario = fld_Concesionario;
	}

	public String getFld_Domicilio() {
		return this.fld_Domicilio;
	}

	public void setFld_Domicilio(String fld_Domicilio) {
		this.fld_Domicilio = fld_Domicilio;
	}

	public short getFld_Region() {
		return this.fld_Region;
	}

	public void setFld_Region(short fld_Region) {
		this.fld_Region = fld_Region;
	}

	public String getFld_Tipo() {
		return this.fld_Tipo;
	}

	public void setFld_Tipo(String fld_Tipo) {
		this.fld_Tipo = fld_Tipo;
	}

}