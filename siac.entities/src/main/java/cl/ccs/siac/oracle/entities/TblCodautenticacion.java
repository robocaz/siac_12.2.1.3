package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_CODAUTENTICACION database table.
 * 
 */
@Entity
@Table(name="TBL_CODAUTENTICACION")
@NamedQuery(name="TblCodautenticacion.findAll", query="SELECT t FROM TblCodautenticacion t")
public class TblCodautenticacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_CODAUTENTICACION_FLDCORRAUTENTICACIONID_GENERATOR", sequenceName="FLD_CORRAUTENTICACION_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_CODAUTENTICACION_FLDCORRAUTENTICACIONID_GENERATOR")
	@Column(name="FLD_CORRAUTENTICACION_ID")
	private long fldCorrautenticacionId;

	@Column(name="FLD_CANTIDAD")
	private BigDecimal fldCantidad;

	@Column(name="FLD_CORRAUTENTICBIC")
	private BigDecimal fldCorrautenticbic;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA")
	private Date fldFecha;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_USUARIOSIAC")
	private String fldUsuariosiac;

	//bi-directional many-to-one association to TblDetautenticacion
	@OneToMany(mappedBy="tblCodautenticacion")
	private List<TblDetautenticacion> tblDetautenticacions;

	public TblCodautenticacion() {
	}

	public long getFldCorrautenticacionId() {
		return this.fldCorrautenticacionId;
	}

	public void setFldCorrautenticacionId(long fldCorrautenticacionId) {
		this.fldCorrautenticacionId = fldCorrautenticacionId;
	}

	public BigDecimal getFldCantidad() {
		return this.fldCantidad;
	}

	public void setFldCantidad(BigDecimal fldCantidad) {
		this.fldCantidad = fldCantidad;
	}

	public BigDecimal getFldCorrautenticbic() {
		return this.fldCorrautenticbic;
	}

	public void setFldCorrautenticbic(BigDecimal fldCorrautenticbic) {
		this.fldCorrautenticbic = fldCorrautenticbic;
	}

	public Date getFldFecha() {
		return this.fldFecha;
	}

	public void setFldFecha(Date fldFecha) {
		this.fldFecha = fldFecha;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldUsuariosiac() {
		return this.fldUsuariosiac;
	}

	public void setFldUsuariosiac(String fldUsuariosiac) {
		this.fldUsuariosiac = fldUsuariosiac;
	}

	public List<TblDetautenticacion> getTblDetautenticacions() {
		return this.tblDetautenticacions;
	}

	public void setTblDetautenticacions(List<TblDetautenticacion> tblDetautenticacions) {
		this.tblDetautenticacions = tblDetautenticacions;
	}

	public TblDetautenticacion addTblDetautenticacion(TblDetautenticacion tblDetautenticacion) {
		getTblDetautenticacions().add(tblDetautenticacion);
		tblDetautenticacion.setTblCodautenticacion(this);

		return tblDetautenticacion;
	}

	public TblDetautenticacion removeTblDetautenticacion(TblDetautenticacion tblDetautenticacion) {
		getTblDetautenticacions().remove(tblDetautenticacion);
		tblDetautenticacion.setTblCodautenticacion(null);

		return tblDetautenticacion;
	}

}