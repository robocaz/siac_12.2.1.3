package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the rs_threads database table.
 * 
 */
@Entity
@Table(name="rs_threads")
@NamedQuery(name="RsThread.findAll", query="SELECT r FROM RsThread r")
public class RsThread implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="conn_id")
	private int connId;

	private int id;

	private String pad1;

	private String pad2;

	private String pad3;

	private String pad4;

	private int seq;

	public RsThread() {
	}

	public int getConnId() {
		return this.connId;
	}

	public void setConnId(int connId) {
		this.connId = connId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPad1() {
		return this.pad1;
	}

	public void setPad1(String pad1) {
		this.pad1 = pad1;
	}

	public String getPad2() {
		return this.pad2;
	}

	public void setPad2(String pad2) {
		this.pad2 = pad2;
	}

	public String getPad3() {
		return this.pad3;
	}

	public void setPad3(String pad3) {
		this.pad3 = pad3;
	}

	public String getPad4() {
		return this.pad4;
	}

	public void setPad4(String pad4) {
		this.pad4 = pad4;
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

}