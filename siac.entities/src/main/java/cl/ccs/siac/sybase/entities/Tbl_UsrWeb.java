package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsrWeb database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsrWeb.findAll", query="SELECT t FROM Tbl_UsrWeb t")
public class Tbl_UsrWeb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Login")
	private String login;

	@Column(name="Mail")
	private String mail;

	@Column(name="Pass")
	private String pass;

	@Column(name="Perfil")
	private String perfil;

	@Column(name="UsrTimeStamp")
	private byte[] usrTimeStamp;

	public Tbl_UsrWeb() {
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPerfil() {
		return this.perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public byte[] getUsrTimeStamp() {
		return this.usrTimeStamp;
	}

	public void setUsrTimeStamp(byte[] usrTimeStamp) {
		this.usrTimeStamp = usrTimeStamp;
	}

}