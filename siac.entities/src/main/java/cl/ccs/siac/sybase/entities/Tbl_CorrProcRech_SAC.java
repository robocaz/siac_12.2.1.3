package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CorrProcRech_SAC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CorrProcRech_SAC.findAll", query="SELECT t FROM Tbl_CorrProcRech_SAC t")
public class Tbl_CorrProcRech_SAC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	public Tbl_CorrProcRech_SAC() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

}