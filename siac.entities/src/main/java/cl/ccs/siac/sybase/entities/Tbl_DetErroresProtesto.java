package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetErroresProtesto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetErroresProtesto.findAll", query="SELECT t FROM Tbl_DetErroresProtesto t")
public class Tbl_DetErroresProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Digitacion")
	private boolean fld_Digitacion;

	@Column(name="Fld_FolioProt")
	private int fld_FolioProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorCampo")
	private String fld_ValorCampo;

	@Column(name="Fld_Verificador")
	private boolean fld_Verificador;

	public Tbl_DetErroresProtesto() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public boolean getFld_Digitacion() {
		return this.fld_Digitacion;
	}

	public void setFld_Digitacion(boolean fld_Digitacion) {
		this.fld_Digitacion = fld_Digitacion;
	}

	public int getFld_FolioProt() {
		return this.fld_FolioProt;
	}

	public void setFld_FolioProt(int fld_FolioProt) {
		this.fld_FolioProt = fld_FolioProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_ValorCampo() {
		return this.fld_ValorCampo;
	}

	public void setFld_ValorCampo(String fld_ValorCampo) {
		this.fld_ValorCampo = fld_ValorCampo;
	}

	public boolean getFld_Verificador() {
		return this.fld_Verificador;
	}

	public void setFld_Verificador(boolean fld_Verificador) {
		this.fld_Verificador = fld_Verificador;
	}

}