package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EstadosSolicAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadosSolicAER.findAll", query="SELECT t FROM Tbl_EstadosSolicAER t")
public class Tbl_EstadosSolicAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaDestino")
	private String fld_CodAreaDestino;

	@Column(name="Fld_CodAreaOrigen")
	private String fld_CodAreaOrigen;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEtapa")
	private short fld_CodEtapa;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_EstadosSolicAER() {
	}

	public String getFld_CodAreaDestino() {
		return this.fld_CodAreaDestino;
	}

	public void setFld_CodAreaDestino(String fld_CodAreaDestino) {
		this.fld_CodAreaDestino = fld_CodAreaDestino;
	}

	public String getFld_CodAreaOrigen() {
		return this.fld_CodAreaOrigen;
	}

	public void setFld_CodAreaOrigen(String fld_CodAreaOrigen) {
		this.fld_CodAreaOrigen = fld_CodAreaOrigen;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEtapa() {
		return this.fld_CodEtapa;
	}

	public void setFld_CodEtapa(short fld_CodEtapa) {
		this.fld_CodEtapa = fld_CodEtapa;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}