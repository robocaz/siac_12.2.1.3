package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CajaPendienteDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = -4509983380053376581L;
	private String corrCaja;
	private String rutTramitante;

	public String getCorrCaja() {
		return corrCaja;
	}

	public void setCorrCaja(String corrCaja) {
		this.corrCaja = corrCaja;
	}

	public String getRutTramitante() {
		return rutTramitante;
	}

	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}

}
