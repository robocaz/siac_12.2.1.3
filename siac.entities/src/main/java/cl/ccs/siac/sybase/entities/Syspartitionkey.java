package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the syspartitionkeys database table.
 * 
 */
@Entity
@Table(name="syspartitionkeys")
@NamedQuery(name="Syspartitionkey.findAll", query="SELECT s FROM Syspartitionkey s")
public class Syspartitionkey implements Serializable {
	private static final long serialVersionUID = 1L;

	private short colid;

	private int id;

	private short indid;

	private short position;

	public Syspartitionkey() {
	}

	public short getColid() {
		return this.colid;
	}

	public void setColid(short colid) {
		this.colid = colid;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getIndid() {
		return this.indid;
	}

	public void setIndid(short indid) {
		this.indid = indid;
	}

	public short getPosition() {
		return this.position;
	}

	public void setPosition(short position) {
		this.position = position;
	}

}