package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_TIPOCREDITO database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOCREDITO")
@NamedQuery(name="TblTipocredito.findAll", query="SELECT t FROM TblTipocredito t")
public class TblTipocredito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOCREDITO_FLDTIPOCREDITO_GENERATOR", sequenceName="FLD_TIPOCREDITO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOCREDITO_FLDTIPOCREDITO_GENERATOR")
	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATIPOCREDITO")
	private String fldGlosatipocredito;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-many association to TblEmisor
	@ManyToMany
	@JoinTable(
		name="TBL_DOCEMI"
		, joinColumns={
			@JoinColumn(name="FLD_TIPOCREDITO")
			}
		, inverseJoinColumns={
			@JoinColumn(name="FLD_CODEMISOR", referencedColumnName="FLD_CODEMISOR"),
			@JoinColumn(name="FLD_TIPOEMISOR", referencedColumnName="FLD_TIPOEMISOR")
			}
		)
	private List<TblEmisor> tblEmisors;

	public TblTipocredito() {
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatipocredito() {
		return this.fldGlosatipocredito;
	}

	public void setFldGlosatipocredito(String fldGlosatipocredito) {
		this.fldGlosatipocredito = fldGlosatipocredito;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblEmisor> getTblEmisors() {
		return this.tblEmisors;
	}

	public void setTblEmisors(List<TblEmisor> tblEmisors) {
		this.tblEmisors = tblEmisors;
	}

}