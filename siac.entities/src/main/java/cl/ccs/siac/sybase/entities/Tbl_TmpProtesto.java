package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TmpProtestos database table.
 * 
 */
@Entity
@Table(name="Tbl_TmpProtestos")
@NamedQuery(name="Tbl_TmpProtesto.findAll", query="SELECT t FROM Tbl_TmpProtesto t")
public class Tbl_TmpProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalidadJuridica_Tmp")
	private String fld_CalidadJuridica_Tmp;

	@Column(name="Fld_CodCausalProt_Tmp")
	private String fld_CodCausalProt_Tmp;

	@Column(name="Fld_CodLocalidad_Tmp")
	private String fld_CodLocalidad_Tmp;

	@Column(name="Fld_CodMoneda_Tmp")
	private String fld_CodMoneda_Tmp;

	@Column(name="Fld_CodMonedaOrig_Tmp")
	private String fld_CodMonedaOrig_Tmp;

	@Column(name="Fld_CodSucursal_Tmp")
	private String fld_CodSucursal_Tmp;

	@Column(name="Fld_CorrCarga_Id")
	private BigDecimal fld_CorrCarga_Id;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_Fecha_Tmp")
	private String fld_Fecha_Tmp;

	@Column(name="Fld_FecVcto_Tmp")
	private String fld_FecVcto_Tmp;

	@Column(name="Fld_Filler")
	private String fld_Filler;

	@Column(name="Fld_GlosaLocalidad_Tmp")
	private String fld_GlosaLocalidad_Tmp;

	@Column(name="Fld_GlosaSucursal_Tmp")
	private String fld_GlosaSucursal_Tmp;

	@Column(name="Fld_Librador_ApMat")
	private String fld_Librador_ApMat;

	@Column(name="Fld_Librador_ApPat")
	private String fld_Librador_ApPat;

	@Column(name="Fld_Librador_Nombres")
	private String fld_Librador_Nombres;

	@Column(name="Fld_Monto_Tmp")
	private String fld_Monto_Tmp;

	@Column(name="Fld_MontoOrig_Tmp")
	private String fld_MontoOrig_Tmp;

	@Column(name="Fld_Nombre_ApMat_Tmp")
	private String fld_Nombre_ApMat_Tmp;

	@Column(name="Fld_Nombre_ApPat_Tmp")
	private String fld_Nombre_ApPat_Tmp;

	@Column(name="Fld_Nombre_Nombres_Tmp")
	private String fld_Nombre_Nombres_Tmp;

	@Column(name="Fld_NroCheque_Tmp")
	private String fld_NroCheque_Tmp;

	@Column(name="Fld_NroCorrelativo_Tmp")
	private String fld_NroCorrelativo_Tmp;

	@Column(name="Fld_Rut_Tmp")
	private String fld_Rut_Tmp;

	@Column(name="Fld_TipoDocumento_Tmp")
	private String fld_TipoDocumento_Tmp;

	@Column(name="Fld_TipoRegistro_Tmp")
	private String fld_TipoRegistro_Tmp;

	public Tbl_TmpProtesto() {
	}

	public String getFld_CalidadJuridica_Tmp() {
		return this.fld_CalidadJuridica_Tmp;
	}

	public void setFld_CalidadJuridica_Tmp(String fld_CalidadJuridica_Tmp) {
		this.fld_CalidadJuridica_Tmp = fld_CalidadJuridica_Tmp;
	}

	public String getFld_CodCausalProt_Tmp() {
		return this.fld_CodCausalProt_Tmp;
	}

	public void setFld_CodCausalProt_Tmp(String fld_CodCausalProt_Tmp) {
		this.fld_CodCausalProt_Tmp = fld_CodCausalProt_Tmp;
	}

	public String getFld_CodLocalidad_Tmp() {
		return this.fld_CodLocalidad_Tmp;
	}

	public void setFld_CodLocalidad_Tmp(String fld_CodLocalidad_Tmp) {
		this.fld_CodLocalidad_Tmp = fld_CodLocalidad_Tmp;
	}

	public String getFld_CodMoneda_Tmp() {
		return this.fld_CodMoneda_Tmp;
	}

	public void setFld_CodMoneda_Tmp(String fld_CodMoneda_Tmp) {
		this.fld_CodMoneda_Tmp = fld_CodMoneda_Tmp;
	}

	public String getFld_CodMonedaOrig_Tmp() {
		return this.fld_CodMonedaOrig_Tmp;
	}

	public void setFld_CodMonedaOrig_Tmp(String fld_CodMonedaOrig_Tmp) {
		this.fld_CodMonedaOrig_Tmp = fld_CodMonedaOrig_Tmp;
	}

	public String getFld_CodSucursal_Tmp() {
		return this.fld_CodSucursal_Tmp;
	}

	public void setFld_CodSucursal_Tmp(String fld_CodSucursal_Tmp) {
		this.fld_CodSucursal_Tmp = fld_CodSucursal_Tmp;
	}

	public BigDecimal getFld_CorrCarga_Id() {
		return this.fld_CorrCarga_Id;
	}

	public void setFld_CorrCarga_Id(BigDecimal fld_CorrCarga_Id) {
		this.fld_CorrCarga_Id = fld_CorrCarga_Id;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public String getFld_Fecha_Tmp() {
		return this.fld_Fecha_Tmp;
	}

	public void setFld_Fecha_Tmp(String fld_Fecha_Tmp) {
		this.fld_Fecha_Tmp = fld_Fecha_Tmp;
	}

	public String getFld_FecVcto_Tmp() {
		return this.fld_FecVcto_Tmp;
	}

	public void setFld_FecVcto_Tmp(String fld_FecVcto_Tmp) {
		this.fld_FecVcto_Tmp = fld_FecVcto_Tmp;
	}

	public String getFld_Filler() {
		return this.fld_Filler;
	}

	public void setFld_Filler(String fld_Filler) {
		this.fld_Filler = fld_Filler;
	}

	public String getFld_GlosaLocalidad_Tmp() {
		return this.fld_GlosaLocalidad_Tmp;
	}

	public void setFld_GlosaLocalidad_Tmp(String fld_GlosaLocalidad_Tmp) {
		this.fld_GlosaLocalidad_Tmp = fld_GlosaLocalidad_Tmp;
	}

	public String getFld_GlosaSucursal_Tmp() {
		return this.fld_GlosaSucursal_Tmp;
	}

	public void setFld_GlosaSucursal_Tmp(String fld_GlosaSucursal_Tmp) {
		this.fld_GlosaSucursal_Tmp = fld_GlosaSucursal_Tmp;
	}

	public String getFld_Librador_ApMat() {
		return this.fld_Librador_ApMat;
	}

	public void setFld_Librador_ApMat(String fld_Librador_ApMat) {
		this.fld_Librador_ApMat = fld_Librador_ApMat;
	}

	public String getFld_Librador_ApPat() {
		return this.fld_Librador_ApPat;
	}

	public void setFld_Librador_ApPat(String fld_Librador_ApPat) {
		this.fld_Librador_ApPat = fld_Librador_ApPat;
	}

	public String getFld_Librador_Nombres() {
		return this.fld_Librador_Nombres;
	}

	public void setFld_Librador_Nombres(String fld_Librador_Nombres) {
		this.fld_Librador_Nombres = fld_Librador_Nombres;
	}

	public String getFld_Monto_Tmp() {
		return this.fld_Monto_Tmp;
	}

	public void setFld_Monto_Tmp(String fld_Monto_Tmp) {
		this.fld_Monto_Tmp = fld_Monto_Tmp;
	}

	public String getFld_MontoOrig_Tmp() {
		return this.fld_MontoOrig_Tmp;
	}

	public void setFld_MontoOrig_Tmp(String fld_MontoOrig_Tmp) {
		this.fld_MontoOrig_Tmp = fld_MontoOrig_Tmp;
	}

	public String getFld_Nombre_ApMat_Tmp() {
		return this.fld_Nombre_ApMat_Tmp;
	}

	public void setFld_Nombre_ApMat_Tmp(String fld_Nombre_ApMat_Tmp) {
		this.fld_Nombre_ApMat_Tmp = fld_Nombre_ApMat_Tmp;
	}

	public String getFld_Nombre_ApPat_Tmp() {
		return this.fld_Nombre_ApPat_Tmp;
	}

	public void setFld_Nombre_ApPat_Tmp(String fld_Nombre_ApPat_Tmp) {
		this.fld_Nombre_ApPat_Tmp = fld_Nombre_ApPat_Tmp;
	}

	public String getFld_Nombre_Nombres_Tmp() {
		return this.fld_Nombre_Nombres_Tmp;
	}

	public void setFld_Nombre_Nombres_Tmp(String fld_Nombre_Nombres_Tmp) {
		this.fld_Nombre_Nombres_Tmp = fld_Nombre_Nombres_Tmp;
	}

	public String getFld_NroCheque_Tmp() {
		return this.fld_NroCheque_Tmp;
	}

	public void setFld_NroCheque_Tmp(String fld_NroCheque_Tmp) {
		this.fld_NroCheque_Tmp = fld_NroCheque_Tmp;
	}

	public String getFld_NroCorrelativo_Tmp() {
		return this.fld_NroCorrelativo_Tmp;
	}

	public void setFld_NroCorrelativo_Tmp(String fld_NroCorrelativo_Tmp) {
		this.fld_NroCorrelativo_Tmp = fld_NroCorrelativo_Tmp;
	}

	public String getFld_Rut_Tmp() {
		return this.fld_Rut_Tmp;
	}

	public void setFld_Rut_Tmp(String fld_Rut_Tmp) {
		this.fld_Rut_Tmp = fld_Rut_Tmp;
	}

	public String getFld_TipoDocumento_Tmp() {
		return this.fld_TipoDocumento_Tmp;
	}

	public void setFld_TipoDocumento_Tmp(String fld_TipoDocumento_Tmp) {
		this.fld_TipoDocumento_Tmp = fld_TipoDocumento_Tmp;
	}

	public String getFld_TipoRegistro_Tmp() {
		return this.fld_TipoRegistro_Tmp;
	}

	public void setFld_TipoRegistro_Tmp(String fld_TipoRegistro_Tmp) {
		this.fld_TipoRegistro_Tmp = fld_TipoRegistro_Tmp;
	}

}