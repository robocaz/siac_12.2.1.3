package cl.ccs.siac.usuario;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;


public interface UsuarioBusiness {

	
	UsuarioDTO obtenerUsuario(String username);
	
	UsuarioDTO esSupervisor (String usuario, String password);
	
	ResponseDTO cambiarClave (String usuario, String password, String passwordAux);
	
}
