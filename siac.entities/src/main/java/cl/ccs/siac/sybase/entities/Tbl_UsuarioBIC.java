package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_UsuarioBIC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsuarioBIC.findAll", query="SELECT t FROM Tbl_UsuarioBIC t")
public class Tbl_UsuarioBIC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaResponsable")
	private String fld_CodAreaResponsable;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecModPassword")
	private Timestamp fld_FecModPassword;

	@Column(name="Fld_FecUltimoIngreso")
	private Timestamp fld_FecUltimoIngreso;

	@Column(name="Fld_FlagModPassword")
	private boolean fld_FlagModPassword;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Password")
	private String fld_Password;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_UsuarioBIC() {
	}

	public String getFld_CodAreaResponsable() {
		return this.fld_CodAreaResponsable;
	}

	public void setFld_CodAreaResponsable(String fld_CodAreaResponsable) {
		this.fld_CodAreaResponsable = fld_CodAreaResponsable;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecModPassword() {
		return this.fld_FecModPassword;
	}

	public void setFld_FecModPassword(Timestamp fld_FecModPassword) {
		this.fld_FecModPassword = fld_FecModPassword;
	}

	public Timestamp getFld_FecUltimoIngreso() {
		return this.fld_FecUltimoIngreso;
	}

	public void setFld_FecUltimoIngreso(Timestamp fld_FecUltimoIngreso) {
		this.fld_FecUltimoIngreso = fld_FecUltimoIngreso;
	}

	public boolean getFld_FlagModPassword() {
		return this.fld_FlagModPassword;
	}

	public void setFld_FlagModPassword(boolean fld_FlagModPassword) {
		this.fld_FlagModPassword = fld_FlagModPassword;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_Password() {
		return this.fld_Password;
	}

	public void setFld_Password(String fld_Password) {
		this.fld_Password = fld_Password;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}