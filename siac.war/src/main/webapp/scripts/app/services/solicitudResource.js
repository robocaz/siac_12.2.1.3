angular.module('ccs').factory('SolicitudResource', function ($resource) {
	var resource = $resource('rest/solicitud/', { nocache : new Date().getTime()},
	{
		'cmbtipo'  : { method:'GET' , isArray:true , url:'rest/solicitud/cmbtipo'  },
		'cmbcateg' : { method:'GET' , isArray:true , url:'rest/solicitud/cmbcateg' },
		'ingreso'  : { method:'POST', isArray:false, url:'rest/solicitud/ingreso'  },
		'buscar'   : { method:'POST', isArray:false, url:'rest/solicitud/buscar'   },
		'historial': { method:'POST', isArray:false, url:'rest/solicitud/historial'},
		'compr'    : { method:'POST', isArray:false, url:'rest/solicitud/compr'    },
		'form'     : { method:'POST', isArray:false, url:'rest/solicitud/form'     }
	});
	return resource;
});
