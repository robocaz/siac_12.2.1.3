package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdRelNombreCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdRelNombreCtaCte.findAll", query="SELECT t FROM Tb_CeIdRelNombreCtaCte t")
public class Tb_CeIdRelNombreCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	//bi-directional many-to-one association to Tb_CeIdDetCtaCte
	@ManyToOne
	@JoinColumn(name="Fld_CorrCtaCte")
	private Tb_CeIdDetCtaCte tbCeIdDetCtaCte;

	public Tb_CeIdRelNombreCtaCte() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public Tb_CeIdDetCtaCte getTbCeIdDetCtaCte() {
		return this.tbCeIdDetCtaCte;
	}

	public void setTbCeIdDetCtaCte(Tb_CeIdDetCtaCte tbCeIdDetCtaCte) {
		this.tbCeIdDetCtaCte = tbCeIdDetCtaCte;
	}

}