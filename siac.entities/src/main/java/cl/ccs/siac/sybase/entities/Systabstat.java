package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the systabstats database table.
 * 
 */
@Entity
@Table(name="systabstats")
@NamedQuery(name="Systabstat.findAll", query="SELECT s FROM Systabstat s")
public class Systabstat implements Serializable {
	private static final long serialVersionUID = 1L;

	private short activestatid;

	@Column(name="conopt_thld")
	private short conoptThld;

	private double datarowsize;

	private double delrowcnt;

	private double dpagecrcnt;

	private double drowcrcnt;

	private int emptypgcnt;

	private int extent0pgcnt;

	private double forwrowcnt;

	private int frlastoam;

	private int frlastpage;

	private int id;

	private short indexheight;

	private short indid;

	private double ipagecrcnt;

	private int leafcnt;

	private double leafrowsize;

	private int oamapgcnt;

	private int oampagecnt;

	private int pagecnt;

	private int partitionid;

	private float pioclmdata;

	private float pioclmindex;

	private float piocsmdata;

	private float piocsmindex;

	private short plldegree;

	private int plljoindegree;

	private double rowcnt;

	private int rslastoam;

	private int rslastpage;

	private Timestamp statmoddate;

	private int status;

	private int unusedpgcnt;

	private int warmcachepgcnt;

	public Systabstat() {
	}

	public short getActivestatid() {
		return this.activestatid;
	}

	public void setActivestatid(short activestatid) {
		this.activestatid = activestatid;
	}

	public short getConoptThld() {
		return this.conoptThld;
	}

	public void setConoptThld(short conoptThld) {
		this.conoptThld = conoptThld;
	}

	public double getDatarowsize() {
		return this.datarowsize;
	}

	public void setDatarowsize(double datarowsize) {
		this.datarowsize = datarowsize;
	}

	public double getDelrowcnt() {
		return this.delrowcnt;
	}

	public void setDelrowcnt(double delrowcnt) {
		this.delrowcnt = delrowcnt;
	}

	public double getDpagecrcnt() {
		return this.dpagecrcnt;
	}

	public void setDpagecrcnt(double dpagecrcnt) {
		this.dpagecrcnt = dpagecrcnt;
	}

	public double getDrowcrcnt() {
		return this.drowcrcnt;
	}

	public void setDrowcrcnt(double drowcrcnt) {
		this.drowcrcnt = drowcrcnt;
	}

	public int getEmptypgcnt() {
		return this.emptypgcnt;
	}

	public void setEmptypgcnt(int emptypgcnt) {
		this.emptypgcnt = emptypgcnt;
	}

	public int getExtent0pgcnt() {
		return this.extent0pgcnt;
	}

	public void setExtent0pgcnt(int extent0pgcnt) {
		this.extent0pgcnt = extent0pgcnt;
	}

	public double getForwrowcnt() {
		return this.forwrowcnt;
	}

	public void setForwrowcnt(double forwrowcnt) {
		this.forwrowcnt = forwrowcnt;
	}

	public int getFrlastoam() {
		return this.frlastoam;
	}

	public void setFrlastoam(int frlastoam) {
		this.frlastoam = frlastoam;
	}

	public int getFrlastpage() {
		return this.frlastpage;
	}

	public void setFrlastpage(int frlastpage) {
		this.frlastpage = frlastpage;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getIndexheight() {
		return this.indexheight;
	}

	public void setIndexheight(short indexheight) {
		this.indexheight = indexheight;
	}

	public short getIndid() {
		return this.indid;
	}

	public void setIndid(short indid) {
		this.indid = indid;
	}

	public double getIpagecrcnt() {
		return this.ipagecrcnt;
	}

	public void setIpagecrcnt(double ipagecrcnt) {
		this.ipagecrcnt = ipagecrcnt;
	}

	public int getLeafcnt() {
		return this.leafcnt;
	}

	public void setLeafcnt(int leafcnt) {
		this.leafcnt = leafcnt;
	}

	public double getLeafrowsize() {
		return this.leafrowsize;
	}

	public void setLeafrowsize(double leafrowsize) {
		this.leafrowsize = leafrowsize;
	}

	public int getOamapgcnt() {
		return this.oamapgcnt;
	}

	public void setOamapgcnt(int oamapgcnt) {
		this.oamapgcnt = oamapgcnt;
	}

	public int getOampagecnt() {
		return this.oampagecnt;
	}

	public void setOampagecnt(int oampagecnt) {
		this.oampagecnt = oampagecnt;
	}

	public int getPagecnt() {
		return this.pagecnt;
	}

	public void setPagecnt(int pagecnt) {
		this.pagecnt = pagecnt;
	}

	public int getPartitionid() {
		return this.partitionid;
	}

	public void setPartitionid(int partitionid) {
		this.partitionid = partitionid;
	}

	public float getPioclmdata() {
		return this.pioclmdata;
	}

	public void setPioclmdata(float pioclmdata) {
		this.pioclmdata = pioclmdata;
	}

	public float getPioclmindex() {
		return this.pioclmindex;
	}

	public void setPioclmindex(float pioclmindex) {
		this.pioclmindex = pioclmindex;
	}

	public float getPiocsmdata() {
		return this.piocsmdata;
	}

	public void setPiocsmdata(float piocsmdata) {
		this.piocsmdata = piocsmdata;
	}

	public float getPiocsmindex() {
		return this.piocsmindex;
	}

	public void setPiocsmindex(float piocsmindex) {
		this.piocsmindex = piocsmindex;
	}

	public short getPlldegree() {
		return this.plldegree;
	}

	public void setPlldegree(short plldegree) {
		this.plldegree = plldegree;
	}

	public int getPlljoindegree() {
		return this.plljoindegree;
	}

	public void setPlljoindegree(int plljoindegree) {
		this.plljoindegree = plljoindegree;
	}

	public double getRowcnt() {
		return this.rowcnt;
	}

	public void setRowcnt(double rowcnt) {
		this.rowcnt = rowcnt;
	}

	public int getRslastoam() {
		return this.rslastoam;
	}

	public void setRslastoam(int rslastoam) {
		this.rslastoam = rslastoam;
	}

	public int getRslastpage() {
		return this.rslastpage;
	}

	public void setRslastpage(int rslastpage) {
		this.rslastpage = rslastpage;
	}

	public Timestamp getStatmoddate() {
		return this.statmoddate;
	}

	public void setStatmoddate(Timestamp statmoddate) {
		this.statmoddate = statmoddate;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getUnusedpgcnt() {
		return this.unusedpgcnt;
	}

	public void setUnusedpgcnt(int unusedpgcnt) {
		this.unusedpgcnt = unusedpgcnt;
	}

	public int getWarmcachepgcnt() {
		return this.warmcachepgcnt;
	}

	public void setWarmcachepgcnt(int warmcachepgcnt) {
		this.warmcachepgcnt = warmcachepgcnt;
	}

}