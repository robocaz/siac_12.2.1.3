package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BoletinTrabajador_Emp implements Serializable {

	private static final long serialVersionUID = -6292193189683577717L;

	private String rutEmpleador;
	private String razonSocial;
	private BigDecimal montoAdeudado;
	private BigDecimal montoLaboralUTM;
	private List<BoletinDataTrabajador> lista;

	public String getRutEmpleador() {
		return rutEmpleador;
	}

	public void setRutEmpleador(String rutEmpleador) {
		this.rutEmpleador = rutEmpleador;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public BigDecimal getMontoAdeudado() {
		return montoAdeudado;
	}

	public void setMontoAdeudado(BigDecimal montoAdeudado) {
		this.montoAdeudado = montoAdeudado;
	}

	public BigDecimal getMontoLaboralUTM() {
		return montoLaboralUTM;
	}

	public void setMontoLaboralUTM(BigDecimal montoLaboralUTM) {
		this.montoLaboralUTM = montoLaboralUTM;
	}

	public List<BoletinDataTrabajador> getLista() {
		return lista;
	}

	public void setLista(List<BoletinDataTrabajador> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n rutEmpleador: " + rutEmpleador);
		str.append(", razonSocial: " + razonSocial);
		str.append(", montoAdeudado: " + montoAdeudado);
		str.append(", montoLaboralUTM: " + montoLaboralUTM);
		str.append(", lista: " + lista);
		return str.toString();
	}

}
