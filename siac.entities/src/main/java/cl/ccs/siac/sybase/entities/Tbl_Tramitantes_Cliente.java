package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Tramitantes_Clientes database table.
 * 
 */
@Entity
@Table(name="Tbl_Tramitantes_Clientes")
@NamedQuery(name="Tbl_Tramitantes_Cliente.findAll", query="SELECT t FROM Tbl_Tramitantes_Cliente t")
public class Tbl_Tramitantes_Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TRAMITANTES_CLIENTES_FLD_CORRTRAMITANTE_ID_GENERATOR", sequenceName="FLD_CORRTRAMITANTE_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TRAMITANTES_CLIENTES_FLD_CORRTRAMITANTE_ID_GENERATOR")
	@Column(name="Fld_CorrTramitante_Id")
	private long fld_CorrTramitante_Id;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	public Tbl_Tramitantes_Cliente() {
	}

	public long getFld_CorrTramitante_Id() {
		return this.fld_CorrTramitante_Id;
	}

	public void setFld_CorrTramitante_Id(long fld_CorrTramitante_Id) {
		this.fld_CorrTramitante_Id = fld_CorrTramitante_Id;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

}