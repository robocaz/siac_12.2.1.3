package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutMontoVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutMontoVCE.findAll", query="SELECT t FROM Tbl_RutMontoVCE t")
public class Tbl_RutMontoVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_Materno")
	private String fld_Materno;

	@Column(name="Fld_Nombres")
	private String fld_Nombres;

	@Column(name="Fld_NroDocAcl")
	private short fld_NroDocAcl;

	@Column(name="Fld_NroDocMora")
	private short fld_NroDocMora;

	@Column(name="Fld_NroDocProt")
	private short fld_NroDocProt;

	@Column(name="Fld_Paterno")
	private String fld_Paterno;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TotalMontoAcl")
	private BigDecimal fld_TotalMontoAcl;

	@Column(name="Fld_TotalMontoMora")
	private BigDecimal fld_TotalMontoMora;

	@Column(name="Fld_TotalMontoProt")
	private BigDecimal fld_TotalMontoProt;

	public Tbl_RutMontoVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public String getFld_Materno() {
		return this.fld_Materno;
	}

	public void setFld_Materno(String fld_Materno) {
		this.fld_Materno = fld_Materno;
	}

	public String getFld_Nombres() {
		return this.fld_Nombres;
	}

	public void setFld_Nombres(String fld_Nombres) {
		this.fld_Nombres = fld_Nombres;
	}

	public short getFld_NroDocAcl() {
		return this.fld_NroDocAcl;
	}

	public void setFld_NroDocAcl(short fld_NroDocAcl) {
		this.fld_NroDocAcl = fld_NroDocAcl;
	}

	public short getFld_NroDocMora() {
		return this.fld_NroDocMora;
	}

	public void setFld_NroDocMora(short fld_NroDocMora) {
		this.fld_NroDocMora = fld_NroDocMora;
	}

	public short getFld_NroDocProt() {
		return this.fld_NroDocProt;
	}

	public void setFld_NroDocProt(short fld_NroDocProt) {
		this.fld_NroDocProt = fld_NroDocProt;
	}

	public String getFld_Paterno() {
		return this.fld_Paterno;
	}

	public void setFld_Paterno(String fld_Paterno) {
		this.fld_Paterno = fld_Paterno;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public BigDecimal getFld_TotalMontoAcl() {
		return this.fld_TotalMontoAcl;
	}

	public void setFld_TotalMontoAcl(BigDecimal fld_TotalMontoAcl) {
		this.fld_TotalMontoAcl = fld_TotalMontoAcl;
	}

	public BigDecimal getFld_TotalMontoMora() {
		return this.fld_TotalMontoMora;
	}

	public void setFld_TotalMontoMora(BigDecimal fld_TotalMontoMora) {
		this.fld_TotalMontoMora = fld_TotalMontoMora;
	}

	public BigDecimal getFld_TotalMontoProt() {
		return this.fld_TotalMontoProt;
	}

	public void setFld_TotalMontoProt(BigDecimal fld_TotalMontoProt) {
		this.fld_TotalMontoProt = fld_TotalMontoProt;
	}

}