package cl.ccs.siac.servicios;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.aclaracion.AclaracionBusiness;
import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.sybase.entities.Tbl_Firma;
import cl.exe.ccs.dto.AclaracionDTO;
import cl.exe.ccs.dto.BipersonalDTO;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ConsultaCAADTO;
import cl.exe.ccs.dto.DetalleFirmaDTO;
import cl.exe.ccs.dto.MonedaDTO;
import cl.exe.ccs.dto.NumeroConfirmatorioDTO;
import cl.exe.ccs.dto.NumeroFirmasDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SucursalDTO;
import cl.exe.ccs.dto.UsuarioDTO;
import cl.exe.ccs.dto.VentaCarteraDTO;
import cl.exe.ccs.resources.Constants;

/**
 * @author rbocaz-ext Clase con los servicios asociados a la funcionalidad de
 *         Aclaracion
 *
 */
@Path("/aclaracion")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class AclaracionServicio {

	private static final Logger log = LogManager.getLogger(AclaracionServicio.class);

	@Inject
	private CombosBusiness combosBusiness;

	@Inject
	private AclaracionBusiness aclaracionBusiness;

	/**
	 * Servicio encargado de obtener los tipo de emisores, a desplegar en la
	 * pantalla de aclaracion
	 * 
	 * @param request
	 * @param tipoDocumento
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/tiposEmisores")
	public Response obtenerTiposEmisores(@Context HttpServletRequest request,@QueryParam("tipoDoc") String tipoDocumento) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerTiposEmisores - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<ComboDTO> respuesta = combosBusiness.obtenerTiposEmisoresPorDoc(tipoDocumento);

		if (respuesta.size() > 0) {
			return Response.ok(respuesta).build();
		} else {
			log.error("Listar Tipos Emisores por Tipo de Document - Message: Error del Sistema - Problemas al cargar listado con el valor :" + tipoDocumento);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	
	/**
	 * Servicio encargado de obtener los tipo de emisores, a desplegar en la
	 * pantalla de aclaracion
	 * 
	 * @param request
	 * @param tipoDocumento
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/tiposEmisoresTodos")
	public Response obtenerTiposEmisoresTodos(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerTiposEmisoresTodos - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<ComboDTO> respuesta = combosBusiness.obtenerTiposEmisores();

		if (respuesta.size() > 0) {
			return Response.ok(respuesta).build();
		} else {
			log.error("Listar Tipos Emisores (Todos) - Message: Error del Sistema - Problemas al cargar listado");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
	
	
	
	/**
	 * Servicio encargado de obtener los emisores en base al tipo de emisor
	 * 
	 * @param request
	 * @param tipoEmisor
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/emisores")
	public Response obtenerEmisores(@Context HttpServletRequest request, @QueryParam("tipoEmisor") String tipoEmisor) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerEmisores - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<ComboDTO> respuesta = combosBusiness.obtenerEmisores(tipoEmisor);

		if (respuesta.size() > 0) {
			return Response.ok(respuesta).build();
		} else {
			log.error("Listar Emisores Por Tipo - Message: Error del Sistema - Problemas al cargar listado con el tipo de emisor: " + tipoEmisor);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Servicio encargado de obtener la documentacion presentada,en base al tipo
	 * de documento
	 * 
	 * @param request
	 * @param tipoDocumento
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/docsaclaracion")
	public Response obtieneDocsAclaracion(@Context HttpServletRequest request,
			@QueryParam("tipoDoc") String tipoDocumento) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtieneDocsAclaracion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<ComboDTO> respuesta = combosBusiness.obtenerDocsAclaracion(tipoDocumento);

		if (respuesta.size() > 0) {
			return Response.ok(respuesta).build();
		} else {
			log.error("Listar Tipos Documentos Aclaracion - Message: Error del Sistema - Problemas al cargar listado con el tipo de documento: " + tipoDocumento);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * @param request
	 * @param tipoEmisor
	 * @param codEmisor
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/sucursales")
	public Response obtieneSucursales(@Context HttpServletRequest request, @QueryParam("tipoEmisor") String tipoEmisor,
			@QueryParam("codEmisor") String codEmisor) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtieneSucursales - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<SucursalDTO> sucursales = combosBusiness.obtenerSucursalesEmisor(tipoEmisor, codEmisor);

		if (sucursales.size() > 0) {
			return Response.ok(sucursales).build();
		} else {
			log.warn("Listar Tipos Documentos Aclaracion - Message: Error del Sistema - Problemas al cargar listado");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Servicio que obtiene los tipo de documentos
	 * 
	 * @param request
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/tiposdocumentos")
	public Response obtieneDocuPago(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtieneDocuPago - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<ComboDTO> sucursales = combosBusiness.tiposDocumentos();

		if (sucursales.size() > 0) {
			return Response.ok(sucursales).build();
		} else {
			log.error("Listar Tipos Documentos - Message: Error del Sistema - Problemas al cargar listado");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Servicio que obtiene valores para el combo tipo de moneda, en la pantalla
	 * de aclaracion
	 * 
	 * @param request
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/tipomoneda")
	public Response obtieneMonedas(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtieneMonedas - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		List<MonedaDTO> monedas = combosBusiness.obtenerMonedas();

		if (monedas.size() > 0) {
			return Response.ok(monedas).build();
		} else {
			log.error("Listar Tipo de Moneda - Message: Error del Sistema - Problemas al cargar listado");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Servicio que trae el listado de firmantes.
	 * 
	 * @param request
	 * @param detalle
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/buscafirmas")
	public Response buscarFirmas(@Context HttpServletRequest request, DetalleFirmaDTO detalle) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: buscarFirmas - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		ResponseListDTO<DetalleFirmaDTO> firmas = aclaracionBusiness.consultaFirmas(detalle);
		
		return Response.ok(firmas).build();

	}

	/**
	 * Servicio que obtiene imagen del firmante seleccionado.
	 * 
	 * @param request
	 * @param detalle
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/cargaimagen")
	public Response obtieneFirma(@Context HttpServletRequest request, DetalleFirmaDTO detalle) {

		try {
			System.out.println(detalle.getCorrFirma());
			String rutaArchivo = Constants.DIR_FIRMAS + "/" + detalle.getFirmante().getRut() + ".jpg";
			Tbl_Firma firma = aclaracionBusiness.obtieneFirma(detalle.getCorrFirma());

			if (firma != null) {
				File imagen = new File(rutaArchivo);
				FileUtils.writeByteArrayToFile(imagen, firma.getFld_Firma());
				detalle.setRutaImagen(imagen.getAbsolutePath());
				detalle.setImageBinary(firma.getFld_Firma());
				detalle.setIdControl(0);
			} else {
				detalle.setIdControl(1);
				detalle.setMsgControl("No existe firma asociada.");
			}

		} catch (IOException e) {
			log.error("Obtiene Firma (Imagen) - Message: Error del Sistema - Problemas al cargar la imagen");
			e.printStackTrace();
		}

		return Response.ok(detalle).build();

	}

	/**
	 * Servicio que verifica si la aclaracion requiere numero confirmatorio
	 * 
	 * @param request
	 * @param numFirmas
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/reqconfirmatorio")
	public Response requiereConfirmatorio(@Context HttpServletRequest request, NumeroFirmasDTO numFirmas) {

		try {
			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: requiereConfirmatorio - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			numFirmas = aclaracionBusiness.obtieneConfirmatorio(numFirmas);

		} catch (Exception e) {
			log.error("Metodo requiereConfirmatorio - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return Response.ok(numFirmas).build();

	}

	/**
	 * Servicio que valida que el numero confirmatorio ingresado sea correcto
	 * 
	 * @param request
	 * @param numConfirmatorio
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/numconfirmatorio")
	public Response validaConfirmatorio(@Context HttpServletRequest request, NumeroConfirmatorioDTO numConfirmatorio) {

		try {
			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: validaConfirmatorio - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
			numConfirmatorio = aclaracionBusiness.confimaNumero(numConfirmatorio);

		} catch (Exception e) {
			log.error("Metodo validaConfirmatorio - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return Response.ok(numConfirmatorio).build();

	}

	/**
	 * Servicio encargado de aceptar y guardar la aclaracion
	 * 
	 * @param request
	 * @param aclaracion
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/aceptaaclaracion")
	public Response aceptaAclaracion(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		int usaConfirmatorio = 0;
		try {
			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: aceptaAclaracion - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			aclaracion.setCorrcaja(Long.parseLong(dto.getCaja()));
			aclaracion.setCentroCosto(dto.getCentroCostoActivo());
			if (aclaracion.getDocpresentada().getCodigo().equals("CE")) {

				usaConfirmatorio = aclaracionBusiness.obtieneConfirmatorio(aclaracion.getNumerofirmas()).getUsaNrosConfirmatorios();
			}
			aclaracion = aclaracionBusiness.aceptaAclaracion(aclaracion);

			// guardo bipersonales
			if (aclaracion.getBipersonales() != null && aclaracion.getBipersonales().size() > 0
					&& aclaracion.getCodRetAcl() == 0) {

				for (BipersonalDTO bipersonal : aclaracion.getBipersonales()) {
					bipersonal.setCorrAclTemp(aclaracion.getTempCorrAcl());
					aclaracionBusiness.guardaBipersonalTemp(bipersonal);
				}

			}

			return Response.ok(aclaracion).build();

		} catch (Exception e) {
			log.error("Metodo aceptaAclaracion - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return Response.ok(null).build();

	}

	/**
	 * Servicion encargadop de aclarar candidato seleccionado
	 * 
	 * @param request
	 * @param aclaracion
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/aclaracandidato")
	public Response aclaraCandidato(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: aclaraCandidato - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			aclaracion = aclaracionBusiness.desDuplica(aclaracion);
			aclaracion = aclaracionBusiness.aclaracionTemp(aclaracion);
			aclaracion.setCodRetAcl(0L);
			aclaracion.setCantFilas(1L);
			// guardo bipersonales
			if (aclaracion.getBipersonales() != null && aclaracion.getBipersonales().size() > 0) {

				for (BipersonalDTO bipersonal : aclaracion.getBipersonales()) {
					bipersonal.setCorrAclTemp(aclaracion.getTempCorrAcl());
					aclaracionBusiness.guardaBipersonalTemp(bipersonal);
				}

			}
			return Response.ok(aclaracion).build();

		} catch (Exception e) {
			log.error("Metodo aclaraCandidato - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Servicio que busca cuotas anteriores para banco estado
	 * 
	 * @param request
	 * @param aclaracion
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/cuotasbcoestado")
	public Response buscaCuotasBcoEstado(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: buscaCuotasBcoEstado - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			aclaracion.setCandidatos(aclaracionBusiness.buscaCuotasBancoEstado(aclaracion));

			return Response.ok(aclaracion).build();

		} catch (Exception e) {
			log.error("Metodo buscaCuotasBcoEstado  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Servicio que aclara las cuotas seleccionadas, para banco estado
	 * 
	 * @param request
	 * @param aclaracion
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/aclaracuotas")
	public Response aclaraCuotasBancoEstado(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: aclaraCuotasBancoEstado - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			for (AclaracionDTO cuota : aclaracion.getCandidatos()) {

				cuota.setCriterio(2L);
				cuota.setCorrcaja(Long.parseLong(dto.getCaja()));
				cuota.setCentroCosto(aclaracion.getCentroCosto());
				cuota.setTramitante(aclaracion.getTramitante());
				cuota.setAfectado(aclaracion.getAfectado());
				cuota.setNumerofirmas(aclaracion.getNumerofirmas());
				cuota.getNumerofirmas().setTipoDocAclaracion("CA");
				cuota.setEstadocuota(aclaracion.getEstadocuota());
				// cambiar
				cuota.setFecPublicacion(aclaracion.getFecPublicacion());
				cuota.setTipoDocumentoImpago(aclaracion.getTipoDocumentoImpago());

				cuota = aclaracionBusiness.desDuplica(cuota);
				if (cuota.getIdControl() == 0) {
					cuota = aclaracionBusiness.aclaracionTemp(cuota);
				} else {
					return Response.ok(aclaracion).build();
				}
			}
			return Response.ok(aclaracion).build();

		} catch (Exception e) {
			log.error("Metodo aclaracuotas  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Servicio que verifica si la aclaracion tiene venta de cartera.
	 * 
	 * @param request
	 * @param aclaracion
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/ventacartera")
	public Response tieneVentaCartera(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: tieneVentaCartera - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			aclaracion = aclaracionBusiness.tieneVentaCartera(aclaracion);
			if (aclaracion.getVentaCartera() == null) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			} else {
				return Response.ok(aclaracion).build();
			}
		} catch (Exception e) {
			log.error("Metodo tieneVentaCartera  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Servicio encargado de validar los datos del emisor de la venta de cartera
	 *
	 * @param request
	 * @param tipoEmisor
	 * @param codEmisor
	 * @param corrProt
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/validaprotestocartera")
	public Response validaVentaCartera(@Context HttpServletRequest request, @QueryParam("tipoEmisor") String tipoEmisor,
			@QueryParam("codEmisor") String codEmisor, @QueryParam("corrProt") Long corrProt) {

		try {

			// agregar parametros a validar y no objecto completo

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: validaVentaCartera - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			VentaCarteraDTO ventaCartera = aclaracionBusiness.buscaProtestoVentaCartera(corrProt, tipoEmisor, codEmisor);
			if (ventaCartera == null) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			} else {
				return Response.ok(ventaCartera).build();
			}

		} catch (Exception e) {
			log.error("Metodo validaVentaCartera  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Servicio que elimina protesto de tablas temporales.
	 * 
	 * @param request
	 * @param corrProt
	 * @param corrCaja
	 * @return Response {@link} {@link Response#}
	 */
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/eliminaprotesto")
	public Response eliminaProtestoTemp(@Context HttpServletRequest request, @QueryParam("corrProt") Long corrProt, @QueryParam("corrCaja") Long corrCaja) {

		try {

			// agregar parametros a validar y no objecto completo

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: eliminaProtestoTemp - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			ResponseDTO respuesta = aclaracionBusiness.eliminaProtestoTemp(corrProt, corrCaja);
			if (respuesta == null) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			} else {
				return Response.ok(respuesta).build();
			}
		} catch (Exception e) {
			log.error("Metodo eliminaProtestoTemp  - Message: Error del Sistema Corr Protesto: " + corrProt + "Caja: " + corrCaja );
			log.error(e.getMessage());
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/guardabipersonal")
	public Response guardaBipersonal(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: guardaBipersonal - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			return Response.ok(aclaracion).build();

		} catch (Exception e) {
			log.error("Metodo guardaBipersonal  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/consultaproceso")
	public Response consultaAclaracionProceso(@Context HttpServletRequest request,
			@QueryParam("rutTramitante") String rutTramitante, @QueryParam("corrCaja") Long corrCaja) {

		try {

			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal == null || dto == null) {
				log.error("Error en el metodo: consultaAclaracionProceso - Sesion expiro");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			ResponseListDTO<PersonaDTO> personas = aclaracionBusiness.listaAclaraciones(corrCaja, rutTramitante);

			return Response.ok(personas).build();

		} catch (Exception e) {
			log.error("Metodo consultaAclaracionProceso  - Message: Error del Sistema Rut tramitante: " + rutTramitante + "Caja: " + corrCaja);
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Servicio que consulta por aclaraciones para un rut y fecha determinada
	 * 
	 * @param request
	 * @param consulta
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/consultaRutFecha")
	public Response consultaRutFecha(@Context HttpServletRequest request, ConsultaCAADTO consulta) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: consultaRutFecha - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		ResponseListDTO<ConsultaCAADTO> response = aclaracionBusiness.consultaRutFecha(consulta);
		return Response.ok(response).build();

	}

	/**
	 * Servicio que consulta por aclaraciones para certificados CAC
	 * 
	 * @param request
	 * @param consulta
	 * @return Response {@link} {@link Response#}
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/consultaAclsCertificado")
	public Response consultaAclsCertificado(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: consultaAclsCertificado - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		try{
			AclaracionDTO response = aclaracionBusiness.aclaracionesParaCertificado(aclaracion);
		
			return Response.ok(response).build();
		}
		catch(Exception e){
			log.error("Metodo consultaAclsCertificado  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
	
	
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/consultacarro")
	public Response consultCarroAclaracion(@Context HttpServletRequest request, AclaracionDTO aclaracion) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: consultCarroAclaracion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		aclaracion.getTramitante().setCorrCaja(Long.parseLong(dto.getCaja()));
		try{
			ResponseDTO response = aclaracionBusiness.validaAclaracionCarro(aclaracion);
			return Response.ok(response).build();
		}
		catch(Exception e){
			log.error("Metodo consultCarroAclaracion  - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
}
