package cl.ccs.siac.fraude;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.DetectorFraudeDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.DetectorFraudeDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("detectorFraudeBusiness")
public class DetectorFraudeBusinessImpl implements DetectorFraudeBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private DetectorFraudeDao detectorFraudeDao;
	
	@Override
	public ResponseListDTO<DetectorFraudeDTO> getDetallesFraudePorRut(final String rut) {
		List<DetectorFraudeDTO> listDetalles = new ArrayList<>();
		ResponseListDTO<DetectorFraudeDTO> responseList = new ResponseListDTO<>();
		responseList.setMsgControl(DetectorFraudeDTO.EnListaNegra.NO.value);
		responseList.setIdControl(0);
		String rutFormateado = "";
		
		if (rut != null && !rut.isEmpty()) {
			rutFormateado = utils.formatZeroRut(rut);
			
			DetectorFraudeDTO detectorDTO = detectorFraudeDao.detectarPorRut(rutFormateado);
			if (detectorDTO.getEnListaNegra() == DetectorFraudeDTO.EnListaNegra.SI) {
				listDetalles = detectorFraudeDao.getDetalleFraudePorRut(rutFormateado);
			}
		}
		
		if (listDetalles != null && !listDetalles.isEmpty()) {
			responseList.setLista(listDetalles);
			responseList.setMsgControl(DetectorFraudeDTO.EnListaNegra.SI.value);
			responseList.setIdControl(1);
		}
		
		return responseList;
	}
	
	@Override
	public ResponseDTO validaAutorizacion(DetectorFraudeDTO dataFraude) {
		ResponseDTO response = new ResponseDTO();
		
		String rutFormateado = utils.formatZeroRut(dataFraude.getRut());
		
		response = detectorFraudeDao.validaAutorizacion(dataFraude.getCorrCaja(), rutFormateado);
		return response;
	}

}
