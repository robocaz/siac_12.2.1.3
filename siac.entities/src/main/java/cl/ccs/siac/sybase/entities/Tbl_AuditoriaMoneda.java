package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditoriaMoneda database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditoriaMoneda.findAll", query="SELECT t FROM Tbl_AuditoriaMoneda t")
public class Tbl_AuditoriaMoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecDesde")
	private Timestamp fld_FecDesde;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_FecHasta")
	private Timestamp fld_FecHasta;

	@Column(name="Fld_TipoGeneracion")
	private String fld_TipoGeneracion;

	@Column(name="Fld_ValorMoneda")
	private BigDecimal fld_ValorMoneda;

	public Tbl_AuditoriaMoneda() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecDesde() {
		return this.fld_FecDesde;
	}

	public void setFld_FecDesde(Timestamp fld_FecDesde) {
		this.fld_FecDesde = fld_FecDesde;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public Timestamp getFld_FecHasta() {
		return this.fld_FecHasta;
	}

	public void setFld_FecHasta(Timestamp fld_FecHasta) {
		this.fld_FecHasta = fld_FecHasta;
	}

	public String getFld_TipoGeneracion() {
		return this.fld_TipoGeneracion;
	}

	public void setFld_TipoGeneracion(String fld_TipoGeneracion) {
		this.fld_TipoGeneracion = fld_TipoGeneracion;
	}

	public BigDecimal getFld_ValorMoneda() {
		return this.fld_ValorMoneda;
	}

	public void setFld_ValorMoneda(BigDecimal fld_ValorMoneda) {
		this.fld_ValorMoneda = fld_ValorMoneda;
	}

}