package cl.ccs.siac.solicitud;

import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarInDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoIngresoOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoIngresoInDTO;

public interface SolicitudBusiness {

	SolicitudReclamoIngresoOutDTO ingreso(SolicitudReclamoIngresoInDTO params);
	
	SolicitudReclamoBuscarOutDTO buscar(SolicitudReclamoBuscarInDTO params);
	
	ResponseListDTO<String[]> historial(Integer corrSolic);

}
