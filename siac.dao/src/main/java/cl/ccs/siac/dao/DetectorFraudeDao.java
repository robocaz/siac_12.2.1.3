package cl.ccs.siac.dao;

import java.util.List;

import cl.exe.ccs.dto.DetectorFraudeDTO;
import cl.exe.ccs.dto.ResponseDTO;

public interface DetectorFraudeDao {

	DetectorFraudeDTO detectarPorRut(String rut);

	List<DetectorFraudeDTO> getDetalleFraudePorRut(String rut);

	ResponseDTO validaAutorizacion(Long corrCaja, String rut);

}
