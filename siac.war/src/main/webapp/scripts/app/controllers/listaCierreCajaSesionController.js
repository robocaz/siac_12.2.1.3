angular.module('ccs').controller('ListaCierreCajaSesionController', function($document, $scope, CajaResource, $rootScope, $location, $filter, $timeout, LoginResource, $uibModal) {

    $scope.listaCajasAbiertas = [];

    $scope.caja = {};

    $scope.listar = function(){
        var successCallback = function(data){
            $scope.listaCajasAbiertas = data;
            $scope.caja = {};
        };
        CajaResource.listarcajastodas(successCallback, $scope.errorCallback);
    }
    $('#listacierrecajasesion').on('show.bs.modal', function (e) {
        $timeout(function(){
          $scope.listar();
        });
    });


    $scope.aceptarSeleccion = function(){
        if($scope.caja.id == null){
            swal('Alerta','Debe seleccionar caja','error')
        }else if($scope.listaCajasAbiertas[$scope.caja.id].estado == 3){
            swal('Alerta','Ya se encuentra en estado Cierre Definitivo','error')
        }else{
			swal({
				title: 'Alerta',
				text: '¿Esta seguro de cerrar la caja?',
				type: 'question',
				confirmButtonText: 'Si',
				showCancelButton: true,
				cancelButtonText: 'No'
			}).then( function() {
					var successCallback = function(data){
						$scope.listaCajasAbiertas[$scope.caja.id].estado = 3;
						if($rootScope.usuario.caja == $scope.listaCajasAbiertas[$scope.caja.id].corrCaja){
							$rootScope.usuario.caja = "0";
							$location.path("/inicio");
						}
					};
					CajaResource.cerrarcajadefinitivo( $scope.listaCajasAbiertas[$scope.caja.id].corrCaja , successCallback, $scope.errorCallback);
				}
			);
        }
    }

    $scope.salir = function(){
        var cantidadsolicitudes = 0;
        var cantidadexitosos = 0;
        console.log("salir")
        for(x=0; x<$scope.listaCajasAbiertas.length; x++){
            if($scope.listaCajasAbiertas[x].estado==1){
                console.log("cantidadsolicitudes: " + cantidadsolicitudes + ", cantidadexitosos: "+cantidadexitosos);
                cantidadsolicitudes = cantidadsolicitudes + 1;
                var successCallback = function(data){
                    cantidadexitosos = cantidadexitosos + 1;
                    if(cantidadsolicitudes==cantidadexitosos){
                        $scope.cerrarsesion();
                    }
                };
                var errorCallback = function(data){
                    console.log("ERROR");
                };
                CajaResource.cerrarcajatemporal($scope.listaCajasAbiertas[x].corrCaja ,  successCallback, errorCallback);
            }
        }
        if(cantidadsolicitudes==0){
            console.log("salir sin abiertas");
            $scope.cerrarsesion();
        }
        $scope.cierraCajaActualiza();
        //$scope.cantidadesabiertas();
    };

    $scope.cerrarsesion = function(){
        var successCallbackLogOut = function(data){
            $rootScope.usuario = {};
            $('.modal-backdrop').remove();
            $scope.blockBack(0);
            $location.url('/login')
        };
        LoginResource.logout(successCallbackLogOut, $scope.errorCallback);
    };
    
    
    $scope.cierraCajaActualiza = function() {
	    var successCallback = function(data){
	
	            $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
	            $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
	    };
	    CajaResource.cierracaja( successCallback, $scope.errorCallback);
    	     
    };
    
    
    $scope.cantidadesabiertas = function() {
        var successCallback = function(data){

                $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
                $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
        };
        CajaResource.cantidadabiertas( successCallback, $scope.errorCallback);
      
    };
});
