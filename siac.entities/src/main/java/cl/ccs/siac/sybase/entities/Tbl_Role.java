package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Roles database table.
 * 
 */
@Entity
@Table(name="Tbl_Roles")
@NamedQuery(name="Tbl_Role.findAll", query="SELECT t FROM Tbl_Role t")
public class Tbl_Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoRol")
	private String fld_CodigoRol;

	@Column(name="Fld_DescripcionRol")
	private String fld_DescripcionRol;

	@Column(name="Fld_ModuloRol")
	private String fld_ModuloRol;

	public Tbl_Role() {
	}

	public String getFld_CodigoRol() {
		return this.fld_CodigoRol;
	}

	public void setFld_CodigoRol(String fld_CodigoRol) {
		this.fld_CodigoRol = fld_CodigoRol;
	}

	public String getFld_DescripcionRol() {
		return this.fld_DescripcionRol;
	}

	public void setFld_DescripcionRol(String fld_DescripcionRol) {
		this.fld_DescripcionRol = fld_DescripcionRol;
	}

	public String getFld_ModuloRol() {
		return this.fld_ModuloRol;
	}

	public void setFld_ModuloRol(String fld_ModuloRol) {
		this.fld_ModuloRol = fld_ModuloRol;
	}

}