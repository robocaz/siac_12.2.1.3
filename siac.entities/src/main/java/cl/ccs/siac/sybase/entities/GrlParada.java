package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the GRL_PARADA database table.
 * 
 */
@Entity
@Table(name="GRL_PARADA")
@NamedQuery(name="GrlParada.findAll", query="SELECT g FROM GrlParada g")
public class GrlParada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="flag_parada")
	private boolean flagParada;

	private String proceso;

	public GrlParada() {
	}

	public boolean getFlagParada() {
		return this.flagParada;
	}

	public void setFlagParada(boolean flagParada) {
		this.flagParada = flagParada;
	}

	public String getProceso() {
		return this.proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

}