package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RATipoSolicitud database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RATipoSolicitud.findAll", query="SELECT t FROM Tbl_RATipoSolicitud t")
public class Tbl_RATipoSolicitud implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoSolicitud")
	private String fld_GlosaTipoSolicitud;

	@Column(name="Fld_TipoSolicitud")
	private String fld_TipoSolicitud;

	public Tbl_RATipoSolicitud() {
	}

	public String getFld_GlosaTipoSolicitud() {
		return this.fld_GlosaTipoSolicitud;
	}

	public void setFld_GlosaTipoSolicitud(String fld_GlosaTipoSolicitud) {
		this.fld_GlosaTipoSolicitud = fld_GlosaTipoSolicitud;
	}

	public String getFld_TipoSolicitud() {
		return this.fld_TipoSolicitud;
	}

	public void setFld_TipoSolicitud(String fld_TipoSolicitud) {
		this.fld_TipoSolicitud = fld_TipoSolicitud;
	}

}