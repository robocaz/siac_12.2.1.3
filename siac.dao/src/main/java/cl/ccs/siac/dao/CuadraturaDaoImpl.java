package cl.ccs.siac.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.CajaInformeDTO;
import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaAclaracionDTO;
import cl.exe.ccs.dto.CuadraturaCajaDTO;
import cl.exe.ccs.dto.CuadraturaCentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaUsuarioDTO;
import cl.exe.ccs.dto.DatoBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleAclaracionDTO;
import cl.exe.ccs.dto.DetalleBoletaDTO;
import cl.exe.ccs.dto.DetalleBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleCajaDTO;
import cl.exe.ccs.dto.DetalleCentroCostoDTO;
import cl.exe.ccs.dto.DetalleFacturaDTO;
import cl.exe.ccs.dto.DetalleListaServicioDTO;
import cl.exe.ccs.dto.DetalleServicioDTO;
import cl.exe.ccs.dto.ItemBoletaDTO;
import cl.exe.ccs.dto.MovimientoDTO;
import cl.exe.ccs.dto.PorcentajeIvaDTO;
import cl.exe.ccs.dto.ReclamoDTO;
import cl.exe.ccs.dto.ResultadoPorCCDTO;
import cl.exe.ccs.dto.ResultadoPorUsuarioDTO;
import cl.exe.ccs.dto.ResumenMovimientoDTO;
import cl.exe.ccs.dto.SolicitudReclamoDTO;
import cl.exe.ccs.dto.UsuariosInformeDTO;

@Named("cuadratguraDao")
@Stateless
public class CuadraturaDaoImpl implements CuadraturaDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger LOG = LogManager.getLogger(CuadraturaDaoImpl.class);

	@Override
	public List<CentroCostoDTO> centrosCostoInforme(Integer centroCosto, Date desde, Date hasta) {

		try{
			
			LOG.info("Ejecutando SQ_InformeCCosto_SW2");
			
			List<CentroCostoDTO> centros = new ArrayList<CentroCostoDTO>();
	
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_InformeCCosto_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			
			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}
	
			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);
	
			storedProcedure.execute();
	
			List<Object[]> listaCentroCosto = storedProcedure.getResultList();
			for (Object item : listaCentroCosto) {
				CentroCostoDTO centro = new CentroCostoDTO();
				Object[] datos = (Object[]) item;
				if (!datos[0].toString().equals("0")) {
					centro.setCodigo(datos[0].toString());
					if(datos[1] != null){
						centro.setDescripcion(datos[1].toString());
					}
					else{
						centro.setDescripcion("");
					}
					centro.setCodigoTipo("CC");
					centro.setDescripcionTipo("CentroCosto");
					centros.add(centro);
				}
			}
	
			return centros;
		}
		catch (Exception e) {
				LOG.error("Error en el SP SQ_InformeCCosto_SW2 " + e.getMessage());
				return null;
		}
		
	}

	@Override
	public List<UsuariosInformeDTO> usuariosInforme(Integer centroCosto, Date desde, Date hasta) {

		List<UsuariosInformeDTO> usuarios = new ArrayList<UsuariosInformeDTO>();

		try {
			
			LOG.info("Ejecutando SQ_InformeUsrCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_InformeUsrCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}

			storedProcedure.execute();

			Integer retorno = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());

			List<Object[]> usuariosLista = storedProcedure.getResultList();
			for (Object item : usuariosLista) {
				UsuariosInformeDTO usuario = new UsuariosInformeDTO();
				usuario.setUsuario(item.toString());
				usuario.setCentroCosto(centroCosto);
				usuario.setCodigoTipo("user");
				usuario.setFechaDesde(desde);
				usuario.setFechaHasta(hasta);
				usuarios.add(usuario);
			}

			return usuarios;
		} catch (Exception e) {
			LOG.error("Error en el SP SQ_InformeUsrCC_SW2 " + e.getMessage());
			return usuarios;
		}

	}

	@Override
	public List<CajaInformeDTO> cajaInforme(String usuario, Integer centroCosto, Date desde, Date hasta) {

		List<CajaInformeDTO> cajas = new ArrayList<CajaInformeDTO>();

		try {
			LOG.info("Ejecutando SQ_InformeCajasCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_InformeCajasCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}
			storedProcedure.setParameter("Fld_CodUsuario", usuario);

			storedProcedure.execute();

			Integer retorno = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());

			List<Object[]> cajaLista = storedProcedure.getResultList();
			for (Object item : cajaLista) {
				CajaInformeDTO caja = new CajaInformeDTO();
				Object[] datos = (Object[]) item;
				caja.setCodigoTipo("caja");
				caja.setCorrCaja(datos[0].toString());
				caja.setInicioCaja(datos[1].toString());
				caja.setCentroCosto(centroCosto);
				cajas.add(caja);
			}

			return cajas;

		} catch (Exception e) {
			LOG.error("Error en el SP SQ_InformeCajasCC_SW2 " + e.getMessage());
			return cajas;
		}

	}

	@Override
	public CuadraturaCentroCostoDTO cuadraturaPorCamara(Integer centroCosto, Date desde, Date hasta) {

		CuadraturaCentroCostoDTO cuadraturaCC = new CuadraturaCentroCostoDTO();

		try {

			LOG.info("Ejecutando SQ_CuadNivelCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadNivelCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalCalculado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCCosto", String.class, ParameterMode.OUT);

			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}
			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			storedProcedure.execute();

			List<ResultadoPorCCDTO> resultados = new ArrayList<ResultadoPorCCDTO>();
			List<Object[]> cuadCentros = storedProcedure.getResultList();
			for (Object item : cuadCentros) {
				ResultadoPorCCDTO resultado = new ResultadoPorCCDTO();
				Object[] datos = (Object[]) item;
				resultado.setCentroCosto(Integer.parseInt(datos[0].toString()));
				resultado.setGlosaCC(datos[1].toString());
				resultado.setEstadoCaja(datos[2].toString());
				resultado.setCantCajasAbiertas(Integer.parseInt(datos[3].toString()));
				int pos = datos[4].toString().indexOf(".");
				resultado.setMontoTransacciones(Long.parseLong(datos[4].toString().substring(0, pos)));
				pos = datos[5].toString().indexOf(".");
				resultado.setMontoIngresadoCajera(Long.parseLong(datos[5].toString().substring(0, pos)));
				pos = datos[6].toString().indexOf(".");
				resultado.setDiferenciaPos(Long.parseLong(datos[6].toString().substring(0, pos)));
				pos = datos[7].toString().indexOf(".");
				resultado.setDiferenciaNeg(Long.parseLong(datos[7].toString().substring(0, pos)));
				resultados.add(resultado);
			}

			ResultadoPorCCDTO centroCostoCero = new ResultadoPorCCDTO();
			centroCostoCero.setCentroCosto(0);
			centroCostoCero.setGlosaCC("Todos");
			centroCostoCero.setEstadoCaja("");
			resultados.add(0, centroCostoCero);

			if(storedProcedure.getOutputParameterValue("Fld_TotalCalculado") != null){
				cuadraturaCC.setMontoResuTransacciones(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalCalculado").toString()));
			}
			else{
				cuadraturaCC.setMontoResuTransacciones(0L);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalPagado") != null){
				cuadraturaCC.setMontoResuIngCajera(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			}
			else{
				cuadraturaCC.setMontoResuIngCajera(0L);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia") != null){
				cuadraturaCC.setMontoResuDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			}
			else{
				cuadraturaCC.setMontoResuDiferencia(0L);
			}
			cuadraturaCC.setResultadoCentros(resultados);

			return cuadraturaCC;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadNivelCC_SW2 " + e.getMessage());
			return null;
		}

	}

	@Override
	public CuadraturaUsuarioDTO cuadraturaPorUsuario(Integer centroCosto, String usuario, Date desde, Date hasta) {

		CuadraturaUsuarioDTO cuadraturaUser = new CuadraturaUsuarioDTO();

		try {

			LOG.info("Ejecutando SQ_CuadNivelUsuarioCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadNivelUsuarioCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalCalculado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCCosto", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}
			storedProcedure.setParameter("Fld_CodUsuario", usuario);

			storedProcedure.execute();

			List<ResultadoPorUsuarioDTO> resultados = new ArrayList<ResultadoPorUsuarioDTO>();
			List<Object[]> cuadUsuarios = storedProcedure.getResultList();
			for (Object item : cuadUsuarios) {
				ResultadoPorUsuarioDTO resultadoUsuario = new ResultadoPorUsuarioDTO();
				Object[] datos = (Object[]) item;
				resultadoUsuario.setCorrCaja(Integer.parseInt(datos[0].toString()));
				resultadoUsuario.setEstadoCaja(datos[1].toString());
				resultadoUsuario.setFechaInicio(datos[2].toString());
				int pos = datos[3].toString().indexOf(".");
				resultadoUsuario.setMontoCalculado(Long.parseLong(datos[3].toString().substring(0, pos)));
				pos = datos[4].toString().indexOf(".");
				resultadoUsuario.setMontoIngresadoCajera(Long.parseLong(datos[4].toString().substring(0, pos)));
				pos = datos[5].toString().indexOf(".");
				resultadoUsuario.setDiferenciaPos(Long.parseLong(datos[5].toString().substring(0, pos)));
				pos = datos[6].toString().indexOf(".");
				resultadoUsuario.setDiferenciaNeg(Long.parseLong(datos[6].toString().substring(0, pos)));
				resultados.add(resultadoUsuario);
			}

			if (storedProcedure.getOutputParameterValue("Fld_TotalCalculado") != null) {
				cuadraturaUser.setMontoResuTransacciones(
						Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalCalculado").toString()));
			} else {
				cuadraturaUser.setMontoResuTransacciones(0L);
			}
			if (storedProcedure.getOutputParameterValue("Fld_TotalPagado") != null) {
				cuadraturaUser.setMontoResuIngCajera(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			} else {
				cuadraturaUser.setMontoResuIngCajera(0L);
			}

			if (storedProcedure.getOutputParameterValue("Fld_TotalDiferencia") != null) {
				cuadraturaUser.setMontoResuDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			} else {
				cuadraturaUser.setMontoResuDiferencia(0L);
			}
			cuadraturaUser.setGolsaCC(storedProcedure.getOutputParameterValue("Fld_GlosaCCosto").toString());
			cuadraturaUser.setResultadoUsuarios(resultados);

			return cuadraturaUser;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadNivelUsuarioCC_SW2 " + e.getMessage());
			return null;
		}

	}

	@Override
	public DetalleCentroCostoDTO cuadraturaPorCentroCosto(Integer centroCosto, Date desde, Date hasta) {

		DetalleCentroCostoDTO detalleCC = new DetalleCentroCostoDTO();

		try {

			LOG.info("Ejecutando SQ_CuadNivelCajasCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadNivelCajasCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotMontoMovimientos", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotMontoTransacciones", Long.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotMontoPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotDifMontoTrans", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotDifMontoMvntos", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCCosto", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(desde.getTime());
			Timestamp hastaTime = new Timestamp(hasta.getTime());
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			if (centroCosto == 0) {
				storedProcedure.setParameter("Fld_CodCCosto", 0);
			} else {
				storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			}

			storedProcedure.execute();

			List<DetalleCajaDTO> resultados = new ArrayList<DetalleCajaDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				DetalleCajaDTO caja = new DetalleCajaDTO();
				caja.setCentroCosto(centroCosto.toString());
				caja.setUsuario(datos[0].toString());
				caja.setNumeroCaja(Integer.parseInt(datos[1].toString()));
				caja.setFechaInicio(datos[2].toString());
				caja.setEstadoCaja(datos[3].toString());
				int pos = datos[4].toString().indexOf(".");
				caja.setMontoCalculaBF(Long.parseLong(datos[4].toString().substring(0, pos)));
				pos = datos[5].toString().indexOf(".");
				caja.setMontoCalculaDet(Long.parseLong(datos[5].toString().substring(0, pos)));
				pos = datos[6].toString().indexOf(".");
				caja.setMontoPagado(Long.parseLong(datos[6].toString().substring(0, pos)));
				pos = datos[7].toString().indexOf(".");
				caja.setDiferenciaBF(Long.parseLong(datos[7].toString().substring(0, pos)));
				pos = datos[8].toString().indexOf(".");
				caja.setDiferenciaDetalle(Long.parseLong(datos[8].toString().substring(0, pos)));
				resultados.add(caja);
			}

			detalleCC.setCajasDetalle(resultados);
			detalleCC.setGlosaCC(storedProcedure.getOutputParameterValue("Fld_GlosaCCosto").toString());
			detalleCC.setMontoCalculadoBF(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotMontoMovimientos").toString()));
			detalleCC.setMontoCalculadoDet(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotMontoTransacciones").toString()));
			detalleCC.setMontoPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotMontoPagado").toString()));
			detalleCC.setDiferenciaBF(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotDifMontoTrans").toString()));
			detalleCC.setDiferenciaDet(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotDifMontoMvntos").toString()));

			return detalleCC;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadNivelCajasCC_SW2 " + e.getMessage());
			return null;
		}

	}

	public PorcentajeIvaDTO obtienePorcentajeIva() {

		PorcentajeIvaDTO porcentaje = new PorcentajeIvaDTO();
		try {

			LOG.info("Ejecutando SQ_CuadNivelCajasCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadNivelCajasCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_PorcentajeIVA", Float.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.execute();

			porcentaje.setPorcentaje(Float.parseFloat(storedProcedure.getOutputParameterValue("Fld_TotMontoMovimientos").toString()));
			porcentaje.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			porcentaje.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			return porcentaje;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadNivelCajasCC_SW2 " + e.getMessage());
			return null;
		}

	}

	public List<CuadraturaCajaDTO> buscarCajasPorUsuario(String usuario, Integer centroCosto, Date fechaInicio) {

		try {

			LOG.info("Ejecutando SQ_CajasXUsrCC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CajasXUsrCC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicio", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(fechaInicio.getTime());

			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			storedProcedure.setParameter("Fld_FecInicio", desdeTime);

			storedProcedure.execute();

			List<CuadraturaCajaDTO> resultados = new ArrayList<CuadraturaCajaDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				CuadraturaCajaDTO cuadCaja = new CuadraturaCajaDTO();
				cuadCaja.setCorrCaja(Integer.parseInt(datos[0].toString()));
				cuadCaja.setCodCentroCosto(Integer.parseInt(datos[1].toString()));
				cuadCaja.setGlosaCentroCosto(datos[2].toString());
				cuadCaja.setHoraInicio(datos[3].toString());
				cuadCaja.setHoraTermino(datos[4].toString());
				resultados.add(cuadCaja);
			}

			if (storedProcedure.getOutputParameterValue("Fld_Retorno") != null) {
				Integer retorno = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());
				if (retorno == 1) {
					LOG.info(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
				}
			}
			return resultados;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CajasXUsrCC_SW2 " + e.getMessage());
			return null;
		}
	}

	public List<CuadraturaCajaDTO> buscarCajasPorCajero(String usuario, Long fechaInicio) {

		try {


			LOG.info("Ejecutando SQ_CajasXCajero_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CajasXCajero_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicio", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_FecInicio", fechaInicio);

			storedProcedure.execute();

			List<CuadraturaCajaDTO> resultados = new ArrayList<CuadraturaCajaDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				CuadraturaCajaDTO cuadCaja = new CuadraturaCajaDTO();
				cuadCaja.setCorrCaja(Integer.parseInt(datos[0].toString()));
				cuadCaja.setCodCentroCosto(Integer.parseInt(datos[1].toString()));
				cuadCaja.setGlosaCentroCosto(datos[2].toString());
				cuadCaja.setHoraInicio(datos[3].toString());
				cuadCaja.setHoraTermino(datos[4].toString());
				resultados.add(cuadCaja);
			}

			Integer retorno = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());
			if (retorno == 1) {
				LOG.info(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}
			return resultados;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CajasXCajero_SW2 " + e.getMessage());
			return null;
		}

	}

	public ResumenMovimientoDTO obtieneMovimientosNac(Integer centroCosto, Date fechaInicio, Date fechaDesde) {

		ResumenMovimientoDTO resumen = new ResumenMovimientoDTO();

		try {
			
			LOG.info("Ejecutando SQ_CuadMovntosNac_SW2");

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadMovntosNac_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDesde", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecHasta", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalTransacciones", Integer.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalMovimientos", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalAviPago", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalBoletas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalFacturas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCCosto", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(fechaInicio.getTime());
			Timestamp hastaTime = new Timestamp(fechaDesde.getTime());

			storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			storedProcedure.setParameter("Fld_FecDesde", desdeTime);
			storedProcedure.setParameter("Fld_FecHasta", hastaTime);

			storedProcedure.execute();

			List<MovimientoDTO> resultados = new ArrayList<MovimientoDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				MovimientoDTO movimiento = new MovimientoDTO();
				movimiento.setGlosaServicio(datos[0].toString());
				movimiento.setBoletaMovimiento(Integer.parseInt(datos[1].toString()));
				movimiento.setMontoTransaccionBoleta(Double.parseDouble(datos[2].toString()));
				movimiento.setMontoCantidadBoleta(Double.parseDouble(datos[3].toString()));
				movimiento.setDiferenciaBoleta(Double.parseDouble(datos[4].toString()));
				movimiento.setFacturaMovimientos(Double.parseDouble(datos[5].toString()));
				movimiento.setMontoTransaccionFactura(Double.parseDouble(datos[6].toString()));
				movimiento.setMontoCantidadFactura(Double.parseDouble(datos[7].toString()));
				movimiento.setDiferenciaFactura(Double.parseDouble(datos[8].toString()));
				resultados.add(movimiento);
			}

			resumen.setMovimientos(resultados);
			if(centroCosto == 0){
				resumen.setGlosaCC("Todos");
			}
			else{
				resumen.setGlosaCC(storedProcedure.getOutputParameterValue("Fld_GlosaCCosto").toString());
			}
			resumen.setTotalTransacciones(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalTransacciones").toString()));
			resumen.setTotalMovimientos(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalMovimientos").toString()));
			resumen.setTotalAviPago(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalAviPago").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_TotalPagado") != null) {
				resumen.setTotalPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			} else {
				resumen.setTotalPagado(0L);
			}
			if (storedProcedure.getOutputParameterValue("Fld_TotalDiferencia") != null) {
				resumen.setTotalDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			} else {
				resumen.setTotalDiferencia(0L);
			}
			if (storedProcedure.getOutputParameterValue("Fld_TotalBoletas") != null) {
				resumen.setTotalBoletas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalBoletas").toString()));
			} else {
				resumen.setTotalBoletas(0);
			}
			if (storedProcedure.getOutputParameterValue("Fld_TotalFacturas") != null) {
				resumen.setTotalFacturas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalFacturas").toString()));
			} else {
				resumen.setTotalFacturas(0);
			}

			return resumen;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadMovntosNac_SW2 " + e.getMessage());
			return null;
		}

	}

	public ResumenMovimientoDTO obtieneMovimientos(Date fechaInicio, String usuario, Integer centroCosto,
			Integer corrCaja, Integer criterio) {

		ResumenMovimientoDTO resumen = new ResumenMovimientoDTO();

		try {

			LOG.info("Ejecutando SQ_CuadMovimientos_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CuadMovimientos_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicio", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalTransacciones", Integer.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalMovimientos", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalAviPago", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalBoletas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalFacturas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCCosto", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(fechaInicio.getTime());

			storedProcedure.setParameter("Fld_FecInicio", desdeTime);
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			if(corrCaja == null){
				storedProcedure.setParameter("Fld_CorrCaja", 0);
			}
			else{
				storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			}
			storedProcedure.setParameter("Fld_Criterio", criterio);

			storedProcedure.execute();

			List<MovimientoDTO> resultados = new ArrayList<MovimientoDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				MovimientoDTO movimiento = new MovimientoDTO();
				movimiento.setGlosaServicio(datos[0].toString());
				movimiento.setBoletaMovimiento(Integer.parseInt(datos[1].toString()));
				movimiento.setMontoTransaccionBoleta(Double.parseDouble(datos[2].toString()));
				movimiento.setMontoCantidadBoleta(Double.parseDouble(datos[3].toString()));
				movimiento.setDiferenciaBoleta(Double.parseDouble(datos[4].toString()));
				movimiento.setFacturaMovimientos(Double.parseDouble(datos[5].toString()));
				movimiento.setMontoTransaccionFactura(Double.parseDouble(datos[6].toString()));
				movimiento.setMontoCantidadFactura(Double.parseDouble(datos[7].toString()));
				movimiento.setDiferenciaFactura(Double.parseDouble(datos[8].toString()));
				resultados.add(movimiento);
			}

			resumen.setMovimientos(resultados);
			resumen.setGlosaCC(storedProcedure.getOutputParameterValue("Fld_GlosaCCosto").toString());
			resumen.setTotalTransacciones(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalTransacciones").toString()));
			resumen.setTotalMovimientos(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalMovimientos").toString()));
			resumen.setTotalAviPago(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalAviPago").toString()));
			
			if(storedProcedure.getOutputParameterValue("Fld_TotalPagado") != null){
				resumen.setTotalPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			}
			else{
				resumen.setTotalPagado(0L);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia") != null){
				resumen.setTotalDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			}
			else{
				resumen.setTotalDiferencia(0L);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalBoletas") != null){
				resumen.setTotalBoletas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalBoletas").toString()));
			}
			else{
				resumen.setTotalBoletas(0);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalFacturas") != null){
				resumen.setTotalFacturas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalFacturas").toString()));
			}
			else{
				resumen.setTotalFacturas(0);
			}

			return resumen;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CuadMovimientos_SW2 " + e.getMessage());
			return null;
		}

	}

	public DetalleBoletaDTO obtieneDetalleBoleta(Date fechaInicio, String usuario, Integer corrCaja,Integer centroCosto, Integer criterio) {

		try {

			LOG.info("Ejecutando SQ_DetalleBoletas2_SW2");
			
			DetalleBoletaDTO detalleBoleta = new DetalleBoletaDTO();

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DetalleBoletas2_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicio", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalCalculado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalBoletas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalFacturas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(fechaInicio.getTime());
			
			storedProcedure.setParameter("Fld_FecInicio", desdeTime);
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			if(corrCaja == null || corrCaja == 0){
				storedProcedure.setParameter("Fld_CorrCaja", 0);
				storedProcedure.setParameter("Fld_Criterio", 1);
			}
			else{
				storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
				storedProcedure.setParameter("Fld_Criterio", 0);
			}
			storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			

			List<ItemBoletaDTO> resultados = new ArrayList<ItemBoletaDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			int i = 1;
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				ItemBoletaDTO itemBoleta = new ItemBoletaDTO();
				itemBoleta.setId(i);
				itemBoleta.setCorrCaja(Integer.parseInt(datos[0].toString()));
				itemBoleta.setTipoDocumento(datos[1].toString());
				itemBoleta.setEstado(datos[2].toString());
				itemBoleta.setNumBoletaFactura(Integer.parseInt(datos[3].toString()));
				itemBoleta.setTipoPago(datos[4].toString());
				itemBoleta.setNumeroCheque(datos[5].toString());
				itemBoleta.setMontoCobradoSistema(Double.parseDouble(datos[6].toString()));
				itemBoleta.setMontoIngresadoCajera(Double.parseDouble(datos[7].toString()));
				itemBoleta.setMontoDiferencia(Double.parseDouble(datos[8].toString()));
				if(datos[9] == null){
					itemBoleta.setRutAfectado("");
				}
				else{
					String rut = datos[9].toString();
					itemBoleta.setRutAfectado(rut);
				}
				if(datos[10] == null){
					itemBoleta.setRutTramitante("");
				}
				else{
					String rut = datos[10].toString();
					if(rut.equals("-0")){
						itemBoleta.setRutTramitante("");
					}
					else{
						itemBoleta.setRutTramitante(rut);
					}
				}
				resultados.add(itemBoleta);
				i++;
			}

			detalleBoleta.setItems(resultados);
			detalleBoleta.setTotalCalculado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalCalculado").toString()));
			detalleBoleta.setTotalPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			detalleBoleta.setTotalDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			detalleBoleta.setTotalBoletas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalBoletas").toString()));
			detalleBoleta.setTotalFacturas(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalFacturas").toString()));
			detalleBoleta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			detalleBoleta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return detalleBoleta;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_DetalleBoletas2_SW2 " + e.getMessage());
			return null;
		}
	}
	
	
	
	
	public DetalleServicioDTO obtieneDetalleServicio(Date fechaInicio, String usuario, Integer corrCaja,Integer centroCosto, Integer criterio, Integer ordenamiento) {

		try {

			
			LOG.info("Ejecutando SQ_DetalleServicios_SW2");
			
			DetalleServicioDTO detalleServicio = new DetalleServicioDTO();
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DetalleServicios_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicio", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Ordenamiento", Integer.class, ParameterMode.IN);			
			storedProcedure.registerStoredProcedureParameter("Fld_TotalCalculado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalDiferencia", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalTransacciones", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TotalMovimientos", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			Timestamp desdeTime = new Timestamp(fechaInicio.getTime());
			
			storedProcedure.setParameter("Fld_FecInicio", desdeTime);
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			storedProcedure.setParameter("Fld_CodCCosto", centroCosto);
			storedProcedure.setParameter("Fld_Criterio", criterio);
			storedProcedure.setParameter("Fld_Ordenamiento", ordenamiento);

			List<DetalleListaServicioDTO> resultados = new ArrayList<DetalleListaServicioDTO>();
			List<Object[]> detalles = storedProcedure.getResultList();
			int i = 1;
			for (Object item : detalles) {
				Object[] datos = (Object[]) item;
				DetalleListaServicioDTO detalle = new DetalleListaServicioDTO();
				
				detalle.setCorrCaja(Integer.parseInt(datos[0].toString()));
				detalle.setCorrMovimiento(Long.parseLong(datos[1].toString()));
				detalle.setBoletaFactura(datos[2].toString());
				detalle.setNroBoletaFactura(Integer.parseInt(datos[3].toString()));
				detalle.setGlosaServicio(datos[4].toString());
				detalle.setRutTramitante(datos[5].toString());
				detalle.setRutAfectado(datos[6].toString());
				detalle.setMontoMovimiento(Double.parseDouble(datos[7].toString()));
				detalle.setMontoCancelado(Double.parseDouble(datos[8].toString()));
				detalle.setMontoDiferencia(Double.parseDouble(datos[9].toString()));
				detalle.setTipoDocumento(datos[10].toString());
				detalle.setGlosaCorta(datos[11].toString());
				detalle.setMontoProt(Double.parseDouble(datos[12].toString()));
				if(datos[13] != null){
					if(datos[13].toString().substring(0,10).equals("1900-01-01")){
						detalle.setFechaProt("");
					}
					else{
						String[] fechaArray = datos[13].toString().substring(0,10).split("-");
						detalle.setFechaProt(fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0]);
					}
				}
				detalle.setNroOper4Dig(Integer.parseInt(datos[14].toString()));
				detalle.setGlosaEmisor(datos[15].toString());
				detalle.setCodServicio(datos[16].toString());
				resultados.add(detalle);
				i++;
			}

			detalleServicio.setServicios(resultados);
			
			
			if(storedProcedure.getOutputParameterValue("Fld_TotalCalculado") != null){
				detalleServicio.setTotalCalculado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalCalculado").toString()));
			}
			else{
				detalleServicio.setTotalCalculado(0L);
			}
			
			if(storedProcedure.getOutputParameterValue("Fld_TotalPagado") != null){
				detalleServicio.setTotalPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalPagado").toString()));
			}
			else{
				detalleServicio.setTotalPagado(0L);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia") != null){
				detalleServicio.setTotalDiferencia(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TotalDiferencia").toString()));
			}
			else{
				detalleServicio.setTotalDiferencia(0L);
			}
			
			if(storedProcedure.getOutputParameterValue("Fld_TotalTransacciones") != null){
				detalleServicio.setTotalTransacciones(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalTransacciones").toString()));
			}
			else{
				detalleServicio.setTotalTransacciones(0);
			}
			if(storedProcedure.getOutputParameterValue("Fld_TotalMovimientos") != null){
				detalleServicio.setTotalMovimientos(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TotalMovimientos").toString()));
			}
			else{
				detalleServicio.setTotalMovimientos(0);
			}
			detalleServicio.setRetorno(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			detalleServicio.setMensaje(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			
			
			
			return detalleServicio;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_DetalleServicios_SW2 " + e.getMessage());
			return null;
		}
	}

	
	
	
	public DetalleBoletaFacturaDTO obtieneDetalleBoletaFactura(DetalleBoletaFacturaDTO detalle){
		
		
		try{
			
			LOG.info("Ejecutando SQ_DatoTransac_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DatoTransac_SW2");
			
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecInicioCaja", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoPagado", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre", String.class, ParameterMode.OUT);

			
			
			storedProcedure.setParameter("Fld_CorrCaja", detalle.getCorrCaja());
			storedProcedure.setParameter("Fld_NroBoletaFactura", detalle.getNroBoletaFactura());
			
			storedProcedure.execute();

			detalle.setBoletaFactura(storedProcedure.getOutputParameterValue("Fld_BoletaFactura").toString());
			detalle.setCorrTransaccion(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CorrTransaccion").toString()));
			detalle.setCodCCosto(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodCCosto").toString()));
			
			String[] fechaArray = storedProcedure.getOutputParameterValue("Fld_FecInicioCaja").toString().substring(0,10).split("-");
			String fechaInicio = fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0];
	
			detalle.setFecInicioCaja(fechaInicio);
			detalle.setMontoPagado(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_MontoPagado").toString()));
			detalle.setNombre(storedProcedure.getOutputParameterValue("Fld_Nombre").toString());
			
			return detalle;
			
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_DatoTransac_SW2 " + e.getMessage());
			return null;
		}
		
		
	}
	
	
	
	public List<DatoBoletaFacturaDTO> getDetalleBoletaModal(DetalleBoletaFacturaDTO detalle){
		
		try{
			
			
			LOG.info("Ejecutando SQ_DatoBoleta_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DatoBoleta_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", Integer.class, ParameterMode.IN);
			
			storedProcedure.setParameter("Fld_CorrCaja", detalle.getCorrCaja());
			storedProcedure.setParameter("Fld_CorrTransaccion", detalle.getCorrTransaccion());
			
			storedProcedure.execute();
			
			List<DatoBoletaFacturaDTO> boletas = new ArrayList<DatoBoletaFacturaDTO>();
			List<Object[]> resultados = storedProcedure.getResultList();
			
			for (Object item : resultados) {
				Object[] datos = (Object[]) item;
				DatoBoletaFacturaDTO boletaServicios = new DatoBoletaFacturaDTO();
				boletaServicios.setCantidad(Integer.parseInt(datos[0].toString()));
				boletaServicios.setGlosaServicio(datos[1].toString());
				boletaServicios.setTotalCalculado(Double.parseDouble(datos[2].toString()));
				boletas.add(boletaServicios);
			}
			
			return boletas;
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_DatoBoleta_SW2 " + e.getMessage());
			return null;
		}		
	}
	
	
	public DetalleBoletaFacturaDTO getDetalleFacturaModal(DetalleBoletaFacturaDTO detalle){
		
		try{
			
			LOG.info("Ejecutando SQ_DatoFactura_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DatoFactura_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RazonSocial", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Ubicacion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoNeto", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoIva", Long.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", detalle.getCorrCaja());
			storedProcedure.setParameter("Fld_CorrTransaccion", detalle.getCorrTransaccion());
			storedProcedure.setParameter("Fld_CodCCosto", detalle.getCodCCosto());
			storedProcedure.setParameter("Fld_NroBoletaFactura", detalle.getNroBoletaFactura());
			
			
			storedProcedure.execute();
			
			List<DatoBoletaFacturaDTO> servFacturas = new ArrayList<DatoBoletaFacturaDTO>();
			List<Object[]> resultados = storedProcedure.getResultList();
			
			for (Object item : resultados) {
				Object[] datos = (Object[]) item;
				DatoBoletaFacturaDTO factServicios = new DatoBoletaFacturaDTO();
				factServicios.setCodigo(Integer.parseInt(datos[0].toString()));
				factServicios.setCantidad(Integer.parseInt(datos[1].toString()));
				factServicios.setGlosaServicio(datos[2].toString());
				factServicios.setTotalCalculado(Double.parseDouble(datos[3].toString()));
				servFacturas.add(factServicios);
			}
			detalle.setDatosBoletaFactura(servFacturas);
			detalle.setDetalleFactura(new DetalleFacturaDTO());
			detalle.getDetalleFactura().setRazonSocial(storedProcedure.getOutputParameterValue("Fld_RazonSocial").toString());
			detalle.getDetalleFactura().setFechaEmision(storedProcedure.getOutputParameterValue("Fld_FecEmision").toString());
			detalle.getDetalleFactura().setIdGiro(storedProcedure.getOutputParameterValue("Fld_Giro").toString());
			detalle.getDetalleFactura().setUbicacion(storedProcedure.getOutputParameterValue("Fld_Ubicacion").toString());
			detalle.getDetalleFactura().setRutAfectado(storedProcedure.getOutputParameterValue("Fld_RutAfectado").toString());
			detalle.getDetalleFactura().setCodOrigen(storedProcedure.getOutputParameterValue("Fld_CodOrigen").toString());
			detalle.getDetalleFactura().setMontoNeto(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_MontoNeto").toString()));
			detalle.getDetalleFactura().setMontoIva(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_MontoIva").toString()));
			
			return detalle;
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_DatoFactura_SW2 " + e.getMessage());
			return null;
		}		
	}
	
	
	public CuadraturaAclaracionDTO validaAclaraciones(Integer corrCaja) {

		CuadraturaAclaracionDTO validacionAclaracion = new CuadraturaAclaracionDTO();
		
		try {

			LOG.info("Ejecutando SQ_ValidadorAcls_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValidadorAcls_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroAcls", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroAvisos", Integer.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			
			storedProcedure.execute();
			
			List<DetalleAclaracionDTO> aclaraciones = new ArrayList<DetalleAclaracionDTO>();
			List<Object[]> resultados = storedProcedure.getResultList();
			
			for (Object item : resultados) {
				Object[] datos = (Object[]) item;
				DetalleAclaracionDTO aclaracion = new DetalleAclaracionDTO();
				aclaracion.setCorrAcl(Long.parseLong(datos[0].toString()));
				aclaracion.setTitular(datos[1].toString());
				aclaracion.setCorrProt(Long.parseLong(datos[2].toString()));
				aclaracion.setCodServicio(datos[3].toString());
				aclaracion.setGlosaEmisor(datos[4].toString());
				aclaracion.setRutAfectado(datos[5].toString());
				aclaracion.setNombre(datos[6].toString());
				aclaracion.setTipoDocAclaracion(datos[7].toString());
				aclaracion.setTipoDocumento(datos[8].toString());
				aclaracion.setGlosaCorta(datos[9].toString());
				aclaracion.setMontoProt(Double.parseDouble(datos[10].toString()));
				
				String[] fechaArray = datos[11].toString().substring(0,10).split("-");
				aclaracion.setFechaProt(fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0]);
				
				
				fechaArray = datos[12].toString().substring(0,10).split("-");
				if((fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0]).equals("01-01-1900")){
					aclaracion.setFechaPago("");
				}
				else{
					aclaracion.setFechaPago(fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0]);
				}
				
				aclaracion.setNroOper4Dig(Integer.parseInt(datos[13].toString()));
				aclaracion.setPtjPareo(Double.parseDouble(datos[14].toString()));
				aclaracion.setMontoMovimiento(Double.valueOf(datos[15].toString()).longValue());
				aclaracion.setMontoDiferencia(Double.valueOf(datos[16].toString()).longValue());
				aclaracion.setBoletaFactura(datos[17].toString());
				aclaracion.setNumeroBoletaFactura(Integer.parseInt(datos[18].toString()));
				aclaraciones.add(aclaracion);
			}
			
			validacionAclaracion.setDetalleAclaracion(aclaraciones);
			validacionAclaracion.setNumeroAclaraciones(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroAcls").toString()));
			validacionAclaracion.setNumeroAvisos(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroAvisos").toString()));
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_ValidadorAcls_SW2 " + e.getMessage());
			return null;
		}
		return validacionAclaracion;

	}
	
	public ReclamoDTO solicitudesReclamos(Integer corrCaja) {
		
		
		ReclamoDTO reclamoDTO = new ReclamoDTO();
		
		try {
			
			LOG.info("Ejecutando SQ_ValSolRec_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValSolRec_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			
			storedProcedure.execute();			
			
			List<SolicitudReclamoDTO> reclamos = new ArrayList<SolicitudReclamoDTO>();
			List<Object[]> resultados = storedProcedure.getResultList();
			
			for (Object item : resultados) {
				Object[] datos = (Object[]) item;
				SolicitudReclamoDTO solRecl = new SolicitudReclamoDTO();
				solRecl.setCorrSolicitudReclamo(Integer.parseInt(datos[0].toString()));
				solRecl.setGlosaTipoSolicitud(datos[1].toString());
				solRecl.setGlosaCategoria(datos[2].toString());
				solRecl.setRutTitular(datos[3].toString());
				solRecl.setNombreApePaterno(datos[4].toString().trim());
				solRecl.setNombreApeMaterno(datos[5].toString().trim());
				solRecl.setNombre(datos[6].toString().trim());
				solRecl.setGlosaEstado(datos[7].toString());
				
				String[] fechaEstadoArray = datos[8].toString().substring(0,10).split("-");
				solRecl.setFechaEstado(fechaEstadoArray[2]+"-"+fechaEstadoArray[1]+"-"+fechaEstadoArray[0]);
				
				solRecl.setUsuarioEstado(datos[9].toString());
				solRecl.setCantDoc(Integer.parseInt(datos[10].toString()));
				
				String[] fechaSolicitudArray = datos[11].toString().substring(0,10).split("-");
				solRecl.setFechaSolicitud(fechaSolicitudArray[2]+"-"+fechaSolicitudArray[1]+"-"+fechaSolicitudArray[0]);
				
				solRecl.setTelefono(datos[12].toString());
				solRecl.setTexto(datos[13].toString());
				reclamos.add(solRecl);
			}
			reclamoDTO.setSolReclamos(reclamos);
			
			return reclamoDTO;
		}
		catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en el SP SQ_ValSolRec_SW2 " + e.getMessage());
			return null;
		}
		
	}

}
