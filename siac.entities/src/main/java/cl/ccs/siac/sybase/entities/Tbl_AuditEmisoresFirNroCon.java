package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditEmisoresFirNroCon database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditEmisoresFirNroCon.findAll", query="SELECT t FROM Tbl_AuditEmisoresFirNroCon t")
public class Tbl_AuditEmisoresFirNroCon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecEliminacion")
	private Timestamp fld_FecEliminacion;

	@Column(name="Fld_FlagFirmaNroConf")
	private boolean fld_FlagFirmaNroConf;

	@Column(name="Fld_FlagUsoOpcional")
	private boolean fld_FlagUsoOpcional;

	@Column(name="Fld_NroFirmas")
	private byte fld_NroFirmas;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_UsaNrosConfirmatorios")
	private boolean fld_UsaNrosConfirmatorios;

	@Column(name="Fld_UsrCreacion")
	private String fld_UsrCreacion;

	@Column(name="Fld_UsrEliminacion")
	private String fld_UsrEliminacion;

	public Tbl_AuditEmisoresFirNroCon() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecEliminacion() {
		return this.fld_FecEliminacion;
	}

	public void setFld_FecEliminacion(Timestamp fld_FecEliminacion) {
		this.fld_FecEliminacion = fld_FecEliminacion;
	}

	public boolean getFld_FlagFirmaNroConf() {
		return this.fld_FlagFirmaNroConf;
	}

	public void setFld_FlagFirmaNroConf(boolean fld_FlagFirmaNroConf) {
		this.fld_FlagFirmaNroConf = fld_FlagFirmaNroConf;
	}

	public boolean getFld_FlagUsoOpcional() {
		return this.fld_FlagUsoOpcional;
	}

	public void setFld_FlagUsoOpcional(boolean fld_FlagUsoOpcional) {
		this.fld_FlagUsoOpcional = fld_FlagUsoOpcional;
	}

	public byte getFld_NroFirmas() {
		return this.fld_NroFirmas;
	}

	public void setFld_NroFirmas(byte fld_NroFirmas) {
		this.fld_NroFirmas = fld_NroFirmas;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public boolean getFld_UsaNrosConfirmatorios() {
		return this.fld_UsaNrosConfirmatorios;
	}

	public void setFld_UsaNrosConfirmatorios(boolean fld_UsaNrosConfirmatorios) {
		this.fld_UsaNrosConfirmatorios = fld_UsaNrosConfirmatorios;
	}

	public String getFld_UsrCreacion() {
		return this.fld_UsrCreacion;
	}

	public void setFld_UsrCreacion(String fld_UsrCreacion) {
		this.fld_UsrCreacion = fld_UsrCreacion;
	}

	public String getFld_UsrEliminacion() {
		return this.fld_UsrEliminacion;
	}

	public void setFld_UsrEliminacion(String fld_UsrEliminacion) {
		this.fld_UsrEliminacion = fld_UsrEliminacion;
	}

}