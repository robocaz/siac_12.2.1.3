package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tp_CeIdSucBancos database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdSucBancos")
@NamedQuery(name="Tp_CeIdSucBanco.findAll", query="SELECT t FROM Tp_CeIdSucBanco t")
public class Tp_CeIdSucBanco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoBanco")
	private int fld_CodigoBanco;

	@Column(name="Fld_CodSucBco")
	private int fld_CodSucBco;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	public Tp_CeIdSucBanco() {
	}

	public int getFld_CodigoBanco() {
		return this.fld_CodigoBanco;
	}

	public void setFld_CodigoBanco(int fld_CodigoBanco) {
		this.fld_CodigoBanco = fld_CodigoBanco;
	}

	public int getFld_CodSucBco() {
		return this.fld_CodSucBco;
	}

	public void setFld_CodSucBco(int fld_CodSucBco) {
		this.fld_CodSucBco = fld_CodSucBco;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

}