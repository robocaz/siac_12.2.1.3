package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdEstEnvios database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdEstEnvios")
@NamedQuery(name="Tp_CeIdEstEnvio.findAll", query="SELECT t FROM Tp_CeIdEstEnvio t")
public class Tp_CeIdEstEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_GlosaEstado")
	private String fld_GlosaEstado;

	public Tp_CeIdEstEnvio() {
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_GlosaEstado() {
		return this.fld_GlosaEstado;
	}

	public void setFld_GlosaEstado(String fld_GlosaEstado) {
		this.fld_GlosaEstado = fld_GlosaEstado;
	}

}