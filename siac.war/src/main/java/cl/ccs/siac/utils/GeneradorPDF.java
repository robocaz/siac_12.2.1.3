package cl.ccs.siac.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.exceptions.ReportNotCreatedException;
import cl.ccs.siac.exceptions.ReportNotFoundException;
import cl.ccs.siac.servicios.PagoServicio;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRExpressionEvalException;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class GeneradorPDF implements GeneradorReporte {

	public static String RUTA = "wlscomun/Certificados/";

	private static final String REPORTE_NO_ENCONTRADO = "Reporte con ruta '%s' no ha sido encontrado";
	private static final String REPORTE_NO_CONSTRUIDO = "Error en la creación del reporte con ruta '%s'. Error: %s";

	private static final String CONTENT_TYPE = "application/pdf";
	private static final String CONTENT_DISPOSITION = "attachment;filename=%s.pdf";
	private static final Logger LOG = LogManager.getLogger(GeneradorPDF.class);

	/**
	 * Constructor.
	 */
	public GeneradorPDF() {
		super();
	}

	public byte[] create(final String plantilla, final Collection<?> data)
			throws ReportNotFoundException, ReportNotCreatedException {
		return this.create(plantilla, data, new HashMap<String, Object>());
	}

	public byte[] create(final String plantilla, final Collection<?> data, final Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException {

		byte[] report = null;
		JRDataSource datos = null;

		// datos = new JRBeanCollectionDataSource(data);

		// TODO Obtener la ruta de otra parte
		final String ruta = RUTA + plantilla + ".jasper";
		LOG.info("ruta: "+ruta);
		// final InputStream stream =
		// GeneradorPDF.class.getResourceAsStream(ruta);
		final File templateFile = new File(ruta);
		InputStream stream = null;
		try {
			stream = new FileInputStream(templateFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (stream == null) {
			throw new ReportNotFoundException(String.format(REPORTE_NO_ENCONTRADO, ruta));
		}

		try {
			
			if(data.size() == 0){
				report = JasperRunManager.runReportToPdf(stream, parametros, new JREmptyDataSource());
			}
			else{
				report = JasperRunManager.runReportToPdf(stream, parametros, new JRBeanCollectionDataSource(data));
			}
			// report = JasperRunManager.runReportToPdf(stream, parametros,
			// datos);

		} catch (JRExpressionEvalException e) {
			throw new ReportNotCreatedException(String.format(REPORTE_NO_CONSTRUIDO, ruta, e.getMessage()), e);
		}
		catch (JRException e) {
			throw new ReportNotCreatedException(String.format(REPORTE_NO_CONSTRUIDO, ruta, e.getMessage()), e);
		}
		LOG.info("se retorna el jasper");
		return report;
	}

	public Response createReportResponse(final String plantilla, final Collection<?> data,
			final Map<String, Object> parametros) throws ReportNotFoundException, ReportNotCreatedException {
		final byte[] fichero = this.create(plantilla, data, parametros);

		final ResponseBuilder builder = Response.ok(fichero, CONTENT_TYPE);
		builder.header("Content-Disposition", String.format(CONTENT_DISPOSITION, plantilla));
		return builder.build();

	}

	public Response createReportResponse(final String plantilla, final Collection<?> data)
			throws ReportNotFoundException, ReportNotCreatedException {
		return this.createReportResponse(plantilla, data, new HashMap<String, Object>());
	}

	public byte[] create(final String plantilla, final Map<String, Object> parametros, final Connection connection)
			throws ReportNotFoundException, ReportNotCreatedException {

		byte[] report = null;

		// TODO Obtener la ruta de otra parte
		final String ruta = "/reportes/" + plantilla + ".jasper";

		final InputStream stream = GeneradorPDF.class.getResourceAsStream(ruta);
		if (stream == null) {
			throw new ReportNotFoundException(String.format(REPORTE_NO_ENCONTRADO, ruta));
		}

		try {
			report = JasperRunManager.runReportToPdf(stream, parametros, connection);

		} catch (JRException e) {
			throw new ReportNotCreatedException(String.format(REPORTE_NO_CONSTRUIDO, ruta, e.getMessage()), e);
		}

		return report;
	}

	public Response createReportResponseBlock(final String plantilla, final Collection<?> data)
			throws ReportNotFoundException, ReportNotCreatedException, JRException {
		return this.createReportResponseBlock(plantilla, data, new HashMap<String, Object>());
	}

	public Response createReportResponseBlock(final String plantilla, final Collection<?> data,
			final Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException, JRException {
		final byte[] fichero = this.createBlock(plantilla, data, parametros);

		final ResponseBuilder builder = Response.ok(fichero, CONTENT_TYPE);
		builder.header("Content-Disposition", String.format(CONTENT_DISPOSITION, plantilla));
		return builder.build();

	}

	/*
	 * En caso de que se quiera restringir la visualizacion dl PDF con alguna
	 * password, se debe agregar las siguientes propiedades:
	 * exporterPDF.setParameter(JRPdfExporterParameter.IS_128_BIT_KEY,
	 * Boolean.TRUE);
	 * exporterPDF.setParameter(JRPdfExporterParameter.USER_PASSWORD, "jasper");
	 * exporterPDF.setParameter(JRPdfExporterParameter.OWNER_PASSWORD,
	 * "reports");
	 *
	 * En caso de querer permitir la impresion del PDF se debe agregar la
	 * propiedad: exporterPDF.setParameter(JRPdfExporterParameter.PERMISSIONS,
	 * new Integer(PdfWriter.ALLOW_PRINTING));
	 * 
	 * En caso de querer permitir copiar el PDF se debe agregar la propiedad:
	 * exporterPDF.setParameter(JRPdfExporterParameter.PERMISSIONS, new
	 * Integer(PdfWriter.ALLOW_COPY));
	 * 
	 * En caso de querer ambas opciones, se debe agregar de esta forma:
	 * exporterPDF.setParameter(JRPdfExporterParameter.PERMISSIONS, new
	 * Integer(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING) );
	 * 
	 */
	public byte[] createBlock(final String plantilla, final Collection<?> data, final Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException, JRException {

		byte[] report = null;
		JRDataSource datos = null;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		datos = new JRBeanCollectionDataSource(data);
		final String ruta = RUTA + plantilla + ".jasper";

		final InputStream stream = GeneradorPDF.class.getResourceAsStream(ruta);
		if (stream == null) {
			throw new ReportNotFoundException(String.format(REPORTE_NO_ENCONTRADO, ruta));
		}

		JasperPrint print = new JasperPrint();
		try {
			print = JasperFillManager.fillReport(stream, parametros, datos);
		} catch (JRException e) {
			throw new ReportNotCreatedException(String.format(REPORTE_NO_CONSTRUIDO, ruta, e.getMessage()), e);
		}
		final JRPdfExporter exporterPDF = new JRPdfExporter();
		exporterPDF.setExporterInput(new SimpleExporterInput(print));
		exporterPDF.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
		SimplePdfExporterConfiguration configuracion = new SimplePdfExporterConfiguration();
		configuracion.setEncrypted(Boolean.TRUE);
		exporterPDF.setConfiguration(configuracion);

		JasperPrintManager.printReport(print, false);
		
		try {
			exporterPDF.exportReport();
		} catch (JRException e) {
			throw new ReportNotCreatedException(String.format(REPORTE_NO_CONSTRUIDO, ruta), e);
		}

		report = byteArrayOutputStream.toByteArray();

		return report;
	}

}
