package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MonedaDTO extends ResponseDTO implements Serializable{
    private Integer codMoneda;
    private String glosaMoneda;
    private String glosaCorta;
    
    public MonedaDTO(){
    	
    }

	public Integer getCodMoneda() {
		return codMoneda;
	}

	public void setCodMoneda(Integer codMoneda) {
		this.codMoneda = codMoneda;
	}

	public String getGlosaMoneda() {
		return glosaMoneda;
	}

	public void setGlosaMoneda(String glosaMoneda) {
		this.glosaMoneda = glosaMoneda;
	}

	public String getGlosaCorta() {
		return glosaCorta;
	}

	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
    



}
