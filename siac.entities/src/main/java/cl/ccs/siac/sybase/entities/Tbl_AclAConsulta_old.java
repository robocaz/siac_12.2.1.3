package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AclAConsulta_old database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclAConsulta_old.findAll", query="SELECT t FROM Tbl_AclAConsulta_old t")
public class Tbl_AclAConsulta_old implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrAConsulta_Id")
	private BigDecimal fld_CorrAConsulta_Id;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FiguraBic")
	private String fld_FiguraBic;

	@Column(name="Fld_GlosaConsulta")
	private String fld_GlosaConsulta;

	@Column(name="Fld_NroDocs")
	private byte fld_NroDocs;

	@Column(name="Fld_ProcAnterior")
	private short fld_ProcAnterior;

	@Column(name="Fld_TipoAclCons")
	private String fld_TipoAclCons;

	public Tbl_AclAConsulta_old() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrAConsulta_Id() {
		return this.fld_CorrAConsulta_Id;
	}

	public void setFld_CorrAConsulta_Id(BigDecimal fld_CorrAConsulta_Id) {
		this.fld_CorrAConsulta_Id = fld_CorrAConsulta_Id;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public String getFld_FiguraBic() {
		return this.fld_FiguraBic;
	}

	public void setFld_FiguraBic(String fld_FiguraBic) {
		this.fld_FiguraBic = fld_FiguraBic;
	}

	public String getFld_GlosaConsulta() {
		return this.fld_GlosaConsulta;
	}

	public void setFld_GlosaConsulta(String fld_GlosaConsulta) {
		this.fld_GlosaConsulta = fld_GlosaConsulta;
	}

	public byte getFld_NroDocs() {
		return this.fld_NroDocs;
	}

	public void setFld_NroDocs(byte fld_NroDocs) {
		this.fld_NroDocs = fld_NroDocs;
	}

	public short getFld_ProcAnterior() {
		return this.fld_ProcAnterior;
	}

	public void setFld_ProcAnterior(short fld_ProcAnterior) {
		this.fld_ProcAnterior = fld_ProcAnterior;
	}

	public String getFld_TipoAclCons() {
		return this.fld_TipoAclCons;
	}

	public void setFld_TipoAclCons(String fld_TipoAclCons) {
		this.fld_TipoAclCons = fld_TipoAclCons;
	}

}