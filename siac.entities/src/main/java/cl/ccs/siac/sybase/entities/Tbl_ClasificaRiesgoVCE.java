package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ClasificaRiesgoVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ClasificaRiesgoVCE.findAll", query="SELECT t FROM Tbl_ClasificaRiesgoVCE t")
public class Tbl_ClasificaRiesgoVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoRiesgo")
	private String fld_TipoRiesgo;

	public Tbl_ClasificaRiesgoVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoRiesgo() {
		return this.fld_TipoRiesgo;
	}

	public void setFld_TipoRiesgo(String fld_TipoRiesgo) {
		this.fld_TipoRiesgo = fld_TipoRiesgo;
	}

}