package cl.ccs.siac.dao;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface CentroCostoDao {
	
	public ResponseListDTO<ComboDTO> listar(String username) throws Exception ;
	public ResponseDTO modificar(String username, String codigoCentroCosto) throws Exception;
	
}
