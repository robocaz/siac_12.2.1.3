angular.module('ccs').factory('PagosResource', function ($resource) {
    var resource = $resource('rest/pagos',{
        nocache : new Date().getTime()
    },{
        'cargaBancos' :  {  method : 'GET', isArray : true, url: 'rest/pagos/combobancos'},
        'cargaSucursales' :  {  method : 'POST', isArray : true, url: 'rest/pagos/combosucursales'},
        'imprimeBoleta' :  {  method : 'POST', isArray : false, url: 'rest/pagos/imprimirboleta'},
        'reimprimirBoleta' :  {  method : 'POST', isArray : false, url: 'rest/pagos/reimprimirboleta'},
        'anularBoleta' :  {  method : 'POST', isArray : false, url: 'rest/pagos/anularboleta'},
        'actualizaBoleta' :  {  method : 'POST', isArray : false, url: 'rest/pagos/actboletarango'},
        'aceptaTransaccion' :  {  method : 'POST', isArray : false, url: 'rest/pagos/aceptatransaccion'},
        'cancelaTransaccion' :  {  method : 'POST', isArray : false, url: 'rest/pagos/eliminatransaccion'},
        'actualizaTransaccion' :  {  method : 'POST', isArray : false, url: 'rest/pagos/actualizar'},
        'descargaCertificado' :  {  method : 'GET', isArray : false, url: 'rest/pagos/descargacertificado'},
        'imprimeBoletarLocal' :  {  method : 'GET', isArray : false, url: 'http://localhost:8080/terminal/siac/impresion'},
        'valorServicio' :  {  method : 'POST', isArray : false, url: 'rest/pagos/valorServicio'},
        'imprimeReportes' :  {  method : 'POST', isArray : false, url: 'http://localhost:8080/terminal/siac/imprimereportearreglo'}
        
        
    });
    return resource;
});