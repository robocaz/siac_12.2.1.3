package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_LGCONTROLCONSULTAS database table.
 * 
 */
@Entity
@Table(name="TBL_LGCONTROLCONSULTAS")
@NamedQuery(name="TblLgcontrolconsulta.findAll", query="SELECT t FROM TblLgcontrolconsulta t")
public class TblLgcontrolconsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODLOG")
	private BigDecimal fldCodlog;

	@Column(name="FLD_FECHAEXTRACCION")
	private Timestamp fldFechaextraccion;

	@Column(name="FLD_REGISTROSEXTRACTADOS")
	private BigDecimal fldRegistrosextractados;

	@Column(name="FLD_ULTIMOCORRELATIVO")
	private BigDecimal fldUltimocorrelativo;

	public TblLgcontrolconsulta() {
	}

	public BigDecimal getFldCodlog() {
		return this.fldCodlog;
	}

	public void setFldCodlog(BigDecimal fldCodlog) {
		this.fldCodlog = fldCodlog;
	}

	public Timestamp getFldFechaextraccion() {
		return this.fldFechaextraccion;
	}

	public void setFldFechaextraccion(Timestamp fldFechaextraccion) {
		this.fldFechaextraccion = fldFechaextraccion;
	}

	public BigDecimal getFldRegistrosextractados() {
		return this.fldRegistrosextractados;
	}

	public void setFldRegistrosextractados(BigDecimal fldRegistrosextractados) {
		this.fldRegistrosextractados = fldRegistrosextractados;
	}

	public BigDecimal getFldUltimocorrelativo() {
		return this.fldUltimocorrelativo;
	}

	public void setFldUltimocorrelativo(BigDecimal fldUltimocorrelativo) {
		this.fldUltimocorrelativo = fldUltimocorrelativo;
	}

}