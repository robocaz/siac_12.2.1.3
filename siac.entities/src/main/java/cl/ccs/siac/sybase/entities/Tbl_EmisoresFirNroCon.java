package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EmisoresFirNroCon database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EmisoresFirNroCon.findAll", query="SELECT t FROM Tbl_EmisoresFirNroCon t")
public class Tbl_EmisoresFirNroCon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_FlagFirmaNroConf")
	private boolean fld_FlagFirmaNroConf;

	@Column(name="Fld_FlagUsoOpcional")
	private boolean fld_FlagUsoOpcional;

	@Column(name="Fld_NroFirmas")
	private byte fld_NroFirmas;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_UsaNrosConfirmatorios")
	private boolean fld_UsaNrosConfirmatorios;

	public Tbl_EmisoresFirNroCon() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public boolean getFld_FlagFirmaNroConf() {
		return this.fld_FlagFirmaNroConf;
	}

	public void setFld_FlagFirmaNroConf(boolean fld_FlagFirmaNroConf) {
		this.fld_FlagFirmaNroConf = fld_FlagFirmaNroConf;
	}

	public boolean getFld_FlagUsoOpcional() {
		return this.fld_FlagUsoOpcional;
	}

	public void setFld_FlagUsoOpcional(boolean fld_FlagUsoOpcional) {
		this.fld_FlagUsoOpcional = fld_FlagUsoOpcional;
	}

	public byte getFld_NroFirmas() {
		return this.fld_NroFirmas;
	}

	public void setFld_NroFirmas(byte fld_NroFirmas) {
		this.fld_NroFirmas = fld_NroFirmas;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public boolean getFld_UsaNrosConfirmatorios() {
		return this.fld_UsaNrosConfirmatorios;
	}

	public void setFld_UsaNrosConfirmatorios(boolean fld_UsaNrosConfirmatorios) {
		this.fld_UsaNrosConfirmatorios = fld_UsaNrosConfirmatorios;
	}

}