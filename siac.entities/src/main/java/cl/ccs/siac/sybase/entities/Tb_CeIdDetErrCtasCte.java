package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tb_CeIdDetErrCtasCtes database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetErrCtasCtes")
@NamedQuery(name="Tb_CeIdDetErrCtasCte.findAll", query="SELECT t FROM Tb_CeIdDetErrCtasCte t")
public class Tb_CeIdDetErrCtasCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	//bi-directional many-to-one association to Tb_CeIdDetCtaCte
	@ManyToOne
	@JoinColumn(name="Fld_CorrCtaCte")
	private Tb_CeIdDetCtaCte tbCeIdDetCtaCte;

	public Tb_CeIdDetErrCtasCte() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public Tb_CeIdDetCtaCte getTbCeIdDetCtaCte() {
		return this.tbCeIdDetCtaCte;
	}

	public void setTbCeIdDetCtaCte(Tb_CeIdDetCtaCte tbCeIdDetCtaCte) {
		this.tbCeIdDetCtaCte = tbCeIdDetCtaCte;
	}

}