package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoSolic database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoSolic.findAll", query="SELECT t FROM Tbl_TipoSolic t")
public class Tbl_TipoSolic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaSolic")
	private String fld_GlosaSolic;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_TipoSolic() {
	}

	public String getFld_GlosaSolic() {
		return this.fld_GlosaSolic;
	}

	public void setFld_GlosaSolic(String fld_GlosaSolic) {
		this.fld_GlosaSolic = fld_GlosaSolic;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}