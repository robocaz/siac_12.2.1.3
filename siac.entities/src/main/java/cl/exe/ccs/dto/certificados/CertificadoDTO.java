package cl.exe.ccs.dto.certificados;

import javax.xml.bind.annotation.XmlRootElement;

import cl.exe.ccs.dto.ResponseDTO;

@XmlRootElement
public class CertificadoDTO extends ResponseDTO {

	private static final long serialVersionUID = 4426795140253040059L;
	private String codServicio;
	private String RutAfectado;
	private Integer tipoConsulta;
	private Long corrMovimiento;
	private Long corrTransaccion;
	private String tipoDocumento;
	private Long numeroBoleta;
	private String glosaServicio;
	private String rutaPDF;
	private byte[] reporte;
	private Integer flagImprime;

	public String getCodServicio() {
		return codServicio;
	}

	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}

	public String getRutAfectado() {
		return RutAfectado;
	}

	public void setRutAfectado(String rutAfectado) {
		RutAfectado = rutAfectado;
	}

	public Integer getTipoConsulta() { return tipoConsulta; }

	public void setTipoConsulta(Integer tipoConsulta) { this.tipoConsulta = tipoConsulta; }

	public Long getCorrMovimiento() {
		return corrMovimiento;
	}

	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}

	public Long getCorrTransaccion() {
		return corrTransaccion;
	}

	public void setCorrTransaccion(Long corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Long getNumeroBoleta() {
		return numeroBoleta;
	}

	public void setNumeroBoleta(Long numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}

	public String getGlosaServicio() {
		return glosaServicio;
	}

	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}

	public String getRutaPDF() {
		return rutaPDF;
	}

	public void setRutaPDF(String rutaPDF) {
		this.rutaPDF = rutaPDF;
	}

	public byte[] getReporte() {
		return reporte;
	}

	public void setReporte(byte[] reporte) {
		this.reporte = reporte;
	}

	public Integer getFlagImprime() {
		return flagImprime;
	}

	public void setFlagImprime(Integer flagImprime) {
		this.flagImprime = flagImprime;
	}

}
