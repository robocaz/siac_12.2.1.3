package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalFiniquito database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalFiniquito.findAll", query="SELECT t FROM Tbl_CausalFiniquito t")
public class Tbl_CausalFiniquito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Articulo")
	private byte fld_Articulo;

	@Column(name="Fld_CausalFiniquito")
	private String fld_CausalFiniquito;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	public Tbl_CausalFiniquito() {
	}

	public byte getFld_Articulo() {
		return this.fld_Articulo;
	}

	public void setFld_Articulo(byte fld_Articulo) {
		this.fld_Articulo = fld_Articulo;
	}

	public String getFld_CausalFiniquito() {
		return this.fld_CausalFiniquito;
	}

	public void setFld_CausalFiniquito(String fld_CausalFiniquito) {
		this.fld_CausalFiniquito = fld_CausalFiniquito;
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

}