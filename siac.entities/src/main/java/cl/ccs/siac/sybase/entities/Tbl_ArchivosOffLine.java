package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ArchivosOffLine database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ArchivosOffLine.findAll", query="SELECT t FROM Tbl_ArchivosOffLine t")
public class Tbl_ArchivosOffLine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FlagProceso")
	private int fld_FlagProceso;

	@Column(name="Fld_NomArchivo")
	private String fld_NomArchivo;

	public Tbl_ArchivosOffLine() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public int getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(int fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

	public String getFld_NomArchivo() {
		return this.fld_NomArchivo;
	}

	public void setFld_NomArchivo(String fld_NomArchivo) {
		this.fld_NomArchivo = fld_NomArchivo;
	}

}