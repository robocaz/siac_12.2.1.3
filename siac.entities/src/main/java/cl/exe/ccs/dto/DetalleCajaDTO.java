package cl.exe.ccs.dto;

import java.io.Serializable;

public class DetalleCajaDTO implements Serializable{
	
	
	private String usuario;
	private Integer numeroCaja;
	private String fechaInicio;
	private String estadoCaja;
	private Long montoCalculaBF;
	private Long montoCalculaDet;
	private Long montoPagado;
	private Long diferenciaBF;
	private Long diferenciaDetalle;
	private String centroCosto;
	
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(Integer numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getEstadoCaja() {
		return estadoCaja;
	}
	public void setEstadoCaja(String estadoCaja) {
		this.estadoCaja = estadoCaja;
	}
	public Long getMontoCalculaBF() {
		return montoCalculaBF;
	}
	public void setMontoCalculaBF(Long montoCalculaBF) {
		this.montoCalculaBF = montoCalculaBF;
	}
	public Long getMontoCalculaDet() {
		return montoCalculaDet;
	}
	public void setMontoCalculaDet(Long montoCalculaDet) {
		this.montoCalculaDet = montoCalculaDet;
	}
	public Long getMontoPagado() {
		return montoPagado;
	}
	public void setMontoPagado(Long montoPagado) {
		this.montoPagado = montoPagado;
	}
	public Long getDiferenciaBF() {
		return diferenciaBF;
	}
	public void setDiferenciaBF(Long diferenciaBF) {
		this.diferenciaBF = diferenciaBF;
	}
	public Long getDiferenciaDetalle() {
		return diferenciaDetalle;
	}
	public void setDiferenciaDetalle(Long diferenciaDetalle) {
		this.diferenciaDetalle = diferenciaDetalle;
	}
	public String getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}
	
	
	

}
