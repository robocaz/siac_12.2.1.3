package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ArqueoCajaVtasConDctoOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 3926269406186294156L;

	private List<ArqueoCajaConDocsData> lista;
	private Integer montoTotal;

	public List<ArqueoCajaConDocsData> getLista() {
		return lista;
	}
	public void setLista(List<ArqueoCajaConDocsData> lista) {
		this.lista = lista;
	}
	public Integer getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaVtasConDctoOutDTO [lista=");
		builder.append(lista);
		builder.append(", montoTotal=");
		builder.append(montoTotal);
		builder.append(", msgControl=");
		builder.append(msgControl);
		builder.append(", idControl=");
		builder.append(idControl);
		builder.append("]");
		return builder.toString();
	}

}
