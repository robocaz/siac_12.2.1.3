package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ley_StockRut database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_StockRut.findAll", query="SELECT l FROM Ley_StockRut l")
public class Ley_StockRut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Ley_StockRut() {
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}