package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdTipoCalles database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdTipoCalles")
@NamedQuery(name="Tp_CeIdTipoCalle.findAll", query="SELECT t FROM Tp_CeIdTipoCalle t")
public class Tp_CeIdTipoCalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoCalle")
	private String fld_GlosaTipoCalle;

	@Column(name="Fld_TipoCalle")
	private byte fld_TipoCalle;

	public Tp_CeIdTipoCalle() {
	}

	public String getFld_GlosaTipoCalle() {
		return this.fld_GlosaTipoCalle;
	}

	public void setFld_GlosaTipoCalle(String fld_GlosaTipoCalle) {
		this.fld_GlosaTipoCalle = fld_GlosaTipoCalle;
	}

	public byte getFld_TipoCalle() {
		return this.fld_TipoCalle;
	}

	public void setFld_TipoCalle(byte fld_TipoCalle) {
		this.fld_TipoCalle = fld_TipoCalle;
	}

}