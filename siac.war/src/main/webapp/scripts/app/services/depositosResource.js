angular.module('ccs').factory('DepositosResource', function ($resource) {
	var resource = $resource('rest/depositos/', { nocache : new Date().getTime()},
	{
		'consulta'   : { method:'POST', isArray:true , url:'rest/depositos/consulta'    },
		'guarda'     : { method:'POST', isArray:false, url:'rest/depositos/guarda'      },
		'cerrar'     : { method:'POST', isArray:false, url:'rest/depositos/cerrar'      },
		'cmbbancos'  : { method: 'GET', isArray:true , url:'rest/depositos/cmbbancos'   },
		'cmbctasctes': { method: 'GET', isArray:true , url:'rest/depositos/cmbctasctes' },
		'pdf'		 : { method:'POST', isArray:false, url:'rest/depositos/pdf'			}
	});
	return resource;
});
