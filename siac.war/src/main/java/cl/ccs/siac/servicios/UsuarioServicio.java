package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.usuario.UsuarioBusiness;
import cl.exe.ccs.dto.LoginDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class UsuarioServicio {

	private static final Logger LOG = LogManager.getLogger(UsuarioServicio.class);

	private static final String CCS_GUID = "ccs_guid";

	private static final String USER = "usuario";

	@Inject
	private UsuarioBusiness usuarioBusiness;

	@POST
	@Path("/ingreso")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest request, LoginDTO loginDTO) {

		String ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
		LOG.info(ipAddress);
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
			if (ipAddress.equals("0:0:0:0:0:0:0:1")) {
				ipAddress = "172.16.0.214"; // para desarrollo usar ip local
			}
		}

		try {
			Principal userPrincipal = request.getUserPrincipal();
			if (userPrincipal != null) {
				request.logout();
			}
			request.login(loginDTO.getUsername(), loginDTO.getPassword());

			UsuarioDTO usuarioDTO = usuarioBusiness.obtenerUsuario(loginDTO.getUsername());
			if (usuarioDTO.getIdControl() == 0) {
				if (usuarioDTO.getPrivilegios() != null && !usuarioDTO.getPrivilegios().isEmpty()) {
					request.getSession(Boolean.TRUE).setAttribute(CCS_GUID, usuarioDTO.getUsername());
					request.getSession().setAttribute(USER, usuarioDTO);
					usuarioDTO.setIdSession(request.getSession().getId());
					usuarioDTO.setIpLocal(ipAddress);
				} else {
					usuarioDTO.setIdControl(6);
					usuarioDTO.setMsgControl("No se identifica perfil de usuario, consulte con administrador");
				}

			} else {
				request.logout();
			}

			LOG.info("login " + loginDTO.getUsername() + " - Message: " + usuarioDTO.getMsgControl() + " - Codigo "
					+ usuarioDTO.getIdControl());
			return Response.ok(usuarioDTO).build();

		} catch (ServletException ex) {
			if ("FailedLoginException".equals(getCauseClassName(ex))) {
				String msge = "Nombre de usuario y/o password no son validos";
				LOG.info("login " + loginDTO.getUsername() + " - Message: " + msge, ex);
				return Response.ok(new UsuarioDTO(5, msge)).build();
			} else {
				LOG.error("login " + loginDTO.getUsername() + " - Message: Error del Sistema", ex);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}
		} catch (Exception ex) {
			LOG.error("login " + loginDTO.getUsername() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	public Response status(@Context HttpServletRequest request) {
		try {
			Principal userPrincipal = request.getUserPrincipal();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal != null && dto != null) {

				return Response.ok(dto).build();
			} else {
				LOG.error("Metodo status - Message: Sesion expirada.");
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}
		} catch (Exception ex) {
			LOG.error("Metodo status - Message: Error del sistema no controlado", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Path("/logout")
	@POST
	@Consumes("application/json")
	public Response logout(@Context HttpServletRequest request) {
		try {
			Principal userPrincipal = request.getUserPrincipal();
			request.getSession().setAttribute(USER, null);
			if (userPrincipal != null) {
				request.logout();
				request.getSession().invalidate();
			}

			ResponseDTO response = new ResponseDTO();
			response.setIdControl(0);

			return Response.ok(response).build();
		} catch (Exception ex) {
			String msge = "Error del sistema no controlado";
			LOG.error("logout - Message: " + msge, ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Path("/esSupervisor")
	@POST
	@Consumes("application/json")
	public Response esSupervisor(@Context HttpServletRequest request, LoginDTO loginDTO) {
		try {
			Principal userPrincipal = request.getUserPrincipal();
			if (userPrincipal == null || userPrincipal.getName() == null) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			UsuarioDTO usuarioSupervisor = usuarioBusiness.esSupervisor(loginDTO.getUsername(), loginDTO.getPassword());

			if (usuarioSupervisor.getIdControl() == 0 && usuarioSupervisor.getEsSupervisor()) {
				usuarioDTO.setValidaSupervisor(true);
				request.getSession().setAttribute("usuario", usuarioDTO);
			}
			LOG.info("login " + usuarioSupervisor.getUsername() + " - Message: " + usuarioSupervisor.getMsgControl() + " - Codigo " + usuarioSupervisor.getIdControl());

			return Response.ok(usuarioSupervisor).build();
		} catch (Exception ex) {
			String msge = "Error del sistema no controlado";
			LOG.error("logout - Message: " + msge, ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("/check")
	@Consumes("application/json")
	public Response checkSession(@Context HttpServletRequest request) {
		try {
			Principal userPrincipal = request.getUserPrincipal();
			ResponseDTO respuesta = new ResponseDTO();
			UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (userPrincipal != null && dto != null) {
				respuesta.setIdControl(0);
				return Response.ok(respuesta).build();
			} else {
				respuesta.setIdControl(1);
				return Response.ok(respuesta).build();
			}
		} catch (Exception ex) {
			LOG.error("status - Message: Error del sistema no controlado", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Path("/cambiarClave")
	@POST
	@Consumes("application/json")
	public Response cambiarClave(@Context HttpServletRequest request, LoginDTO loginDTO) {
		try {
			Principal userPrincipal = request.getUserPrincipal();
			if (userPrincipal == null || userPrincipal.getName() == null) {
				return Response.status(Response.Status.UNAUTHORIZED).build();
			}

			LOG.info("cambiarClave " + loginDTO.getUsername() + " - Password: " + loginDTO.getPassword());

			ResponseDTO usuarioCambiaClave = usuarioBusiness.cambiarClave(loginDTO.getUsername(),loginDTO.getPassword(), loginDTO.getPasswordAux());

			LOG.info("Message: " + usuarioCambiaClave.getMsgControl());

			return Response.ok(usuarioCambiaClave).build();

		} catch (Exception ex) {
			String msge = "Error del sistema no controlado";
			LOG.error("logout - Message: " + msge, ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/nombremaquinasesion")
	public Response nombreMaquinaSesion(@Context HttpServletRequest request, @QueryParam("nombremaquina") String maquina) {
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try{
			
			if(request.getSession().getAttribute("nombremaquina") == null){
			
				request.getSession().setAttribute("nombremaquina", maquina);
			
			}
			
			return Response.ok(maquina).build();
		}
		catch(Exception e){
			LOG.error("Metodo nombreMaquinaSesion - Message: Error del Sistema " + e.getMessage());	
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	private String getCauseClassName(Exception ex) {
		String clsCause = null;
		try {
			Throwable cause = ex.getCause();
			if (cause != null) {
				clsCause = cause.getClass().getSimpleName();
			}
		} catch (Exception e) {
		}
		return clsCause;
	}
}
