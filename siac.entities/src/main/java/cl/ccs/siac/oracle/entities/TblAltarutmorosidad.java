package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_ALTARUTMOROSIDAD database table.
 * 
 */
@Entity
@Table(name="TBL_ALTARUTMOROSIDAD")
@NamedQuery(name="TblAltarutmorosidad.findAll", query="SELECT t FROM TblAltarutmorosidad t")
public class TblAltarutmorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_CORRDIRECCION")
	private BigDecimal fldCorrdireccion;

	@Column(name="FLD_CORRENVIOCARGA")
	private BigDecimal fldCorrenviocarga;

	@Column(name="FLD_CORRMOROSIDAD")
	private BigDecimal fldCorrmorosidad;

	@Column(name="FLD_CORRNOMBRE")
	private BigDecimal fldCorrnombre;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_TIPORELACION")
	private String fldTiporelacion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblAltarutmorosidad() {
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public BigDecimal getFldCorrdireccion() {
		return this.fldCorrdireccion;
	}

	public void setFldCorrdireccion(BigDecimal fldCorrdireccion) {
		this.fldCorrdireccion = fldCorrdireccion;
	}

	public BigDecimal getFldCorrenviocarga() {
		return this.fldCorrenviocarga;
	}

	public void setFldCorrenviocarga(BigDecimal fldCorrenviocarga) {
		this.fldCorrenviocarga = fldCorrenviocarga;
	}

	public BigDecimal getFldCorrmorosidad() {
		return this.fldCorrmorosidad;
	}

	public void setFldCorrmorosidad(BigDecimal fldCorrmorosidad) {
		this.fldCorrmorosidad = fldCorrmorosidad;
	}

	public BigDecimal getFldCorrnombre() {
		return this.fldCorrnombre;
	}

	public void setFldCorrnombre(BigDecimal fldCorrnombre) {
		this.fldCorrnombre = fldCorrnombre;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldTiporelacion() {
		return this.fldTiporelacion;
	}

	public void setFldTiporelacion(String fldTiporelacion) {
		this.fldTiporelacion = fldTiporelacion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}