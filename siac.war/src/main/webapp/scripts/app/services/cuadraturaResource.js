angular.module('ccs').factory('CuadraturaResource', function ($resource) {
    var resource = $resource('rest/cuadratura/',  { nocache : new Date().getTime()},
    {
        'buscarCentroCostos':  { method : 'GET', params:{fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: true, url: 'rest/cuadratura/centroscostos', nocache : new Date().getTime()},
        'buscarUsuarios':  { method : 'GET', params:{centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: true, url: 'rest/cuadratura/usuarios', nocache : new Date().getTime()},
        'buscarCajas':  { method : 'GET', params:{usuario: 'usuario', centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: true, url: 'rest/cuadratura/cajas', nocache : new Date().getTime()},
        'cuadraturaCCS':  { method : 'GET', params:{centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: false, url: 'rest/cuadratura/cuadraturacentrocosto', nocache : new Date().getTime()},
        'cuadraturaUser':  { method : 'GET', params:{usuario: 'usuario', centroCosto:'centroCosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: false, url: 'rest/cuadratura/cuadraturausuario', nocache : new Date().getTime()},
        'cuadraturaCC':  { method : 'GET', params:{centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'},  isArray: false, url: 'rest/cuadratura/detcentrocosto', nocache : new Date().getTime()},
        'printCuadraturaCC':  { method : 'GET', isArray: false, params:{centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'}, url: 'rest/cuadratura/imprimereportecc', nocache : new Date().getTime()}, 
        'printCuadraturaCaja':  { method : 'POST', isArray: false, url: 'rest/cuadratura/imprimereportecaja', nocache : new Date().getTime()},
        'printCuadraturaUsuario':  { method : 'POST', isArray: false, url: 'rest/cuadratura/imprimereporteusuario', nocache : new Date().getTime()},
        'cuadraturaGeneral':  { method : 'GET', params:{centroscosto:'centroscosto', fechaDesde:'fechaDesde', fechaHasta:'fechaHasta'}, isArray: false, url: 'rest/cuadratura/cuadraturageneral', nocache : new Date().getTime()},
        'cuadraturaPorCentro':  { method : 'GET', params:{fechaDesde:'fechaDesde', centroscosto:'centroscosto', usuario: 'usuario', corrcaja: 'corrcaja', criterio:'criterio'}, isArray: false, url: 'rest/cuadratura/cuadraturaporcentro', nocache : new Date().getTime()},
        'cargaDatosCaja':  { method : 'GET', params:{fechaDesde:'fechaDesde', centrocosto:'centrocosto', usuario: 'usuario'}, isArray: true, url: 'rest/cuadratura/cargadatoscaja', nocache : new Date().getTime()},
        'cuadraturaPorCaja':  { method : 'GET', params:{fechaDesde:'fechaDesde', usuario:'usuario' , centroscosto:'centroscosto',corrcaja: 'corrcaja' , criterio:'criterio'}, isArray: false, url: 'rest/cuadratura/cuadraturaporcaja', nocache : new Date().getTime()},
        'imprimeCuadratura':  { method : 'POST',isArray: false, url: 'rest/cuadratura/imprimecuadratura', nocache : new Date().getTime()},
        'obtenerIVA' :  {  method : 'GET', isArray : false, url: 'rest/cuadratura/obtenerIVA'},
        'detalleBoleta':  { method : 'GET', params:{fechaDesde:'fechaDesde', centroscosto:'centroscosto', usuario: 'usuario', corrcaja: 'corrcaja', criterio:'criterio'}, isArray: false, url: 'rest/cuadratura/detalleboleta', nocache : new Date().getTime()},
        'modalBoletaCuad':  { method : 'POST', isArray: false, url: 'rest/cuadratura/detalleboletamodal', nocache : new Date().getTime()},
        'detalleServicio':  { method : 'GET', params:{fechaDesde:'fechaDesde', centrocosto:'centrocosto', usuario: 'usuario', corrcaja: 'corrcaja', criterio:'criterio', ordenamiento:'ordenamiento'}, isArray: false, url: 'rest/cuadratura/detalleservicio', nocache : new Date().getTime()},
        'imprimeDetalleBoleta':  { method : 'GET', params:{fechaDesde:'fechaDesde', centroscosto:'centroscosto', usuario: 'usuario', corrcaja: 'corrcaja', criterio:'criterio'}, isArray: false, url: 'rest/cuadratura/imprimedetalleboleta', nocache : new Date().getTime()},
        'imprimeDetalleServicio':  { method : 'GET', params:{fechaDesde:'fechaDesde', centrocosto:'centrocosto', usuario: 'usuario', corrcaja: 'corrcaja', criterio:'criterio', ordenamiento:'ordenamiento'}, isArray: false, url: 'rest/cuadratura/imprimedetalleservicio', nocache : new Date().getTime()},
        'imprimeValidacion':  { method : 'GET', params:{fechaDesde:'fechaDesde', centrocosto:'centrocosto', usuario: 'usuario', corrcaja: 'corrcaja'}, isArray: false, url: 'rest/cuadratura/validacionaclaracion', nocache : new Date().getTime()},
        'imprimeSolReclamo':  { method : 'GET', params:{fechaDesde:'fechaDesde', centrocosto:'centrocosto', usuario: 'usuario', corrcaja: 'corrcaja'}, isArray: false, url: 'rest/cuadratura/solicitudreclamo', nocache : new Date().getTime()},

    });
    return resource;
});
