package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_TipoCorreccion database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_TipoCorreccion.findAll", query="SELECT t FROM Tp_TipoCorreccion t")
public class Tp_TipoCorreccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_GlosaModificacion")
	private String fld_GlosaModificacion;

	@Column(name="Fld_TipoModificacion")
	private byte fld_TipoModificacion;

	public Tp_TipoCorreccion() {
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public String getFld_GlosaModificacion() {
		return this.fld_GlosaModificacion;
	}

	public void setFld_GlosaModificacion(String fld_GlosaModificacion) {
		this.fld_GlosaModificacion = fld_GlosaModificacion;
	}

	public byte getFld_TipoModificacion() {
		return this.fld_TipoModificacion;
	}

	public void setFld_TipoModificacion(byte fld_TipoModificacion) {
		this.fld_TipoModificacion = fld_TipoModificacion;
	}

}