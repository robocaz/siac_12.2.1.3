package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysdepends database table.
 * 
 */
@Entity
@Table(name="sysdepends")
@NamedQuery(name="Sysdepend.findAll", query="SELECT s FROM Sysdepend s")
public class Sysdepend implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] columns;

	private int depid;

	private short depnumber;

	private int id;

	private short number;

	private boolean readobj;

	private boolean resultobj;

	private boolean selall;

	private short status;

	public Sysdepend() {
	}

	public byte[] getColumns() {
		return this.columns;
	}

	public void setColumns(byte[] columns) {
		this.columns = columns;
	}

	public int getDepid() {
		return this.depid;
	}

	public void setDepid(int depid) {
		this.depid = depid;
	}

	public short getDepnumber() {
		return this.depnumber;
	}

	public void setDepnumber(short depnumber) {
		this.depnumber = depnumber;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getNumber() {
		return this.number;
	}

	public void setNumber(short number) {
		this.number = number;
	}

	public boolean getReadobj() {
		return this.readobj;
	}

	public void setReadobj(boolean readobj) {
		this.readobj = readobj;
	}

	public boolean getResultobj() {
		return this.resultobj;
	}

	public void setResultobj(boolean resultobj) {
		this.resultobj = resultobj;
	}

	public boolean getSelall() {
		return this.selall;
	}

	public void setSelall(boolean selall) {
		this.selall = selall;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

}