'use strict';
angular.module('ccs').directive('topbar',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/topbar.html?nocache='+ new Date().getTime()
    };
});
