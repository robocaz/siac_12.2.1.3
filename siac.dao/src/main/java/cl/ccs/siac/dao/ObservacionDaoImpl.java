package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ObservacionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("observacionDao")
@Stateless
public class ObservacionDaoImpl implements ObservacionDao{

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	private static final Logger log = LogManager.getLogger(ObservacionDaoImpl.class);
	
	@Override
	public ResponseListDTO<ObservacionDTO> listar(String rutTramitante) {
		
		
		try{
		
			log.info("Ejecutando SQ_TramitanteObservacion_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TramitanteObservacion_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.setParameter("Fld_RutTramitante", rutTramitante);
			
			storedProcedure.execute();
			
			ResponseListDTO<ObservacionDTO> respuesta = new ResponseListDTO<ObservacionDTO>();
			List<ObservacionDTO> lista = new ArrayList<ObservacionDTO>();
			
			List<Object[]> listaCentroCosto = storedProcedure.getResultList();
			
			for (Object item : listaCentroCosto) {
				Object[] datos = (Object[]) item;
				ObservacionDTO observacionDTO = new ObservacionDTO();
				observacionDTO.setCodigoObservacion(new Integer(datos[0].toString()));
				observacionDTO.setGlosaObservacion(datos[1].toString());
				observacionDTO.setUsuario (datos[2].toString());
				observacionDTO.setCodigoCentroCosto(new Integer(datos[3].toString()));
				observacionDTO.setGlosaCentroCosto(datos[4].toString());
				observacionDTO.setFechaCreacion(datos[5].toString());
				lista.add(observacionDTO);
			}
			
			respuesta.setLista(lista);
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SQ_TramitanteObservacion_SW2 " + e.getMessage());
			return null;
		}
		
	}

	@Override
	public ResponseDTO crear(ObservacionDTO observacionDTO) {
		
		try{
			
			log.info("Ejecutando SI_Observacion_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_Observacion_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodObservacion", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
	
			
			storedProcedure.setParameter("Fld_Rut",observacionDTO.getRutTramitante() );
			storedProcedure.setParameter("Fld_CodObservacion", observacionDTO.getCodigoObservacion());
			storedProcedure.setParameter("Fld_CodUsuario", observacionDTO.getUsuario());
			storedProcedure.setParameter("Fld_CodCCosto", observacionDTO.getCodigoCentroCosto());
	
			storedProcedure.execute();
			
			ResponseDTO responseDTO = new ResponseDTO();
			responseDTO.setIdControl(1);
			return responseDTO;
		}
		catch (Exception e) {
			log.error("Error en el SP SI_Observacion_SW2 " + e.getMessage());
			return null;
		}		
		
	}

}
