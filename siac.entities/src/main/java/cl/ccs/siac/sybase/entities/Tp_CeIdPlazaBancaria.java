package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdPlazaBancaria database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdPlazaBancaria.findAll", query="SELECT t FROM Tp_CeIdPlazaBancaria t")
public class Tp_CeIdPlazaBancaria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodPlazaBancaria")
	private int fld_CodPlazaBancaria;

	@Column(name="Fld_GlosaPlazaBancaria")
	private String fld_GlosaPlazaBancaria;

	public Tp_CeIdPlazaBancaria() {
	}

	public int getFld_CodPlazaBancaria() {
		return this.fld_CodPlazaBancaria;
	}

	public void setFld_CodPlazaBancaria(int fld_CodPlazaBancaria) {
		this.fld_CodPlazaBancaria = fld_CodPlazaBancaria;
	}

	public String getFld_GlosaPlazaBancaria() {
		return this.fld_GlosaPlazaBancaria;
	}

	public void setFld_GlosaPlazaBancaria(String fld_GlosaPlazaBancaria) {
		this.fld_GlosaPlazaBancaria = fld_GlosaPlazaBancaria;
	}

}