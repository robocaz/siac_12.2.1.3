package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ColaCartasDev database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ColaCartasDev.findAll", query="SELECT t FROM Tbl_ColaCartasDev t")
public class Tbl_ColaCartasDev implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrTarea")
	private BigDecimal fld_CorrTarea;

	public Tbl_ColaCartasDev() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrTarea() {
		return this.fld_CorrTarea;
	}

	public void setFld_CorrTarea(BigDecimal fld_CorrTarea) {
		this.fld_CorrTarea = fld_CorrTarea;
	}

}