package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsuarioRoles database table.
 * 
 */
@Entity
@Table(name="Tbl_UsuarioRoles")
@NamedQuery(name="Tbl_UsuarioRole.findAll", query="SELECT t FROM Tbl_UsuarioRole t")
public class Tbl_UsuarioRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoRol")
	private String fld_CodigoRol;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	public Tbl_UsuarioRole() {
	}

	public String getFld_CodigoRol() {
		return this.fld_CodigoRol;
	}

	public void setFld_CodigoRol(String fld_CodigoRol) {
		this.fld_CodigoRol = fld_CodigoRol;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

}