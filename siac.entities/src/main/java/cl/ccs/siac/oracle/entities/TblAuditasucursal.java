package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_AUDITASUCURSAL database table.
 * 
 */
@Entity
@Table(name="TBL_AUDITASUCURSAL")
@NamedQuery(name="TblAuditasucursal.findAll", query="SELECT t FROM TblAuditasucursal t")
public class TblAuditasucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ACCION")
	private String fldAccion;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODLOCALIDAD")
	private BigDecimal fldCodlocalidad;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSASUCURSAL")
	private String fldGlosasucursal;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOELIMINACION")
	private String fldUsuarioeliminacion;

	public TblAuditasucursal() {
	}

	public String getFldAccion() {
		return this.fldAccion;
	}

	public void setFldAccion(String fldAccion) {
		this.fldAccion = fldAccion;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodlocalidad() {
		return this.fldCodlocalidad;
	}

	public void setFldCodlocalidad(BigDecimal fldCodlocalidad) {
		this.fldCodlocalidad = fldCodlocalidad;
	}

	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}

	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosasucursal() {
		return this.fldGlosasucursal;
	}

	public void setFldGlosasucursal(String fldGlosasucursal) {
		this.fldGlosasucursal = fldGlosasucursal;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuarioeliminacion() {
		return this.fldUsuarioeliminacion;
	}

	public void setFldUsuarioeliminacion(String fldUsuarioeliminacion) {
		this.fldUsuarioeliminacion = fldUsuarioeliminacion;
	}

}