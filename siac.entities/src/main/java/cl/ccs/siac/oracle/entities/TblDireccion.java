package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_DIRECCION database table.
 * 
 */
@Entity
@Table(name="TBL_DIRECCION")
@NamedQuery(name="TblDireccion.findAll", query="SELECT t FROM TblDireccion t")
public class TblDireccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_DIRECCION_FLDCORRDIRECCIONID_GENERATOR", sequenceName="FLD_CORRDIRECCION_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_DIRECCION_FLDCORRDIRECCIONID_GENERATOR")
	@Column(name="FLD_CORRDIRECCION_ID")
	private long fldCorrdireccionId;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSADIRECCION")
	private String fldGlosadireccion;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_TIPODIRECCION")
	private BigDecimal fldTipodireccion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblDireccioncomuna
	@ManyToOne
	@JoinColumn(name="FLD_CORRCOMUNA")
	private TblDireccioncomuna tblDireccioncomuna;

	//bi-directional many-to-one association to TblRutmorhisto
	@OneToMany(mappedBy="tblDireccion")
	private List<TblRutmorhisto> tblRutmorhistos;

	//bi-directional many-to-one association to TblRutmorosidad
	@OneToMany(mappedBy="tblDireccion")
	private List<TblRutmorosidad> tblRutmorosidads;

	public TblDireccion() {
	}

	public long getFldCorrdireccionId() {
		return this.fldCorrdireccionId;
	}

	public void setFldCorrdireccionId(long fldCorrdireccionId) {
		this.fldCorrdireccionId = fldCorrdireccionId;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosadireccion() {
		return this.fldGlosadireccion;
	}

	public void setFldGlosadireccion(String fldGlosadireccion) {
		this.fldGlosadireccion = fldGlosadireccion;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public BigDecimal getFldTipodireccion() {
		return this.fldTipodireccion;
	}

	public void setFldTipodireccion(BigDecimal fldTipodireccion) {
		this.fldTipodireccion = fldTipodireccion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public TblDireccioncomuna getTblDireccioncomuna() {
		return this.tblDireccioncomuna;
	}

	public void setTblDireccioncomuna(TblDireccioncomuna tblDireccioncomuna) {
		this.tblDireccioncomuna = tblDireccioncomuna;
	}

	public List<TblRutmorhisto> getTblRutmorhistos() {
		return this.tblRutmorhistos;
	}

	public void setTblRutmorhistos(List<TblRutmorhisto> tblRutmorhistos) {
		this.tblRutmorhistos = tblRutmorhistos;
	}

	public TblRutmorhisto addTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().add(tblRutmorhisto);
		tblRutmorhisto.setTblDireccion(this);

		return tblRutmorhisto;
	}

	public TblRutmorhisto removeTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().remove(tblRutmorhisto);
		tblRutmorhisto.setTblDireccion(null);

		return tblRutmorhisto;
	}

	public List<TblRutmorosidad> getTblRutmorosidads() {
		return this.tblRutmorosidads;
	}

	public void setTblRutmorosidads(List<TblRutmorosidad> tblRutmorosidads) {
		this.tblRutmorosidads = tblRutmorosidads;
	}

	public TblRutmorosidad addTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().add(tblRutmorosidad);
		tblRutmorosidad.setTblDireccion(this);

		return tblRutmorosidad;
	}

	public TblRutmorosidad removeTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().remove(tblRutmorosidad);
		tblRutmorosidad.setTblDireccion(null);

		return tblRutmorosidad;
	}

}