package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolicitudVtaInternet_old database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolicitudVtaInternet_old.findAll", query="SELECT t FROM Tbl_SolicitudVtaInternet_old t")
public class Tbl_SolicitudVtaInternet_old implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CantServicios")
	private byte fld_CantServicios;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEmpresaPago")
	private String fld_CodEmpresaPago;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodMedioPago")
	private String fld_CodMedioPago;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrSolicitud_Id")
	private BigDecimal fld_CorrSolicitud_Id;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecContable")
	private Timestamp fld_FecContable;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecSolicitud")
	private Timestamp fld_FecSolicitud;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_MontoBruto")
	private BigDecimal fld_MontoBruto;

	@Column(name="Fld_NombreArchivo")
	private String fld_NombreArchivo;

	@Column(name="Fld_NombreCompleto")
	private String fld_NombreCompleto;

	@Column(name="Fld_NroFactura")
	private int fld_NroFactura;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_TelefonoCelular")
	private String fld_TelefonoCelular;

	@Column(name="Fld_TelefonoDomicilio")
	private String fld_TelefonoDomicilio;

	@Column(name="Fld_TransEmpresaPago")
	private String fld_TransEmpresaPago;

	@Column(name="Fld_Ubicacion")
	private String fld_Ubicacion;

	public Tbl_SolicitudVtaInternet_old() {
	}

	public byte getFld_CantServicios() {
		return this.fld_CantServicios;
	}

	public void setFld_CantServicios(byte fld_CantServicios) {
		this.fld_CantServicios = fld_CantServicios;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public String getFld_CodEmpresaPago() {
		return this.fld_CodEmpresaPago;
	}

	public void setFld_CodEmpresaPago(String fld_CodEmpresaPago) {
		this.fld_CodEmpresaPago = fld_CodEmpresaPago;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodMedioPago() {
		return this.fld_CodMedioPago;
	}

	public void setFld_CodMedioPago(String fld_CodMedioPago) {
		this.fld_CodMedioPago = fld_CodMedioPago;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrSolicitud_Id() {
		return this.fld_CorrSolicitud_Id;
	}

	public void setFld_CorrSolicitud_Id(BigDecimal fld_CorrSolicitud_Id) {
		this.fld_CorrSolicitud_Id = fld_CorrSolicitud_Id;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecContable() {
		return this.fld_FecContable;
	}

	public void setFld_FecContable(Timestamp fld_FecContable) {
		this.fld_FecContable = fld_FecContable;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecSolicitud() {
		return this.fld_FecSolicitud;
	}

	public void setFld_FecSolicitud(Timestamp fld_FecSolicitud) {
		this.fld_FecSolicitud = fld_FecSolicitud;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public BigDecimal getFld_MontoBruto() {
		return this.fld_MontoBruto;
	}

	public void setFld_MontoBruto(BigDecimal fld_MontoBruto) {
		this.fld_MontoBruto = fld_MontoBruto;
	}

	public String getFld_NombreArchivo() {
		return this.fld_NombreArchivo;
	}

	public void setFld_NombreArchivo(String fld_NombreArchivo) {
		this.fld_NombreArchivo = fld_NombreArchivo;
	}

	public String getFld_NombreCompleto() {
		return this.fld_NombreCompleto;
	}

	public void setFld_NombreCompleto(String fld_NombreCompleto) {
		this.fld_NombreCompleto = fld_NombreCompleto;
	}

	public int getFld_NroFactura() {
		return this.fld_NroFactura;
	}

	public void setFld_NroFactura(int fld_NroFactura) {
		this.fld_NroFactura = fld_NroFactura;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public String getFld_TelefonoCelular() {
		return this.fld_TelefonoCelular;
	}

	public void setFld_TelefonoCelular(String fld_TelefonoCelular) {
		this.fld_TelefonoCelular = fld_TelefonoCelular;
	}

	public String getFld_TelefonoDomicilio() {
		return this.fld_TelefonoDomicilio;
	}

	public void setFld_TelefonoDomicilio(String fld_TelefonoDomicilio) {
		this.fld_TelefonoDomicilio = fld_TelefonoDomicilio;
	}

	public String getFld_TransEmpresaPago() {
		return this.fld_TransEmpresaPago;
	}

	public void setFld_TransEmpresaPago(String fld_TransEmpresaPago) {
		this.fld_TransEmpresaPago = fld_TransEmpresaPago;
	}

	public String getFld_Ubicacion() {
		return this.fld_Ubicacion;
	}

	public void setFld_Ubicacion(String fld_Ubicacion) {
		this.fld_Ubicacion = fld_Ubicacion;
	}

}