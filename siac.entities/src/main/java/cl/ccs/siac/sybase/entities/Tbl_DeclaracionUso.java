package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DeclaracionUso database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DeclaracionUso.findAll", query="SELECT t FROM Tbl_DeclaracionUso t")
public class Tbl_DeclaracionUso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMotivo")
	private short fld_CodMotivo;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutRequirente")
	private String fld_RutRequirente;

	public Tbl_DeclaracionUso() {
	}

	public short getFld_CodMotivo() {
		return this.fld_CodMotivo;
	}

	public void setFld_CodMotivo(short fld_CodMotivo) {
		this.fld_CodMotivo = fld_CodMotivo;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutRequirente() {
		return this.fld_RutRequirente;
	}

	public void setFld_RutRequirente(String fld_RutRequirente) {
		this.fld_RutRequirente = fld_RutRequirente;
	}

}