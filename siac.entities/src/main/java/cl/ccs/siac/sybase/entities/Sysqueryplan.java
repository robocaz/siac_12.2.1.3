package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sysqueryplans database table.
 * 
 */
@Entity
@Table(name="sysqueryplans")
@NamedQuery(name="Sysqueryplan.findAll", query="SELECT s FROM Sysqueryplan s")
public class Sysqueryplan implements Serializable {
	private static final long serialVersionUID = 1L;

	private int dbid;

	private int gid;

	private int hashkey;

	private int hashkey2;

	private int id;

	private int key1;

	private int key2;

	private int key3;

	private int key4;

	private Timestamp qpdate;

	private short sequence;

	private int sprocid;

	private int status;

	private String text;

	private short type;

	private int uid;

	public Sysqueryplan() {
	}

	public int getDbid() {
		return this.dbid;
	}

	public void setDbid(int dbid) {
		this.dbid = dbid;
	}

	public int getGid() {
		return this.gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public int getHashkey() {
		return this.hashkey;
	}

	public void setHashkey(int hashkey) {
		this.hashkey = hashkey;
	}

	public int getHashkey2() {
		return this.hashkey2;
	}

	public void setHashkey2(int hashkey2) {
		this.hashkey2 = hashkey2;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getKey1() {
		return this.key1;
	}

	public void setKey1(int key1) {
		this.key1 = key1;
	}

	public int getKey2() {
		return this.key2;
	}

	public void setKey2(int key2) {
		this.key2 = key2;
	}

	public int getKey3() {
		return this.key3;
	}

	public void setKey3(int key3) {
		this.key3 = key3;
	}

	public int getKey4() {
		return this.key4;
	}

	public void setKey4(int key4) {
		this.key4 = key4;
	}

	public Timestamp getQpdate() {
		return this.qpdate;
	}

	public void setQpdate(Timestamp qpdate) {
		this.qpdate = qpdate;
	}

	public short getSequence() {
		return this.sequence;
	}

	public void setSequence(short sequence) {
		this.sequence = sequence;
	}

	public int getSprocid() {
		return this.sprocid;
	}

	public void setSprocid(int sprocid) {
		this.sprocid = sprocid;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}