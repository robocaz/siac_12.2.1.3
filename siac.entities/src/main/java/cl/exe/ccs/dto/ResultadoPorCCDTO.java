package cl.exe.ccs.dto;

import java.io.Serializable;

public class ResultadoPorCCDTO implements Serializable{

	
	
	private Integer centroCosto;
	private String glosaCC;
	private String estadoCaja;
	private Integer cantCajasAbiertas;
	private Long montoTransacciones;
	private Long montoIngresadoCajera;
	private Long diferenciaPos;
	private Long diferenciaNeg;
	
	
	
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getGlosaCC() {
		return glosaCC;
	}
	public void setGlosaCC(String glosaCC) {
		this.glosaCC = glosaCC;
	}
	public String getEstadoCaja() {
		return estadoCaja;
	}
	public void setEstadoCaja(String estadoCaja) {
		this.estadoCaja = estadoCaja;
	}
	public Long getMontoTransacciones() {
		return montoTransacciones;
	}
	public void setMontoTransacciones(Long montoTransacciones) {
		this.montoTransacciones = montoTransacciones;
	}
	public Long getMontoIngresadoCajera() {
		return montoIngresadoCajera;
	}
	public void setMontoIngresadoCajera(Long montoIngresadoCajera) {
		this.montoIngresadoCajera = montoIngresadoCajera;
	}
	public Long getDiferenciaPos() {
		return diferenciaPos;
	}
	public void setDiferenciaPos(Long diferenciaPos) {
		this.diferenciaPos = diferenciaPos;
	}
	public Long getDiferenciaNeg() {
		return diferenciaNeg;
	}
	public void setDiferenciaNeg(Long diferenciaNeg) {
		this.diferenciaNeg = diferenciaNeg;
	}
	public Integer getCantCajasAbiertas() {
		return cantCajasAbiertas;
	}
	public void setCantCajasAbiertas(Integer cantCajasAbiertas) {
		this.cantCajasAbiertas = cantCajasAbiertas;
	}

	
	
}
