package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoComprInDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = -262380983979702758L;

	private Integer numero;
	private String tituNombre;
	private String tituRut;
	private String tituFono;
	private String tituEmail;
	private String tituDirec;
	private String tituComuna;
	private String tramNombre;
	private String tramRut;
	private String tramFono;
	private String tramEmail;
	private String tramDirec;
	private String tramComuna;
	private Integer cantDoc;
	private String texto;
	private String codUser;

	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getTituNombre() {
		return tituNombre;
	}
	public void setTituNombre(String tituNombre) {
		this.tituNombre = tituNombre;
	}
	public String getTituRut() {
		return tituRut;
	}
	public void setTituRut(String tituRut) {
		this.tituRut = tituRut;
	}
	public String getTituFono() {
		return tituFono;
	}
	public void setTituFono(String tituFono) {
		this.tituFono = tituFono;
	}
	public String getTituEmail() {
		return tituEmail;
	}
	public void setTituEmail(String tituEmail) {
		this.tituEmail = tituEmail;
	}
	public String getTituDirec() {
		return tituDirec;
	}
	public void setTituDirec(String tituDirec) {
		this.tituDirec = tituDirec;
	}
	public String getTituComuna() {
		return tituComuna;
	}
	public void setTituComuna(String tituComuna) {
		this.tituComuna = tituComuna;
	}
	public String getTramNombre() {
		return tramNombre;
	}
	public void setTramNombre(String tramNombre) {
		this.tramNombre = tramNombre;
	}
	public String getTramRut() {
		return tramRut;
	}
	public void setTramRut(String tramRut) {
		this.tramRut = tramRut;
	}
	public String getTramFono() {
		return tramFono;
	}
	public void setTramFono(String tramFono) {
		this.tramFono = tramFono;
	}
	public String getTramEmail() {
		return tramEmail;
	}
	public void setTramEmail(String tramEmail) {
		this.tramEmail = tramEmail;
	}
	public String getTramDirec() {
		return tramDirec;
	}
	public void setTramDirec(String tramDirec) {
		this.tramDirec = tramDirec;
	}
	public String getTramComuna() {
		return tramComuna;
	}
	public void setTramComuna(String tramComuna) {
		this.tramComuna = tramComuna;
	}
	public Integer getCantDoc() {
		return cantDoc;
	}
	public void setCantDoc(Integer cantDoc) {
		this.cantDoc = cantDoc;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getCodUser() {
		return codUser;
	}
	public void setCodUser(String codUser) {
		this.codUser = codUser;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudReclamoComprInDTO [numero=");
		builder.append(numero);
		builder.append(", tituNombre=");
		builder.append(tituNombre);
		builder.append(", tituRut=");
		builder.append(tituRut);
		builder.append(", tituFono=");
		builder.append(tituFono);
		builder.append(", tituEmail=");
		builder.append(tituEmail);
		builder.append(", tituDirec=");
		builder.append(tituDirec);
		builder.append(", tituComuna=");
		builder.append(tituComuna);
		builder.append(", tramNombre=");
		builder.append(tramNombre);
		builder.append(", tramRut=");
		builder.append(tramRut);
		builder.append(", tramFono=");
		builder.append(tramFono);
		builder.append(", tramEmail=");
		builder.append(tramEmail);
		builder.append(", tramDirec=");
		builder.append(tramDirec);
		builder.append(", tramComuna=");
		builder.append(tramComuna);
		builder.append(", cantDoc=");
		builder.append(cantDoc);
		builder.append(", texto=");
		builder.append(texto);
		builder.append(", codUser=");
		builder.append(codUser);
		builder.append("]");
		return builder.toString();
	}

}
