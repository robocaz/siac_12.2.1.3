package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_Bloqueos database table.
 * 
 */
@Entity
@Table(name="tmp_Bloqueos")
@NamedQuery(name="Tmp_Bloqueo.findAll", query="SELECT t FROM Tmp_Bloqueo t")
public class Tmp_Bloqueo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	public Tmp_Bloqueo() {
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

}