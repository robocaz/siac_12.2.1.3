package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_PROCESOTAREA database table.
 * 
 */
@Entity
@Table(name="TBL_PROCESOTAREA")
@NamedQuery(name="TblProcesotarea.findAll", query="SELECT t FROM TblProcesotarea t")
public class TblProcesotarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODPROCESO")
	private BigDecimal fldCodproceso;

	@Column(name="FLD_NOMBREPROCESO")
	private String fldNombreproceso;

	@Column(name="FLD_NOMBRESCRIPTS")
	private String fldNombrescripts;

	@Column(name="FLD_NROPROCACTIVOS")
	private BigDecimal fldNroprocactivos;

	@Column(name="FLD_PATH")
	private String fldPath;

	public TblProcesotarea() {
	}

	public BigDecimal getFldCodproceso() {
		return this.fldCodproceso;
	}

	public void setFldCodproceso(BigDecimal fldCodproceso) {
		this.fldCodproceso = fldCodproceso;
	}

	public String getFldNombreproceso() {
		return this.fldNombreproceso;
	}

	public void setFldNombreproceso(String fldNombreproceso) {
		this.fldNombreproceso = fldNombreproceso;
	}

	public String getFldNombrescripts() {
		return this.fldNombrescripts;
	}

	public void setFldNombrescripts(String fldNombrescripts) {
		this.fldNombrescripts = fldNombrescripts;
	}

	public BigDecimal getFldNroprocactivos() {
		return this.fldNroprocactivos;
	}

	public void setFldNroprocactivos(BigDecimal fldNroprocactivos) {
		this.fldNroprocactivos = fldNroprocactivos;
	}

	public String getFldPath() {
		return this.fldPath;
	}

	public void setFldPath(String fldPath) {
		this.fldPath = fldPath;
	}

}