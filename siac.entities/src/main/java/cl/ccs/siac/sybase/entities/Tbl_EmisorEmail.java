package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EmisorEmail database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EmisorEmail.findAll", query="SELECT t FROM Tbl_EmisorEmail t")
public class Tbl_EmisorEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_TipoEmail")
	private short fld_TipoEmail;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_EmisorEmail() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public short getFld_TipoEmail() {
		return this.fld_TipoEmail;
	}

	public void setFld_TipoEmail(short fld_TipoEmail) {
		this.fld_TipoEmail = fld_TipoEmail;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}