package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_MotivoConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_MotivoConsLey20575.findAll", query="SELECT t FROM Tbl_MotivoConsLey20575 t")
public class Tbl_MotivoConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMotivo")
	private short fld_CodMotivo;

	@Column(name="Fld_CodNemotecnico")
	private String fld_CodNemotecnico;

	@Column(name="Fld_GlosaMotivo")
	private String fld_GlosaMotivo;

	public Tbl_MotivoConsLey20575() {
	}

	public short getFld_CodMotivo() {
		return this.fld_CodMotivo;
	}

	public void setFld_CodMotivo(short fld_CodMotivo) {
		this.fld_CodMotivo = fld_CodMotivo;
	}

	public String getFld_CodNemotecnico() {
		return this.fld_CodNemotecnico;
	}

	public void setFld_CodNemotecnico(String fld_CodNemotecnico) {
		this.fld_CodNemotecnico = fld_CodNemotecnico;
	}

	public String getFld_GlosaMotivo() {
		return this.fld_GlosaMotivo;
	}

	public void setFld_GlosaMotivo(String fld_GlosaMotivo) {
		this.fld_GlosaMotivo = fld_GlosaMotivo;
	}

}