package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CodAutenticacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CodAutenticacion.findAll", query="SELECT t FROM Tbl_CodAutenticacion t")
public class Tbl_CodAutenticacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Cantidad")
	private short fld_Cantidad;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrAutenticacion_Id")
	private BigDecimal fld_CorrAutenticacion_Id;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoConsulta")
	private byte fld_TipoConsulta;

	public Tbl_CodAutenticacion() {
	}

	public short getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(short fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrAutenticacion_Id() {
		return this.fld_CorrAutenticacion_Id;
	}

	public void setFld_CorrAutenticacion_Id(BigDecimal fld_CorrAutenticacion_Id) {
		this.fld_CorrAutenticacion_Id = fld_CorrAutenticacion_Id;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public byte getFld_TipoConsulta() {
		return this.fld_TipoConsulta;
	}

	public void setFld_TipoConsulta(byte fld_TipoConsulta) {
		this.fld_TipoConsulta = fld_TipoConsulta;
	}

}