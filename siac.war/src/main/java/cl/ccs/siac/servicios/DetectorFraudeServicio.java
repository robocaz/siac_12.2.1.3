package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.fraude.DetectorFraudeBusiness;
import cl.exe.ccs.dto.DetectorFraudeDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Path("/detectorfraude")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class DetectorFraudeServicio {

	private static final Logger log = LogManager.getLogger(DetectorFraudeServicio.class);

	@Inject
	private DetectorFraudeBusiness detectorFraude;

	@POST
	@Path("/detallesFraudePorRut")
	public Response getDetallesFraudePorRut(@Context HttpServletRequest request, PersonaDTO persona) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResponseListDTO<DetectorFraudeDTO> responseList = detectorFraude.getDetallesFraudePorRut(persona.getRut());
			return Response.ok(responseList).build();
		} catch (Exception ex) {
			log.error("Metodo getDetallesFraudePorRut - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/validaAutorizacion")
	public Response validaAutorizacion(@Context HttpServletRequest request, DetectorFraudeDTO dataFraude) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResponseDTO response = detectorFraude.validaAutorizacion(dataFraude);
			return Response.ok(response).build();
		} catch (Exception ex) {
			log.error("Metodo validaAutorizacion - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
