package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.CajaDTO;
import cl.exe.ccs.dto.CajaPendienteDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("cajaDao")
@Stateless
public class CajaDaoImpl implements CajaDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(CajaDaoImpl.class);

	@Override
	public CajaDTO cantidadAbiertas(String usuario, String codCentroCosto) {

		
		try{
			log.info("Ejecutando SP_ConsultaCajas2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_ConsultaCajas2_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RetornoAux", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCajasAbiertas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCajasPermitidas", Integer.class, ParameterMode.OUT);
	
			log.info("Fld_CodCCosto " + codCentroCosto);
			log.info("Fld_CodUsuario " + usuario);
			log.info("Fld_CodOrigen " + "ON");		
			
			storedProcedure.setParameter("Fld_CodCCosto", new Integer(codCentroCosto));
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodOrigen", "ON");
	
			storedProcedure.execute();
	
			CajaDTO cajaDTO = new CajaDTO();
	
			cajaDTO.setIdControl(new Integer(storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString()));
			cajaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString());
	
			cajaDTO.setCajasAbiertas(new Integer(storedProcedure.getOutputParameterValue("Fld_NroCajasAbiertas").toString()));
			cajaDTO.setCajasPermitidas(new Integer(storedProcedure.getOutputParameterValue("Fld_NroCajasPermitidas").toString()));
			cajaDTO.setCorrCaja(storedProcedure.getOutputParameterValue("Fld_CorrCaja").toString());
	
			return cajaDTO;
		}
		 catch (Exception e) {
				log.error("Error en el SP SP_ConsultaCajas2_SW2 " + e.getMessage());
				return null;
		}

	}
	
	@Override
	public CajaDTO numeroCajasAbiertas(String usuario, String codCentroCosto) {
	
		try{
			log.info("Ejecutando SP_ConsultaCajas2_SW2");
				
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_NroCajasAbiertas_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RetornoAux", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCajasAbiertas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCajasPermitidas", Integer.class, ParameterMode.OUT);
	
			log.info("Fld_CodCCosto " + codCentroCosto);
			log.info("Fld_CodUsuario " + usuario);
			log.info("Fld_CodOrigen " + "ON");		
				
			storedProcedure.setParameter("Fld_CodCCosto", new Integer(codCentroCosto));
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodOrigen", "ON");
	
			storedProcedure.execute();
	
			CajaDTO cajaDTO = new CajaDTO();
	
			cajaDTO.setIdControl(new Integer(storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString()));
			cajaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString());
	
			cajaDTO.setCajasAbiertas(new Integer(storedProcedure.getOutputParameterValue("Fld_NroCajasAbiertas").toString()));
			cajaDTO.setCajasPermitidas(new Integer(storedProcedure.getOutputParameterValue("Fld_NroCajasPermitidas").toString()));
			cajaDTO.setCorrCaja(storedProcedure.getOutputParameterValue("Fld_CorrCaja").toString());
	
			return cajaDTO;
		}
		catch (Exception e) {
			log.error("Error en el SP SP_ConsultaCajas2_SW2 " + e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseListDTO<CajaDTO> listarCajasAbiertas(String usuario, String codCentroCosto) {

		
		try{
			
			log.info("Ejecutando SQ_CajasAbiertas_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CajasAbiertas_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
	
			log.info("Fld_CodUsuario " + usuario);
			log.info("Fld_CodCCosto " + codCentroCosto);
			
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodCCosto", new Integer(codCentroCosto));
	
			storedProcedure.execute();
	
			ResponseListDTO<CajaDTO> respuesta = new ResponseListDTO<CajaDTO>();
			List<CajaDTO> lista = new ArrayList<CajaDTO>();
	
			List<Object[]> listaCentroCosto = storedProcedure.getResultList();
	
			for (Object item : listaCentroCosto) {
				Object[] datos = (Object[]) item;
				lista.add(new CajaDTO(datos[0].toString(), datos[1].toString(), datos[2].toString(), (byte[]) datos[3],datos[4].toString()));
			}
	
			respuesta.setLista(lista);
	
			return respuesta;
		}
		catch (Exception e) {
				log.error("Error en el SP SQ_CajasAbiertas_SW2 " + e.getMessage());
				return null;
		}
	}

	@Override
	public CajaDTO aperturaCaja(String usuario, String codCentroCosto) {

		try{
			
			log.info("Ejecutando SP_AperturaCaja2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_AperturaCaja2_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodOrigen", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RetornoAux", String.class, ParameterMode.OUT);
	
			log.info("Fld_CodCCosto " + codCentroCosto);
			log.info("Fld_CodUsuario " + usuario);
			log.info("Fld_CodOrigen " + "ON");
			
			storedProcedure.setParameter("Fld_CodCCosto", new Integer(codCentroCosto));
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			storedProcedure.setParameter("Fld_CodOrigen", "ON");
	
			storedProcedure.execute();
	
			CajaDTO cajaDTO = new CajaDTO();
			cajaDTO.setCorrCaja(storedProcedure.getOutputParameterValue("Fld_CorrCaja").toString());
			cajaDTO.setIdControl(new Integer(storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString()));
	
			return cajaDTO;
		}
		catch (Exception e) {
			log.error("Error en el SP SP_AperturaCaja2_SW2 " + e.getMessage());
			return null;
		}		
	}

	@Override
	public String tomarCaja(String CorrCaja, byte[] timeStamp, String username) {

		try{
			log.info("Ejecutando SU_TomaCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_TomaCaja_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Timestamp", byte[].class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RetornoAux", String.class, ParameterMode.OUT);
	
			log.info("Fld_CorrCaja " + CorrCaja);
			log.info("Fld_Timestamp " + timeStamp);
			log.info("Fld_CodUsuario " + username);
			
			
			storedProcedure.setParameter("Fld_CorrCaja", new Long(CorrCaja));
			storedProcedure.setParameter("Fld_Timestamp", timeStamp);
			storedProcedure.setParameter("Fld_CodUsuario", username);
			storedProcedure.execute();
	
			return storedProcedure.getOutputParameterValue("Fld_RetornoAux").toString();
		}
		catch (Exception e) {
			log.error("Error en el SP SU_TomaCaja_SW2 " + e.getMessage());
			return null;
		}		
	}

	@Override
	public Boolean cerrarCajaTemporal(String CorrCaja, String usuario) {

		try{
			
			log.info("Ejecutando SU_CierreTempCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_CierreTempCaja_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			
			log.info("Fld_CorrCaja " + CorrCaja);
			log.info("Fld_CodUsuario " + usuario);
			
			storedProcedure.setParameter("Fld_CorrCaja", new Long(CorrCaja));
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			
			return storedProcedure.execute();
		}
		catch (Exception e) {
			log.error("Error en el SP SU_CierreTempCaja_SW2 " + e.getMessage());
			return null;
		}		
	}

	@Override
	public Boolean cerrarCajaDefinitivo(String CorrCaja, String usuario) {

		try{
			
			log.info("Ejecutando SU_CierreCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_CierreCaja_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			
			log.info("Fld_CorrCaja " + CorrCaja);
			log.info("Fld_CodUsuario " + usuario);

			
			storedProcedure.setParameter("Fld_CorrCaja", new Long(CorrCaja));
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
			return storedProcedure.execute();
		}
		catch (Exception e) {
			log.error("Error en el SP SU_CierreCaja_SW2 " + e.getMessage());
			return null;
		}	
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseListDTO<CajaDTO> listarCajasTodas(String usuario) {

		
		try{
			
			log.info("Ejecutando SQ_CajasAbieUsr_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CajasAbieUsr_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);

			log.info("Fld_CodUsuario " + usuario);
			
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
	
			storedProcedure.execute();
	
			ResponseListDTO<CajaDTO> respuesta = new ResponseListDTO<CajaDTO>();
			List<CajaDTO> lista = new ArrayList<CajaDTO>();
	
			List<Object[]> listaCajas = storedProcedure.getResultList();
	
			for (Object item : listaCajas) {
				Object[] datos = (Object[]) item;
				lista.add(new CajaDTO(datos[0].toString(), datos[1].toString(), datos[2].toString(), datos[3].toString(),datos[4].toString(), datos[5].toString()));
			}
	
			respuesta.setLista(lista);
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SQ_CajasAbieUsr_SW2 " + e.getMessage());
			return null;
		}	
		
	}

	@Override
	public ResponseListDTO<CajaDTO> listarCajasDelDiaPorPrivilegio(String codCentroCosto, String usuario,int tipoUsuario) {
		
		
		
		try{
			log.info("Ejecutando SQ_CmbCajasDelDia_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbCajasDelDia_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Usuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoUsuario", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			log.info("Fld_CCosto " + new Integer(codCentroCosto));
			log.info("Fld_Usuario " + usuario);
			log.info("Fld_TipoUsuario " + tipoUsuario);
			
			storedProcedure.setParameter("Fld_CCosto", new Integer(codCentroCosto));
			storedProcedure.setParameter("Fld_Usuario", usuario);
			storedProcedure.setParameter("Fld_TipoUsuario", tipoUsuario);
	
			storedProcedure.execute();
	
			ResponseListDTO<CajaDTO> respuesta = new ResponseListDTO<CajaDTO>();
			List<CajaDTO> lista = new ArrayList<CajaDTO>();
	
			List<Object[]> listaCajas = storedProcedure.getResultList();
	
			for (Object item : listaCajas) {
				lista.add(new CajaDTO(item.toString()));
			}
	
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setLista(lista);
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SQ_CmbCajasDelDia_SW2 " + e.getMessage());
			return null;
		}			
	}

	public CajaPendienteDTO cajasAbiertaPendiente(String usuario) {

		CajaPendienteDTO cajaPendiente = new CajaPendienteDTO();

		try{
			
			log.info("Ejecutando SQ_TransacPendientes_SW2");
		
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TransacPendientes_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			log.info("Fld_CodUsuario " + usuario);
			
			storedProcedure.setParameter("Fld_CodUsuario", usuario);
	
			storedProcedure.execute();
	
			Integer salida = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());
			if (salida != null && salida == 0) {
				cajaPendiente.setCorrCaja(storedProcedure.getOutputParameterValue("Fld_CorrCaja").toString());
				cajaPendiente.setRutTramitante(storedProcedure.getOutputParameterValue("Fld_RutTramitante").toString());
			}
			
			cajaPendiente.setIdControl(salida);
			
			return cajaPendiente;
		}
		catch (Exception e) {
			log.error("Error en el SP SQ_TransacPendientes_SW2 " + e.getMessage());
			return null;
		}	
			
		
	}

}
