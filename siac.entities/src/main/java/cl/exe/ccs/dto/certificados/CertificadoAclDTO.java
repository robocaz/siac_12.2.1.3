package cl.exe.ccs.dto.certificados;

import java.io.Serializable;
import java.math.BigDecimal;

import cl.exe.ccs.dto.ResponseDTO;

public class CertificadoAclDTO extends ResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3527050471552867144L;
	private Long corrAutenticacion;
	private Integer nroBoletinProt;
    private Integer pagBoletinProt;
    private String fecPublicacion;
    private String fecProt;
    private String tipoDocumento;
    private String glosaTipoDocumento;
    private String tipoDocumentoImpago;
    private Integer nroOper4Dig;
    private String codEmisor;
	private String glosaCorta;
    private BigDecimal montoProt;
    private String montoProtString;
    private String glosaEmisor;
    private String nombreLibrador;
    private String ciudad;
    private String fecAclaracion;
    private String fecPago;
    private String codAutenticBic;
    private String rutAfectado;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String moneda;
    private String glosaDetalleCertificado;

    public CertificadoAclDTO() { }

    public CertificadoAclDTO(String glosaDetalleCertificado) {
        this.setGlosaDetalleCertificado(glosaDetalleCertificado);
    }

	public Long getCorrAutenticacion() {
		return corrAutenticacion;
	}
	public void setCorrAutenticacion(Long corrAutenticacion) {
		this.corrAutenticacion = corrAutenticacion;
	}
	public Integer getNroBoletinProt() {
		return nroBoletinProt;
	}
	public void setNroBoletinProt(Integer nroBoletinProt) {
		this.nroBoletinProt = nroBoletinProt;
	}
	public Integer getPagBoletinProt() {
		return pagBoletinProt;
	}
	public void setPagBoletinProt(Integer pagBoletinProt) {
		this.pagBoletinProt = pagBoletinProt;
	}
	public String getFecPublicacion() {
		return fecPublicacion;
	}
	public void setFecPublicacion(String fecPublicacion) {
		this.fecPublicacion = fecPublicacion;
	}
	public String getFecProt() {
		return fecProt;
	}
	public void setFecProt(String fecProt) {
		this.fecProt = fecProt;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getGlosaTipoDocumento() {
		return glosaTipoDocumento;
	}
	public void setGlosaTipoDocumento(String glosaTipoDocumento) {
		this.glosaTipoDocumento = glosaTipoDocumento;
	}
	public String getTipoDocumentoImpago() {
		return tipoDocumentoImpago;
	}
	public void setTipoDocumentoImpago(String tipoDocumentoImpago) {
		this.tipoDocumentoImpago = tipoDocumentoImpago;
	}
	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}
	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public String getGlosaCorta() {
		return glosaCorta;
	}
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
	public BigDecimal getMontoProt() {
		return montoProt;
	}
	public void setMontoProt(BigDecimal montoProt) {
		this.montoProt = montoProt;
	}
	public String getMontoProtString() {
		return montoProtString;
	}

	public void setMontoProtString(String montoProtString) {
		this.montoProtString = montoProtString;
	}

	public String getGlosaEmisor() {
		return glosaEmisor;
	}
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}
	public String getNombreLibrador() {
		return nombreLibrador;
	}
	public void setNombreLibrador(String nombreLibrador) {
		this.nombreLibrador = nombreLibrador;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getFecAclaracion() {
		return fecAclaracion;
	}
	public void setFecAclaracion(String fecAclaracion) {
		this.fecAclaracion = fecAclaracion;
	}
	public String getFecPago() {
		return fecPago;
	}
	public void setFecPago(String fecPago) {
		this.fecPago = fecPago;
	}
	public String getCodAutenticBic() {
		return codAutenticBic;
	}
	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getGlosaDetalleCertificado() {
		return glosaDetalleCertificado;
	}

	public void setGlosaDetalleCertificado(String glosaDetalleCertificado) {
		this.glosaDetalleCertificado = glosaDetalleCertificado;
	}
    
}
