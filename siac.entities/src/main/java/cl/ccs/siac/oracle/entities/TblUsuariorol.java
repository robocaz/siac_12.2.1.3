package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_USUARIOROL database table.
 * 
 */
@Entity
@Table(name="TBL_USUARIOROL")
@NamedQuery(name="TblUsuariorol.findAll", query="SELECT t FROM TblUsuariorol t")
public class TblUsuariorol implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TblUsuariorolPK id;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblUsuariorol() {
	}

	public TblUsuariorolPK getId() {
		return this.id;
	}

	public void setId(TblUsuariorolPK id) {
		this.id = id;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}