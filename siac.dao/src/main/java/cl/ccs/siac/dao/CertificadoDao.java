package cl.ccs.siac.dao;

import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;

/**
 * @author rbocaz
 *
 */
/**
 * @author emira
 *
 */
/**
 * @author emira
 *
 */
/**
 * @author emira
 *
 */
public interface CertificadoDao {

	/**
	 * @param user
	 * @param certificado
	 * @return informacion de usuarios desde Base de Datos BIC para informe ISA
	 */
	public ConsultaBICDTO generaCOACBIC(String user, CertificadoDTO certificado);

	/**
	 * @param user
	 * @param certificado
	 * @param codAutenticacionBIC
	 * @return informacion de usuario desde la Base de Datos MOL para informe
	 *         ISA
	 */
	public ConsultaMOLDTO generaCOACMOL(String user, CertificadoDTO certificado, String codAutenticacionBIC);

	/**
	 * @param consultaCerAclDTO
	 * @return informacion de usuario desde la Base de Datos para Certificado
	 *         CAA/CAR
	 */
	public ConsultaCerAclDTO generaCAA(ConsultaCerAclDTO consultaCerAclDTO);

	/**
	 * @param consultaCerAclDTO
	 * @return informacion de usuario desde la Base de Datos para Certificado
	 *         CAC
	 */
	public CertificadoAclDTO generaCAC(ConsultaCerAclDTO consultaCerAclDTO);

	/**
	 * @param consCertificado
	 * @return informe Lista Publico
	 */
	public ConsCertificadoILPDTO generaILP(ConsCertificadoILPDTO consCertificado);

	/**
	 * @param consCertificado
	 * @return Informe Comercial
	 */
	public ConsCertificadoICMDTO generaICM(ConsCertificadoICMDTO consCertificado);
	
	/**
	 * @param teavisa
	 * @return Contrato Te Avisa
	 */
	public TeAvisaDTO getDatosContratoTeAvisa(TeAvisaDTO contrato);
	
}
