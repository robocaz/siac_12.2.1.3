package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_AclDocAclaracion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclDocAclaracion.findAll", query="SELECT t FROM Tbl_AclDocAclaracion t")
public class Tbl_AclDocAclaracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Campo_1")
	private String fld_Campo_1;

	@Column(name="Fld_Campo_2")
	private String fld_Campo_2;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_NomInstitucion")
	private String fld_NomInstitucion;

	public Tbl_AclDocAclaracion() {
	}

	public String getFld_Campo_1() {
		return this.fld_Campo_1;
	}

	public void setFld_Campo_1(String fld_Campo_1) {
		this.fld_Campo_1 = fld_Campo_1;
	}

	public String getFld_Campo_2() {
		return this.fld_Campo_2;
	}

	public void setFld_Campo_2(String fld_Campo_2) {
		this.fld_Campo_2 = fld_Campo_2;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public String getFld_NomInstitucion() {
		return this.fld_NomInstitucion;
	}

	public void setFld_NomInstitucion(String fld_NomInstitucion) {
		this.fld_NomInstitucion = fld_NomInstitucion;
	}

}