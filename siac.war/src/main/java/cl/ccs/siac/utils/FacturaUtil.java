package cl.ccs.siac.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.inject.Inject;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.factura.FacturaBusiness;
import cl.exe.ccs.dto.TramitanteDTO;
import cl.exe.ccs.resources.Constants;

public class FacturaUtil {

	private static final Logger log = LogManager.getLogger(FacturaUtil.class);

	@Inject
	private FacturaBusiness facturaBusiness;

	public static String getRutWS(String rut) {
		rut = FacturaUtil.getRutSinCero(rut);
		if (rut.contains(".")) {
			rut = rut.replace(".", "");
		}
		return rut.trim();
	}

	public static String getRutSinCero(String rut) {
		while (rut.charAt(0) == '0') {
			rut = rut.substring(1);
		}
		if (!rut.contains("-")) {
			rut = rut.substring(0, rut.length() - 1) + "-" + rut.charAt(rut.length() - 1);
		}
		return rut.trim();
	}

	public static String getTipoPersonaRut(String rut) {
		if (rut.contains(".")) {
			rut = rut.replace(".", "");
		}
		if (rut.contains("-")) {
			rut = rut.substring(0,rut.lastIndexOf("-"));
		}
		return Integer.parseInt(rut) < 50000000 ? "N" : "J";
	}

	public static String getTramitante(TramitanteDTO tramitante) {
		return tramitante.getNombres().trim() + " " + tramitante.getApellidoPaterno().trim() + " "
				+ tramitante.getApellidoMaterno().trim();
	}

	public static XMLGregorianCalendar getFecha() {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		XMLGregorianCalendar fecha = null;
		try {
			fecha = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException ex) {
			log.info("getFecha - " + ex);
		}
		return fecha;
	}
	
	public static XMLGregorianCalendar getFechaSinHora() {
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		XMLGregorianCalendar fecha = null;
		try {
			fecha = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException ex) {
			log.info("getFechaSinHora - " + ex.getLocalizedMessage());
		}
		return fecha;
	}

	public static Integer getIVA(Float iva, Integer monto) {
		Double bruto = (double) (monto * iva / 100);
		 //Math.round(totalCosto / (1 + $scope.iva / 100)) : 0);
		return (int) Math.round(bruto);
	}

	public static String getMedioPago(String codMedioPago) {
		/*
		 * {codigo: "CHE", descripcion: "CHEQUE"} {codigo: "EFT", descripcion:
		 * "EFECTIVO"} {codigo: "VVS", descripcion: "VALE VISTA"} {codigo:
		 * "TCD", descripcion: "T. CREDITO"} {codigo: "RC", descripcion:
		 * "REDCOMPRA"}
		 * 
		 * CH, LT, EF, PE, TC, CF, OT;
		 */
		switch (codMedioPago) {
		case "CHE":
			return "CH";
		case "EFT":
			return "EF";
		case "VVS":
			return "OT";
		case "TCD":
			return "TC";
		case "RC":
			return "OT";
		default:
			return "OT";
		}
	}

	public static String getFileName(String folio, String modo, Boolean ext) {
		Date fecha = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		
		String folio2 = folio;
		for(int x=folio.length();x<8;x++){
			folio2="0"+folio2;
		}

		String file = Constants.INVOICE_COST_CENTER_SYMBOL + Constants.INVOICE_COST_CENTER_RUT_ONLY
				+ Constants.INVOICE_DOCUMENT_TYPE + folio2 + df.format(fecha);

		if (modo.compareTo("11") == 0) {
			file = file + "_c";
		}

		if (ext) {
			file = file + ".pdf";
		}

		return file;
	}

	public static String getFileName(String folio, String modo) {
		return FacturaUtil.getFileName(folio, modo, true);
	}

}
