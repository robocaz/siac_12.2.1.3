package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysroles database table.
 * 
 */
@Entity
@Table(name="sysroles")
@NamedQuery(name="Sysrole.findAll", query="SELECT s FROM Sysrole s")
public class Sysrole implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private int lrid;

	private int status;

	private short type;

	public Sysrole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLrid() {
		return this.lrid;
	}

	public void setLrid(int lrid) {
		this.lrid = lrid;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

}