package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoInscripcion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoInscripcion.findAll", query="SELECT t FROM Tbl_TipoInscripcion t")
public class Tbl_TipoInscripcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoInscripcion")
	private String fld_GlosaTipoInscripcion;

	@Column(name="Fld_TipoInscripcion")
	private byte fld_TipoInscripcion;

	public Tbl_TipoInscripcion() {
	}

	public String getFld_GlosaTipoInscripcion() {
		return this.fld_GlosaTipoInscripcion;
	}

	public void setFld_GlosaTipoInscripcion(String fld_GlosaTipoInscripcion) {
		this.fld_GlosaTipoInscripcion = fld_GlosaTipoInscripcion;
	}

	public byte getFld_TipoInscripcion() {
		return this.fld_TipoInscripcion;
	}

	public void setFld_TipoInscripcion(byte fld_TipoInscripcion) {
		this.fld_TipoInscripcion = fld_TipoInscripcion;
	}

}