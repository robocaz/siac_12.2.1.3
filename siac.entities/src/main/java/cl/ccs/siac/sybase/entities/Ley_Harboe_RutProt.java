package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_Harboe_RutProt database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_Harboe_RutProt.findAll", query="SELECT l FROM Ley_Harboe_RutProt l")
public class Ley_Harboe_RutProt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Ley_Harboe_RutProt() {
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}