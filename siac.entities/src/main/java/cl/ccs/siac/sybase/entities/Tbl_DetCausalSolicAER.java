package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetCausalSolicAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetCausalSolicAER.findAll", query="SELECT t FROM Tbl_DetCausalSolicAER t")
public class Tbl_DetCausalSolicAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalSolic")
	private byte fld_CodCausalSolic;

	@Column(name="Fld_CorrDocum")
	private BigDecimal fld_CorrDocum;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_DetCausalSolicAER() {
	}

	public byte getFld_CodCausalSolic() {
		return this.fld_CodCausalSolic;
	}

	public void setFld_CodCausalSolic(byte fld_CodCausalSolic) {
		this.fld_CodCausalSolic = fld_CodCausalSolic;
	}

	public BigDecimal getFld_CorrDocum() {
		return this.fld_CorrDocum;
	}

	public void setFld_CorrDocum(BigDecimal fld_CorrDocum) {
		this.fld_CorrDocum = fld_CorrDocum;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}