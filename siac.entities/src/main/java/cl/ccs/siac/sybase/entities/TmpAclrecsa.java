package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_aclrecsa database table.
 * 
 */
@Entity
@Table(name="tmp_aclrecsa")
@NamedQuery(name="TmpAclrecsa.findAll", query="SELECT t FROM TmpAclrecsa t")
public class TmpAclrecsa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Corr")
	private BigDecimal corr;

	@Column(name="CorrAcl")
	private BigDecimal corrAcl;

	@Column(name="CorrProt")
	private BigDecimal corrProt;

	public TmpAclrecsa() {
	}

	public BigDecimal getCorr() {
		return this.corr;
	}

	public void setCorr(BigDecimal corr) {
		this.corr = corr;
	}

	public BigDecimal getCorrAcl() {
		return this.corrAcl;
	}

	public void setCorrAcl(BigDecimal corrAcl) {
		this.corrAcl = corrAcl;
	}

	public BigDecimal getCorrProt() {
		return this.corrProt;
	}

	public void setCorrProt(BigDecimal corrProt) {
		this.corrProt = corrProt;
	}

}