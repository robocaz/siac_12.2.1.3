package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tp_AuditCeIdTipoFuente database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_AuditCeIdTipoFuente.findAll", query="SELECT t FROM Tp_AuditCeIdTipoFuente t")
public class Tp_AuditCeIdTipoFuente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecEliminacion")
	private Timestamp fld_FecEliminacion;

	@Column(name="Fld_GlosaTipoFuente")
	private String fld_GlosaTipoFuente;

	@Column(name="Fld_TipoFuente")
	private byte fld_TipoFuente;

	@Column(name="Fld_UsrCreacion")
	private String fld_UsrCreacion;

	@Column(name="Fld_UsrEliminacion")
	private String fld_UsrEliminacion;

	public Tp_AuditCeIdTipoFuente() {
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecEliminacion() {
		return this.fld_FecEliminacion;
	}

	public void setFld_FecEliminacion(Timestamp fld_FecEliminacion) {
		this.fld_FecEliminacion = fld_FecEliminacion;
	}

	public String getFld_GlosaTipoFuente() {
		return this.fld_GlosaTipoFuente;
	}

	public void setFld_GlosaTipoFuente(String fld_GlosaTipoFuente) {
		this.fld_GlosaTipoFuente = fld_GlosaTipoFuente;
	}

	public byte getFld_TipoFuente() {
		return this.fld_TipoFuente;
	}

	public void setFld_TipoFuente(byte fld_TipoFuente) {
		this.fld_TipoFuente = fld_TipoFuente;
	}

	public String getFld_UsrCreacion() {
		return this.fld_UsrCreacion;
	}

	public void setFld_UsrCreacion(String fld_UsrCreacion) {
		this.fld_UsrCreacion = fld_UsrCreacion;
	}

	public String getFld_UsrEliminacion() {
		return this.fld_UsrEliminacion;
	}

	public void setFld_UsrEliminacion(String fld_UsrEliminacion) {
		this.fld_UsrEliminacion = fld_UsrEliminacion;
	}

}