package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_LOGCONSULTA database table.
 * 
 */
@Entity
@Table(name="TBL_LOGCONSULTA")
@NamedQuery(name="TblLogconsulta.findAll", query="SELECT t FROM TblLogconsulta t")
public class TblLogconsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_LOGCONSULTA_FLDCORRLOGCONSULTAID_GENERATOR", sequenceName="FLD_CORRLOGCONSULTA_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_LOGCONSULTA_FLDCORRLOGCONSULTAID_GENERATOR")
	@Column(name="FLD_CORRLOGCONSULTA_ID")
	private long fldCorrlogconsultaId;

	@Column(name="FLD_CANTIDAD")
	private BigDecimal fldCantidad;

	@Column(name="FLD_FECHATRANSACCION")
	private Timestamp fldFechatransaccion;

	@Column(name="FLD_FLAGBATCH")
	private BigDecimal fldFlagbatch;

	@Column(name="FLD_RESULTADO")
	private BigDecimal fldResultado;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	//bi-directional many-to-one association to TblUsuariomol
	@ManyToOne
	@JoinColumn(name="FLD_USUARIOCONSULTA")
	private TblUsuariomol tblUsuariomol;

	//bi-directional many-to-one association to TblTipotransaccion
	@ManyToOne
	@JoinColumn(name="FLD_CODTRANSACCION")
	private TblTipotransaccion tblTipotransaccion;

	public TblLogconsulta() {
	}

	public long getFldCorrlogconsultaId() {
		return this.fldCorrlogconsultaId;
	}

	public void setFldCorrlogconsultaId(long fldCorrlogconsultaId) {
		this.fldCorrlogconsultaId = fldCorrlogconsultaId;
	}

	public BigDecimal getFldCantidad() {
		return this.fldCantidad;
	}

	public void setFldCantidad(BigDecimal fldCantidad) {
		this.fldCantidad = fldCantidad;
	}

	public Timestamp getFldFechatransaccion() {
		return this.fldFechatransaccion;
	}

	public void setFldFechatransaccion(Timestamp fldFechatransaccion) {
		this.fldFechatransaccion = fldFechatransaccion;
	}

	public BigDecimal getFldFlagbatch() {
		return this.fldFlagbatch;
	}

	public void setFldFlagbatch(BigDecimal fldFlagbatch) {
		this.fldFlagbatch = fldFlagbatch;
	}

	public BigDecimal getFldResultado() {
		return this.fldResultado;
	}

	public void setFldResultado(BigDecimal fldResultado) {
		this.fldResultado = fldResultado;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public TblUsuariomol getTblUsuariomol() {
		return this.tblUsuariomol;
	}

	public void setTblUsuariomol(TblUsuariomol tblUsuariomol) {
		this.tblUsuariomol = tblUsuariomol;
	}

	public TblTipotransaccion getTblTipotransaccion() {
		return this.tblTipotransaccion;
	}

	public void setTblTipotransaccion(TblTipotransaccion tblTipotransaccion) {
		this.tblTipotransaccion = tblTipotransaccion;
	}

}