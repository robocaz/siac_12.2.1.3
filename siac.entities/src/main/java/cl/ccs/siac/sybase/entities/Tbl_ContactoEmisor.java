package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ContactoEmisor database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ContactoEmisor.findAll", query="SELECT t FROM Tbl_ContactoEmisor t")
public class Tbl_ContactoEmisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CargoContacto")
	private String fld_CargoContacto;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CodUsuarioMod")
	private String fld_CodUsuarioMod;

	@Column(name="Fld_DireccionContacto")
	private String fld_DireccionContacto;

	@Column(name="Fld_Estado")
	private boolean fld_Estado;

	@Column(name="Fld_FaxContacto")
	private double fld_FaxContacto;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecMod")
	private Timestamp fld_FecMod;

	@Column(name="Fld_FonoContacto")
	private String fld_FonoContacto;

	@Column(name="Fld_MailContacto")
	private String fld_MailContacto;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_NomContacto_Nombres")
	private String fld_NomContacto_Nombres;

	@Column(name="Fld_RutContacto")
	private String fld_RutContacto;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_ContactoEmisor() {
	}

	public String getFld_CargoContacto() {
		return this.fld_CargoContacto;
	}

	public void setFld_CargoContacto(String fld_CargoContacto) {
		this.fld_CargoContacto = fld_CargoContacto;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public String getFld_CodUsuarioMod() {
		return this.fld_CodUsuarioMod;
	}

	public void setFld_CodUsuarioMod(String fld_CodUsuarioMod) {
		this.fld_CodUsuarioMod = fld_CodUsuarioMod;
	}

	public String getFld_DireccionContacto() {
		return this.fld_DireccionContacto;
	}

	public void setFld_DireccionContacto(String fld_DireccionContacto) {
		this.fld_DireccionContacto = fld_DireccionContacto;
	}

	public boolean getFld_Estado() {
		return this.fld_Estado;
	}

	public void setFld_Estado(boolean fld_Estado) {
		this.fld_Estado = fld_Estado;
	}

	public double getFld_FaxContacto() {
		return this.fld_FaxContacto;
	}

	public void setFld_FaxContacto(double fld_FaxContacto) {
		this.fld_FaxContacto = fld_FaxContacto;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecMod() {
		return this.fld_FecMod;
	}

	public void setFld_FecMod(Timestamp fld_FecMod) {
		this.fld_FecMod = fld_FecMod;
	}

	public String getFld_FonoContacto() {
		return this.fld_FonoContacto;
	}

	public void setFld_FonoContacto(String fld_FonoContacto) {
		this.fld_FonoContacto = fld_FonoContacto;
	}

	public String getFld_MailContacto() {
		return this.fld_MailContacto;
	}

	public void setFld_MailContacto(String fld_MailContacto) {
		this.fld_MailContacto = fld_MailContacto;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_NomContacto_Nombres() {
		return this.fld_NomContacto_Nombres;
	}

	public void setFld_NomContacto_Nombres(String fld_NomContacto_Nombres) {
		this.fld_NomContacto_Nombres = fld_NomContacto_Nombres;
	}

	public String getFld_RutContacto() {
		return this.fld_RutContacto;
	}

	public void setFld_RutContacto(String fld_RutContacto) {
		this.fld_RutContacto = fld_RutContacto;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}