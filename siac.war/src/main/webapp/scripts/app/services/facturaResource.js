angular.module('ccs').factory('FacturaResource', function ($resource) {
    var resource = $resource('rest/factura',{
        nocache : new Date().getTime()
    },{
        'consultar' :   {  method : 'POST', isArray : false, url: 'rest/factura/consultar'},
        'grabar' : {  method : 'POST', isArray : false, url: 'rest/factura/grabar'},
        'eliminar' : {  method : 'POST', isArray : false, url: 'rest/factura/eliminar'},
        'actualizarTransaccion' : {  method : 'POST', isArray : false, url: 'rest/factura/actualizarTransaccion'},
        'folio' : {  method : 'POST', isArray : false, url: 'rest/factura/folio'},
        'emitir' : {  method : 'POST', isArray : false, url: 'rest/factura/emitir'},
        'descargar' :  {  method : 'GET', isArray : false, url: 'rest/factura/descargar'},
        'rut' : {method: 'POST', isArray : false, url: 'rest/factura/rut' },
        'consultaRangoBF' : {  method : 'POST', isArray : false, url: 'rest/factura/consultaRangoBF' },
        'reemplazarBoleta': { method: 'POST', isArray: false, url: 'rest/factura/reemplazarBoleta' },
        'reemplazarBoletaCero': { method: 'POST', isArray: false, url: 'rest/factura/reemplazarBoletaCero' },
        'obtenerIVA' :  {  method : 'GET', isArray : false, url: 'rest/factura/obtenerIVA'}
    });
    return resource;
});
