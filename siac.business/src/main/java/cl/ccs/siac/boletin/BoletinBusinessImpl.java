package cl.ccs.siac.boletin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.BoletinDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.BoletinInDTO;
import cl.exe.ccs.dto.BoletinDataDetalle;
import cl.exe.ccs.dto.BoletinDataEmpleador;
import cl.exe.ccs.dto.BoletinDataTrabajador;
import cl.exe.ccs.dto.BoletinDetalle_DP;
import cl.exe.ccs.dto.BoletinEmpleador_DP;
import cl.exe.ccs.dto.BoletinTrabajador_Emp;
import cl.exe.ccs.dto.BoletinOutDTO;

@Named("boletinBusiness")
public class BoletinBusinessImpl implements BoletinBusiness {

	static final String RUT_SIN_INFO = "999999999-6";

	private static final Logger LOG = LogManager.getLogger(BoletinBusinessImpl.class);

	@Inject
	private BoletinDao boletinDao;

	@Inject
	private Utils utils;

	@Override
	public BoletinOutDTO<BoletinDataTrabajador> consultaTrabajador(BoletinInDTO params) {
		
		return boletinDao.consultaTrabajador(params);
	}
	
	public Map<String, Object> obtenerMapaBLT(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataTrabajador> dataOut) {
		LOG.debug("> Mapa BLT - RUT: " + dataIn.getRutAfectado());
		
		Map<String, Object> mapa = new HashMap<String, Object>();
		
		mapa.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		
		mapa.put("RUT", utils.formateaRUTSin0(dataIn.getRutAfectado()));
		mapa.put("NOMBRE", dataOut.getNombreInfractor());
		mapa.put("FECHA", utils.formatearFecha(new Date()));
		mapa.put("NUM_DEUDAS", dataOut.getNroDeudasLaboral());
		mapa.put("MTO_DEUDAS", dataOut.getMontoDeudaLaboral());
		
		mapa.put("USUARIO_CC", dataIn.getCodUsuario() + " - " + dataIn.getCentroCosto());
		mapa.put("COD_AUTENTIC", dataOut.getCodAutenticBic());
		
		List<BoletinTrabajador_Emp> boletinPDF = new ArrayList<BoletinTrabajador_Emp>();
		
		BoletinTrabajador_Emp empleador = new BoletinTrabajador_Emp();
		List<BoletinDataTrabajador> filas = new ArrayList<BoletinDataTrabajador>();
		
		if ( dataOut.getLista() != null && !dataOut.getLista().isEmpty() ) {
			
			for ( int i=0; i < dataOut.getLista().size(); i++ ) {
				BoletinDataTrabajador row = dataOut.getLista().get(i);
				
				if ( row.getTipoDeudaLaboral().equals("D") ) {
					filas.add(row);
				} else if ( row.getTipoDeudaLaboral().equals("T") ) {
					empleador.setRutEmpleador(utils.formateaRUTSin0(row.getRut()));
					empleador.setRazonSocial(row.getNombre());
					empleador.setMontoAdeudado(row.getMontoAdeudado());
					empleador.setMontoLaboralUTM(row.getMontoLaboralUTM());
					Collections.sort(filas);
					empleador.setLista(filas);
					boletinPDF.add(empleador);
					filas = new ArrayList<BoletinDataTrabajador>();
					empleador = new BoletinTrabajador_Emp();
				}
			}
			
		}
		mapa.put("COLECCION_PAG", boletinPDF);
		LOG.debug(mapa.toString());
		
		return mapa;
	}

	@Override
	public BoletinOutDTO<BoletinDataDetalle> consultaInfractor(BoletinInDTO params) {
		
		return boletinDao.consultaInfractor(params);
	}
	
	public Map<String, Object> obtenerMapaBLI(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataDetalle> dataOut) {
		LOG.debug("> Mapa BLI - RUT: " + dataIn.getRutAfectado());
		
		Map<String, Object> mapa = new HashMap<String, Object>();
		
		mapa.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		
		mapa.put("RUT", utils.formateaRUTSin0(dataIn.getRutAfectado()));
		mapa.put("NOMBRE", dataOut.getNombreInfractor());
		mapa.put("FECHA", utils.formatearFecha(new Date()));
		mapa.put("NUM_DEUDAS", dataOut.getNroDeudasLaboral());
		mapa.put("MTO_DEUDAS", dataOut.getMontoDeudaLaboral());
		mapa.put("FECHA_DEUDA_REC", utils.checkTimestampToStrFechaVista(dataOut.getFecUltimaDeuda()));
		mapa.put("NUM_INST_ACREE", dataOut.getNroAcreedores());
		mapa.put("NUM_MULTAS", dataOut.getNroMultasLaboral());
		mapa.put("MTO_MULTAS", dataOut.getMontoMultaLaboral());
		mapa.put("FECHA_MULTA_REC", utils.checkTimestampToStrFechaVista(dataOut.getFecUltimaMulta()));
		
		mapa.put("USUARIO_CC", dataIn.getCodUsuario() + " - " + dataIn.getCentroCosto());
		mapa.put("COD_AUTENTIC", dataOut.getCodAutenticBic());
		
		List<BoletinDetalle_DP> listaDeudasDP = new ArrayList<BoletinDetalle_DP>();
		List<BoletinDataDetalle> listaMultas = new ArrayList<BoletinDataDetalle>();
		
		if ( dataOut.getLista() != null && !dataOut.getLista().isEmpty() ) {
			
			List<BoletinDataDetalle> listaDeudas = new ArrayList<BoletinDataDetalle>();
			
			for ( int i=0; i < dataOut.getLista().size(); i++ ) {
				BoletinDataDetalle row = dataOut.getLista().get(i);
				
				if ( row.getTipoDeudaLaboral().equals("D") ) {
					continue;
				} else if ( row.getTipoDeudaLaboral().equals("I") ) {
					row.setRut(checkRutAFormatear(row.getRut()));
					listaDeudas.add(row);
				} else if ( row.getTipoDeudaLaboral().equals("M") ) {
					row.setFecBolLaboral(utils.checkTimestampToStrFechaVista(row.getFecBolLaboral()));
					listaMultas.add(row);
				}
				
			}
			
			if ( listaDeudas != null && !listaDeudas.isEmpty() ) {
				
				String tmpRut = null;
				String tmpInst = null;
				BigDecimal totalTrab = BigDecimal.ZERO;
				BigDecimal totalTrabUTM = BigDecimal.ZERO;
				BigDecimal totalInst = BigDecimal.ZERO;
				BigDecimal totalInstUTM = BigDecimal.ZERO;
				
				BoletinDetalle_DP grupoInst = new BoletinDetalle_DP();
				List<BoletinDataDetalle> tabla = new ArrayList<BoletinDataDetalle>();
				
				for ( int i=0; i < listaDeudas.size(); i++ ) {
					BoletinDataDetalle row = listaDeudas.get(i);
					
					if ( i == 0 ) {
						addHeadDeudasDP(grupoInst, row);
					}
					
					if ( tmpInst != null && !tmpInst.equals(row.getNombreInstitucion()) ) {
						addSubtotal(tabla, totalTrab, totalTrabUTM);
						addLineaSubtotal(tabla);
						
						grupoInst.setTotal(totalInst.add(totalTrab));
						grupoInst.setTotalUTM(totalInstUTM.add(totalTrabUTM));
						
						grupoInst.setLista(tabla);
						listaDeudasDP.add(grupoInst);
						
						tabla = new ArrayList<BoletinDataDetalle>();
						grupoInst = new BoletinDetalle_DP();
						
						totalTrab = BigDecimal.ZERO;
						totalTrabUTM = BigDecimal.ZERO;
						totalInst = BigDecimal.ZERO;
						totalInstUTM = BigDecimal.ZERO;
						
						addHeadDeudasDP(grupoInst, row);
						
					} else if ( tmpRut != null && !tmpRut.equals(row.getRut()) ) {
						
						addSubtotal(tabla, totalTrab, totalTrabUTM);
						addLineaSubtotal(tabla);
						
						totalInst = totalInst.add(totalTrab);
						totalInstUTM = totalInstUTM.add(totalTrabUTM);
						totalTrab = BigDecimal.ZERO;
						totalTrabUTM = BigDecimal.ZERO;
					}
					
					totalTrab = totalTrab.add(row.getMontoLaboral());
					totalTrabUTM = totalTrabUTM.add(row.getMontoLaboralUTM());
					
					tmpRut = row.getRut();
					tmpInst = row.getNombreInstitucion();
					
					tabla.add(row);
					
					if ( i == (listaDeudas.size()-1) ) {
						addSubtotal(tabla, totalTrab, totalTrabUTM);
						addLineaSubtotal(tabla);
						
						grupoInst.setTotal(totalInst.add(totalTrab));
						grupoInst.setTotalUTM(totalInstUTM.add(totalTrabUTM));
						
						grupoInst.setLista(tabla);
						listaDeudasDP.add(grupoInst);
					}
					
				}
				
			}
			
		}
		
		mapa.put("LISTA_MULTAS", listaMultas);
		mapa.put("COLECCION_PAG", listaDeudasDP);
		LOG.debug(mapa.toString());
		
		return mapa;
	}
	
	private void addHeadDeudasDP(BoletinDetalle_DP grupo, BoletinDataDetalle row) {
		grupo.setInstitucion(row.getNombreInstitucion());
		grupo.setMotivo(row.getMotivoInfraccion());
		grupo.setBoletin(row.getNroBolLaboral());
		grupo.setPagina(row.getPagBolLaboral());
		grupo.setFecha(utils.checkTimestampToStrFechaVista(row.getFecBolLaboral()));
		grupo.setMeses(row.getMeses());
		grupo.setCotizaciones(row.getNroCotizaciones());
		grupo.setMontoAdeudado(row.getMontoAdeudado());
	}
	
	private void addSubtotal(List<BoletinDataDetalle> filas, BigDecimal monto, BigDecimal montoUTM) {
		final BoletinDataDetalle subTotal = new BoletinDataDetalle();
		subTotal.setTipoDeudaLaboral("SUBTOTAL");
		subTotal.setMontoLaboral(monto);
		subTotal.setMontoLaboralUTM(montoUTM);
		filas.add(subTotal);
	}
	
	private void addLineaSubtotal(List<BoletinDataDetalle> filas) {
		final BoletinDataDetalle linea = new BoletinDataDetalle();
		linea.setTipoDeudaLaboral("LINEA");
		filas.add(linea);
	}
	
	@Override
	public BoletinOutDTO<BoletinDataEmpleador> consultaResumen(BoletinInDTO params) {
		
		return boletinDao.consultaResumen(params);
	}
	
	public Map<String, Object> obtenerMapaBLR(BoletinInDTO dataIn, BoletinOutDTO<BoletinDataEmpleador> dataOut) {
		LOG.debug("> Mapa BLR - RUT: " + dataIn.getRutAfectado());
		
		Map<String, Object> mapa = new HashMap<String, Object>();
		
		mapa.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		
		mapa.put("RUT", utils.formateaRUTSin0(dataIn.getRutAfectado()));
		mapa.put("NOMBRE", dataOut.getNombreInfractor());
		mapa.put("FECHA", utils.formatearFecha(new Date()));
		mapa.put("NUM_DEUDAS", dataOut.getNroDeudasLaboral());
		mapa.put("MTO_DEUDAS", dataOut.getMontoDeudaLaboral());
		mapa.put("FECHA_DEUDA_REC", utils.checkTimestampToStrFechaVista(dataOut.getFecUltimaDeuda()));
		mapa.put("NUM_INST_ACREE", dataOut.getNroAcreedores());
		mapa.put("NUM_MULTAS", dataOut.getNroMultasLaboral());
		mapa.put("MTO_MULTAS", dataOut.getMontoMultaLaboral());
		mapa.put("FECHA_MULTA_REC", utils.checkTimestampToStrFechaVista(dataOut.getFecUltimaMulta()));
		
		mapa.put("USUARIO_CC", dataIn.getCodUsuario() + " - " + dataIn.getCentroCosto());
		mapa.put("COD_AUTENTIC", dataOut.getCodAutenticBic());
		
		List<BoletinDataDetalle> listaResumen = new ArrayList<BoletinDataDetalle>();
		List<BoletinEmpleador_DP> listaDeudasDP = new ArrayList<BoletinEmpleador_DP>();
		List<BoletinDataDetalle> listaMultas = new ArrayList<BoletinDataDetalle>();
		
		if ( dataOut.getLista() != null && !dataOut.getLista().isEmpty() ) {
			List<BoletinDataEmpleador> listaDeudas = new ArrayList<BoletinDataEmpleador>();
			
			for ( int i=0; i < dataOut.getLista().size(); i++ ) {
				BoletinDataEmpleador row = dataOut.getLista().get(i);
				
				if ( row.getTipoDeudaLaboral().equals("D") ) {
					final String tmp = utils.checkTimestampToStrFechaVista(row.getFecBolLaboral());
					if ( tmp == null || tmp.isEmpty() ) {
						continue;
					}
					row.setFecBolLaboral(tmp);
					listaResumen.add(row);
				} else if ( row.getTipoDeudaLaboral().equals("I") ) {
					listaDeudas.add(row);
				} else if ( row.getTipoDeudaLaboral().equals("M") ) {
					row.setFecBolLaboral(utils.checkTimestampToStrFechaVista(row.getFecBolLaboral()));
					listaMultas.add(row);
				}
				
			}
			
			if ( listaDeudas != null && !listaDeudas.isEmpty() ) {
				String tmpInst = null;
				BoletinEmpleador_DP grupoInst = new BoletinEmpleador_DP();
				List<BoletinDataEmpleador> tabla = new ArrayList<BoletinDataEmpleador>();
				
				for ( int i=0; i < listaDeudas.size(); i++ ) {
					BoletinDataEmpleador row = listaDeudas.get(i);
					
					if ( i == 0 ) {
						addHeadDeudasDP(grupoInst, row);
					}
					
					if ( tmpInst != null && !tmpInst.equals(row.getNombreInstitucion()) ) {
						Collections.sort(tabla);
						grupoInst.setLista(tabla);
						listaDeudasDP.add(grupoInst);
						
						tabla = new ArrayList<BoletinDataEmpleador>();
						grupoInst = new BoletinEmpleador_DP();
						
						addHeadDeudasDP(grupoInst, row);
					}
					
					tmpInst = row.getNombreInstitucion();
					
					// Se deja en 1 tal cual SIAC antiguo, aprobado por cliente.
					row.setNroCotizaciones(1);
					
					tabla.add(row);
					
					if ( i == (listaDeudas.size()-1) ) {
						Collections.sort(tabla);
						grupoInst.setLista(tabla);
						listaDeudasDP.add(grupoInst);
					}
					
				}
				
			}
			
		}
		
		mapa.put("LISTA_RESUMEN", listaResumen);
		mapa.put("LISTA_MULTAS", listaMultas);
		mapa.put("COLECCION_PAG", listaDeudasDP);
		LOG.debug(mapa.toString());
		
		return mapa;
	}
	
	private void addHeadDeudasDP(BoletinEmpleador_DP grupo, BoletinDataDetalle row) {
		grupo.setInstitucion(row.getNombreInstitucion());
		grupo.setMotivo(row.getMotivoInfraccion());
		grupo.setBoletin(row.getNroBolLaboral());
		grupo.setPagina(row.getPagBolLaboral());
		grupo.setFecha(utils.checkTimestampToStrFechaVista(row.getFecBolLaboral()));
		grupo.setMeses(row.getMeses());
		grupo.setCotizaciones(row.getNroCotizaciones());
		grupo.setMontoAdeudado(row.getMontoAdeudado());
	}

	/**
	 * Revisa el Rut ingresado para formatearlo sin 0's a la izquierda
	 * Si es un Rut sin info en BD (999999999-6), no lo formatea
	 */
	protected String checkRutAFormatear(String strRut) {
		if ( strRut != null && !strRut.isEmpty() ) {
			if ( !strRut.equals(RUT_SIN_INFO) ) {
				return utils.formateaRUTSin0(strRut);
			}
			return strRut;
		}
		return new String();
	}

}
