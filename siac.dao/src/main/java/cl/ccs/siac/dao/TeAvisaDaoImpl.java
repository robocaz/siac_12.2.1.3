package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TeAvisaDTO;

@Named("teAvisaDao")
@Stateless
public class TeAvisaDaoImpl implements TeAvisaDao{

	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;
	
	private static final Logger LOG = LogManager.getLogger(ProtestosDaoImpl.class);
	
	
	
	public TeAvisaDTO contrataTeAvisa(TeAvisaDTO contrato) {
		
		try {
			
			LOG.info("Ejecutando SI_ServicioTBicTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_ServicioTBicTmp_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoMovimiento", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutContratante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomContratante", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_DireccionCompleta", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TelefonoCelular", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TelefonoDomicilio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Email", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTAvisa", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombreTAvisa", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoTA", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrContrato", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", contrato.getCorrCaja());
			storedProcedure.setParameter("Fld_CodCCosto", contrato.getCentroCosto());
			storedProcedure.setParameter("Fld_CodServicio", contrato.getTipoServicio());
			storedProcedure.setParameter("Fld_RutTramitante", contrato.getTramitante().getRut());
			storedProcedure.setParameter("Fld_RutAfectado", contrato.getAfectado().getRut());
			storedProcedure.setParameter("Fld_MontoMovimiento", contrato.getMontoServicio());
			storedProcedure.setParameter("Fld_RutContratante", contrato.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrNomContratante", contrato.getAfectado().getCorrelativo());
			storedProcedure.setParameter("Fld_DireccionCompleta", contrato.getDireccion());
			if(contrato.getTelcel() != null){
				storedProcedure.setParameter("Fld_TelefonoCelular", contrato.getTelcel());
			}
			else{
				storedProcedure.setParameter("Fld_TelefonoCelular", "");
			}
			if(contrato.getTelfijo() != null){
				storedProcedure.setParameter("Fld_TelefonoDomicilio", contrato.getTelfijo());
			}
			else{
				storedProcedure.setParameter("Fld_TelefonoDomicilio", "");
			}
			
			storedProcedure.setParameter("Fld_Email", contrato.getEmail());
			storedProcedure.setParameter("Fld_RutTAvisa", contrato.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrNombreTAvisa", contrato.getAfectado().getCorrelativo());
			
			if(contrato.getCorrContrato() == null || contrato.getCorrContrato()== 0){
				storedProcedure.setParameter("Fld_TipoTA", 0);
			}
			if(contrato.getCorrContrato() != null && contrato.getCorrContrato() > 0 && !contrato.isUpgrade()){
				storedProcedure.setParameter("Fld_TipoTA", 2);
			}
			if(contrato.getCorrContrato() != null && contrato.getCorrContrato() > 0 && contrato.isUpgrade()){
				storedProcedure.setParameter("Fld_TipoTA", 3);
			}

			
			if(contrato.getCorrContrato() != null && contrato.getCorrContrato() > 0){
				storedProcedure.setParameter("Fld_CorrContrato", contrato.getCorrContrato());
			}
			else{
				storedProcedure.setParameter("Fld_CorrContrato", 0L);
			}
			
			
			LOG.info(contrato.getCorrCaja());
			LOG.info(contrato.getCentroCosto());
			LOG.info(contrato.getTipoServicio());
			LOG.info(contrato.getTramitante().getRut());
			LOG.info(contrato.getAfectado().getRut());
			LOG.info(contrato.getMontoServicio());
			LOG.info(contrato.getAfectado().getRut());
			LOG.info(contrato.getAfectado().getCorrelativo());
			LOG.info(contrato.getDireccion());
			LOG.info(contrato.getTelcel());
			LOG.info(contrato.getTelfijo());
			LOG.info(contrato.getEmail());
			LOG.info(contrato.getAfectado().getRut());
			LOG.info(contrato.getAfectado().getCorrelativo());
			LOG.info(1);
			LOG.info(0L);
			
			storedProcedure.execute();
			
			contrato.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			contrato.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			
			
		}
		catch(Exception e){
			e.printStackTrace();
			contrato.setIdControl(1);
			LOG.error("Error en el SP SI_ServicioTBicTmp_SW2 " + e.getMessage());
		}
		return contrato;
		
	}
	
	
	
	
	public ResponseDTO validaContratoTeAvisa(String rutAvisa){
		
		ResponseDTO respuesta = new ResponseDTO();
		
		try {
			LOG.info("Ejecutando SQ_ValidaContratoABI2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValidaContratoABI2_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutContratante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTAvisa", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_ExisteContrato", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoContrato", Integer.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_RutContratante", rutAvisa);
			storedProcedure.setParameter("Fld_RutTAvisa", rutAvisa);
			
			storedProcedure.execute();
			
			
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_ExisteContrato").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_TipoContrato").toString());
			return respuesta;
		}
		
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_ValidaContratoABI2_SW2 " + e.getMessage());
			return null;
		}
		
		
		
		
	}
	
	
	
	public List<TeAvisaDTO> buscaContratos(String rut){
		
		List<TeAvisaDTO> contratosLista = new ArrayList<TeAvisaDTO>();
		
		try{
			
			LOG.info("Ejecutando SQ_ContratosTA_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ContratosTA_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			
			
			storedProcedure.setParameter("Fld_Rut", rut);
			
			boolean result = storedProcedure.execute();

			if (result) {
				List<Object[]> contratos = storedProcedure.getResultList();
				for (Object item : contratos) {
					Object[] datos = (Object[]) item;
					TeAvisaDTO contrato = new TeAvisaDTO();
					contrato.setGlosaServicio(datos[0].toString());
					PersonaDTO contratante = new PersonaDTO();
					contratante.setRut(datos[1].toString());
					contratante.setCorrelativo(Long.parseLong(datos[2].toString()));
					contratante.setNombres(datos[20].toString());
					contratante.setApellidoPaterno(datos[18].toString());
					contratante.setApellidoMaterno(datos[19].toString());
					contrato.setContratante(contratante);

					PersonaDTO personaAvisa = new PersonaDTO();
					personaAvisa.setRut(datos[4].toString());
					
					String[] nomCompleto = datos[5].toString().split(" ");
					personaAvisa.setNombres(datos[20].toString());
					personaAvisa.setApellidoPaterno(datos[18].toString());
					personaAvisa.setApellidoMaterno(datos[19].toString());
					contrato.setPersTeAvisa(personaAvisa);
					
					String[] fechaArrayIni = datos[6].toString().substring(0,10).split("-");
					contrato.setFechaInicioContrato(fechaArrayIni[2]+"-"+fechaArrayIni[1]+"-"+fechaArrayIni[0]);
					String[] fechaArrayFin = datos[7].toString().substring(0,10).split("-");
					contrato.setFechaFinContrato(fechaArrayFin[2]+"-"+fechaArrayFin[1]+"-"+fechaArrayFin[0]);
					contrato.setEmail(datos[8].toString());
					contrato.setTelcel(datos[9].toString());
					contrato.setDireccion(datos[10].toString());
					contrato.setTelfijo(datos[11].toString());
					contrato.setServcorreo(Boolean.parseBoolean(datos[12].toString()));
					contrato.setServsms(Boolean.parseBoolean(datos[13].toString()));
					contrato.setCorrContrato(Long.parseLong(datos[14].toString()));
					//Afectado
					PersonaDTO afectado = new PersonaDTO();
					afectado.setApellidoMaterno(datos[16].toString());
					afectado.setApellidoPaterno(datos[18].toString());
					afectado.setNombres(datos[17].toString());
					afectado.setRut(datos[1].toString());
					afectado.setCorrelativo(Long.parseLong(datos[2].toString()));
					contrato.setAfectado(afectado);
					
					if(contrato.getServcorreo() && contrato.getServsms()){
						contrato.setTipoServicio("ADM");
					}

					if(!contrato.getServcorreo() && contrato.getServsms()){
						contrato.setTipoServicio("AMM");
					}
					
					if(contrato.getServcorreo() && !contrato.getServsms()){
						contrato.setTipoServicio("ARM");
					}
					contratosLista.add(contrato);
					
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_ContratosTA_SW2 " + e.getMessage());
			return null;
		}		
		return contratosLista;
		
	}
	
	
	
	
	
	public List<TeAvisaDTO> buscaContratosRenovacion(String rut, Integer criterio, Long corrContrato){
		
		
		List<TeAvisaDTO> contratosLista = new ArrayList<TeAvisaDTO>();
		
		try{
			
			LOG.info("Ejecutando SQ_BuscaContratosTA_SW2");
			
			StoredProcedureQuery storedProcedure = emlink.createStoredProcedureQuery("SQ_BuscaContratosTA_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrContrato", Integer.class, ParameterMode.IN);
			
			LOG.info("RUT " + rut);
			LOG.info("CRITERIO " + criterio);
			LOG.info("CORRELATIVO " + corrContrato);

			storedProcedure.setParameter("Fld_Rut", rut);
			storedProcedure.setParameter("Fld_Criterio", criterio);
			storedProcedure.setParameter("Fld_RutAfectado", rut);
			if(corrContrato > 0){
				storedProcedure.setParameter("Fld_CorrContrato", corrContrato.intValue());
			}
			else{
				storedProcedure.setParameter("Fld_CorrContrato", 0);
			}
			
	
			
			boolean result = storedProcedure.execute();

			if (result) {
				List<Object[]> contratos = storedProcedure.getResultList();
				for (Object item : contratos) {
					Object[] datos = (Object[]) item;
					TeAvisaDTO contrato = new TeAvisaDTO();
					contrato.setAvisoRenovacion(Integer.parseInt(datos[0].toString()));
					contrato.setCorrContrato(Long.parseLong(datos[1].toString()));
					contrato.setCodServicioAux(datos[2].toString());
					contrato.setGlosaServicio(datos[3].toString());
					PersonaDTO contratante = new PersonaDTO();
					contratante.setRut(datos[4].toString());
					contratante.setCorrelativo(Long.parseLong(datos[5].toString()));
					contratante.setApellidoPaterno(datos[7].toString());
					contratante.setApellidoMaterno(datos[8].toString());
					contratante.setNombres(datos[9].toString());
					contrato.setAfectado(contratante);

					PersonaDTO personaAvisa = new PersonaDTO();
					personaAvisa.setRut(datos[10].toString());
					personaAvisa.setCorrelativo(Long.parseLong(datos[11].toString()));
					personaAvisa.setApellidoPaterno(datos[13].toString());
					personaAvisa.setApellidoMaterno(datos[14].toString());
					personaAvisa.setNombres(datos[15].toString());
					contrato.setPersTeAvisa(personaAvisa);
					String[] fechaArrayIni = datos[16].toString().substring(0,10).split("-");
					contrato.setFechaInicioContrato(fechaArrayIni[2]+"-"+fechaArrayIni[1]+"-"+fechaArrayIni[0]);
					String[] fechaArrayFin = datos[17].toString().substring(0,10).split("-");
					contrato.setFechaFinContrato(fechaArrayFin[2]+"-"+fechaArrayFin[1]+"-"+fechaArrayFin[0]);
					contrato.setDireccion(datos[18].toString());
					contrato.setTelfijo(datos[19].toString());
					contrato.setTelcel(datos[20].toString());
					contrato.setEmail(datos[21].toString());
					
					if(contrato.getCodServicioAux().equals("ADM")){
						contrato.setServcorreo(true);
						contrato.setServsms(true);
					}
					if(contrato.getCodServicioAux().equals("ARM")){
						contrato.setServcorreo(true);
						contrato.setServsms(false);
					}				
					if(contrato.getCodServicioAux().equals("AMM")){
						contrato.setServcorreo(false);
						contrato.setServsms(true);
					}
					contratosLista.add(contrato);
					
				}
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_BuscaContratosTA_SW2 " + e.getMessage());
			return null;
		}		
		return contratosLista;
		
	}
	
	
	public Long costoServicioUpgrade(Long corrContrato, String codServicio){
		
		Long costoUpgrade = 0L;
		
		try {
			LOG.info("Ejecutando SQ_CostoServicioUpg_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CostoServicioUpg_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrContrato", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CostoServicio", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrContrato", corrContrato);
			storedProcedure.setParameter("Fld_CodServicio", codServicio);
			
			storedProcedure.execute();
			
			
			costoUpgrade = Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CostoServicio").toString());
			
			
			return costoUpgrade;
		}
		
		catch(Exception e){
			e.printStackTrace();
			LOG.error("Error en el SP SQ_CostoServicioUpg_SW2 " + e.getMessage());
			return null;
		}
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
}
