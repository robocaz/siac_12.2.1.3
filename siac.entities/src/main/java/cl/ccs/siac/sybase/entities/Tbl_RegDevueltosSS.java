package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RegDevueltosSS database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RegDevueltosSS.findAll", query="SELECT t FROM Tbl_RegDevueltosSS t")
public class Tbl_RegDevueltosSS implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CausaQuiebra")
	private String fld_CausaQuiebra;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodMonedaOriginal")
	private byte fld_CodMonedaOriginal;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodTribunal")
	private String fld_CodTribunal;

	@Column(name="Fld_CodUsuarioDigitacion")
	private String fld_CodUsuarioDigitacion;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecDeclaratoria")
	private Timestamp fld_FecDeclaratoria;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecPublicacionDiarioOfic")
	private Timestamp fld_FecPublicacionDiarioOfic;

	@Column(name="Fld_FecPublicacionFiscalia")
	private Timestamp fld_FecPublicacionFiscalia;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_FolioProt")
	private int fld_FolioProt;

	@Column(name="Fld_GlosaSindico")
	private String fld_GlosaSindico;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NomAfectado_ApMat")
	private String fld_NomAfectado_ApMat;

	@Column(name="Fld_NomAfectado_ApPat")
	private String fld_NomAfectado_ApPat;

	@Column(name="Fld_NomAfectado_Nombres")
	private String fld_NomAfectado_Nombres;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_NroPublicacionDiarioOfic")
	private int fld_NroPublicacionDiarioOfic;

	@Column(name="Fld_RolQuiebra")
	private String fld_RolQuiebra;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_SdoCapital")
	private BigDecimal fld_SdoCapital;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_RegDevueltosSS() {
	}

	public String getFld_CausaQuiebra() {
		return this.fld_CausaQuiebra;
	}

	public void setFld_CausaQuiebra(String fld_CausaQuiebra) {
		this.fld_CausaQuiebra = fld_CausaQuiebra;
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_CodMonedaOriginal() {
		return this.fld_CodMonedaOriginal;
	}

	public void setFld_CodMonedaOriginal(byte fld_CodMonedaOriginal) {
		this.fld_CodMonedaOriginal = fld_CodMonedaOriginal;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodTribunal() {
		return this.fld_CodTribunal;
	}

	public void setFld_CodTribunal(String fld_CodTribunal) {
		this.fld_CodTribunal = fld_CodTribunal;
	}

	public String getFld_CodUsuarioDigitacion() {
		return this.fld_CodUsuarioDigitacion;
	}

	public void setFld_CodUsuarioDigitacion(String fld_CodUsuarioDigitacion) {
		this.fld_CodUsuarioDigitacion = fld_CodUsuarioDigitacion;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecDeclaratoria() {
		return this.fld_FecDeclaratoria;
	}

	public void setFld_FecDeclaratoria(Timestamp fld_FecDeclaratoria) {
		this.fld_FecDeclaratoria = fld_FecDeclaratoria;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecPublicacionDiarioOfic() {
		return this.fld_FecPublicacionDiarioOfic;
	}

	public void setFld_FecPublicacionDiarioOfic(Timestamp fld_FecPublicacionDiarioOfic) {
		this.fld_FecPublicacionDiarioOfic = fld_FecPublicacionDiarioOfic;
	}

	public Timestamp getFld_FecPublicacionFiscalia() {
		return this.fld_FecPublicacionFiscalia;
	}

	public void setFld_FecPublicacionFiscalia(Timestamp fld_FecPublicacionFiscalia) {
		this.fld_FecPublicacionFiscalia = fld_FecPublicacionFiscalia;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public int getFld_FolioProt() {
		return this.fld_FolioProt;
	}

	public void setFld_FolioProt(int fld_FolioProt) {
		this.fld_FolioProt = fld_FolioProt;
	}

	public String getFld_GlosaSindico() {
		return this.fld_GlosaSindico;
	}

	public void setFld_GlosaSindico(String fld_GlosaSindico) {
		this.fld_GlosaSindico = fld_GlosaSindico;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_NomAfectado_ApMat() {
		return this.fld_NomAfectado_ApMat;
	}

	public void setFld_NomAfectado_ApMat(String fld_NomAfectado_ApMat) {
		this.fld_NomAfectado_ApMat = fld_NomAfectado_ApMat;
	}

	public String getFld_NomAfectado_ApPat() {
		return this.fld_NomAfectado_ApPat;
	}

	public void setFld_NomAfectado_ApPat(String fld_NomAfectado_ApPat) {
		this.fld_NomAfectado_ApPat = fld_NomAfectado_ApPat;
	}

	public String getFld_NomAfectado_Nombres() {
		return this.fld_NomAfectado_Nombres;
	}

	public void setFld_NomAfectado_Nombres(String fld_NomAfectado_Nombres) {
		this.fld_NomAfectado_Nombres = fld_NomAfectado_Nombres;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public int getFld_NroPublicacionDiarioOfic() {
		return this.fld_NroPublicacionDiarioOfic;
	}

	public void setFld_NroPublicacionDiarioOfic(int fld_NroPublicacionDiarioOfic) {
		this.fld_NroPublicacionDiarioOfic = fld_NroPublicacionDiarioOfic;
	}

	public String getFld_RolQuiebra() {
		return this.fld_RolQuiebra;
	}

	public void setFld_RolQuiebra(String fld_RolQuiebra) {
		this.fld_RolQuiebra = fld_RolQuiebra;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public BigDecimal getFld_SdoCapital() {
		return this.fld_SdoCapital;
	}

	public void setFld_SdoCapital(BigDecimal fld_SdoCapital) {
		this.fld_SdoCapital = fld_SdoCapital;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}