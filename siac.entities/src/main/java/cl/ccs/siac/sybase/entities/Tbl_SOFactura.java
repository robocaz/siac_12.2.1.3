package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SOFactura database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SOFactura.findAll", query="SELECT t FROM Tbl_SOFactura t")
public class Tbl_SOFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodOrigen")
	private String fld_CodOrigen;

	@Column(name="Fld_CorrOffline")
	private BigDecimal fld_CorrOffline;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_MontoBruto")
	private BigDecimal fld_MontoBruto;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroFactura")
	private int fld_NroFactura;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	public Tbl_SOFactura() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodOrigen() {
		return this.fld_CodOrigen;
	}

	public void setFld_CodOrigen(String fld_CodOrigen) {
		this.fld_CodOrigen = fld_CodOrigen;
	}

	public BigDecimal getFld_CorrOffline() {
		return this.fld_CorrOffline;
	}

	public void setFld_CorrOffline(BigDecimal fld_CorrOffline) {
		this.fld_CorrOffline = fld_CorrOffline;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public BigDecimal getFld_MontoBruto() {
		return this.fld_MontoBruto;
	}

	public void setFld_MontoBruto(BigDecimal fld_MontoBruto) {
		this.fld_MontoBruto = fld_MontoBruto;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public int getFld_NroFactura() {
		return this.fld_NroFactura;
	}

	public void setFld_NroFactura(int fld_NroFactura) {
		this.fld_NroFactura = fld_NroFactura;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

}