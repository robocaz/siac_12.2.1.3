package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdTipoDomicilios database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdTipoDomicilios")
@NamedQuery(name="Tp_CeIdTipoDomicilio.findAll", query="SELECT t FROM Tp_CeIdTipoDomicilio t")
public class Tp_CeIdTipoDomicilio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoDomicilio")
	private String fld_GlosaTipoDomicilio;

	@Column(name="Fld_TipoDomicilio")
	private byte fld_TipoDomicilio;

	public Tp_CeIdTipoDomicilio() {
	}

	public String getFld_GlosaTipoDomicilio() {
		return this.fld_GlosaTipoDomicilio;
	}

	public void setFld_GlosaTipoDomicilio(String fld_GlosaTipoDomicilio) {
		this.fld_GlosaTipoDomicilio = fld_GlosaTipoDomicilio;
	}

	public byte getFld_TipoDomicilio() {
		return this.fld_TipoDomicilio;
	}

	public void setFld_TipoDomicilio(byte fld_TipoDomicilio) {
		this.fld_TipoDomicilio = fld_TipoDomicilio;
	}

}