package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutmuestra database table.
 * 
 */
@Entity
@Table(name="tmp_rutmuestra")
@NamedQuery(name="TmpRutmuestra.findAll", query="SELECT t FROM TmpRutmuestra t")
public class TmpRutmuestra implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rut;

	public TmpRutmuestra() {
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}