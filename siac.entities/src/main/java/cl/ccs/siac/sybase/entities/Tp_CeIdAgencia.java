package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdAgencias database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdAgencias")
@NamedQuery(name="Tp_CeIdAgencia.findAll", query="SELECT t FROM Tp_CeIdAgencia t")
public class Tp_CeIdAgencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAgencia")
	private short fld_CodAgencia;

	@Column(name="Fld_CodUsuarioResp")
	private String fld_CodUsuarioResp;

	@Column(name="Fld_GlosaAgencia")
	private String fld_GlosaAgencia;

	public Tp_CeIdAgencia() {
	}

	public short getFld_CodAgencia() {
		return this.fld_CodAgencia;
	}

	public void setFld_CodAgencia(short fld_CodAgencia) {
		this.fld_CodAgencia = fld_CodAgencia;
	}

	public String getFld_CodUsuarioResp() {
		return this.fld_CodUsuarioResp;
	}

	public void setFld_CodUsuarioResp(String fld_CodUsuarioResp) {
		this.fld_CodUsuarioResp = fld_CodUsuarioResp;
	}

	public String getFld_GlosaAgencia() {
		return this.fld_GlosaAgencia;
	}

	public void setFld_GlosaAgencia(String fld_GlosaAgencia) {
		this.fld_GlosaAgencia = fld_GlosaAgencia;
	}

}