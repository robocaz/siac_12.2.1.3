package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditSolClaves database table.
 * 
 */
@Entity
@Table(name="Tbl_AuditSolClaves")
@NamedQuery(name="Tbl_AuditSolClave.findAll", query="SELECT t FROM Tbl_AuditSolClave t")
public class Tbl_AuditSolClave implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrRegistro")
	private BigDecimal fld_CorrRegistro;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	public Tbl_AuditSolClave() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrRegistro() {
		return this.fld_CorrRegistro;
	}

	public void setFld_CorrRegistro(BigDecimal fld_CorrRegistro) {
		this.fld_CorrRegistro = fld_CorrRegistro;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

}