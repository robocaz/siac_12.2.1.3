package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolDetVtaDetalles database table.
 * 
 */
@Entity
@Table(name="Tbl_SolDetVtaDetalles")
@NamedQuery(name="Tbl_SolDetVtaDetalle.findAll", query="SELECT t FROM Tbl_SolDetVtaDetalle t")
public class Tbl_SolDetVtaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrMail")
	private BigDecimal fld_CorrMail;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	public Tbl_SolDetVtaDetalle() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrMail() {
		return this.fld_CorrMail;
	}

	public void setFld_CorrMail(BigDecimal fld_CorrMail) {
		this.fld_CorrMail = fld_CorrMail;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

}