package cl.exe.ccs.dto;

import java.io.Serializable;

public class DetalleFacturaDTO extends ResponseDTO implements Serializable{

	
	
	private Integer nroBoletaFactura;
    private String fechaEmision;
    private String razonSocial;
    private String atencion;
    private String idGiro;
    private String ubicacion;
    private String rutAfectado;
    private String codOrigen;
    private Long montoNeto;
    private Long montoIva;
    
    
	public Integer getNroBoletaFactura() {
		return nroBoletaFactura;
	}
	public void setNroBoletaFactura(Integer nroBoletaFactura) {
		this.nroBoletaFactura = nroBoletaFactura;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getAtencion() {
		return atencion;
	}
	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}
	public String getIdGiro() {
		return idGiro;
	}
	public void setIdGiro(String idGiro) {
		this.idGiro = idGiro;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getCodOrigen() {
		return codOrigen;
	}
	public void setCodOrigen(String codOrigen) {
		this.codOrigen = codOrigen;
	}
	public Long getMontoNeto() {
		return montoNeto;
	}
	public void setMontoNeto(Long montoNeto) {
		this.montoNeto = montoNeto;
	}
	public Long getMontoIva() {
		return montoIva;
	}
	public void setMontoIva(Long montoIva) {
		this.montoIva = montoIva;
	}

    
    
	
	
}
