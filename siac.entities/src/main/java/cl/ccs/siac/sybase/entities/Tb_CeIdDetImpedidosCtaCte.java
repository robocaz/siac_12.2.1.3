package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetImpedidosCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetImpedidosCtaCte.findAll", query="SELECT t FROM Tb_CeIdDetImpedidosCtaCte t")
public class Tb_CeIdDetImpedidosCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETIMPEDIDOSCTACTE_FLD_CORRIMPEDIDO_ID_GENERATOR", sequenceName="FLD_CORRIMPEDIDO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETIMPEDIDOSCTACTE_FLD_CORRIMPEDIDO_ID_GENERATOR")
	@Column(name="Fld_CorrImpedido_Id")
	private long fld_CorrImpedido_Id;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecCircular")
	private Timestamp fld_FecCircular;

	@Column(name="Fld_FecFinal")
	private Timestamp fld_FecFinal;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_NroCircular")
	private short fld_NroCircular;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdDetErrImpedCtaCte
	@OneToMany(mappedBy="tbCeIdDetImpedidosCtaCte")
	private List<Tb_CeIdDetErrImpedCtaCte> tbCeIdDetErrImpedCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	public Tb_CeIdDetImpedidosCtaCte() {
	}

	public long getFld_CorrImpedido_Id() {
		return this.fld_CorrImpedido_Id;
	}

	public void setFld_CorrImpedido_Id(long fld_CorrImpedido_Id) {
		this.fld_CorrImpedido_Id = fld_CorrImpedido_Id;
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecCircular() {
		return this.fld_FecCircular;
	}

	public void setFld_FecCircular(Timestamp fld_FecCircular) {
		this.fld_FecCircular = fld_FecCircular;
	}

	public Timestamp getFld_FecFinal() {
		return this.fld_FecFinal;
	}

	public void setFld_FecFinal(Timestamp fld_FecFinal) {
		this.fld_FecFinal = fld_FecFinal;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public short getFld_NroCircular() {
		return this.fld_NroCircular;
	}

	public void setFld_NroCircular(short fld_NroCircular) {
		this.fld_NroCircular = fld_NroCircular;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public List<Tb_CeIdDetErrImpedCtaCte> getTbCeIdDetErrImpedCtaCtes() {
		return this.tbCeIdDetErrImpedCtaCtes;
	}

	public void setTbCeIdDetErrImpedCtaCtes(List<Tb_CeIdDetErrImpedCtaCte> tbCeIdDetErrImpedCtaCtes) {
		this.tbCeIdDetErrImpedCtaCtes = tbCeIdDetErrImpedCtaCtes;
	}

	public Tb_CeIdDetErrImpedCtaCte addTbCeIdDetErrImpedCtaCte(Tb_CeIdDetErrImpedCtaCte tbCeIdDetErrImpedCtaCte) {
		getTbCeIdDetErrImpedCtaCtes().add(tbCeIdDetErrImpedCtaCte);
		tbCeIdDetErrImpedCtaCte.setTbCeIdDetImpedidosCtaCte(this);

		return tbCeIdDetErrImpedCtaCte;
	}

	public Tb_CeIdDetErrImpedCtaCte removeTbCeIdDetErrImpedCtaCte(Tb_CeIdDetErrImpedCtaCte tbCeIdDetErrImpedCtaCte) {
		getTbCeIdDetErrImpedCtaCtes().remove(tbCeIdDetErrImpedCtaCte);
		tbCeIdDetErrImpedCtaCte.setTbCeIdDetImpedidosCtaCte(null);

		return tbCeIdDetErrImpedCtaCte;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

}