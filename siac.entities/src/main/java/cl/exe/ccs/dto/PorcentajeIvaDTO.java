package cl.exe.ccs.dto;

import java.io.Serializable;

public class PorcentajeIvaDTO extends ResponseDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6100671564005420724L;
	
	
	private Float porcentaje;

	public Float getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Float porcentaje) {
		this.porcentaje = porcentaje;
	} 
	
	

}
