package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ResumenMovimientoDTO extends ResponseDTO implements Serializable{
	
	
	private Integer totalTransacciones;
	private Integer totalMovimientos;
	private Integer totalAviPago;
	private Long totalPagado;
	private Long totalDiferencia;
	private Integer totalBoletas;
	private Integer totalFacturas;
	private List<MovimientoDTO> movimientos;
	private String fechaDesde;
	private String fechaHasta;
	private TotalesCuadraturaDTO totales;
	private String glosaCC;
	private Integer centroCosto;
	private String usuario;
	private Integer corrCaja;
	
	
	
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getGlosaCC() {
		return glosaCC;
	}
	public void setGlosaCC(String glosaCC) {
		this.glosaCC = glosaCC;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public TotalesCuadraturaDTO getTotales() {
		return totales;
	}
	public void setTotales(TotalesCuadraturaDTO totales) {
		this.totales = totales;
	}
	public Integer getTotalTransacciones() {
		return totalTransacciones;
	}
	public void setTotalTransacciones(Integer totalTransacciones) {
		this.totalTransacciones = totalTransacciones;
	}
	public Integer getTotalMovimientos() {
		return totalMovimientos;
	}
	public void setTotalMovimientos(Integer totalMovimientos) {
		this.totalMovimientos = totalMovimientos;
	}
	public Integer getTotalAviPago() {
		return totalAviPago;
	}
	public void setTotalAviPago(Integer totalAviPago) {
		this.totalAviPago = totalAviPago;
	}
	public Long getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(Long totalPagado) {
		this.totalPagado = totalPagado;
	}
	public Long getTotalDiferencia() {
		return totalDiferencia;
	}
	public void setTotalDiferencia(Long totalDiferencia) {
		this.totalDiferencia = totalDiferencia;
	}
	public Integer getTotalBoletas() {
		return totalBoletas;
	}
	public void setTotalBoletas(Integer totalBoletas) {
		this.totalBoletas = totalBoletas;
	}
	public Integer getTotalFacturas() {
		return totalFacturas;
	}
	public void setTotalFacturas(Integer totalFacturas) {
		this.totalFacturas = totalFacturas;
	}
	public List<MovimientoDTO> getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(List<MovimientoDTO> movimientos) {
		this.movimientos = movimientos;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
	
}
