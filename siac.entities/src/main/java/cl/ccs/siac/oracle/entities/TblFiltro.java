package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_FILTROS database table.
 * 
 */
@Entity
@Table(name="TBL_FILTROS")
@NamedQuery(name="TblFiltro.findAll", query="SELECT t FROM TblFiltro t")
public class TblFiltro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODIGO")
	private BigDecimal fldCodigo;

	@Column(name="FLD_GLOSA")
	private String fldGlosa;

	public TblFiltro() {
	}

	public BigDecimal getFldCodigo() {
		return this.fldCodigo;
	}

	public void setFldCodigo(BigDecimal fldCodigo) {
		this.fldCodigo = fldCodigo;
	}

	public String getFldGlosa() {
		return this.fldGlosa;
	}

	public void setFldGlosa(String fldGlosa) {
		this.fldGlosa = fldGlosa;
	}

}