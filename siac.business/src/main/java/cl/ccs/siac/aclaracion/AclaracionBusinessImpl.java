package cl.ccs.siac.aclaracion;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.AclaracionDao;
import cl.ccs.siac.sybase.entities.Tbl_Firma;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.AclaracionDTO;
import cl.exe.ccs.dto.BipersonalDTO;
import cl.exe.ccs.dto.ConsultaCAADTO;
import cl.exe.ccs.dto.DetalleFirmaDTO;
import cl.exe.ccs.dto.FirmanteDTO;
import cl.exe.ccs.dto.NumeroConfirmatorioDTO;
import cl.exe.ccs.dto.NumeroFirmasDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.VentaCarteraDTO;

@Named("aclaracionBusiness")
public class AclaracionBusinessImpl implements AclaracionBusiness {

	@Inject
	private AclaracionDao aclaracionDao;

	@Inject
	private Utils utils;

	public ResponseListDTO<DetalleFirmaDTO> consultaFirmas(DetalleFirmaDTO detalle) {
		
		if(null != detalle.getFirmante()){
			detalle.getFirmante().setRut(utils.formatZeroRut(detalle.getFirmante().getRut()));
			if (detalle.getFirmante().getRut() != null && !detalle.getFirmante().getRut().equals("")) {
				detalle.setCriterio(0);
			} else if (detalle.getFirmante().getApellidoPaterno() != null && !detalle.getFirmante().getApellidoPaterno().equals("")) {
				detalle.setCriterio(1);
				if(detalle.getCodTipoDoc().equals("CT")){
					detalle.setCriterio(3);
				}
			}else{
				detalle.setCriterio(2);
			}
		}else{
			detalle.setFirmante(new FirmanteDTO());
			detalle.setCriterio(2);
		}
		return aclaracionDao.consultaFirmas(detalle);

	}

	public Tbl_Firma obtieneFirma(Long corrFirma) {
		return aclaracionDao.obtieneFirma(corrFirma);
	}

	public NumeroConfirmatorioDTO confimaNumero(NumeroConfirmatorioDTO numero) {
		return aclaracionDao.confimaNumero(numero);
	}

	public NumeroFirmasDTO obtieneConfirmatorio(NumeroFirmasDTO numero) {
		return aclaracionDao.obtieneConfirmatorio(numero);
	}

	public AclaracionDTO aceptaAclaracion(AclaracionDTO aclaracion) {

		aclaracion.getAfectado().setRut(utils.formatZeroRut(aclaracion.getAfectado().getRut()));
		aclaracion.getTramitante().setRut(utils.formatZeroRut(aclaracion.getTramitante().getRut()));
		String listaRut = "";
		for (DetalleFirmaDTO firmante : aclaracion.getListafirmantes()) {
			listaRut = listaRut.concat(firmante.getFirmante().getRut()).concat(";");
		}

		if (!listaRut.equals("")) {
			listaRut = listaRut.substring(0, listaRut.length() - 1)+";";
		}
		aclaracion.setListaRut(listaRut);

		aclaracion = aclaracionDao.aceptaAclaracion(aclaracion);

		if (aclaracion.getCantFilas() == 1) {
			if (aclaracion.getCodRetAcl() == 0 && aclaracion.getMarcaAclaracion() == 1) {
				aclaracion.setCodServicio("ACL");
				aclaracion = this.aclaracionTemp(aclaracion);
				return aclaracion;
			} else if(aclaracion.getCodRetAcl() == 0 && aclaracion.getMarcaAclaracion() == 2 ) {
				aclaracion.setCodServicio("OMI");
				aclaracion = this.aclaracionTemp(aclaracion);
				return aclaracion;
			}else {

				return aclaracion;
			}
		}

		else if (aclaracion.getCantFilas() != 1) {

			List<AclaracionDTO> candidatos = this.listaCandidatos(aclaracion);
			System.out.println(candidatos.size());
			aclaracion.setCandidatos(candidatos);

		}
		return aclaracion;
	}

	public AclaracionDTO aclaracionTemp(AclaracionDTO aclaracion) {

		return aclaracionDao.aclaracionTemp(aclaracion);
	}

	public List<AclaracionDTO> listaCandidatos(AclaracionDTO aclaracion) {
		return aclaracionDao.listaCandidatos(aclaracion);
	}

	public AclaracionDTO desDuplica(AclaracionDTO aclaracion) {

		aclaracion = aclaracionDao.desDuplica(aclaracion);

		if (aclaracion.getIdControl() == 0) {

			if (aclaracion.getCodEstado() == 12) {
				aclaracion.setMarcaAclaracion(1L);
				aclaracion.setCodServicio("ACL");
			} else {
				aclaracion.setMarcaAclaracion(2L);
				aclaracion.setCodServicio("OMI");
			}
			return aclaracion;
		} else {
			return null;
		}

	}

	public List<AclaracionDTO> buscaCuotasBancoEstado(AclaracionDTO aclaracion) {
		return aclaracionDao.buscaCuotasBancoEstado(aclaracion);
	}

	public AclaracionDTO tieneVentaCartera(AclaracionDTO aclaracion) {

		aclaracion = aclaracionDao.tieneVentaCartera(aclaracion);


		if (aclaracion.getVentaCartera().getFlagVentaCartera() == 1) {
			if (aclaracion.getTipodocumento().getCodigo().equals("CM")) {
				switch (aclaracion.getNumerofirmas().getTipoDocAclaracion()) {
				case "CT":
				case "AP":
					aclaracion.getVentaCartera().setMensaje("Este Protesto Posee Venta de Cartera, Favor Ingrese datos del Emisor Comprador del Protesto");
					aclaracion.getVentaCartera().setFlagDespliegue(1);
					break;
				case "PC":
					aclaracion.getVentaCartera().setMensaje("Advertencia : Este Protesto Posee Venta de Cartera");
					aclaracion.getVentaCartera().setFlagDespliegue(0);					
					break;
				case "PJ":
					aclaracion.getVentaCartera().setMensaje("Advertencia : Este Protesto Posee Venta de Cartera");
					aclaracion.getVentaCartera().setFlagDespliegue(0);
					break;
				default:
					aclaracion.getVentaCartera().setMensaje(
							"Este Protesto Posee Venta de Cartera, No puede Aclarar con esta Documentación Presentada");
					aclaracion.getVentaCartera().setFlagDespliegue(0);
					break;
				}

			}
			else if (aclaracion.getTipodocumento().getCodigo().equals("LT")	|| aclaracion.getTipodocumento().getCodigo().equals("PG")) {
				switch (aclaracion.getNumerofirmas().getTipoDocAclaracion()) {
				case "CT":
					aclaracion.getVentaCartera().setMensaje("Este Protesto Posee Venta de Cartera, Favor Ingrese datos del Emisor Comprador del Protesto");
					aclaracion.getVentaCartera().setFlagDespliegue(1);
					break;
				case "DO":
				case "PC":
				case "PJ":
					aclaracion.getVentaCartera().setMensaje("Advertencia : Este Protesto Posee Venta de Cartera");
					aclaracion.getVentaCartera().setFlagDespliegue(0);
					break;
				default:
					aclaracion.getVentaCartera().setMensaje(
							"Este Protesto Posee Venta de Cartera, No puede Aclarar con esta Documentación Presentada");
					aclaracion.getVentaCartera().setFlagDespliegue(0);
					break;
			}
			}else{
				aclaracion.getVentaCartera().setFlagDespliegue(0);
			}
		}
		return aclaracion;
	}

	public VentaCarteraDTO buscaProtestoVentaCartera(Long corrProt, String tipoEmisor, String emisor) {
		return aclaracionDao.buscaProtestoVentaCartera(corrProt, tipoEmisor, emisor);
	}

	public ResponseDTO eliminaProtestoTemp(Long corrProt, Long corrCaja) {
		return aclaracionDao.eliminaProtestoTemp(corrProt, corrCaja);
	}
	
	
	public ResponseDTO guardaBipersonalTemp(BipersonalDTO bipersonal){
		bipersonal.setRut(utils.formatZeroRut(bipersonal.getRut()));
		return aclaracionDao.guardaBipersonalTemp(bipersonal);
	}
	
	
	public ResponseListDTO<PersonaDTO> listaAclaraciones(Long corrCaja, String rutTramitante){
		rutTramitante = utils.formatZeroRut(rutTramitante);
		return aclaracionDao.listaAclaraciones(corrCaja, rutTramitante);
	}

	public ResponseListDTO<ConsultaCAADTO> consultaRutFecha(ConsultaCAADTO consulta){
		return aclaracionDao.consultaRutFecha(consulta);
	}
	
	public AclaracionDTO aclaracionesParaCertificado(AclaracionDTO aclaracion){
		aclaracion = aclaracionDao.aclaracionesParaCertificado(aclaracion);
		if( aclaracion.getIdControl() == 0 && aclaracion.getCandidatos().size() > 0 ) {
			for( Integer i=0; i < aclaracion.getCandidatos().size(); i++ ) {
				aclaracion.getCandidatos().get(i).setFechaprotesto(utils.formatearFecha(aclaracion.getCandidatos().get(i).getFechaprotesto()));
				aclaracion.getCandidatos().get(i).setFechaAclaracion(utils.formatearFecha(aclaracion.getCandidatos().get(i).getFechaAclaracion()));
				aclaracion.getCandidatos().get(i).setFecPublicacionString(utils.formatearFecha(aclaracion.getCandidatos().get(i).getFecPublicacionString()));
			}
		}
		return aclaracion;
	}
	
	public ResponseDTO validaAclaracionCarro(AclaracionDTO aclaracion){
		
		aclaracion.getAfectado().setRut(utils.formatZeroRut(aclaracion.getAfectado().getRut()));
		String fechaProtesto = aclaracion.getFechaprotesto().replace("T", " ").replace("Z", "");
		aclaracion.setFechaprotesto(utils.formatearFecha(fechaProtesto).replace("/", ""));
		
		ResponseDTO respuesta = aclaracionDao.validaAclaracionCarro(aclaracion);
		return respuesta;
		
	}

}
