package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABITeAvisaDetalle database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABITeAvisaDetalle.findAll", query="SELECT t FROM Tbl_ABITeAvisaDetalle t")
public class Tbl_ABITeAvisaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrMail")
	private BigDecimal fld_CorrMail;

	@Column(name="Fld_EmiLib")
	private String fld_EmiLib;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_Folio")
	private int fld_Folio;

	@Column(name="Fld_Moneda")
	private String fld_Moneda;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_TipoDetalle")
	private String fld_TipoDetalle;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_ABITeAvisaDetalle() {
	}

	public BigDecimal getFld_CorrMail() {
		return this.fld_CorrMail;
	}

	public void setFld_CorrMail(BigDecimal fld_CorrMail) {
		this.fld_CorrMail = fld_CorrMail;
	}

	public String getFld_EmiLib() {
		return this.fld_EmiLib;
	}

	public void setFld_EmiLib(String fld_EmiLib) {
		this.fld_EmiLib = fld_EmiLib;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public int getFld_Folio() {
		return this.fld_Folio;
	}

	public void setFld_Folio(int fld_Folio) {
		this.fld_Folio = fld_Folio;
	}

	public String getFld_Moneda() {
		return this.fld_Moneda;
	}

	public void setFld_Moneda(String fld_Moneda) {
		this.fld_Moneda = fld_Moneda;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_TipoDetalle() {
		return this.fld_TipoDetalle;
	}

	public void setFld_TipoDetalle(String fld_TipoDetalle) {
		this.fld_TipoDetalle = fld_TipoDetalle;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}