package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_Harboe database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_Harboe.findAll", query="SELECT l FROM Ley_Harboe l")
public class Ley_Harboe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CantBenefBic")
	private short fld_CantBenefBic;

	@Column(name="Fld_CantBenefDeuda")
	private short fld_CantBenefDeuda;

	@Column(name="Fld_CantBenefMol")
	private short fld_CantBenefMol;

	@Column(name="Fld_CantDeuda")
	private short fld_CantDeuda;

	@Column(name="Fld_CantTotalBic")
	private short fld_CantTotalBic;

	@Column(name="Fld_CantTotalMol")
	private short fld_CantTotalMol;

	@Column(name="Fld_FlagBeneficiado")
	private byte fld_FlagBeneficiado;

	@Column(name="Fld_MontoBenefBic")
	private BigDecimal fld_MontoBenefBic;

	@Column(name="Fld_MontoBenefMol")
	private BigDecimal fld_MontoBenefMol;

	@Column(name="Fld_MontoTotalBic")
	private BigDecimal fld_MontoTotalBic;

	@Column(name="Fld_MontoTotalMol")
	private BigDecimal fld_MontoTotalMol;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TotalBenefDeuda")
	private BigDecimal fld_TotalBenefDeuda;

	@Column(name="Fld_TotalDeuda")
	private BigDecimal fld_TotalDeuda;

	public Ley_Harboe() {
	}

	public short getFld_CantBenefBic() {
		return this.fld_CantBenefBic;
	}

	public void setFld_CantBenefBic(short fld_CantBenefBic) {
		this.fld_CantBenefBic = fld_CantBenefBic;
	}

	public short getFld_CantBenefDeuda() {
		return this.fld_CantBenefDeuda;
	}

	public void setFld_CantBenefDeuda(short fld_CantBenefDeuda) {
		this.fld_CantBenefDeuda = fld_CantBenefDeuda;
	}

	public short getFld_CantBenefMol() {
		return this.fld_CantBenefMol;
	}

	public void setFld_CantBenefMol(short fld_CantBenefMol) {
		this.fld_CantBenefMol = fld_CantBenefMol;
	}

	public short getFld_CantDeuda() {
		return this.fld_CantDeuda;
	}

	public void setFld_CantDeuda(short fld_CantDeuda) {
		this.fld_CantDeuda = fld_CantDeuda;
	}

	public short getFld_CantTotalBic() {
		return this.fld_CantTotalBic;
	}

	public void setFld_CantTotalBic(short fld_CantTotalBic) {
		this.fld_CantTotalBic = fld_CantTotalBic;
	}

	public short getFld_CantTotalMol() {
		return this.fld_CantTotalMol;
	}

	public void setFld_CantTotalMol(short fld_CantTotalMol) {
		this.fld_CantTotalMol = fld_CantTotalMol;
	}

	public byte getFld_FlagBeneficiado() {
		return this.fld_FlagBeneficiado;
	}

	public void setFld_FlagBeneficiado(byte fld_FlagBeneficiado) {
		this.fld_FlagBeneficiado = fld_FlagBeneficiado;
	}

	public BigDecimal getFld_MontoBenefBic() {
		return this.fld_MontoBenefBic;
	}

	public void setFld_MontoBenefBic(BigDecimal fld_MontoBenefBic) {
		this.fld_MontoBenefBic = fld_MontoBenefBic;
	}

	public BigDecimal getFld_MontoBenefMol() {
		return this.fld_MontoBenefMol;
	}

	public void setFld_MontoBenefMol(BigDecimal fld_MontoBenefMol) {
		this.fld_MontoBenefMol = fld_MontoBenefMol;
	}

	public BigDecimal getFld_MontoTotalBic() {
		return this.fld_MontoTotalBic;
	}

	public void setFld_MontoTotalBic(BigDecimal fld_MontoTotalBic) {
		this.fld_MontoTotalBic = fld_MontoTotalBic;
	}

	public BigDecimal getFld_MontoTotalMol() {
		return this.fld_MontoTotalMol;
	}

	public void setFld_MontoTotalMol(BigDecimal fld_MontoTotalMol) {
		this.fld_MontoTotalMol = fld_MontoTotalMol;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public BigDecimal getFld_TotalBenefDeuda() {
		return this.fld_TotalBenefDeuda;
	}

	public void setFld_TotalBenefDeuda(BigDecimal fld_TotalBenefDeuda) {
		this.fld_TotalBenefDeuda = fld_TotalBenefDeuda;
	}

	public BigDecimal getFld_TotalDeuda() {
		return this.fld_TotalDeuda;
	}

	public void setFld_TotalDeuda(BigDecimal fld_TotalDeuda) {
		this.fld_TotalDeuda = fld_TotalDeuda;
	}

}