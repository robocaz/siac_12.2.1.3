package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetConsultaVerifCartera database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetConsultaVerifCartera.findAll", query="SELECT t FROM Tbl_DetConsultaVerifCartera t")
public class Tbl_DetConsultaVerifCartera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_FecVerifCartera")
	private Timestamp fld_FecVerifCartera;

	@Column(name="Fld_FlagProcesado")
	private String fld_FlagProcesado;

	@Column(name="Fld_NroRegRespuesta")
	private int fld_NroRegRespuesta;

	@Column(name="Fld_NroRutConsulta")
	private int fld_NroRutConsulta;

	@Column(name="Fld_RutCliente")
	private String fld_RutCliente;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorBase")
	private BigDecimal fld_ValorBase;

	public Tbl_DetConsultaVerifCartera() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public Timestamp getFld_FecVerifCartera() {
		return this.fld_FecVerifCartera;
	}

	public void setFld_FecVerifCartera(Timestamp fld_FecVerifCartera) {
		this.fld_FecVerifCartera = fld_FecVerifCartera;
	}

	public String getFld_FlagProcesado() {
		return this.fld_FlagProcesado;
	}

	public void setFld_FlagProcesado(String fld_FlagProcesado) {
		this.fld_FlagProcesado = fld_FlagProcesado;
	}

	public int getFld_NroRegRespuesta() {
		return this.fld_NroRegRespuesta;
	}

	public void setFld_NroRegRespuesta(int fld_NroRegRespuesta) {
		this.fld_NroRegRespuesta = fld_NroRegRespuesta;
	}

	public int getFld_NroRutConsulta() {
		return this.fld_NroRutConsulta;
	}

	public void setFld_NroRutConsulta(int fld_NroRutConsulta) {
		this.fld_NroRutConsulta = fld_NroRutConsulta;
	}

	public String getFld_RutCliente() {
		return this.fld_RutCliente;
	}

	public void setFld_RutCliente(String fld_RutCliente) {
		this.fld_RutCliente = fld_RutCliente;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorBase() {
		return this.fld_ValorBase;
	}

	public void setFld_ValorBase(BigDecimal fld_ValorBase) {
		this.fld_ValorBase = fld_ValorBase;
	}

}