package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Resource database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Resource.findAll", query="SELECT t FROM Tbl_Resource t")
public class Tbl_Resource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	@Column(name="Fld_ResourceDescription")
	private String fld_ResourceDescription;

	@Column(name="Fld_ResourceName")
	private String fld_ResourceName;

	@Column(name="Fld_ResourceTimeStamp")
	private byte[] fld_ResourceTimeStamp;

	@Column(name="Fld_ResourceType")
	private byte fld_ResourceType;

	public Tbl_Resource() {
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

	public String getFld_ResourceDescription() {
		return this.fld_ResourceDescription;
	}

	public void setFld_ResourceDescription(String fld_ResourceDescription) {
		this.fld_ResourceDescription = fld_ResourceDescription;
	}

	public String getFld_ResourceName() {
		return this.fld_ResourceName;
	}

	public void setFld_ResourceName(String fld_ResourceName) {
		this.fld_ResourceName = fld_ResourceName;
	}

	public byte[] getFld_ResourceTimeStamp() {
		return this.fld_ResourceTimeStamp;
	}

	public void setFld_ResourceTimeStamp(byte[] fld_ResourceTimeStamp) {
		this.fld_ResourceTimeStamp = fld_ResourceTimeStamp;
	}

	public byte getFld_ResourceType() {
		return this.fld_ResourceType;
	}

	public void setFld_ResourceType(byte fld_ResourceType) {
		this.fld_ResourceType = fld_ResourceType;
	}

}