package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_ACLARACION database table.
 * 
 */
@Entity
@Table(name="TBL_ACLARACION")
@NamedQuery(name="TblAclaracion.findAll", query="SELECT t FROM TblAclaracion t")
public class TblAclaracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ACLARACION_FLDCORRACLARACIONID_GENERATOR", sequenceName="FLD_CORRACLARACION_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ACLARACION_FLDCORRACLARACIONID_GENERATOR")
	@Column(name="FLD_CORRACLARACION_ID")
	private long fldCorraclaracionId;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_CORRMOROSIDAD")
	private BigDecimal fldCorrmorosidad;

	@Column(name="FLD_FECHAACLARACION")
	private Timestamp fldFechaaclaracion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAPAGO")
	private Date fldFechapago;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_USUARIOACLARACION")
	private String fldUsuarioaclaracion;

	public TblAclaracion() {
	}

	public long getFldCorraclaracionId() {
		return this.fldCorraclaracionId;
	}

	public void setFldCorraclaracionId(long fldCorraclaracionId) {
		this.fldCorraclaracionId = fldCorraclaracionId;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public BigDecimal getFldCorrmorosidad() {
		return this.fldCorrmorosidad;
	}

	public void setFldCorrmorosidad(BigDecimal fldCorrmorosidad) {
		this.fldCorrmorosidad = fldCorrmorosidad;
	}

	public Timestamp getFldFechaaclaracion() {
		return this.fldFechaaclaracion;
	}

	public void setFldFechaaclaracion(Timestamp fldFechaaclaracion) {
		this.fldFechaaclaracion = fldFechaaclaracion;
	}

	public Date getFldFechapago() {
		return this.fldFechapago;
	}

	public void setFldFechapago(Date fldFechapago) {
		this.fldFechapago = fldFechapago;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldUsuarioaclaracion() {
		return this.fldUsuarioaclaracion;
	}

	public void setFldUsuarioaclaracion(String fldUsuarioaclaracion) {
		this.fldUsuarioaclaracion = fldUsuarioaclaracion;
	}

}