package cl.ccs.siac.dao;

import cl.exe.ccs.dto.BoletinInDTO;
import cl.exe.ccs.dto.BoletinDataDetalle;
import cl.exe.ccs.dto.BoletinDataEmpleador;
import cl.exe.ccs.dto.BoletinDataTrabajador;
import cl.exe.ccs.dto.BoletinOutDTO;

public interface BoletinDao {

	BoletinOutDTO<BoletinDataTrabajador> consultaTrabajador(BoletinInDTO params);

	BoletinOutDTO<BoletinDataDetalle> consultaInfractor(BoletinInDTO params);

	BoletinOutDTO<BoletinDataEmpleador> consultaResumen(BoletinInDTO params);

}
