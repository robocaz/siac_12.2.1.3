angular.module('ccs')

.factory('LoginResource', function ($resource) {
    var resource = $resource('rest/login/',  { nocache : new Date().getTime()},
    {
        'login' :  { method : 'POST' ,url: 'rest/login/ingreso' },
        'status':  { method : 'GET'   },
        'logout':  { method : 'POST', url: 'rest/login/logout'},
        'esSupervisor' : {method: 'POST', url: 'rest/login/esSupervisor'},
        'validaSesion' : {method: 'GET', url: 'rest/login/check'},
        'cambiarClave' : {method: 'POST', url: 'rest/login/cambiarClave'}
    });
    return resource;
});
