package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetListaPublico database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetListaPublico.findAll", query="SELECT t FROM Tbl_DetListaPublico t")
public class Tbl_DetListaPublico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEspecial")
	private int fld_CodEspecial;

	@Column(name="Fld_CodEspecialAux")
	private int fld_CodEspecialAux;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrLP_Id")
	private BigDecimal fld_CorrLP_Id;

	@Column(name="Fld_CorrLPAux")
	private BigDecimal fld_CorrLPAux;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FlagTitulos")
	private short fld_FlagTitulos;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_GlosaLocalidad")
	private String fld_GlosaLocalidad;

	@Column(name="Fld_GlosaMoneda")
	private String fld_GlosaMoneda;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	@Column(name="Fld_GlosaTipoEmisor")
	private String fld_GlosaTipoEmisor;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_PaginaRegistro")
	private short fld_PaginaRegistro;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	@Column(name="Fld_SecuenciaLP")
	private int fld_SecuenciaLP;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_DetListaPublico() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodEspecial() {
		return this.fld_CodEspecial;
	}

	public void setFld_CodEspecial(int fld_CodEspecial) {
		this.fld_CodEspecial = fld_CodEspecial;
	}

	public int getFld_CodEspecialAux() {
		return this.fld_CodEspecialAux;
	}

	public void setFld_CodEspecialAux(int fld_CodEspecialAux) {
		this.fld_CodEspecialAux = fld_CodEspecialAux;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrLP_Id() {
		return this.fld_CorrLP_Id;
	}

	public void setFld_CorrLP_Id(BigDecimal fld_CorrLP_Id) {
		this.fld_CorrLP_Id = fld_CorrLP_Id;
	}

	public BigDecimal getFld_CorrLPAux() {
		return this.fld_CorrLPAux;
	}

	public void setFld_CorrLPAux(BigDecimal fld_CorrLPAux) {
		this.fld_CorrLPAux = fld_CorrLPAux;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public short getFld_FlagTitulos() {
		return this.fld_FlagTitulos;
	}

	public void setFld_FlagTitulos(short fld_FlagTitulos) {
		this.fld_FlagTitulos = fld_FlagTitulos;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public String getFld_GlosaLocalidad() {
		return this.fld_GlosaLocalidad;
	}

	public void setFld_GlosaLocalidad(String fld_GlosaLocalidad) {
		this.fld_GlosaLocalidad = fld_GlosaLocalidad;
	}

	public String getFld_GlosaMoneda() {
		return this.fld_GlosaMoneda;
	}

	public void setFld_GlosaMoneda(String fld_GlosaMoneda) {
		this.fld_GlosaMoneda = fld_GlosaMoneda;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

	public String getFld_GlosaTipoEmisor() {
		return this.fld_GlosaTipoEmisor;
	}

	public void setFld_GlosaTipoEmisor(String fld_GlosaTipoEmisor) {
		this.fld_GlosaTipoEmisor = fld_GlosaTipoEmisor;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public short getFld_PaginaRegistro() {
		return this.fld_PaginaRegistro;
	}

	public void setFld_PaginaRegistro(short fld_PaginaRegistro) {
		this.fld_PaginaRegistro = fld_PaginaRegistro;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

	public int getFld_SecuenciaLP() {
		return this.fld_SecuenciaLP;
	}

	public void setFld_SecuenciaLP(int fld_SecuenciaLP) {
		this.fld_SecuenciaLP = fld_SecuenciaLP;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}