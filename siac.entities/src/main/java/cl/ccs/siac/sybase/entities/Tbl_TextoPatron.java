package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TextoPatron database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TextoPatron.findAll", query="SELECT t FROM Tbl_TextoPatron t")
public class Tbl_TextoPatron implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CategoriaTexto")
	private byte fld_CategoriaTexto;

	@Column(name="Fld_CodigoTexto")
	private byte fld_CodigoTexto;

	@Column(name="Fld_CorrOrdenStr_Id")
	private BigDecimal fld_CorrOrdenStr_Id;

	@Column(name="Fld_LargoStr")
	private short fld_LargoStr;

	@Column(name="Fld_OrigenSolicitud")
	private byte fld_OrigenSolicitud;

	@Column(name="Fld_TextoLinea1")
	private String fld_TextoLinea1;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_TextoPatron() {
	}

	public byte getFld_CategoriaTexto() {
		return this.fld_CategoriaTexto;
	}

	public void setFld_CategoriaTexto(byte fld_CategoriaTexto) {
		this.fld_CategoriaTexto = fld_CategoriaTexto;
	}

	public byte getFld_CodigoTexto() {
		return this.fld_CodigoTexto;
	}

	public void setFld_CodigoTexto(byte fld_CodigoTexto) {
		this.fld_CodigoTexto = fld_CodigoTexto;
	}

	public BigDecimal getFld_CorrOrdenStr_Id() {
		return this.fld_CorrOrdenStr_Id;
	}

	public void setFld_CorrOrdenStr_Id(BigDecimal fld_CorrOrdenStr_Id) {
		this.fld_CorrOrdenStr_Id = fld_CorrOrdenStr_Id;
	}

	public short getFld_LargoStr() {
		return this.fld_LargoStr;
	}

	public void setFld_LargoStr(short fld_LargoStr) {
		this.fld_LargoStr = fld_LargoStr;
	}

	public byte getFld_OrigenSolicitud() {
		return this.fld_OrigenSolicitud;
	}

	public void setFld_OrigenSolicitud(byte fld_OrigenSolicitud) {
		this.fld_OrigenSolicitud = fld_OrigenSolicitud;
	}

	public String getFld_TextoLinea1() {
		return this.fld_TextoLinea1;
	}

	public void setFld_TextoLinea1(String fld_TextoLinea1) {
		this.fld_TextoLinea1 = fld_TextoLinea1;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}