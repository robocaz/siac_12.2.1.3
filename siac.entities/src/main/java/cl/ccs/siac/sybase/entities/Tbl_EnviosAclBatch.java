package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EnviosAclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosAclBatch.findAll", query="SELECT t FROM Tbl_EnviosAclBatch t")
public class Tbl_EnviosAclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_AvisosRecuperados")
	private int fld_AvisosRecuperados;

	@Column(name="Fld_BoletaFactura")
	private String fld_BoletaFactura;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrEnvio_Id")
	private BigDecimal fld_CorrEnvio_Id;

	@Column(name="Fld_FecElaboracionSolic")
	private Timestamp fld_FecElaboracionSolic;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecInicioCalculada")
	private Timestamp fld_FecInicioCalculada;

	@Column(name="Fld_FecInicioInformado")
	private Timestamp fld_FecInicioInformado;

	@Column(name="Fld_FecReProceso")
	private Timestamp fld_FecReProceso;

	@Column(name="Fld_FecTerminoCalculada")
	private Timestamp fld_FecTerminoCalculada;

	@Column(name="Fld_FecTerminoInformado")
	private Timestamp fld_FecTerminoInformado;

	@Column(name="Fld_Firma_ApMat")
	private String fld_Firma_ApMat;

	@Column(name="Fld_Firma_ApPat")
	private String fld_Firma_ApPat;

	@Column(name="Fld_Firma_Nombres")
	private String fld_Firma_Nombres;

	@Column(name="Fld_Flag_EnvioCentralizado")
	private boolean fld_Flag_EnvioCentralizado;

	@Column(name="Fld_Flag_Rechazo")
	private boolean fld_Flag_Rechazo;

	@Column(name="Fld_FlagCostoCero")
	private boolean fld_FlagCostoCero;

	@Column(name="Fld_FlagFormato")
	private boolean fld_FlagFormato;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_MarcaFacturado")
	private boolean fld_MarcaFacturado;

	@Column(name="Fld_NroBoletaFactura")
	private int fld_NroBoletaFactura;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroIntentos")
	private byte fld_NroIntentos;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	@Column(name="Fld_TotAvisosNoPar")
	private int fld_TotAvisosNoPar;

	@Column(name="Fld_TotAvisosNoParDup")
	private int fld_TotAvisosNoParDup;

	@Column(name="Fld_TotMediosMagneticos")
	private byte fld_TotMediosMagneticos;

	@Column(name="Fld_TotRechazosGuardados")
	private int fld_TotRechazosGuardados;

	@Column(name="Fld_TotRechazosGuardadosDup")
	private int fld_TotRechazosGuardadosDup;

	@Column(name="Fld_TotRegistrosAclarados")
	private int fld_TotRegistrosAclarados;

	@Column(name="Fld_TotRegistrosDevueltos")
	private int fld_TotRegistrosDevueltos;

	@Column(name="Fld_TotRegistrosGuardados")
	private int fld_TotRegistrosGuardados;

	@Column(name="Fld_TotRegistrosIgnorados")
	private int fld_TotRegistrosIgnorados;

	@Column(name="Fld_TotRegistrosInformados")
	private int fld_TotRegistrosInformados;

	@Column(name="Fld_TotRegistrosOmitidos")
	private int fld_TotRegistrosOmitidos;

	public Tbl_EnviosAclBatch() {
	}

	public int getFld_AvisosRecuperados() {
		return this.fld_AvisosRecuperados;
	}

	public void setFld_AvisosRecuperados(int fld_AvisosRecuperados) {
		this.fld_AvisosRecuperados = fld_AvisosRecuperados;
	}

	public String getFld_BoletaFactura() {
		return this.fld_BoletaFactura;
	}

	public void setFld_BoletaFactura(String fld_BoletaFactura) {
		this.fld_BoletaFactura = fld_BoletaFactura;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrEnvio_Id() {
		return this.fld_CorrEnvio_Id;
	}

	public void setFld_CorrEnvio_Id(BigDecimal fld_CorrEnvio_Id) {
		this.fld_CorrEnvio_Id = fld_CorrEnvio_Id;
	}

	public Timestamp getFld_FecElaboracionSolic() {
		return this.fld_FecElaboracionSolic;
	}

	public void setFld_FecElaboracionSolic(Timestamp fld_FecElaboracionSolic) {
		this.fld_FecElaboracionSolic = fld_FecElaboracionSolic;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecInicioCalculada() {
		return this.fld_FecInicioCalculada;
	}

	public void setFld_FecInicioCalculada(Timestamp fld_FecInicioCalculada) {
		this.fld_FecInicioCalculada = fld_FecInicioCalculada;
	}

	public Timestamp getFld_FecInicioInformado() {
		return this.fld_FecInicioInformado;
	}

	public void setFld_FecInicioInformado(Timestamp fld_FecInicioInformado) {
		this.fld_FecInicioInformado = fld_FecInicioInformado;
	}

	public Timestamp getFld_FecReProceso() {
		return this.fld_FecReProceso;
	}

	public void setFld_FecReProceso(Timestamp fld_FecReProceso) {
		this.fld_FecReProceso = fld_FecReProceso;
	}

	public Timestamp getFld_FecTerminoCalculada() {
		return this.fld_FecTerminoCalculada;
	}

	public void setFld_FecTerminoCalculada(Timestamp fld_FecTerminoCalculada) {
		this.fld_FecTerminoCalculada = fld_FecTerminoCalculada;
	}

	public Timestamp getFld_FecTerminoInformado() {
		return this.fld_FecTerminoInformado;
	}

	public void setFld_FecTerminoInformado(Timestamp fld_FecTerminoInformado) {
		this.fld_FecTerminoInformado = fld_FecTerminoInformado;
	}

	public String getFld_Firma_ApMat() {
		return this.fld_Firma_ApMat;
	}

	public void setFld_Firma_ApMat(String fld_Firma_ApMat) {
		this.fld_Firma_ApMat = fld_Firma_ApMat;
	}

	public String getFld_Firma_ApPat() {
		return this.fld_Firma_ApPat;
	}

	public void setFld_Firma_ApPat(String fld_Firma_ApPat) {
		this.fld_Firma_ApPat = fld_Firma_ApPat;
	}

	public String getFld_Firma_Nombres() {
		return this.fld_Firma_Nombres;
	}

	public void setFld_Firma_Nombres(String fld_Firma_Nombres) {
		this.fld_Firma_Nombres = fld_Firma_Nombres;
	}

	public boolean getFld_Flag_EnvioCentralizado() {
		return this.fld_Flag_EnvioCentralizado;
	}

	public void setFld_Flag_EnvioCentralizado(boolean fld_Flag_EnvioCentralizado) {
		this.fld_Flag_EnvioCentralizado = fld_Flag_EnvioCentralizado;
	}

	public boolean getFld_Flag_Rechazo() {
		return this.fld_Flag_Rechazo;
	}

	public void setFld_Flag_Rechazo(boolean fld_Flag_Rechazo) {
		this.fld_Flag_Rechazo = fld_Flag_Rechazo;
	}

	public boolean getFld_FlagCostoCero() {
		return this.fld_FlagCostoCero;
	}

	public void setFld_FlagCostoCero(boolean fld_FlagCostoCero) {
		this.fld_FlagCostoCero = fld_FlagCostoCero;
	}

	public boolean getFld_FlagFormato() {
		return this.fld_FlagFormato;
	}

	public void setFld_FlagFormato(boolean fld_FlagFormato) {
		this.fld_FlagFormato = fld_FlagFormato;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public boolean getFld_MarcaFacturado() {
		return this.fld_MarcaFacturado;
	}

	public void setFld_MarcaFacturado(boolean fld_MarcaFacturado) {
		this.fld_MarcaFacturado = fld_MarcaFacturado;
	}

	public int getFld_NroBoletaFactura() {
		return this.fld_NroBoletaFactura;
	}

	public void setFld_NroBoletaFactura(int fld_NroBoletaFactura) {
		this.fld_NroBoletaFactura = fld_NroBoletaFactura;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte getFld_NroIntentos() {
		return this.fld_NroIntentos;
	}

	public void setFld_NroIntentos(byte fld_NroIntentos) {
		this.fld_NroIntentos = fld_NroIntentos;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

	public int getFld_TotAvisosNoPar() {
		return this.fld_TotAvisosNoPar;
	}

	public void setFld_TotAvisosNoPar(int fld_TotAvisosNoPar) {
		this.fld_TotAvisosNoPar = fld_TotAvisosNoPar;
	}

	public int getFld_TotAvisosNoParDup() {
		return this.fld_TotAvisosNoParDup;
	}

	public void setFld_TotAvisosNoParDup(int fld_TotAvisosNoParDup) {
		this.fld_TotAvisosNoParDup = fld_TotAvisosNoParDup;
	}

	public byte getFld_TotMediosMagneticos() {
		return this.fld_TotMediosMagneticos;
	}

	public void setFld_TotMediosMagneticos(byte fld_TotMediosMagneticos) {
		this.fld_TotMediosMagneticos = fld_TotMediosMagneticos;
	}

	public int getFld_TotRechazosGuardados() {
		return this.fld_TotRechazosGuardados;
	}

	public void setFld_TotRechazosGuardados(int fld_TotRechazosGuardados) {
		this.fld_TotRechazosGuardados = fld_TotRechazosGuardados;
	}

	public int getFld_TotRechazosGuardadosDup() {
		return this.fld_TotRechazosGuardadosDup;
	}

	public void setFld_TotRechazosGuardadosDup(int fld_TotRechazosGuardadosDup) {
		this.fld_TotRechazosGuardadosDup = fld_TotRechazosGuardadosDup;
	}

	public int getFld_TotRegistrosAclarados() {
		return this.fld_TotRegistrosAclarados;
	}

	public void setFld_TotRegistrosAclarados(int fld_TotRegistrosAclarados) {
		this.fld_TotRegistrosAclarados = fld_TotRegistrosAclarados;
	}

	public int getFld_TotRegistrosDevueltos() {
		return this.fld_TotRegistrosDevueltos;
	}

	public void setFld_TotRegistrosDevueltos(int fld_TotRegistrosDevueltos) {
		this.fld_TotRegistrosDevueltos = fld_TotRegistrosDevueltos;
	}

	public int getFld_TotRegistrosGuardados() {
		return this.fld_TotRegistrosGuardados;
	}

	public void setFld_TotRegistrosGuardados(int fld_TotRegistrosGuardados) {
		this.fld_TotRegistrosGuardados = fld_TotRegistrosGuardados;
	}

	public int getFld_TotRegistrosIgnorados() {
		return this.fld_TotRegistrosIgnorados;
	}

	public void setFld_TotRegistrosIgnorados(int fld_TotRegistrosIgnorados) {
		this.fld_TotRegistrosIgnorados = fld_TotRegistrosIgnorados;
	}

	public int getFld_TotRegistrosInformados() {
		return this.fld_TotRegistrosInformados;
	}

	public void setFld_TotRegistrosInformados(int fld_TotRegistrosInformados) {
		this.fld_TotRegistrosInformados = fld_TotRegistrosInformados;
	}

	public int getFld_TotRegistrosOmitidos() {
		return this.fld_TotRegistrosOmitidos;
	}

	public void setFld_TotRegistrosOmitidos(int fld_TotRegistrosOmitidos) {
		this.fld_TotRegistrosOmitidos = fld_TotRegistrosOmitidos;
	}

}