package cl.ccs.siac.servicios;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.cuadratura.CuadraturaBusiness;
import cl.ccs.siac.factura.FacturaBusiness;
import cl.ccs.siac.usuario.UsuarioBusiness;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.CajaInformeDTO;
import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaAclaracionDTO;
import cl.exe.ccs.dto.CuadraturaCajaDTO;
import cl.exe.ccs.dto.CuadraturaCentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaUsuarioDTO;
import cl.exe.ccs.dto.DetalleBoletaDTO;
import cl.exe.ccs.dto.DetalleBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleCentroCostoDTO;
import cl.exe.ccs.dto.DetalleServicioDTO;
import cl.exe.ccs.dto.PDFResponseDTO;
import cl.exe.ccs.dto.ReclamoDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResultadoPorCCDTO;
import cl.exe.ccs.dto.ResumenMovimientoDTO;
import cl.exe.ccs.dto.UsuarioDTO;
import cl.exe.ccs.dto.UsuariosInformeDTO;

@Path("/cuadratura")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class CuadraturaServicio {

	private static final Logger log = LogManager.getLogger(CuadraturaServicio.class);

	@Inject
	private CuadraturaBusiness cuadraturaBusiness;

	@Inject
	private GeneradorPDF generador;
	
	@Inject
	private Utils util;
	
	@Inject
	private FacturaBusiness facturaBusiness;

	@Inject
	private UsuarioBusiness usuarioBusiness;

	public static String RUTA = "wlscomun/reportes/";

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/centroscostos")
	public Response buscarCentroCosto(@Context HttpServletRequest request, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			Integer centroCosto = 0;

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			if (usuarioDTO.getEsAdministrador()) {
				centroCosto = 0;
			} else if (usuarioDTO.getEsSupervisor()) {
				centroCosto = Integer.parseInt(usuarioDTO.getCentroCostoActivo().getCodigo());
			}

			List<CentroCostoDTO> centros = cuadraturaBusiness.centrosCostoInforme(centroCosto, fechaDesde, fechaHasta);
			if (centros != null) {
				return Response.ok(centros).build();
			} else {
				log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal()
						+ " - Message: Error del Sistema");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/usuarios")
	public Response buscarUsuariosCC(@Context HttpServletRequest request,
			@QueryParam("centroCosto") Integer centroCosto, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			List<UsuariosInformeDTO> usuarios = cuadraturaBusiness.usuariosInforme(centroCosto, fechaDesde, fechaHasta);
			if (usuarios.size() > 0) {
				return Response.ok(usuarios).build();
			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cajas")
	public Response buscarCajasCC(@Context HttpServletRequest request, @QueryParam("usuario") String usuario,
			@QueryParam("centroCosto") Integer centroCosto, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			List<CajaInformeDTO> cajas = cuadraturaBusiness.cajaInforme(usuario, centroCosto, fechaDesde, fechaHasta);
			if (cajas.size() > 0) {
				return Response.ok(cajas).build();
			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cuadraturacentrocosto")
	public Response cuadraturaPorCC(@Context HttpServletRequest request, @QueryParam("centroCosto") Integer centroCosto,
			@QueryParam("fechaDesde") String fechaDesde, @QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			CuadraturaCentroCostoDTO cuadraturaCC = cuadraturaBusiness.cuadraturaPorCamara(centroCosto, fechaDesde,
					fechaHasta);

			if (cuadraturaCC != null) {
				return Response.ok(cuadraturaCC).build();
			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cuadraturausuario")
	public Response cuadraturaPorUsuario(@Context HttpServletRequest request, @QueryParam("usuario") String usuario,
			@QueryParam("centroCosto") Integer centroCosto, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			CuadraturaUsuarioDTO cuadraturaUsuarioDTO = cuadraturaBusiness.cuadraturaPorUsuario(usuario, centroCosto,
					fechaDesde, fechaHasta);
			if (cuadraturaUsuarioDTO != null) {
				return Response.ok(cuadraturaUsuarioDTO).build();
			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/detcentrocosto")
	public Response cuadraturaPorCentroCostoDetalle(@Context HttpServletRequest request,
			@QueryParam("centroCosto") Integer centroCosto, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			DetalleCentroCostoDTO detalleCentroCosto = cuadraturaBusiness.cuadraturaPorCentroCosto(centroCosto,fechaDesde, fechaHasta);
			if (detalleCentroCosto != null) {
				return Response.ok(detalleCentroCosto).build();
			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Path("/imprimereportecc")
	@Consumes("application/json")
	@Produces("application/json")
	public Response informeCentroCostoDetalle(@Context HttpServletRequest request,
			@QueryParam("centroCosto") Integer centroCosto, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			CuadraturaCentroCostoDTO cuadraturaCC = cuadraturaBusiness.cuadraturaPorCamara(centroCosto, fechaDesde,fechaHasta);
			Map<String, Object> parametros = new HashMap<String, Object>();

			parametros.put("FECHA_DESDE", cuadraturaCC.getFechaDesde());
			parametros.put("FECHA_HASTA", cuadraturaCC.getFechaHasta());
			parametros.put("TOTAL_TRANSACCION", cuadraturaCC.getMontoResuTransacciones());
			parametros.put("TOTAL_INGRESADO", cuadraturaCC.getMontoResuIngCajera());
			parametros.put("TOTAL_DIFERENCIA", cuadraturaCC.getMontoResuDiferencia());
			List<ResultadoPorCCDTO>  resultados = cuadraturaCC.getResultadoCentros();
			resultados.remove(0);
			byte[] archivo = generador.create("reporteCCS", resultados, parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/imprimereportecaja")
	public Response informeCajaDetalle(@Context HttpServletRequest request, DetalleCentroCostoDTO detalleCaja) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			

			Map<String, Object> parametros = new HashMap<String, Object>();

			parametros.put("FECHA_DESDE", detalleCaja.getFechaDesde());
			parametros.put("FECHA_HASTA", detalleCaja.getFechaHasta());
			parametros.put("CENTRO_COSTO", detalleCaja.getCentroCosto().toString() + " " + detalleCaja.getGlosaCC());
			parametros.put("MONTO_CALCULADO", detalleCaja.getMontoCalculadoBF());
			parametros.put("MONTO_INGRESADO", detalleCaja.getMontoCalculadoDet());
			parametros.put("MONTO_PAGADO", detalleCaja.getMontoPagado().doubleValue());
			parametros.put("DIFERENCIA_BF", detalleCaja.getDiferenciaBF());
			parametros.put("DIFERENCIA_PESO", detalleCaja.getDiferenciaDet());

			byte[] archivo = generador.create("reportePorCentoCosto", detalleCaja.getCajasDetalle(), parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/imprimereporteusuario")
	public Response informeUsuarioDetalle(@Context HttpServletRequest request,
			CuadraturaUsuarioDTO cuadraturaUsuarioDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = usuarioBusiness.obtenerUsuario(cuadraturaUsuarioDTO.getUsuario());
			
			Map<String, Object> parametros = new HashMap<String, Object>();

			
			parametros.put("FECHA_DESDE", cuadraturaUsuarioDTO.getFechaDesde());
			parametros.put("FECHA_HASTA", cuadraturaUsuarioDTO.getFechaHasta());
			parametros.put("USUARIO", usuarioDTO.getNombre());
			parametros.put("CENTRO_COSTO", cuadraturaUsuarioDTO.getCentroCosto().toString() + " " + cuadraturaUsuarioDTO.getGolsaCC());
			parametros.put("MONTO_CALCULADO", cuadraturaUsuarioDTO.getMontoResuTransacciones());
			parametros.put("MONTO_INGRESADO", cuadraturaUsuarioDTO.getMontoResuIngCajera());
			parametros.put("DIFERENCIA_BF", cuadraturaUsuarioDTO.getMontoResuDiferencia());
			// parametros.put("DIFERENCIA_PESO", cuadraturaUsuarioDTO.get);

			byte[] archivo = generador.create("reporteUsuarioCaja", cuadraturaUsuarioDTO.getResultadoUsuarios(),parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",
					ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cuadraturageneral")
	public Response cuadraturaGeneralCCS(@Context HttpServletRequest request,@QueryParam("centroscosto") String centroscosto, 
			@QueryParam("fechaDesde") String fechaDesde, @QueryParam("fechaHasta") String fechaHasta) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResumenMovimientoDTO resumen = cuadraturaBusiness.obtieneMovimientosNac(Integer.parseInt(centroscosto), fechaDesde, fechaHasta);
			if(!(resumen==null)) return Response.ok(resumen).build();
			else return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		} catch (Exception ex) {
			log.error("Busca Cuadratura centro costo, Usuario " + request.getUserPrincipal()
					+ " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cuadraturaporcentro")
	public Response cuadraturaPorCentroCosto(@Context HttpServletRequest request,
			@QueryParam("fechaDesde") String fechaDesde, @QueryParam("centroscosto") Integer centroCosto,
			@QueryParam("usuario") String usuario, @QueryParam("corrcaja") Integer corrcaja,
			@QueryParam("criterio") Integer criterio) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			
			
			
			ResumenMovimientoDTO resumen = cuadraturaBusiness.obtieneMovimientos(fechaDesde, usuario, centroCosto,corrcaja, criterio);
			if(!(resumen==null)) return Response.ok(resumen).build();
			else return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		} catch (Exception ex) {
			log.error("Busca Cuadratura centro costo, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cargadatoscaja")
	public Response cargaDatosPorCaja(@Context HttpServletRequest request, @QueryParam("fechaDesde") String fechaDesde,
			@QueryParam("centrocosto") String centroCosto, @QueryParam("usuario") String usuario) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<CuadraturaCajaDTO> cajas = cuadraturaBusiness.buscarCajasPorUsuario(usuario,Integer.parseInt(centroCosto), fechaDesde);
			return Response.ok(cajas).build();
		} catch (Exception ex) {
			log.error("Busca Cuadratura centro costo, Usuario " + request.getUserPrincipal()
					+ " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
	
	
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cuadraturaporcaja")
	public Response cuadraturaPorCaja(@Context HttpServletRequest request,
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrCaja, @QueryParam("criterio") Integer criterio) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResumenMovimientoDTO resumen = cuadraturaBusiness.obtieneMovimientos(fechaInicio, usuario, centroCosto, corrCaja, criterio);
			if(!(resumen==null)) return Response.ok(resumen).build();
			else return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		} catch (Exception ex) {
			log.error("Busca Cuadratura centro costo, Usuario " + request.getUserPrincipal()
					+ " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/imprimecuadratura")
	public Response informeCuadraturaGeneral(@Context HttpServletRequest request, ResumenMovimientoDTO resumenCuad) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		UsuarioDTO usuarioDTO = usuarioBusiness.obtenerUsuario(resumenCuad.getUsuario());
		try {

			Map<String, Object> parametros = new HashMap<String, Object>();

			
			parametros.put("GLOSA_CC", resumenCuad.getGlosaCC());
			parametros.put("COD_CC", resumenCuad.getCentroCosto());
			
			if(resumenCuad.getCorrCaja() != null){
				parametros.put("CANT_CAJAS", resumenCuad.getCorrCaja().toString()); 
			}
			else{
				parametros.put("CANT_CAJAS", "TODAS"); 
			}

			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

			Date today = Calendar.getInstance().getTime();        
			String fechaReporte = df.format(today);
			
			parametros.put("FECHA_CUAD", resumenCuad.getFechaDesde().substring(0, 2) +"/"+resumenCuad.getFechaDesde().substring(2, 4)+"/"+resumenCuad.getFechaDesde().substring(4, 8));
			parametros.put("TOTAL_SERVICIOS", resumenCuad.getTotalMovimientos());
			parametros.put("TOTAL_BOLETAS", resumenCuad.getTotalBoletas());
			parametros.put("TOTAL_FACTURAS", resumenCuad.getTotalFacturas());
			if(resumenCuad.getUsuario() == null){
				parametros.put("USUARIO", "TODOS");
			}
			else{
				parametros.put("USUARIO", usuarioDTO.getUsername());
			}
			
			parametros.put("FECHA", fechaReporte);
			parametros.put("HORA", util.getHora()); 
			parametros.put("TOTAL_NUM_SERVICIOS_BOL", resumenCuad.getTotales().getTotBrutoNumMovBoleta());
			parametros.put("TOTAL_BRUTO_BOL",  resumenCuad.getTotales().getTotBrutoTransaccionBoleta());
			parametros.put("TOTAL_MON_BRUTO_CANCEL_BOL", resumenCuad.getTotales().getTotBrutoCanceladoBoleta());
			parametros.put("TOTAL_DIF_BRUTO_BOL", resumenCuad.getTotales().getTotBrutoDiferenciaBoleta());
			parametros.put("TOTAL_NUM_SERVICIOS_FACT", resumenCuad.getTotales().getTotBrutoNumMovFactura());
			parametros.put("TOTAL_BRUTO_FACT", resumenCuad.getTotales().getTotBrutoTransaccionFactura());
			parametros.put("TOTAL_MON_BRUTO_CANC_FACT", resumenCuad.getTotales().getTotBrutoCanceladoFactura());
			parametros.put("TOTAL_DIF_BRUTO_FACT", resumenCuad.getTotales().getTotBrutoDiferenciaFactura());
			parametros.put("TOTAL_NETO_BOL", resumenCuad.getTotales().getTotNetoTransaccionBoleta());
			parametros.put("TOTAL_MON_NETO_CANCEL_BOL", resumenCuad.getTotales().getTotNetoCanceladoBoleta());
			parametros.put("TOTAL_DIF_NETO_BOL", resumenCuad.getTotales().getTotNetoDiferenciaBoleta());
			parametros.put("TOTAL_NETO_FACT", resumenCuad.getTotales().getTotNetoTransaccionFactura());
			parametros.put("TOTAL_MON_NETO_CANC_FACT", resumenCuad.getTotales().getTotNetoCanceladoFactura());
			parametros.put("TOTAL_DIF_NETO_FACT", resumenCuad.getTotales().getTotNetoDiferenciaFactura());
			parametros.put("TOTAL_IVA_BOL", resumenCuad.getTotales().getTotIvaTransaccionBoleta());
			parametros.put("TOTAL_MON_IVA_CANCEL_BOL", resumenCuad.getTotales().getTotIvaCanceladoBoleta());
			parametros.put("TOTAL_IVA_FACT", resumenCuad.getTotales().getTotIvaTransaccionFactura());
			parametros.put("TOTAL_MON_IVA_CANC_FACT", resumenCuad.getTotales().getTotIvaCanceladoFactura());
			parametros.put("TOTAL_DIF_IVA_FACT", resumenCuad.getTotales().getTotIvaDiferenciaFactura());
			parametros.put("SUM_VENTA_BRUTO_TRAN", (resumenCuad.getTotales().getTotBrutoTransaccionBoleta()+ resumenCuad.getTotales().getTotBrutoTransaccionFactura()));
			parametros.put("SUM_VENTA_BRUTO_CANCEL", (resumenCuad.getTotales().getTotBrutoCanceladoBoleta()+resumenCuad.getTotales().getTotBrutoCanceladoFactura()));
			parametros.put("SUM_VENTA_BRUTO_DIF", (resumenCuad.getTotales().getTotBrutoDiferenciaBoleta()+ resumenCuad.getTotales().getTotBrutoDiferenciaFactura()));
//			
//			
//
			byte[] archivo = generador.create("cuadraturaGeneral", resumenCuad.getMovimientos(), parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.error("Busca Centros Costos, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	
	@GET
	@Produces("application/json")
	@Path("/obtenerIVA")
	public Response obtenerIVA(@Context HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResponseDTO response = facturaBusiness.obtenerIVA();
			return Response.ok(response).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Produces("application/json")
	@Path("/detalleboleta")
	public Response obtineDetalleBoleta(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrCaja, 
			@QueryParam("criterio") Integer criterio) {
		
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			DetalleBoletaDTO detalleBoleta =  cuadraturaBusiness.obtieneDetalleBoleta(fechaInicio, usuario, corrCaja, centroCosto, criterio);
			
			return Response.ok(detalleBoleta).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	
	@GET
	@Produces("application/json")
	@Path("/imprimedetalleboleta")
	public Response imprimeDetalleBoleta(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrCaja, 
			@QueryParam("criterio") Integer criterio) {
		
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			DetalleBoletaDTO detalleBoleta =  cuadraturaBusiness.obtieneDetalleBoleta(fechaInicio, usuario, corrCaja, centroCosto, criterio);
			ResponseDTO iva = facturaBusiness.obtenerIVA();
			Float ivaNum = Float.parseFloat(iva.getMsgControl());
			Map<String, Object> parametros = new HashMap<String, Object>();

			UsuarioDTO usuarioDTO = usuarioBusiness.obtenerUsuario(usuario);
			
			parametros.put("FECHA_CAJA", fechaInicio.substring(0, 2) +"/"+fechaInicio.substring(2, 4)+"/"+fechaInicio.substring(4, 8));
			parametros.put("USUARIO", usuarioDTO.getNombre());
			parametros.put("CENTRO_COSTO", centroCosto.toString()); 
			parametros.put("CORR_CAJA", corrCaja.toString()); 
			parametros.put("TOTAL_BOLETA", detalleBoleta.getTotalBoletas().toString()); 
			parametros.put("TOTAL_FACTURAS", detalleBoleta.getTotalFacturas().toString()); 
			parametros.put("TOT_TRANSAC_BRUTO", detalleBoleta.getTotalCalculado()); 
			parametros.put("TOT_CANCE_BRUTO", detalleBoleta.getTotalPagado());
			parametros.put("TOT_DIFER_BRUTO", detalleBoleta.getTotalDiferencia());
			
			parametros.put("TOT_TRANSAC_NETO", Long.valueOf(Math.round(detalleBoleta.getTotalCalculado() / (1 + ivaNum/100))));
			parametros.put("TOT_TRANSAC_IVA", detalleBoleta.getTotalCalculado() - Long.valueOf(Math.round(detalleBoleta.getTotalCalculado() / (1 + ivaNum/100))));
			
			parametros.put("TOT_CANCE_NETO", Long.valueOf(Math.round(detalleBoleta.getTotalPagado() / (1 + ivaNum/100))));
			parametros.put("TOT_CANCE_IVA", detalleBoleta.getTotalPagado() - Long.valueOf(Math.round(detalleBoleta.getTotalPagado() / (1 + ivaNum/100))));
			
			parametros.put("TOT_DIFER_NETO", Long.valueOf(Math.round(detalleBoleta.getTotalDiferencia() / (1 + ivaNum/100))));
			parametros.put("TOT_DIFER_IVA", detalleBoleta.getTotalDiferencia() - Long.valueOf(Math.round(detalleBoleta.getTotalDiferencia() / (1 + ivaNum/100))));
			
			
			byte[] archivo = generador.create("detalleBolFact", detalleBoleta.getItems(), parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
		
	
	@POST
	@Produces("application/json")
	@Path("/detalleboletamodal")
	public Response modalDetalleBoleta(@Context HttpServletRequest request, DetalleBoletaFacturaDTO detalle) {
		
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			//Detalle de la boleta
			detalle =  cuadraturaBusiness.obtieneDetalleBoletaFactura(detalle);
			//Detalle de la transaccion
			if(detalle.getBoletaFactura().equals("B")){
				detalle.setDatosBoletaFactura(cuadraturaBusiness.getDetalleBoletaModal(detalle));
			}
			if(detalle.getBoletaFactura().equals("F")){
				detalle = cuadraturaBusiness.getDetalleFacturaModal(detalle);
			}
			
			return Response.ok(detalle).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}	
	
	
	
	
	
	
	@GET
	@Produces("application/json")
	@Path("/detalleservicio")
	public Response obtineDetalleServicio(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrcaja, 
			@QueryParam("criterio") Integer criterio, @QueryParam("ordenamiento") Integer ordenamiento) {
		
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			
			DetalleServicioDTO detalleservicio =  cuadraturaBusiness.obtieneDetalleServicio(fechaInicio, usuario, corrcaja, centroCosto, criterio, ordenamiento);
			detalleservicio.setCentroCosto(centroCosto);
			detalleservicio.setCorrCaja(corrcaja);
			detalleservicio.setFecha(fechaInicio);
			detalleservicio.setUsuario(usuario);
			return Response.ok(detalleservicio).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/imprimedetalleservicio")
	public Response imprimeDetalleServicio(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrcaja, 
			@QueryParam("criterio") Integer criterio, @QueryParam("ordenamiento") Integer ordenamiento) {
		
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		UsuarioDTO usuarioDTO = usuarioBusiness.obtenerUsuario(usuario);
		try {
			
			DetalleServicioDTO detalleservicio =  cuadraturaBusiness.obtieneDetalleServicio(fechaInicio, usuario, corrcaja, centroCosto, criterio, ordenamiento);
			detalleservicio.setCentroCosto(centroCosto);
			detalleservicio.setCorrCaja(corrcaja);
			detalleservicio.setFecha(fechaInicio);
			detalleservicio.setUsuario(usuario);
			
			
			ResponseDTO iva = facturaBusiness.obtenerIVA();
			Float ivaNum = Float.parseFloat(iva.getMsgControl());
			Map<String, Object> parametros = new HashMap<String, Object>();

			
			parametros.put("FECHA_CAJA", fechaInicio.substring(0, 2) +"/"+fechaInicio.substring(2, 4)+"/"+fechaInicio.substring(4, 8));
			parametros.put("USUARIO", usuarioDTO.getNombre());
			parametros.put("CENTRO_COSTO", centroCosto.toString()); 
			parametros.put("CORR_CAJA", corrcaja.toString()); 
			parametros.put("TOTAL_SERVICIOS", String.valueOf(detalleservicio.getServicios().size())); 

			parametros.put("TOT_TRANSAC_BRUTO", detalleservicio.getTotalCalculado()); 
			parametros.put("TOT_CANCE_BRUTO", detalleservicio.getTotalPagado());
			parametros.put("TOT_DIFER_BRUTO", detalleservicio.getTotalDiferencia());
			
			parametros.put("TOT_TRANSAC_NETO", Long.valueOf(Math.round(detalleservicio.getTotalCalculado() / (1 + ivaNum/100))));
			parametros.put("TOT_TRANSAC_IVA", detalleservicio.getTotalCalculado() - Long.valueOf(Math.round(detalleservicio.getTotalCalculado() / (1 + ivaNum/100))));
			
			parametros.put("TOT_CANCE_NETO", Long.valueOf(Math.round(detalleservicio.getTotalPagado() / (1 + ivaNum/100))));
			parametros.put("TOT_CANCE_IVA", detalleservicio.getTotalPagado() - Long.valueOf(Math.round(detalleservicio.getTotalPagado() / (1 + ivaNum/100))));
			
			parametros.put("TOT_DIFER_NETO", Long.valueOf(Math.round(detalleservicio.getTotalDiferencia() / (1 + ivaNum/100))));
			parametros.put("TOT_DIFER_IVA", detalleservicio.getTotalDiferencia() - Long.valueOf(Math.round(detalleservicio.getTotalDiferencia() / (1 + ivaNum/100))));
			
			
			byte[] archivo = generador.create("detalleServicios", detalleservicio.getServicios(), parametros);
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/validacionaclaracion")
	public Response validacionAclaracion(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrcaja, 
			@QueryParam("criterio") Integer criterio) {
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		
		try { 
     		CuadraturaAclaracionDTO cuadraturaAclaracion =  cuadraturaBusiness.validaAclaraciones(corrcaja);
     		PDFResponseDTO pdf = new PDFResponseDTO();
     		
     		if(cuadraturaAclaracion.getDetalleAclaracion().size() == 0){
     			pdf.setIdControl(1);
     		}
     		else{
				ResponseDTO iva = facturaBusiness.obtenerIVA();
				Float ivaNum = Float.parseFloat(iva.getMsgControl());
				Map<String, Object> parametros = new HashMap<String, Object>();
	
				
				parametros.put("FECHA_CAJA", fechaInicio.substring(0, 2) +"/"+fechaInicio.substring(2, 4)+"/"+fechaInicio.substring(4, 8));
				parametros.put("USUARIO", usuario);
				parametros.put("CENTRO_COSTO", centroCosto.toString()); 
				parametros.put("CORR_CAJA", corrcaja.toString()); 
				parametros.put("TOTAL_ACLS", cuadraturaAclaracion.getNumeroAclaraciones().toString()); 
				parametros.put("AVISO_PAGO", cuadraturaAclaracion.getNumeroAvisos().toString()); 
				//
	//			
	//			
				byte[] archivoAcls = generador.create("informeValAclaracion", cuadraturaAclaracion.getDetalleAclaracion(), parametros);
				pdf.setReporte(archivoAcls);
				pdf.setIdControl(0);
     		}

			return Response.ok(pdf).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	
	
	
	@GET
	@Produces("application/json")
	@Path("/solicitudreclamo")
	public Response solicitudReclamo(@Context HttpServletRequest request, 
			@QueryParam("fechaDesde") String fechaInicio, @QueryParam("usuario") String usuario, 
			@QueryParam("centrocosto") Integer centroCosto, @QueryParam("corrcaja") Integer corrcaja, 
			@QueryParam("criterio") Integer criterio) {
		
		
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try { 

			Map<String, Object> parametros = new HashMap<String, Object>();
			ReclamoDTO reclamos = cuadraturaBusiness.solicitudesReclamos(corrcaja);
			
			PDFResponseDTO pdf2 = new PDFResponseDTO();
			
			if(reclamos.getSolReclamos().size() == 0){
				pdf2.setIdControl(1);
			}
			else{
			
				parametros.put("FECHA_CAJA", fechaInicio.substring(0, 2) +"/"+fechaInicio.substring(2, 4)+"/"+fechaInicio.substring(4, 8));
				parametros.put("USUARIO", usuario);
				parametros.put("CENTRO_COSTO", centroCosto.toString()); 
				parametros.put("CORR_CAJA", corrcaja.toString()); 
				parametros.put("ACLS_ESP",  reclamos.getSolReclamos().size() /**reclamos.getAclaracionEspecial()**/); 
				
				byte[] archSolRecl = generador.create("solicitudReclamo", reclamos.getSolReclamos(), parametros);
				pdf2.setReporte(archSolRecl);
				pdf2.setIdControl(0);
			}

			return Response.ok(pdf2).build();

		} catch (Exception ex) {
			log.info("Factura - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
