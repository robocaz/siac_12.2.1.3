package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EstadoCivil database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadoCivil.findAll", query="SELECT t FROM Tbl_EstadoCivil t")
public class Tbl_EstadoCivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstCivil")
	private byte fld_CodEstCivil;

	@Column(name="Fld_EstadoCivil")
	private String fld_EstadoCivil;

	@Column(name="Fld_GlosaEstCivil")
	private String fld_GlosaEstCivil;

	public Tbl_EstadoCivil() {
	}

	public byte getFld_CodEstCivil() {
		return this.fld_CodEstCivil;
	}

	public void setFld_CodEstCivil(byte fld_CodEstCivil) {
		this.fld_CodEstCivil = fld_CodEstCivil;
	}

	public String getFld_EstadoCivil() {
		return this.fld_EstadoCivil;
	}

	public void setFld_EstadoCivil(String fld_EstadoCivil) {
		this.fld_EstadoCivil = fld_EstadoCivil;
	}

	public String getFld_GlosaEstCivil() {
		return this.fld_GlosaEstCivil;
	}

	public void setFld_GlosaEstCivil(String fld_GlosaEstCivil) {
		this.fld_GlosaEstCivil = fld_GlosaEstCivil;
	}

}