package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtrConsultaBIC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CtrConsultaBIC.findAll", query="SELECT t FROM Tbl_CtrConsultaBIC t")
public class Tbl_CtrConsultaBIC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_CTRCONSULTABIC_FLD_RUTAFECTADO_GENERATOR", sequenceName="FLD_RUTAFECTADO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_CTRCONSULTABIC_FLD_RUTAFECTADO_GENERATOR")
	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_FecConsulta")
	private Timestamp fld_FecConsulta;

	public Tbl_CtrConsultaBIC() {
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public Timestamp getFld_FecConsulta() {
		return this.fld_FecConsulta;
	}

	public void setFld_FecConsulta(Timestamp fld_FecConsulta) {
		this.fld_FecConsulta = fld_FecConsulta;
	}

}