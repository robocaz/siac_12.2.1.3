package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_WsUsuarios database table.
 * 
 */
@Entity
@Table(name="Tbl_WsUsuarios")
@NamedQuery(name="Tbl_WsUsuario.findAll", query="SELECT t FROM Tbl_WsUsuario t")
public class Tbl_WsUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecModPassword")
	private Timestamp fld_FecModPassword;

	@Column(name="Fld_FlagModPassword")
	private boolean fld_FlagModPassword;

	@Column(name="Fld_Password")
	private String fld_Password;

	@Column(name="Fld_RazonSocial")
	private String fld_RazonSocial;

	@Column(name="Fld_TipoUsuario")
	private byte fld_TipoUsuario;

	public Tbl_WsUsuario() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecModPassword() {
		return this.fld_FecModPassword;
	}

	public void setFld_FecModPassword(Timestamp fld_FecModPassword) {
		this.fld_FecModPassword = fld_FecModPassword;
	}

	public boolean getFld_FlagModPassword() {
		return this.fld_FlagModPassword;
	}

	public void setFld_FlagModPassword(boolean fld_FlagModPassword) {
		this.fld_FlagModPassword = fld_FlagModPassword;
	}

	public String getFld_Password() {
		return this.fld_Password;
	}

	public void setFld_Password(String fld_Password) {
		this.fld_Password = fld_Password;
	}

	public String getFld_RazonSocial() {
		return this.fld_RazonSocial;
	}

	public void setFld_RazonSocial(String fld_RazonSocial) {
		this.fld_RazonSocial = fld_RazonSocial;
	}

	public byte getFld_TipoUsuario() {
		return this.fld_TipoUsuario;
	}

	public void setFld_TipoUsuario(byte fld_TipoUsuario) {
		this.fld_TipoUsuario = fld_TipoUsuario;
	}

}