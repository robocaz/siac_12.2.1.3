package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_AclsFirmantes database table.
 * 
 */
@Entity
@Table(name="Tbl_AclsFirmantes")
@NamedQuery(name="Tbl_AclsFirmante.findAll", query="SELECT t FROM Tbl_AclsFirmante t")
public class Tbl_AclsFirmante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrVigNC")
	private BigDecimal fld_CorrVigNC;

	@Column(name="Fld_FolioNro")
	private short fld_FolioNro;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	public Tbl_AclsFirmante() {
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrVigNC() {
		return this.fld_CorrVigNC;
	}

	public void setFld_CorrVigNC(BigDecimal fld_CorrVigNC) {
		this.fld_CorrVigNC = fld_CorrVigNC;
	}

	public short getFld_FolioNro() {
		return this.fld_FolioNro;
	}

	public void setFld_FolioNro(short fld_FolioNro) {
		this.fld_FolioNro = fld_FolioNro;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

}