package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the OrdenDagDef database table.
 * 
 */
@Entity
@NamedQuery(name="OrdenDagDef.findAll", query="SELECT o FROM OrdenDagDef o")
public class OrdenDagDef implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fld_Column;

	private String fld_ColumnAlias;

	private String fld_Database;

	private String fld_Flag;

	private String fld_Format;

	private String fld_Table;

	private String fld_TableAlias;

	public OrdenDagDef() {
	}

	public String getFld_Column() {
		return this.fld_Column;
	}

	public void setFld_Column(String fld_Column) {
		this.fld_Column = fld_Column;
	}

	public String getFld_ColumnAlias() {
		return this.fld_ColumnAlias;
	}

	public void setFld_ColumnAlias(String fld_ColumnAlias) {
		this.fld_ColumnAlias = fld_ColumnAlias;
	}

	public String getFld_Database() {
		return this.fld_Database;
	}

	public void setFld_Database(String fld_Database) {
		this.fld_Database = fld_Database;
	}

	public String getFld_Flag() {
		return this.fld_Flag;
	}

	public void setFld_Flag(String fld_Flag) {
		this.fld_Flag = fld_Flag;
	}

	public String getFld_Format() {
		return this.fld_Format;
	}

	public void setFld_Format(String fld_Format) {
		this.fld_Format = fld_Format;
	}

	public String getFld_Table() {
		return this.fld_Table;
	}

	public void setFld_Table(String fld_Table) {
		this.fld_Table = fld_Table;
	}

	public String getFld_TableAlias() {
		return this.fld_TableAlias;
	}

	public void setFld_TableAlias(String fld_TableAlias) {
		this.fld_TableAlias = fld_TableAlias;
	}

}