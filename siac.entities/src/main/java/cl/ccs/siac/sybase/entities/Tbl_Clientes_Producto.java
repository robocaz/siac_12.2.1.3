package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Clientes_Productos database table.
 * 
 */
@Entity
@Table(name="Tbl_Clientes_Productos")
@NamedQuery(name="Tbl_Clientes_Producto.findAll", query="SELECT t FROM Tbl_Clientes_Producto t")
public class Tbl_Clientes_Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCobroProd")
	private BigDecimal fld_CorrCobroProd;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecInicioVigencia")
	private Timestamp fld_FecInicioVigencia;

	@Column(name="Fld_FecTerminoVigencia")
	private Timestamp fld_FecTerminoVigencia;

	@Column(name="Fld_RutCliente")
	private String fld_RutCliente;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Clientes_Producto() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCobroProd() {
		return this.fld_CorrCobroProd;
	}

	public void setFld_CorrCobroProd(BigDecimal fld_CorrCobroProd) {
		this.fld_CorrCobroProd = fld_CorrCobroProd;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecInicioVigencia() {
		return this.fld_FecInicioVigencia;
	}

	public void setFld_FecInicioVigencia(Timestamp fld_FecInicioVigencia) {
		this.fld_FecInicioVigencia = fld_FecInicioVigencia;
	}

	public Timestamp getFld_FecTerminoVigencia() {
		return this.fld_FecTerminoVigencia;
	}

	public void setFld_FecTerminoVigencia(Timestamp fld_FecTerminoVigencia) {
		this.fld_FecTerminoVigencia = fld_FecTerminoVigencia;
	}

	public String getFld_RutCliente() {
		return this.fld_RutCliente;
	}

	public void setFld_RutCliente(String fld_RutCliente) {
		this.fld_RutCliente = fld_RutCliente;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}