angular.module('ccs').controller('PagoController', function($scope, CarroCompraResource, PagosResource,TramitanteResource, MediosResource, TransaccionResource, BoletaResource, FacturaResource, FacturaUtil, RutHelper, $location, $rootScope, $filter, $localStorage, $uibModal,$document,$q,$timeout,$http) {

  //$scope.get();
  $scope.transacionesSession = [];
  $scope.totalCobrar = 0;
  $scope.vuelto = 0;
  $scope.montoingresado = "";
  $scope.boletaImpresa = {};
  $scope.listaBoleta = [];
  $scope.boleta = {};
  $scope.boleta.ip = $rootScope.ip;
  $scope.boleta.idCentroCosto = $rootScope.usuario.centroCostoActivo.codigo;
  $scope.cantidadBoletas = 0;
  $scope.keepGoing = true;
  $scope.mediospago = [];
  $scope.mediopagocombo = {};
  $scope.numCaja = $rootScope.usuario.caja;
  $scope.tipoDocumento = "B";
  $scope.nextPrint = false;
  $scope.datospago = {};
  $scope.banco = {};
  var rutAfectadoIter = "";
  $scope.boletasEmitidas = [];
  $scope.boletasAccion = [];
  $scope.mediopagocombo.mediopagoSel = {};
  $scope.descuadre = 0;
  $localStorage.generadas;
  $localStorage.montoingrado;
  $rootScope.disableTram = true;
  $scope.montoingresado = "";
  $scope.listacert = [];
  $scope.tieneServicios = true;
  $localStorage.ultimaBoleta = 0;
  $scope.blockBack = false;

  $scope.iva = 0;
  $scope.comunas = [];
  $scope.facturas = [];
  $scope.facturasEmitidas = [];
  $scope.esAgencia = $rootScope.usuario.esAgencia;

  $scope.obtieneNombreMaquina();
  console.log($rootScope.nombreMaquina);

  $scope.listaBoletaDeclaracion = [];
  $scope.boletaImpresaUso = {};
  $scope.ultBoletaImpresa = 0;
  

  $("#montoingresado").focus();

  if($localStorage.generadas !== undefined){
    $scope.reimpresion = $localStorage.generadas;
  }
  else{
    $scope.reimpresion = false;
  }

  if($localStorage.montoingrado !== undefined){
    $scope.montoingresado = $localStorage.montoingrado;
  }
  else{
    $scope.montoingresado = "";
  }


  $scope.inicializarListadoDecUso = function(){

      $scope.totalCobrar = 0;
      angular.forEach($scope.transacionesSession, function (value, key) {
        $scope.transacionesSession[key].boletaFactura = "B";
        $scope.totalCobrar = parseInt($scope.totalCobrar) + parseInt(value.costoServicio);
      });

      $scope.listaBoletaUso = [];

      $scope.transacionesSession = $filter('orderBy')($scope.transacionesSession, 'rutAfectado');
      

      var rutAfectadoIter = '';
      angular.forEach($scope.transacionesSession, function (value, key) {
        if( FacturaUtil.existeRutEnFacturas(value.rutAfectado, $scope.facturas) ) {
          $scope.transacionesSession[key].boletaFactura = "F";
        }else{
          if(rutAfectadoIter != value.rutAfectado){
            rutAfectadoIter = value.rutAfectado;
            $scope.transacionesSession[key].boletaFactura = "B";
            $scope.boletaImpresa = {};
            $scope.boletaImpresa.tipoDocumento = "B";
            $scope.boletaImpresa.totalBoleta = 0;
            $scope.boletaImpresa.rutTramitante = $rootScope.tramitante.rut;
            $scope.boletaImpresa.rutAfectado  = value.rutAfectado;
            $scope.boletaImpresa.transacciones = $filter('filter')($scope.transacionesSession, { rutAfectado: value.rutAfectado });

            angular.forEach($scope.boletaImpresa.transacciones, function (value, key) {
              $scope.boletaImpresa.totalBoleta = parseInt($scope.boletaImpresa.totalBoleta) + parseInt(value.costoServicio);
            });

            if($scope.boletaImpresa.totalBoleta > 0){
              $scope.listaBoletaUso.push($scope.boletaImpresa);
            }
            $scope.boletaImpresa = {};
          }
        }
      });

  }
  
  $scope.boletaDeclaracionUso = function(){
      $scope.totalCobrar = 0;
      angular.forEach($scope.transacionesSession, function (value, key) {
        $scope.transacionesSession[key].boletaFactura = "B";
        $scope.totalCobrar = parseInt($scope.totalCobrar) + parseInt(value.costoServicio);
        
      });

      
        $scope.boletaImpresaUso = {};
        $scope.boletaImpresaUso.tipoDocumento = "B";
        $scope.boletaImpresaUso.totalBoleta = $scope.totalCobrar;
        $scope.boletaImpresaUso.rutTramitante = $rootScope.tramitante.rut;
        $scope.boletaImpresaUso.rutAfectado  = $rootScope.tramitante.rut;
        $scope.boletaImpresaUso.transacciones = $filter('filter')($scope.transacionesSession, { boletaFactura: 'B' });



        if($scope.boletaImpresaUso.totalBoleta > 0){
          $scope.listaBoletaDeclaracion.push($scope.boletaImpresaUso);
        }
  }

  
  
  $scope.inicializarListado = function(){
    $scope.totalCobrar = 0;
    angular.forEach($scope.transacionesSession, function (value, key) {
      $scope.transacionesSession[key].boletaFactura = "B";
      $scope.totalCobrar = parseInt($scope.totalCobrar) + parseInt(value.costoServicio);
    });

    $scope.listaBoleta = [];

    $scope.transacionesSession = $filter('orderBy')($scope.transacionesSession, 'rutAfectado');

    //armo arreglo boletas
    var rutAfectadoIter = '';
    angular.forEach($scope.transacionesSession, function (value, key) {
      if( FacturaUtil.existeRutEnFacturas(value.rutAfectado, $scope.facturas) ) {
        $scope.transacionesSession[key].boletaFactura = "F";
      }else{
        if(rutAfectadoIter != value.rutAfectado){
          rutAfectadoIter = value.rutAfectado;
          $scope.transacionesSession[key].boletaFactura = "B";
          $scope.boletaImpresa = {};
          $scope.boletaImpresa.tipoDocumento = "B";
          $scope.boletaImpresa.totalBoleta = 0;
          $scope.boletaImpresa.rutTramitante = $rootScope.tramitante.rut;
          $scope.boletaImpresa.rutAfectado  = value.rutAfectado;
          $scope.boletaImpresa.transacciones = $filter('filter')($scope.transacionesSession, { rutAfectado: value.rutAfectado });

          angular.forEach($scope.boletaImpresa.transacciones, function (value, key) {
            $scope.boletaImpresa.totalBoleta = parseInt($scope.boletaImpresa.totalBoleta) + parseInt(value.costoServicio);
          });

          if($scope.boletaImpresa.totalBoleta > 0){
            $scope.listaBoleta.push($scope.boletaImpresa);
          }
          $scope.boletaImpresa = {};
        }
      }
    });
	actListarServicioTramitantePago();
	
    if($scope.totalCobrar == 0){
      $scope.vuelto = 0;
    }
    else{
      $scope.vueltofun();
    }

    $scope.cantidadBoletas = $scope.listaBoleta.length;
    if($scope.transacionesSession.length == 0){
      $scope.tieneServicios = false;
    }

    console.log('***inicializarListado - Boletas***');
    console.log($scope.listaBoleta);
    console.log('***inicializarListado - Facturas***');
    console.log($scope.facturas);
  }



  $scope.vueltofun = function(){

    if($scope.montoingresado != "0" && parseInt($scope.montoingresado) > 0){
      if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){

        swal({
          title: 'Aviso',
          text: "Ingresar un monto menor al calculado, implica descuadrar la caja",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continuar',
          allowOutsideClick: false
        }).then(function () {

          var modalInstance = $uibModal.open({
            templateUrl: 'views/modal/modalloginsupervisor.html',
            controller: 'ModalLoginSupervisorController',
            appendTo: angular.element($document[0].querySelector('#tblpagos')),
            size: 'sm'
          });
          modalInstance.result.then(function (respuesta) {
            if(respuesta){
              $scope.vuelto =  parseInt($scope.montoingresado) - parseInt($scope.totalCobrar);
            }
            else{
              $scope.vuelto = "";
              $scope.montoingresado = "";
              $("#montoingresado").focus();
            }
          });
          $scope.descuadre = 1;

        }, function(dismiss) {
          $scope.vuelto = "";
          $scope.montoingresado = "";
          $scope.$apply();
          $("#montoingresado").focus();
        });

      }
      else{
        $scope.vuelto =  parseInt($scope.montoingresado) - parseInt($scope.totalCobrar);
      }
    }
  }



  $scope.imprimeTicket = function (ticket) {

    var url = "http://localhost:8080/terminal/siac/impresion";

    $http({
      method: 'POST',
      url: url,
      data: JSON.stringify(ticket),
      timeout: 4000
    }).then(function(response) {
      console.log(unit, response);
    }, function(response) {
      console.log(response);
    });
  }





  $scope.printNext = function(proxBoleta, nextReg){

	$("#btnImprimir1").attr("disabled", true);  
    $scope.blockBack = true;
    console.log('printNext');
    console.log($scope.boletaPrint);

    //Envio a BD la boleta inicial y sucesoras
    
    angular.forEach($scope.boletaPrint.transacciones, function (item) {
      item.tipoServicio = item.glosaBoleta;
    });
    
    
    
    //PagosResource.imprimeBoleta($scope.boletaPrint);
	imprimeBoleta($scope.boletaPrint);

    $scope.imprimeTicket($scope.boletaPrint);


    $scope.boletasEmitidas.push($scope.boletaPrint);

    //aumento numero boleta actual
    PagosResource.actualizaBoleta($scope.boletaPrint);



    angular.element('#boleta').modal("hide");


    if($localStorage.ultimaBoleta == 0){
      $scope.ultimaBoleta = $scope.boletaPrint.numeroBoleta;
      $localStorage.ultimaBoleta = $scope.ultimaBoleta;
    }
    else{
      $scope.ultimaBoleta = $localStorage.ultimaBoleta;
    }



    if(proxBoleta <= $scope.boleta.finBoleta){
      if(nextReg > 0 && nextReg < $scope.listaBoleta.length){
        swal({
          title: 'Aviso',
          text: "Proxima Boleta " +  $scope.boletaPrint.proxBoleta,
          type: 'info',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Continuar',
          allowOutsideClick: false
        }).then(function () {
          $("#btnImprimir1").attr("disabled", false); 

          //if(nextReg > 0 && nextReg < $scope.listaBoleta.length){

          $scope.boletaPrint = $scope.listaBoleta[nextReg];
          $scope.boletaPrint.numeroBoleta = proxBoleta;
          $scope.ultBoletaImpresa = $scope.boletaPrint.numeroBoleta;
          $scope.boletaPrint.proxBoleta = proxBoleta + 1;
          $scope.boletaPrint.nextReg = nextReg + 1;
          $scope.boletaPrint.idCaja = $scope.numCaja;
          $scope.boletaPrint.descCentroCosto = $scope.boleta.descCentroCosto;
          $scope.boletaPrint.idCentroCosto = $scope.boleta.idCentroCosto;
          $scope.boletaPrint.fecha = new Date();
          $scope.boletaPrint.medioPago = $scope.mediopagocombo.mediopagoSel;
          $scope.boletaPrint.banco = $scope.datospago.banco;
          $scope.boletaPrint.sucursal = $scope.datospago.sucursal;
          $scope.boletaPrint.numerocheque = $scope.datospago.numerocheque;
          $scope.boletaPrint.operacion = $scope.datospago.operacion;
          $scope.boletaPrint.autorizacion = $scope.datospago.autorizacion;
          $scope.boletaPrint.total = $scope.totalCobrar;
          if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){
            $scope.boletaPrint.montoingresado = $scope.montoingresado;
          }
          else{
            $scope.boletaPrint.montoingresado = $scope.totalCobrar;
          }

          $scope.boletaPrint.tipoDocumento = $scope.tipoDocumento;
          $scope.boletaPrint.nombreMaquina = $scope.boleta.nombreMaquina;
          $scope.$apply();
          angular.element('#boleta').modal();

        });
      }
      else{
        $scope.datospago = {};
        $localStorage.generadas = true;
        $scope.reimpresion = $localStorage.generadas;
      }
    }
    else{
      swal('Alerta','No hay mas boletas disponibles','error');
      $scope.datospago = {};
      $localStorage.generadas = true;
      $scope.reimpresion = $localStorage.generadas;
    }

  }

	function imprimeBoleta(boletaPrint) {
		console.log("> imprimeBoleta..");
		var successCallback = function(data, responseHeaders) {
			console.log("> imprimeBoleta data:");
			console.log(data);
			actListarServicioTramitantePago();
		};
		PagosResource.imprimeBoleta(boletaPrint, successCallback, $scope.errorCallback);
	}

	function actListarServicioTramitantePago() {
		console.log("> actListarServicioTramitantePago..");
		var successCallback = function(data, responseHeaders) {
			$rootScope.transacciones = data;
			$scope.transacionesSession = $rootScope.transacciones;
		};
		TransaccionResource.listarservicio(successCallback, $scope.errorCallback);
	}

  $scope.mostrarBoletaDeclUso = function(){

    console.log('mostrarBoleta');
    console.log($scope.listaBoleta);

    if($scope.totalCobrar == 0){
      swal('Alerta','No se pueden imprimir boletas por monto 0','error');
      return false;
    }
    if($scope.montoingresado === undefined || $scope.montoingresado == "" || parseInt($scope.montoingresado) == 0){
        swal('Alerta','Ud. Debe ingresar el monto a cancelar','error');
        $("#montoingresado").focus();
        return false;
    }
    var numboletaprint = 0
    if(parseInt($scope.boleta.actualBoleta) == 1){
      numboletaprint = 1;
    }
    else if(parseInt($scope.boleta.actualBoleta) == parseInt($scope.boleta.finBoleta)){
      numboletaprint = parseInt($scope.boleta.actualBoleta) - 1;
    }
    else{
      numboletaprint = parseInt($scope.boleta.actualBoleta);
    }
    ///

    $scope.cantidadBoletas = $scope.listaBoleta.length;

    swal({
      title: 'Aviso',
      //text: "Se imprimirá boleta N° " + (parseInt(numboletaprint) + 1) + " en adelante",
      text: "Se imprimirá boleta N° " + (parseInt(numboletaprint))  + " en adelante",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {

      if($scope.montoingresado === undefined || $scope.montoingresado == "" || $scope.montoingresado == 0){
        swal('Alerta','Ud. Debe ingresar el monto a cancelar','error');
        return false;
      }

      if($scope.mediopagocombo.mediopagoSel === undefined || $scope.mediopagocombo.mediopagoSel == null){
        swal('Alerta','Seleccione Medio de Pago','error');
        return false;
      }

      if($scope.boleta.actualBoleta <= $scope.boleta.finBoleta){
        $scope.boletaPrint = $scope.listaBoletaDeclaracion[0];

        console.log($scope.boletaPrint);

        if(parseInt($scope.boleta.actualBoleta) == 1){
          $scope.boletaPrint.numeroBoleta = $scope.boleta.actualBoleta;
        }
        else{
          $scope.boletaPrint.numeroBoleta = numboletaprint;
          $scope.ultBoletaImpresa = numboletaprint;
        }

        $scope.boletaPrint.proxBoleta = numboletaprint + 1;
        $scope.boletaPrint.idCaja = $scope.numCaja;
        $scope.boletaPrint.descCentroCosto = $scope.boleta.descCentroCosto;
        $scope.boletaPrint.idCentroCosto = $scope.boleta.idCentroCosto;
        $scope.boletaPrint.fecha = new Date();
        $scope.boletaPrint.medioPago = $scope.mediopagocombo.mediopagoSel;
        $scope.boletaPrint.total = $scope.totalCobrar;
        $scope.boletaPrint.banco = $scope.datospago.banco;
        $scope.boletaPrint.sucursal = $scope.datospago.sucursal;
        $scope.boletaPrint.numerocheque = $scope.datospago.numerocheque;
        $scope.boletaPrint.operacion = $scope.datospago.operacion;
        $scope.boletaPrint.autorizacion = $scope.datospago.autorizacion;
        if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){
          $scope.boletaPrint.montoingresado = $scope.montoingresado;
        }
        else{
          $scope.boletaPrint.montoingresado = $scope.totalCobrar;
        }
        $localStorage.montoingrado = $scope.montoingresado;
        $scope.boletaPrint.tipoDocumento = "B"; //$scope.tipoDocumento;
        $scope.boletaPrint.nombreMaquina = $scope.boleta.nombreMaquina;
        $scope.boletaPrint.nextReg = 0;
        if($scope.cantidadBoletas > 1){
          $scope.boletaPrint.nextReg = 1;
        }

        $scope.$apply();
        angular.element('#boleta').modal();
      }
      else{
        swal('Alerta','No hay mas boletas disponibles','error');
        return false;
      }
    }, function(dismiss) {
      return false;
    });
  }
  
  
  $scope.printBoletaDecUso = function(){
	  $("#btnImprimir2").attr("disabled", true);
	  $scope.blockBack = true;
      console.log('printNext');
      console.log($scope.boletaPrint);
      console.log($scope.listaBoletaUso);
      //Envio a BD la boleta inicial y sucesoras

      
      // $scope.boletaPrint.transacciones.splice(0, 1);
       
      
      $scope.boletaPrint.tercero = "1";
      PagosResource.imprimeBoleta($scope.boletaPrint);
      $scope.imprimeTicket($scope.boletaPrint);
      
      
      
      
      $scope.boletasEmitidas.push($scope.boletaPrint);
      angular.element('#boleta').modal('hide');
      
      
      
      //aumento numero boleta actual
      PagosResource.actualizaBoleta($scope.boletaPrint);





      
  }
  
  
  
  $scope.mostrarBoleta = function(){
	  
	  
	$scope.$emit('logout');
  
    console.log('mostrarBoleta');
    console.log($scope.listaBoleta);

    if($rootScope.tramitante.declaracionUso){
      $scope.mostrarBoletaDeclUso();
      return false;
    }
    
    
    if($scope.totalCobrar == 0){
      swal('Alerta','No se pueden imprimir boletas por monto 0','error');
      return false;
    }
    
    if($scope.montoingresado === undefined || $scope.montoingresado == "" || parseInt($scope.montoingresado) == 0){
        swal('Alerta','Ud. Debe ingresar el monto a cancelar','error');
        $scope.montoingresado = "";
        $("#montoingresado").focus();
        return false;
    }
    
    var numboletaprint = 0
    if(parseInt($scope.boleta.actualBoleta) == 1){
      numboletaprint = 1;
    }
    else if(parseInt($scope.boleta.actualBoleta) == parseInt($scope.boleta.finBoleta)){
      numboletaprint = parseInt($scope.boleta.actualBoleta) - 1;
    }
    else{
      numboletaprint = parseInt($scope.boleta.actualBoleta);
    }
    ///

    $scope.cantidadBoletas = $scope.listaBoleta.length;

    swal({
      title: 'Aviso',
      //text: "Se imprimirá boleta N° " + (parseInt(numboletaprint) + 1) + " en adelante",
      text: "Se imprimirá boleta N° " + (parseInt(numboletaprint))  + " en adelante",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {

      if($scope.montoingresado === undefined || $scope.montoingresado == "" || $scope.montoingresado == 0){
        swal('Alerta','Ud. Debe ingresar el monto a cancelar','error');
        return false;
      }

      if($scope.mediopagocombo.mediopagoSel === undefined || $scope.mediopagocombo.mediopagoSel == null){
        swal('Alerta','Seleccione Medio de Pago','error');
        return false;
      }

      if($scope.boleta.actualBoleta <= $scope.boleta.finBoleta){
        $scope.boletaPrint = $scope.listaBoleta[0];

        console.log($scope.boletaPrint);

        if(parseInt($scope.boleta.actualBoleta) == 1){
          $scope.boletaPrint.numeroBoleta = $scope.boleta.actualBoleta;
        }
        else{
          $scope.boletaPrint.numeroBoleta = numboletaprint;
        }

        $scope.boletaPrint.proxBoleta = numboletaprint + 1;
        $scope.ultBoletaImpresa = $scope.boletaPrint.numeroBoleta;
        $scope.boletaPrint.idCaja = $scope.numCaja;
        $scope.boletaPrint.descCentroCosto = $scope.boleta.descCentroCosto;
        $scope.boletaPrint.idCentroCosto = $scope.boleta.idCentroCosto;
        $scope.boletaPrint.fecha = new Date();
        $scope.boletaPrint.medioPago = $scope.mediopagocombo.mediopagoSel;
        $scope.boletaPrint.total = $scope.totalCobrar;
        $scope.boletaPrint.banco = $scope.datospago.banco;
        $scope.boletaPrint.sucursal = $scope.datospago.sucursal;
        $scope.boletaPrint.numerocheque = $scope.datospago.numerocheque;
        $scope.boletaPrint.operacion = $scope.datospago.operacion;
        $scope.boletaPrint.autorizacion = $scope.datospago.autorizacion;
        if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){
          $scope.boletaPrint.montoingresado = $scope.montoingresado;
        }
        else{
          $scope.boletaPrint.montoingresado = $scope.totalCobrar;
        }
        $localStorage.montoingrado = $scope.montoingresado;
        $scope.boletaPrint.tipoDocumento = "B"; //$scope.tipoDocumento;
        $scope.boletaPrint.nombreMaquina = $scope.boleta.nombreMaquina;
        $scope.boletaPrint.nextReg = 0;
        if($scope.cantidadBoletas > 1){
          $scope.boletaPrint.nextReg = 1;
        }

        $scope.$apply();
        angular.element('#boleta').modal();
      }
      else{
        swal('Alerta','No hay mas boletas disponibles','error');
        return false;
      }
    }, function(dismiss) {
      return false;
    });
  }


  $scope.continuar = function(){

    swal({
      title: 'Aviso',
      text: "Proxima Boleta " +  $scope.boletaPrint.proxBoleta,
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      allowOutsideClick: false
    }).then(function () {
      $scope.nextPrint = true;
    }, function(dismiss) {
      $scope.nextPrint = false;
    });
  }



  $scope.consultaBoleta = function() {


    $scope.boleta.idCentroCosto = $rootScope.usuario.centroCostoActivo.codigo;
    $scope.boleta.nombreMaquina = $rootScope.nombreMaquina;

    
    
    var successCallback = function(data,responseHeaders){

      console.log(data);
      if(data.idControl == 0){
        $scope.boleta = data;
        $scope.boleta.descCentroCosto = $rootScope.usuario.centroCostoActivo.descripcion;
        $scope.ultimaBoleta = $scope.boleta.actualBoleta;
        $scope.boletasDisponibles = $scope.boleta.finBoleta;
        var diferencia =  ($scope.boletasDisponibles - $scope.ultimaBoleta);
        if($scope.ultimaBoleta > $scope.boletasDisponibles){
          swal('Alerta','No hay mas boletas disponibles','error');
        }
        if($scope.ultimaBoleta == $scope.boletasDisponibles){
          swal('Alerta','Tiene solo una boleta disponible','error');
        }
        if(diferencia == 10){
          swal('Alerta','Solo le quedan 10 boletas disponibles ','error');
        }
        console.log("Boleta "+ $scope.boleta);
      }
      else{
        $scope.boleta.msgControl = "Error al iniciar el rango de boleta."
      }
    };
    var errorCallback2 = function(response) {
      $scope.error = "Servicio de Aplicación no se encuentra disponible";
    };

    BoletaResource.consultaBoleta($scope.boleta, successCallback, errorCallback2);
  };


  $scope.listarServicioTramitantePago = function() {
    console.log("listarServicioTramitante");
    var successCallback = function(data,responseHeaders){

      $rootScope.transacciones = data;
      $scope.transacionesSession = $rootScope.transacciones;
      if($rootScope.tramitante.declaracionUso){
        $scope.inicializarListadoDecUso();
        $scope.boletaDeclaracionUso();
      }
      else{
        $scope.inicializarListado();  
      }
      
    };
    TransaccionResource.listarservicio( successCallback, $scope.errorCallback);
  };

  $scope.cargaMedios = function() {

    var successCallback = function(data,responseHeaders){
      $scope.mediospago = data;
      $scope.mediopagocombo.mediopagoSel = $filter('filter')($scope.mediospago, {codigo: $scope.mediopagocombo.mediopagoSel.codigo}) [1];

    };
    var errorCallback2 = function(response) {
      $scope.error = "Servicio de Aplicación no se encuentra disponible";
    };
    MediosResource.cargaMedios(successCallback, errorCallback2);
  };

  $scope.abrirModalPago = function(){

    $scope.datospago = {};
    if($scope.mediopagocombo.mediopagoSel.codigo == "EFT"){
      console.log("medio efectivo");
      //$scope.mediopago.numerocheque = "";

    }
    if($scope.mediopagocombo.mediopagoSel.codigo == "CHE"){
      console.log("medio cheque");
      angular.element("#modalcarropagocheque").modal();
    }
    if($scope.mediopagocombo.mediopagoSel.codigo == "VVS"){
      console.log("medio vale vista");
      angular.element("#pagovalevista").modal();
    }
    if($scope.mediopagocombo.mediopagoSel.codigo == "TCD"){
      console.log("medio tarjeta credito");
      angular.element("#pagotarjetacredito").modal();
    }
    if($scope.mediopagocombo.mediopagoSel.codigo == "RC"){
      console.log("medio red compra");
      angular.element("#pagoredcompra").modal();
    }

  }

  $scope.cargaBancosPago = function() {

    var successCallback = function(data,responseHeaders){

      $scope.bancospago = data;
    };
    var errorCallback2 = function(response) {
      $scope.error = "Servicio de Aplicación no se encuentra disponible";
    };
    PagosResource.cargaBancos(successCallback, errorCallback2);
  };

  $scope.cargaSucursales = function() {

    angular.element("#cargando").modal();

    var successCallback = function(data,responseHeaders){
      angular.element("#cargando").modal("hide");
      console.log(data);
      $scope.sucursalespago = [];
      $scope.sucursal = "";
      $scope.sucursalespago = data;
    };
    var errorCallback2 = function(response) {
      $scope.error = "Servicio de Aplicación no se encuentra disponible";
    };
    PagosResource.cargaSucursales($scope.datospago.banco, successCallback, errorCallback2);
  };


  $scope.setDatosMedioPago = function(idDiv){

  $scope.datospago.msgControl = "";
    var flag = false;
    if($scope.mediopagocombo.mediopagoSel.codigo == "CHE" || $scope.mediopagocombo.mediopagoSel.codigo == "VVS"){
      if($scope.datospago.numerocheque === undefined || $scope.datospago.numerocheque == null || $scope.datospago.numerocheque == ""){
        $scope.datospago.msgControl = "Debe ingresar número de documento";
        return false;
      }
      if($scope.datospago.banco == null || $scope.datospago.banco.codigo == ""){
        $scope.datospago.msgControl = "Debe Seleccionar Banco";
        return false;
      }
      if($scope.datospago.sucursal == null || $scope.datospago.sucursal.codSucursal == ""){
        $scope.datospago.msgControl = "Debe Seleccionar Sucursal";
        return false;
      }
      else{
        $scope.datospago.msgControl = "";
        angular.element("#"+idDiv).modal('hide');
      }
    }
    if($scope.mediopagocombo.mediopagoSel.codigo == "RC"){
      if($scope.datospago.autorizacion === undefined || $scope.datospago.autorizacion == null || $scope.datospago.autorizacion == ""){
        $scope.datospago.msgControl = "Debe ingresar código de autorización";
        return false;
      }
      if($scope.datospago.operacion === undefined || $scope.datospago.operacion == null || $scope.datospago.operacion == ""){
        $scope.datospago.msgControl = "Debe ingresar número de operación";
        return false;
      }
      if($scope.datospago.banco == null || $scope.datospago.banco.codigo == ""){
        $scope.datospago.msgControl = "Debe Seleccionar Banco";
        return false;
      }
      else{
        $scope.datospago.msgControl = "";

        $scope.datospago.autorizacion = $scope.datospago.autorizacion.padStart(6, "0");
        $scope.datospago.operacion = $scope.datospago.operacion.padStart(6, "0");
        $scope.datospago.numerocheque = $scope.datospago.autorizacion + $scope.datospago.operacion;
        $scope.datospago.msgControl = "";
        angular.element("#"+idDiv).modal('hide');
      }
    }

    if($scope.mediopagocombo.mediopagoSel.codigo == "TCD"){
      if($scope.datospago.numerocheque === undefined || $scope.datospago.numerocheque == null || $scope.datospago.numerocheque == ""){
        $scope.datospago.msgControl = "Debe ingresar número de comprobante";
        return false;
      }
      if($scope.datospago.banco == null || $scope.datospago.banco.codigo == ""){
        $scope.datospago.msgControl = "Debe Seleccionar Banco";
        return false;
      }
      else{
        $scope.datospago.msgControl = "";
        angular.element("#"+idDiv).modal('hide');

      }
    }
    console.log($scope.datospago);
    $scope.actualizaMedioPago();
  }


  $scope.reimpresionModal = function(){

    angular.element("#reimprimir").modal();
  }


  $scope.seleccion = {};

  $scope.selectedBoletas = function () {
    $scope.seleccion.boletas = $filter('filter')($scope.boletasEmitidas, {checked: true});
  }

  $scope.accionModalBoletas = function(){


    if($scope.seleccion.boletas === undefined || $scope.seleccion.boletas.length == 0){
      swal('Alerta','Ud. Debe seleccionar una o mas boletas para realizar alguna acción','error');
      return false;
    }

    if($scope.accion === undefined || $scope.accion == ""){
      swal('Alerta','Ud. Debe seleccionar la acción a realizar','error');
      return false;
    }
    if($scope.accion == "A" && $scope.ultimaBoleta >= $scope.boletasDisponibles){
      swal('Alerta','No hay mas boletas disponibles','error');
      return false;
    }
    else{
      if($scope.accion == "R"){ // reimpresion
        swal({
          title: 'Aviso',
          text: "¿Está seguro de reimprimir las boletas seleccionadas?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continuar',
          allowOutsideClick: false
        }).then(function () {
          angular.forEach($scope.seleccion.boletas, function (value, key) {
            var boletaId = value.numeroBoleta;
            var selected = value;
            var reprintBol = {};
            angular.forEach($scope.boletasEmitidas, function (boleta, key) {
              if(boleta.numeroBoleta == boletaId && selected){
                $scope.reprintBol = boleta;
                console.log($scope.reprintBol);
                //
                $scope.$apply();
                //angular.element("#reimprimir").modal("hide");
                //angular.element('#boletareprint').modal();
                $scope.imprimeTicket($scope.reprintBol);
                //PagosResource.reimprimirBoleta($scope.reprintBol);
                return;
              }
            });
          });
        });

      }
      if($scope.accion == "A"){ // anulacion

        swal({
          title: 'Aviso',
          text: "¿Está seguro de anular las boletas seleccionadas?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'SI',
          cancelButtonText: 'NO',
          allowOutsideClick: false
        }).then(function () {
          angular.forEach($scope.seleccion.boletas, function (value, key) {
            var boletaId = value.numeroBoleta;
            var selected = value;
            var anulBol = {};
            var reprintBol = {};
            var i = 0;
            $scope.consultaBoleta();
            angular.forEach($scope.boletasEmitidas, function (boleta, key) {
              if(boleta.numeroBoleta == boletaId && selected){

                anulBol = angular.copy(boleta);
                var duplicateObject = JSON.parse(JSON.stringify(boleta));
                //anulBol = boleta;
                reprintBol = boleta;
                console.log($scope.boleta);
                reprintBol.numeroBoleta = parseInt($scope.ultBoletaImpresa) + 1;
                $scope.ultimaBoleta = parseInt($scope.boleta.actualBoleta) + 1;
                console.log(anulBol);
                console.log(reprintBol);
                $scope.boletasEmitidas.splice(i, 1);
                $scope.$apply();
                $scope.boletasEmitidas.push(reprintBol);

                $scope.anularBoleta(duplicateObject);

                $scope.$apply();
                $scope.reprintBol = reprintBol;
                angular.element("#reimprimir").modal("hide");
                angular.element('#boletareprint').modal();


//              PagosResource.imprimeBoleta(reprintBol);
                imprimeBoleta(reprintBol);
                
                PagosResource.actualizaBoleta(reprintBol);

                //  return;
              }
              i++;
            });
          });
        });
      }
    }
  }

  $scope.updateSelection = function(position, entities) {
    console.log(position);
    angular.forEach(entities, function(bol, index) {
      if(entities.length == 1){
        $scope.selectedBoletas();
      }
      if (position != index) {
        bol.checked = false;
        $scope.selectedBoletas();

      }
    });
  }



  $scope.reimpresionBoleta = function(reprintBol){

    $scope.imprimeTicket(reprintBol);
    angular.element('#boletareprint').modal("hide");

  }

  $scope.anularBoleta = function(anulBol){
	console.log("> anularBoleta..");
    var successCallback = function(data,responseHeaders){
      console.log(data);
    };
    var errorCallback2 = function(response) {
      $scope.error = "Servicio de Aplicación no se encuentra disponible";
    };

    PagosResource.anularBoleta(anulBol, successCallback, errorCallback2)
  }



  $scope.aceptarTransaccion = function(){
    console.log('aceptarTransaccion');

	$scope.$emit('logout');


    if($scope.totalCobrar > 0 && ($scope.montoingresado === undefined || $scope.montoingresado == "" || $scope.montoingresado == "0")){
      swal('Atención','Debe ingresar un monto','error');
      return false;
    }

    var numBoletasEmitidas = $scope.boletasEmitidas.length;
    var numFacturasEmitidas = 0;

    if( $scope.facturas.length > 0 ) {
      for (var f=0; f < $scope.facturas.length; f++) {
        numFacturasEmitidas += ( typeof($scope.facturas[f].emitida) === 'undefined' || $scope.facturas[f].emitida === null || $scope.facturas[f].emitida === false ) ? 0 : 1;
      }
    }

    if(numBoletasEmitidas <= 0 && $scope.facturas.length <= 0  && $scope.totalCobrar > 0){
      swal({
        title: 'Aviso',
        text: "No ha emitido Boleta o Factura, ¿Desea aceptar la transacción?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI',
        cancelButtonText: 'NO',
        allowOutsideClick: false
      }).then(function () {

        //llamo a funcion que finaliza transaccion
        $scope.transaccionCierre = {};
        $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
        $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
        $scope.tareasTransaccion();

      }, function(dismiss) {
        return false;
      });
    }else{
      $scope.transaccionCierre = {};
      $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
      $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
      $scope.tareasTransaccion();
    }

  }

  $scope.tareasTransaccion = function (){

    console.log('tareasTransaccion');

    angular.element("#fintransaccion").modal({
      backdrop: 'static',
      keyboard: false
    });
    var promises = [];

    // Procesamos las Facturas
    angular.forEach($scope.facturas, function (value, key) {
      var deferred = $q.defer();

      // si no es agencia, debemos emitir la factura
      if( !$scope.esAgencia ) {

        var successCallbackComunas = function (dataComunas, responseHeaders) {

          // Primero Solicitamos Folio
          var successCallbackFolio = function (dataFactura, responseHeaders) {
            console.log('successCallbackFolioT');
            console.log(dataFactura);

            // Revisamos errores
            if (dataFactura.idControl > 1) {
              deferred.reject('No se logró obtener folio para la Facturación Electrónica, por favor avisar a Soporte');
              return;
            }

            var successCallbackEmision = function (dataEmision, responseHeaders) {
              console.log('successCallbackEmisionT');
              console.log(dataEmision);

              // Revisamos errores
              if (dataEmision.idControl !== 0) {
                deferred.reject(dataEmision.msgControl);
                return;
              }

              dataFactura.pdf = dataEmision.msgControl;
              dataFactura.numeroBoleta = dataFactura.numeroFactura;
            dataFactura.tramitante.rut = RutHelper.getRutSP(dataFactura.tramitante.rut);

              // Actualizamos Transaccion
            var successCallbackActualizarTransaccionFactura = function(dataTransaccion, responseHeaders) {
              console.log('successCallbackEmisionT');
                  console.log(dataEmision);
              if( dataTransaccion.idControl !== 0 ) {
             swal({ title: 'Error', text: dataTransaccion.msgControl, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
             	return;
              }

              dataFactura.isFactura = true;
              dataFactura.isResolved = true;
              deferred.resolve(dataFactura);
            };
            FacturaResource.actualizarTransaccion(dataFactura, successCallbackActualizarTransaccionFactura, $scope.errorCallback);

            };

            dataFactura.tramitante = $rootScope.tramitante;
            dataFactura.medioPago = $scope.mediopagocombo.mediopagoSel.codigo;
            dataFactura.transacciones = [];

            if ($scope.transacionesSession.length > 0) {
              for (var i = 0; i < $scope.transacionesSession.length; i++) {
                dataFactura = $scope.agregarTransaccionFactura(dataFactura, $scope.transacionesSession[i]);
              }
            }

            dataFactura = $scope.recalcularFactura(dataFactura);

            console.log(dataFactura);
            FacturaResource.emitir(dataFactura, successCallbackEmision, $scope.errorCallback);
          }
          $scope.facturas[key].usuario = $rootScope.usuario.username;
          $scope.facturas[key].corrCaja = $rootScope.usuario.caja;
          $scope.facturas[key].comunaDesc = $filter('filter')(dataComunas, {codigo: $scope.facturas[key].codComuna})[0].descripcion;
          FacturaResource.folio($scope.facturas[key], successCallbackFolio, $scope.errorCallback);
        }
        TramitanteResource.cargaComunas({idRegion: $scope.facturas[key].codRegion}, successCallbackComunas, $scope.errorCallback);

      }else{
        $scope.facturas[key].isFactura = true;
        $scope.facturas[key].isResolved = true;
        deferred.resolve($scope.facturas[key]);
      }

      promises.push(deferred.promise);
    });

    // Procesamos las Boletas
    console.log('Procesamos las Boletas...');
    console.log($scope.listaBoleta);
    angular.forEach($scope.listaBoleta, function (value, key) {

      //var promise = $http.get('/data' + i);
      //promises.push(promise);
      var deferred = $q.defer();

      $scope.boletaNoPrint = value;
      // $scope.boletaNoPrint.numeroBoleta = $scope.boleta.actualBoleta + 1;
      // $scope.boletaNoPrint.proxBoleta = $scope.boletaNoPrint.numeroBoleta + 1;
      $scope.boletaNoPrint.idCaja = $scope.numCaja;
      $scope.boletaNoPrint.descCentroCosto = $scope.boleta.descCentroCosto;
      $scope.boletaNoPrint.idCentroCosto = $scope.boleta.idCentroCosto;
      $scope.boletaNoPrint.fecha = $filter('date')(new Date(), "yyyy-MM-dd");
      $scope.boletaNoPrint.medioPago = $scope.mediopagocombo.mediopagoSel;
      $scope.boletaNoPrint.total = $scope.totalCobrar;
      if($scope.boletaTransasaccion !== undefined){
        $scope.boletaNoPrint.banco =  $scope.boletaTransasaccion.banco;
        $scope.boletaNoPrint.sucursal = $scope.boletaTransasaccion.sucursal;
        $scope.boletaNoPrint.numerocheque =  $scope.boletaTransasaccion.numerocheque;
        $scope.boletaNoPrint.operacion = $scope.boletaTransasaccion.operacion;
        $scope.boletaNoPrint.autorizacion = $scope.boletaTransasaccion.autorizacion;
        
      }
      else{
        $scope.boletaNoPrint.banco =  null;
        $scope.boletaNoPrint.sucursal = null;
        $scope.boletaNoPrint.numerocheque =  null;
        $scope.boletaNoPrint.operacion = null;
        $scope.boletaNoPrint.autorizacion = null;

      }
      $scope.boletaNoPrint.montoingresado = $scope.montoingresado;
      if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){
        $scope.boletaNoPrint.montoingresado = $scope.montoingresado;
      }
      else{
        $scope.boletaNoPrint.montoingresado = $scope.totalCobrar;
      }
      $localStorage.montoingrado = $scope.montoingresado;
      $scope.boletaNoPrint.tipoDocumento = $scope.tipoDocumento;
      $scope.boletaNoPrint.nombreMaquina = $scope.boleta.nombreMaquina;

      // $scope.$apply();
      $scope.listacert.push($scope.boletaNoPrint);
      JSON.stringify($scope.boletaNoPrint);

      // actualizaMedioPagoTemp -> SU_TipoPagoTempTransac_SW2 = ACTUALIZA MEDIO DE PAGO EN CADA BOLOETA??!
      $scope.respuestaAct = PagosResource.actualizaTransaccion($scope.boletaNoPrint);
      $scope.respuestaAct.$promise.then(function(data) {
        console.log(data);
        deferred.resolve(data);
        //console.log($scope.boletaNoPrint);
        //angular.element("modalaceptatransaccion").modal();
      });

      promises.push(deferred.promise);

    });

    $q.all(promises).then(
      // success
      // results: an array of data objects from each deferred.resolve(data) call
      function(results) {

        console.log('Results de las promesas!');

        $scope.facturasEmitidas = [];
        angular.forEach(results, function(resultPromise, keyPromise){
          if( typeof(resultPromise.isFactura) !== 'undefined' && typeof(resultPromise.isResolved) !== 'undefined' && resultPromise.isResolved ){
            $scope.facturasEmitidas.push(resultPromise);
          }
        });
        console.log('***facturasEmitidas***');
        console.log($scope.facturasEmitidas);


        $scope.transaccionCierre = {};
        $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
        $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
        $scope.transaccionCierre.corrNombre = $rootScope.tramitante.correlativo;
        $scope.transaccionCierre.nombreMaquina = $scope.boleta.nombreMaquina;
        var successCallbackTransaccion = function(data,responseHeaders){
          console.log('successCallbackTransaccion');
          console.log(data);
          angular.element("#fintransaccion").modal("hide");
          if(data.idControl === 0){
            $scope.resumen = data;

            // Listado de Certificados, no incorpora facturas, se hace la alteración, según datos locales!!!
            /*if( $scope.resumen.lista.length > 0 ){
              for ( var r=0; r < $scope.resumen.lista.length; r++ ) {
                $scope.resumen.lista[r].tipoDocumento = "B";
                if($scope.facturasEmitidas.length > 0){
                  var folioFactura = FacturaUtil.getFolioFacturaRutAfectado($scope.resumen.lista[r].rutAfectado, $scope.facturasEmitidas);
                  if ( folioFactura !== 0 || $scope.facturasEmitidas[0].tipoFacturacion === 'Tercero' ) {
                    $scope.resumen.lista[r].tipoDocumento = "F";
                    $scope.resumen.lista[r].numeroBoleta = folioFactura === 0 ? $scope.facturasEmitidas[0].numeroFactura : folioFactura;
                  }
                }
              }
            }*/

            console.log('*** Resumen ***');
            console.log($scope.resumen);
            if($scope.resumen.lista.length>0){
              angular.element("#listado").modal();
              //$scope.imprimeTodos();
              $scope.imprimeTodosArreglo();
            }
            else{
              swal('Atención','Transacción Exitosa','success');
              $scope.blockBack = false;
              $scope.cancelarTransaccion();

            }
          }else{
            swal({ title: 'Error', text: data.msgControl, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
            return;
          }
        }
        $scope.respuesta = PagosResource.aceptaTransaccion($scope.transaccionCierre, successCallbackTransaccion,$scope.errorCallback);

      },
      // error
      function(error) {
        angular.element("#fintransaccion").modal("hide");
        swal({ title: 'Error', text: error, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }
    );
  }

  $scope.aceptarTransaccionORIG = function(){

    //valido si ingreso valor la cajera
    //valido que se imprimio boleta


    if($scope.totalCobrar > 0 && ($scope.montoingresado === undefined || $scope.montoingresado == "" || $scope.montoingresado == "0")){
      swal('Atención','Debe ingresar un monto','error');
      return false;
    }

    if($scope.boletasEmitidas.length <= 0){
      swal({
        title: 'Aviso',
        text: "No ha emitido Boleta o Factura, ¿Desea aceptar la transacción?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI',
        cancelButtonText: 'NO',
        allowOutsideClick: false
      }).then(function () {

        //llamo a funcion que finaliza transaccion
        $scope.transaccionCierre = {};
        $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
        $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
        $scope.tareasTransaccion();

      }, function(dismiss) {
        return false;
      });
    }
    else{
      $scope.transaccionCierre = {};
      $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
      $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
      $scope.tareasTransaccion();
    }


  }



  $scope.tareasTransaccionORIG = function (){

    //angular.element("#cargando").modal();
	  angular.element("#modalfintransaccion").modal();
    if($scope.montoingresado === undefined || $scope.montoingresado == "" || $scope.montoingresado == "0"){
      swal('Atención','Debe ingresar un monto','error');
      return false;
    }
    else{
      var promises = [];
      angular.forEach($scope.listaBoleta, function (value, key) {


        //var promise = $http.get('/data' + i);
        //promises.push(promise);
        var deferred = $q.defer();

        $scope.boletaNoPrint = value;
        // $scope.boletaNoPrint.numeroBoleta = $scope.boleta.actualBoleta + 1;
        // $scope.boletaNoPrint.proxBoleta = $scope.boletaNoPrint.numeroBoleta + 1;
        $scope.boletaNoPrint.idCaja = $scope.numCaja;
        $scope.boletaNoPrint.descCentroCosto = $scope.boleta.descCentroCosto;
        $scope.boletaNoPrint.idCentroCosto = $scope.boleta.idCentroCosto;
        $scope.boletaNoPrint.fecha = $filter('date')(new Date(), "yyyy-MM-dd");
        $scope.boletaNoPrint.medioPago = $scope.mediopagocombo.mediopagoSel;
        $scope.boletaNoPrint.total = $scope.totalCobrar;
        $scope.boletaNoPrint.banco = $scope.datospago.banco;
        $scope.boletaNoPrint.sucursal = $scope.datospago.sucursal;
        $scope.boletaNoPrint.numerocheque = $scope.datospago.numerocheque;
        $scope.boletaNoPrint.operacion = $scope.datospago.operacion;
        $scope.boletaNoPrint.autorizacion = $scope.datospago.autorizacion;
        $scope.boletaNoPrint.montoingresado = $scope.montoingresado;
        if(parseInt($scope.montoingresado) < parseInt($scope.totalCobrar)){
          $scope.boletaNoPrint.montoingresado = $scope.montoingresado;
        }
        else{
          $scope.boletaNoPrint.montoingresado = $scope.totalCobrar;
        }
        $localStorage.montoingrado = $scope.montoingresado;
        $scope.boletaNoPrint.tipoDocumento = $scope.tipoDocumento;
        $scope.boletaNoPrint.nombreMaquina = $scope.boleta.nombreMaquina;

        // $scope.$apply();
        $scope.listacert.push($scope.boletaNoPrint);
        JSON.stringify($scope.boletaNoPrint);
        $scope.respuestaAct = PagosResource.actualizaTransaccion($scope.boletaNoPrint);
        $scope.respuestaAct.$promise.then(function(data) {
          console.log(data);
          deferred.resolve(data);
          //console.log($scope.boletaNoPrint);
          //angular.element("modalaceptatransaccion").modal();
        });
        promises.push(deferred.promise);

      });

      $q.all(promises).then(
        // success
        // results: an array of data objects from each deferred.resolve(data) call
        function(results) {

          //angular.element("#aceptatransaccion").modal();
          $scope.transaccionCierre = {};
          $scope.transaccionCierre.idCaja = $rootScope.usuario.caja;
          $scope.transaccionCierre.rutTramitante = $rootScope.tramitante.rut;
          $scope.transaccionCierre.nombreMaquina = $scope.boleta.nombreMaquina;
          //$scope.respuesta = $scope.tareasTransaccion();
          var successCallback2 = function(data,responseHeaders){
            angular.element("#cargando").modal("hide");
            if(data.idControl == 0){
              //$scope.cancelarTransaccion();
              console.log(data);
              $scope.resumen = data;

              angular.element("#listado").modal();
            }
          }
          $scope.respuesta = PagosResource.aceptaTransaccion($scope.transaccionCierre,successCallback2, $scope.errorCallback);

        },
        // error
        function(response) {
          console.log(response);
        }
      );
    }

  }

  $scope.obtenerTramitante = function(){
    $scope.tramitante = $rootScope.tramitante;
  }




  $scope.cancelarConfirm = function(){


    swal({
      title: 'Aviso',
      text: "¿Desea cancelar la transacción?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {
      $scope.cancelarTransaccion();
    }, function(dismiss) {
      return false;
    });
  }

  $scope.cancelarTransaccion = function(){
    $scope.aceptaTransaccion = {};
    $scope.aceptaTransaccion.idCaja = $rootScope.usuario.caja;
    $scope.aceptaTransaccion.rutTramitante = $rootScope.tramitante.rut;

    var successCallback2 = function(data,responseHeaders){
      if(data.idControl == 0){
        //angular.element("#listado").modal("hide");

        $scope.tramitante = null;
        $rootScope.tramitante = {};
        $rootScope.disableTram = false;
        $rootScope.rutDisbale = false;
        $localStorage.generadas = undefined;
        $localStorage.montoingrado = undefined;
        $scope.blockBack = false;
        $rootScope.tramitante = {};
        $rootScope.transacciones = [];
        $timeout(function () {
          $location.path("/atencion");
        }, 200);
        
        var url = "http://localhost:8080/terminal/siac/borratemporales";
        $http({
            method: 'POST',
            url: url,
            timeout: 4000,
            headers : {
                'Content-Type' : 'application/json'
             }
          }).then(function(response) {
            console.log(response);
        });
        
        //$location.path("/atencion");
      }
    }
    PagosResource.cancelaTransaccion($scope.aceptaTransaccion, successCallback2, $scope.errorCallback);
  }


  $scope.eliminarServicio = function(servicio) {
    if( FacturaUtil.existeRutEnFacturas(servicio.rutAfectado, $scope.facturas) ) {
      swal({ title: 'Error', text: 'No se puede eliminar el servicio, ya que tiene una factura asociada!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    swal({
      title: 'Aviso',
      text: "¿Desea eliminar servicio?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {
      var successCallback2 = function(data,responseHeaders){
        if(data.idControl == 0){
          $scope.transacionesSession = [];
          $scope.totalCobrar = 0;
          $scope.listarServicioTramitantePago();
        }

      }
      CarroCompraResource.eliminaServicios(servicio,successCallback2, $scope.errorCallback )
    }, function(dismiss) {
      return false;
    });

  }

  $scope.descargar = function(nombreArchivo){
    PagosResource.descargaCertificado();
  }


  $scope.actualizaMedioPago = function(){

    $scope.boletaTransasaccion = {};
    $scope.boletaTransasaccion.banco = $scope.datospago.banco
      $scope.boletaTransasaccion.sucursal = $scope.datospago.sucursal;
      $scope.boletaTransasaccion.numerocheque = $scope.datospago.numerocheque;
      $scope.boletaTransasaccion.operacion = $scope.datospago.operacion;
      $scope.boletaTransasaccion.autorizacion = $scope.datospago.autorizacion;
      $scope.boletaTransasaccion.medioPago =$scope.mediopagocombo.mediopagoSel;
      $scope.boletaTransasaccion.idCaja = $rootScope.usuario.caja;
      PagosResource.actualizaTransaccion($scope.boletaTransasaccion);

  }



  $scope.cancelaMedioPago = function(modal){

    swal({
      title: 'Aviso',
      text: "¿Desea salir de este medio de pago ?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {
      $scope.mediopago = {};
      $scope.mediopagocombo.mediopagoSel = $filter('filter')($scope.mediospago, {codigo: $scope.mediospago.codigo}) [1];
      $scope.$apply();
      angular.element("#"+modal).modal("hide");
    });

  }

  $scope.consultarFacturas = function() {
    $scope.facturas = [];
    var successCallbackFacturas = function(data, responseHeaders){
      console.log('FACTURAS!!!');
      console.log(data);
      if( data.idControl === 0 ) {
        $scope.facturas = data.facturas;
      }
    }
    var facturaConsulta = {};
    facturaConsulta.corrCaja = $rootScope.usuario.caja;
    facturaConsulta.idCentroCosto = Number($rootScope.usuario.centroCostoActivo.codigo);
    FacturaResource.consultar(facturaConsulta, successCallbackFacturas, $scope.errorCallback);
  }

  $scope.facturar = function() {
    angular.element('#factura').modal('show');
    angular.element("#factura").on('shown.bs.modal', function () {
      $rootScope.$broadcast('onFacturar', [$rootScope.tramitante, $scope.facturas, $scope.transacionesSession]);
    });
  }

  $scope.$on('onFacturarTermina', function(event, facturas) {
    $scope.facturas = facturas;
    $scope.inicializarListado();

    /*for( t = 0; t < $scope.transacionesSession.length; t++ ) {
          $scope.transacionesSession[t].tipoDocumento = 'B';
      }

      if ( tipo === 'Tercero' && $scope.facturas.length > 0 ) {
          for( t = 0; t < $scope.transacionesSession.length; t++ ) {
              $scope.transacionesSession[t].tipoDocumento = 'F';
          }
      }else if ( tipo === 'Rut' && $scope.facturas.length > 0 ) {
          for( t = 0; t < $scope.transacionesSession.length; t++ ) {
              esFactura = false;
              for( f = 0; f < $scope.facturas.length; f++ ) {
                  if( $scope.transacionesSession[t].rutAfectado === $scope.facturas[f].rutAfectado ) {
                      esFactura = true;
                  }
              }
              $scope.transacionesSession[t].tipoDocumento = esFactura ? 'F' : 'B';
          }
      }*/

    $scope.$applyAsync();
  });

  $scope.agregarTransaccionFactura = function (factura, transac) {
  var transaccion = JSON.parse(JSON.stringify(transac));
  transaccion.costoServicio = transaccion.costoServicio;
    if( typeof factura.transacciones !== 'undefined' && !Array.isArray(factura.transacciones) ) {
      factura.transacciones = [];
    }

    if ( factura.rutAfectado !== '000000000-0' && factura.rutAfectado !== transaccion.rutAfectado ){
      return factura;
    }

    if ( factura.transacciones.length > 0 ) {
      for (var i=0; i < factura.transacciones.length; i++) {
        if ( factura.transacciones[i].codServicio == transaccion.codServicio ) {
          factura.transacciones[i].cantidadServicio += 1;
          return factura;
        }
      }
    }

    transaccion.cantidadServicio = 1;
    factura.transacciones.push(transaccion);
    return factura;
  }

  $scope.recalcularFactura = function (factura) {
      factura.montoBruto = 0;
      factura.iva = 0;
      factura.total = 0;
      var totalCosto = 0;
      if ( factura.transacciones.length > 0 ) {
        for (var i=0; i < factura.transacciones.length; i++) {
          totalCosto += factura.transacciones[i].cantidadServicio * factura.transacciones[i].costoServicio;
        }
      }

        factura.iva =   totalCosto - (totalCosto > 0 ? Math.round(totalCosto / (1 + $scope.iva / 100)) : 0);
        factura.montoBruto = totalCosto - factura.iva;
       
       factura.total = totalCosto;
      //factura.montoBruto = factura.montoBruto - factura.iva;

      return factura;
  };

  $scope.obtenerIVA = function () {
    var successCallbackIVA = function(data, responseHeaders){
      console.log('successCallbackIVA');
      console.log(data);
      if(data.idControl !== 0) {
        swal({ title: 'Error', text: 'No se puede obtener el dato IVA!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
        return;
      }
      $scope.iva = parseFloat(data.msgControl);
    }
    FacturaResource.obtenerIVA(successCallbackIVA, $scope.errorCallback);
  }

  $scope.imprimeBinario = function(reporte){
    console.log(reporte);

      var base64str = reporte;

      // decode base64 string, remove space for IE compatibility
      var binary = atob(base64str.replace(/\s/g, ''));
      
      var len = binary.length;
      var buffer = new ArrayBuffer(len);
      var view = new Uint8Array(buffer);
      for (var i = 0; i < len; i++) {
          view[i] = binary.charCodeAt(i);
      }

      var blob = new Blob( [view], { type: "application/pdf" });
      //saveAs(blob, "cuadraturaGeneral.pdf");
      var url = "http://localhost:8080/terminal/siac/imprimereporte";
  
      $http({
        method: 'POST',
        url: url,
        data: blob,
        timeout: 4000,
        headers : {
            'Content-Type' : 'application/json'
         }
      }).then(function(response) {
        console.log(unit, response);
      }, function(response) {
        console.log(response);
      });
     
  }
  
  $scope.imprimeTodos = function(){
	  
	  
	  //PagosResource.imprimeBoleta($scope.boletaPrint);
	    swal({
	        title: 'Aviso',
	        text: "Se imprimiran " + $scope.resumen.lista.length + " documentos. Use papel con logo.",
	        type: 'warning',
	        //showCancelButton: false,
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: 'Aceptar',
	        allowOutsideClick: false
	      }).then(function () {

	          angular.forEach($scope.resumen.lista, function (pdf, key) {
	        	var binary = null;
	      	  	if(pdf.flagImprime == 1){
		      	      var base64str = pdf.reporte;
	
		      	      // decode base64 string, remove space for IE compatibility
		      	      binary = atob(base64str.replace(/\s/g, ''));
		      	      
		      	      var len = binary.length;
		      	      var buffer = new ArrayBuffer(len);
		      	      var view = new Uint8Array(buffer);
		      	      for (var i = 0; i < len; i++) {
		      	          view[i] = binary.charCodeAt(i);
		      	      }
	
		      	      var blob = new Blob( [view], { type: "application/pdf" });
		      	      //saveAs(blob, "cuadraturaGeneral.pdf");

		      	      var url = "http://localhost:8080/terminal/siac/imprimereporte";
		      	  
		      	      $http({
		      	        method: 'POST',
		      	        url: url,
		      	        data: blob,
		      	        timeout: 4000,
		      	        headers : {
		      	            'Content-Type' : 'application/json'
		      	         }
		      	      }).then(function(response) {
		      	        console.log(response);
		      	      });	      	  		
	      	  	}
	      	  	
	      	  	
	      	  	
	          });

	      });
	    
	    

	  
  }
  
  $scope.files = [];
  
  $scope.imprimeTodosArreglo = function(){
	  
	$scope.certificadoList = [];
    $scope.certificadoDTO = {};
    $scope.certificadoDTO.archivopdf = []
    $scope.cantidadArchivos = $filter('filter')($scope.resumen.lista, { flagImprime: 1 });
    
    var listado = [];
	  //PagosResource.imprimeBoleta($scope.boletaPrint);
	    swal({
	        title: 'Aviso',
	        text: "Se imprimiran " + $scope.cantidadArchivos.length + " documentos,  Use papel con logo.",
	        type: 'warning',
	        showCancelButton: false,
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: 'Aceptar',
	        cancelButtonText: 'Cancelar',
	        allowOutsideClick: false
	      }).then(function () {
	    	  
	    	
	          angular.forEach($scope.resumen.lista, function (pdf, key) {
	        	var binary = null;
	      	  	if(pdf.flagImprime == 1){
		      	      var base64str = pdf.reporte;
	
		      	      // decode base64 string, remove space for IE compatibility
		      	      binary = atob(base64str.replace(/\s/g, ''));
		      	      
		      	      var len = binary.length;
		      	      var buffer = new ArrayBuffer(len);
		      	      var view = new Uint8Array(buffer);
		      	      for (var i = 0; i < len; i++) {
		      	          view[i] = binary.charCodeAt(i);
		      	      }
	
		      	      var blob = new Blob( [view], { type: "application/pdf" });

		      	      //saveAs(blob, "cuadraturaGeneral.pdf");

		      	    $scope.files.push(view);
              
		      	  //$scope.certificadoList.push($scope.certificadoDTO);
                var data = {
                  archivopdf: pdf.reporte,
                  nombre: key
                };

                $scope.certificadoList.push(data);
              
	      	  	}
	      	  	
	          });
	          
              var url = "http://localhost:8080/terminal/siac/imprimereportearreglo";



              

              $http({
                method: 'POST',
                url: url,
                data:  $scope.certificadoList,
                timeout: 4000,
                headers : {
                    'Content-Type' : 'application/json'
                 }
              }).then(function(response) {
                console.log(response);
              });  


	      }, function(dismiss) {
	        return false;
	      });
	    
	    

	  
  }
  
	$scope.noCeros = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.datospago.numerocheque = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Número Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}

	$scope.noCerosAutorizacion = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.datospago.autorizacion = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Código de Autorización Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}

	$scope.noCerosOperacion = function(valor, inputId){
        if((valor.length > 0 && valor === "") || angular.isUndefined(valor) || parseInt(valor, 12) === 0){
        	$scope.datospago.operacion = "";
        	$("#"+inputId).focus();
        	swal({ title: 'Error', text: "Número de Operación Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	
        }
	}
	

	$scope.$emit('logout');
	
	$scope.obtenerIVA();
	$scope.consultarFacturas();
	$scope.cargaBancosPago();
	$scope.listarServicioTramitantePago();
	$scope.consultaBoleta();
	$scope.cargaMedios();

});
