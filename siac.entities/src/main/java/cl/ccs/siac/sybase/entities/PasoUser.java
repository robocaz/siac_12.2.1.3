package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the paso_user database table.
 * 
 */
@Entity
@Table(name="paso_user")
@NamedQuery(name="PasoUser.findAll", query="SELECT p FROM PasoUser p")
public class PasoUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;

	private byte[] password;

	public PasoUser() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getPassword() {
		return this.password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

}