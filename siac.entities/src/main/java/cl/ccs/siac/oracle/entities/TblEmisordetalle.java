package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_EMISORDETALLE database table.
 * 
 */
@Entity
@Table(name="TBL_EMISORDETALLE")
@NamedQuery(name="TblEmisordetalle.findAll", query="SELECT t FROM TblEmisordetalle t")
public class TblEmisordetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_EMISORDETALLE_FLDCORREMIDETID_GENERATOR", sequenceName="FLD_CORREMIDET_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_EMISORDETALLE_FLDCORREMIDETID_GENERATOR")
	@Column(name="FLD_CORREMIDET_ID")
	private long fldCorremidetId;

	@Column(name="FLD_CARGO")
	private String fldCargo;

	@Column(name="FLD_CODCOMUNA")
	private BigDecimal fldCodcomuna;

	@Column(name="FLD_CODUSUARIOACT")
	private String fldCodusuarioact;

	@Column(name="FLD_DIRECCION")
	private String fldDireccion;

	@Column(name="FLD_EMAIL")
	private String fldEmail;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECACTUALIZACION")
	private Date fldFecactualizacion;

	@Column(name="FLD_NOMBRECONTACTO")
	private String fldNombrecontacto;

	@Column(name="FLD_PROGFORMATOARCHIVO")
	private BigDecimal fldProgformatoarchivo;

	@Column(name="FLD_PROGGRUPOEXTRACCION")
	private BigDecimal fldProggrupoextraccion;

	@Column(name="FLD_PROGMODOENVIO")
	private String fldProgmodoenvio;

	@Column(name="FLD_TELEFONO")
	private String fldTelefono;

	@Column(name="FLD_TELEFONOFAX")
	private String fldTelefonofax;

	@Column(name="FLD_TOLDIFERRORES")
	private double fldToldiferrores;

	@Column(name="FLD_TOLDIFMONTO")
	private double fldToldifmonto;

	@Column(name="FLD_TOLDIFREGISTROS")
	private double fldToldifregistros;

	@Column(name="FLD_TOLMARCAAVALES")
	private BigDecimal fldTolmarcaavales;

	@Column(name="FLD_TOLMARCADETALLE")
	private BigDecimal fldTolmarcadetalle;

	@Column(name="FLD_TOLVALANOMAXIMO")
	private BigDecimal fldTolvalanomaximo;

	@Column(name="FLD_TOLVALDIASMINIMO")
	private BigDecimal fldTolvaldiasminimo;

	@Column(name="FLD_TOLVALMONTOMAXIMO")
	private BigDecimal fldTolvalmontomaximo;

	@Column(name="FLD_TOLVALMONTOMINIMO")
	private BigDecimal fldTolvalmontominimo;

	@Column(name="FLD_TOLVALNOMBRE")
	private BigDecimal fldTolvalnombre;

	//bi-directional many-to-one association to TblEmisor
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FLD_CODEMISOR", referencedColumnName="FLD_CODEMISOR"),
		@JoinColumn(name="FLD_TIPOEMISOR", referencedColumnName="FLD_TIPOEMISOR")
		})
	private TblEmisor tblEmisor;

	public TblEmisordetalle() {
	}

	public long getFldCorremidetId() {
		return this.fldCorremidetId;
	}

	public void setFldCorremidetId(long fldCorremidetId) {
		this.fldCorremidetId = fldCorremidetId;
	}

	public String getFldCargo() {
		return this.fldCargo;
	}

	public void setFldCargo(String fldCargo) {
		this.fldCargo = fldCargo;
	}

	public BigDecimal getFldCodcomuna() {
		return this.fldCodcomuna;
	}

	public void setFldCodcomuna(BigDecimal fldCodcomuna) {
		this.fldCodcomuna = fldCodcomuna;
	}

	public String getFldCodusuarioact() {
		return this.fldCodusuarioact;
	}

	public void setFldCodusuarioact(String fldCodusuarioact) {
		this.fldCodusuarioact = fldCodusuarioact;
	}

	public String getFldDireccion() {
		return this.fldDireccion;
	}

	public void setFldDireccion(String fldDireccion) {
		this.fldDireccion = fldDireccion;
	}

	public String getFldEmail() {
		return this.fldEmail;
	}

	public void setFldEmail(String fldEmail) {
		this.fldEmail = fldEmail;
	}

	public Date getFldFecactualizacion() {
		return this.fldFecactualizacion;
	}

	public void setFldFecactualizacion(Date fldFecactualizacion) {
		this.fldFecactualizacion = fldFecactualizacion;
	}

	public String getFldNombrecontacto() {
		return this.fldNombrecontacto;
	}

	public void setFldNombrecontacto(String fldNombrecontacto) {
		this.fldNombrecontacto = fldNombrecontacto;
	}

	public BigDecimal getFldProgformatoarchivo() {
		return this.fldProgformatoarchivo;
	}

	public void setFldProgformatoarchivo(BigDecimal fldProgformatoarchivo) {
		this.fldProgformatoarchivo = fldProgformatoarchivo;
	}

	public BigDecimal getFldProggrupoextraccion() {
		return this.fldProggrupoextraccion;
	}

	public void setFldProggrupoextraccion(BigDecimal fldProggrupoextraccion) {
		this.fldProggrupoextraccion = fldProggrupoextraccion;
	}

	public String getFldProgmodoenvio() {
		return this.fldProgmodoenvio;
	}

	public void setFldProgmodoenvio(String fldProgmodoenvio) {
		this.fldProgmodoenvio = fldProgmodoenvio;
	}

	public String getFldTelefono() {
		return this.fldTelefono;
	}

	public void setFldTelefono(String fldTelefono) {
		this.fldTelefono = fldTelefono;
	}

	public String getFldTelefonofax() {
		return this.fldTelefonofax;
	}

	public void setFldTelefonofax(String fldTelefonofax) {
		this.fldTelefonofax = fldTelefonofax;
	}

	public double getFldToldiferrores() {
		return this.fldToldiferrores;
	}

	public void setFldToldiferrores(double fldToldiferrores) {
		this.fldToldiferrores = fldToldiferrores;
	}

	public double getFldToldifmonto() {
		return this.fldToldifmonto;
	}

	public void setFldToldifmonto(double fldToldifmonto) {
		this.fldToldifmonto = fldToldifmonto;
	}

	public double getFldToldifregistros() {
		return this.fldToldifregistros;
	}

	public void setFldToldifregistros(double fldToldifregistros) {
		this.fldToldifregistros = fldToldifregistros;
	}

	public BigDecimal getFldTolmarcaavales() {
		return this.fldTolmarcaavales;
	}

	public void setFldTolmarcaavales(BigDecimal fldTolmarcaavales) {
		this.fldTolmarcaavales = fldTolmarcaavales;
	}

	public BigDecimal getFldTolmarcadetalle() {
		return this.fldTolmarcadetalle;
	}

	public void setFldTolmarcadetalle(BigDecimal fldTolmarcadetalle) {
		this.fldTolmarcadetalle = fldTolmarcadetalle;
	}

	public BigDecimal getFldTolvalanomaximo() {
		return this.fldTolvalanomaximo;
	}

	public void setFldTolvalanomaximo(BigDecimal fldTolvalanomaximo) {
		this.fldTolvalanomaximo = fldTolvalanomaximo;
	}

	public BigDecimal getFldTolvaldiasminimo() {
		return this.fldTolvaldiasminimo;
	}

	public void setFldTolvaldiasminimo(BigDecimal fldTolvaldiasminimo) {
		this.fldTolvaldiasminimo = fldTolvaldiasminimo;
	}

	public BigDecimal getFldTolvalmontomaximo() {
		return this.fldTolvalmontomaximo;
	}

	public void setFldTolvalmontomaximo(BigDecimal fldTolvalmontomaximo) {
		this.fldTolvalmontomaximo = fldTolvalmontomaximo;
	}

	public BigDecimal getFldTolvalmontominimo() {
		return this.fldTolvalmontominimo;
	}

	public void setFldTolvalmontominimo(BigDecimal fldTolvalmontominimo) {
		this.fldTolvalmontominimo = fldTolvalmontominimo;
	}

	public BigDecimal getFldTolvalnombre() {
		return this.fldTolvalnombre;
	}

	public void setFldTolvalnombre(BigDecimal fldTolvalnombre) {
		this.fldTolvalnombre = fldTolvalnombre;
	}

	public TblEmisor getTblEmisor() {
		return this.tblEmisor;
	}

	public void setTblEmisor(TblEmisor tblEmisor) {
		this.tblEmisor = tblEmisor;
	}

}