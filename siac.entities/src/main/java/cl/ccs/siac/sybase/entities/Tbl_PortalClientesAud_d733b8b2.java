package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PortalClientesAud_d733b8b2 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PortalClientesAud_d733b8b2.findAll", query="SELECT t FROM Tbl_PortalClientesAud_d733b8b2 t")
public class Tbl_PortalClientesAud_d733b8b2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CausalBlqAnulacion")
	private String fld_CausalBlqAnulacion;

	@Column(name="Fld_CCosto")
	private short fld_CCosto;

	@Column(name="Fld_Ciudad")
	private String fld_Ciudad;

	@Column(name="Fld_Clave")
	private String fld_Clave;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodPregunta")
	private byte fld_CodPregunta;

	@Column(name="Fld_CorrCliente")
	private BigDecimal fld_CorrCliente;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_EstadoCivil")
	private byte fld_EstadoCivil;

	@Column(name="Fld_FecCambioClave")
	private Timestamp fld_FecCambioClave;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecGenClave")
	private Timestamp fld_FecGenClave;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecInscripcion")
	private Timestamp fld_FecInscripcion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecNacimiento")
	private Timestamp fld_FecNacimiento;

	@Column(name="Fld_FecRecuperacionClave")
	private Timestamp fld_FecRecuperacionClave;

	@Column(name="Fld_FonoFijo")
	private String fld_FonoFijo;

	@Column(name="Fld_FonoMovil")
	private String fld_FonoMovil;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_IP")
	private String fld_IP;

	@Column(name="Fld_NivelEducacional")
	private byte fld_NivelEducacional;

	@Column(name="Fld_Respuesta")
	private String fld_Respuesta;

	@Column(name="Fld_RutCliente")
	private String fld_RutCliente;

	@Column(name="Fld_Sexo")
	private String fld_Sexo;

	@Column(name="Fld_TipoInscripcion")
	private byte fld_TipoInscripcion;

	@Column(name="Fld_UsuarioGenClave")
	private String fld_UsuarioGenClave;

	@Column(name="Fld_UsuarioInscripcion")
	private String fld_UsuarioInscripcion;

	@Column(name="Fld_UsuarioModificacion")
	private String fld_UsuarioModificacion;

	@Column(name="Fld_UsuarioRecuperacionClave")
	private String fld_UsuarioRecuperacionClave;

	public Tbl_PortalClientesAud_d733b8b2() {
	}

	public String getFld_CausalBlqAnulacion() {
		return this.fld_CausalBlqAnulacion;
	}

	public void setFld_CausalBlqAnulacion(String fld_CausalBlqAnulacion) {
		this.fld_CausalBlqAnulacion = fld_CausalBlqAnulacion;
	}

	public short getFld_CCosto() {
		return this.fld_CCosto;
	}

	public void setFld_CCosto(short fld_CCosto) {
		this.fld_CCosto = fld_CCosto;
	}

	public String getFld_Ciudad() {
		return this.fld_Ciudad;
	}

	public void setFld_Ciudad(String fld_Ciudad) {
		this.fld_Ciudad = fld_Ciudad;
	}

	public String getFld_Clave() {
		return this.fld_Clave;
	}

	public void setFld_Clave(String fld_Clave) {
		this.fld_Clave = fld_Clave;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodPregunta() {
		return this.fld_CodPregunta;
	}

	public void setFld_CodPregunta(byte fld_CodPregunta) {
		this.fld_CodPregunta = fld_CodPregunta;
	}

	public BigDecimal getFld_CorrCliente() {
		return this.fld_CorrCliente;
	}

	public void setFld_CorrCliente(BigDecimal fld_CorrCliente) {
		this.fld_CorrCliente = fld_CorrCliente;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public byte getFld_EstadoCivil() {
		return this.fld_EstadoCivil;
	}

	public void setFld_EstadoCivil(byte fld_EstadoCivil) {
		this.fld_EstadoCivil = fld_EstadoCivil;
	}

	public Timestamp getFld_FecCambioClave() {
		return this.fld_FecCambioClave;
	}

	public void setFld_FecCambioClave(Timestamp fld_FecCambioClave) {
		this.fld_FecCambioClave = fld_FecCambioClave;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecGenClave() {
		return this.fld_FecGenClave;
	}

	public void setFld_FecGenClave(Timestamp fld_FecGenClave) {
		this.fld_FecGenClave = fld_FecGenClave;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecInscripcion() {
		return this.fld_FecInscripcion;
	}

	public void setFld_FecInscripcion(Timestamp fld_FecInscripcion) {
		this.fld_FecInscripcion = fld_FecInscripcion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecNacimiento() {
		return this.fld_FecNacimiento;
	}

	public void setFld_FecNacimiento(Timestamp fld_FecNacimiento) {
		this.fld_FecNacimiento = fld_FecNacimiento;
	}

	public Timestamp getFld_FecRecuperacionClave() {
		return this.fld_FecRecuperacionClave;
	}

	public void setFld_FecRecuperacionClave(Timestamp fld_FecRecuperacionClave) {
		this.fld_FecRecuperacionClave = fld_FecRecuperacionClave;
	}

	public String getFld_FonoFijo() {
		return this.fld_FonoFijo;
	}

	public void setFld_FonoFijo(String fld_FonoFijo) {
		this.fld_FonoFijo = fld_FonoFijo;
	}

	public String getFld_FonoMovil() {
		return this.fld_FonoMovil;
	}

	public void setFld_FonoMovil(String fld_FonoMovil) {
		this.fld_FonoMovil = fld_FonoMovil;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public String getFld_IP() {
		return this.fld_IP;
	}

	public void setFld_IP(String fld_IP) {
		this.fld_IP = fld_IP;
	}

	public byte getFld_NivelEducacional() {
		return this.fld_NivelEducacional;
	}

	public void setFld_NivelEducacional(byte fld_NivelEducacional) {
		this.fld_NivelEducacional = fld_NivelEducacional;
	}

	public String getFld_Respuesta() {
		return this.fld_Respuesta;
	}

	public void setFld_Respuesta(String fld_Respuesta) {
		this.fld_Respuesta = fld_Respuesta;
	}

	public String getFld_RutCliente() {
		return this.fld_RutCliente;
	}

	public void setFld_RutCliente(String fld_RutCliente) {
		this.fld_RutCliente = fld_RutCliente;
	}

	public String getFld_Sexo() {
		return this.fld_Sexo;
	}

	public void setFld_Sexo(String fld_Sexo) {
		this.fld_Sexo = fld_Sexo;
	}

	public byte getFld_TipoInscripcion() {
		return this.fld_TipoInscripcion;
	}

	public void setFld_TipoInscripcion(byte fld_TipoInscripcion) {
		this.fld_TipoInscripcion = fld_TipoInscripcion;
	}

	public String getFld_UsuarioGenClave() {
		return this.fld_UsuarioGenClave;
	}

	public void setFld_UsuarioGenClave(String fld_UsuarioGenClave) {
		this.fld_UsuarioGenClave = fld_UsuarioGenClave;
	}

	public String getFld_UsuarioInscripcion() {
		return this.fld_UsuarioInscripcion;
	}

	public void setFld_UsuarioInscripcion(String fld_UsuarioInscripcion) {
		this.fld_UsuarioInscripcion = fld_UsuarioInscripcion;
	}

	public String getFld_UsuarioModificacion() {
		return this.fld_UsuarioModificacion;
	}

	public void setFld_UsuarioModificacion(String fld_UsuarioModificacion) {
		this.fld_UsuarioModificacion = fld_UsuarioModificacion;
	}

	public String getFld_UsuarioRecuperacionClave() {
		return this.fld_UsuarioRecuperacionClave;
	}

	public void setFld_UsuarioRecuperacionClave(String fld_UsuarioRecuperacionClave) {
		this.fld_UsuarioRecuperacionClave = fld_UsuarioRecuperacionClave;
	}

}