angular.module('ccs').controller('InicioController', function(TransaccionResource,ImpresoraResource, InicioResource, BoletaResource, $rootScope, $scope, $interval, LoginResource, TramitanteResource, CajaResource,
 $location, $uibModal, $document, $localStorage,$http,$q, $window) {

    /* Variable global */
    $rootScope.usuario = {};
    $rootScope.tramitante = {};
    $rootScope.transacciones = [];
    $rootScope.montoCobra = 0;
    $rootScope.enableBoleta = false;
    $localStorage.impresoraSelected = {};
    $rootScope.nombreMaquina = "";
    $rootScope.timeout = false;
    $rootScope.configuracion = {};
    $scope.rec = {isImprime: false, isFactura: false}
    $rootScope.pendienteFlag = "";
    $scope.fechaActual = new Date();
    
    if($rootScope.disableTram === undefined || !$rootScope.disableTram){
        $rootScope.disableTram = false;
    }


    $scope.cargaConfiguracion = function(){
        
        var successCallback = function(data,responseHeaders){
            if(data.idControl == 0){
            	//swal('Alerta','Este terminal ya se encuentra configurado.','success');
                $rootScope.configuracion = data;
                $localStorage.checkBoleta = $rootScope.configuracion.emiteBoleta;    
                $scope.rec.isImprime = $rootScope.configuracion.emiteBoleta;
                $scope.emiteBoleta = $rootScope.configuracion.emiteBoleta;
                $scope.rec.isFactura = $rootScope.configuracion.emiteFactura;
                $scope.emiteFactura = $rootScope.configuracion.emiteFactura;
                $scope.imprimeBoletas();
                console.log($rootScope.configuracion);
                swal({
                  title: 'Atención',
                  text: "Este terminal ya se encuentra configurado",
                  type: 'success',
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Continuar',
                  allowOutsideClick: false
                }).then(function () {
                    $scope.transaccionesPendienteUsuario();
                })
               
            }
            else if(data.idControl == 1){
    			swal({
  				  title: 'Atención',
  				  text: "Este terminal debe ser configurado",
  				  type: 'error',
  				  confirmButtonColor: '#3085d6',
  				  cancelButtonColor: '#d33',
  				  confirmButtonText: 'Continuar',
  				  allowOutsideClick: false
  				}).then(function () {
  					if(!$scope.esrutjuridico){
  						angular.element("#configuraciones").modal();
  					}
  				})
        		
        		//$scope.$digest();
            }
         };
        
        InicioResource.cargaConfig(successCallback, $scope.errorCallback);
    }
    
    
	$scope.aceptarConfiguracion = function(){
		console.log($scope.configuracion);
		
		var successCallback = function(data,responseHeaders){
			console.log(data);
            $localStorage.checkBoleta = data.emiteBoleta;    
            $scope.rec.isImprime = data.emiteBoleta;
            $scope.emiteBoleta = data.emiteBoleta;
            $scope.rec.isFactura = data.emiteFactura;
            $scope.emiteFactura = data.emiteFactura;
            angular.element("#configuraciones").modal("hide");
			swal({
				  title: 'Operación Exitosa',
				  text: "Actualización realizada exitosamente",
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Continuar'
				}).then(function () {
		            $rootScope.$emit('iniciarBoleta', data);
		            $scope.configuracion = {};
			});
			


		};
		
		InicioResource.configInicial($scope.configuracion, successCallback, $scope.errorCallback);
		
	}
	
    
   

   $scope.imprimeBoletas = function(){
       $scope.emiteBoleta = $scope.rec.isImprime;
   }
   
   
   $scope.actulizaEmiteBoleta = function(){
	   InicioResource.emiteBoleta({emiteboleta: $scope.emiteBoleta})
   }

  $scope.imprimeFacturas = function(){
       $scope.emiteFactura = $scope.rec.isFactura;
   }

  $scope.actulizaEmiteFactura = function(){
	   InicioResource.emiteFactura({emitefactura: $scope.emiteFactura})
  }


    
    /* Manejos de errores */

    $scope.errorCallback = function(response) {
        console.log("Capturar Error");
        if(response.status == 401){
            $('.modal-backdrop').remove();
            $location.url('/login')
        }else{
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
            console.log($scope.error);
        }
    };

    /* Inicio - datos del centro de costo */
    $scope.centroCosto = {
        model : null,
        lista : []
    };

    $scope.ingresarCentroCosto = function() {

        $scope.centroCosto.model = $rootScope.usuario.centroCostoActivo;
        angular.element('#centrocosto').modal();
    };

    $scope.setCentroCosto = function(newCentroCosto){
        console.log("se modifica");
        $rootScope.usuario.centroCostoActivo = newCentroCosto;
    }
    /* Fin  - datos del centro de costo */

    /* Inicio - Revisión Caja */
    $scope.cantidadesabiertas = function() {
        if(!$rootScope.disableTram){
            if($rootScope.usuario.caja == 0 || $rootScope.usuario.caja == null){
                var successCallback = function(data){
                    if(data.cajasAbiertas == 0){
                        $rootScope.usuario.caja = data.corrCaja;
                        $location.path("/atencion");
                    }else{
                        $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
                        $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
    
                        angular.element('#modalaperturacaja').modal();
                    }
                };
                CajaResource.cantidadabiertas( successCallback, $scope.errorCallback);
            }else {
                $location.path("/atencion");
            }
        }
    };

    
    $scope.buscapendientes = function(){
        
		$scope.$emit('logout');

        var successCallback = function(data){
            if(data.corrCaja > 0){
                $rootScope.usuario.caja = data.corrCaja;
                $scope.tramitante.rut = data.rutTramitante;
                $location.path("/atencion");
                $scope.listarServicioTramitante();
            }
            else{
                $scope.cantidadesabiertas();
            }
        };
        
        
        CajaResource.cajapendiente(successCallback, $scope.errorCallback);
    }
    
    $scope.listar = function(){
        var successCallback = function(data){
            $scope.listaCajasAbiertas = data;
        };
        CajaResource.cajasabiertas( successCallback, $scope.errorCallback);
    }
    
    
    $scope.confirmarcambiarcaja = function(){
        // si tiene transacciones pendientes
        if($rootScope.transacciones.length==0){
            angular.element('#confirmarcambiarcaja').modal();
        }else{
            angular.element('#transaccionespendientescaja').modal();
        }
    }
    /* Fin - Revisión Caja */


    /* Inicio Salir */
    $scope.salirCarroCompra = function(){
        // si tiene transacciones pendientes
    	$scope.cantidadesabiertasActualiza();
        if($rootScope.transacciones.length==0){
            angular.element('#confirmarsalircarrocompra').modal();
        }else{
            angular.element('#transaccionespendientescaja').modal();
        }
    }
    /* Fin Salir */
    $scope.cantidadesabiertasActualiza = function() {
        var successCallback = function(data){

                $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
                $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
        };
        CajaResource.cantidadabiertas( successCallback, $scope.errorCallback);
      
    };
    $scope.get = function() {
    	console.log("--valido sesion--")
        var successCallback = function(data){
        	data.esAgencia = Number(data.centroCostoActivo.codigo) > 2300 && Number(data.centroCostoActivo.codigo) < 3000;
            $rootScope.usuario = data;
            $scope.getTramitanteSession();
        };
        LoginResource.status({}, successCallback, $scope.errorCallback);
    };

    $scope.get();

    $scope.getTramitanteSession = function() {
        var successCallback = function(data){
            $rootScope.tramitante = data;
            $scope.$digest();
        };
        TramitanteResource.obtenerPorCaja($rootScope.usuario.caja, successCallback, $scope.errorCallback);
    };

    $scope.setUsuario = function(newUsuario){
        $rootScope.usuario = newUsuario;
    }

    $scope.blockBack = function(valor){
        $rootScope.blockBack = valor;
    }


    $scope.setTramitante = function(newTramitante){
        $rootScope.tramitante = newTramitante;
    }

    $scope.ingresar = function() {

        var successCallback = function(data,responseHeaders){

            if(data.idControl == 0){
            	data.esAgencia = Number(data.centroCostoActivo.codigo) > 2300 && Number(data.centroCostoActivo.codigo) < 3000;
                $rootScope.usuario = data;
                $rootScope.ip = $rootScope.usuario.ipLocal;
                localStorage.clear();
                
                $location.path("/inicio");
            }else if (data.idControl == 1){
                $scope.usuario.msgControl = "Usuario no Registrado";
            }else if (data.idControl == 2){
                $scope.usuario.msgControl = "Error al Buscar Nombre del Usuario";
            }else if (data.idControl == 3){
                $scope.usuario.msgControl = "Usuario no tiene Centro de Costo Activo, consulte con administrador";
            }else if (data.idControl == 4){
                $scope.usuario.msgControl = "Usuario no Registrado";
            }else if (data.idControl == 5){
                $scope.usuario.msgControl = "Nombre de usuario y/o password no son válidos";
            }
        };
        var errorCallback2 = function(response) {
            $scope.usuario.msgControl = "Servicio de Aplicación no se encuentra disponible";
        };
        LoginResource.login($scope.login, successCallback, errorCallback2);
    };

    $scope.logout = function() {

        var successCallback = function(data,responseHeaders){

            if(data.idControl == 0){
                $location.path("/inicio");
                localStorage.clear();
                $rootScope.disableTram = false;
            }else if (data.idControl == 1){
                $scope.usuario.msgControl = "Usuario no Registrado";
            }else if (data.idControl == 2){
                $scope.usuario.msgControl = "Error al Buscar Nombre del Usuario";
            }else if (data.idControl == 3){
                $scope.usuario.msgControl = "Usuario no tiene Centro de Costo Activo, consulte con administrador";
            }else if (data.idControl == 4){
                $scope.usuario.msgControl = "Usuario no Registrado";
            }else if (data.idControl == 5){
                $scope.usuario.msgControl = "Nombre de usuario y/o password no son válidos";
            }
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        LoginResource.logout($scope.login, successCallback, errorCallback2);
    };

    $scope.getClass = function (path) {
      return ($location.path().substr(0, path.length) === path) ? 'active' : '';
    }

    $scope.listarServicioTramitante = function() {
        console.log("listarServicioTramitante");
        var successCallback = function(data,responseHeaders){
            console.log("listarServicioTramitante"+data);
            
            $rootScope.transacciones = data;
            if(data.length > 0){
            	
            	if(!$rootScope.pendienteFlag && $rootScope.pendienteFlag !== ""){
                    $rootScope.pendienteFlag = true;
                    swal({
                        title: 'Atención',
                        text: "Tiene transacciones pendientes",
                        type: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Continuar',
                        allowOutsideClick: false
                      }).then(function () {
                    	 
                      })
            	}
            }
        };
        TransaccionResource.listarservicio( successCallback, $scope.errorCallback);
    };

    
    $scope.transaccionesPendienteUsuario = function() {
        console.log("transaccionesPendienteUsuario");
        var successCallback = function(dataCaja,responseHeaders){
            
        	if(dataCaja.corrCaja !== null){

                $rootScope.usuario.caja = dataCaja.corrCaja;
                $scope.tramitante.rut = dataCaja.rutTramitante;
                //$location.path("/atencion");
                
                var successCallbackTrans = function(data,responseHeaders){
					
		            $rootScope.transacciones = data;
		            if(data.length > 0){
		            	
		            	if(!$rootScope.pendienteFlag && $rootScope.pendienteFlag === ""){
		                    $rootScope.pendienteFlag = true;
		                    swal({
		                        title: 'Atención',
		                        text: "Tiene transacciones pendientes",
		                        type: 'warning',
		                        confirmButtonColor: '#3085d6',
		                        cancelButtonColor: '#d33',
		                        confirmButtonText: 'Continuar',
		                        allowOutsideClick: false
		                      }).then(function () {
		                    	  var pId = $location.path().split("/")[1];
                                  if(pId === "inicio"){
		                    		  $location.path("/atencion");
			                    	  $scope.$apply();  
		                    	  }
		                    	  
		                      })
		            	}
	            }
	           
              }
              TransaccionResource.listarservicio(successCallbackTrans, $scope.errorCallback);
        }

        };
        CajaResource.cajapendiente(successCallback, $scope.errorCallback);
        //TransaccionResource.listarservicio( successCallback, $scope.errorCallback);
    };    
    $scope.getServiciosTramitante = function(){
        return $rootScope.transacciones;
    }

    $scope.alerta = function(texto, parent){
        $uibModal.open({
            templateUrl: 'views/modal/modalalerta.html',
            appendTo: parent? angular.element($document[0].querySelector("#modalaperturacaja")): undefined,
            size: 'sm',
            controller: function($scope, $uibModalInstance) {
                $scope.descripcion = "Debe seleccionar una opción";
                $scope.cerrar = function(){
                    $uibModalInstance.close();
                }
            }
        });
    }

    $rootScope.$on("NombreMaquina", function(){
          $scope.obtieneNombreMaquina();
    });
    
    $scope.obtieneNombreMaquina = function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        var promises = [];
        $scope.respuesta = InicioResource.nombreMaquina();
        $scope.respuesta.$promise.then(function(data) {
             console.log(data.nombreEquipo);
             deferred.resolve(data);
       });
       promises.push(deferred.promise);
        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function(results) {
                
              console.log(results[0].nombreEquipo);
              $rootScope.nombreMaquina = results[0].nombreEquipo;
              $scope.setNombreMaquina(results[0].nombreEquipo);
              InicioResource.maquinaSesion({nombremaquina: results[0].nombreEquipo});
             
              
            },
            // error
            function(response) {
                console.log(response);
            }
        );
        
    };
    
    $scope.obtieneNombreMaquina();
    //$scope.cargaConfiguracion();
    
    $scope.setNombreMaquina = function(nombreMaquina){
        $rootScope.nombreMaquina = nombreMaquina;
    }
    
    $scope.getNombreMaquina = function(){
        return $rootScope.nombreMaquina;
    }
    
    $scope.$on('$locationChangeStart', function(evnt, next, current){            
        
       if(!$rootScope.timeout){
            var a = next.indexOf("login");
            if(a  != -1 && ($rootScope.blockBack !== undefined &&  $rootScope.blockBack == 1)){
                console.log(a);
                //Prevent browser's back button default action.
                evnt.preventDefault();            
    
            }
    
            console.log(next.indexOf("inicio"));
            console.log(current.indexOf("atencion"));
    
            if(next.indexOf("inicio") != -1  && current.indexOf("atencion") != -1){
                $rootScope.disableTram = false;
            }
            
       }

    });


/**

    $scope.cerrarsesion = function(){
        var successCallbackLogOut = function(data){
            $('.modal-backdrop').remove();
            $location.url('/login')
        };
        LoginResource.logout(successCallbackLogOut, $scope.errorCallback);
    };


    $scope.onExit = function() {
        
        $rootScope.tramitante = {};
        $scope.tramitante = {};
        $rootScope.tramitante = {};
        $rootScope.disableTram = false;
        $rootScope.rutDisbale = false;
        $localStorage.generadas = undefined;
        $localStorage.montoingrado = undefined;
        $rootScope.transacciones = [];
        $rootScope.timeout = false;
        $scope.StopTimer();
        rutSearch = '';          
        $scope.cerrarsesion()
    };

    $window.onbeforeunload =  $scope.onExit;

**/


    $rootScope.Timer = null;

    //Timer start function.
    $scope.StartTimer = function () {
        //Set the Timer start message.
        //Initialize the Timer to run every 1000 milliseconds i.e. one second.
        $rootScope.Timer = $interval(function () {
            $scope.validaSesion();
        }, 1860000);
    };

    //Timer stop function.
    $scope.StopTimer = function () {

        //Cancel the Timer.
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };
    
    
    $scope.validaTransaccionesAbiertas = function(){
        if($rootScope.transacciones.length > 0){
            swal('Alerta','Para cerrar sesión debe finiquitar transacciones pendientes.','error');
            return false;
        }
        else{
            $rootScope.tramitante = {};
            $scope.tramitante = {};
            $rootScope.tramitante = {};
            $rootScope.disableTram = false;
            $rootScope.rutDisbale = false;
            $localStorage.generadas = undefined;
            $localStorage.montoingrado = undefined;
            $rootScope.transacciones = [];
            $scope.centros = [];
            $scope.centroCosto = {};
            $rootScope.timeout = false;
            $scope.StopTimer();
            rutSearch = '';         
            angular.element("#confirmarcierrecajasesion").modal();
        }
    }
    
    
    $scope.validaSesion = function(){
    	console.log("validacionSesion")
        var successCallback = function(data,responseHeaders){
           if(data.idControl == 1){
               swal('Alerta','Su sessión a expirado, ingrese nuevamente.','error');
               $rootScope.tramitante = {};
               $scope.tramitante = {};
               $rootScope.tramitante = {};
               $rootScope.disableTram = false;
               $rootScope.rutDisbale = false;
               $localStorage.generadas = undefined;
               $localStorage.montoingrado = undefined;
               $rootScope.transacciones = [];
               $rootScope.timeout = true;
               $scope.StopTimer();
               rutSearch = ''; 
               $('.modal-backdrop').remove();
               $location.path("/login");
           }
            
        };
        LoginResource.validaSesion(successCallback, $scope.errorCallback);
        
    };
    
    $scope.$on('logout', function(event, args){
    	$scope.validaSesion();
    })
    
    //agregar validacion y emit
    
    $scope.abreCuadratura = function(){
    	angular.element("#cuadraturaindex").modal();
    }

	$scope.abreConsultaProtesto = function(){
		angular.element("#busquedaprotestos").modal();
	}

	
    //$scope.listarServicioTramitante();
});
