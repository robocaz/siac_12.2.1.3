angular.module('ccs').controller('BoletasController', function($scope, BoletaResource,LoginResource, $rootScope, $location, $filter, 
    $timeout, $uibModal, $document, $http, $localStorage, $templateCache) {

    $scope.boleta = {};
    $scope.boleta.inicioBoleta = "";
    $scope.boleta.finBoleta = "";
   
    $rootScope.usuario;
    $scope.boleta.nombreMaquina = $localStorage.nombreMaquina;
    $scope.boleta.ip = $rootScope.ip;
    $scope.obtieneNombreMaquina();
    console.log($rootScope.nombreMaquina);
    $scope.boleta.nombreMaquina = $rootScope.nombreMaquina;
    $scope.consultaBoleta = function(page) {

        
        $scope.boleta.idCentroCosto = $rootScope.usuario.centroCostoActivo.codigo;
        var successCallback = function(data,responseHeaders){

            console.log(data);
            if(data.idControl == 0){
                
                if(data.inicioBoleta == 0 && data.actualBoleta == 0 && page == 0){
                    $scope.boleta = data;
                    angular.element('#rangoboletas').modal();
                }
                if(data.inicioBoleta > 0 && data.actualBoleta > 0 && page == 1){
                    $scope.boleta = data;
                    angular.element('#rangoboletasedicion').modal();
                }
            }
            else{
                $scope.boleta.msgControl = "Error al iniciar el rango de boleta."   
            }
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        
        BoletaResource.consultaBoleta($scope.boleta, successCallback, errorCallback2);
    };
    

    
	$rootScope.$on('iniciarBoleta', function(event, data) { 
		$scope.consultaBoleta(0);
	});
    
    $scope.aceptarSeleBoleta = function(){
        
        
        if($scope.boleta.inicioBoleta == "" && $scope.boleta.finBoleta == ""){
            $scope.boleta.msgControl = "Ingrese los datos solicitados.";
            return false;
        }       
        if($scope.boleta.inicioBoleta == null || $scope.boleta.inicioBoleta == "" || $scope.boleta.inicioBoleta == 0){
             $scope.boleta.msgControl = "Numero inicial de boleta debe ser mayor a 0";
             return false;
        }
        if(parseInt($scope.boleta.inicioBoleta) > ($scope.boleta.finBoleta)){
            $scope.boleta.msgControl = "Numero inicial debe ser igual o menor a Numero final";
            return false;
        }
        if($scope.boleta.finBoleta == null || $scope.boleta.finBoleta == "" || $scope.boleta.finBoleta == 0){
             $scope.boleta.msgControl = "Numero final de boleta debe ser mayor a 0";
             return false;
        }
        else{
            $scope.abrirModal();
        }
    }

    $scope.abrirModal = function(){
        var modalInstance = $uibModal.open({
          templateUrl: 'views/modal/modalloginsupervisor.html',
          controller: 'ModalLoginSupervisorController',
          appendTo: angular.element($document[0].querySelector('#rangoboletas')),
          size: 'sm'
        });
        modalInstance.result.then(function (respuesta) {
            if(respuesta){
                console.log(respuesta);
                $scope.inicializaBoleta();
            }
        });
    }

    $scope.inicializaBoleta = function() {

        
        $scope.boleta.idCentroCosto = $rootScope.usuario.centroCostoActivo.codigo;
        $scope.boleta.codUsuario = $rootScope.usuario.username;
        
        var successCallback = function(data,responseHeaders){

            console.log(data);
            if(data.idControl == 0){
                swal({
                      title: 'Ingreso Exitoso',
                      text: 'Rango creado exitosamente.',
                      animation: false
                })
                angular.element('#rangoboletas').modal("hide");
            }
            else{
                $scope.boleta.msgControl = "Error al iniciar el rango de boleta."   
            }
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        
        BoletaResource.inicializaBoleta($scope.boleta, successCallback, errorCallback2);
    };




    $scope.editaBoleta = function(){

        $scope.cargaBoleta(); 

        
    }

    var modalInstanceED = {};
    
    $scope.cargaBoleta = function(page) {

        
        $scope.boleta.idCentroCosto = $rootScope.usuario.centroCostoActivo.codigo;
        if($scope.boleta.nombreMaquina === undefined || $scope.boleta.nombreMaquina === null || $scope.boleta.nombreMaquina === ""){
        	swal('Alerta','No se han cargado los datos necesarios, para modificar el rango de boletas','error');
        	return false;
        }
        else{
	        var successCallback = function(data,responseHeaders){
	
	            if(data.idControl == 0){
	                
	                    //$templateCache.remove('views/modal/modalrangoboletaedicion.html');
	                    $scope.boleta = data;
	                        modalInstanceED = $uibModal.open({
	                        templateUrl: 'views/modal/modalrangoboletaedicion.html', // loads the template
	                        scope: $scope,
	                        backdrop: false,
	                        resolve: {
	                               boleta: function () {
	                                  return $scope.boleta;
	                               }
	                        }
	
	                        });//end of modal.open
	                        
	            }
	            else{
	                $scope.boleta.msgControl = "Error al iniciar el rango de boleta."   
	            }
	        };
	        var errorCallback2 = function(response) {
	            $scope.error = "Servicio de Aplicación no se encuentra disponible";
	        };
	        
	        BoletaResource.consultaBoleta($scope.boleta, successCallback, errorCallback2);
        }
    };

	$scope.openSecond = function() {
		
		$scope.boleta.msgControl = "";
		$scope.boleta.msgControl2 = "";
		$scope.boleta.msgControl3 = "";
		
		if ( isUndefinedNullEmpty($scope.boleta.inicioBoleta) ) {
			$scope.boleta.msgControl2 = "Debe completar todos los campos";
			$("#inicioNun").focus();
			return false;
		}
		if ( isUndefinedNullEmpty($scope.boleta.finBoleta) ) {
			$scope.boleta.msgControl3 = "Debe completar todos los campos";
			$("#fin").focus();
			return false;
		}
		if ( $scope.boleta.inicioBoleta == 0 ) {
			$scope.boleta.msgControl2 = "Número inicial de boleta debe ser mayor a 0";
			$("#inicioNun").focus();
			return false;
		}
		if ( $scope.boleta.finBoleta == 0 ) {
			$scope.boleta.msgControl3 = "Número final de boleta debe ser mayor a 0";
			$("#fin").focus();
			return false;
		}
		if ( parseInt($scope.boleta.inicioBoleta) > parseInt($scope.boleta.finBoleta) ) {
			$scope.boleta.msgControl2 = "Número inicial no puede ser mayor a número final";
			$("#inicioNun").focus();
			return false;
		}
		
		var modalInstanceSup = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			size: 'sm'
		});
		modalInstanceSup.result.then(function (respuesta) {
			if ( respuesta ) {
				console.log(respuesta);
				$scope.closeModal();
				$scope.inicializaBoleta();
				$scope.boleta.msgControl = "";
				$scope.boleta.msgControl2 = "";
				$scope.boleta.msgControl3 = "";
			}
		});
	}

    $scope.closeModal = function(){
    	$scope.boleta.msgControl = "";
		$scope.boleta.msgControl2 = "";
		$scope.boleta.msgControl3 = "";
       modalInstanceED.dismiss('cancel');
    }

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

});
