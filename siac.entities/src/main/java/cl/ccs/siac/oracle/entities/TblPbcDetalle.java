package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_PBC_DETALLE database table.
 * 
 */
@Entity
@Table(name="TBL_PBC_DETALLE")
@NamedQuery(name="TblPbcDetalle.findAll", query="SELECT t FROM TblPbcDetalle t")
public class TblPbcDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODRESOLUCION")
	private String fldCodresolucion;

	@Column(name="FLD_CODRETACL")
	private BigDecimal fldCodretacl;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_CORRCONTROLPBC")
	private BigDecimal fldCorrcontrolpbc;

	@Column(name="FLD_CORRMOROSIDAD")
	private BigDecimal fldCorrmorosidad;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECMARCAACL")
	private Date fldFecmarcaacl;

	@Column(name="FLD_FLAGACLARACION")
	private BigDecimal fldFlagaclaracion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FLAGFECACLARACION")
	private Date fldFlagfecaclaracion;

	@Column(name="FLD_FLAGUSUARIO")
	private String fldFlagusuario;

	@Column(name="FLD_MARCAACL")
	private BigDecimal fldMarcaacl;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblPbcDetalle() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public String getFldCodresolucion() {
		return this.fldCodresolucion;
	}

	public void setFldCodresolucion(String fldCodresolucion) {
		this.fldCodresolucion = fldCodresolucion;
	}

	public BigDecimal getFldCodretacl() {
		return this.fldCodretacl;
	}

	public void setFldCodretacl(BigDecimal fldCodretacl) {
		this.fldCodretacl = fldCodretacl;
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public BigDecimal getFldCorrcontrolpbc() {
		return this.fldCorrcontrolpbc;
	}

	public void setFldCorrcontrolpbc(BigDecimal fldCorrcontrolpbc) {
		this.fldCorrcontrolpbc = fldCorrcontrolpbc;
	}

	public BigDecimal getFldCorrmorosidad() {
		return this.fldCorrmorosidad;
	}

	public void setFldCorrmorosidad(BigDecimal fldCorrmorosidad) {
		this.fldCorrmorosidad = fldCorrmorosidad;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public Date getFldFecmarcaacl() {
		return this.fldFecmarcaacl;
	}

	public void setFldFecmarcaacl(Date fldFecmarcaacl) {
		this.fldFecmarcaacl = fldFecmarcaacl;
	}

	public BigDecimal getFldFlagaclaracion() {
		return this.fldFlagaclaracion;
	}

	public void setFldFlagaclaracion(BigDecimal fldFlagaclaracion) {
		this.fldFlagaclaracion = fldFlagaclaracion;
	}

	public Date getFldFlagfecaclaracion() {
		return this.fldFlagfecaclaracion;
	}

	public void setFldFlagfecaclaracion(Date fldFlagfecaclaracion) {
		this.fldFlagfecaclaracion = fldFlagfecaclaracion;
	}

	public String getFldFlagusuario() {
		return this.fldFlagusuario;
	}

	public void setFldFlagusuario(String fldFlagusuario) {
		this.fldFlagusuario = fldFlagusuario;
	}

	public BigDecimal getFldMarcaacl() {
		return this.fldMarcaacl;
	}

	public void setFldMarcaacl(BigDecimal fldMarcaacl) {
		this.fldMarcaacl = fldMarcaacl;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}