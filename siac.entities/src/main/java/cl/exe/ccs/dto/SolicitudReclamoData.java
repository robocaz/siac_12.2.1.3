package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoData implements Serializable{

	private Integer corrSolicitudReclamo;
	private String rutTitular;
	private String nombreApePaterno;
	private String nombreApeMaterno;
	private String nombres;
	private String direccion;
	private String email;
	private Long telefono;
	private Integer tipoSolicitud;
	private String glosaTipoSolicitud;
	private Integer cantDoc;
	private Integer codCategoria;
	private String glosaCategoria;
	private Integer codComuna;
	private Integer codRegion;
	private String glosaComuna;
	private Integer codEstado;
	private String glosaEstado;
	private Integer corrCaja;
	private Integer corrNombre;
	private String fechaEstado;
	private String usuarioEstado;
	private String glosaCC;
	private String fechaSolicitud;
	private String usuarioCreacion;
	private String pdf;

	public Integer getCorrSolicitudReclamo() {
		return corrSolicitudReclamo;
	}
	public void setCorrSolicitudReclamo(Integer corrSolicitudReclamo) {
		this.corrSolicitudReclamo = corrSolicitudReclamo;
	}
	public String getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(String rutTitular) {
		this.rutTitular = rutTitular;
	}
	public String getNombreApePaterno() {
		return nombreApePaterno;
	}
	public void setNombreApePaterno(String nombreApePaterno) {
		this.nombreApePaterno = nombreApePaterno;
	}
	public String getNombreApeMaterno() {
		return nombreApeMaterno;
	}
	public void setNombreApeMaterno(String nombreApeMaterno) {
		this.nombreApeMaterno = nombreApeMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getTelefono() {
		return telefono;
	}
	public void setTelefono(Long telefono) {
		this.telefono = telefono;
	}
	public Integer getTipoSolicitud() {
		return tipoSolicitud;
	}
	public void setTipoSolicitud(Integer tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	public String getGlosaTipoSolicitud() {
		return glosaTipoSolicitud;
	}
	public void setGlosaTipoSolicitud(String glosaTipoSolicitud) {
		this.glosaTipoSolicitud = glosaTipoSolicitud;
	}
	public Integer getCantDoc() {
		return cantDoc;
	}
	public void setCantDoc(Integer cantDoc) {
		this.cantDoc = cantDoc;
	}
	public Integer getCodCategoria() {
		return codCategoria;
	}
	public void setCodCategoria(Integer codCategoria) {
		this.codCategoria = codCategoria;
	}
	public String getGlosaCategoria() {
		return glosaCategoria;
	}
	public void setGlosaCategoria(String glosaCategoria) {
		this.glosaCategoria = glosaCategoria;
	}
	public Integer getCodComuna() {
		return codComuna;
	}
	public void setCodComuna(Integer codComuna) {
		this.codComuna = codComuna;
	}
	public Integer getCodRegion() {
		return codRegion;
	}
	public void setCodRegion(Integer codRegion) {
		this.codRegion = codRegion;
	}
	public String getGlosaComuna() {
		return glosaComuna;
	}
	public void setGlosaComuna(String glosaComuna) {
		this.glosaComuna = glosaComuna;
	}
	public Integer getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}
	public String getGlosaEstado() {
		return glosaEstado;
	}
	public void setGlosaEstado(String glosaEstado) {
		this.glosaEstado = glosaEstado;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Integer corrNombre) {
		this.corrNombre = corrNombre;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public String getUsuarioEstado() {
		return usuarioEstado;
	}
	public void setUsuarioEstado(String usuarioEstado) {
		this.usuarioEstado = usuarioEstado;
	}
	public String getGlosaCC() {
		return glosaCC;
	}
	public void setGlosaCC(String glosaCC) {
		this.glosaCC = glosaCC;
	}
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getPdf() {
		return pdf;
	}
	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

}
