package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SOMovimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SOMovimiento.findAll", query="SELECT t FROM Tbl_SOMovimiento t")
public class Tbl_SOMovimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrOffline")
	private BigDecimal fld_CorrOffline;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_CorrVigNC")
	private BigDecimal fld_CorrVigNC;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FolioNro")
	private short fld_FolioNro;

	@Column(name="Fld_ListaRut")
	private String fld_ListaRut;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MontoMovimiento")
	private BigDecimal fld_MontoMovimiento;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_PtjePareo")
	private BigDecimal fld_PtjePareo;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoCert")
	private byte fld_TipoCert;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoPareo")
	private short fld_TipoPareo;

	@Column(name="Fld_TotFaltante")
	private BigDecimal fld_TotFaltante;

	@Column(name="Fld_TotSobrante")
	private BigDecimal fld_TotSobrante;

	public Tbl_SOMovimiento() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrOffline() {
		return this.fld_CorrOffline;
	}

	public void setFld_CorrOffline(BigDecimal fld_CorrOffline) {
		this.fld_CorrOffline = fld_CorrOffline;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public BigDecimal getFld_CorrVigNC() {
		return this.fld_CorrVigNC;
	}

	public void setFld_CorrVigNC(BigDecimal fld_CorrVigNC) {
		this.fld_CorrVigNC = fld_CorrVigNC;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public short getFld_FolioNro() {
		return this.fld_FolioNro;
	}

	public void setFld_FolioNro(short fld_FolioNro) {
		this.fld_FolioNro = fld_FolioNro;
	}

	public String getFld_ListaRut() {
		return this.fld_ListaRut;
	}

	public void setFld_ListaRut(String fld_ListaRut) {
		this.fld_ListaRut = fld_ListaRut;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public BigDecimal getFld_MontoMovimiento() {
		return this.fld_MontoMovimiento;
	}

	public void setFld_MontoMovimiento(BigDecimal fld_MontoMovimiento) {
		this.fld_MontoMovimiento = fld_MontoMovimiento;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public BigDecimal getFld_PtjePareo() {
		return this.fld_PtjePareo;
	}

	public void setFld_PtjePareo(BigDecimal fld_PtjePareo) {
		this.fld_PtjePareo = fld_PtjePareo;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte getFld_TipoCert() {
		return this.fld_TipoCert;
	}

	public void setFld_TipoCert(byte fld_TipoCert) {
		this.fld_TipoCert = fld_TipoCert;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public short getFld_TipoPareo() {
		return this.fld_TipoPareo;
	}

	public void setFld_TipoPareo(short fld_TipoPareo) {
		this.fld_TipoPareo = fld_TipoPareo;
	}

	public BigDecimal getFld_TotFaltante() {
		return this.fld_TotFaltante;
	}

	public void setFld_TotFaltante(BigDecimal fld_TotFaltante) {
		this.fld_TotFaltante = fld_TotFaltante;
	}

	public BigDecimal getFld_TotSobrante() {
		return this.fld_TotSobrante;
	}

	public void setFld_TotSobrante(BigDecimal fld_TotSobrante) {
		this.fld_TotSobrante = fld_TotSobrante;
	}

}