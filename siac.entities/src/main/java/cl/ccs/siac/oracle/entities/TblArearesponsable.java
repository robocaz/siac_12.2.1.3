package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_AREARESPONSABLE database table.
 * 
 */
@Entity
@Table(name="TBL_AREARESPONSABLE")
@NamedQuery(name="TblArearesponsable.findAll", query="SELECT t FROM TblArearesponsable t")
public class TblArearesponsable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODAREARESPONSABLE")
	private String fldCodarearesponsable;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSAAREARESPONSABLE")
	private String fldGlosaarearesponsable;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblArearesponsable() {
	}

	public String getFldCodarearesponsable() {
		return this.fldCodarearesponsable;
	}

	public void setFldCodarearesponsable(String fldCodarearesponsable) {
		this.fldCodarearesponsable = fldCodarearesponsable;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosaarearesponsable() {
		return this.fldGlosaarearesponsable;
	}

	public void setFldGlosaarearesponsable(String fldGlosaarearesponsable) {
		this.fldGlosaarearesponsable = fldGlosaarearesponsable;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}