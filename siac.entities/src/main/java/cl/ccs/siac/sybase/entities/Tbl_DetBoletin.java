package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetBoletin database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetBoletin.findAll", query="SELECT t FROM Tbl_DetBoletin t")
public class Tbl_DetBoletin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEspecial")
	private int fld_CodEspecial;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrBOL_Id")
	private BigDecimal fld_CorrBOL_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrVarios")
	private BigDecimal fld_CorrVarios;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_FlagTitulos")
	private short fld_FlagTitulos;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_GlosaLocalidad")
	private String fld_GlosaLocalidad;

	@Column(name="Fld_GlosaMoneda")
	private String fld_GlosaMoneda;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	@Column(name="Fld_GlosaTipoEmisor")
	private String fld_GlosaTipoEmisor;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletinPubl")
	private short fld_NroBoletinPubl;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_PagBoletinPubl")
	private short fld_PagBoletinPubl;

	@Column(name="Fld_PaginaRegistro")
	private short fld_PaginaRegistro;

	@Column(name="Fld_PagUbicRegistro")
	private short fld_PagUbicRegistro;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_RutBueno")
	private String fld_RutBueno;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	@Column(name="Fld_SecuenciaBOL")
	private int fld_SecuenciaBOL;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoRegBol")
	private String fld_TipoRegBol;

	@Column(name="Fld_TipoRelacion")
	private byte fld_TipoRelacion;

	public Tbl_DetBoletin() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodEspecial() {
		return this.fld_CodEspecial;
	}

	public void setFld_CodEspecial(int fld_CodEspecial) {
		this.fld_CodEspecial = fld_CodEspecial;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrBOL_Id() {
		return this.fld_CorrBOL_Id;
	}

	public void setFld_CorrBOL_Id(BigDecimal fld_CorrBOL_Id) {
		this.fld_CorrBOL_Id = fld_CorrBOL_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrVarios() {
		return this.fld_CorrVarios;
	}

	public void setFld_CorrVarios(BigDecimal fld_CorrVarios) {
		this.fld_CorrVarios = fld_CorrVarios;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public short getFld_FlagTitulos() {
		return this.fld_FlagTitulos;
	}

	public void setFld_FlagTitulos(short fld_FlagTitulos) {
		this.fld_FlagTitulos = fld_FlagTitulos;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public String getFld_GlosaLocalidad() {
		return this.fld_GlosaLocalidad;
	}

	public void setFld_GlosaLocalidad(String fld_GlosaLocalidad) {
		this.fld_GlosaLocalidad = fld_GlosaLocalidad;
	}

	public String getFld_GlosaMoneda() {
		return this.fld_GlosaMoneda;
	}

	public void setFld_GlosaMoneda(String fld_GlosaMoneda) {
		this.fld_GlosaMoneda = fld_GlosaMoneda;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

	public String getFld_GlosaTipoEmisor() {
		return this.fld_GlosaTipoEmisor;
	}

	public void setFld_GlosaTipoEmisor(String fld_GlosaTipoEmisor) {
		this.fld_GlosaTipoEmisor = fld_GlosaTipoEmisor;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletinPubl() {
		return this.fld_NroBoletinPubl;
	}

	public void setFld_NroBoletinPubl(short fld_NroBoletinPubl) {
		this.fld_NroBoletinPubl = fld_NroBoletinPubl;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public short getFld_PagBoletinPubl() {
		return this.fld_PagBoletinPubl;
	}

	public void setFld_PagBoletinPubl(short fld_PagBoletinPubl) {
		this.fld_PagBoletinPubl = fld_PagBoletinPubl;
	}

	public short getFld_PaginaRegistro() {
		return this.fld_PaginaRegistro;
	}

	public void setFld_PaginaRegistro(short fld_PaginaRegistro) {
		this.fld_PaginaRegistro = fld_PaginaRegistro;
	}

	public short getFld_PagUbicRegistro() {
		return this.fld_PagUbicRegistro;
	}

	public void setFld_PagUbicRegistro(short fld_PagUbicRegistro) {
		this.fld_PagUbicRegistro = fld_PagUbicRegistro;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_RutBueno() {
		return this.fld_RutBueno;
	}

	public void setFld_RutBueno(String fld_RutBueno) {
		this.fld_RutBueno = fld_RutBueno;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

	public int getFld_SecuenciaBOL() {
		return this.fld_SecuenciaBOL;
	}

	public void setFld_SecuenciaBOL(int fld_SecuenciaBOL) {
		this.fld_SecuenciaBOL = fld_SecuenciaBOL;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_TipoRegBol() {
		return this.fld_TipoRegBol;
	}

	public void setFld_TipoRegBol(String fld_TipoRegBol) {
		this.fld_TipoRegBol = fld_TipoRegBol;
	}

	public byte getFld_TipoRelacion() {
		return this.fld_TipoRelacion;
	}

	public void setFld_TipoRelacion(byte fld_TipoRelacion) {
		this.fld_TipoRelacion = fld_TipoRelacion;
	}

}