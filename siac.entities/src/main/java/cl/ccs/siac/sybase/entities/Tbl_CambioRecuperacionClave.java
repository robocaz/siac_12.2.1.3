package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CambioRecuperacionClave database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CambioRecuperacionClave.findAll", query="SELECT t FROM Tbl_CambioRecuperacionClave t")
public class Tbl_CambioRecuperacionClave implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCliente")
	private BigDecimal fld_CorrCliente;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_TipoCambio")
	private byte fld_TipoCambio;

	@Column(name="Fld_Usuario")
	private String fld_Usuario;

	public Tbl_CambioRecuperacionClave() {
	}

	public BigDecimal getFld_CorrCliente() {
		return this.fld_CorrCliente;
	}

	public void setFld_CorrCliente(BigDecimal fld_CorrCliente) {
		this.fld_CorrCliente = fld_CorrCliente;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public byte getFld_TipoCambio() {
		return this.fld_TipoCambio;
	}

	public void setFld_TipoCambio(byte fld_TipoCambio) {
		this.fld_TipoCambio = fld_TipoCambio;
	}

	public String getFld_Usuario() {
		return this.fld_Usuario;
	}

	public void setFld_Usuario(String fld_Usuario) {
		this.fld_Usuario = fld_Usuario;
	}

}