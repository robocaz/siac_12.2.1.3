package cl.ccs.siac.dao;

import java.util.List;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ImpresoraDTO;
import cl.exe.ccs.dto.MonedaDTO;
import cl.exe.ccs.dto.ServicioDTO;
import cl.exe.ccs.dto.SucursalDTO;
import cl.exe.ccs.dto.TarifaDTO;

public interface CombosDao {

	public List<ComboDTO> obtenerRegiones();
	public List<ComboDTO> obtenerComunas(Integer idRegion);
	public List<ServicioDTO> obtenerServicios();
	public List<TarifaDTO> obtenerTarifas();
	
	public List<ComboDTO> obtenerObservaciones();
	public List<ComboDTO> obtenerEmisores(String codigoTipoEmisor);
	public List<SucursalDTO> obtenerSucursales(ComboDTO banco);
	public List<ImpresoraDTO> obtenerImpresoras(Integer centroCosto);
	public List<ComboDTO> obtenerTiposEmisores();
	public List<ComboDTO> obtenerDocsAclaracion(String tipoDoc);
	public List<SucursalDTO> obtenerSucursalesEmisor(String tipoEmisor, String  codEmisor);
	public List<ComboDTO> tiposDocumentos();
	public List<MonedaDTO> obtenerMonedas();
	public List<ComboDTO> obtenerTiposEmisoresPorDoc(String tipoDocumento);
	public List<ComboDTO> obtenerMotivosDeclaracionUso();
	public List<ComboDTO> cmbDocPresentado();
	public List<ComboDTO> cmbBancos();
	public List<ComboDTO> cmbCtasCtes(String codEmisor);
	public List<ComboDTO> cmbTipoSolicRecl();
	public List<ComboDTO> cmbCategoriaSolic(Integer idSolic);

}
