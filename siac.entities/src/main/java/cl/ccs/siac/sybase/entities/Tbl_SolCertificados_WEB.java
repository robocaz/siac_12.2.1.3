package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolCertificados_WEB database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolCertificados_WEB.findAll", query="SELECT t FROM Tbl_SolCertificados_WEB t")
public class Tbl_SolCertificados_WEB implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_SOLCERTIFICADOS_WEB_FLD_CORRSOLICITUD_ID_GENERATOR", sequenceName="FLD_CORRSOLICITUD_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_SOLCERTIFICADOS_WEB_FLD_CORRSOLICITUD_ID_GENERATOR")
	@Column(name="Fld_CorrSolicitud_Id")
	private long fld_CorrSolicitud_Id;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodigoPostal")
	private String fld_CodigoPostal;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_Fax")
	private String fld_Fax;

	@Column(name="Fld_FecEntrega")
	private Timestamp fld_FecEntrega;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_ModoEnvio")
	private boolean fld_ModoEnvio;

	@Column(name="Fld_MontoTransaccion")
	private BigDecimal fld_MontoTransaccion;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NombreReceptor")
	private String fld_NombreReceptor;

	@Column(name="Fld_NroBoleta")
	private BigDecimal fld_NroBoleta;

	@Column(name="Fld_RutReceptor")
	private String fld_RutReceptor;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_TelefonoDomicilio")
	private String fld_TelefonoDomicilio;

	@Column(name="Fld_TelefonoMovil")
	private String fld_TelefonoMovil;

	@Column(name="Fld_TelefonoTrabajo")
	private String fld_TelefonoTrabajo;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TotalCertificados")
	private short fld_TotalCertificados;

	public Tbl_SolCertificados_WEB() {
	}

	public long getFld_CorrSolicitud_Id() {
		return this.fld_CorrSolicitud_Id;
	}

	public void setFld_CorrSolicitud_Id(long fld_CorrSolicitud_Id) {
		this.fld_CorrSolicitud_Id = fld_CorrSolicitud_Id;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodigoPostal() {
		return this.fld_CodigoPostal;
	}

	public void setFld_CodigoPostal(String fld_CodigoPostal) {
		this.fld_CodigoPostal = fld_CodigoPostal;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public String getFld_Fax() {
		return this.fld_Fax;
	}

	public void setFld_Fax(String fld_Fax) {
		this.fld_Fax = fld_Fax;
	}

	public Timestamp getFld_FecEntrega() {
		return this.fld_FecEntrega;
	}

	public void setFld_FecEntrega(Timestamp fld_FecEntrega) {
		this.fld_FecEntrega = fld_FecEntrega;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public boolean getFld_ModoEnvio() {
		return this.fld_ModoEnvio;
	}

	public void setFld_ModoEnvio(boolean fld_ModoEnvio) {
		this.fld_ModoEnvio = fld_ModoEnvio;
	}

	public BigDecimal getFld_MontoTransaccion() {
		return this.fld_MontoTransaccion;
	}

	public void setFld_MontoTransaccion(BigDecimal fld_MontoTransaccion) {
		this.fld_MontoTransaccion = fld_MontoTransaccion;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_NombreReceptor() {
		return this.fld_NombreReceptor;
	}

	public void setFld_NombreReceptor(String fld_NombreReceptor) {
		this.fld_NombreReceptor = fld_NombreReceptor;
	}

	public BigDecimal getFld_NroBoleta() {
		return this.fld_NroBoleta;
	}

	public void setFld_NroBoleta(BigDecimal fld_NroBoleta) {
		this.fld_NroBoleta = fld_NroBoleta;
	}

	public String getFld_RutReceptor() {
		return this.fld_RutReceptor;
	}

	public void setFld_RutReceptor(String fld_RutReceptor) {
		this.fld_RutReceptor = fld_RutReceptor;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public String getFld_TelefonoDomicilio() {
		return this.fld_TelefonoDomicilio;
	}

	public void setFld_TelefonoDomicilio(String fld_TelefonoDomicilio) {
		this.fld_TelefonoDomicilio = fld_TelefonoDomicilio;
	}

	public String getFld_TelefonoMovil() {
		return this.fld_TelefonoMovil;
	}

	public void setFld_TelefonoMovil(String fld_TelefonoMovil) {
		this.fld_TelefonoMovil = fld_TelefonoMovil;
	}

	public String getFld_TelefonoTrabajo() {
		return this.fld_TelefonoTrabajo;
	}

	public void setFld_TelefonoTrabajo(String fld_TelefonoTrabajo) {
		this.fld_TelefonoTrabajo = fld_TelefonoTrabajo;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public short getFld_TotalCertificados() {
		return this.fld_TotalCertificados;
	}

	public void setFld_TotalCertificados(short fld_TotalCertificados) {
		this.fld_TotalCertificados = fld_TotalCertificados;
	}

}