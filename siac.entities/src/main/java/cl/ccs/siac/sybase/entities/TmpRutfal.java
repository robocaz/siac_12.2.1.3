package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutfal database table.
 * 
 */
@Entity
@Table(name="tmp_rutfal")
@NamedQuery(name="TmpRutfal.findAll", query="SELECT t FROM TmpRutfal t")
public class TmpRutfal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public TmpRutfal() {
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}