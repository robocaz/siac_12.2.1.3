package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolTribunal database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolTribunal.findAll", query="SELECT t FROM Tbl_SolTribunal t")
public class Tbl_SolTribunal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CargoSolicitante")
	private String fld_CargoSolicitante;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEtapa")
	private short fld_CodEtapa;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CorrSolic_Id")
	private BigDecimal fld_CorrSolic_Id;

	@Column(name="Fld_FecElaboracionOficio")
	private Timestamp fld_FecElaboracionOficio;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecRecepSTrib")
	private Timestamp fld_FecRecepSTrib;

	@Column(name="Fld_Firma_ApMat")
	private String fld_Firma_ApMat;

	@Column(name="Fld_Firma_ApPat")
	private String fld_Firma_ApPat;

	@Column(name="Fld_Firma_Nombres")
	private String fld_Firma_Nombres;

	@Column(name="Fld_GlosaTribunal")
	private String fld_GlosaTribunal;

	@Column(name="Fld_NomAfectado_ApMat")
	private String fld_NomAfectado_ApMat;

	@Column(name="Fld_NomAfectado_ApPat")
	private String fld_NomAfectado_ApPat;

	@Column(name="Fld_NomAfectado_Nombres")
	private String fld_NomAfectado_Nombres;

	@Column(name="Fld_NomSolicitante_ApMat")
	private String fld_NomSolicitante_ApMat;

	@Column(name="Fld_NomSolicitante_ApPat")
	private String fld_NomSolicitante_ApPat;

	@Column(name="Fld_NomSolicitante_Nombres")
	private String fld_NomSolicitante_Nombres;

	@Column(name="Fld_NroOficio")
	private String fld_NroOficio;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_SolTribunal() {
	}

	public String getFld_CargoSolicitante() {
		return this.fld_CargoSolicitante;
	}

	public void setFld_CargoSolicitante(String fld_CargoSolicitante) {
		this.fld_CargoSolicitante = fld_CargoSolicitante;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEtapa() {
		return this.fld_CodEtapa;
	}

	public void setFld_CodEtapa(short fld_CodEtapa) {
		this.fld_CodEtapa = fld_CodEtapa;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public BigDecimal getFld_CorrSolic_Id() {
		return this.fld_CorrSolic_Id;
	}

	public void setFld_CorrSolic_Id(BigDecimal fld_CorrSolic_Id) {
		this.fld_CorrSolic_Id = fld_CorrSolic_Id;
	}

	public Timestamp getFld_FecElaboracionOficio() {
		return this.fld_FecElaboracionOficio;
	}

	public void setFld_FecElaboracionOficio(Timestamp fld_FecElaboracionOficio) {
		this.fld_FecElaboracionOficio = fld_FecElaboracionOficio;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecRecepSTrib() {
		return this.fld_FecRecepSTrib;
	}

	public void setFld_FecRecepSTrib(Timestamp fld_FecRecepSTrib) {
		this.fld_FecRecepSTrib = fld_FecRecepSTrib;
	}

	public String getFld_Firma_ApMat() {
		return this.fld_Firma_ApMat;
	}

	public void setFld_Firma_ApMat(String fld_Firma_ApMat) {
		this.fld_Firma_ApMat = fld_Firma_ApMat;
	}

	public String getFld_Firma_ApPat() {
		return this.fld_Firma_ApPat;
	}

	public void setFld_Firma_ApPat(String fld_Firma_ApPat) {
		this.fld_Firma_ApPat = fld_Firma_ApPat;
	}

	public String getFld_Firma_Nombres() {
		return this.fld_Firma_Nombres;
	}

	public void setFld_Firma_Nombres(String fld_Firma_Nombres) {
		this.fld_Firma_Nombres = fld_Firma_Nombres;
	}

	public String getFld_GlosaTribunal() {
		return this.fld_GlosaTribunal;
	}

	public void setFld_GlosaTribunal(String fld_GlosaTribunal) {
		this.fld_GlosaTribunal = fld_GlosaTribunal;
	}

	public String getFld_NomAfectado_ApMat() {
		return this.fld_NomAfectado_ApMat;
	}

	public void setFld_NomAfectado_ApMat(String fld_NomAfectado_ApMat) {
		this.fld_NomAfectado_ApMat = fld_NomAfectado_ApMat;
	}

	public String getFld_NomAfectado_ApPat() {
		return this.fld_NomAfectado_ApPat;
	}

	public void setFld_NomAfectado_ApPat(String fld_NomAfectado_ApPat) {
		this.fld_NomAfectado_ApPat = fld_NomAfectado_ApPat;
	}

	public String getFld_NomAfectado_Nombres() {
		return this.fld_NomAfectado_Nombres;
	}

	public void setFld_NomAfectado_Nombres(String fld_NomAfectado_Nombres) {
		this.fld_NomAfectado_Nombres = fld_NomAfectado_Nombres;
	}

	public String getFld_NomSolicitante_ApMat() {
		return this.fld_NomSolicitante_ApMat;
	}

	public void setFld_NomSolicitante_ApMat(String fld_NomSolicitante_ApMat) {
		this.fld_NomSolicitante_ApMat = fld_NomSolicitante_ApMat;
	}

	public String getFld_NomSolicitante_ApPat() {
		return this.fld_NomSolicitante_ApPat;
	}

	public void setFld_NomSolicitante_ApPat(String fld_NomSolicitante_ApPat) {
		this.fld_NomSolicitante_ApPat = fld_NomSolicitante_ApPat;
	}

	public String getFld_NomSolicitante_Nombres() {
		return this.fld_NomSolicitante_Nombres;
	}

	public void setFld_NomSolicitante_Nombres(String fld_NomSolicitante_Nombres) {
		this.fld_NomSolicitante_Nombres = fld_NomSolicitante_Nombres;
	}

	public String getFld_NroOficio() {
		return this.fld_NroOficio;
	}

	public void setFld_NroOficio(String fld_NroOficio) {
		this.fld_NroOficio = fld_NroOficio;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}