package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.caja.CajaBusiness;
import cl.ccs.siac.caja.CajaBusinessImpl;
import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.dao.CajaDaoImpl;
import cl.ccs.siac.persona.PersonaBusiness;
import cl.ccs.siac.tramitante.TramitanteBusiness;
import cl.exe.ccs.dto.CajaPendienteDTO;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteCajaDTO;
import cl.exe.ccs.dto.TramitanteDTO;
import cl.exe.ccs.dto.UsuarioDTO;

/**
 * @author rbocaz-ext Clase con servicios asociados a la pantalla de atencion de
 *         cliente (tramitante)
 *
 */
@Path("/tramitante")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class TramitanteServicio {

	private static final Logger logger = LogManager.getLogger(TramitanteServicio.class);

	@Inject
	private CajaBusiness cajaBusiness;
	
	@Inject
	private TramitanteBusiness tramitanteBusiness;

	@Inject
	private CombosBusiness combosBusiness;
	
	@Inject
	private PersonaBusiness personaBusiness;

	/**
	 * Servicio que carga el tramitante segun rut ingresado
	 * 
	 * @param request
	 * @param tramitanteDTO
	 * @return {@link Response}
	 * @throws ServletException
	 */
	@POST
	@Path("/carga")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cargaTramitante(@Context HttpServletRequest request, TramitanteDTO tramitanteDTO)
			throws ServletException {
		try {

			logger.debug("this is a sample log message.");

			tramitanteDTO = tramitanteBusiness.obtenerTramitante(tramitanteDTO.getRut());
			if (tramitanteDTO != null) {
				request.getSession().setAttribute("tramitante", tramitanteDTO);
			}
			return Response.ok(tramitanteDTO).build();

		} catch (Exception ex) {
			logger.error("Metodo cargaTramitante  - Carga Tramitante " + tramitanteDTO.getRut() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Serivio que carga combo de regiones
	 * 
	 * @param request
	 * @param comboDTO
	 * @return {@link Response}
	 */
	@GET
	@Path("/cargaRegiones")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cargaRegiones(@Context HttpServletRequest request) {

		List<ComboDTO> regionesCombo = combosBusiness.obtenerRegiones();
		return Response.ok(regionesCombo).build();

	}

	/**
	 * Servicio que carga las comuna por codigo de region
	 * 
	 * @param idRegion
	 * @return {@link Response}
	 */
	@GET
	@Path("/cargaComunas")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cargaComunas(@QueryParam("idRegion") String idRegion) {

		try{
			List<ComboDTO> comunasCombo = combosBusiness.obtenerComunas(Integer.parseInt(idRegion));
			return Response.ok(comunasCombo).build();
		}
		catch (Exception ex) {
			logger.error("Metodo cargaComunas  - Carga Comunas - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	/**
	 * Servicio encargado de actualizar tramitante
	 * 
	 * @param request
	 * @param tramitanteDTO
	 * @return {@link Response}
	 */
	@POST
	@Path("/actualizatramitante")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response actualizaTramitante(@Context HttpServletRequest request, TramitanteDTO tramitanteDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			logger.error("Error en el metodo: actualizaTramitante - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		TramitanteDTO tramupdate = new TramitanteDTO();
		try {
			
			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			CajaPendienteDTO cajaPendiente = cajaBusiness.cajasAbiertaPendiente(usuarioDTO.getUsername());
			if(null != cajaPendiente && null!=cajaPendiente.getCorrCaja() && !"0".equals(cajaPendiente.getCorrCaja())){
				usuarioDTO.setCaja(cajaPendiente.getCorrCaja());
				request.getSession().setAttribute("usuario", usuarioDTO);
			}
			
			if (tramitanteDTO != null) {
				tramitanteDTO.setUsuario(userPrincipal.getName());
				tramupdate = tramitanteBusiness.actualizarTramitante(tramitanteDTO);
				if (tramupdate != null) {
					request.getSession().setAttribute("tramitante", tramupdate);
				}
			}

		} catch (Exception e) {
			logger.error("Metodo actualizaTramitante - Actualiza Tramitante " + tramitanteDTO.getRut() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.ok(tramupdate).build();

	}

	/**
	 * Servicio que revisa el objeto tramitante en sesion
	 * 
	 * @param request
	 * @return {@link Response}
	 */
	@GET
	@Path("/status")
	@Consumes("application/json")
	public Response status(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			TramitanteDTO dto = (TramitanteDTO) request.getSession().getAttribute("tramitante");
			if (userPrincipal != null && dto != null) {
				return Response.ok(dto).build();
			} else {
				return Response.ok(null).build();
			}
		} catch (Exception ex) {
			logger.error("Metodo status - Message: Error del sistema no controlado", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Obtiene el tramitante por id de caja.
	 * 
	 * @param request
	 * @param idCaja
	 * @return {@link Response}
	 * @throws ServletException
	 */
	@POST
	@Path("/obtenertramitantecaja")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenertramitantecaja(@Context HttpServletRequest request, String idCaja) throws ServletException {
		try {
			
			if (!"".equals(idCaja) || idCaja != null) {

				UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
				
				if(usuarioDTO.getCaja() != null && !usuarioDTO.getCaja().equals("")){
					TramitanteDTO tramitanteDTO = tramitanteBusiness.obtenerTramitante(new Long(usuarioDTO.getCaja()));
					return Response.ok(tramitanteDTO).build();
				}
				else{
					TramitanteDTO tramitanteDTO = tramitanteBusiness.obtenerTramitante(new Long(idCaja));
					return Response.ok(tramitanteDTO).build();
				}

				

			} else {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			logger.error("Metodo obtenertramitantecaja  - Obtener Tramitante Caja  idCaja: " + idCaja + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Servicio que obtiene la declaracion de uso
	 * 
	 * @param request
	 * @param tramitanteCajaDTO
	 * @return {@link Response}
	 */
	@POST
	@Path("/obtenerDeclaracionUso")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDeclaracionUso(@Context HttpServletRequest request, TramitanteCajaDTO tramitanteCajaDTO) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			logger.error("Error en el metodo: actualizaTramitante - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(tramitanteBusiness.obtenerDeclaracionUso(tramitanteCajaDTO)).build();
		} catch (Exception e) {
			logger.error("Metodo obtenerDeclaracionUso  - Obtener Declaración Uso " + tramitanteCajaDTO.getRutTramitante() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * 
	 * @param request
	 * @param tramitanteCajaDTO
	 * @return {@link Response}
	 */
	@POST
	@Path("/declaracionUso")
	@Produces(MediaType.APPLICATION_JSON)
	public Response declaracionUso(@Context HttpServletRequest request, TramitanteCajaDTO tramitanteCajaDTO) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			logger.error("Error en el metodo: actualizaTramitante - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			
		   //TramitanteDTO tramitanteDTO = tramitanteBusiness.actualizarTramitante(tramitanteCajaDTO);
			
			ResponseDTO response = tramitanteBusiness.declaracionUso(tramitanteCajaDTO);
			return Response.ok(response).build();
		} catch (Exception e) {
			logger.error("Actualiza Declaración Uso " + tramitanteCajaDTO.getRutTramitante() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	
	@POST
	@Path("/cargaDeclaracionUso")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cargaDeclaracionUso(@Context HttpServletRequest request, TramitanteCajaDTO tramitanteCajaDTO) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			logger.error("Error en el metodo: actualizaTramitante - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			tramitanteCajaDTO = tramitanteBusiness.obtenerDeclaracionUso(tramitanteCajaDTO);
			//ResponseDTO response = tramitanteBusiness.declaracionUso(tramitanteCajaDTO);
			return Response.ok(tramitanteCajaDTO).build();
		} catch (Exception e) {
			logger.error("Obtiene Declaración Uso " + tramitanteCajaDTO.getRutTramitante() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	

	/**
	 * Servicio que carga el combo de motivos
	 * 
	 * @param request
	 * @param comboDTO
	 * @return {@link Response}
	 */

	@GET
	@Path("/cargaMotivos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cargaMotivos(@Context HttpServletRequest request) {
		try {
			List<ComboDTO> motivosCombo = combosBusiness.obtenerMotivosDeclaracionUso();
			return Response.ok(motivosCombo).build();
		} catch (Exception ex) {
			logger.error("Obtener Motivos de Declaración de Uso - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
