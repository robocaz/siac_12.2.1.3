package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.persona.PersonaBusiness;
import cl.ccs.siac.tramitante.TramitanteBusiness;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteDTO;

@Path("/persona")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class PersonaServicio {

	private static final Logger log = LogManager.getLogger(PersonaServicio.class);

	@Inject
	private PersonaBusiness personaBusiness;

	@Inject
	private TramitanteBusiness tramitanteBusiness;

	@POST
	@Path("/obtenertramitante")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerTramitante(@Context HttpServletRequest request, PersonaDTO personaDTO)
			throws ServletException {
		try {

			TramitanteDTO tramitante = tramitanteBusiness.obtenerTramitante(personaDTO.getRut());
			personaDTO.setApellidoPaterno(tramitante.getApellidoPaterno());
			personaDTO.setApellidoMaterno(tramitante.getApellidoMaterno());
			personaDTO.setNombres(tramitante.getNombres());

			return Response.ok(personaDTO).build();

		} catch (Exception ex) {
			log.error("Metodo obtenerTramitante - Obtener Persona" + personaDTO.getRut() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/obtenerpersona")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerPersona(@Context HttpServletRequest request, PersonaDTO personaDTO) throws ServletException {
		try {

			personaDTO = personaBusiness.obtener(personaDTO.getRut());

			return Response.ok(personaDTO).build();

		} catch (Exception ex) {
			log.error("Metodo obtenerPersona - Obtener Persona" + personaDTO.getRut() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/obtenerpersonanombre")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerPersonaNombre(@Context HttpServletRequest request, PersonaDTO personaDTO) throws ServletException {
		try {

			personaDTO = personaBusiness.getNombre(personaDTO.getRut(), personaDTO.getCorrelativo());

			return Response.ok(personaDTO).build();

		} catch (Exception ex) {
			log.error("Metodo obtenerPersona - Obtener Persona" + personaDTO.getRut() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	
	
	
	@POST
	@Path("/actualizar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response actualizar(@Context HttpServletRequest request, PersonaDTO tramitanteDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			tramitanteDTO = personaBusiness.actualizar(tramitanteDTO);

		} catch (Exception e) {
			log.error("Metodo actualizar - Actualizar Persona " + tramitanteDTO.getRut() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.ok(tramitanteDTO).build();

	}
	
	@POST
	@Path("/verificar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response verificar(@Context HttpServletRequest request, String rut) {
		ResponseDTO verif = new ResponseDTO();
		
		try {
			verif = personaBusiness.verificar(rut);

		} catch (Exception e) {
			log.error("Metodo verificar - Persona no puede sacar ILG " + rut + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.ok(verif).build();

	}

}
