package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AclaracionDTO extends ResponseDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6871544369376202465L;
	
	private ComboDTO docpresentada;
	private ComboDTO emisor;
	private SucursalDTO sucursal;
	private ComboDTO tipoemisor;
	private NumeroConfirmatorioDTO numeroconfirmatorio;
	private NumeroFirmasDTO numerofirmas;
	private List<DetalleFirmaDTO> listafirmantes;
	private String fechapago;
	private String fecPublicacionString;
	private String fechaprotesto;
	private String fechaAclaracion;
	private String monto;
	private Long numboletin;
	private Long numoperacion;
	private Long estadocuota;
	private PersonaDTO afectado;
	private Long corrcaja;
	private ComboDTO tipodocumento;
	private MonedaDTO tipomoneda;
	private Long codRetAcl;
	private String glosaRetAcl;
	private Long ptjePareo;
	private Long tipoPareo;
	private Long montoMovimiento;
	private Long corrProt;
	private Long marcaAclaracion;
	private Long nroBoletin;
	private Long pagBoletin;
	private Date fecPublicacion;
	private String nombreLibrador;
	private Long codLocPub;
	private String tipoDocumentoImpago;
	private Long nroOper4DigAux;
	private Long codMonedaAux;
	private String montoProtAux;
	private Long cantFilas;
	private String codServicio;
	private CentroCostoDTO centroCosto;
	private TramitanteDTO tramitante;
	private PersonaDTO persona;
	private String listaRut;
	private Long tempCorrAcl;
	private List<AclaracionDTO> candidatos;
	private AclaracionDTO candidatoSeleccionado;
	private BigDecimal costoAclaracion;
	private Long codLocalidad;
	private Long codEstado;
	private Long marcaAclaracionAux;
	private Long criterio;
	private ComboDTO estadoProtesto;
	private Long tipoCertificado;
	private VentaCarteraDTO ventaCartera;
	private List<BipersonalDTO> bipersonales;

	public ComboDTO getDocpresentada() {
		return docpresentada;
	}
	public void setDocpresentada(ComboDTO docpresentada) {
		this.docpresentada = docpresentada;
	}
	public ComboDTO getEmisor() {
		return emisor;
	}
	public void setEmisor(ComboDTO emisor) {
		this.emisor = emisor;
	}
	public SucursalDTO getSucursal() {
		return sucursal;
	}
	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}
	public ComboDTO getTipoemisor() {
		return tipoemisor;
	}
	public void setTipoemisor(ComboDTO tipoemisor) {
		this.tipoemisor = tipoemisor;
	}
	public NumeroConfirmatorioDTO getNumeroconfirmatorio() {
		return numeroconfirmatorio;
	}
	public void setNumeroconfirmatorio(NumeroConfirmatorioDTO numeroconfirmatorio) {
		this.numeroconfirmatorio = numeroconfirmatorio;
	}
	public NumeroFirmasDTO getNumerofirmas() {
		return numerofirmas;
	}
	public void setNumerofirmas(NumeroFirmasDTO numerofirmas) {
		this.numerofirmas = numerofirmas;
	}
	public List<DetalleFirmaDTO> getListafirmantes() {
		return listafirmantes;
	}
	public void setListafirmantes(List<DetalleFirmaDTO> listafirmantes) {
		this.listafirmantes = listafirmantes;
	}
	public String getFechapago() {
		return fechapago;
	}
	public void setFechapago(String fechapago) {
		this.fechapago = fechapago;
	}
	public String getFechaprotesto() {
		return fechaprotesto;
	}
	public void setFechaprotesto(String fechaprotesto) {
		this.fechaprotesto = fechaprotesto;
	}
	public String getFechaAclaracion() {
		return fechaAclaracion;
	}
	public void setFechaAclaracion(String fechaAclaracion) {
		this.fechaAclaracion = fechaAclaracion;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public Long getNumboletin() {
		return numboletin;
	}
	public void setNumboletin(Long numboletin) {
		this.numboletin = numboletin;
	}
	public Long getNumoperacion() {
		return numoperacion;
	}
	public void setNumoperacion(Long numoperacion) {
		this.numoperacion = numoperacion;
	}
	public Long getEstadocuota() {
		return estadocuota;
	}
	public void setEstadocuota(Long estadocuota) {
		this.estadocuota = estadocuota;
	}

	
	public PersonaDTO getAfectado() {
		return afectado;
	}
	public void setAfectado(PersonaDTO afectado) {
		this.afectado = afectado;
	}
	public Long getCorrcaja() {
		return corrcaja;
	}
	public void setCorrcaja(Long corrcaja) {
		this.corrcaja = corrcaja;
	}
	public ComboDTO getTipodocumento() {
		return tipodocumento;
	}
	public void setTipodocumento(ComboDTO tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	public MonedaDTO getTipomoneda() {
		return tipomoneda;
	}
	public void setTipomoneda(MonedaDTO tipomoneda) {
		this.tipomoneda = tipomoneda;
	}
	public Long getCodRetAcl() {
		return codRetAcl;
	}
	public void setCodRetAcl(Long codRetAcl) {
		this.codRetAcl = codRetAcl;
	}
	public String getGlosaRetAcl() {
		return glosaRetAcl;
	}
	public void setGlosaRetAcl(String glosaRetAcl) {
		this.glosaRetAcl = glosaRetAcl;
	}
	public Long getPtjePareo() {
		return ptjePareo;
	}
	public void setPtjePareo(Long ptjePareo) {
		this.ptjePareo = ptjePareo;
	}
	public Long getTipoPareo() {
		return tipoPareo;
	}
	public void setTipoPareo(Long tipoPareo) {
		this.tipoPareo = tipoPareo;
	}
	public Long getMontoMovimiento() {
		return montoMovimiento;
	}
	public void setMontoMovimiento(Long montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}
	public Long getCorrProt() {
		return corrProt;
	}
	public void setCorrProt(Long corrProt) {
		this.corrProt = corrProt;
	}
	public Long getMarcaAclaracion() {
		return marcaAclaracion;
	}
	public void setMarcaAclaracion(Long marcaAclaracion) {
		this.marcaAclaracion = marcaAclaracion;
	}
	public Long getNroBoletin() {
		return nroBoletin;
	}
	public void setNroBoletin(Long nroBoletin) {
		this.nroBoletin = nroBoletin;
	}
	public Long getPagBoletin() {
		return pagBoletin;
	}
	public void setPagBoletin(Long pagBoletin) {
		this.pagBoletin = pagBoletin;
	}
	public Date getFecPublicacion() {
		return fecPublicacion;
	}
	public void setFecPublicacion(Date fecPublicacion) {
		this.fecPublicacion = fecPublicacion;
	}
	public String getNombreLibrador() {
		return nombreLibrador;
	}
	public void setNombreLibrador(String nombreLibrador) {
		this.nombreLibrador = nombreLibrador;
	}
	public Long getCodLocPub() {
		return codLocPub;
	}
	public void setCodLocPub(Long codLocPub) {
		this.codLocPub = codLocPub;
	}
	public String getTipoDocumentoImpago() {
		return tipoDocumentoImpago;
	}
	public void setTipoDocumentoImpago(String tipoDocumentoImpago) {
		this.tipoDocumentoImpago = tipoDocumentoImpago;
	}
	public Long getNroOper4DigAux() {
		return nroOper4DigAux;
	}
	public void setNroOper4DigAux(Long nroOper4DigAux) {
		this.nroOper4DigAux = nroOper4DigAux;
	}
	public Long getCodMonedaAux() {
		return codMonedaAux;
	}
	public void setCodMonedaAux(Long codMonedaAux) {
		this.codMonedaAux = codMonedaAux;
	}
	public String getMontoProtAux() {
		return montoProtAux;
	}
	public void setMontoProtAux(String montoProtAux) {
		this.montoProtAux = montoProtAux;
	}
	public Long getCantFilas() {
		return cantFilas;
	}
	public void setCantFilas(Long cantFilas) {
		this.cantFilas = cantFilas;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public CentroCostoDTO getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(CentroCostoDTO centroCosto) {
		this.centroCosto = centroCosto;
	}
	public TramitanteDTO getTramitante() {
		return tramitante;
	}
	public void setTramitante(TramitanteDTO tramitante) {
		this.tramitante = tramitante;
	}
	public PersonaDTO getPersona() {
		return persona;
	}
	public void setPersona(PersonaDTO persona) {
		this.persona = persona;
	}
	public String getListaRut() {
		return listaRut;
	}
	public void setListaRut(String listaRut) {
		this.listaRut = listaRut;
	}
	public Long getTempCorrAcl() {
		return tempCorrAcl;
	}
	public void setTempCorrAcl(Long tempCorrAcl) {
		this.tempCorrAcl = tempCorrAcl;
	}
	public List<AclaracionDTO> getCandidatos() {
		return candidatos;
	}
	public void setCandidatos(List<AclaracionDTO> candidatos) {
		this.candidatos = candidatos;
	}
	public BigDecimal getCostoAclaracion() {
		return costoAclaracion;
	}
	public void setCostoAclaracion(BigDecimal costoAclaracion) {
		this.costoAclaracion = costoAclaracion;
	}
	public Long getCodLocalidad() {
		return codLocalidad;
	}
	public void setCodLocalidad(Long codLocalidad) {
		this.codLocalidad = codLocalidad;
	}
	public Long getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(Long codEstado) {
		this.codEstado = codEstado;
	}
	public Long getMarcaAclaracionAux() {
		return marcaAclaracionAux;
	}
	public void setMarcaAclaracionAux(Long marcaAclaracionAux) {
		this.marcaAclaracionAux = marcaAclaracionAux;
	}
	public AclaracionDTO getCandidatoSeleccionado() {
		return candidatoSeleccionado;
	}
	public void setCandidatoSeleccionado(AclaracionDTO candidatoSeleccionado) {
		this.candidatoSeleccionado = candidatoSeleccionado;
	}
	public Long getCriterio() {
		return criterio;
	}
	public void setCriterio(Long criterio) {
		this.criterio = criterio;
	}
	public ComboDTO getEstadoProtesto() {
		return estadoProtesto;
	}
	public void setEstadoProtesto(ComboDTO estadoProtesto) {
		this.estadoProtesto = estadoProtesto;
	}
	public Long getTipoCertificado() {
		return tipoCertificado;
	}
	public void setTipoCertificado(Long tipoCertificado) {
		this.tipoCertificado = tipoCertificado;
	}
	public VentaCarteraDTO getVentaCartera() {
		return ventaCartera;
	}
	public void setVentaCartera(VentaCarteraDTO ventaCartera) {
		this.ventaCartera = ventaCartera;
	}
	public List<BipersonalDTO> getBipersonales() {
		return bipersonales;
	}
	public void setBipersonales(List<BipersonalDTO> bipersonales) {
		this.bipersonales = bipersonales;
	}
	public String getFecPublicacionString() {
		return fecPublicacionString;
	}
	public void setFecPublicacionString(String fecPublicacionString) {
		this.fecPublicacionString = fecPublicacionString;
	}

	
	
	
	
	
	
	

}
