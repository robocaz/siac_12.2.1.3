package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdSegtoBloqueoCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdSegtoBloqueoCtaCte.findAll", query="SELECT t FROM Tb_CeIdSegtoBloqueoCtaCte t")
public class Tb_CeIdSegtoBloqueoCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDSEGTOBLOQUEOCTACTE_FLD_CORRBLOQUEOCTA_ID_GENERATOR", sequenceName="FLD_CORRBLOQUEOCTA_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDSEGTOBLOQUEOCTACTE_FLD_CORRBLOQUEOCTA_ID_GENERATOR")
	@Column(name="Fld_CorrBloqueoCta_Id")
	private long fld_CorrBloqueoCta_Id;

	@Column(name="Fld_CodCausalBloqueo")
	private byte fld_CodCausalBloqueo;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodSitBloqueo")
	private byte fld_CodSitBloqueo;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecBloqueo")
	private Timestamp fld_FecBloqueo;

	@Column(name="Fld_FecSolicBloqueo")
	private Timestamp fld_FecSolicBloqueo;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_Flag_VigenciaBloqueo")
	private boolean fld_Flag_VigenciaBloqueo;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_NroSolicBloqueo")
	private BigDecimal fld_NroSolicBloqueo;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdDetErrBloCtaCte
	@OneToMany(mappedBy="tbCeIdSegtoBloqueoCtaCte")
	private List<Tb_CeIdDetErrBloCtaCte> tbCeIdDetErrBloCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdCtaCteBloqueada
	@ManyToOne
	@JoinColumn(name="Fld_CorrCtaCteBlq")
	private Tb_CeIdCtaCteBloqueada tbCeIdCtaCteBloqueada;

	public Tb_CeIdSegtoBloqueoCtaCte() {
	}

	public long getFld_CorrBloqueoCta_Id() {
		return this.fld_CorrBloqueoCta_Id;
	}

	public void setFld_CorrBloqueoCta_Id(long fld_CorrBloqueoCta_Id) {
		this.fld_CorrBloqueoCta_Id = fld_CorrBloqueoCta_Id;
	}

	public byte getFld_CodCausalBloqueo() {
		return this.fld_CodCausalBloqueo;
	}

	public void setFld_CodCausalBloqueo(byte fld_CodCausalBloqueo) {
		this.fld_CodCausalBloqueo = fld_CodCausalBloqueo;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodSitBloqueo() {
		return this.fld_CodSitBloqueo;
	}

	public void setFld_CodSitBloqueo(byte fld_CodSitBloqueo) {
		this.fld_CodSitBloqueo = fld_CodSitBloqueo;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecBloqueo() {
		return this.fld_FecBloqueo;
	}

	public void setFld_FecBloqueo(Timestamp fld_FecBloqueo) {
		this.fld_FecBloqueo = fld_FecBloqueo;
	}

	public Timestamp getFld_FecSolicBloqueo() {
		return this.fld_FecSolicBloqueo;
	}

	public void setFld_FecSolicBloqueo(Timestamp fld_FecSolicBloqueo) {
		this.fld_FecSolicBloqueo = fld_FecSolicBloqueo;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_Flag_VigenciaBloqueo() {
		return this.fld_Flag_VigenciaBloqueo;
	}

	public void setFld_Flag_VigenciaBloqueo(boolean fld_Flag_VigenciaBloqueo) {
		this.fld_Flag_VigenciaBloqueo = fld_Flag_VigenciaBloqueo;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public BigDecimal getFld_NroSolicBloqueo() {
		return this.fld_NroSolicBloqueo;
	}

	public void setFld_NroSolicBloqueo(BigDecimal fld_NroSolicBloqueo) {
		this.fld_NroSolicBloqueo = fld_NroSolicBloqueo;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public List<Tb_CeIdDetErrBloCtaCte> getTbCeIdDetErrBloCtaCtes() {
		return this.tbCeIdDetErrBloCtaCtes;
	}

	public void setTbCeIdDetErrBloCtaCtes(List<Tb_CeIdDetErrBloCtaCte> tbCeIdDetErrBloCtaCtes) {
		this.tbCeIdDetErrBloCtaCtes = tbCeIdDetErrBloCtaCtes;
	}

	public Tb_CeIdDetErrBloCtaCte addTbCeIdDetErrBloCtaCte(Tb_CeIdDetErrBloCtaCte tbCeIdDetErrBloCtaCte) {
		getTbCeIdDetErrBloCtaCtes().add(tbCeIdDetErrBloCtaCte);
		tbCeIdDetErrBloCtaCte.setTbCeIdSegtoBloqueoCtaCte(this);

		return tbCeIdDetErrBloCtaCte;
	}

	public Tb_CeIdDetErrBloCtaCte removeTbCeIdDetErrBloCtaCte(Tb_CeIdDetErrBloCtaCte tbCeIdDetErrBloCtaCte) {
		getTbCeIdDetErrBloCtaCtes().remove(tbCeIdDetErrBloCtaCte);
		tbCeIdDetErrBloCtaCte.setTbCeIdSegtoBloqueoCtaCte(null);

		return tbCeIdDetErrBloCtaCte;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public Tb_CeIdCtaCteBloqueada getTbCeIdCtaCteBloqueada() {
		return this.tbCeIdCtaCteBloqueada;
	}

	public void setTbCeIdCtaCteBloqueada(Tb_CeIdCtaCteBloqueada tbCeIdCtaCteBloqueada) {
		this.tbCeIdCtaCteBloqueada = tbCeIdCtaCteBloqueada;
	}

}