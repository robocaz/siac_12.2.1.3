package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_AUDITAUSUARIOROL database table.
 * 
 */
@Entity
@Table(name="TBL_AUDITAUSUARIOROL")
@NamedQuery(name="TblAuditausuariorol.findAll", query="SELECT t FROM TblAuditausuariorol t")
public class TblAuditausuariorol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODIGOROL")
	private String fldCodigorol;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FECHADELETE")
	private Timestamp fldFechadelete;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIODELETE")
	private String fldUsuariodelete;

	public TblAuditausuariorol() {
	}

	public String getFldCodigorol() {
		return this.fldCodigorol;
	}

	public void setFldCodigorol(String fldCodigorol) {
		this.fldCodigorol = fldCodigorol;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Timestamp getFldFechadelete() {
		return this.fldFechadelete;
	}

	public void setFldFechadelete(Timestamp fldFechadelete) {
		this.fldFechadelete = fldFechadelete;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuariodelete() {
		return this.fldUsuariodelete;
	}

	public void setFldUsuariodelete(String fldUsuariodelete) {
		this.fldUsuariodelete = fldUsuariodelete;
	}

}