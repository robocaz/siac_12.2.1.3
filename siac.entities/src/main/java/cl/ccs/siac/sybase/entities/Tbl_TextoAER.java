package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TextoAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TextoAER.findAll", query="SELECT t FROM Tbl_TextoAER t")
public class Tbl_TextoAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrDocum")
	private BigDecimal fld_CorrDocum;

	@Column(name="Fld_CorrOrdenStr_Id")
	private BigDecimal fld_CorrOrdenStr_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_TextoLinea1")
	private String fld_TextoLinea1;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_TextoAER() {
	}

	public BigDecimal getFld_CorrDocum() {
		return this.fld_CorrDocum;
	}

	public void setFld_CorrDocum(BigDecimal fld_CorrDocum) {
		this.fld_CorrDocum = fld_CorrDocum;
	}

	public BigDecimal getFld_CorrOrdenStr_Id() {
		return this.fld_CorrOrdenStr_Id;
	}

	public void setFld_CorrOrdenStr_Id(BigDecimal fld_CorrOrdenStr_Id) {
		this.fld_CorrOrdenStr_Id = fld_CorrOrdenStr_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public String getFld_TextoLinea1() {
		return this.fld_TextoLinea1;
	}

	public void setFld_TextoLinea1(String fld_TextoLinea1) {
		this.fld_TextoLinea1 = fld_TextoLinea1;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}