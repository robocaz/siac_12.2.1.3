package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_PtjePareo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PtjePareo.findAll", query="SELECT t FROM Tbl_PtjePareo t")
public class Tbl_PtjePareo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_PtjePareo")
	private BigDecimal fld_PtjePareo;

	public Tbl_PtjePareo() {
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_PtjePareo() {
		return this.fld_PtjePareo;
	}

	public void setFld_PtjePareo(BigDecimal fld_PtjePareo) {
		this.fld_PtjePareo = fld_PtjePareo;
	}

}