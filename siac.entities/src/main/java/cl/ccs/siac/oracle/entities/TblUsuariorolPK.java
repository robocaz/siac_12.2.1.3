package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TBL_USUARIOROL database table.
 * 
 */
@Embeddable
public class TblUsuariorolPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	@Column(name="FLD_CODIGOROL")
	private String fldCodigorol;

	public TblUsuariorolPK() {
	}
	public String getFldUsuario() {
		return this.fldUsuario;
	}
	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}
	public String getFldCodigorol() {
		return this.fldCodigorol;
	}
	public void setFldCodigorol(String fldCodigorol) {
		this.fldCodigorol = fldCodigorol;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TblUsuariorolPK)) {
			return false;
		}
		TblUsuariorolPK castOther = (TblUsuariorolPK)other;
		return 
			this.fldUsuario.equals(castOther.fldUsuario)
			&& this.fldCodigorol.equals(castOther.fldCodigorol);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fldUsuario.hashCode();
		hash = hash * prime + this.fldCodigorol.hashCode();
		
		return hash;
	}
}