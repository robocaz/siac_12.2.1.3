package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_TIPOUSUARIO database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOUSUARIO")
@NamedQuery(name="TblTipousuario.findAll", query="SELECT t FROM TblTipousuario t")
public class TblTipousuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOUSUARIO_FLDTIPOUSUARIO_GENERATOR", sequenceName="FLD_TIPOUSUARIO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOUSUARIO_FLDTIPOUSUARIO_GENERATOR")
	@Column(name="FLD_TIPOUSUARIO")
	private long fldTipousuario;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATIPOUSUARIO")
	private String fldGlosatipousuario;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblUsuariomol
	@OneToMany(mappedBy="tblTipousuario")
	private List<TblUsuariomol> tblUsuariomols;

	public TblTipousuario() {
	}

	public long getFldTipousuario() {
		return this.fldTipousuario;
	}

	public void setFldTipousuario(long fldTipousuario) {
		this.fldTipousuario = fldTipousuario;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatipousuario() {
		return this.fldGlosatipousuario;
	}

	public void setFldGlosatipousuario(String fldGlosatipousuario) {
		this.fldGlosatipousuario = fldGlosatipousuario;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblUsuariomol> getTblUsuariomols() {
		return this.tblUsuariomols;
	}

	public void setTblUsuariomols(List<TblUsuariomol> tblUsuariomols) {
		this.tblUsuariomols = tblUsuariomols;
	}

	public TblUsuariomol addTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().add(tblUsuariomol);
		tblUsuariomol.setTblTipousuario(this);

		return tblUsuariomol;
	}

	public TblUsuariomol removeTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().remove(tblUsuariomol);
		tblUsuariomol.setTblTipousuario(null);

		return tblUsuariomol;
	}

}