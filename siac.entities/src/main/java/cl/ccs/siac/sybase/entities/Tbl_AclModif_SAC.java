package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AclModif_SAC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclModif_SAC.findAll", query="SELECT t FROM Tbl_AclModif_SAC t")
public class Tbl_AclModif_SAC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="corr_acl")
	private BigDecimal corrAcl;

	@Column(name="fec_gen")
	private Timestamp fecGen;

	@Column(name="fec_modif")
	private Timestamp fecModif;

	public Tbl_AclModif_SAC() {
	}

	public BigDecimal getCorrAcl() {
		return this.corrAcl;
	}

	public void setCorrAcl(BigDecimal corrAcl) {
		this.corrAcl = corrAcl;
	}

	public Timestamp getFecGen() {
		return this.fecGen;
	}

	public void setFecGen(Timestamp fecGen) {
		this.fecGen = fecGen;
	}

	public Timestamp getFecModif() {
		return this.fecModif;
	}

	public void setFecModif(Timestamp fecModif) {
		this.fecModif = fecModif;
	}

}