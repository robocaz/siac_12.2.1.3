package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class PDFArqueoCajaDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = -7582559955048902606L;

	private String caja;
	private String fecha;
	private String user;
	private Integer cc;
	private String ccDesc;
	private String rev;
	private List<Integer> billNums;
	private List<Integer> billTots;
	private List<Integer> coinNums;
	private List<Integer> coinTots;
	private Integer billTotal;
	private Integer coinTotal;
	private Integer totDocs;
	private Integer totCash;
	private Integer fondo;
	private Integer totArqueo;
	private Integer diff;
	private String obs;

	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Integer getCc() {
		return cc;
	}
	public void setCc(Integer cc) {
		this.cc = cc;
	}
	public String getCcDesc() {
		return ccDesc;
	}
	public void setCcDesc(String ccDesc) {
		this.ccDesc = ccDesc;
	}
	public String getRev() {
		return rev;
	}
	public void setRev(String rev) {
		this.rev = rev;
	}
	public List<Integer> getBillNums() {
		return billNums;
	}
	public void setBillNums(List<Integer> billNums) {
		this.billNums = billNums;
	}
	public List<Integer> getBillTots() {
		return billTots;
	}
	public void setBillTots(List<Integer> billTots) {
		this.billTots = billTots;
	}
	public List<Integer> getCoinNums() {
		return coinNums;
	}
	public void setCoinNums(List<Integer> coinNums) {
		this.coinNums = coinNums;
	}
	public List<Integer> getCoinTots() {
		return coinTots;
	}
	public void setCoinTots(List<Integer> coinTots) {
		this.coinTots = coinTots;
	}
	public Integer getBillTotal() {
		return billTotal;
	}
	public void setBillTotal(Integer billTotal) {
		this.billTotal = billTotal;
	}
	public Integer getCoinTotal() {
		return coinTotal;
	}
	public void setCoinTotal(Integer coinTotal) {
		this.coinTotal = coinTotal;
	}
	public Integer getTotDocs() {
		return totDocs;
	}
	public void setTotDocs(Integer totDocs) {
		this.totDocs = totDocs;
	}
	public Integer getTotCash() {
		return totCash;
	}
	public void setTotCash(Integer totCash) {
		this.totCash = totCash;
	}
	public Integer getFondo() {
		return fondo;
	}
	public void setFondo(Integer fondo) {
		this.fondo = fondo;
	}
	public Integer getTotArqueo() {
		return totArqueo;
	}
	public void setTotArqueo(Integer totArqueo) {
		this.totArqueo = totArqueo;
	}
	public Integer getDiff() {
		return diff;
	}
	public void setDiff(Integer diff) {
		this.diff = diff;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PDFArqueoCajaDTO [caja=");
		builder.append(caja);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", user=");
		builder.append(user);
		builder.append(", cc=");
		builder.append(cc);
		builder.append(", ccDesc=");
		builder.append(ccDesc);
		builder.append(", rev=");
		builder.append(rev);
		builder.append(", billNums=");
		builder.append(billNums);
		builder.append(", billTots=");
		builder.append(billTots);
		builder.append(", coinNums=");
		builder.append(coinNums);
		builder.append(", coinTots=");
		builder.append(coinTots);
		builder.append(", billTotal=");
		builder.append(billTotal);
		builder.append(", coinTotal=");
		builder.append(coinTotal);
		builder.append(", totDocs=");
		builder.append(totDocs);
		builder.append(", totCash=");
		builder.append(totCash);
		builder.append(", fondo=");
		builder.append(fondo);
		builder.append(", totArqueo=");
		builder.append(totArqueo);
		builder.append(", diff=");
		builder.append(diff);
		builder.append(", obs=");
		builder.append(obs);
		builder.append("]");
		return builder.toString();
	}

}
