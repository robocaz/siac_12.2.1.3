package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.util.List;

import cl.exe.ccs.dto.DepositoDTO;
import cl.exe.ccs.dto.ResponseDTO;

public interface DepositosDao {

	List<DepositoDTO> consultaDepositos(Integer codCC, String desde, String hasta);
	
	ResponseDTO guardaDeposito(DepositoDTO depo);
	
	ResponseDTO cerrarDeposito(BigDecimal corrCtaId);

}
