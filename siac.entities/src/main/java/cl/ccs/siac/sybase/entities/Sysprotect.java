package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysprotects database table.
 * 
 */
@Entity
@Table(name="sysprotects")
@NamedQuery(name="Sysprotect.findAll", query="SELECT s FROM Sysprotect s")
public class Sysprotect implements Serializable {
	private static final long serialVersionUID = 1L;

	private short action;

	private byte[] columns;

	private int grantor;

	private int id;

	private byte protecttype;

	private int uid;

	public Sysprotect() {
	}

	public short getAction() {
		return this.action;
	}

	public void setAction(short action) {
		this.action = action;
	}

	public byte[] getColumns() {
		return this.columns;
	}

	public void setColumns(byte[] columns) {
		this.columns = columns;
	}

	public int getGrantor() {
		return this.grantor;
	}

	public void setGrantor(int grantor) {
		this.grantor = grantor;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getProtecttype() {
		return this.protecttype;
	}

	public void setProtecttype(byte protecttype) {
		this.protecttype = protecttype;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}