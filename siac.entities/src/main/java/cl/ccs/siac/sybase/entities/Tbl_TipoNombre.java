package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoNombre database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoNombre.findAll", query="SELECT t FROM Tbl_TipoNombre t")
public class Tbl_TipoNombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoNombre")
	private String fld_GlosaTipoNombre;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoNombre")
	private boolean fld_TipoNombre;

	public Tbl_TipoNombre() {
	}

	public String getFld_GlosaTipoNombre() {
		return this.fld_GlosaTipoNombre;
	}

	public void setFld_GlosaTipoNombre(String fld_GlosaTipoNombre) {
		this.fld_GlosaTipoNombre = fld_GlosaTipoNombre;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public boolean getFld_TipoNombre() {
		return this.fld_TipoNombre;
	}

	public void setFld_TipoNombre(boolean fld_TipoNombre) {
		this.fld_TipoNombre = fld_TipoNombre;
	}

}