package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_SISTEMA database table.
 * 
 */
@Entity
@Table(name="TBL_SISTEMA")
@NamedQuery(name="TblSistema.findAll", query="SELECT t FROM TblSistema t")
public class TblSistema implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODRESPONSABLE")
	private BigDecimal fldCodresponsable;

	@Column(name="FLD_CODSISTEMA")
	private BigDecimal fldCodsistema;

	@Column(name="FLD_GLOSASISTEMA")
	private String fldGlosasistema;

	public TblSistema() {
	}

	public BigDecimal getFldCodresponsable() {
		return this.fldCodresponsable;
	}

	public void setFldCodresponsable(BigDecimal fldCodresponsable) {
		this.fldCodresponsable = fldCodresponsable;
	}

	public BigDecimal getFldCodsistema() {
		return this.fldCodsistema;
	}

	public void setFldCodsistema(BigDecimal fldCodsistema) {
		this.fldCodsistema = fldCodsistema;
	}

	public String getFldGlosasistema() {
		return this.fldGlosasistema;
	}

	public void setFldGlosasistema(String fldGlosasistema) {
		this.fldGlosasistema = fldGlosasistema;
	}

}