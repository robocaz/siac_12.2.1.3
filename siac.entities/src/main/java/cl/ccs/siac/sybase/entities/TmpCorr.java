package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_corr database table.
 * 
 */
@Entity
@Table(name="tmp_corr")
@NamedQuery(name="TmpCorr.findAll", query="SELECT t FROM TmpCorr t")
public class TmpCorr implements Serializable {
	private static final long serialVersionUID = 1L;

	private String texto;

	public TmpCorr() {
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}