package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditoriaListaNegra database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditoriaListaNegra.findAll", query="SELECT t FROM Tbl_AuditoriaListaNegra t")
public class Tbl_AuditoriaListaNegra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_CodUsuarioDigitacion")
	private String fld_CodUsuarioDigitacion;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrListaN")
	private BigDecimal fld_CorrListaN;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FecDeteccion")
	private Timestamp fld_FecDeteccion;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoRegistroLN")
	private byte fld_TipoRegistroLN;

	public Tbl_AuditoriaListaNegra() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public String getFld_CodUsuarioDigitacion() {
		return this.fld_CodUsuarioDigitacion;
	}

	public void setFld_CodUsuarioDigitacion(String fld_CodUsuarioDigitacion) {
		this.fld_CodUsuarioDigitacion = fld_CodUsuarioDigitacion;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrListaN() {
		return this.fld_CorrListaN;
	}

	public void setFld_CorrListaN(BigDecimal fld_CorrListaN) {
		this.fld_CorrListaN = fld_CorrListaN;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_FecDeteccion() {
		return this.fld_FecDeteccion;
	}

	public void setFld_FecDeteccion(Timestamp fld_FecDeteccion) {
		this.fld_FecDeteccion = fld_FecDeteccion;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoRegistroLN() {
		return this.fld_TipoRegistroLN;
	}

	public void setFld_TipoRegistroLN(byte fld_TipoRegistroLN) {
		this.fld_TipoRegistroLN = fld_TipoRegistroLN;
	}

}