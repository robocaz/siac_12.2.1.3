package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SORegControl database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SORegControl.findAll", query="SELECT t FROM Tbl_SORegControl t")
public class Tbl_SORegControl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ArchivoSO")
	private String fld_ArchivoSO;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioEjecucion")
	private String fld_CodUsuarioEjecucion;

	@Column(name="Fld_CorrCajaDesde")
	private BigDecimal fld_CorrCajaDesde;

	@Column(name="Fld_CorrCajaHasta")
	private BigDecimal fld_CorrCajaHasta;

	@Column(name="Fld_CorrOffline_Id")
	private BigDecimal fld_CorrOffline_Id;

	@Column(name="Fld_FecDesde")
	private Timestamp fld_FecDesde;

	@Column(name="Fld_FecGeneracion")
	private Timestamp fld_FecGeneracion;

	@Column(name="Fld_FecHasta")
	private Timestamp fld_FecHasta;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_TotalBoletas")
	private int fld_TotalBoletas;

	@Column(name="Fld_TotalCalculado")
	private BigDecimal fld_TotalCalculado;

	@Column(name="Fld_TotalFacturas")
	private int fld_TotalFacturas;

	@Column(name="Fld_TotalPagado")
	private BigDecimal fld_TotalPagado;

	@Column(name="Fld_TotMovimientos")
	private int fld_TotMovimientos;

	@Column(name="Fld_TotTransacciones")
	private int fld_TotTransacciones;

	@Column(name="Fld_Volumen")
	private byte fld_Volumen;

	public Tbl_SORegControl() {
	}

	public String getFld_ArchivoSO() {
		return this.fld_ArchivoSO;
	}

	public void setFld_ArchivoSO(String fld_ArchivoSO) {
		this.fld_ArchivoSO = fld_ArchivoSO;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioEjecucion() {
		return this.fld_CodUsuarioEjecucion;
	}

	public void setFld_CodUsuarioEjecucion(String fld_CodUsuarioEjecucion) {
		this.fld_CodUsuarioEjecucion = fld_CodUsuarioEjecucion;
	}

	public BigDecimal getFld_CorrCajaDesde() {
		return this.fld_CorrCajaDesde;
	}

	public void setFld_CorrCajaDesde(BigDecimal fld_CorrCajaDesde) {
		this.fld_CorrCajaDesde = fld_CorrCajaDesde;
	}

	public BigDecimal getFld_CorrCajaHasta() {
		return this.fld_CorrCajaHasta;
	}

	public void setFld_CorrCajaHasta(BigDecimal fld_CorrCajaHasta) {
		this.fld_CorrCajaHasta = fld_CorrCajaHasta;
	}

	public BigDecimal getFld_CorrOffline_Id() {
		return this.fld_CorrOffline_Id;
	}

	public void setFld_CorrOffline_Id(BigDecimal fld_CorrOffline_Id) {
		this.fld_CorrOffline_Id = fld_CorrOffline_Id;
	}

	public Timestamp getFld_FecDesde() {
		return this.fld_FecDesde;
	}

	public void setFld_FecDesde(Timestamp fld_FecDesde) {
		this.fld_FecDesde = fld_FecDesde;
	}

	public Timestamp getFld_FecGeneracion() {
		return this.fld_FecGeneracion;
	}

	public void setFld_FecGeneracion(Timestamp fld_FecGeneracion) {
		this.fld_FecGeneracion = fld_FecGeneracion;
	}

	public Timestamp getFld_FecHasta() {
		return this.fld_FecHasta;
	}

	public void setFld_FecHasta(Timestamp fld_FecHasta) {
		this.fld_FecHasta = fld_FecHasta;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public int getFld_TotalBoletas() {
		return this.fld_TotalBoletas;
	}

	public void setFld_TotalBoletas(int fld_TotalBoletas) {
		this.fld_TotalBoletas = fld_TotalBoletas;
	}

	public BigDecimal getFld_TotalCalculado() {
		return this.fld_TotalCalculado;
	}

	public void setFld_TotalCalculado(BigDecimal fld_TotalCalculado) {
		this.fld_TotalCalculado = fld_TotalCalculado;
	}

	public int getFld_TotalFacturas() {
		return this.fld_TotalFacturas;
	}

	public void setFld_TotalFacturas(int fld_TotalFacturas) {
		this.fld_TotalFacturas = fld_TotalFacturas;
	}

	public BigDecimal getFld_TotalPagado() {
		return this.fld_TotalPagado;
	}

	public void setFld_TotalPagado(BigDecimal fld_TotalPagado) {
		this.fld_TotalPagado = fld_TotalPagado;
	}

	public int getFld_TotMovimientos() {
		return this.fld_TotMovimientos;
	}

	public void setFld_TotMovimientos(int fld_TotMovimientos) {
		this.fld_TotMovimientos = fld_TotMovimientos;
	}

	public int getFld_TotTransacciones() {
		return this.fld_TotTransacciones;
	}

	public void setFld_TotTransacciones(int fld_TotTransacciones) {
		this.fld_TotTransacciones = fld_TotTransacciones;
	}

	public byte getFld_Volumen() {
		return this.fld_Volumen;
	}

	public void setFld_Volumen(byte fld_Volumen) {
		this.fld_Volumen = fld_Volumen;
	}

}