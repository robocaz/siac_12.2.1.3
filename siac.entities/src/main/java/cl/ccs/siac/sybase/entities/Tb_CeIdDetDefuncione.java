package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetDefunciones database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetDefunciones")
@NamedQuery(name="Tb_CeIdDetDefuncione.findAll", query="SELECT t FROM Tb_CeIdDetDefuncione t")
public class Tb_CeIdDetDefuncione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETDEFUNCIONES_FLD_CORRDEFUNCION_ID_GENERATOR", sequenceName="FLD_CORRDEFUNCION_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETDEFUNCIONES_FLD_CORRDEFUNCION_ID_GENERATOR")
	@Column(name="Fld_CorrDefuncion_Id")
	private long fld_CorrDefuncion_Id;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FecDefuncion")
	private Timestamp fld_FecDefuncion;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FlagObituarios")
	private boolean fld_FlagObituarios;

	@Column(name="Fld_FlagRegCivil")
	private boolean fld_FlagRegCivil;

	@Column(name="Fld_FlagRegElectoral")
	private boolean fld_FlagRegElectoral;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdSegtoDefuncione
	@OneToMany(mappedBy="tbCeIdDetDefuncione")
	private List<Tb_CeIdSegtoDefuncione> tbCeIdSegtoDefunciones;

	public Tb_CeIdDetDefuncione() {
	}

	public long getFld_CorrDefuncion_Id() {
		return this.fld_CorrDefuncion_Id;
	}

	public void setFld_CorrDefuncion_Id(long fld_CorrDefuncion_Id) {
		this.fld_CorrDefuncion_Id = fld_CorrDefuncion_Id;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_FecDefuncion() {
		return this.fld_FecDefuncion;
	}

	public void setFld_FecDefuncion(Timestamp fld_FecDefuncion) {
		this.fld_FecDefuncion = fld_FecDefuncion;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_FlagObituarios() {
		return this.fld_FlagObituarios;
	}

	public void setFld_FlagObituarios(boolean fld_FlagObituarios) {
		this.fld_FlagObituarios = fld_FlagObituarios;
	}

	public boolean getFld_FlagRegCivil() {
		return this.fld_FlagRegCivil;
	}

	public void setFld_FlagRegCivil(boolean fld_FlagRegCivil) {
		this.fld_FlagRegCivil = fld_FlagRegCivil;
	}

	public boolean getFld_FlagRegElectoral() {
		return this.fld_FlagRegElectoral;
	}

	public void setFld_FlagRegElectoral(boolean fld_FlagRegElectoral) {
		this.fld_FlagRegElectoral = fld_FlagRegElectoral;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public List<Tb_CeIdSegtoDefuncione> getTbCeIdSegtoDefunciones() {
		return this.tbCeIdSegtoDefunciones;
	}

	public void setTbCeIdSegtoDefunciones(List<Tb_CeIdSegtoDefuncione> tbCeIdSegtoDefunciones) {
		this.tbCeIdSegtoDefunciones = tbCeIdSegtoDefunciones;
	}

	public Tb_CeIdSegtoDefuncione addTbCeIdSegtoDefuncione(Tb_CeIdSegtoDefuncione tbCeIdSegtoDefuncione) {
		getTbCeIdSegtoDefunciones().add(tbCeIdSegtoDefuncione);
		tbCeIdSegtoDefuncione.setTbCeIdDetDefuncione(this);

		return tbCeIdSegtoDefuncione;
	}

	public Tb_CeIdSegtoDefuncione removeTbCeIdSegtoDefuncione(Tb_CeIdSegtoDefuncione tbCeIdSegtoDefuncione) {
		getTbCeIdSegtoDefunciones().remove(tbCeIdSegtoDefuncione);
		tbCeIdSegtoDefuncione.setTbCeIdDetDefuncione(null);

		return tbCeIdSegtoDefuncione;
	}

}