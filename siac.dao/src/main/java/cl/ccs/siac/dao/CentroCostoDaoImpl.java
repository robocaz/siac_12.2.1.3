package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("centroCostoDao")
@Stateless
public class CentroCostoDaoImpl implements CentroCostoDao {

	@PersistenceContext(unitName = "orcl-link")
	private EntityManager em;

	private static final Logger LOG = LogManager.getLogger(CentroCostoDaoImpl.class);
	
	public ResponseListDTO<ComboDTO> listar(String username) throws Exception {

		ResponseListDTO<ComboDTO> respuesta = new ResponseListDTO<ComboDTO>();
		List<ComboDTO> lista = new ArrayList<ComboDTO>();
		
		try{
			
			LOG.info("Ejecutando MOL.PKG_SIAC.SQ_CmbCCostoUs");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("MOL.PKG_SIAC.SQ_CmbCCostoUs");
			storedProcedure.registerStoredProcedureParameter("pUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pReturn", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pMensaje", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pRegistro", void.class, ParameterMode.REF_CURSOR);
	
			LOG.info("pUsuario " + username);
			
			storedProcedure.setParameter("pUsuario", username);
	
			boolean execute = storedProcedure.execute();
	
			
	
			respuesta.setIdControl(new Integer(storedProcedure.getOutputParameterValue("pReturn").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("pMensaje").toString());
	
			List<Object[]> listaCentroCosto = storedProcedure.getResultList();
			for (Object item : listaCentroCosto) {
				Object[] datos = (Object[]) item;
				lista.add(new ComboDTO(datos[0].toString(), datos[1].toString()));
			}
			
			respuesta.setIdControl(0);
			respuesta.setMsgControl("");
	//		lista.add(new ComboDTO("2510", "Arica"));
	//		lista.add(new ComboDTO("2530", "Antofagasta"));
			respuesta.setLista(lista);
	
			return respuesta;
		}
		 catch (Exception e) {
				LOG.error("Error en el SP MOL.PKG_SIAC.SQ_CmbCCostoUs " + e.getMessage());
				return respuesta;
		}

	}

	@Override
	public ResponseDTO modificar(String username, String codigoCentroCosto) throws Exception {
		
		try{
			
			LOG.info("Ejecutando MOL.PKG_SIAC.SP_GrabaCCostoUs");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("MOL.PKG_SIAC.SP_GrabaCCostoUs");
			storedProcedure.registerStoredProcedureParameter("pUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pReturn", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pMensaje", String.class, ParameterMode.OUT);
	
			LOG.info("pUsuario " + username);
			LOG.info("pCCosto " + codigoCentroCosto);

			
			storedProcedure.setParameter("pUsuario", username);
			storedProcedure.setParameter("pCCosto", new Integer(codigoCentroCosto));
	
			storedProcedure.execute();
	
			ResponseDTO respuesta = new ResponseDTO();
	
			respuesta.setIdControl(new Integer(storedProcedure.getOutputParameterValue("pReturn").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("pMensaje").toString());
	
			return respuesta;
		}
		 catch (Exception e) {
				LOG.error("Error en el SP MOL.PKG_SIAC.SP_GrabaCCostoUs " + e.getMessage());
				return null;
		}		
	}

}