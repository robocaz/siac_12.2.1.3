package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditCertNacimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditCertNacimiento.findAll", query="SELECT t FROM Tbl_AuditCertNacimiento t")
public class Tbl_AuditCertNacimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuarioIngreso")
	private String fld_CodUsuarioIngreso;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_Contenedor")
	private int fld_Contenedor;

	@Column(name="Fld_CorrCertificado")
	private BigDecimal fld_CorrCertificado;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Pagina")
	private byte fld_Pagina;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_AuditCertNacimiento() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioIngreso() {
		return this.fld_CodUsuarioIngreso;
	}

	public void setFld_CodUsuarioIngreso(String fld_CodUsuarioIngreso) {
		this.fld_CodUsuarioIngreso = fld_CodUsuarioIngreso;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public int getFld_Contenedor() {
		return this.fld_Contenedor;
	}

	public void setFld_Contenedor(int fld_Contenedor) {
		this.fld_Contenedor = fld_Contenedor;
	}

	public BigDecimal getFld_CorrCertificado() {
		return this.fld_CorrCertificado;
	}

	public void setFld_CorrCertificado(BigDecimal fld_CorrCertificado) {
		this.fld_CorrCertificado = fld_CorrCertificado;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public byte getFld_Pagina() {
		return this.fld_Pagina;
	}

	public void setFld_Pagina(byte fld_Pagina) {
		this.fld_Pagina = fld_Pagina;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}