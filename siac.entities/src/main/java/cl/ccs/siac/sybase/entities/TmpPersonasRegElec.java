package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmpPersonasRegElec database table.
 * 
 */
@Entity
@Table(name="tmpPersonasRegElec")
@NamedQuery(name="TmpPersonasRegElec.findAll", query="SELECT t FROM TmpPersonasRegElec t")
public class TmpPersonasRegElec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	public TmpPersonasRegElec() {
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

}