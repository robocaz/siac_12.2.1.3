package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RegCivilTransito database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RegCivilTransito.findAll", query="SELECT t FROM Tbl_RegCivilTransito t")
public class Tbl_RegCivilTransito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_RegCivilTransito() {
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}