package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DirFiniquito database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DirFiniquito.findAll", query="SELECT t FROM Tbl_DirFiniquito t")
public class Tbl_DirFiniquito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrDirFnq_Id")
	private BigDecimal fld_CorrDirFnq_Id;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDomicilio")
	private byte fld_TipoDomicilio;

	public Tbl_DirFiniquito() {
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrDirFnq_Id() {
		return this.fld_CorrDirFnq_Id;
	}

	public void setFld_CorrDirFnq_Id(BigDecimal fld_CorrDirFnq_Id) {
		this.fld_CorrDirFnq_Id = fld_CorrDirFnq_Id;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte getFld_TipoDomicilio() {
		return this.fld_TipoDomicilio;
	}

	public void setFld_TipoDomicilio(byte fld_TipoDomicilio) {
		this.fld_TipoDomicilio = fld_TipoDomicilio;
	}

}