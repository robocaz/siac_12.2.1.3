package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConsultaRangoBolFacDTO extends ResponseDTO implements Serializable{
	
	private Integer corrCaja;
	private String tipoDoc;
	private Integer codCCosto;
	private Integer rangoDesde;
	private Integer rangoHasta;

	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public Integer getCodCCosto() {
		return codCCosto;
	}
	public void setCodCCosto(Integer codCCosto) {
		this.codCCosto = codCCosto;
	}
	public Integer getRangoDesde() {
		return rangoDesde;
	}
	public void setRangoDesde(Integer rangoDesde) {
		this.rangoDesde = rangoDesde;
	}
	public Integer getRangoHasta() {
        return rangoHasta;
    }
    public void setRangoHasta(Integer rangoHasta) {
        this.rangoHasta = rangoHasta;
    }

}
