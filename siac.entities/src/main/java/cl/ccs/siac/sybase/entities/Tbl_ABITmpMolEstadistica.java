package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABITmpMolEstadistica database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABITmpMolEstadistica.findAll", query="SELECT t FROM Tbl_ABITmpMolEstadistica t")
public class Tbl_ABITmpMolEstadistica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_EmiLib")
	private String fld_EmiLib;

	@Column(name="Fld_FecEnvio")
	private Timestamp fld_FecEnvio;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_Moneda")
	private String fld_Moneda;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	public Tbl_ABITmpMolEstadistica() {
	}

	public String getFld_EmiLib() {
		return this.fld_EmiLib;
	}

	public void setFld_EmiLib(String fld_EmiLib) {
		this.fld_EmiLib = fld_EmiLib;
	}

	public Timestamp getFld_FecEnvio() {
		return this.fld_FecEnvio;
	}

	public void setFld_FecEnvio(Timestamp fld_FecEnvio) {
		this.fld_FecEnvio = fld_FecEnvio;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public String getFld_Moneda() {
		return this.fld_Moneda;
	}

	public void setFld_Moneda(String fld_Moneda) {
		this.fld_Moneda = fld_Moneda;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

}