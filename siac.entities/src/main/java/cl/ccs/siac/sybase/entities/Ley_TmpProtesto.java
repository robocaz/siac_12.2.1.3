package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_TmpProtestos database table.
 * 
 */
@Entity
@Table(name="Ley_TmpProtestos")
@NamedQuery(name="Ley_TmpProtesto.findAll", query="SELECT l FROM Ley_TmpProtesto l")
public class Ley_TmpProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CorrProt_Id")
	private BigDecimal fld_CorrProt_Id;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	public Ley_TmpProtesto() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrProt_Id() {
		return this.fld_CorrProt_Id;
	}

	public void setFld_CorrProt_Id(BigDecimal fld_CorrProt_Id) {
		this.fld_CorrProt_Id = fld_CorrProt_Id;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

}