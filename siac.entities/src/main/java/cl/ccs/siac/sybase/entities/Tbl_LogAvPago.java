package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_LogAvPago database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LogAvPago.findAll", query="SELECT t FROM Tbl_LogAvPago t")
public class Tbl_LogAvPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrAclBat")
	private BigDecimal fld_CorrAclBat;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	public Tbl_LogAvPago() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrAclBat() {
		return this.fld_CorrAclBat;
	}

	public void setFld_CorrAclBat(BigDecimal fld_CorrAclBat) {
		this.fld_CorrAclBat = fld_CorrAclBat;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

}