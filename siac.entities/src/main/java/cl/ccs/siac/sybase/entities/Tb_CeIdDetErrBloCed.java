package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tb_CeIdDetErrBloCed database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetErrBloCed.findAll", query="SELECT t FROM Tb_CeIdDetErrBloCed t")
public class Tb_CeIdDetErrBloCed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	//bi-directional many-to-one association to Tb_CeIdDetBloqueoCedula
	@ManyToOne
	@JoinColumn(name="Fld_CorrBloqueo")
	private Tb_CeIdDetBloqueoCedula tbCeIdDetBloqueoCedula;

	public Tb_CeIdDetErrBloCed() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public Tb_CeIdDetBloqueoCedula getTbCeIdDetBloqueoCedula() {
		return this.tbCeIdDetBloqueoCedula;
	}

	public void setTbCeIdDetBloqueoCedula(Tb_CeIdDetBloqueoCedula tbCeIdDetBloqueoCedula) {
		this.tbCeIdDetBloqueoCedula = tbCeIdDetBloqueoCedula;
	}

}