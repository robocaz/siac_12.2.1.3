package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ControlBoletaDTO extends ResponseDTO implements Serializable{
	
	private Integer inicioBoleta;
	private Integer finBoleta;
	private Integer actualBoleta;
	private Integer idCaja;
	private Integer idCentroCosto;
	private String nombreMaquina;
	private String codUsuario;
	private String ip;
	private String tipo;
	
	
	

	public Integer getIdCentroCosto() {
		return idCentroCosto;
	}
	public void setIdCentroCosto(Integer idCentroCosto) {
		this.idCentroCosto = idCentroCosto;
	}

	public Integer getInicioBoleta() {
		return inicioBoleta;
	}
	public void setInicioBoleta(Integer inicioBoleta) {
		this.inicioBoleta = inicioBoleta;
	}
	public Integer getFinBoleta() {
		return finBoleta;
	}
	public void setFinBoleta(Integer finBoleta) {
		this.finBoleta = finBoleta;
	}
	public Integer getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(Integer idCaja) {
		this.idCaja = idCaja;
	}
	public Integer getActualBoleta() {
		return actualBoleta;
	}
	public void setActualBoleta(Integer actualBoleta) {
		this.actualBoleta = actualBoleta;
	}
	public String getNombreMaquina() {
		return nombreMaquina;
	}
	public void setNombreMaquina(String nombreMaquina) {
		this.nombreMaquina = nombreMaquina;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	

}
