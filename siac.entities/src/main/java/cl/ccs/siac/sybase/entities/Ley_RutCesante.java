package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ley_RutCesantes database table.
 * 
 */
@Entity
@Table(name="Ley_RutCesantes")
@NamedQuery(name="Ley_RutCesante.findAll", query="SELECT l FROM Ley_RutCesante l")
public class Ley_RutCesante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Ley_RutCesante() {
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}