package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_EMISOR_CONVERSION database table.
 * 
 */
@Entity
@Table(name="TBL_EMISOR_CONVERSION")
@NamedQuery(name="TblEmisorConversion.findAll", query="SELECT t FROM TblEmisorConversion t")
public class TblEmisorConversion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODIGO")
	private String fldCodigo;

	@Column(name="FLD_EMPRESA")
	private String fldEmpresa;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblEmisorConversion() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public String getFldCodigo() {
		return this.fldCodigo;
	}

	public void setFldCodigo(String fldCodigo) {
		this.fldCodigo = fldCodigo;
	}

	public String getFldEmpresa() {
		return this.fldEmpresa;
	}

	public void setFldEmpresa(String fldEmpresa) {
		this.fldEmpresa = fldEmpresa;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}