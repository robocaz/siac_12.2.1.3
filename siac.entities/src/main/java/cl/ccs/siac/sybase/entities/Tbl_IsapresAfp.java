package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_IsapresAfp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_IsapresAfp.findAll", query="SELECT t FROM Tbl_IsapresAfp t")
public class Tbl_IsapresAfp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Codigo")
	private short fld_Codigo;

	@Column(name="Fld_Glosa")
	private String fld_Glosa;

	@Column(name="Fld_Tipo")
	private byte fld_Tipo;

	public Tbl_IsapresAfp() {
	}

	public short getFld_Codigo() {
		return this.fld_Codigo;
	}

	public void setFld_Codigo(short fld_Codigo) {
		this.fld_Codigo = fld_Codigo;
	}

	public String getFld_Glosa() {
		return this.fld_Glosa;
	}

	public void setFld_Glosa(String fld_Glosa) {
		this.fld_Glosa = fld_Glosa;
	}

	public byte getFld_Tipo() {
		return this.fld_Tipo;
	}

	public void setFld_Tipo(byte fld_Tipo) {
		this.fld_Tipo = fld_Tipo;
	}

}