package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_LibradorRectif database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LibradorRectif.findAll", query="SELECT t FROM Tbl_LibradorRectif t")
public class Tbl_LibradorRectif implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrLibrRect_Id")
	private BigDecimal fld_CorrLibrRect_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrProtRectif")
	private BigDecimal fld_CorrProtRectif;

	@Column(name="Fld_Entero")
	private int fld_Entero;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	public Tbl_LibradorRectif() {
	}

	public BigDecimal getFld_CorrLibrRect_Id() {
		return this.fld_CorrLibrRect_Id;
	}

	public void setFld_CorrLibrRect_Id(BigDecimal fld_CorrLibrRect_Id) {
		this.fld_CorrLibrRect_Id = fld_CorrLibrRect_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrProtRectif() {
		return this.fld_CorrProtRectif;
	}

	public void setFld_CorrProtRectif(BigDecimal fld_CorrProtRectif) {
		this.fld_CorrProtRectif = fld_CorrProtRectif;
	}

	public int getFld_Entero() {
		return this.fld_Entero;
	}

	public void setFld_Entero(int fld_Entero) {
		this.fld_Entero = fld_Entero;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

}