package cl.ccs.siac.utils;

import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

import javax.ws.rs.core.Response;

import cl.ccs.siac.exceptions.ReportNotCreatedException;
import cl.ccs.siac.exceptions.ReportNotFoundException;
import net.sf.jasperreports.engine.JRException;

public interface GeneradorReporte {

	public static GeneradorPDF GENERADOR_PDF = new GeneradorPDF();

	/**
	 * Método que genera un reporte a partir de una plantilla, una colección de
	 * datos y parámetros.
	 * 
	 * @param plantilla
	 *            Nombre de la plantilla.
	 * @param data
	 *            Colección de objetos de donde se consumen los Field del
	 *            reporte.
	 * @return Arreglo de bytes que representa el archivo generado.
	 * @throws ReportNotFoundException
	 *             Si la plantilla entregada no se encuentra.
	 * @throws ReportNotCreatedException
	 *             Si ocurre algún error al generar el reporte.
	 */
	byte[] create(String plantilla, Collection<?> data) throws ReportNotFoundException, ReportNotCreatedException;

	/**
	 * Método que genera un reporte a partir de una plantilla, una colección de
	 * datos y parámetros.
	 * 
	 * @param plantilla
	 *            Nombre de la plantilla.
	 * @param data
	 *            Colección de objetos de donde se consumen los Field del
	 *            reporte.
	 * @param parametros
	 *            Mapa de parámetros de donde se consumen los Parameters del
	 *            reporte.
	 * @return Arreglo de bytes que representa el archivo generado.
	 * @throws ReportNotFoundException
	 *             Si la plantilla entregada no se encuentra.
	 * @throws ReportNotCreatedException
	 *             Si ocurre algún error al generar el reporte.
	 */
	byte[] create(String plantilla, Collection<?> data, Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException;

	/**
	 * Método que genera un reporte a partir de una plantilla, una colección de
	 * datos y parámetros.
	 * 
	 * @param plantilla
	 *            Nombre de la plantilla.
	 * @param data
	 *            Colección de objetos de donde se consumen los Field del
	 *            reporte.
	 * @param parametros
	 *            Mapa de parámetros de donde se consumen los Parameters del
	 *            reporte.
	 * @return Arreglo de bytes que representa el archivo generado.
	 * @throws ReportNotFoundException
	 *             Si la plantilla entregada no se encuentra.
	 * @throws ReportNotCreatedException
	 *             Si ocurre algún error al generar el reporte.
	 */
	byte[] create(String plantilla, Map<String, Object> parametros, Connection conexion)
			throws ReportNotFoundException, ReportNotCreatedException;

	/**
	 * Método que crea un ResponseBuilder con el reporte atachado. El nombre del
	 * archivo a ser descargado es el nombre de la plantilla.
	 * 
	 * @param plantilla
	 *            Nombre de la plantilla.
	 * @param data
	 *            Colección de objetos de donde se consumen los Field del
	 *            reporte.
	 * @param parametros
	 *            Mapa de parámetros de donde se consumen los Parameters del
	 *            reporte.
	 * @return ResponseBuilder con las cabaceras seteadas con el reporte.
	 * @throws ReportNotFoundException
	 *             Si la plantilla entregada no se encuentra.
	 * @throws ReportNotCreatedException
	 *             Si ocurre algún error al generar el reporte.
	 */
	Response createReportResponse(String plantilla, Collection<?> data, Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException;

	/**
	 * Método que crea un ResponseBuilder con el reporte atachado. El nombre del
	 * archivo a ser descargado es el nombre de la plantilla.
	 * 
	 * @param plantilla
	 *            Nombre de la plantilla.
	 * @param data
	 *            Colección de objetos de donde se consumen los Field del
	 *            reporte.
	 * @return ResponseBuilder con las cabaceras seteadas con el reporte.
	 * @throws ReportNotFoundException
	 *             Si la plantilla entregada no se encuentra.
	 * @throws ReportNotCreatedException
	 *             Si ocurre algún error al generar el reporte.
	 */
	Response createReportResponse(String plantilla, Collection<?> data)
			throws ReportNotFoundException, ReportNotCreatedException;

	Response createReportResponseBlock(String plantilla, Collection<?> data, Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException, JRException;

	byte[] createBlock(String plantilla, Collection<?> data, Map<String, Object> parametros)
			throws ReportNotFoundException, ReportNotCreatedException, JRException;

	Response createReportResponseBlock(String plantilla, Collection<?> data)
			throws ReportNotFoundException, ReportNotCreatedException, JRException;

}
