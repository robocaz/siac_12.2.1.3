package cl.exe.ccs.dto;

import java.io.Serializable;

public class MovimientoDTO implements Serializable{
	
	private String glosaServicio;
	private Integer boletaMovimiento;
	private Double montoTransaccionBoleta;
	private Double montoCantidadBoleta;
	private Double diferenciaBoleta;
	private Double facturaMovimientos;
	private Double montoTransaccionFactura;
	private Double montoCantidadFactura;
	private Double diferenciaFactura;
	
	
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public Integer getBoletaMovimiento() {
		return boletaMovimiento;
	}
	public void setBoletaMovimiento(Integer boletaMovimiento) {
		this.boletaMovimiento = boletaMovimiento;
	}
	public Double getMontoTransaccionBoleta() {
		return montoTransaccionBoleta;
	}
	public void setMontoTransaccionBoleta(Double montoTransaccionBoleta) {
		this.montoTransaccionBoleta = montoTransaccionBoleta;
	}
	public Double getMontoCantidadBoleta() {
		return montoCantidadBoleta;
	}
	public void setMontoCantidadBoleta(Double montoCantidadBoleta) {
		this.montoCantidadBoleta = montoCantidadBoleta;
	}
	public Double getDiferenciaBoleta() {
		return diferenciaBoleta;
	}
	public void setDiferenciaBoleta(Double diferenciaBoleta) {
		this.diferenciaBoleta = diferenciaBoleta;
	}
	public Double getFacturaMovimientos() {
		return facturaMovimientos;
	}
	public void setFacturaMovimientos(Double facturaMovimientos) {
		this.facturaMovimientos = facturaMovimientos;
	}
	public Double getMontoTransaccionFactura() {
		return montoTransaccionFactura;
	}
	public void setMontoTransaccionFactura(Double montoTransaccionFactura) {
		this.montoTransaccionFactura = montoTransaccionFactura;
	}
	public Double getMontoCantidadFactura() {
		return montoCantidadFactura;
	}
	public void setMontoCantidadFactura(Double montoCantidadFactura) {
		this.montoCantidadFactura = montoCantidadFactura;
	}
	public Double getDiferenciaFactura() {
		return diferenciaFactura;
	}
	public void setDiferenciaFactura(Double diferenciaFactura) {
		this.diferenciaFactura = diferenciaFactura;
	}
	
	
	
	
	

}
