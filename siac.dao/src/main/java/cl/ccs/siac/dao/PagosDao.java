package cl.ccs.siac.dao;

import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;

public interface PagosDao {
	
	public ResponseDTO actualizaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion);

	public Long consultaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion);
	
	public ResponseDTO actualizaMedioPagoTemp(BoletaTransaccionDTO boletaTransaccion);
	
	public ResponseDTO anularBoleta(BoletaTransaccionDTO boletaTransaccion);
	
	public ResponseDTO anulacionRegistroNuevo(Long idCorrCaja, Long corrTransaccion);
	
	public ResponseDTO valorServicio(String codServicio);
}
