package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolicitudCorreccion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolicitudCorreccion.findAll", query="SELECT t FROM Tbl_SolicitudCorreccion t")
public class Tbl_SolicitudCorreccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_BolPubCorr")
	private short fld_BolPubCorr;

	@Column(name="Fld_CausaRechazo")
	private String fld_CausaRechazo;

	@Column(name="Fld_CodAgencia")
	private short fld_CodAgencia;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodTipoCorr")
	private byte fld_CodTipoCorr;

	@Column(name="Fld_CorrProtesto")
	private BigDecimal fld_CorrProtesto;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_FecEstadoCorr")
	private Timestamp fld_FecEstadoCorr;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecPubCorr")
	private Timestamp fld_FecPubCorr;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_FolioInterno")
	private BigDecimal fld_FolioInterno;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_MotivoCorreccion")
	private String fld_MotivoCorreccion;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_NroBoletinProt")
	private short fld_NroBoletinProt;

	@Column(name="Fld_NroBoletinRec")
	private short fld_NroBoletinRec;

	@Column(name="Fld_NroOperacion")
	private short fld_NroOperacion;

	@Column(name="Fld_PagBoletinAcl")
	private short fld_PagBoletinAcl;

	@Column(name="Fld_PagBoletinProt")
	private short fld_PagBoletinProt;

	@Column(name="Fld_PagBoletinRec")
	private short fld_PagBoletinRec;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoModificacion")
	private byte fld_TipoModificacion;

	@Column(name="Fld_UsuarioCorr")
	private String fld_UsuarioCorr;

	@Column(name="Fld_UsuarioRecepcion")
	private String fld_UsuarioRecepcion;

	public Tbl_SolicitudCorreccion() {
	}

	public short getFld_BolPubCorr() {
		return this.fld_BolPubCorr;
	}

	public void setFld_BolPubCorr(short fld_BolPubCorr) {
		this.fld_BolPubCorr = fld_BolPubCorr;
	}

	public String getFld_CausaRechazo() {
		return this.fld_CausaRechazo;
	}

	public void setFld_CausaRechazo(String fld_CausaRechazo) {
		this.fld_CausaRechazo = fld_CausaRechazo;
	}

	public short getFld_CodAgencia() {
		return this.fld_CodAgencia;
	}

	public void setFld_CodAgencia(short fld_CodAgencia) {
		this.fld_CodAgencia = fld_CodAgencia;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_CodTipoCorr() {
		return this.fld_CodTipoCorr;
	}

	public void setFld_CodTipoCorr(byte fld_CodTipoCorr) {
		this.fld_CodTipoCorr = fld_CodTipoCorr;
	}

	public BigDecimal getFld_CorrProtesto() {
		return this.fld_CorrProtesto;
	}

	public void setFld_CorrProtesto(BigDecimal fld_CorrProtesto) {
		this.fld_CorrProtesto = fld_CorrProtesto;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public Timestamp getFld_FecEstadoCorr() {
		return this.fld_FecEstadoCorr;
	}

	public void setFld_FecEstadoCorr(Timestamp fld_FecEstadoCorr) {
		this.fld_FecEstadoCorr = fld_FecEstadoCorr;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecPubCorr() {
		return this.fld_FecPubCorr;
	}

	public void setFld_FecPubCorr(Timestamp fld_FecPubCorr) {
		this.fld_FecPubCorr = fld_FecPubCorr;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public BigDecimal getFld_FolioInterno() {
		return this.fld_FolioInterno;
	}

	public void setFld_FolioInterno(BigDecimal fld_FolioInterno) {
		this.fld_FolioInterno = fld_FolioInterno;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_MotivoCorreccion() {
		return this.fld_MotivoCorreccion;
	}

	public void setFld_MotivoCorreccion(String fld_MotivoCorreccion) {
		this.fld_MotivoCorreccion = fld_MotivoCorreccion;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public short getFld_NroBoletinProt() {
		return this.fld_NroBoletinProt;
	}

	public void setFld_NroBoletinProt(short fld_NroBoletinProt) {
		this.fld_NroBoletinProt = fld_NroBoletinProt;
	}

	public short getFld_NroBoletinRec() {
		return this.fld_NroBoletinRec;
	}

	public void setFld_NroBoletinRec(short fld_NroBoletinRec) {
		this.fld_NroBoletinRec = fld_NroBoletinRec;
	}

	public short getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(short fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public short getFld_PagBoletinAcl() {
		return this.fld_PagBoletinAcl;
	}

	public void setFld_PagBoletinAcl(short fld_PagBoletinAcl) {
		this.fld_PagBoletinAcl = fld_PagBoletinAcl;
	}

	public short getFld_PagBoletinProt() {
		return this.fld_PagBoletinProt;
	}

	public void setFld_PagBoletinProt(short fld_PagBoletinProt) {
		this.fld_PagBoletinProt = fld_PagBoletinProt;
	}

	public short getFld_PagBoletinRec() {
		return this.fld_PagBoletinRec;
	}

	public void setFld_PagBoletinRec(short fld_PagBoletinRec) {
		this.fld_PagBoletinRec = fld_PagBoletinRec;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoModificacion() {
		return this.fld_TipoModificacion;
	}

	public void setFld_TipoModificacion(byte fld_TipoModificacion) {
		this.fld_TipoModificacion = fld_TipoModificacion;
	}

	public String getFld_UsuarioCorr() {
		return this.fld_UsuarioCorr;
	}

	public void setFld_UsuarioCorr(String fld_UsuarioCorr) {
		this.fld_UsuarioCorr = fld_UsuarioCorr;
	}

	public String getFld_UsuarioRecepcion() {
		return this.fld_UsuarioRecepcion;
	}

	public void setFld_UsuarioRecepcion(String fld_UsuarioRecepcion) {
		this.fld_UsuarioRecepcion = fld_UsuarioRecepcion;
	}

}