package cl.ccs.siac.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("pagosDao")
@Stateless
public class PagosDaoImpl implements PagosDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(PagosDaoImpl.class);

	public ResponseDTO actualizaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			log.info("Ejecutando SU_ActBoletaFacturaTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_ActBoletaFacturaTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_ATerceros", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", boletaTransaccion.getIdCaja());
			storedProcedure.setParameter("Fld_BoletaFactura", boletaTransaccion.getTipoDocumento());
			storedProcedure.setParameter("Fld_NroBoletaFactura", boletaTransaccion.getNumeroBoleta());
			if(boletaTransaccion.getTercero() != null && boletaTransaccion.getTercero().equals("1")){
				storedProcedure.setParameter("Fld_ATerceros", 1);
			}
			if(boletaTransaccion.getTercero() == null || boletaTransaccion.getTercero().equals("")){
				storedProcedure.setParameter("Fld_ATerceros", 0);
			}
			storedProcedure.setParameter("Fld_RutAfectado", boletaTransaccion.getRutAfectado());
			storedProcedure.setParameter("Fld_RutTramitante", boletaTransaccion.getRutTramitante());

			storedProcedure.execute();

			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

		} catch (Exception e) {
			log.error("Error en el SP SU_ActBoletaFacturaTmp_SW2 " + e.getMessage());
			e.printStackTrace();

		}
		return respuesta;

	}

	public Long consultaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion) {

		try{
		
			log.info("Ejecutando SQ_ListaTransacTemp_SW2");
			
			Long numeroBoleta = new Long(0);
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ListaTransacTemp_SW2");
	
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEstadoTransac", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
	
			storedProcedure.setParameter("Fld_CorrCaja", boletaTransaccion.getIdCaja());
			storedProcedure.setParameter("Fld_CodEstadoTransac", 0);
			storedProcedure.setParameter("Fld_BoletaFactura", boletaTransaccion.getTipoDocumento());
			storedProcedure.setParameter("Fld_RutAfectado", boletaTransaccion.getRutAfectado());
	
			storedProcedure.execute();
	
			List<Object[]> boletas = storedProcedure.getResultList();
	
			for (Object[] item : boletas) {
				numeroBoleta = Long.parseLong(item[2].toString());
			}
	
			return numeroBoleta;
		}
		catch (Exception e) {
			log.error("Error en el SP SQ_ListaTransacTemp_SW2 " + e.getMessage());
			return null;
		}			
	}

	public ResponseDTO actualizaMedioPagoTemp(BoletaTransaccionDTO boletaTransaccion) {

		
		try{
		
			ResponseDTO respuesta = new ResponseDTO();
			
			log.info("Ejecutando SU_TipoPagoTempTransac_SW2");
						
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_TipoPagoTempTransac_SW2");
	
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCheque", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodBanco", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodSucursal", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("Fld_CorrCaja", boletaTransaccion.getIdCaja());
			storedProcedure.setParameter("Fld_TipoPago", boletaTransaccion.getMedioPago().getCodigo());
			if (boletaTransaccion.getNumerocheque() == null) {
				storedProcedure.setParameter("Fld_NroCheque", "");
			} else {
				storedProcedure.setParameter("Fld_NroCheque", boletaTransaccion.getNumerocheque());
			}
			if (boletaTransaccion.getBanco() == null) {
				storedProcedure.setParameter("Fld_CodBanco", "");
			} else {
				storedProcedure.setParameter("Fld_CodBanco", boletaTransaccion.getBanco().getCodigo());
			}
			if (boletaTransaccion.getSucursal() == null) {
				storedProcedure.setParameter("Fld_CodSucursal", 0);
			} else {
				storedProcedure.setParameter("Fld_CodSucursal", boletaTransaccion.getSucursal().getCodSucursal());
			}
			storedProcedure.execute();
	
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SU_TipoPagoTempTransac_SW2 " + e.getMessage());
			return null;
		}			
	}

	public ResponseDTO anularBoleta(BoletaTransaccionDTO boletaTransaccion) {

		try{
			ResponseDTO respuesta = new ResponseDTO();
	
			log.info("Ejecutando SU_AnulaBoleta_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_AnulaBoleta_SW2");
	
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("Fld_CorrCaja", boletaTransaccion.getIdCaja());
			storedProcedure.setParameter("Fld_BoletaFactura", boletaTransaccion.getTipoDocumento());
			storedProcedure.setParameter("Fld_NroBoletaFactura", boletaTransaccion.getNumeroBoleta());
	
			storedProcedure.execute();
	
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SU_AnulaBoleta_SW2 " + e.getMessage());
			return null;
		}		
		
	}

	public ResponseDTO anulacionRegistroNuevo(Long idCorrCaja, Long corrTransaccion) {
		
		try{
			
			log.info("Ejecutando SD_BolTrasacTemp_SW2");
			
			ResponseDTO respuesta = new ResponseDTO();
	
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_BolTrasacTemp_SW2");
	
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTempTransac", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
	
			storedProcedure.setParameter("Fld_CorrCaja", idCorrCaja);
			storedProcedure.setParameter("Fld_CorrTempTransac", corrTransaccion);
	
			storedProcedure.execute();
	
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
	
			return respuesta;
		}
		catch (Exception e) {
			log.error("Error en el SP SD_BolTrasacTemp_SW2 " + e.getMessage());
			return null;
		}		
	}

	
	
	public ResponseDTO valorServicio(String codServicio) {
		ResponseDTO respuesta = new ResponseDTO();

		try {
			
			log.info("Ejecutando SQ_ValorServicio_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValorServicio_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CostoServicio", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CodServicio", codServicio);

			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			if (respuesta.getIdControl() == 0) {
				respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_CostoServicio").toString().trim());
			}
		} catch (Exception e) {
			respuesta.setIdControl(1);
			log.error("Error en el SP SQ_ValorServicio_SW2 " + e.getMessage());
			e.printStackTrace();
		}

		return respuesta;
	}

}
