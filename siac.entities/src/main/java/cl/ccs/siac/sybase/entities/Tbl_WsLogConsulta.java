package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_WsLogConsultas database table.
 * 
 */
@Entity
@Table(name="Tbl_WsLogConsultas")
@NamedQuery(name="Tbl_WsLogConsulta.findAll", query="SELECT t FROM Tbl_WsLogConsulta t")
public class Tbl_WsLogConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrConsulta_Id")
	private BigDecimal fld_CorrConsulta_Id;

	@Column(name="Fld_FecConsulta")
	private Timestamp fld_FecConsulta;

	@Column(name="Fld_NroDocumentos")
	private short fld_NroDocumentos;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_RutUsuario")
	private String fld_RutUsuario;

	@Column(name="Fld_WebService")
	private String fld_WebService;

	public Tbl_WsLogConsulta() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrConsulta_Id() {
		return this.fld_CorrConsulta_Id;
	}

	public void setFld_CorrConsulta_Id(BigDecimal fld_CorrConsulta_Id) {
		this.fld_CorrConsulta_Id = fld_CorrConsulta_Id;
	}

	public Timestamp getFld_FecConsulta() {
		return this.fld_FecConsulta;
	}

	public void setFld_FecConsulta(Timestamp fld_FecConsulta) {
		this.fld_FecConsulta = fld_FecConsulta;
	}

	public short getFld_NroDocumentos() {
		return this.fld_NroDocumentos;
	}

	public void setFld_NroDocumentos(short fld_NroDocumentos) {
		this.fld_NroDocumentos = fld_NroDocumentos;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public String getFld_RutUsuario() {
		return this.fld_RutUsuario;
	}

	public void setFld_RutUsuario(String fld_RutUsuario) {
		this.fld_RutUsuario = fld_RutUsuario;
	}

	public String getFld_WebService() {
		return this.fld_WebService;
	}

	public void setFld_WebService(String fld_WebService) {
		this.fld_WebService = fld_WebService;
	}

}