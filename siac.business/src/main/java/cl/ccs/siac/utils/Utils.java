package cl.ccs.siac.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Named;

@Named("utils")
public class Utils {

	public static final String TIMESTAMP_FORMATO = "yyyy-MM-dd HH:mm:ss.S";
	public static final String FECHA_FORMATO_DEFAULT = "dd-MM-yyyy";
	public static final String FECHA_FORMATO_VISTA = "dd/MM/yyyy";
	public static final String FECHA_FORMATO_HORA_VISTA = "dd/MM/yyyy HH:mm:ss";
	public static final String DEFAULT_STR_DATE = "1900-01-01";
	public static final String DEFAULT_STR_TIMESTAMP = "1900-01-01 00:00:00";
	public static final String DEFAULT_ADD_STR_TIME = " 00:00:00";

	public String formatearRUT(String rut) {

		int cont = 0;
		String format;
		rut = rut.replace(".", "");
		rut = rut.replace("-", "");
		format = "-" + rut.substring(rut.length() - 1);
		for (int i = rut.length() - 2; i >= 0; i--) {
			format = rut.substring(i, i + 1) + format;
			cont++;
			if (cont == 3 && i != 0) {
				format = "." + format;
				cont = 0;
			}
		}
		return format;
	}

	/**
	 * Formatea el Rut ingresado, sin ceros a las izquierda
	 * @param rut
	 * @return rut formateado (ej.: 2.305.450-7).
	 */
	public String formateaRUTSin0(String strRut) {
		if ( strRut != null && !strRut.isEmpty() ) {
			strRut = strRut.replaceFirst("^0*", "");
			return formatearRUT(strRut);
		}
		return strRut;
	}

	/**
	 * Formatea un rut dado en formato 00xxxxxxx-x, o sea, sin puntos, con guion y con ceros a la izquierda para que el largo sea 11.
	 * @param rut
	 * @return rut formateado (ej.: 002305450-7).
	 */
	public String formatZeroRut(String rut) {
		if (rut != null && !rut.isEmpty()) {
			rut = formatearRUT(rut);
			rut = rut.replace(".", "");
			rut = String.format("%11s", rut).replace(' ', '0');
		}
		
		return rut;
	}
	
	/**
	 * Formatea una fecha dado una fecha
	 * @param fecha
	 * @return fecha formateada (ej.: 28/11/2017).
	 */
	public String formatearFecha(Date fecha) {
		return new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	}

	/**
	 * Formatea una fecha dado una timestamp (Long)
	 * @param fecha
	 * @return fecha formateada (ej.: 28/11/2017).
	 */
	public String formatearFecha(Long fecha) { return new SimpleDateFormat("dd/MM/yyyy").format(new Date(fecha)); }
	
	/**
	 * Formatea una fecha dado una cadena datetime yyyy-MM-dd HH:mm:ss.SSS
	 * @param fecha
	 * @return fecha formateada (ej.: 28/11/2017).
	 */
	public String formatearFecha(String fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date laFecha = new Date();
		try {
			laFecha = formatter.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(laFecha);
	}

	
	/**
	 * Formatea una fecha dado una cadena datetime yyyy-MM-dd HH:mm:ss.SSS
	 * @param fecha
	 * @return fecha formateada (ej.: 28/11/2017).
	 */
	public Date formatearFechaFromForm(String fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		Date laFecha = new Date();
		try {
			if(!fecha.equalsIgnoreCase(""))
				laFecha = formatter.parse(fecha);
			
		} catch (ParseException e) {
			e.getLocalizedMessage();
			return null;
			//e.printStackTrace();
		}
		new SimpleDateFormat("dd/MM/yyyy").format(laFecha);
		
		return laFecha;
	}
	
	/**
	 * Formatea el String desde el formato de entrada al formato del salida
	 * @param strFecha "formatIn"
	 * @return strFecha "formatOut"
	 */
	public String parseFormatFechaStr(String strFecha, String formatIn, String formatOut) {
		final SimpleDateFormat parseador = new SimpleDateFormat(formatIn);
		final SimpleDateFormat formateador = new SimpleDateFormat(formatOut);
		Date date = null;
		try {
			date = parseador.parse(strFecha);
		} catch (ParseException e) {
			return null;
		}
		return formateador.format(date);
	}
	
	/**
	 * Formatea el String de Timestamp al formato de fecha utilizado en las vistas
	 * @param strTimestamp "yyyy-MM-dd 00:00:00"
	 * @return strFecha "dd-MM-yyyy"
	 */
	public String formatTimestampStrToFechaStr(String strTs, String formato) {
		return parseFormatFechaStr(strTs, TIMESTAMP_FORMATO, formato);
	}
	
	/**
	 * Retorna el String por defecto de un Timestamp para la BD
	 * @return String "1900-01-01 00:00:00"
	 */
	public String getDefaultTimestampStr() {
		return DEFAULT_STR_TIMESTAMP;
	}
	
	/**
	 * Agrega el formato de tiempo al String ingresado
	 * @param strFecha
	 * @return strFecha + " 00:00:00"
	 */
	public String addTimeStrFormat(String strFecha) {
		if ( strFecha != null && !strFecha.isEmpty() ) {
			return strFecha.trim() + DEFAULT_ADD_STR_TIME;
		}
		return null;
	}
	
	/**
	 * Revisa el timestamp ingresado para retornar un String con formato fecha dd-MM-yyyy
	 * Si no aplica, retorna un String vacio
	 */
	public String checkTimestampToStrFecha(String strTs) {
		if ( strTs != null && strTs.length() > 10 ) {
			final String tmp = strTs.substring(0, 10);
			if ( !tmp.equals(DEFAULT_STR_DATE) ) {
				return formatTimestampStrToFechaStr(strTs, FECHA_FORMATO_DEFAULT);
			}
		}
		return new String();
	}
	
	/**
	 * Revisa el timestamp ingresado para retornar un String con formato fecha dd/MM/yyyy
	 * Si no aplica, retorna un String vacio
	 */
	public String checkTimestampToStrFechaVista(String strTs) {
		if ( strTs != null && strTs.length() > 10 ) {
			final String tmp = strTs.substring(0, 10);
			if ( !tmp.equals(DEFAULT_STR_DATE) ) {
				return formatTimestampStrToFechaStr(strTs, FECHA_FORMATO_VISTA);
			}
		}
		return new String();
	}
	
	/**
	 * Revisa el timestamp ingresado para retornar un String con formato fecha dd/MM/yyyy HH:mm:ss
	 * Si no aplica, retorna un String vacio
	 */
	public String checkTimestampToStrFechaHoraVista(String strTs) {
		if ( strTs != null && strTs.length() > 10 ) {
			final String tmp = strTs.substring(0, 10);
			if ( !tmp.equals(DEFAULT_STR_DATE) ) {
				return formatTimestampStrToFechaStr(strTs, FECHA_FORMATO_HORA_VISTA);
			}
		}
		return new String();
	}
	
	/**
	 * Revisa la fecha con formato "yyyyMM" y retorna String con formato "MM/yyyy"
	 * Si no aplica, retorna un String vacio
	 */
	public String checkFechaStrToMesAnioStr(String strFecha) {
		if ( strFecha != null && strFecha.length() == 6 ) {
			return parseFormatFechaStr(strFecha, "yyyyMM", "MM/yyyy");
		}
		return new String();
	}
	
	public String getHora(){
		Date date = new Date();
		//Caso 1: obtener la hora y salida por pantalla con formato:
		DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
		return hourFormat.format(date);

	}
	
	
	/** Obtiene precio en palabras **/
	public String getTextoValor(String valor){
		String texto = "";
		if(valor.equals("1000")){
			texto =  "MIL PESOS";
		}
		if(valor.equals("10000")){
			texto =  "(DIEZ MIL PESOS)";
		}
		if(valor.equals("8000")){
			texto =  "(OCHO MIL PESOS)";
		}	
		if(valor.equals("5000")){
			texto =  "(CINCO MIL PESOS)";
		}
		return texto;
		
		
		
		
		
		
	}
}
