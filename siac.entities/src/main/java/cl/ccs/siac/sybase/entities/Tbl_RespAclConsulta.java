package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RespAclConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RespAclConsulta.findAll", query="SELECT t FROM Tbl_RespAclConsulta t")
public class Tbl_RespAclConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodResponsable")
	private byte fld_CodResponsable;

	@Column(name="Fld_GlosaResponsable")
	private String fld_GlosaResponsable;

	public Tbl_RespAclConsulta() {
	}

	public byte getFld_CodResponsable() {
		return this.fld_CodResponsable;
	}

	public void setFld_CodResponsable(byte fld_CodResponsable) {
		this.fld_CodResponsable = fld_CodResponsable;
	}

	public String getFld_GlosaResponsable() {
		return this.fld_GlosaResponsable;
	}

	public void setFld_GlosaResponsable(String fld_GlosaResponsable) {
		this.fld_GlosaResponsable = fld_GlosaResponsable;
	}

}