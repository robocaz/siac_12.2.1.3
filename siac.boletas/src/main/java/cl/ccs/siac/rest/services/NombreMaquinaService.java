package cl.ccs.siac.rest.services;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cl.ccs.siac.dto.ArchivoBoleta;

@Path("/siac")
public class NombreMaquinaService {

	
    @GET
    @Path("/boletas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerNombreMaquina() throws UnknownHostException {
    	
    	
        //Obtener nombre y direccion IP del equipo local juntos
        InetAddress direccion = InetAddress.getLocalHost();
        System.out.println ("Localhost = "+direccion);

        //Obtener nombre y direccion Ip del equipo local por separado
        System.out.println("HostAddress " + direccion.getHostAddress());
        System.out.println("HostName " + direccion.getHostName());

        ArchivoBoleta archivo = new ArchivoBoleta();
        archivo.setNombreEquipo(direccion.getHostName());
        return Response.ok().entity(archivo).build();
        
    }
}
