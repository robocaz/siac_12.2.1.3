package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdSegtoDefunciones database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdSegtoDefunciones")
@NamedQuery(name="Tb_CeIdSegtoDefuncione.findAll", query="SELECT t FROM Tb_CeIdSegtoDefuncione t")
public class Tb_CeIdSegtoDefuncione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdDetDefuncione
	@ManyToOne
	@JoinColumn(name="Fld_CorrDefuncion")
	private Tb_CeIdDetDefuncione tbCeIdDetDefuncione;

	public Tb_CeIdSegtoDefuncione() {
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public Tb_CeIdDetDefuncione getTbCeIdDetDefuncione() {
		return this.tbCeIdDetDefuncione;
	}

	public void setTbCeIdDetDefuncione(Tb_CeIdDetDefuncione tbCeIdDetDefuncione) {
		this.tbCeIdDetDefuncione = tbCeIdDetDefuncione;
	}

}