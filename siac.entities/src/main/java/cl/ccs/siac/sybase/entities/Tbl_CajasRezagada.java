package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CajasRezagadas database table.
 * 
 */
@Entity
@Table(name="Tbl_CajasRezagadas")
@NamedQuery(name="Tbl_CajasRezagada.findAll", query="SELECT t FROM Tbl_CajasRezagada t")
public class Tbl_CajasRezagada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_FlagProceso")
	private short fld_FlagProceso;

	public Tbl_CajasRezagada() {
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public short getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(short fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

}