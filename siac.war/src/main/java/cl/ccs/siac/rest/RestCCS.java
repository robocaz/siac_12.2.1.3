package cl.ccs.siac.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.servicios.ArqueoCajaServicio;
import cl.ccs.siac.servicios.CajaServicio;
import cl.ccs.siac.servicios.CarroCompraServicio;
import cl.ccs.siac.servicios.CentroCostoServicio;
import cl.ccs.siac.servicios.DepositosServicio;
import cl.ccs.siac.servicios.DetectorFraudeServicio;
import cl.ccs.siac.servicios.FiniquitoServicio;
import cl.ccs.siac.servicios.InicioBoletaServicio;
import cl.ccs.siac.servicios.ListadosServicio;
import cl.ccs.siac.servicios.MediosPagoServicio;
import cl.ccs.siac.servicios.ObservacionServicio;
import cl.ccs.siac.servicios.PagoServicio;
import cl.ccs.siac.servicios.PersonaServicio;
import cl.ccs.siac.servicios.ProtestosServicio;
import cl.ccs.siac.servicios.SolicitudServicio;
import cl.ccs.siac.servicios.TramitanteServicio;
import cl.ccs.siac.servicios.TransaccionServicio;
import cl.ccs.siac.servicios.UsuarioServicio;

//@ApplicationPath("/rest/*")
public class RestCCS extends Application {

	private static final Logger LOGGER = LogManager.getLogger(RestCCS.class);

	@Override
	public Set<Class<?>> getClasses() {
		LOGGER.info("Registrando recursos REST SIAC...");
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(CajaServicio.class);
		classes.add(CarroCompraServicio.class);
		classes.add(CentroCostoServicio.class);
		classes.add(DetectorFraudeServicio.class);
		classes.add(InicioBoletaServicio.class);
		classes.add(ListadosServicio.class);
		classes.add(MediosPagoServicio.class);
		classes.add(ObservacionServicio.class);
		classes.add(PagoServicio.class);
		classes.add(PersonaServicio.class);
		classes.add(TramitanteServicio.class);
		classes.add(TransaccionServicio.class);
		classes.add(UsuarioServicio.class);
		classes.add(FiniquitoServicio.class);
		classes.add(DepositosServicio.class);
		classes.add(ProtestosServicio.class);
		classes.add(SolicitudServicio.class);
		classes.add(ArqueoCajaServicio.class);
		//classes.add(CuadraturaServicio.class);
		return classes;

	}

}
