package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CodCampos database table.
 * 
 */
@Entity
@Table(name="Tbl_CodCampos")
@NamedQuery(name="Tbl_CodCampo.findAll", query="SELECT t FROM Tbl_CodCampo t")
public class Tbl_CodCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_GlosaCampo")
	private String fld_GlosaCampo;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CodCampo() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public String getFld_GlosaCampo() {
		return this.fld_GlosaCampo;
	}

	public void setFld_GlosaCampo(String fld_GlosaCampo) {
		this.fld_GlosaCampo = fld_GlosaCampo;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}