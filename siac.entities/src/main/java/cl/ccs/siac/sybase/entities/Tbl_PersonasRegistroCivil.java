package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PersonasRegistroCivil database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PersonasRegistroCivil.findAll", query="SELECT t FROM Tbl_PersonasRegistroCivil t")
public class Tbl_PersonasRegistroCivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_EstadoCivil")
	private String fld_EstadoCivil;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecCarga")
	private Timestamp fld_FecCarga;

	@Column(name="Fld_FecDefuncion")
	private Timestamp fld_FecDefuncion;

	@Column(name="Fld_FecMatrimonio")
	private Timestamp fld_FecMatrimonio;

	@Column(name="Fld_FecNacimiento")
	private Timestamp fld_FecNacimiento;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_Nacionalidad")
	private String fld_Nacionalidad;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	@Column(name="Fld_Sexo_Genero")
	private String fld_Sexo_Genero;

	public Tbl_PersonasRegistroCivil() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_EstadoCivil() {
		return this.fld_EstadoCivil;
	}

	public void setFld_EstadoCivil(String fld_EstadoCivil) {
		this.fld_EstadoCivil = fld_EstadoCivil;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecCarga() {
		return this.fld_FecCarga;
	}

	public void setFld_FecCarga(Timestamp fld_FecCarga) {
		this.fld_FecCarga = fld_FecCarga;
	}

	public Timestamp getFld_FecDefuncion() {
		return this.fld_FecDefuncion;
	}

	public void setFld_FecDefuncion(Timestamp fld_FecDefuncion) {
		this.fld_FecDefuncion = fld_FecDefuncion;
	}

	public Timestamp getFld_FecMatrimonio() {
		return this.fld_FecMatrimonio;
	}

	public void setFld_FecMatrimonio(Timestamp fld_FecMatrimonio) {
		this.fld_FecMatrimonio = fld_FecMatrimonio;
	}

	public Timestamp getFld_FecNacimiento() {
		return this.fld_FecNacimiento;
	}

	public void setFld_FecNacimiento(Timestamp fld_FecNacimiento) {
		this.fld_FecNacimiento = fld_FecNacimiento;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_Nacionalidad() {
		return this.fld_Nacionalidad;
	}

	public void setFld_Nacionalidad(String fld_Nacionalidad) {
		this.fld_Nacionalidad = fld_Nacionalidad;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public String getFld_Sexo_Genero() {
		return this.fld_Sexo_Genero;
	}

	public void setFld_Sexo_Genero(String fld_Sexo_Genero) {
		this.fld_Sexo_Genero = fld_Sexo_Genero;
	}

}