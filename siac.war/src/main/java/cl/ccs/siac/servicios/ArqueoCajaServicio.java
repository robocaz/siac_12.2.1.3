package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.arqueo.ArqueoCajaBusiness;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ArqueoCajaInDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasBolFactEmiOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasConDctoOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasTipoPagoOutDTO;
import cl.exe.ccs.dto.PDFArqueoCajaDTO;
import cl.exe.ccs.dto.PDFResponseDTO;

@Path("/arqueocaja")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class ArqueoCajaServicio {

	private static final Logger LOG = LogManager.getLogger(ArqueoCajaServicio.class);

	@Inject
	private ArqueoCajaBusiness arqueoCajaBusiness;

	@Inject
	private Utils utils;

	private boolean validaUsuario(HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if ( userPrincipal == null || userPrincipal.getName() == null ) {
			return false;
		}
		return true;
	}

	@POST
	@Path("/tipopago")
	public Response vtasTipoPago(@Context HttpServletRequest request, ArqueoCajaInDTO params) {
		LOG.info("-> vtasTipoPago: " + params);
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ArqueoCajaVtasTipoPagoOutDTO response = arqueoCajaBusiness.vtasTipoPago(params);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("ArqueoCaja - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/condcto")
	public Response vtasConDcto(@Context HttpServletRequest request, ArqueoCajaInDTO params) {
		LOG.info("-> vtasConDcto: " + params);
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ArqueoCajaVtasConDctoOutDTO response = arqueoCajaBusiness.vtasConDcto(params);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("ArqueoCaja - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/pdf")
	public Response pdf(@Context HttpServletRequest request, PDFArqueoCajaDTO dto) {
		LOG.info("-> PDF ArqueoCaja: " + dto);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		GeneradorPDF generador = new GeneradorPDF();
		Map<String, Object> mapa = obtenerMapa(dto);
		
		try {
			byte[] archivo = generador.create("ArqueoCaja", new HashSet(), mapa);
			
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);
			pdf.setIdControl(1);
			pdf.setMsgControl("OK");
			
			return Response.ok(pdf).build();
			
		} catch (Exception e) {
			LOG.error("ArqueoCaja - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	private Map<String, Object> obtenerMapa(PDFArqueoCajaDTO dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("CAJA", dto.getCaja());
		map.put("FECHA", utils.parseFormatFechaStr(dto.getFecha(), "ddMMyyyy", "dd/MM/yyyy"));
		map.put("USER", dto.getUser());
		map.put("CC", dto.getCc() + " " + dto.getCcDesc());
		map.put("REV", dto.getRev());
		map.put("BILL_NUMS", dto.getBillNums());
		map.put("BILL_TOTS", dto.getBillTots());
		map.put("COIN_NUMS", dto.getCoinNums());
		map.put("COIN_TOTS", dto.getCoinTots());
		map.put("BILL_TOTAL", dto.getBillTotal());
		map.put("COIN_TOTAL", dto.getCoinTotal());
		map.put("TOT_DOCS", dto.getTotDocs());
		map.put("TOT_CASH", dto.getTotCash());
		map.put("FONDO", dto.getFondo());
		map.put("TOT_ARQUEO", dto.getTotArqueo());
		map.put("DIFF", dto.getDiff());
		map.put("OBS", dto.getObs());
		
		try {
			final ArqueoCajaInDTO params = new ArqueoCajaInDTO();
			params.setFechaInicio(utils.parseFormatFechaStr(dto.getFecha(), "ddMMyyyy", "yyyy-MM-dd"));
			params.setCodUsuario(dto.getUser());
			params.setCodCC(dto.getCc());
			
			final ArqueoCajaVtasTipoPagoOutDTO resp1 = arqueoCajaBusiness.vtasTipoPago(params);
			map.put("LISTA_TIPO", resp1.getLista());
			map.put("TIPO_TRXS", resp1.getTotalTrxs());
			map.put("TIPO_PAG", resp1.getTotalPagado());
			map.put("TIPO_CALC", resp1.getTotalCalc());
			map.put("TIPO_DIFF", resp1.getTotalDiff());
			
			params.setFechaInicio(utils.parseFormatFechaStr(dto.getFecha(), "ddMMyyyy", "yyyy-MM-dd"));
			final ArqueoCajaVtasConDctoOutDTO resp2 = arqueoCajaBusiness.vtasConDcto(params);
			map.put("LISTA_DOCS", resp2.getLista());
			
			final ArqueoCajaVtasBolFactEmiOutDTO resp3 = arqueoCajaBusiness.vtasBolFactEmi(params);
			
			map.put("BOL_VAL", resp3.getTotBolEmitidas());
			map.put("BOL_NULL", resp3.getTotBolNulas());
			map.put("FACT_VAL", resp3.getTotFacEmitidas());
			map.put("FACT_NULL", resp3.getTotFacNulas());
			map.put("FOLIO_VAL", resp3.getTotFolCeroEmit());
			map.put("FOLIO_NULL", resp3.getTotFolCeroNulo());
			
		} catch (Exception e) {
			LOG.error("ArqueoCaja - Error Message: " + e.getMessage());
		}
		
		return map;
	}

}
