angular.module('ccs').factory('FiniquitoResource', function ($resource) {
	var resource = $resource('rest/finiquito/', { nocache : new Date().getTime()},
	{
		'consulta': { method:'POST', isArray:false, url:'rest/finiquito/consulta' },
		'guarda'  : { method:'POST', isArray:false, url:'rest/finiquito/guarda'   },
		'cmbdocs' : { method:'GET' , isArray:true , url:'rest/finiquito/cmbdocs'  },
		'pdf'	  : { method:'POST', isArray:false, url:'rest/finiquito/pdf'	  }
	});
	return resource;
});
