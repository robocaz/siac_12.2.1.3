package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.CombosDao;
import cl.ccs.siac.dao.MediosPagoDao;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.MedioPagoDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.SucursalDTO;

@Path("/mediopago")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class MediosPagoServicio {

	private static final Logger log = LogManager.getLogger(MediosPagoServicio.class);

	@Inject
	private MediosPagoDao mediosPagoDao;

	@Inject
	private CombosDao combosDao;

	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/combomedios")
	public Response obtenerMediosPago(@Context HttpServletRequest request) {

		ResponseDTO respuesta = new ResponseDTO();

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<ComboDTO> mediosPago = mediosPagoDao.obtenerMediosPago();

			if (mediosPago == null || mediosPago.size() == 0) {
				log.error("Ha ocurrido un error al carga medios de pagos");
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(mediosPago).build();
			}
		} catch (Exception ex) {
			log.error("Medios de Pago, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/cargaboleta")
	public Response obtenerBoleta(@Context HttpServletRequest request, MedioPagoDTO medioPagoDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			MedioPagoDTO boletas = mediosPagoDao.obtenerBoletas(medioPagoDTO);
			if (boletas.getIdControl() == 1) {
				log.error("Obtener Boleta, no existe numero boleta");
				boletas.setMsgControl("No existe boleta.");
				return Response.ok(boletas).build();

			} else {
				return Response.ok(boletas).build();
			}

		} catch (Exception ex) {
			log.error("Obtener Boleta, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/actualizamedio")
	public Response actualizaMedioPago(@Context HttpServletRequest request, MedioPagoDTO medioPagoDTO) {

		ResponseDTO respuesta = new ResponseDTO();

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			MedioPagoDTO pagoactualizar = mediosPagoDao.actualizaMedioDePago(medioPagoDTO);
			if (pagoactualizar.getIdControl() > 0) {
				log.info("No se realizo la actualizacion para numero de boleta " + pagoactualizar.getNumboletafac());
				return Response.status(Response.Status.NOT_FOUND).build();
			} else {
				log.info("Pago actualizado para numero de boleta " + pagoactualizar.getNumboletafac());
				return Response.ok(pagoactualizar).build();

			}
		} catch (Exception ex) {
			log.error("Actualizar Medio de Pago, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/combobancos")
	public Response obtenerBancos(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<ComboDTO> bancos = combosDao.obtenerEmisores("BCO");

			if (bancos == null || bancos.size() == 0) {
				log.error("Ha ocurrido un error al cargar barcos ");
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(bancos).build();
			}
		} catch (Exception ex) {
			log.error("Cargar Bancos, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/combosucursales")
	public Response obtenerSucursales(@Context HttpServletRequest request, MedioPagoDTO medioPago) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<SucursalDTO> sucursales = combosDao.obtenerSucursales(medioPago.getEmisor());

			if (sucursales == null || sucursales.size() == 0) {
				log.error("Ha ocurrido un error al cargar sucursales ");
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(sucursales).build();
			}
		} catch (Exception ex) {
			log.error("Cargar Sucursales, Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

}
