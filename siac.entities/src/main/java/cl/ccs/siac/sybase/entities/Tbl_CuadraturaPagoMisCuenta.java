package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CuadraturaPagoMisCuentas database table.
 * 
 */
@Entity
@Table(name="Tbl_CuadraturaPagoMisCuentas")
@NamedQuery(name="Tbl_CuadraturaPagoMisCuenta.findAll", query="SELECT t FROM Tbl_CuadraturaPagoMisCuenta t")
public class Tbl_CuadraturaPagoMisCuenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_CUADRATURAPAGOMISCUENTAS_IDCOMPRA_GENERATOR", sequenceName="IDCOMPRA")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_CUADRATURAPAGOMISCUENTAS_IDCOMPRA_GENERATOR")
	@Column(name="IdCompra")
	private long idCompra;

	@Column(name="Debito")
	private String debito;

	@Column(name="Estado")
	private byte estado;

	@Column(name="Monto")
	private double monto;

	@Column(name="Rut")
	private String rut;

	public Tbl_CuadraturaPagoMisCuenta() {
	}

	public long getIdCompra() {
		return this.idCompra;
	}

	public void setIdCompra(long idCompra) {
		this.idCompra = idCompra;
	}

	public String getDebito() {
		return this.debito;
	}

	public void setDebito(String debito) {
		this.debito = debito;
	}

	public byte getEstado() {
		return this.estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public double getMonto() {
		return this.monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}