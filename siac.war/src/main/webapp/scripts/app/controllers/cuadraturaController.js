angular.module('ccs').controller('CuadraturaController', function($rootScope, $scope, LoginResource, CuadraturaResource, NgTableParams, $location, $filter, $sce) {

	$scope.show = false;
	$scope.fechaDesde = {};
	$scope.fechaHasta = {};
	
	var pattern = /(\d{2})(\d{2})(\d{4})/;
	
	$scope.listadoCC = [];

	$scope.centroCosto = {
		descripcion : "",
		codigo : "",
		usuario : []

	}

	$scope.itemPadre = {
		descripcion : "Camara de Comercio",
		tipo : "CCS",
		divId : "CCS",
		centroCosto: 0
	}

	$scope.cuadraturaCCList = {};
	$scope.objSeleccionado = {};
	$scope.centroSeleccionado = {};
	$scope.muestraccs = false;

	$scope.cuadraturaGnrl = {};
	$scope.cuadXCss = false;
	$scope.tipoCuadratura = {}; 
	$scope.cajaSeleccionada = {};

	$scope.fechaDesde = $filter('date')(new Date(), "ddMMyyyy");
	$scope.fechaHasta = $filter('date')(new Date(), "ddMMyyyy");

	$scope.fechaDesdeCuad = "";
	$scope.fechaHastaCuad = "";
	$scope.usuarioCuad = "";
	$scope.centroCostoCuad = "";
	$scope.centroCostoCodigo = {};
	$scope.iva = 0;

	$scope.ocultaFecha = false;
	$scope.detalleBoletaObj = {};
	$scope.fechaSeleccion = {};
	$scope.blockForm = false;

	$scope.totales = {
		totBrutoNumMovBoleta : 0,
		totBrutoTransaccionBoleta : 0,
		totNetoTransaccionBoleta : 0,
		totIvaTransaccionBoleta : 0,
		totBrutoCanceladoBoleta : 0,
		totNetoCanceladoBoleta : 0,
		totIvaCanceladoBoleta : 0,
		totBrutoDiferenciaBoleta : 0,
		totNetoDiferenciaBoleta : 0,
		totIvaDiferenciaBoleta : 0,

		totBrutoNumMovFactura: 0,
		totBrutoTransaccionFactura : 0,
		totNetoTransaccionFactura : 0,
		totIvaTransaccionFactura : 0,
		totBrutoCanceladoFactura : 0,
		totNetoCanceladoFactura : 0,
		totIvaCanceladoFactura : 0,
		totBrutoDiferenciaFactura : 0,
		totNetoDiferenciaFactura : 0,
		totIvaDiferenciaFactura : 0
	}
	
	$scope.disableForm = false;
	$scope.habilitaBoton = false;
	$scope.opcion = {};

	$("#informecc").on('shown.bs.modal', function () {
		
		if ( !checkPermisos() ) {
			swal('Alerta','No tiene acceso a esta funcionalidad','error');
			$('#informecc').modal('hide');
		} else {
			if ( angular.equals({}, $scope.fechaDesde) && angular.equals({}, $scope.fechaHasta) ) {
				$scope.fechaDesde = $filter('date')(new Date(), "ddMMyyyy");
				$scope.fechaHasta = $filter('date')(new Date(), "ddMMyyyy");
			}
			
			$scope.$apply();
			//$(this).off('shown.bs.modal');
		}
	});

	function checkPermisos() {
		
		if ( $rootScope.usuario.esAdministrador || $rootScope.usuario.esSupervisor ) {
			$scope.show = true;
		} else {
			$scope.show = false;
		}
		return $scope.show;
	}

	$scope.buscarCC = function(){
		
		$scope.itemPadre.nodes = [];
		$scope.listadoCC  = [];

		console.log($rootScope.usuario);
		
		if((angular.equals({}, $scope.fechaDesde) || $scope.fechaDesde === "")) {
			swal('Alerta',"Ingrese fecha desde",'error');
			return false;

		}
		if(angular.equals({}, $scope.fechaHasta) || $scope.fechaHasta === ""){ 
			swal('Alerta',"Ingrese fecha hasta",'error');
			return false;
		}


		$scope.dateDesde = new Date($scope.fechaDesde.replace(pattern, '$3-$2-$1'));
		$scope.dateHasta = new Date($scope.fechaHasta.replace(pattern, '$3-$2-$1'));
		

		var desde = $filter('date')($scope.dateDesde, 'short');
		var hasta = $filter('date')($scope.dateHasta, 'short');
		var res = Date.parse(hasta) - Date.parse(desde);
		if (res < 0){
		   swal('Alerta',"Fecha Hasta no puede ser mayor a Fecha Desde",'error');
		   return false; 
		}

		var MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;
		
	  
	 	var diferencia =  Math.floor(res / MILISENGUNDOS_POR_DIA);
		
		if(diferencia >= 5 && diferencia < 30){
			swal('Alerta',"Dado el rango de fechas, la consulta podría demorar",'warning');
		}

		if(diferencia >= 30){
			swal('Alerta',"El rango de busqueda no puede ser igual o mayor a 30 días",'error');
			$scope.listadoCC = {};
			return false;
		}
		angular.element("#cargando").modal();
		var successCallback = function(data,responseHeaders){
			angular.element("#cargando").modal('hide');
			if(data.length > 0){
				angular.forEach(data, function(value, key) {
				 console.log(value.descripcion);
				 $scope.centroCosto = {};
				 $scope.centroCosto.descripcion = value.descripcion;
				 $scope.centroCosto.codigo = value.codigo;
				 $scope.centroCosto.tipo = value.codigoTipo;
				 $scope.centroCosto.divId = value.descripcion.replace(/\s+/g, '');
				 $scope.centroCosto.divId = $scope.centroCosto.divId.replace('.', '');
				 $scope.centroCosto.divId = $scope.centroCosto.divId.substring(0, $scope.centroCosto.divId.length-1);
				 $scope.centroCosto.nodes = [];
				 $scope.listadoCC.push($scope.centroCosto);
				});			

				if($rootScope.usuario.esAdministrador){
					$scope.muestraccs = true;
				}
			}
			else{
				 swal('Alerta',"No existen registros para este rango de fechas",'warning');
			}
		}
	  	CuadraturaResource.buscarCentroCostos({fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
	}

	$scope.calcDiff = function(firstDate, secondDate){
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
		var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
	}
	
	
	
	$scope.esfecha = function(fecha, tipofecha){
		
		var valor;
		if(fecha !== undefined  && !angular.equals({}, fecha)){
			dateArray = fecha.split("");

			if(dateArray.length > 0){
		        var fechaString = dateArray[0] + dateArray[1] + "/" + dateArray[2] + dateArray[3] + "/" + dateArray[4] + dateArray[5] + dateArray[6] + dateArray[7];
		 		
		 		valor = $scope.isValidDate(fechaString);
		 		if(!valor){
		 			
		 			if(tipofecha == "desde"){
		 				$scope.fechaDesde = "";
		 				swal('Alerta','Fecha Desde Invalida o no puede ser mayor a la fecha actual','error');
		 				$("#fecha_desde").focus();
		 			}
		 			else if(tipofecha == "hasta"){
						$scope.fechaHasta = "";
						swal('Alerta','Fecha Hasta Invalida o no puede ser mayor a la fecha actual','error');
						$("#fecha_hasta").focus();
		 			}
		 			else if(tipofecha == "hastaCuad"){
						$scope.fechaHastaCuad = "";
						swal('Alerta','Fecha Hasta Invalida o no puede ser mayor a la fecha actual','error');
						$("#fecha_fin").focus();
		 			}		 			
		 			else if(tipofecha == "desdeCuad"){
						$scope.fechaDesdeCuad = "";
						swal('Alerta','Fecha Desde Invalida o no puede ser mayor a la fecha actual','error');
						$("#fecha_inicio").focus();
		 			}		 			
		 		}
			}	
		}
	}
	$scope.isValidDate =  function(date) {

		var dateParts = date.split('/');
		var dateString = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]
	    var d = new Date(dateString);
		var today  = new Date();
		if(d > today){
           return false;
		}
	    return (d &&  d.getDate() == Number(dateParts[0])  && (d.getMonth() + 1) == dateParts[1] && d.getFullYear() == Number(dateParts[2]));
	}
  
	$scope.diferenciaEntreDiasEnDias = function(a, b){
	  var MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;
		
	  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  return Math.floor((utc2 - utc1) / MILISENGUNDOS_POR_DIA);
	}





    $scope.remove = function (scope) {
        scope.remove();
    };

    $scope.toggle = function (scope) {
       scope.toggle();
    };

    $scope.moveLastToTheBeginning = function () {
        var a = $scope.data.pop();
        $scope.data.splice(0, 0, a);
    };

    $scope.newSubItem = function (scope) {
        var nodeData = scope.$modelValue;
        //identificamos que tipo de busqueda es.
        nodeData.nodes = [];
        if(nodeData.tipo == "CC"){

			var successCallback = function(data,responseHeaders){

				 console.log(data);
				 angular.forEach(data, function(value, key) {
					 nodeData.nodes.push({
			          centroCosto: value.centroCosto,
			          descripcion: value.usuario,
			          tipo: value.codigoTipo,
			          divId: value.usuario.trim(), 
			          fechaDesde: value.fechaDesde,
			          fechasHasta: value.fechaHasta,
			          nodes: []
			         });
			    });
			}
		  	CuadraturaResource.buscarUsuarios({centroCosto: nodeData.codigo, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);        	
        }


        if(nodeData.tipo == "user"){
			var successCallback = function(data,responseHeaders){

				 console.log(data);
				 angular.forEach(data, function(value, key) {
					 nodeData.nodes.push({
			          centroCosto: value.centroCosto,
			          descripcion: value.corrCaja + " - " + value.inicioCaja,
			          tipo: value.codigoTipo,
			          divId: "div"+value.corrCaja, 
			          fechas: value.inicioCaja,
			          usuario: nodeData.descripcion,
			          corrCaja: value.corrCaja,
			          nodes: []
			         });
			    });
			}
		  	CuadraturaResource.buscarCajas({usuario: nodeData.descripcion, centroCosto: nodeData.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
        }

    };


    $scope.selectItem = function(value){
		$scope.objSeleccionado = value;
    }

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
       $scope.$broadcast('angular-ui-tree:expand-all');
    };
    

    $scope.seleccionarTipoCuadratura = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CCS"){
			$scope.cuadraturaCamara();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "user"){
    		$scope.cuadraturaUsuario();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CC"){
    		$scope.cuadraturaCentroCosto();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "caja"){
    		$scope.cuadraturaPorCajaArbol();
    	}

    }




    $scope.cuadraturaCamara = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CCS"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;
    		if($rootScope.usuario.esAdministrador){
    			centroCostoVar = 0;
    		}
    		else{
    			centroCostoVar = $rootScope.usuario.centroCostoActivo.codigo;
    		}

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaCCList = data;
				 angular.element("#reportecamara").modal()
			}
		  	CuadraturaResource.cuadraturaCCS({centroCosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta}  ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Camara de Comercio','error');
    		return false;
    	}

    	
    }
    



    $scope.cuadraturaUsuario = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "user"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;
    		if($rootScope.usuario.esAdministrador){
    			centroCostoVar = 0;
    		}
    		else{
    			centroCostoVar = $rootScope.usuario.centroCostoActivo.codigo;
    		}

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaUserList = data;
				 angular.element("#detalleusuario").modal();
				 $scope.centroSeleccionado = {};
				 
			}
		  	CuadraturaResource.cuadraturaUser({usuario: $scope.objSeleccionado.descripcion, centroCosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Usuario','error');
    		return false;
    	}

    	
    }

	$scope.cuadraturaCentroCosto = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CC"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaDetCCList = data;
				 angular.element("#detallecc").modal()
			}
		  	CuadraturaResource.cuadraturaCC({centroCosto: $scope.objSeleccionado.codigo, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Centro de Costo','error');
    		return false;
    	}


	}

	
	$scope.obtenerDetalleCC = function(){
		console.log($scope.centroSeleccionado);
		$scope.centroCostoCodigo = $scope.centroSeleccionado.centroCosto;
		if($scope.centroCostoCodigo == 0){
    		swal('Alerta','Seleccione Centro de Costo distinto de "Todos"','error');
    		return false;
   		}
   		
    	if(!angular.equals({}, $scope.centroSeleccionado)){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.centroSeleccionado ={};
				 $scope.cuadraturaDetCCList = data;
				 angular.element("#detallecc").modal()
			}
		  	CuadraturaResource.cuadraturaCC({centroCosto: $scope.centroSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Centro de Costo','error');
    		return false;
    	}
    	
    	
	}
	
	
	
	
	$scope.imprimeReporteCC = function(){
		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteCSS.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
	  	CuadraturaResource.printCuadraturaCC({centroCosto: 0, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
	}
	
	
	$scope.imprimeDetalleCajas = function(){
		
		console.log($scope.cuadraturaUserList);

		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteCajaCC.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}

		CuadraturaResource.printCuadraturaCaja($scope.cuadraturaDetCCList ,successCallback, $scope.errorCallback);
		
	}
	
	
	
	$scope.imprimeDetalleUsuario = function(){
		
		console.log($scope.cuadraturaUserList);

		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteUsuarioCC.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}

		CuadraturaResource.printCuadraturaUsuario($scope.cuadraturaUserList ,successCallback, $scope.errorCallback);
		
	}
	
	$scope.getValor = function(value){
		$scope.centroSeleccionado = value;
	}
	
	

    $scope.cerrarCuadCC = function(){
    	angular.element("#reportecamara").modal("hide");
		if($rootScope.usuario.esAdministrador){
			$scope.muestraccs = true;
		}
		else{
			$scope.muestraccs = false;
		}
    	//$scope.cuadraturaCCList = {};
    }

    $scope.cerrarCuadUsuario = function(){
    	angular.element("#detalleusuario").modal("hide");
		if($rootScope.usuario.esAdministrador){
			$scope.muestraccs = true;
		}
		else{
			$scope.muestraccs = false;
		}
    	$scope.cuadraturaUserList = {};
    }


    $scope.cerrarCuadDetCC = function(){
    	angular.element("#detallecc").modal("hide");
		if($rootScope.usuario.esAdministrador){
			$scope.muestraccs = true;
		}
		else{
			$scope.muestraccs = false;
		}
    	$scope.cuadraturaCCList.resultadoCentros.forEach(function (item, i) {
            item.centroSeleccionadocheck = false;
        });
        $scope.centroSeleccionado = {};
    	$scope.cuadraturaUserList = {};
    }

    $scope.cerrarBusqueda = function(){
    	angular.element("#informecc").modal("hide");
    	$scope.fechaHasta = {};
    	$scope.fechaDesde = {};
    	$scope.objSeleccionado = {};
    	$scope.listadoCC = [];
    	$scope.muestraccs = false;
    	$scope.centroCosto = {
			descripcion : "",
			codigo : "",
			usuario : []
		}
    }
    

    $scope.limpiar = function(){
    	$scope.fechaHasta = "";
    	$scope.fechaDesde = "";
    	$scope.objSeleccionado = {};
    	$scope.listadoCC = [];
    	$scope.muestraccs = false;
    	$scope.centroCosto = {
			descripcion : "",
			codigo : "",
			usuario : []
		}
    }
    
    
    
    
    
    
    $scope.cuadraturaGeneral = function(opcion){
    	
    	$scope.opcion = opcion;
    	console.log($scope.centroSeleccionado);
		if((angular.equals({}, $scope.centroSeleccionado)) && opcion !== 3){
			if($scope.opcion === 1){
				swal('Alerta','Seleccione Centro de Costo','error');
				return false;
			}
			if($scope.opcion === 0){
				swal('Alerta','Seleccione Usuario','error');
				return false;
			}
			
		}


    		var successCallback = function(data,responseHeaders){
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			if($scope.opcion == 1){
    				$scope.cuadraturaGnrl.impresion = 1;
    			}
    			else{
    				$scope.cuadraturaGnrl.impresion = 0;
    			}
    			
    			$scope.cuadraturaGnrl.centroCosto = $scope.centroSeleccionado.centroCosto;
    			if($scope.centroSeleccionado.glosaCC === undefined){
    				$scope.glosaCentro = "";
    			}
    			else{
    				$scope.glosaCentro = $scope.centroSeleccionado.glosaCC;
    			}
    			$scope.cuadraturaGnrl.glosaCC = $scope.centroSeleccionado.centroCosto + " - " + data.glosaCC;
    			if(angular.equals({}, $scope.fechaSeleccion)){
	    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
	    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
	    		}
	    		else{
	    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaSeleccion;
	    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;	  
	    			$scope.ocultaFecha = true;  			
	    		}

    			
    			
    			
    			$scope.habilitaBoton = false;
				$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $scope.centroSeleccionado.usuario;
				$scope.centroCostoCuad = $scope.centroSeleccionado.centroCosto;

				if($scope.usuarioCuad === undefined){
					$scope.cuadXCcs = true;
					$scope.disableForm = true;
					$scope.tipoCuadratura = "cc";
					
				}
				else{
					$scope.blockForm = true;
					$scope.cuadXCcs = false;
					$scope.disableForm = false;	
					$scope.tipoCuadratura = "caja";				
				}
				if($scope.opcion == 0){
					$scope.blockForm = true;
				}
				else{
					$scope.blockForm = false;
				}
				
				
				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					//Math.round(data.totalCalculado / (1 + $scope.iva)
					//$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * $scope.iva));
					$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta - Math.round(item.montoTransaccionBoleta / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					//$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * $scope.iva);
					$scope.totales.totIvaCanceladoBoleta = Math.round($scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta - Math.round(item.montoCantidadBoleta / (1 + $scope.iva))));
					

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					//$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * $scope.iva);
					$scope.totales.totIvaDiferenciaBoleta = Math.round($scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta - Math.round(item.diferenciaBoleta / (1 + $scope.iva))));
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;

					//$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * $scope.iva);
					$scope.totales.totIvaTransaccionFactura = Math.round($scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura - Math.round(item.montoTransaccionFactura / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = Math.round($scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura - Math.round(item.montoCantidadFactura / (1 + $scope.iva))));					
					//$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * $scope.iva);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					//$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * $scope.iva);
					$scope.totales.totIvaDiferenciaFactura = Math.round($scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura - Math.round(item.diferenciaFactura / (1 + $scope.iva))));					



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);    			
    			//$scope.cuadraturaGnrl.totales = $scope.totales;
				if($scope.opcion != 1){
	    			$scope.cuadraturaGnrl.totales = {};
	    			$scope.totales = {};
	    			
	    			$scope.cuadraturaGnrl.totalAviPago = 0;
					$scope.cuadraturaGnrl.totalBoletas = 0;
					$scope.cuadraturaGnrl.totalDiferencia = 0;
					$scope.cuadraturaGnrl.totalFacturas = 0;
					$scope.cuadraturaGnrl.totalMovimientos = 0;
					$scope.cuadraturaGnrl.totalPagado = 0;
					$scope.cuadraturaGnrl.totalTransacciones = 0;
					
	    			$scope.cuadraturaGnrl.movimientos  = [];
				}
				else{
					$scope.cuadraturaGnrl.totales = $scope.totales;
				}
    			if($scope.cuadraturaGnrl.centroCosto > 0){
    				$scope.buscaCajas();
    			}
    			if($scope.opcion == 3){
    				angular.element("#cuadraturaindex").modal();
    			}
    			else{
    				angular.element("#cuadratura").modal();
    			}
    		}

    		if($scope.opcion == 1){
    			$scope.blockForm = true;
    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.centroSeleccionado.centroCosto, 
    											fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta}
    											 ,successCallback, $scope.errorCallback);
    		}
    		if($scope.opcion == 0){

    			var fechaCaja = $scope.centroSeleccionado.fechaInicio.substring(0,10);
    			$scope.fechaSeleccion = fechaCaja.split("-")[2]+fechaCaja.split("-")[1]+fechaCaja.split("-")[0];

				CuadraturaResource.cuadraturaPorCentro({centroscosto: $scope.centroSeleccionado.centroCosto, 
    											fechaDesde: $scope.fechaSeleccion, usuario: $scope.centroSeleccionado.usuario,
    											corrcaja: $scope.centroSeleccionado.numeroCaja, criterio: 0 }
    											 ,successCallback, $scope.errorCallback);
    		}
    	
    		if($scope.opcion == 3){
    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.usuario.centroCostoActivo.codigo, 
    											fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta}
    											 ,successCallback, $scope.errorCallback);
    		}
    	
    }



    $scope.cuadraturaGeneralModal = function(){
    	
    		
    		if($scope.tipoCuadratura == 'cc'){
    			if($scope.fechaDesdeCuad === undefined || $scope.fechaDesdeCuad === ""){
    				swal('Alerta','Ingrese fecha desde','error');
    				return false;
    			}
    		}
    	
    	
    		var successCallback = function(data,responseHeaders){
    			
    	    	$scope.resetTotales();
    	    	//$scope.cuadraturaGnrl = {};
    	    	$scope.cuadraturaGnrl.impresion = 0;
    	    	
    			$scope.cuadraturaGnrl.totalAviPago = 0;
    			$scope.cuadraturaGnrl.totalBoletas = 0;
    			$scope.cuadraturaGnrl.totalDiferencia = 0;
    			$scope.cuadraturaGnrl.totalFacturas = 0;
    			$scope.cuadraturaGnrl.totalMovimientos = 0;
    			$scope.cuadraturaGnrl.totalPagado = 0;
    			$scope.cuadraturaGnrl.totalTransacciones = 0;
    		
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			$scope.cuadraturaGnrl.centroCosto = 0;
    			$scope.cuadraturaGnrl.glosaCC = "Todos";
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
    			$scope.cuadXCcs = true;
    			$scope.disableForm = true;
    			$scope.tipoCuadratura = "cc";
    			$scope.ocultaFecha = false;
    			$scope.habilitaBoton = false;
				//$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				//$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $scope.centroSeleccionado.usuario;
				$scope.centroCostoCuad = $scope.objSeleccionado.codigo;
    			
				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					$scope.totales.totIvaTransaccionBoleta = $scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * 0.19);

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * 0.19);

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * 0.19);
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;
					$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * 0.19);

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * 0.19);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * 0.19);



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);    			
				$scope.cuadraturaGnrl.totales = $scope.totales;
    			
    			//angular.element("#cuadratura").modal();
    		}
    		CuadraturaResource.cuadraturaGeneral({centroscosto: 0, fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaHastaCuad} ,successCallback, $scope.errorCallback);
    	
    }


 	$scope.cuadraturaGeneralPorCaja = function(){

 			
 			if(angular.equals({}, $scope.centroSeleccionado)){
 				swal('Alerta','Seleccione una Caja','error');
 				return false;
 			}
 			
			var fechaCaja = $scope.centroSeleccionado.fechaInicio.substring(0,10);
			$scope.fechaSeleccion = fechaCaja.split("-")[2]+fechaCaja.split("-")[1]+fechaCaja.split("-")[0];
			
			
			var successCallback = function(data,responseHeaders){
				$scope.listadoCajas = data;
    			$scope.cuadraturaGnrl.centroCosto = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.glosaCC = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaSeleccion;
    			$scope.cuadraturaGnrl.fechaHasta = "";//$scope.fechaHasta;
    			$scope.cuadraturaGnrl.usuario = $scope.objSeleccionado.descripcion;
    			$scope.cuadraturaGnrl.listadoCajas = data;
    			$scope.tipoCuadratura = "caja";


				$scope.fechaDesdeCuad = $scope.fechaSeleccion;
				//$scope.fechaHastaCuad = $scope.fechaHasta;
				$scope.fechaHastaCuad = "";
				$scope.usuarioCuad = $scope.cuadraturaUserList.usuario;
				$scope.centroCostoCuad = $scope.cuadraturaUserList.centroCosto;

				$scope.cuadXCcs = false;
    			$scope.disableForm = false;
    			$scope.blockForm = true;
    			angular.element("#cuadratura").modal();
			}



    		CuadraturaResource.cargaDatosCaja({fechaDesde: $scope.fechaSeleccion, centrocosto: $scope.cuadraturaUserList.centroCosto,
    											  usuario: $scope.cuadraturaUserList.usuario} ,successCallback, $scope.errorCallback);


 	}

    $scope.divAnterior = {};
    $scope.divNuevo = {};
    $scope.cambiaEstilo = function(id){
    	if(angular.equals({}, $scope.divAnterior) && angular.equals({}, $scope.divNuevo)){
    		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');
     	}
     	else if(!angular.equals({}, $scope.divAnterior) && angular.equals({}, $scope.divNuevo)){
     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divAnterior = {};

     		$scope.divNuevo = id;
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');
     		//$scope.divNuevo = {};
     	}
     	else if(!angular.equals({}, $scope.divNuevo) && angular.equals({}, $scope.divAnterior)){

     		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');

     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divNuevo = {};

     	}
     
     	else if(!angular.equals({}, $scope.divNuevo) && !angular.equals({}, $scope.divAnterior)){

     		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');

     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divNuevo = {};

     	}

    }

	$scope.getCaja = function(value){
		$scope.cajaSeleccionada = value;
	}


    $scope.buscaCuadratura = function(){

    	console.log($scope.cajaSeleccionada);
    	 
    	$scope.resetTotales();
    	//$scope.cuadraturaGnrl = {};
    	$scope.cuadraturaGnrl.impresion = 0;
    	$scope.cuadraturaGnrl.movimientos = [];
		$scope.cuadraturaGnrl.totalAviPago = 0;
		$scope.cuadraturaGnrl.totalBoletas = 0;
		$scope.cuadraturaGnrl.totalDiferencia = 0;
		$scope.cuadraturaGnrl.totalFacturas = 0;
		$scope.cuadraturaGnrl.totalMovimientos = 0;
		$scope.cuadraturaGnrl.totalPagado = 0;
		$scope.cuadraturaGnrl.totalTransacciones = 0;
    	
    	if($scope.tipoCuadratura === "caja"){
			if(angular.equals({}, $scope.cajaSeleccionada)){
				swal('Alerta','Seleccione una Caja','error');
				return false;
			}
    	}
    	
    	//$scope.cuadraturaGnrl.movimientos = {};

    	$scope.usuarioCuad
    	
    	if($scope.fechaDesdeCuad === undefined ||  $scope.fechaDesdeCuad === ""){
    		swal('Alerta',"Ingrese fecha desde",'error');
    		return false;
    	}
    	
    	if($scope.opcion == 1){
        	if($scope.fechaHastaCuad === undefined ||  $scope.fechaHastaCuad === ""){
        		swal('Alerta',"Ingrese fecha hasta",'error');
        		return false;
        	}   		
    	}

    	if($scope.tipoCuadratura === "CC" && $scope.usuarioCuad !== undefined && angular.equals({}, $scope.cajaSeleccionada)){
    		swal('Alerta',"Seleccione una caja",'error');
    		return false;
    	}

		console.log($scope.cuadraturaGnrl);

			   // $scope.resetTotales();

		    	var successCallback = function(data,responseHeaders){
		    		
	         	if(!angular.equals({}, $scope.cuadraturaGnrl.listadoCajas)){
	        		if($scope.cuadraturaGnrl.listadoCajas !== undefined && $scope.cuadraturaGnrl.listadoCajas !== undefined){
	    	        	$scope.cuadraturaGnrl.listadoCajas.forEach(function (item, i) {
	    	                item.centroSeleccionadocheck = false;
	    	            });
	        		}
	        	}
	         	 $scope.cajaSeleccionada = {};		    		
    			console.log(data);
    			//$scope.fechaHastaCuad = "";
    			if(data.movimientos.length == 0){
    				swal('Alerta',"No se encontrarón registros",'error');
    			}


    			$scope.cuadraturaGnrl.movimientos = data.movimientos;
    			if(!$scope.blockForm){
    				$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesdeCuad;
    			}
    			else{
    				$scope.cuadraturaGnrl.fechaDesde = "";
    			}
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHastaCuad;
    			
    			$scope.cuadraturaGnrl.totalAviPago = data.totalAviPago;
				$scope.cuadraturaGnrl.totalBoletas = data.totalBoletas;
				$scope.cuadraturaGnrl.totalDiferencia = data.totalDiferencia;
				$scope.cuadraturaGnrl.totalFacturas = data.totalFacturas;
				$scope.cuadraturaGnrl.totalMovimientos = data.totalMovimientos;
				$scope.cuadraturaGnrl.totalPagado = data.totalPagado;
				$scope.cuadraturaGnrl.totalTransacciones = data.totalTransacciones;
				$scope.cuadraturaGnrl.impresion = 1;
				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					//Math.round(data.totalCalculado / (1 + $scope.iva)
					//$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * $scope.iva));
					$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta - Math.round(item.montoTransaccionBoleta / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					//$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * $scope.iva);
					$scope.totales.totIvaCanceladoBoleta = Math.round($scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta - Math.round(item.montoCantidadBoleta / (1 + $scope.iva))));
					

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					//$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * $scope.iva);
					$scope.totales.totIvaDiferenciaBoleta = Math.round($scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta - Math.round(item.diferenciaBoleta / (1 + $scope.iva))));
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;

					//$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * $scope.iva);
					$scope.totales.totIvaTransaccionFactura = Math.round($scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura - Math.round(item.montoTransaccionFactura / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = Math.round($scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura - Math.round(item.montoCantidadFactura / (1 + $scope.iva))));					
					//$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * $scope.iva);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					//$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * $scope.iva);
					$scope.totales.totIvaDiferenciaFactura = Math.round($scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura - Math.round(item.diferenciaFactura / (1 + $scope.iva))));					




		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;

				console.log($scope.totales);    			
				$scope.cuadraturaGnrl.totales = $scope.totales;
    			
    			//angular.element("#cuadratura").modal();
    		}

//    		if(angular.equals({}, $scope.cajaSeleccionada)){
//    			//if($scope.fechaHastaCuad === ""){
//    				$scope.fechaHastaCuad = $scope.fechaDesdeCuad;
//    			//}
//    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.centroCostoCuad, 
//    											fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaHastaCuad}
//    											 ,successCallback, $scope.errorCallback);
//
//    		}
//    		
    		if(angular.equals({}, $scope.cajaSeleccionada) && ($scope.usuarioCuad === undefined || $scope.usuarioCuad === "" )){
    			$scope.fechaHastaCuad = $scope.fechaDesdeCuad;
    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.centroCostoCuad, 
    											fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaDesdeCuad}
    											 ,successCallback, $scope.errorCallback);

    		}
    		else if(angular.equals({}, $scope.cajaSeleccionada) && ($scope.usuarioCuad !== undefined || $scope.usuarioCuad !== "" )){
    			CuadraturaResource.cuadraturaPorCentro({fechaDesde: $scope.fechaDesdeCuad, centroscosto: $scope.centroCostoCuad,
					usuario: $scope.usuarioCuad, corrcaja:  $scope.cajaSeleccionada.corrCaja, criterio: 1} ,successCallback, $scope.errorCallback);
    		}
    		
    		else{
	    		CuadraturaResource.cuadraturaPorCentro({fechaDesde: $scope.fechaDesdeCuad, centroscosto: $scope.cajaSeleccionada.codCentroCosto,
    												usuario: $scope.usuarioCuad, corrcaja:  $scope.cajaSeleccionada.corrCaja, criterio: 0} ,successCallback, $scope.errorCallback);
    		}
	}

    

	$scope.cuadraturaPorCajaArbol = function(){

			var fechaCaja = $scope.objSeleccionado.fechas.substring(0,10);
			$scope.fechaSeleccion = fechaCaja.split("-")[2]+fechaCaja.split("-")[1]+fechaCaja.split("-")[0];

    		var successCallback = function(data,responseHeaders){
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			$scope.cuadraturaGnrl.centroCosto = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.glosaCC = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaSeleccion;
    			$scope.cuadraturaGnrl.fechaHasta = "";//$scope.fechaHasta;
    			$scope.cuadXCcs = false;
    			$scope.disableForm = false;
    			$scope.blockForm = true;
    			$scope.tipoCuadratura = "caja";

    			
				$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $scope.objSeleccionado.usuario;
				$scope.centroCostoCuad = $scope.objSeleccionado.centroCosto;
    			

    			if(data.movimientos.length == 0){
    				swal('Alerta',"No se encontrarón registros",'error');
    			}

				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					$scope.totales.totIvaTransaccionBoleta = $scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * 0.19);

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * 0.19);

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * 0.19);
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;
					$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * 0.19);

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * 0.19);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * 0.19);



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);  
				$scope.cuadraturaGnrl.totales = $scope.totales;

    			$scope.buscaCajas();
    			angular.element("#cuadratura").modal();
    		}

			
    		CuadraturaResource.cuadraturaPorCaja({centrocosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaSeleccion, usuario: $scope.objSeleccionado.usuario, 
    											corrcaja:  $scope.objSeleccionado.corrCaja, criterio: 0} ,successCallback, $scope.errorCallback);



	}



    
    $scope.cerrarCuadratura = function(){

    	$scope.cajaSeleccionada = {};
		$scope.centroSeleccionado = {};
		$scope.cuadraturaGnrl = {};
    	if(!angular.equals({}, $scope.cuadraturaCCList)){
    		if($scope.cuadraturaCCList.resultadoCentros !== undefined){
	        	$scope.cuadraturaCCList.resultadoCentros.forEach(function (item, i) {
	                item.centroSeleccionadocheck = false;
	            });
    		}
    	}

    	if(!angular.equals({}, $scope.cuadraturaDetCCList)){
    		if($scope.cuadraturaDetCCList !== undefined && $scope.cuadraturaDetCCList.cajasDetalle !== undefined){
	        	$scope.cuadraturaDetCCList.cajasDetalle.forEach(function (item, i) {
	                item.centroSeleccionadocheck = false;
	            });
    		}
    	}
    	
    	
    	
     	if(!angular.equals({}, $scope.cuadraturaUserList)){
    		if($scope.cuadraturaUserList !== undefined && $scope.cuadraturaUserList.resultadoUsuarios !== undefined){
	        	$scope.cuadraturaUserList.resultadoUsuarios.forEach(function (item, i) {
	                item.centroSeleccionadocheck = false;
	            });
    		}
    	}   	

		$scope.resetTotales();
		$scope.cuadXCss = false;
		$scope.cuadraturaGnrl = {};
		$scope.opcion = {};
		angular.element("#cuadratura").modal('hide');
    }
    
    
    $scope.resetTotales = function(){

	  	$scope.totales = {
			totBrutoNumMovBoleta : 0,
			totBrutoTransaccionBoleta : 0,
			totNetoTransaccionBoleta : 0,
			totIvaTransaccionBoleta : 0,
			totBrutoCanceladoBoleta : 0,
			totNetoCanceladoBoleta : 0,
			totIvaCanceladoBoleta : 0,
			totBrutoDiferenciaBoleta : 0,
			totNetoDiferenciaBoleta : 0,
			totIvaDiferenciaBoleta : 0,

			totBrutoNumMovFactura: 0,
			totBrutoTransaccionFactura : 0,
			totNetoTransaccionFactura : 0,
			totIvaTransaccionFactura : 0,
			totBrutoCanceladoFactura : 0,
			totNetoCanceladoFactura : 0,
			totIvaCanceladoFactura : 0,
			totBrutoDiferenciaFactura : 0,
			totNetoDiferenciaFactura : 0,
			totIvaDiferenciaFactura : 0
		}  	

    }


    $scope.buscaCajas = function(){
    	
    	$scope.resetTotales();
    	$scope.cuadraturaGnrl.movimientos = [];
    	$scope.cuadraturaGnrl.impresion = 0;
    	$scope.cuadraturaGnrl.movimientos = [];
		$scope.cuadraturaGnrl.totalAviPago = 0;
		$scope.cuadraturaGnrl.totalBoletas = 0;
		$scope.cuadraturaGnrl.totalDiferencia = 0;
		$scope.cuadraturaGnrl.totalFacturas = 0;
		$scope.cuadraturaGnrl.totalMovimientos = 0;
		$scope.cuadraturaGnrl.totalPagado = 0;
		$scope.cuadraturaGnrl.totalTransacciones = 0;
		
    	if($scope.fechaDesdeCuad === undefined || $scope.fechaDesdeCuad === ""){
    		swal('Alerta',"Ingrese fecha de busqueda.",'error');
    		return false;
    	}
    	
    	var successCallback = function(data,responseHeaders){
    		$scope.cuadraturaGnrl.listadoCajas = data;
    	}
    	
    	CuadraturaResource.cargaDatosCaja({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad} ,successCallback, $scope.errorCallback);
    }



    $scope.imprimeCuadratura = function(value){

    	console.log(value);
    	
    	if(value.impresion == 0){
			swal('Alerta','Realice una búsqueda de cuadratura para imprimir','error');
			return false;
    	}
    	
    	
    	value.usuario = $scope.usuarioCuad;
    	if((value.corrCaja === undefined || value.corrCaja === null) && $scope.centroSeleccionado.numeroCaja !== undefined){
    		value.corrCaja = $scope.centroSeleccionado.numeroCaja; 
    	}
    	
    	var successCallback = function(data,responseHeaders){
    		console.log(data);
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "cuadraturaGeneral.pdf");

    	}
    	CuadraturaResource.imprimeCuadratura(value,successCallback, $scope.errorCallback);
    }



    $scope.cargaIva = function(){
    	var successCallbackIVA = function(data, responseHeaders){
			console.log('successCallbackIVA');
			console.log(data);
			if(data.idControl !== 0) {
       			 swal({ title: 'Error', text: 'No se puede obtener el dato IVA!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false})
       			 .then(function(){
          			return false;	
        		});
        		
			}
    		$scope.iva = parseFloat(data.msgControl)/100;
		}
		CuadraturaResource.obtenerIVA(successCallbackIVA, $scope.errorCallback);
		
    }
     $scope.cargaIva();
     
     
     $scope.detalleBoleta = function(){
    	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);
    	 $scope.detalleBoletaObj = {};

     	if(angular.equals({}, $scope.cajaSeleccionada)){
    		swal('Alerta',"Seleccione una caja",'error');
    		return false;
    	}

    	var successCallback = function(data, responseHeaders){
			console.log(data);
			$scope.detalleBoletaObj = data;

			var valorIva = Math.round(data.totalCalculado / (1 + $scope.iva));

			$scope.detalleBoletaObj.totalTransaccionNetoBoletas = Math.round(data.totalCalculado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalCanceladoNetoBoletas = Math.round(data.totalPagado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalDiferenciaNetoBoletas = Math.round(data.totalDiferencia / (1 + $scope.iva));

			$scope.detalleBoletaObj.totalTransaccionIvaBoletas = data.totalCalculado - Math.round(data.totalCalculado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalCanceladoIvaBoletas = data.totalPagado - Math.round(data.totalPagado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalDiferenciaIvaBoletas = data.totalDiferencia - Math.round(data.totalDiferencia / (1 + $scope.iva));

		}
		CuadraturaResource.detalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0 } ,successCallback, $scope.errorCallback);


    	 angular.element("#detalleboleta").modal();
     }
     
     $scope.cerraDetalleBoleta = function(){
    	

     	if(!angular.equals({}, $scope.cuadraturaGnrl.listadoCajas)){
    		if($scope.cuadraturaGnrl.listadoCajas !== undefined && $scope.cuadraturaGnrl.listadoCajas !== undefined){
	        	$scope.cuadraturaGnrl.listadoCajas.forEach(function (item, i) {
	                item.centroSeleccionadocheck = false;
	            });
    		}
    	}
     	 $scope.cajaSeleccionada = {};
    	 angular.element("#detalleboleta").modal('hide');
     }
     


     $scope.detalleServicios = function(){
     	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);

    	if(angular.equals({}, $scope.cajaSeleccionada)){
    		swal('Alerta',"Seleccione una caja",'error');
    		return false;
    	}
    	else{
    		
	    	var successCallback = function(data, responseHeaders){
	    		console.log("-----------------------------");
				console.log(data);
				$scope.detalleServiciosDto = data;
	
	
				$scope.tableParams = new NgTableParams({}, { dataset: $scope.detalleServiciosDto.servicios});
				angular.element("#detalleservicios").modal();
			}
	
	
			CuadraturaResource.detalleServicio({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
				  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0, ordenamiento: 0} ,successCallback, $scope.errorCallback);
    	}


    	 
     }

     
     $scope.imprimeDetalleServicios = function(){
     	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);

    	var successCallback = function(data, responseHeaders){

			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "detalleServicios.pdf");
		}


		CuadraturaResource.imprimeDetalleServicio({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0, ordenamiento: 0} ,successCallback, $scope.errorCallback);


    	 
     }
     
     
     
     $scope.cerrarServicios = function(){
    	 angular.element("#detalleservicios").modal('hide');
     }

     $scope.detalleBoletaModal = function(){
    	 
    	console.log($scope.centroSeleccionado) ;
    	
    	if(angular.equals({}, $scope.centroSeleccionado)){
    		swal('Alerta',"Seleccione una boleta o factura",'error');
    		return false;
    	}
    	$scope.detalleboletaDto = {};
    	$scope.detalleboletaDto.corrCaja = $scope.centroSeleccionado.corrCaja;
    	$scope.detalleboletaDto.nroBoletaFactura = 	$scope.centroSeleccionado.numBoletaFactura



     	var successCallback = function(data, responseHeaders){
			console.log(data);
			$scope.detalleboletaDto = data;
			if($scope.detalleboletaDto.boletaFactura == "B"){
				angular.element("#detalletransaccionboleta").modal();	
			}
			if($scope.detalleboletaDto.boletaFactura == "F"){
				dateArray = $scope.detalleboletaDto.detalleFactura.ubicacion.split("\\");
				
				$scope.detalleboletaDto.detalleFactura.direccion = dateArray[0];
				$scope.detalleboletaDto.detalleFactura.ciudad = dateArray[2];
				$scope.detalleboletaDto.detalleFactura.comuna = dateArray[1];
				angular.element("#detalletransaccionfactura").modal();	
			}			

		}
		CuadraturaResource.modalBoletaCuad($scope.detalleboletaDto,successCallback, $scope.errorCallback);

     	
     }


     
     $scope.imprimeDetalleBoleta = function(){

    	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);
    	if($scope.tipoCuadratura !== "cc"){
			if(angular.equals({}, $scope.cajaSeleccionada)){
				swal({ title: 'Error', text: "Seleccione una Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
				return false;
			}  
    	}  	
    	var successCallback = function(data, responseHeaders){
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "detalleBoletaFactura.pdf");

		}
		if($scope.tipoCuadratura !== "cc"){
			CuadraturaResource.imprimeDetalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0 } ,successCallback, $scope.errorCallback);
		}
		else{
			CuadraturaResource.imprimeDetalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: 0, criterio:1 } ,successCallback, $scope.errorCallback);
			
		}


    	
     	
     }

     $scope.imprimeValidador = function(){
    	 
    	console.log($scope.cuadraturaGnrl);
    	console.log($scope.cajaSeleccionada);
    	
    	if(angular.equals({}, $scope.cajaSeleccionada)){
			swal('Alerta','Seleccione una Caja','error');
			return false;
    	}
    	
    	var salida1 = 0;
    	var salida2 = 0;
    	
    	var successCallback = function(data, responseHeaders){
			// base64 string
    		
    		if(data.idControl == 1){
    			salida1 = data.idControl;
    		}
    		
    		else{
    		
    		
				var base64str = data.reporte;
	
				// decode base64 string, remove space for IE compatibility
				var binary = atob(base64str.replace(/\s/g, ''));
				var len = binary.length;
				var buffer = new ArrayBuffer(len);
				var view = new Uint8Array(buffer);
				for (var i = 0; i < len; i++) {
				    view[i] = binary.charCodeAt(i);
				}
	
				var blob = new Blob( [view], { type: "application/pdf" });
				saveAs(blob, "validadorAclaraciones.pdf");
			
    		}
			
			
			var successCallbackSolicitud = function(data, responseHeaders){

	    		if(data.idControl == 1){
	    			salida2 = data.idControl;
	    		}
	    		
	    		else{
				
					var base64sol = data.reporte;
	
					// decode base64 string, remove space for IE compatibility
					var binarySol = atob(base64sol.replace(/\s/g, ''));
					var len = binarySol.length;
					var buffer = new ArrayBuffer(len);
					var view = new Uint8Array(buffer);
					for (var i = 0; i < len; i++) {
					    view[i] = binarySol.charCodeAt(i);
					}
	
					var blob = new Blob( [view], { type: "application/pdf" });
					saveAs(blob, "solicitudAclaraciones.pdf");
	    		}
	    		
	    		
	    		if(salida1 && salida2 == 1){
	    			swal({ title: 'Error', text: "No existen datos.", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	    		} 
			}
			
			CuadraturaResource.imprimeSolReclamo({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
				  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja} ,successCallbackSolicitud, $scope.errorCallback);
			

		}
		CuadraturaResource.imprimeValidacion({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja} ,successCallback, $scope.errorCallback);
		
		
     }
     
     $scope.cierraBoletaModal = function(){
     	angular.element("#detalletransaccionboleta").modal('hide');
     	$scope.detalleboletaDto = {};
     	$scope.centroSeleccionado = {};
     	if(!angular.equals({}, $scope.detalleBoletaObj.items)){
    		if($scope.detalleBoletaObj.items !== undefined && $scope.detalleBoletaObj.items !== undefined){
	        	$scope.detalleBoletaObj.items.forEach(function (item, i) {
	                item.boletaSeleccionada = false;
	            });
    		}
    	}       	

     }

     $scope.cerrarDetalleFactura= function(){
      	angular.element("#detalletransaccionfactura").modal('hide');
     	$scope.detalleboletaDto = {};  
     	$scope.centroSeleccionado = {};
     	if(!angular.equals({}, $scope.detalleBoletaObj.items)){
    		if($scope.detalleBoletaObj.items !== undefined && $scope.detalleBoletaObj.items !== undefined){
	        	$scope.detalleBoletaObj.items.forEach(function (item, i) {
	                item.boletaSeleccionada = false;
	            });
    		}
    	}  	 
     }

});
