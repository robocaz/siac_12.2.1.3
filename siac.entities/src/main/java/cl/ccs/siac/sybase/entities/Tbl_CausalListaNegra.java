package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalListaNegra database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalListaNegra.findAll", query="SELECT t FROM Tbl_CausalListaNegra t")
public class Tbl_CausalListaNegra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CausalListaNegra() {
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}