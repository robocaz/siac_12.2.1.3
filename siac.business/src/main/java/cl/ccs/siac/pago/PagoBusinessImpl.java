package cl.ccs.siac.pago;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.PagosDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("pagoBusiness")
public class PagoBusinessImpl implements PagoBusiness {

	@Inject
	private PagosDao pagosDao;
	
	@Inject
	private Utils utils;
	
	
	public ResponseDTO actualizaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion){
		
		
		String rutParseTramitante = "";
		String rutParseAfectado = "";
		if(boletaTransaccion.getRutTramitante() != null && !boletaTransaccion.getRutTramitante().equals("")){

			rutParseTramitante = utils.formatearRUT(boletaTransaccion.getRutTramitante());
			rutParseTramitante = rutParseTramitante.replace(".", "");
			rutParseTramitante = String.format("%11s", rutParseTramitante).replace(' ', '0');
			
			boletaTransaccion.setRutTramitante(rutParseTramitante);
		}
		
		if(boletaTransaccion.getRutAfectado() != null && !boletaTransaccion.getRutAfectado().equals("")){

			rutParseAfectado = utils.formatearRUT(boletaTransaccion.getRutAfectado());
			rutParseAfectado = rutParseAfectado.replace(".", "");
			rutParseAfectado = String.format("%11s", rutParseAfectado).replace(' ', '0');
			
			boletaTransaccion.setRutAfectado(rutParseAfectado);
		}		
		return pagosDao.actualizaBoletaFacturaTemp(boletaTransaccion);
		
	}

	
	public Long consultaBoletaFacturaTemp(BoletaTransaccionDTO boletaTransaccion){
		return pagosDao.consultaBoletaFacturaTemp(boletaTransaccion);
	}

	
	public ResponseDTO actualizaMedioPagoTemp(BoletaTransaccionDTO boletaTransaccion){
		String rutParse = "";
		if(boletaTransaccion.getRutTramitante() != null && !boletaTransaccion.getRutTramitante().equals("")){

			rutParse = utils.formatearRUT(boletaTransaccion.getRutTramitante());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			boletaTransaccion.setRutTramitante(rutParse);
		}
		return pagosDao.actualizaMedioPagoTemp(boletaTransaccion);
	}
	
	
	public ResponseDTO anularBoleta(BoletaTransaccionDTO boletaTransaccion){
		return pagosDao.anularBoleta(boletaTransaccion);
	}
	
	
	public ResponseDTO anulacionRegistroNuevo(Long idCorrCaja, Long corrTransaccion){
		return pagosDao.anulacionRegistroNuevo(idCorrCaja, corrTransaccion);
	}
	
	public ResponseDTO valorServicio(String codServicio){
		return pagosDao.valorServicio(codServicio);
	}
	
}
