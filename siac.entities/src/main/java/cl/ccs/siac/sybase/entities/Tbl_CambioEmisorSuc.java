package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CambioEmisorSuc database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CambioEmisorSuc.findAll", query="SELECT t FROM Tbl_CambioEmisorSuc t")
public class Tbl_CambioEmisorSuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodSucActual")
	private BigDecimal fld_CodSucActual;

	@Column(name="Fld_CodSucNuevo")
	private BigDecimal fld_CodSucNuevo;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	public Tbl_CambioEmisorSuc() {
	}

	public BigDecimal getFld_CodSucActual() {
		return this.fld_CodSucActual;
	}

	public void setFld_CodSucActual(BigDecimal fld_CodSucActual) {
		this.fld_CodSucActual = fld_CodSucActual;
	}

	public BigDecimal getFld_CodSucNuevo() {
		return this.fld_CodSucNuevo;
	}

	public void setFld_CodSucNuevo(BigDecimal fld_CodSucNuevo) {
		this.fld_CodSucNuevo = fld_CodSucNuevo;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

}