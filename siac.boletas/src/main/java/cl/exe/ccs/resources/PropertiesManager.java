package cl.exe.ccs.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
@Startup
public class PropertiesManager implements ServletContextListener {
	private static final Logger logger = LogManager.getLogger(PropertiesManager.class);

	private static String filename;

	private Properties propsMessages = new Properties();
	private Properties propsConstants = new Properties();

	public PropertiesManager() {
		super();
		logger.info("PropertiesManager:-->Inicio----------------");

		InputStream inputConst = null;
		InputStream inputMsg = null;
		try {
			inputConst = getClass().getClassLoader().getResourceAsStream("/resources/config-desa.properties");
			propsConstants.load(inputConst);

			inputMsg = getClass().getClassLoader().getResourceAsStream("/resources/messages.properties");
			propsMessages.load(inputMsg);

		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.info("PropertiesManager:-->Fin");
	}

	public Properties getPropertiesMessages() {
		return propsMessages;
	}

	public Properties getPropertiesConstants() {
		return propsConstants;
	}

	public static void setFilename(String initFile) {
		filename = initFile;
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}
}
