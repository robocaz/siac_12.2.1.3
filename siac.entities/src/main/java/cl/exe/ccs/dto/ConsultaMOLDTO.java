package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ConsultaMOLDTO implements Serializable {
    
    private String codAutenticMol; 
    private Integer cantRegistros;
    private List<MorosidadVigenteDTO> listMorosidadVig;
    
    
	public String getCodAutenticMol() {
		return codAutenticMol;
	}
	public void setCodAutenticMol(String codAutenticMol) {
		this.codAutenticMol = codAutenticMol;
	}
	public Integer getCantRegistros() {
		return cantRegistros;
	}
	public void setCantRegistros(Integer cantRegistros) {
		this.cantRegistros = cantRegistros;
	}
	public List<MorosidadVigenteDTO> getListMorosidadVig() {
		return listMorosidadVig;
	}
	public void setListMorosidadVig(List<MorosidadVigenteDTO> listMorosidadVig) {
		this.listMorosidadVig = listMorosidadVig;
	}
    
    
    

}
