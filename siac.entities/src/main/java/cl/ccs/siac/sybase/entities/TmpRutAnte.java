package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rut_antes database table.
 * 
 */
@Entity
@Table(name="tmp_rut_antes")
@NamedQuery(name="TmpRutAnte.findAll", query="SELECT t FROM TmpRutAnte t")
public class TmpRutAnte implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rut;

	public TmpRutAnte() {
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}