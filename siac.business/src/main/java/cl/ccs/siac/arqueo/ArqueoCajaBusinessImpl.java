package cl.ccs.siac.arqueo;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.ArqueoCajaDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ArqueoCajaInDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasBolFactEmiOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasConDctoOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasTipoPagoOutDTO;

@Named("arqueoCajaBusiness")
public class ArqueoCajaBusinessImpl implements ArqueoCajaBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private ArqueoCajaDao arqueoCajaDao;
	
	@Override
	public ArqueoCajaVtasTipoPagoOutDTO vtasTipoPago(ArqueoCajaInDTO params) {
		
		params.setFechaInicio(utils.addTimeStrFormat(params.getFechaInicio()));
		
		return arqueoCajaDao.vtasTipoPago(params);
	}
	
	@Override
	public ArqueoCajaVtasConDctoOutDTO vtasConDcto(ArqueoCajaInDTO params) {
		
		params.setFechaInicio(utils.addTimeStrFormat(params.getFechaInicio()));
		
		return arqueoCajaDao.vtasConDcto(params);
	}
	
	@Override
	public ArqueoCajaVtasBolFactEmiOutDTO vtasBolFactEmi(ArqueoCajaInDTO params) {
		
		return arqueoCajaDao.vtasBolFactEmi(params);
	}

}
