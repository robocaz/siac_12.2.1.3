package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_BLOQUEOEMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_BLOQUEOEMISOR")
@NamedQuery(name="TblBloqueoemisor.findAll", query="SELECT t FROM TblBloqueoemisor t")
public class TblBloqueoemisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblBloqueoemisor() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}