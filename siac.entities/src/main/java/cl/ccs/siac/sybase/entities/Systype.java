package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the systypes database table.
 * 
 */
@Entity
@Table(name="systypes")
@NamedQuery(name="Systype.findAll", query="SELECT s FROM Systype s")
public class Systype implements Serializable {
	private static final long serialVersionUID = 1L;

	private int accessrule;

	private boolean allownulls;

	private int domain;

	private byte hierarchy;

	private byte ident;

	private int length;

	private String name;

	private byte prec;

	private String printfmt;

	private byte scale;

	private int tdefault;

	private byte type;

	private int uid;

	private short usertype;

	private boolean variable;

	private int xdbid;

	private int xtypeid;

	public Systype() {
	}

	public int getAccessrule() {
		return this.accessrule;
	}

	public void setAccessrule(int accessrule) {
		this.accessrule = accessrule;
	}

	public boolean getAllownulls() {
		return this.allownulls;
	}

	public void setAllownulls(boolean allownulls) {
		this.allownulls = allownulls;
	}

	public int getDomain() {
		return this.domain;
	}

	public void setDomain(int domain) {
		this.domain = domain;
	}

	public byte getHierarchy() {
		return this.hierarchy;
	}

	public void setHierarchy(byte hierarchy) {
		this.hierarchy = hierarchy;
	}

	public byte getIdent() {
		return this.ident;
	}

	public void setIdent(byte ident) {
		this.ident = ident;
	}

	public int getLength() {
		return this.length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getPrec() {
		return this.prec;
	}

	public void setPrec(byte prec) {
		this.prec = prec;
	}

	public String getPrintfmt() {
		return this.printfmt;
	}

	public void setPrintfmt(String printfmt) {
		this.printfmt = printfmt;
	}

	public byte getScale() {
		return this.scale;
	}

	public void setScale(byte scale) {
		this.scale = scale;
	}

	public int getTdefault() {
		return this.tdefault;
	}

	public void setTdefault(int tdefault) {
		this.tdefault = tdefault;
	}

	public byte getType() {
		return this.type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public short getUsertype() {
		return this.usertype;
	}

	public void setUsertype(short usertype) {
		this.usertype = usertype;
	}

	public boolean getVariable() {
		return this.variable;
	}

	public void setVariable(boolean variable) {
		this.variable = variable;
	}

	public int getXdbid() {
		return this.xdbid;
	}

	public void setXdbid(int xdbid) {
		this.xdbid = xdbid;
	}

	public int getXtypeid() {
		return this.xtypeid;
	}

	public void setXtypeid(int xtypeid) {
		this.xtypeid = xtypeid;
	}

}