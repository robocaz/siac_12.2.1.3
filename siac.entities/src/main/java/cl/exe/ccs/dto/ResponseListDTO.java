package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseListDTO<T> implements Serializable{
    private String msgControl;
    private Integer idControl;
    private List<T> lista;
    
    public List<T> getLista() {
            return lista;
    }
    public void setLista(List<T> lista) {
            this.lista = lista;
    }

    public void setMsgControl(String msgControl) {
        this.msgControl = msgControl;
    }

    public String getMsgControl() {
        return msgControl;
    }

    public void setIdControl(Integer idControl) {
        this.idControl = idControl;
    }

    public Integer getIdControl() {
        return idControl;
    }
}
