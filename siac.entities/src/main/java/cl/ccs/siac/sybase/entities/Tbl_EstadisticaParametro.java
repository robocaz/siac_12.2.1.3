package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EstadisticaParametro database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadisticaParametro.findAll", query="SELECT t FROM Tbl_EstadisticaParametro t")
public class Tbl_EstadisticaParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodConsulta")
	private byte fld_CodConsulta;

	@Column(name="Fld_DatoConsulta")
	private String fld_DatoConsulta;

	@Column(name="Fld_Glosa")
	private String fld_Glosa;

	@Column(name="Fld_TipoParametro")
	private String fld_TipoParametro;

	public Tbl_EstadisticaParametro() {
	}

	public byte getFld_CodConsulta() {
		return this.fld_CodConsulta;
	}

	public void setFld_CodConsulta(byte fld_CodConsulta) {
		this.fld_CodConsulta = fld_CodConsulta;
	}

	public String getFld_DatoConsulta() {
		return this.fld_DatoConsulta;
	}

	public void setFld_DatoConsulta(String fld_DatoConsulta) {
		this.fld_DatoConsulta = fld_DatoConsulta;
	}

	public String getFld_Glosa() {
		return this.fld_Glosa;
	}

	public void setFld_Glosa(String fld_Glosa) {
		this.fld_Glosa = fld_Glosa;
	}

	public String getFld_TipoParametro() {
		return this.fld_TipoParametro;
	}

	public void setFld_TipoParametro(String fld_TipoParametro) {
		this.fld_TipoParametro = fld_TipoParametro;
	}

}