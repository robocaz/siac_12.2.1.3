package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoSeccion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoSeccion.findAll", query="SELECT t FROM Tbl_TipoSeccion t")
public class Tbl_TipoSeccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaCortaSec")
	private String fld_GlosaCortaSec;

	@Column(name="Fld_GlosaSeccion")
	private String fld_GlosaSeccion;

	@Column(name="Fld_Orden")
	private byte fld_Orden;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipCert")
	private byte fld_TipCert;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	@Column(name="Fld_TipoRegBol")
	private String fld_TipoRegBol;

	public Tbl_TipoSeccion() {
	}

	public String getFld_GlosaCortaSec() {
		return this.fld_GlosaCortaSec;
	}

	public void setFld_GlosaCortaSec(String fld_GlosaCortaSec) {
		this.fld_GlosaCortaSec = fld_GlosaCortaSec;
	}

	public String getFld_GlosaSeccion() {
		return this.fld_GlosaSeccion;
	}

	public void setFld_GlosaSeccion(String fld_GlosaSeccion) {
		this.fld_GlosaSeccion = fld_GlosaSeccion;
	}

	public byte getFld_Orden() {
		return this.fld_Orden;
	}

	public void setFld_Orden(byte fld_Orden) {
		this.fld_Orden = fld_Orden;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipCert() {
		return this.fld_TipCert;
	}

	public void setFld_TipCert(byte fld_TipCert) {
		this.fld_TipCert = fld_TipCert;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

	public String getFld_TipoRegBol() {
		return this.fld_TipoRegBol;
	}

	public void setFld_TipoRegBol(String fld_TipoRegBol) {
		this.fld_TipoRegBol = fld_TipoRegBol;
	}

}