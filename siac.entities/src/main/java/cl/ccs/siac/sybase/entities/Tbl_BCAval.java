package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCAval database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCAval.findAll", query="SELECT t FROM Tbl_BCAval t")
public class Tbl_BCAval implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcreedor")
	private BigDecimal fld_CorrAcreedor;

	@Column(name="Fld_FecVigencia")
	private Timestamp fld_FecVigencia;

	@Column(name="Fld_NombreAval")
	private String fld_NombreAval;

	@Column(name="Fld_RutAval")
	private String fld_RutAval;

	@Column(name="Fld_Vigencia")
	private boolean fld_Vigencia;

	public Tbl_BCAval() {
	}

	public BigDecimal getFld_CorrAcreedor() {
		return this.fld_CorrAcreedor;
	}

	public void setFld_CorrAcreedor(BigDecimal fld_CorrAcreedor) {
		this.fld_CorrAcreedor = fld_CorrAcreedor;
	}

	public Timestamp getFld_FecVigencia() {
		return this.fld_FecVigencia;
	}

	public void setFld_FecVigencia(Timestamp fld_FecVigencia) {
		this.fld_FecVigencia = fld_FecVigencia;
	}

	public String getFld_NombreAval() {
		return this.fld_NombreAval;
	}

	public void setFld_NombreAval(String fld_NombreAval) {
		this.fld_NombreAval = fld_NombreAval;
	}

	public String getFld_RutAval() {
		return this.fld_RutAval;
	}

	public void setFld_RutAval(String fld_RutAval) {
		this.fld_RutAval = fld_RutAval;
	}

	public boolean getFld_Vigencia() {
		return this.fld_Vigencia;
	}

	public void setFld_Vigencia(boolean fld_Vigencia) {
		this.fld_Vigencia = fld_Vigencia;
	}

}