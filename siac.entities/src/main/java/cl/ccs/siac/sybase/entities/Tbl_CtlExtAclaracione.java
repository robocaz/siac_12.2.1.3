package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtlExtAclaraciones database table.
 * 
 */
@Entity
@Table(name="Tbl_CtlExtAclaraciones")
@NamedQuery(name="Tbl_CtlExtAclaracione.findAll", query="SELECT t FROM Tbl_CtlExtAclaracione t")
public class Tbl_CtlExtAclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_FecExtraccion")
	private Timestamp fld_FecExtraccion;

	@Column(name="Fld_FlagProceso")
	private boolean fld_FlagProceso;

	public Tbl_CtlExtAclaracione() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public Timestamp getFld_FecExtraccion() {
		return this.fld_FecExtraccion;
	}

	public void setFld_FecExtraccion(Timestamp fld_FecExtraccion) {
		this.fld_FecExtraccion = fld_FecExtraccion;
	}

	public boolean getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(boolean fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

}