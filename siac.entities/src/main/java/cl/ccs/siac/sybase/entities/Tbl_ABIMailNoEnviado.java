package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ABIMailNoEnviados database table.
 * 
 */
@Entity
@Table(name="Tbl_ABIMailNoEnviados")
@NamedQuery(name="Tbl_ABIMailNoEnviado.findAll", query="SELECT t FROM Tbl_ABIMailNoEnviado t")
public class Tbl_ABIMailNoEnviado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrMail")
	private BigDecimal fld_CorrMail;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	public Tbl_ABIMailNoEnviado() {
	}

	public BigDecimal getFld_CorrMail() {
		return this.fld_CorrMail;
	}

	public void setFld_CorrMail(BigDecimal fld_CorrMail) {
		this.fld_CorrMail = fld_CorrMail;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

}