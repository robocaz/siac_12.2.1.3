package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdDetDirecciones database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetDirecciones")
@NamedQuery(name="Tb_CeIdDetDireccione.findAll", query="SELECT t FROM Tb_CeIdDetDireccione t")
public class Tb_CeIdDetDireccione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETDIRECCIONES_FLD_CORRDIRECCION_ID_GENERATOR", sequenceName="FLD_CORRDIRECCION_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETDIRECCIONES_FLD_CORRDIRECCION_ID_GENERATOR")
	@Column(name="Fld_CorrDireccion_Id")
	private long fld_CorrDireccion_Id;

	@Column(name="Fld_CodCircunsElect")
	private short fld_CodCircunsElect;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodPostal")
	private BigDecimal fld_CodPostal;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_NroInscripElect")
	private short fld_NroInscripElect;

	@Column(name="Fld_NroRegistroElect")
	private short fld_NroRegistroElect;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	@Column(name="Fld_Sexo")
	private boolean fld_Sexo;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDomicilio")
	private byte fld_TipoDomicilio;

	public Tb_CeIdDetDireccione() {
	}

	public long getFld_CorrDireccion_Id() {
		return this.fld_CorrDireccion_Id;
	}

	public void setFld_CorrDireccion_Id(long fld_CorrDireccion_Id) {
		this.fld_CorrDireccion_Id = fld_CorrDireccion_Id;
	}

	public short getFld_CodCircunsElect() {
		return this.fld_CodCircunsElect;
	}

	public void setFld_CodCircunsElect(short fld_CodCircunsElect) {
		this.fld_CodCircunsElect = fld_CodCircunsElect;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CodPostal() {
		return this.fld_CodPostal;
	}

	public void setFld_CodPostal(BigDecimal fld_CodPostal) {
		this.fld_CodPostal = fld_CodPostal;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public short getFld_NroInscripElect() {
		return this.fld_NroInscripElect;
	}

	public void setFld_NroInscripElect(short fld_NroInscripElect) {
		this.fld_NroInscripElect = fld_NroInscripElect;
	}

	public short getFld_NroRegistroElect() {
		return this.fld_NroRegistroElect;
	}

	public void setFld_NroRegistroElect(short fld_NroRegistroElect) {
		this.fld_NroRegistroElect = fld_NroRegistroElect;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public boolean getFld_Sexo() {
		return this.fld_Sexo;
	}

	public void setFld_Sexo(boolean fld_Sexo) {
		this.fld_Sexo = fld_Sexo;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoDomicilio() {
		return this.fld_TipoDomicilio;
	}

	public void setFld_TipoDomicilio(byte fld_TipoDomicilio) {
		this.fld_TipoDomicilio = fld_TipoDomicilio;
	}

}