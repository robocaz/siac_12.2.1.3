package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetRutExisBICMCCVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetRutExisBICMCCVCE.findAll", query="SELECT t FROM Tbl_DetRutExisBICMCCVCE t")
public class Tbl_DetRutExisBICMCCVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_FecPubAMD")
	private String fld_FecPubAMD;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_Procedencia")
	private String fld_Procedencia;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_DetRutExisBICMCCVCE() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_FecPubAMD() {
		return this.fld_FecPubAMD;
	}

	public void setFld_FecPubAMD(String fld_FecPubAMD) {
		this.fld_FecPubAMD = fld_FecPubAMD;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public String getFld_Procedencia() {
		return this.fld_Procedencia;
	}

	public void setFld_Procedencia(String fld_Procedencia) {
		this.fld_Procedencia = fld_Procedencia;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}