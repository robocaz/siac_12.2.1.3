package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RelAclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RelAclBatch.findAll", query="SELECT t FROM Tbl_RelAclBatch t")
public class Tbl_RelAclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAclBat")
	private BigDecimal fld_CorrAclBat;

	@Column(name="Fld_CorrEnvioAcl")
	private BigDecimal fld_CorrEnvioAcl;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FolioAclBat")
	private int fld_FolioAclBat;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_RelAclBatch() {
	}

	public BigDecimal getFld_CorrAclBat() {
		return this.fld_CorrAclBat;
	}

	public void setFld_CorrAclBat(BigDecimal fld_CorrAclBat) {
		this.fld_CorrAclBat = fld_CorrAclBat;
	}

	public BigDecimal getFld_CorrEnvioAcl() {
		return this.fld_CorrEnvioAcl;
	}

	public void setFld_CorrEnvioAcl(BigDecimal fld_CorrEnvioAcl) {
		this.fld_CorrEnvioAcl = fld_CorrEnvioAcl;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public int getFld_FolioAclBat() {
		return this.fld_FolioAclBat;
	}

	public void setFld_FolioAclBat(int fld_FolioAclBat) {
		this.fld_FolioAclBat = fld_FolioAclBat;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}