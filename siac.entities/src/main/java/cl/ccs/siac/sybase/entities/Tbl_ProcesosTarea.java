package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ProcesosTarea database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ProcesosTarea.findAll", query="SELECT t FROM Tbl_ProcesosTarea t")
public class Tbl_ProcesosTarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProceso")
	private short fld_CodProceso;

	@Column(name="Fld_NombreProceso")
	private String fld_NombreProceso;

	@Column(name="Fld_NombreScripts")
	private String fld_NombreScripts;

	@Column(name="Fld_NroProcActivos")
	private byte fld_NroProcActivos;

	@Column(name="Fld_Path")
	private String fld_Path;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_ProcesosTarea() {
	}

	public short getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(short fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public String getFld_NombreProceso() {
		return this.fld_NombreProceso;
	}

	public void setFld_NombreProceso(String fld_NombreProceso) {
		this.fld_NombreProceso = fld_NombreProceso;
	}

	public String getFld_NombreScripts() {
		return this.fld_NombreScripts;
	}

	public void setFld_NombreScripts(String fld_NombreScripts) {
		this.fld_NombreScripts = fld_NombreScripts;
	}

	public byte getFld_NroProcActivos() {
		return this.fld_NroProcActivos;
	}

	public void setFld_NroProcActivos(byte fld_NroProcActivos) {
		this.fld_NroProcActivos = fld_NroProcActivos;
	}

	public String getFld_Path() {
		return this.fld_Path;
	}

	public void setFld_Path(String fld_Path) {
		this.fld_Path = fld_Path;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}