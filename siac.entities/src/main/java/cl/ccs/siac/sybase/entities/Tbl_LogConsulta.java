package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LogConsultas database table.
 * 
 */
@Entity
@Table(name="Tbl_LogConsultas")
@NamedQuery(name="Tbl_LogConsulta.findAll", query="SELECT t FROM Tbl_LogConsulta t")
public class Tbl_LogConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_LogConsultaPK id;

	@Column(name="Fld_Batch")
	private byte fld_Batch;

	@Column(name="Fld_Cantidad")
	private short fld_Cantidad;

	@Column(name="Fld_Interactivo")
	private byte fld_Interactivo;

	@Column(name="Fld_Resultado")
	private byte fld_Resultado;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_LogConsulta() {
	}

	public Tbl_LogConsultaPK getId() {
		return this.id;
	}

	public void setId(Tbl_LogConsultaPK id) {
		this.id = id;
	}

	public byte getFld_Batch() {
		return this.fld_Batch;
	}

	public void setFld_Batch(byte fld_Batch) {
		this.fld_Batch = fld_Batch;
	}

	public short getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(short fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public byte getFld_Interactivo() {
		return this.fld_Interactivo;
	}

	public void setFld_Interactivo(byte fld_Interactivo) {
		this.fld_Interactivo = fld_Interactivo;
	}

	public byte getFld_Resultado() {
		return this.fld_Resultado;
	}

	public void setFld_Resultado(byte fld_Resultado) {
		this.fld_Resultado = fld_Resultado;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}