package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TotalVerifEspecial database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TotalVerifEspecial.findAll", query="SELECT t FROM Tbl_TotalVerifEspecial t")
public class Tbl_TotalVerifEspecial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProceso")
	private byte fld_CodProceso;

	@Column(name="Fld_TotalMonedasInval")
	private int fld_TotalMonedasInval;

	@Column(name="Fld_TotalRegGrabados")
	private int fld_TotalRegGrabados;

	@Column(name="Fld_TotalRegLeidos")
	private int fld_TotalRegLeidos;

	@Column(name="Fld_TotalRiesgoAlto")
	private int fld_TotalRiesgoAlto;

	@Column(name="Fld_TotalRiesgoBajo")
	private int fld_TotalRiesgoBajo;

	@Column(name="Fld_TotalRiesgoMedio")
	private int fld_TotalRiesgoMedio;

	@Column(name="Fld_TotalRutBuenos")
	private int fld_TotalRutBuenos;

	@Column(name="Fld_TotalRutConProt")
	private int fld_TotalRutConProt;

	@Column(name="Fld_TotalRutErroneos")
	private int fld_TotalRutErroneos;

	@Column(name="Fld_TotalRutSinProt")
	private int fld_TotalRutSinProt;

	public Tbl_TotalVerifEspecial() {
	}

	public byte getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(byte fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public int getFld_TotalMonedasInval() {
		return this.fld_TotalMonedasInval;
	}

	public void setFld_TotalMonedasInval(int fld_TotalMonedasInval) {
		this.fld_TotalMonedasInval = fld_TotalMonedasInval;
	}

	public int getFld_TotalRegGrabados() {
		return this.fld_TotalRegGrabados;
	}

	public void setFld_TotalRegGrabados(int fld_TotalRegGrabados) {
		this.fld_TotalRegGrabados = fld_TotalRegGrabados;
	}

	public int getFld_TotalRegLeidos() {
		return this.fld_TotalRegLeidos;
	}

	public void setFld_TotalRegLeidos(int fld_TotalRegLeidos) {
		this.fld_TotalRegLeidos = fld_TotalRegLeidos;
	}

	public int getFld_TotalRiesgoAlto() {
		return this.fld_TotalRiesgoAlto;
	}

	public void setFld_TotalRiesgoAlto(int fld_TotalRiesgoAlto) {
		this.fld_TotalRiesgoAlto = fld_TotalRiesgoAlto;
	}

	public int getFld_TotalRiesgoBajo() {
		return this.fld_TotalRiesgoBajo;
	}

	public void setFld_TotalRiesgoBajo(int fld_TotalRiesgoBajo) {
		this.fld_TotalRiesgoBajo = fld_TotalRiesgoBajo;
	}

	public int getFld_TotalRiesgoMedio() {
		return this.fld_TotalRiesgoMedio;
	}

	public void setFld_TotalRiesgoMedio(int fld_TotalRiesgoMedio) {
		this.fld_TotalRiesgoMedio = fld_TotalRiesgoMedio;
	}

	public int getFld_TotalRutBuenos() {
		return this.fld_TotalRutBuenos;
	}

	public void setFld_TotalRutBuenos(int fld_TotalRutBuenos) {
		this.fld_TotalRutBuenos = fld_TotalRutBuenos;
	}

	public int getFld_TotalRutConProt() {
		return this.fld_TotalRutConProt;
	}

	public void setFld_TotalRutConProt(int fld_TotalRutConProt) {
		this.fld_TotalRutConProt = fld_TotalRutConProt;
	}

	public int getFld_TotalRutErroneos() {
		return this.fld_TotalRutErroneos;
	}

	public void setFld_TotalRutErroneos(int fld_TotalRutErroneos) {
		this.fld_TotalRutErroneos = fld_TotalRutErroneos;
	}

	public int getFld_TotalRutSinProt() {
		return this.fld_TotalRutSinProt;
	}

	public void setFld_TotalRutSinProt(int fld_TotalRutSinProt) {
		this.fld_TotalRutSinProt = fld_TotalRutSinProt;
	}

}