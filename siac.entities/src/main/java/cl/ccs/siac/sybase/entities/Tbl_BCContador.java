package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_BCContador database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCContador.findAll", query="SELECT t FROM Tbl_BCContador t")
public class Tbl_BCContador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_NroRegistro")
	private BigDecimal fld_NroRegistro;

	@Column(name="Fld_RutContador")
	private String fld_RutContador;

	public Tbl_BCContador() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_NroRegistro() {
		return this.fld_NroRegistro;
	}

	public void setFld_NroRegistro(BigDecimal fld_NroRegistro) {
		this.fld_NroRegistro = fld_NroRegistro;
	}

	public String getFld_RutContador() {
		return this.fld_RutContador;
	}

	public void setFld_RutContador(String fld_RutContador) {
		this.fld_RutContador = fld_RutContador;
	}

}