package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_NotarioInterinoSuplente database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_NotarioInterinoSuplente.findAll", query="SELECT t FROM Tbl_NotarioInterinoSuplente t")
public class Tbl_NotarioInterinoSuplente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodIntSup")
	private String fld_CodIntSup;

	@Column(name="Fld_CodNotario")
	private String fld_CodNotario;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	@Column(name="Fld_FlagEliminado")
	private boolean fld_FlagEliminado;

	@Column(name="Fld_NombreIntSup")
	private String fld_NombreIntSup;

	@Column(name="Fld_Observacion")
	private String fld_Observacion;

	@Column(name="Fld_TipoNotario")
	private String fld_TipoNotario;

	public Tbl_NotarioInterinoSuplente() {
	}

	public String getFld_CodIntSup() {
		return this.fld_CodIntSup;
	}

	public void setFld_CodIntSup(String fld_CodIntSup) {
		this.fld_CodIntSup = fld_CodIntSup;
	}

	public String getFld_CodNotario() {
		return this.fld_CodNotario;
	}

	public void setFld_CodNotario(String fld_CodNotario) {
		this.fld_CodNotario = fld_CodNotario;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

	public boolean getFld_FlagEliminado() {
		return this.fld_FlagEliminado;
	}

	public void setFld_FlagEliminado(boolean fld_FlagEliminado) {
		this.fld_FlagEliminado = fld_FlagEliminado;
	}

	public String getFld_NombreIntSup() {
		return this.fld_NombreIntSup;
	}

	public void setFld_NombreIntSup(String fld_NombreIntSup) {
		this.fld_NombreIntSup = fld_NombreIntSup;
	}

	public String getFld_Observacion() {
		return this.fld_Observacion;
	}

	public void setFld_Observacion(String fld_Observacion) {
		this.fld_Observacion = fld_Observacion;
	}

	public String getFld_TipoNotario() {
		return this.fld_TipoNotario;
	}

	public void setFld_TipoNotario(String fld_TipoNotario) {
		this.fld_TipoNotario = fld_TipoNotario;
	}

}