package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_PersonasAConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PersonasAConsulta.findAll", query="SELECT t FROM Tbl_PersonasAConsulta t")
public class Tbl_PersonasAConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstadoConsulta")
	private byte fld_CodEstadoConsulta;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FlagPrincipal")
	private boolean fld_FlagPrincipal;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Ocurrencia")
	private int fld_Ocurrencia;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_PersonasAConsulta() {
	}

	public byte getFld_CodEstadoConsulta() {
		return this.fld_CodEstadoConsulta;
	}

	public void setFld_CodEstadoConsulta(byte fld_CodEstadoConsulta) {
		this.fld_CodEstadoConsulta = fld_CodEstadoConsulta;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public boolean getFld_FlagPrincipal() {
		return this.fld_FlagPrincipal;
	}

	public void setFld_FlagPrincipal(boolean fld_FlagPrincipal) {
		this.fld_FlagPrincipal = fld_FlagPrincipal;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public int getFld_Ocurrencia() {
		return this.fld_Ocurrencia;
	}

	public void setFld_Ocurrencia(int fld_Ocurrencia) {
		this.fld_Ocurrencia = fld_Ocurrencia;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}