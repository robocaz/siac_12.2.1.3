package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetObituarios database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetObituarios")
@NamedQuery(name="Tb_CeIdDetObituario.findAll", query="SELECT t FROM Tb_CeIdDetObituario t")
public class Tb_CeIdDetObituario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETOBITUARIOS_FLD_CORROBITUARIO_ID_GENERATOR", sequenceName="FLD_CORROBITUARIO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETOBITUARIOS_FLD_CORROBITUARIO_ID_GENERATOR")
	@Column(name="Fld_CorrObituario_Id")
	private long fld_CorrObituario_Id;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodPareo")
	private byte fld_CodPareo;

	@Column(name="Fld_FecDefuncion")
	private Timestamp fld_FecDefuncion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdNoFallecidosRC
	@OneToMany(mappedBy="tbCeIdDetObituario")
	private List<Tb_CeIdNoFallecidosRC> tbCeIdNoFallecidosRcs;

	//bi-directional many-to-one association to Tb_CeIdPareoObituario
	@OneToMany(mappedBy="tbCeIdDetObituario")
	private List<Tb_CeIdPareoObituario> tbCeIdPareoObituarios;

	public Tb_CeIdDetObituario() {
	}

	public long getFld_CorrObituario_Id() {
		return this.fld_CorrObituario_Id;
	}

	public void setFld_CorrObituario_Id(long fld_CorrObituario_Id) {
		this.fld_CorrObituario_Id = fld_CorrObituario_Id;
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodPareo() {
		return this.fld_CodPareo;
	}

	public void setFld_CodPareo(byte fld_CodPareo) {
		this.fld_CodPareo = fld_CodPareo;
	}

	public Timestamp getFld_FecDefuncion() {
		return this.fld_FecDefuncion;
	}

	public void setFld_FecDefuncion(Timestamp fld_FecDefuncion) {
		this.fld_FecDefuncion = fld_FecDefuncion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public List<Tb_CeIdNoFallecidosRC> getTbCeIdNoFallecidosRcs() {
		return this.tbCeIdNoFallecidosRcs;
	}

	public void setTbCeIdNoFallecidosRcs(List<Tb_CeIdNoFallecidosRC> tbCeIdNoFallecidosRcs) {
		this.tbCeIdNoFallecidosRcs = tbCeIdNoFallecidosRcs;
	}

	public Tb_CeIdNoFallecidosRC addTbCeIdNoFallecidosRc(Tb_CeIdNoFallecidosRC tbCeIdNoFallecidosRc) {
		getTbCeIdNoFallecidosRcs().add(tbCeIdNoFallecidosRc);
		tbCeIdNoFallecidosRc.setTbCeIdDetObituario(this);

		return tbCeIdNoFallecidosRc;
	}

	public Tb_CeIdNoFallecidosRC removeTbCeIdNoFallecidosRc(Tb_CeIdNoFallecidosRC tbCeIdNoFallecidosRc) {
		getTbCeIdNoFallecidosRcs().remove(tbCeIdNoFallecidosRc);
		tbCeIdNoFallecidosRc.setTbCeIdDetObituario(null);

		return tbCeIdNoFallecidosRc;
	}

	public List<Tb_CeIdPareoObituario> getTbCeIdPareoObituarios() {
		return this.tbCeIdPareoObituarios;
	}

	public void setTbCeIdPareoObituarios(List<Tb_CeIdPareoObituario> tbCeIdPareoObituarios) {
		this.tbCeIdPareoObituarios = tbCeIdPareoObituarios;
	}

	public Tb_CeIdPareoObituario addTbCeIdPareoObituario(Tb_CeIdPareoObituario tbCeIdPareoObituario) {
		getTbCeIdPareoObituarios().add(tbCeIdPareoObituario);
		tbCeIdPareoObituario.setTbCeIdDetObituario(this);

		return tbCeIdPareoObituario;
	}

	public Tb_CeIdPareoObituario removeTbCeIdPareoObituario(Tb_CeIdPareoObituario tbCeIdPareoObituario) {
		getTbCeIdPareoObituarios().remove(tbCeIdPareoObituario);
		tbCeIdPareoObituario.setTbCeIdDetObituario(null);

		return tbCeIdPareoObituario;
	}

}