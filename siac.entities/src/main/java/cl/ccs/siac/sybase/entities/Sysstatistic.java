package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sysstatistics database table.
 * 
 */
@Entity
@Table(name="sysstatistics")
@NamedQuery(name="Sysstatistic.findAll", query="SELECT s FROM Sysstatistic s")
public class Sysstatistic implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] c0;

	private byte[] c1;

	private byte[] c10;

	private byte[] c11;

	private byte[] c12;

	private byte[] c13;

	private byte[] c14;

	private byte[] c15;

	private byte[] c16;

	private byte[] c17;

	private byte[] c18;

	private byte[] c19;

	private byte[] c2;

	private byte[] c20;

	private byte[] c21;

	private byte[] c22;

	private byte[] c23;

	private byte[] c24;

	private byte[] c25;

	private byte[] c26;

	private byte[] c27;

	private byte[] c28;

	private byte[] c29;

	private byte[] c3;

	private byte[] c30;

	private byte[] c31;

	private byte[] c32;

	private byte[] c33;

	private byte[] c34;

	private byte[] c35;

	private byte[] c36;

	private byte[] c37;

	private byte[] c38;

	private byte[] c39;

	private byte[] c4;

	private byte[] c40;

	private byte[] c41;

	private byte[] c42;

	private byte[] c43;

	private byte[] c44;

	private byte[] c45;

	private byte[] c46;

	private byte[] c47;

	private byte[] c48;

	private byte[] c49;

	private byte[] c5;

	private byte[] c50;

	private byte[] c51;

	private byte[] c52;

	private byte[] c53;

	private byte[] c54;

	private byte[] c55;

	private byte[] c56;

	private byte[] c57;

	private byte[] c58;

	private byte[] c59;

	private byte[] c6;

	private byte[] c60;

	private byte[] c61;

	private byte[] c62;

	private byte[] c63;

	private byte[] c64;

	private byte[] c65;

	private byte[] c66;

	private byte[] c67;

	private byte[] c68;

	private byte[] c69;

	private byte[] c7;

	private byte[] c70;

	private byte[] c71;

	private byte[] c72;

	private byte[] c73;

	private byte[] c74;

	private byte[] c75;

	private byte[] c76;

	private byte[] c77;

	private byte[] c78;

	private byte[] c79;

	private byte[] c8;

	private byte[] c9;

	private byte[] colidarray;

	private byte formatid;

	private int id;

	private short indid;

	private Timestamp moddate;

	private int partitionid;

	private int sequence;

	private int spare2;

	private int spare3;

	private short statid;

	private short ststatus;

	private byte usedcount;

	public Sysstatistic() {
	}

	public byte[] getC0() {
		return this.c0;
	}

	public void setC0(byte[] c0) {
		this.c0 = c0;
	}

	public byte[] getC1() {
		return this.c1;
	}

	public void setC1(byte[] c1) {
		this.c1 = c1;
	}

	public byte[] getC10() {
		return this.c10;
	}

	public void setC10(byte[] c10) {
		this.c10 = c10;
	}

	public byte[] getC11() {
		return this.c11;
	}

	public void setC11(byte[] c11) {
		this.c11 = c11;
	}

	public byte[] getC12() {
		return this.c12;
	}

	public void setC12(byte[] c12) {
		this.c12 = c12;
	}

	public byte[] getC13() {
		return this.c13;
	}

	public void setC13(byte[] c13) {
		this.c13 = c13;
	}

	public byte[] getC14() {
		return this.c14;
	}

	public void setC14(byte[] c14) {
		this.c14 = c14;
	}

	public byte[] getC15() {
		return this.c15;
	}

	public void setC15(byte[] c15) {
		this.c15 = c15;
	}

	public byte[] getC16() {
		return this.c16;
	}

	public void setC16(byte[] c16) {
		this.c16 = c16;
	}

	public byte[] getC17() {
		return this.c17;
	}

	public void setC17(byte[] c17) {
		this.c17 = c17;
	}

	public byte[] getC18() {
		return this.c18;
	}

	public void setC18(byte[] c18) {
		this.c18 = c18;
	}

	public byte[] getC19() {
		return this.c19;
	}

	public void setC19(byte[] c19) {
		this.c19 = c19;
	}

	public byte[] getC2() {
		return this.c2;
	}

	public void setC2(byte[] c2) {
		this.c2 = c2;
	}

	public byte[] getC20() {
		return this.c20;
	}

	public void setC20(byte[] c20) {
		this.c20 = c20;
	}

	public byte[] getC21() {
		return this.c21;
	}

	public void setC21(byte[] c21) {
		this.c21 = c21;
	}

	public byte[] getC22() {
		return this.c22;
	}

	public void setC22(byte[] c22) {
		this.c22 = c22;
	}

	public byte[] getC23() {
		return this.c23;
	}

	public void setC23(byte[] c23) {
		this.c23 = c23;
	}

	public byte[] getC24() {
		return this.c24;
	}

	public void setC24(byte[] c24) {
		this.c24 = c24;
	}

	public byte[] getC25() {
		return this.c25;
	}

	public void setC25(byte[] c25) {
		this.c25 = c25;
	}

	public byte[] getC26() {
		return this.c26;
	}

	public void setC26(byte[] c26) {
		this.c26 = c26;
	}

	public byte[] getC27() {
		return this.c27;
	}

	public void setC27(byte[] c27) {
		this.c27 = c27;
	}

	public byte[] getC28() {
		return this.c28;
	}

	public void setC28(byte[] c28) {
		this.c28 = c28;
	}

	public byte[] getC29() {
		return this.c29;
	}

	public void setC29(byte[] c29) {
		this.c29 = c29;
	}

	public byte[] getC3() {
		return this.c3;
	}

	public void setC3(byte[] c3) {
		this.c3 = c3;
	}

	public byte[] getC30() {
		return this.c30;
	}

	public void setC30(byte[] c30) {
		this.c30 = c30;
	}

	public byte[] getC31() {
		return this.c31;
	}

	public void setC31(byte[] c31) {
		this.c31 = c31;
	}

	public byte[] getC32() {
		return this.c32;
	}

	public void setC32(byte[] c32) {
		this.c32 = c32;
	}

	public byte[] getC33() {
		return this.c33;
	}

	public void setC33(byte[] c33) {
		this.c33 = c33;
	}

	public byte[] getC34() {
		return this.c34;
	}

	public void setC34(byte[] c34) {
		this.c34 = c34;
	}

	public byte[] getC35() {
		return this.c35;
	}

	public void setC35(byte[] c35) {
		this.c35 = c35;
	}

	public byte[] getC36() {
		return this.c36;
	}

	public void setC36(byte[] c36) {
		this.c36 = c36;
	}

	public byte[] getC37() {
		return this.c37;
	}

	public void setC37(byte[] c37) {
		this.c37 = c37;
	}

	public byte[] getC38() {
		return this.c38;
	}

	public void setC38(byte[] c38) {
		this.c38 = c38;
	}

	public byte[] getC39() {
		return this.c39;
	}

	public void setC39(byte[] c39) {
		this.c39 = c39;
	}

	public byte[] getC4() {
		return this.c4;
	}

	public void setC4(byte[] c4) {
		this.c4 = c4;
	}

	public byte[] getC40() {
		return this.c40;
	}

	public void setC40(byte[] c40) {
		this.c40 = c40;
	}

	public byte[] getC41() {
		return this.c41;
	}

	public void setC41(byte[] c41) {
		this.c41 = c41;
	}

	public byte[] getC42() {
		return this.c42;
	}

	public void setC42(byte[] c42) {
		this.c42 = c42;
	}

	public byte[] getC43() {
		return this.c43;
	}

	public void setC43(byte[] c43) {
		this.c43 = c43;
	}

	public byte[] getC44() {
		return this.c44;
	}

	public void setC44(byte[] c44) {
		this.c44 = c44;
	}

	public byte[] getC45() {
		return this.c45;
	}

	public void setC45(byte[] c45) {
		this.c45 = c45;
	}

	public byte[] getC46() {
		return this.c46;
	}

	public void setC46(byte[] c46) {
		this.c46 = c46;
	}

	public byte[] getC47() {
		return this.c47;
	}

	public void setC47(byte[] c47) {
		this.c47 = c47;
	}

	public byte[] getC48() {
		return this.c48;
	}

	public void setC48(byte[] c48) {
		this.c48 = c48;
	}

	public byte[] getC49() {
		return this.c49;
	}

	public void setC49(byte[] c49) {
		this.c49 = c49;
	}

	public byte[] getC5() {
		return this.c5;
	}

	public void setC5(byte[] c5) {
		this.c5 = c5;
	}

	public byte[] getC50() {
		return this.c50;
	}

	public void setC50(byte[] c50) {
		this.c50 = c50;
	}

	public byte[] getC51() {
		return this.c51;
	}

	public void setC51(byte[] c51) {
		this.c51 = c51;
	}

	public byte[] getC52() {
		return this.c52;
	}

	public void setC52(byte[] c52) {
		this.c52 = c52;
	}

	public byte[] getC53() {
		return this.c53;
	}

	public void setC53(byte[] c53) {
		this.c53 = c53;
	}

	public byte[] getC54() {
		return this.c54;
	}

	public void setC54(byte[] c54) {
		this.c54 = c54;
	}

	public byte[] getC55() {
		return this.c55;
	}

	public void setC55(byte[] c55) {
		this.c55 = c55;
	}

	public byte[] getC56() {
		return this.c56;
	}

	public void setC56(byte[] c56) {
		this.c56 = c56;
	}

	public byte[] getC57() {
		return this.c57;
	}

	public void setC57(byte[] c57) {
		this.c57 = c57;
	}

	public byte[] getC58() {
		return this.c58;
	}

	public void setC58(byte[] c58) {
		this.c58 = c58;
	}

	public byte[] getC59() {
		return this.c59;
	}

	public void setC59(byte[] c59) {
		this.c59 = c59;
	}

	public byte[] getC6() {
		return this.c6;
	}

	public void setC6(byte[] c6) {
		this.c6 = c6;
	}

	public byte[] getC60() {
		return this.c60;
	}

	public void setC60(byte[] c60) {
		this.c60 = c60;
	}

	public byte[] getC61() {
		return this.c61;
	}

	public void setC61(byte[] c61) {
		this.c61 = c61;
	}

	public byte[] getC62() {
		return this.c62;
	}

	public void setC62(byte[] c62) {
		this.c62 = c62;
	}

	public byte[] getC63() {
		return this.c63;
	}

	public void setC63(byte[] c63) {
		this.c63 = c63;
	}

	public byte[] getC64() {
		return this.c64;
	}

	public void setC64(byte[] c64) {
		this.c64 = c64;
	}

	public byte[] getC65() {
		return this.c65;
	}

	public void setC65(byte[] c65) {
		this.c65 = c65;
	}

	public byte[] getC66() {
		return this.c66;
	}

	public void setC66(byte[] c66) {
		this.c66 = c66;
	}

	public byte[] getC67() {
		return this.c67;
	}

	public void setC67(byte[] c67) {
		this.c67 = c67;
	}

	public byte[] getC68() {
		return this.c68;
	}

	public void setC68(byte[] c68) {
		this.c68 = c68;
	}

	public byte[] getC69() {
		return this.c69;
	}

	public void setC69(byte[] c69) {
		this.c69 = c69;
	}

	public byte[] getC7() {
		return this.c7;
	}

	public void setC7(byte[] c7) {
		this.c7 = c7;
	}

	public byte[] getC70() {
		return this.c70;
	}

	public void setC70(byte[] c70) {
		this.c70 = c70;
	}

	public byte[] getC71() {
		return this.c71;
	}

	public void setC71(byte[] c71) {
		this.c71 = c71;
	}

	public byte[] getC72() {
		return this.c72;
	}

	public void setC72(byte[] c72) {
		this.c72 = c72;
	}

	public byte[] getC73() {
		return this.c73;
	}

	public void setC73(byte[] c73) {
		this.c73 = c73;
	}

	public byte[] getC74() {
		return this.c74;
	}

	public void setC74(byte[] c74) {
		this.c74 = c74;
	}

	public byte[] getC75() {
		return this.c75;
	}

	public void setC75(byte[] c75) {
		this.c75 = c75;
	}

	public byte[] getC76() {
		return this.c76;
	}

	public void setC76(byte[] c76) {
		this.c76 = c76;
	}

	public byte[] getC77() {
		return this.c77;
	}

	public void setC77(byte[] c77) {
		this.c77 = c77;
	}

	public byte[] getC78() {
		return this.c78;
	}

	public void setC78(byte[] c78) {
		this.c78 = c78;
	}

	public byte[] getC79() {
		return this.c79;
	}

	public void setC79(byte[] c79) {
		this.c79 = c79;
	}

	public byte[] getC8() {
		return this.c8;
	}

	public void setC8(byte[] c8) {
		this.c8 = c8;
	}

	public byte[] getC9() {
		return this.c9;
	}

	public void setC9(byte[] c9) {
		this.c9 = c9;
	}

	public byte[] getColidarray() {
		return this.colidarray;
	}

	public void setColidarray(byte[] colidarray) {
		this.colidarray = colidarray;
	}

	public byte getFormatid() {
		return this.formatid;
	}

	public void setFormatid(byte formatid) {
		this.formatid = formatid;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getIndid() {
		return this.indid;
	}

	public void setIndid(short indid) {
		this.indid = indid;
	}

	public Timestamp getModdate() {
		return this.moddate;
	}

	public void setModdate(Timestamp moddate) {
		this.moddate = moddate;
	}

	public int getPartitionid() {
		return this.partitionid;
	}

	public void setPartitionid(int partitionid) {
		this.partitionid = partitionid;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getSpare2() {
		return this.spare2;
	}

	public void setSpare2(int spare2) {
		this.spare2 = spare2;
	}

	public int getSpare3() {
		return this.spare3;
	}

	public void setSpare3(int spare3) {
		this.spare3 = spare3;
	}

	public short getStatid() {
		return this.statid;
	}

	public void setStatid(short statid) {
		this.statid = statid;
	}

	public short getStstatus() {
		return this.ststatus;
	}

	public void setStstatus(short ststatus) {
		this.ststatus = ststatus;
	}

	public byte getUsedcount() {
		return this.usedcount;
	}

	public void setUsedcount(byte usedcount) {
		this.usedcount = usedcount;
	}

}