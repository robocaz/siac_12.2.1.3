angular.module('ccs').factory('MediosResource', function ($resource) {
    var resource = $resource('rest/mediopago',{
        nocache : new Date().getTime()
    },{
        'cargaMedios' :  {  method : 'GET', isArray : true, url: 'rest/mediopago/combomedios'},
        'cargaBoletas' :  {  method : 'POST', isArray : false, url: 'rest/mediopago/cargaboleta'},
        'cajasabiertas'   : { method : 'GET', isArray: true,  url : 'rest/caja/cajasabiertas' },
        'actualizaMetodo' :  {  method : 'POST', isArray : false, url: 'rest/mediopago/actualizamedio'},
        'cargaBancos' :  {  method : 'GET', isArray : true, url: 'rest/mediopago/combobancos'},
        'cargaSucursales' :  {  method : 'POST', isArray : true, url: 'rest/mediopago/combosucursales'}
    });
    return resource;
});