package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsuarioCCosto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsuarioCCosto.findAll", query="SELECT t FROM Tbl_UsuarioCCosto t")
public class Tbl_UsuarioCCosto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Activo")
	private boolean fld_Activo;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	public Tbl_UsuarioCCosto() {
	}

	public boolean getFld_Activo() {
		return this.fld_Activo;
	}

	public void setFld_Activo(boolean fld_Activo) {
		this.fld_Activo = fld_Activo;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

}