package cl.ccs.siac.certificados;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.CertificadoDao;
import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;

@Named("certificadosBusiness")
public class CertificadosBusinessImpl implements CertificadosBusiness {

	@Inject
	private CertificadoDao certificadoDao;

	public ConsultaBICDTO generaCOACBIC(String user, CertificadoDTO certificado) {
		return certificadoDao.generaCOACBIC(user, certificado);
	}

	public ConsultaMOLDTO generaCOACMOL(String user, CertificadoDTO certificado, String codAutenticacionBIC) {
		return certificadoDao.generaCOACMOL(user, certificado, codAutenticacionBIC);
	}

	public ConsultaCerAclDTO generaCAA(ConsultaCerAclDTO consultaCerAclDTO) {
		ConsultaCerAclDTO certificado = certificadoDao.generaCAA(consultaCerAclDTO);
		certificado.setCodAutenticBic(certificado.getCodAutenticBic() + " - 0");
		return certificado;
	}

	public CertificadoAclDTO generaCAC(ConsultaCerAclDTO consultaCerAclDTO) {
		CertificadoAclDTO certificado = certificadoDao.generaCAC(consultaCerAclDTO);
		certificado.setCodAutenticBic(certificado.getCodAutenticBic() + " - 0");
		return certificado;
	}

	@Override
	public ConsCertificadoILPDTO generaILP(ConsCertificadoILPDTO consCertificado) {
		ConsCertificadoILPDTO certificadoILP = certificadoDao.generaILP(consCertificado);
		certificadoILP.setCodAutenticBic(certificadoILP.getCodAutenticBic() + " - 0");
		return certificadoILP;
	}

	@Override
	public ConsCertificadoICMDTO generaICM(ConsCertificadoICMDTO consCertificado) {
		ConsCertificadoICMDTO certificadoICM = certificadoDao.generaICM(consCertificado);
		certificadoICM.setCodAutenticBic(certificadoICM.getCodAutenticBic() + " - 0");
		return certificadoICM;
	}
	
	public TeAvisaDTO getDatosContratoTeAvisa(TeAvisaDTO contrato){
		contrato = certificadoDao.getDatosContratoTeAvisa(contrato);
		return contrato;
		
	}
}
