package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ArqueoCajaConDocsData;
import cl.exe.ccs.dto.ArqueoCajaInDTO;
import cl.exe.ccs.dto.ArqueoCajaTipoPagoData;
import cl.exe.ccs.dto.ArqueoCajaVtasBolFactEmiOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasConDctoOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasTipoPagoOutDTO;

@Named("arqueoCajaDao")
@Stateless
public class ArqueoCajaDaoImpl implements ArqueoCajaDao {
	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;
	
	private static final Logger LOG = LogManager.getLogger(ArqueoCajaDaoImpl.class);
	
	@Override
	public ArqueoCajaVtasTipoPagoOutDTO vtasTipoPago(ArqueoCajaInDTO params) {
		LOG.debug("> DAO: vtasTipoPago - SP: SQ_VtasTipoPago_SW2");
		
		ArqueoCajaVtasTipoPagoOutDTO out = new ArqueoCajaVtasTipoPagoOutDTO();
		List<ArqueoCajaTipoPagoData> lista = new ArrayList<ArqueoCajaTipoPagoData>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_VtasTipoPago_SW2");
			
			spq.registerStoredProcedureParameter(1, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
			
			spq.setParameter(1, Timestamp.valueOf(params.getFechaInicio()));
			spq.setParameter(2, params.getCodUsuario());
			spq.setParameter(3, params.getCodCC());
			LOG.debug("> Params: " + params);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			if ( result != null && !result.isEmpty() ) {
				
				for ( Object row : result ) {
					Object[] data = (Object[]) row;
					ArqueoCajaTipoPagoData fila = new ArqueoCajaTipoPagoData();
					fila.setGlosa(data[0].toString());
					fila.setTrxs((Integer) data[1]);
					fila.setMntoIngresado(((BigDecimal) data[2]).intValue());
					fila.setMntoCobrado(((BigDecimal) data[3]).intValue());
					fila.setMntoDiff(((BigDecimal) data[4]).intValue());
					lista.add(fila);
				}
				
			}
			
			out.setLista(lista);
			
			out.setTotalTrxs((Integer) spq.getOutputParameterValue(4));
			out.setTotalPagado((Integer) spq.getOutputParameterValue(5));
			out.setTotalCalc((Integer) spq.getOutputParameterValue(6));
			out.setTotalDiff((Integer) spq.getOutputParameterValue(7));
			out.setTotalDctos((Integer) spq.getOutputParameterValue(8));
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SQ_VtasTipoPago_SW2 " + e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ArqueoCajaVtasConDctoOutDTO vtasConDcto(ArqueoCajaInDTO params) {
		LOG.debug("> DAO: vtasConDcto - SP: SQ_VtasConDcto_SW2");
		
		ArqueoCajaVtasConDctoOutDTO out = new ArqueoCajaVtasConDctoOutDTO();
		List<ArqueoCajaConDocsData> lista = new ArrayList<ArqueoCajaConDocsData>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_VtasConDcto_SW2");
			
			spq.registerStoredProcedureParameter(1, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
			
			spq.setParameter(1, Timestamp.valueOf(params.getFechaInicio()));
			spq.setParameter(2, params.getCodUsuario());
			spq.setParameter(3, params.getCodCC());
			LOG.debug("> Params: " + params);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			if ( result != null && !result.isEmpty() ) {
				
				for ( Object row : result ) {
					Object[] data = (Object[]) row;
					ArqueoCajaConDocsData fila = new ArqueoCajaConDocsData();
					fila.setNumero(data[0].toString());
					fila.setMntoTrx(((BigDecimal) data[1]).intValue());
					fila.setTipoPago(data[2].toString());
					fila.setGlosa(data[3].toString());
					lista.add(fila);
				}
				
			}
			
			out.setLista(lista);
			
			out.setMontoTotal((Integer) spq.getOutputParameterValue(4));
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SQ_VtasConDcto_SW2 " + e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ArqueoCajaVtasBolFactEmiOutDTO vtasBolFactEmi(ArqueoCajaInDTO params) {
		LOG.debug("> DAO: vtasBolFactEmi - SP: SQ_VtasBolFactEmi_SW2");
		
		ArqueoCajaVtasBolFactEmiOutDTO out = new ArqueoCajaVtasBolFactEmiOutDTO();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_VtasBolFactEmi_SW2");
			
			spq.registerStoredProcedureParameter(1, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(9, Integer.class, ParameterMode.OUT);
			
			spq.setParameter(1, Timestamp.valueOf(params.getFechaInicio()));
			spq.setParameter(2, params.getCodUsuario());
			spq.setParameter(3, params.getCodCC());
			LOG.debug("> Params: " + params);
			
			spq.execute();
			
			out.setTotBolNulas((Integer) spq.getOutputParameterValue(4));
			out.setTotBolEmitidas((Integer) spq.getOutputParameterValue(5));
			out.setTotFacEmitidas((Integer) spq.getOutputParameterValue(6));
			out.setTotFacNulas((Integer) spq.getOutputParameterValue(7));
			out.setTotFolCeroNulo((Integer) spq.getOutputParameterValue(8));
			out.setTotFolCeroEmit((Integer) spq.getOutputParameterValue(9));
			
			out.setIdControl(1);
			out.setMsgControl("OK");
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SQ_VtasBolFactEmi_SW2 " + e.getMessage());
		}
		
		return out;
	}

}
