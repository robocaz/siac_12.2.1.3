package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RegistroClaveAcceso database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RegistroClaveAcceso.findAll", query="SELECT t FROM Tbl_RegistroClaveAcceso t")
public class Tbl_RegistroClaveAcceso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ApMat")
	private String fld_ApMat;

	@Column(name="Fld_ApPat")
	private String fld_ApPat;

	@Column(name="Fld_ClaveAcceso")
	private String fld_ClaveAcceso;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_ContConsultas")
	private int fld_ContConsultas;

	@Column(name="Fld_CorrRegistro_Id")
	private BigDecimal fld_CorrRegistro_Id;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_EnvioEmail")
	private boolean fld_EnvioEmail;

	@Column(name="Fld_Fax")
	private String fld_Fax;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecNacimiento")
	private Timestamp fld_FecNacimiento;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FlagHabilitacion")
	private boolean fld_FlagHabilitacion;

	@Column(name="Fld_Nombres")
	private String fld_Nombres;

	@Column(name="Fld_NombreSolicitante")
	private String fld_NombreSolicitante;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_Telefono1")
	private String fld_Telefono1;

	@Column(name="Fld_Telefono2")
	private String fld_Telefono2;

	@Column(name="Fld_Telefono3")
	private String fld_Telefono3;

	public Tbl_RegistroClaveAcceso() {
	}

	public String getFld_ApMat() {
		return this.fld_ApMat;
	}

	public void setFld_ApMat(String fld_ApMat) {
		this.fld_ApMat = fld_ApMat;
	}

	public String getFld_ApPat() {
		return this.fld_ApPat;
	}

	public void setFld_ApPat(String fld_ApPat) {
		this.fld_ApPat = fld_ApPat;
	}

	public String getFld_ClaveAcceso() {
		return this.fld_ClaveAcceso;
	}

	public void setFld_ClaveAcceso(String fld_ClaveAcceso) {
		this.fld_ClaveAcceso = fld_ClaveAcceso;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public int getFld_ContConsultas() {
		return this.fld_ContConsultas;
	}

	public void setFld_ContConsultas(int fld_ContConsultas) {
		this.fld_ContConsultas = fld_ContConsultas;
	}

	public BigDecimal getFld_CorrRegistro_Id() {
		return this.fld_CorrRegistro_Id;
	}

	public void setFld_CorrRegistro_Id(BigDecimal fld_CorrRegistro_Id) {
		this.fld_CorrRegistro_Id = fld_CorrRegistro_Id;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public boolean getFld_EnvioEmail() {
		return this.fld_EnvioEmail;
	}

	public void setFld_EnvioEmail(boolean fld_EnvioEmail) {
		this.fld_EnvioEmail = fld_EnvioEmail;
	}

	public String getFld_Fax() {
		return this.fld_Fax;
	}

	public void setFld_Fax(String fld_Fax) {
		this.fld_Fax = fld_Fax;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecNacimiento() {
		return this.fld_FecNacimiento;
	}

	public void setFld_FecNacimiento(Timestamp fld_FecNacimiento) {
		this.fld_FecNacimiento = fld_FecNacimiento;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_FlagHabilitacion() {
		return this.fld_FlagHabilitacion;
	}

	public void setFld_FlagHabilitacion(boolean fld_FlagHabilitacion) {
		this.fld_FlagHabilitacion = fld_FlagHabilitacion;
	}

	public String getFld_Nombres() {
		return this.fld_Nombres;
	}

	public void setFld_Nombres(String fld_Nombres) {
		this.fld_Nombres = fld_Nombres;
	}

	public String getFld_NombreSolicitante() {
		return this.fld_NombreSolicitante;
	}

	public void setFld_NombreSolicitante(String fld_NombreSolicitante) {
		this.fld_NombreSolicitante = fld_NombreSolicitante;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public String getFld_Telefono1() {
		return this.fld_Telefono1;
	}

	public void setFld_Telefono1(String fld_Telefono1) {
		this.fld_Telefono1 = fld_Telefono1;
	}

	public String getFld_Telefono2() {
		return this.fld_Telefono2;
	}

	public void setFld_Telefono2(String fld_Telefono2) {
		this.fld_Telefono2 = fld_Telefono2;
	}

	public String getFld_Telefono3() {
		return this.fld_Telefono3;
	}

	public void setFld_Telefono3(String fld_Telefono3) {
		this.fld_Telefono3 = fld_Telefono3;
	}

}