/**
 * 
 */
package cl.ccs.siac.utils.certificados;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.utils.CertificadosUtil;
import cl.exe.ccs.dto.certificados.CertificadoICMDTO;
import cl.exe.ccs.dto.certificados.CertificadoICMPaginacionDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;

/**
 * @author emiranda
 *
 */
public class CertificadoICMUtils {

	private static final Logger LOG = LogManager.getLogger(CertificadoICMUtils.class);

	public static Map<String, Object> getParametrosICM(ConsCertificadoICMDTO consCertificadoICMDTO) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		SimpleDateFormat of = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			String rut = consCertificadoICMDTO.getRutAfectado();
			Boolean juridico = Long.parseLong(rut.substring(0, rut.indexOf('-'))) > 50000000 ? true : false;
			parametros.put("isJuridico", juridico);
			parametros.put("nroBoletin", consCertificadoICMDTO.getNumeroAcls());
			parametros.put("pRut",
					CertificadosUtil.getRut(consCertificadoICMDTO.getRutAfectado().toUpperCase().trim()));
			if (juridico) {
				parametros.put("pNombre",
						(((consCertificadoICMDTO.getAppPat() != null)
								? consCertificadoICMDTO.getAppPat().toUpperCase().trim() : "")
								+ ((consCertificadoICMDTO.getAppMat() != null)
										? consCertificadoICMDTO.getAppMat().toUpperCase().trim() : ""))
								+ ((consCertificadoICMDTO.getNombres() != null)
										? consCertificadoICMDTO.getNombres().toUpperCase().trim() : ""));
			} else {
				parametros.put("pNombre",
						(((consCertificadoICMDTO.getAppPat() != null)
								? consCertificadoICMDTO.getAppPat().toUpperCase().trim() : "")
								+ ", "
								+ ((consCertificadoICMDTO.getAppMat() != null)
										? consCertificadoICMDTO.getAppMat().toUpperCase().trim() : ""))
								+ ", " + ((consCertificadoICMDTO.getNombres() != null)
										? consCertificadoICMDTO.getNombres().toUpperCase().trim() : ""));

			}
			parametros.put("cantDocsVigentes", consCertificadoICMDTO.getCertICMResult().size());
			parametros.put("cantBoletinComercial", consCertificadoICMDTO.getCertICMResult().size());
			parametros.put("user", consCertificadoICMDTO.getCodUsuario());
			parametros.put("sucursal", consCertificadoICMDTO.getSucursal());
			parametros.put("codVerificacion", consCertificadoICMDTO.getCodAutenticBic());
			parametros.put("documentos", formatData(consCertificadoICMDTO.getCertICMResult()));
			parametros.put("nroCheques", countByType(consCertificadoICMDTO.getCertICMResult(), "CH"));
			parametros.put("nroLetras", (countByType(consCertificadoICMDTO.getCertICMResult(), "LT"))
					+ (countByType(consCertificadoICMDTO.getCertICMResult(), "PG")));
			parametros.put("nroCuotas", countByType(consCertificadoICMDTO.getCertICMResult(), "CM"));
			parametros.put("nroRentas", countByType(consCertificadoICMDTO.getCertICMResult(), "RA"));

			// Paginacion ILP
			List<CertificadoICMDTO> certificados = formatData(consCertificadoICMDTO.getCertICMResult());
			List<CertificadoICMPaginacionDTO> paginado = new ArrayList<CertificadoICMPaginacionDTO>();

			Integer totalBc = certificados.size();
			Integer maxBC = 26;
			Integer paginas = 1;

			// bc
			Integer bc_inicio = 1;
			Integer bc_termino = bc_inicio + CertificadosUtil.getPaginas(totalBc, maxBC, 0);
			Integer bc_resto = (bc_termino * maxBC) - totalBc;

			paginas = bc_termino;

			// elaboracion lista de paginas y bloques
			for (int p = 0; p < paginas; p++) {
				int pag = p + 1;
				int resto = 0;

				CertificadoICMPaginacionDTO paginacionICM = new CertificadoICMPaginacionDTO();
				paginacionICM.setPagina(pag);

				// bc
				if (totalBc > 0) {
					paginacionICM.setBc_inicio(pag == bc_inicio ? 0 : paginado.get(p - 1).getBc_termino());
					int diff_bc = totalBc - paginacionICM.getBc_inicio();
					diff_bc = diff_bc > maxBC ? maxBC : diff_bc;
					resto = diff_bc < maxBC ? maxBC - diff_bc : 0;
					paginacionICM.setBc_termino(paginacionICM.getBc_inicio() + diff_bc);
					if (paginacionICM.getBc_inicio() == paginacionICM.getBc_termino()) {
						paginacionICM.getBc_coleccion().add(certificados.get(paginacionICM.getBc_inicio()));
					} else {
						paginacionICM.setBc_coleccion(
								certificados.subList(paginacionICM.getBc_inicio(), paginacionICM.getBc_termino()));
					}
				}

				paginado.add(paginacionICM);
			}

			parametros.put("COLECCION_PAG", paginado);

			parametros.put("TOTAL_BC", totalBc);
			parametros.put("MAX_POR_PAGINA", maxBC);
			parametros.put("PAGINAS", paginas);

			parametros.put("BC_INICIO", bc_inicio);
			parametros.put("BC_TERMINO", bc_termino);

			parametros.put("SUBREPORT_DIR", "wlscomun/Certificados/");

		} catch (Exception e) {
			LOG.error("Error formando parametros para certificado ICM.");
			e.printStackTrace();
		}

		return parametros;

	}

	private static Integer countByType(List<CertificadoICMDTO> certILPResult, String type) {
		int count = 0;
		for (CertificadoICMDTO c : certILPResult) {
			if (c.getTipoDocumento().trim().equals(type)) {
				count++;
			}
		}

		return count;
	}

	private static List<CertificadoICMDTO> formatData(List<CertificadoICMDTO> certICMResult) {
		List<CertificadoICMDTO> resultSet = new ArrayList<CertificadoICMDTO>();
		for (CertificadoICMDTO c : certICMResult) {
			CertificadoICMDTO cert = new CertificadoICMDTO();
			cert.setCorrAutenticacion(c.getCorrAutenticacion());
			cert.setCiudad(c.getCiudad().trim());
			cert.setCodEmisor(c.getCodEmisor().trim());
			cert.setFechaProt(c.getFechaProt());
			cert.setFechaPublicacion(c.getFechaPublicacion());
			cert.setGlosaCorta(c.getGlosaCorta().trim());
			cert.setGlosaEmisor(c.getGlosaEmisor().trim());
			cert.setMontoProt(CertificadosUtil.getMonto(new BigDecimal(c.getMontoProt())));
			cert.setNombreLiberador(c.getNombreLiberador().trim());
			cert.setNumBoletinProt(c.getNumBoletinProt());
			cert.setNumOperDig(c.getNumOperDig());
			cert.setPagBoletinProt(c.getPagBoletinProt());
			cert.setRut(CertificadosUtil.getRut(c.getRut().trim()));
			cert.setTipoDocumento(c.getTipoDocumento().trim());
			cert.setTipoDocumentoImpago(c.getTipoDocumentoImpago().trim());
			cert.setMarcaAclaracion(c.getMarcaAclaracion());
			cert.setFechaAclaracion(c.getFechaAclaracion());
			resultSet.add(cert);
		}

		return resultSet;
	}

}
