package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EmisorConsolidado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EmisorConsolidado.findAll", query="SELECT t FROM Tbl_EmisorConsolidado t")
public class Tbl_EmisorConsolidado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_FlagContrato")
	private byte fld_FlagContrato;

	@Column(name="Fld_RutEmisor")
	private String fld_RutEmisor;

	public Tbl_EmisorConsolidado() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public byte getFld_FlagContrato() {
		return this.fld_FlagContrato;
	}

	public void setFld_FlagContrato(byte fld_FlagContrato) {
		this.fld_FlagContrato = fld_FlagContrato;
	}

	public String getFld_RutEmisor() {
		return this.fld_RutEmisor;
	}

	public void setFld_RutEmisor(String fld_RutEmisor) {
		this.fld_RutEmisor = fld_RutEmisor;
	}

}