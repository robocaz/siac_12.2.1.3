package cl.ccs.siac.pos.printer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Named;

/**
 * Class declaration
 *
 *
 * @author
 * @version 1.10, 08/04/00
 */
@Named("impresora")
@Stateless
public class Impresora {
	// Variables de acceso al dispositivo
	private FileWriter fw;
	private BufferedWriter bw;
	private PrintWriter pw;
	private String dispositivo = "";

	/** Esta funcion inicia el dispositivo donde se va a imprimir **/

	public void setDispositivo(String texto) {
		dispositivo = texto;
		try {
			fw = new FileWriter(texto);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void inicializar() {
		try {
			char[] RATE = new char[] { 9600, 'r', 0 };
			pw.write(RATE);
			char[] STOPBITE = new char[] { 1, 'r', 0 };
			pw.write(STOPBITE);
			char[] PARITY = new char[] { 0, 'r', 0 };
			pw.write(PARITY);
			char[] DATABITS_8 = new char[] { 8, 'r', 0 };
			pw.write(DATABITS_8);
			//
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}
	
	
	public void escribir(String texto) {
		try {

			pw.println(texto);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void cortar() {
		try {

			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'm' };
			pw.write(ESC_CUT_PAPER);
			

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void avanza_pagina() {
		try {
			pw.write(0x0C);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void setRojo() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 1 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}

	}

	public void setNegro() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 0 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void setTipoCaracterLatino() {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, 'R', 18 };
			pw.write(ESC_CUT_PAPER);

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void setFormato(int formato) {
		try {
			char[] ESC_CUT_PAPER = new char[] { 0x1B, '!', (char) formato };
			pw.write(ESC_CUT_PAPER);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void correr(int fin) {
		try {
			int i = 0;
			for (i = 1; i <= fin; i++) {
				this.salto();
			}

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	public void salto() {
		try {

			pw.println("");

		} catch (Exception e) {
			System.out.print(e);

		}

	}

	public void dividir() {
		escribir("—————————————-");

	}

	public void cerrarDispositivo() {
		try {

			pw.close();
			bw.close();
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String args[]){
		

		
		
		String numeroBoleta = String.format("%07d", 78453);
		String corrCaja = String.format("%09d", 568222);
		String centroCosto = String.format("%04d", 2530);
		String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
		StringBuffer stb = new StringBuffer();

		
	
		stb.append("\n\n\n\n\n\n\n");
		stb.append("\n");
		stb.append("                                           #" + numeroBoleta);
		stb.append("   " + corrCaja + " " + centroCosto + "  " + date);
		
		
		Impresora p = new Impresora();
		p.setDispositivo("COM1");
		p.inicializar();
		p.escribir((char) 27 + "@");
		
		
//	    private void setSerialPortParameters() throws IOException {
//	        int baudRate = 9600;
//
//	        try {
//	            serialPort.setSerialPortParams(
//	                    baudRate,
//	                    SerialPort.DATABITS_8,
//	                    SerialPort.STOPBITS_1,
//	                    SerialPort.PARITY_NONE);
//
//	            serialPort.setFlowControlMode(
//	                    SerialPort.FLOWCONTROL_NONE);
//	        } catch (UnsupportedCommOperationException ex) {
//	            throw new IOException("Unsupported serial port parameter");
//	        }
//	    }
		p.setTipoCaracterLatino();
		p.setNegro();
		p.setFormato(10);
		p.escribir(stb.toString());
		p.escribir("\n");
		//iteracion
		for (int i = 0; i < 9; i++) {
			p.escribir("  " + "1 " + "   " +"descripcion qw" + "       "+ "4000");
		}
		//p.escribir("\n");
		p.escribir("                 " + "Total $36000");
		
		
		p.correr(10);
		p.cortar();
		p.cerrarDispositivo();
	}
}