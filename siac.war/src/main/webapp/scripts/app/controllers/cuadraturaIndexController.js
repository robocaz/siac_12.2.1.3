angular.module('ccs').controller('CuadraturaIndexController', function($rootScope, $scope, LoginResource, CuadraturaResource, NgTableParams, $location, $filter, $sce) {

	
	$scope.fechaDesde = {};
	$scope.fechaHasta = {};
	
	var pattern = /(\d{2})(\d{2})(\d{4})/;
	
	$scope.listadoCC = [];

	$scope.centroCosto = {
		descripcion : "",
		codigo : "",
		usuario : []

	}

	$scope.itemPadre = {
		descripcion : "Camara de Comercio",
		tipo : "CCS",
		divId : "CCS",
		centroCosto: 0
	}

	$scope.cuadraturaCCList = {};
	$scope.objSeleccionado = {};
	$scope.centroSeleccionado = {};
	$scope.muestraccs = false;

	$scope.cuadraturaGnrl = {};
	$scope.cuadXCss = false;
	$scope.tipoCuadratura = {}; 
	$scope.cajaSeleccionada = {};

	$scope.fechaDesde = $filter('date')(new Date(), "ddMMyyyy");
	$scope.fechaHasta = $filter('date')(new Date(), "ddMMyyyy");

	$scope.fechaDesdeCuad = "";
	$scope.fechaHastaCuad = "";
	$scope.usuarioCuad = "";
	$scope.centroCostoCuad = "";
	$scope.centroCostoCodigo = {};
	$scope.iva = 0;

	$scope.ocultaFecha = false;
	$scope.detalleBoletaObj = {};
	$scope.fechaSeleccion = {};

	$scope.totales = {
		totBrutoNumMovBoleta : 0,
		totBrutoTransaccionBoleta : 0,
		totNetoTransaccionBoleta : 0,
		totIvaTransaccionBoleta : 0,
		totBrutoCanceladoBoleta : 0,
		totNetoCanceladoBoleta : 0,
		totIvaCanceladoBoleta : 0,
		totBrutoDiferenciaBoleta : 0,
		totNetoDiferenciaBoleta : 0,
		totIvaDiferenciaBoleta : 0,

		totBrutoNumMovFactura: 0,
		totBrutoTransaccionFactura : 0,
		totNetoTransaccionFactura : 0,
		totIvaTransaccionFactura : 0,
		totBrutoCanceladoFactura : 0,
		totNetoCanceladoFactura : 0,
		totIvaCanceladoFactura : 0,
		totBrutoDiferenciaFactura : 0,
		totNetoDiferenciaFactura : 0,
		totIvaDiferenciaFactura : 0
	}
	
	$scope.disableForm = false;
	$scope.habilitaBoton = false;
	
	
	
	$scope.buscarCC = function(){
		
		$scope.itemPadre.nodes = [];
		$scope.listadoCC  = [];

		console.log($rootScope.usuario);
		
		//
		//if(!$rootScope.usuario.esAdministrador && !$rootScope.usuario.esSupervisor){
		//	swal('Alerta',"No tiene acceso a esta funcionalidad",'error');
		//	return false;
		//}


		if((angular.equals({}, $scope.fechaDesde) || angular.equals({}, $scope.fechaDesde) === "")) {
			swal('Alerta',"Ingrese fecha desde",'error');
			return false;

		}
		if(angular.equals({}, $scope.fechaHasta) || angular.equals({}, $scope.fechaHasta)){ 
			swal('Alerta',"Ingrese fecha hasta",'error');
			return false;
		}


		$scope.dateDesde = new Date($scope.fechaDesde.replace(pattern, '$3-$2-$1'));
		$scope.dateHasta = new Date($scope.fechaHasta.replace(pattern, '$3-$2-$1'));
		

		var desde = $filter('date')($scope.dateDesde, 'short');
		var hasta = $filter('date')($scope.dateHasta, 'short');
		var res = Date.parse(hasta) - Date.parse(desde);
		if (res < 0){
		   swal('Alerta',"Fecha Hasta no puede ser mayor a Fecha Desde",'error');
		   return false; 
		}
		
		var successCallback = function(data,responseHeaders){

			if(data.length > 0){
				angular.forEach(data, function(value, key) {
				 console.log(value.descripcion);
				 $scope.centroCosto = {};
				 $scope.centroCosto.descripcion = value.descripcion;
				 $scope.centroCosto.codigo = value.codigo;
				 $scope.centroCosto.tipo = value.codigoTipo;
				 $scope.centroCosto.divId = value.descripcion.replace(/\s+/g, '');
				 $scope.centroCosto.divId = $scope.centroCosto.divId.replace('.', '');
				 $scope.centroCosto.divId = $scope.centroCosto.divId.substring(0, $scope.centroCosto.divId.length-1);
				 $scope.centroCosto.nodes = [];
				 $scope.listadoCC.push($scope.centroCosto);
				});			

				if($rootScope.usuario.esAdministrador){
					$scope.muestraccs = true;
				}
			}
			else{
				 swal('Alerta',"No existen registros para este rango de fechas",'warning');
			}
		}
	  	CuadraturaResource.buscarCentroCostos({fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
	}

	$scope.calcDiff = function(firstDate, secondDate){
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
		var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
	}
	
	
	
	$scope.mensajeActualFecha = "";
	$scope.esfecha = function(fecha, tipofecha){
		
		var valor;
		if(fecha !== undefined  && !angular.equals({}, fecha)){
			dateArray = fecha.split("");

			if(dateArray.length > 0){
		        var fechaString = dateArray[0] + dateArray[1] + "/" + dateArray[2] + dateArray[3] + "/" + dateArray[4] + dateArray[5] + dateArray[6] + dateArray[7];
		 		
		 		valor = $scope.isValidDate(fechaString);
		 		if(!valor){
		 			
		 			if(tipofecha == "desde"){
		 				$scope.fechaDesde = "";
		 				swal('Alerta','Fecha de busqueda invalida','error');
		 				$("#fecha_desde").focus();
		 			}
		 			if(tipofecha == "inicio"){
		 				$scope.cuadraturaGnrl.fechaDesde = "";
		 				$scope.fechaDesdeCuad = "";
		 				if($scope.mensajeActualFecha !== ""){
								swal('Alerta',$scope.mensajeActualFecha,'error');
								$scope.mensajeActualFecha = "";
		 				}
		 				else{
		 					swal('Alerta','Fecha de busqueda invalida','error');
		 				}
		 				$("#fecha_inicio").focus();
		 			}		 			
		 			else if(tipofecha == "hasta"){
						$scope.fechaHasta = "";
						swal('Alerta','Fecha Hasta Invalida','error');
						$("#fecha_hasta").focus();
		 			}
		 			
		 		}
			}	
		}
	}
	$scope.isValidDate =  function(date) {

		var dateParts = date.split('/');
		var dateString = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]
	    var d = new Date(dateString);
		$scope.current = new Date();
		if(d > $scope.current){
			$scope.mensajeActualFecha = "Fecha de busqueda no puede ser mayor a la fecha actual"; 
			return false;
		}
	    return (d &&  d.getDate() == Number(dateParts[0])  && (d.getMonth() + 1) == dateParts[1] && d.getFullYear() == Number(dateParts[2]));
	}
  

    $scope.remove = function (scope) {
        scope.remove();
    };

    $scope.toggle = function (scope) {
       scope.toggle();
    };

    $scope.moveLastToTheBeginning = function () {
        var a = $scope.data.pop();
        $scope.data.splice(0, 0, a);
    };

    $scope.newSubItem = function (scope) {
        var nodeData = scope.$modelValue;
        //identificamos que tipo de busqueda es.

        if(nodeData.tipo == "CC"){

			var successCallback = function(data,responseHeaders){

				 console.log(data);
				 angular.forEach(data, function(value, key) {
					 nodeData.nodes.push({
			          centroCosto: value.centroCosto,
			          descripcion: value.usuario,
			          tipo: value.codigoTipo,
			          divId: value.usuario.trim(), 
			          fechaDesde: value.fechaDesde,
			          fechasHasta: value.fechaHasta,
			          nodes: []
			         });
			    });
			}
		  	CuadraturaResource.buscarUsuarios({centroCosto: nodeData.codigo, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);        	
        }


        if(nodeData.tipo == "user"){
			var successCallback = function(data,responseHeaders){

				 console.log(data);
				 angular.forEach(data, function(value, key) {
					 nodeData.nodes.push({
			          centroCosto: value.centroCosto,
			          descripcion: value.corrCaja + " - " + value.inicioCaja,
			          tipo: value.codigoTipo,
			          divId: "div"+value.corrCaja, 
			          fechas: value.inicioCaja,
			          usuario: nodeData.descripcion,
			          corrCaja: value.corrCaja,
			          nodes: []
			         });
			    });
			}
		  	CuadraturaResource.buscarCajas({usuario: nodeData.descripcion, centroCosto: nodeData.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
        }

    };


    $scope.selectItem = function(value){
		$scope.objSeleccionado = value;
    }

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
       $scope.$broadcast('angular-ui-tree:expand-all');
    };
    

    $scope.seleccionarTipoCuadratura = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CCS"){
			$scope.cuadraturaCamara();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "user"){
    		$scope.cuadraturaUsuario();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CC"){
    		$scope.cuadraturaCentroCosto();
    	}
    	else if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "caja"){
    		$scope.cuadraturaPorCajaArbol();
    	}

    }




    $scope.cuadraturaCamara = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CCS"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;
    		if($rootScope.usuario.esAdministrador){
    			centroCostoVar = 0;
    		}
    		else{
    			centroCostoVar = $rootScope.usuario.centroCostoActivo.codigo;
    		}

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaCCList = data;
				 angular.element("#reportecamara").modal()
			}
		  	CuadraturaResource.cuadraturaCCS({centroCosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta}  ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Camara de Comercio','error');
    		return false;
    	}

    	
    }
    



    $scope.cuadraturaUsuario = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "user"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;
    		if($rootScope.usuario.esAdministrador){
    			centroCostoVar = 0;
    		}
    		else{
    			centroCostoVar = $rootScope.usuario.centroCostoActivo.codigo;
    		}

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaUserList = data;
				 angular.element("#detalleusuario").modal()
			}
		  	CuadraturaResource.cuadraturaUser({usuario: $scope.objSeleccionado.descripcion, centroCosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Usuario','error');
    		return false;
    	}

    	
    }

	$scope.cuadraturaCentroCosto = function(){

    	if(!angular.equals({}, $scope.objSeleccionado) && $scope.objSeleccionado.tipo == "CC"){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;

			var successCallback = function(data,responseHeaders){
				 console.log(data);
				 $scope.cuadraturaDetCCList = data;
				 angular.element("#detallecc").modal()
			}
		  	CuadraturaResource.cuadraturaCC({centroCosto: $scope.objSeleccionado.codigo, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Centro de Costo','error');
    		return false;
    	}


	}

	
	$scope.obtenerDetalleCC = function(){
		console.log($scope.centroSeleccionado);
		$scope.centroCostoCodigo = $scope.centroSeleccionado.centroCosto;
		if($scope.centroCostoCodigo == 0){
    		swal('Alerta','Seleccione Centro de Costo distinto de "Todos"','error');
    		return false;
   		}
   		
    	if(!angular.equals({}, $scope.centroSeleccionado)){

    		console.log($rootScope.usuario);
    		var centroCostoVar = 0;

			var successCallback = function(data,responseHeaders){
				 console.log(data);

				 $scope.cuadraturaDetCCList = data;
				 angular.element("#detallecc").modal()
			}
		  	CuadraturaResource.cuadraturaCC({centroCosto: $scope.centroSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
    	}
    	else{
    		swal('Alerta','Seleccione Centro de Costo','error');
    		return false;
    	}
    	
    	
	}
	
	
	
	
	$scope.imprimeReporteCC = function(){
		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteCSS.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
	  	CuadraturaResource.printCuadraturaCC({centroCosto: 0, fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta} ,successCallback, $scope.errorCallback);
	}
	
	
	$scope.imprimeDetalleCajas = function(){
		
		console.log($scope.cuadraturaUserList);

		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteCajaCC.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}

		CuadraturaResource.printCuadraturaCaja($scope.cuadraturaDetCCList ,successCallback, $scope.errorCallback);
		
	}
	
	
	
	$scope.imprimeDetalleUsuario = function(){
		
		console.log($scope.cuadraturaUserList);

		var successCallback = function(data,responseHeaders){
			 console.log(data);
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "reporteUsuarioCC.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}

		CuadraturaResource.printCuadraturaUsuario($scope.cuadraturaUserList ,successCallback, $scope.errorCallback);
		
	}
	
	$scope.getValor = function(value){
		$scope.centroSeleccionado = value;
	}
	
	

    $scope.cerrarCuadCC = function(){
    	angular.element("#reportecamara").modal("hide");
    	$scope.muestraccs = true;
    	
    	//$scope.cuadraturaCCList = {};
    }

    $scope.cerrarCuadUsuario = function(){
    	angular.element("#detalleusuario").modal("hide");
    	$scope.muestraccs = true;
    	$scope.cuadraturaUserList = {};
    }


    $scope.cerrarCuadDetCC = function(){
    	angular.element("#detallecc").modal("hide");
    	$scope.muestraccs = true;
    	$scope.cuadraturaCCList.resultadoCentros.forEach(function (item, i) {
            item.centroSeleccionadocheck = false;
        });
        $scope.centroSeleccionado = {};
    	$scope.cuadraturaUserList = {};
    }

    $scope.cerrarBusqueda = function(){
    	angular.element("#informecc").modal("hide");
    	$scope.fechaHasta = "";
    	$scope.fechaDesde = "";
    	$scope.objSeleccionado = {};
    	$scope.listadoCC = [];
    	$scope.muestraccs = false;
    	$scope.centroCosto = {
			descripcion : "",
			codigo : "",
			usuario : []
		}
    }
    

    $scope.limpiar = function(){
    	$scope.fechaHasta = "";
    	$scope.fechaDesde = "";
    	$scope.objSeleccionado = {};
    	$scope.listadoCC = [];
    	$scope.muestraccs = false;
    	$scope.centroCosto = {
			descripcion : "",
			codigo : "",
			usuario : []
		}
    }
    
    
    
    
    
    
    $scope.cuadraturaGeneral = function(opcion){
    	
    	console.log($scope.centroSeleccionado);
		if((angular.equals({}, $scope.centroSeleccionado)) && opcion !== 3){
			swal('Alerta','Seleccione Centro de Costo','error');
			return false;
		}


    		var successCallback = function(data,responseHeaders){
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			if($scope.opcion == 1){
    				$scope.cuadraturaGnrl.impresion = 1;
    			}
    			else{
    				$scope.cuadraturaGnrl.impresion = 0;
    			}
    			
    			$scope.cuadraturaGnrl.centroCosto = $scope.centroSeleccionado.centroCosto;
    			if($scope.centroSeleccionado.glosaCC === undefined){
    				$scope.glosaCentro = "";
    			}
    			else{
    				$scope.glosaCentro = $scope.centroSeleccionado.glosaCC;
    			}
    			$scope.cuadraturaGnrl.glosaCC = $rootScope.usuario.centroCostoActivo.codigo + " - " + $rootScope.usuario.centroCostoActivo.descripcion;
    			if(angular.equals({}, $scope.fechaSeleccion)){
	    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
	    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
	    		}
	    		else{
	    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaSeleccion;
	    			$scope.cuadraturaGnrl.fechaHasta = "";	  
	    			$scope.ocultaFecha = true;  			
	    		}

    			
    			
    			
    			$scope.habilitaBoton = false;
				$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $rootScope.usuario.username;
				$scope.centroCostoCuad = $rootScope.usuario.centroCostoActivo.codigo;

				if($scope.usuarioCuad === undefined){
					$scope.cuadXCcs = true;
					$scope.disableForm = true;
					$scope.tipoCuadratura = "cc";
				}
				else{
					$scope.cuadXCcs = false;
					$scope.disableForm = false;	
					$scope.tipoCuadratura = "caja";				
				}

    			
				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					
					$scope.totales.totIvaTransaccionBoleta = $scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * $scope.iva);

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * $scope.iva);

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * $scope.iva);
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;
					$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * $scope.iva);

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * $scope.iva);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * $scope.iva);



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);    			
    			$scope.cuadraturaGnrl.totales = $scope.totales;
    			if($scope.cuadraturaGnrl.centroCosto > 0){
    				$scope.buscaCajas();
    			}
    			if(opcion == 3){
    				$scope.buscaCajas();
    				$scope.cuadraturaGnrl.totales = {}
    				$scope.cuadraturaGnrl.movimientos = [];
    				$scope.totales = {};
	    			$scope.cuadraturaGnrl.totalAviPago = 0;
					$scope.cuadraturaGnrl.totalBoletas = 0;
					$scope.cuadraturaGnrl.totalDiferencia = 0;
					$scope.cuadraturaGnrl.totalFacturas = 0;
					$scope.cuadraturaGnrl.totalMovimientos = 0;
					$scope.cuadraturaGnrl.totalPagado = 0;
					$scope.cuadraturaGnrl.totalTransacciones = 0;    				
    				angular.element("#cuadraturaindex").modal();
    			}
    			else{
    				angular.element("#cuadratura").modal();
    			}
    		}

    		if(opcion == 1){
    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.centroSeleccionado.centroCosto, 
    											fechaDesde: $scope.fechaDesde, fechaHasta: $scope.fechaHasta}
    											 ,successCallback, $scope.errorCallback);
    		}
    		if(opcion == 0){

    			var fechaCaja = $scope.centroSeleccionado.fechaInicio.substring(0,10);
    			$scope.fechaSeleccion = fechaCaja.split("-")[2]+fechaCaja.split("-")[1]+fechaCaja.split("-")[0];

				CuadraturaResource.cuadraturaPorCentro({centroscosto: $scope.centroSeleccionado.centroCosto, 
    											fechaDesde: $scope.fechaSeleccion, usuario: $scope.centroSeleccionado.usuario,
    											corrcaja: $scope.centroSeleccionado.numeroCaja, criterio: 0 }
    											 ,successCallback, $scope.errorCallback);
    		}
    	
    		if(opcion == 3){



    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.usuario.centroCostoActivo.codigo, 
    											fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaHastaCuad}
    											 ,successCallback, $scope.errorCallback);
    		}
    	
    }



    $scope.cuadraturaGeneralModal = function(){
    	
    		
    		var successCallback = function(data,responseHeaders){
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			$scope.cuadraturaGnrl.centroCosto = 0;
    			$scope.cuadraturaGnrl.glosaCC = "Todos";
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
    			$scope.cuadXCcs = true;
    			$scope.disableForm = true;
    			$scope.tipoCuadratura = "cc";
    			$scope.ocultaFecha = false;
    			$scope.habilitaBoton = false;
				//$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				//$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $scope.centroSeleccionado.usuario;
				$scope.centroCostoCuad = $scope.objSeleccionado.codigo;
    			
				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					$scope.totales.totIvaTransaccionBoleta = $scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * 0.19);

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * 0.19);

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * 0.19);
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;
					$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * 0.19);

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * 0.19);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * 0.19);



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);    			
				$scope.cuadraturaGnrl.totales = $scope.totales;
    			
    			//angular.element("#cuadratura").modal();
    		}
    		CuadraturaResource.cuadraturaGeneral({centroCosto: 0, fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaHastaCuad} ,successCallback, $scope.errorCallback);
    	
    }


 	$scope.cuadraturaGeneralPorCaja = function(){


			var successCallback = function(data,responseHeaders){
				$scope.listadoCajas = data;
    			$scope.cuadraturaGnrl.centroCosto = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.glosaCC = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
    			$scope.cuadraturaGnrl.usuario = $scope.objSeleccionado.descripcion;
    			$scope.cuadraturaGnrl.listadoCajas = data;
    			$scope.tipoCuadratura = "caja";


				$scope.fechaDesdeCuad = $scope.fechaDesde;
				$scope.fechaHastaCuad = $scope.fechaHasta;
				$scope.usuarioCuad = $scope.cuadraturaUserList.usuario;
				$scope.centroCostoCuad = $scope.cuadraturaUserList.centroCosto;

				$scope.cuadXCcs = false;
    			$scope.disableForm = false;
    			angular.element("#cuadratura").modal();
			}



    		CuadraturaResource.cargaDatosCaja({fechaDesde: $scope.fechaDesde, centrocosto: $scope.cuadraturaUserList.centroCosto,
    											  usuario: $scope.cuadraturaUserList.usuario} ,successCallback, $scope.errorCallback);


 	}

    $scope.divAnterior = {};
    $scope.divNuevo = {};
    $scope.cambiaEstilo = function(id){
    	if(angular.equals({}, $scope.divAnterior) && angular.equals({}, $scope.divNuevo)){
    		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');
     	}
     	else if(!angular.equals({}, $scope.divAnterior) && angular.equals({}, $scope.divNuevo)){
     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divAnterior = {};

     		$scope.divNuevo = id;
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');
     		//$scope.divNuevo = {};
     	}
     	else if(!angular.equals({}, $scope.divNuevo) && angular.equals({}, $scope.divAnterior)){

     		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');

     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divNuevo = {};

     	}
     
     	else if(!angular.equals({}, $scope.divNuevo) && !angular.equals({}, $scope.divAnterior)){

     		$scope.divAnterior = id;
    		var myEl = angular.element( document.querySelector( '#'+ $scope.divNuevo ) );
     		myEl.css('color','white');     
     		myEl.css('background','black');

     		
     		var myEl = angular.element( document.querySelector( '#'+ $scope.divAnterior ) );
     		myEl.css('color','grey');     
     		myEl.css('background','white'); 
     		
			$scope.divNuevo = {};

     	}

    }

	$scope.getCaja = function(value){
		$scope.cajaSeleccionada = value;
	}


    $scope.buscaCuadratura = function(){

    	
    	$scope.cuadraturaGnrl.movimientos = {};
    	$scope.resetTotales();
    	
    	$scope.usuarioCuad
    	if($scope.tipoCuadratura === "caja"){
			if(angular.equals({}, $scope.cajaSeleccionada)){
				swal('Alerta','Seleccione una Caja','error');
				return false;
			}
    	}

    	if($scope.tipoCuadratura === "cc" && $scope.usuarioCuad !== undefined && ($scope.fechaDesdeCuad === undefined || $scope.fechaDesdeCuad === "")){
    		swal('Alerta',"Ingrese fecha de busqueda",'error');
    		return false;
    	}

    	if($scope.tipoCuadratura === "cc"){
    		$scope.cajaSeleccionada = {};
          	if(!angular.equals({}, $scope.cuadraturaGnrl.listadoCajas)){
        		if($scope.cuadraturaGnrl.listadoCajas !== undefined && $scope.cuadraturaGnrl.listadoCajas !== undefined){
    	        	$scope.cuadraturaGnrl.listadoCajas.forEach(function (item, i) {
    	                item.centroSeleccionadocheck = false;
    	            });
        		}
        	}    		
    	}
    	
		console.log($scope.cuadraturaGnrl);

			    $scope.resetTotales();

		    	var successCallback = function(data,responseHeaders){
    			console.log(data);

    			if(data.movimientos.length == 0){
    				swal('Alerta',"No se encontrarón registros",'error');
    			}

    			
    			$scope.cuadraturaGnrl.impresion = 1;
    			
    			
    			$scope.cuadraturaGnrl.movimientos = data.movimientos;
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
    			
    			$scope.cuadraturaGnrl.totalAviPago = data.totalAviPago;
				$scope.cuadraturaGnrl.totalBoletas = data.totalBoletas;
				$scope.cuadraturaGnrl.totalDiferencia = data.totalDiferencia;
				$scope.cuadraturaGnrl.totalFacturas = data.totalFacturas;
				$scope.cuadraturaGnrl.totalMovimientos = data.totalMovimientos;
				$scope.cuadraturaGnrl.totalPagado = data.totalPagado;
				$scope.cuadraturaGnrl.totalTransacciones = data.totalTransacciones;

				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					//Math.round(data.totalCalculado / (1 + $scope.iva)
					//$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * $scope.iva));
					$scope.totales.totIvaTransaccionBoleta = Math.round($scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta - Math.round(item.montoTransaccionBoleta / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					//$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * $scope.iva);
					$scope.totales.totIvaCanceladoBoleta = Math.round($scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta - Math.round(item.montoCantidadBoleta / (1 + $scope.iva))));
					

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					//$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * $scope.iva);
					$scope.totales.totIvaDiferenciaBoleta = Math.round($scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta - Math.round(item.diferenciaBoleta / (1 + $scope.iva))));
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;

					//$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * $scope.iva);
					$scope.totales.totIvaTransaccionFactura = Math.round($scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura - Math.round(item.montoTransaccionFactura / (1 + $scope.iva))));

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = Math.round($scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura - Math.round(item.montoCantidadFactura / (1 + $scope.iva))));					
					//$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * $scope.iva);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					//$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * $scope.iva);
					$scope.totales.totIvaDiferenciaFactura = Math.round($scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura - Math.round(item.diferenciaFactura / (1 + $scope.iva))));					



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);    			
				$scope.cuadraturaGnrl.totales = $scope.totales;
    			
    			//angular.element("#cuadratura").modal();
    		}

    		if(angular.equals({}, $scope.cajaSeleccionada) && ($scope.usuarioCuad === undefined || $scope.usuarioCuad === "" )){
    			CuadraturaResource.cuadraturaGeneral({centroscosto: $scope.centroCostoCuad, 
    											fechaDesde: $scope.fechaDesdeCuad, fechaHasta: $scope.fechaDesdeCuad}
    											 ,successCallback, $scope.errorCallback);

    		}
    		else if(angular.equals({}, $scope.cajaSeleccionada) && ($scope.usuarioCuad !== undefined || $scope.usuarioCuad !== "" )){
    			CuadraturaResource.cuadraturaPorCentro({fechaDesde: $scope.fechaDesdeCuad, centroscosto: $scope.centroCostoCuad,
					usuario: $scope.usuarioCuad, corrcaja:  $scope.cajaSeleccionada.corrCaja, criterio: 1} ,successCallback, $scope.errorCallback);
    		}
    		else{
	    		CuadraturaResource.cuadraturaPorCentro({fechaDesde: $scope.fechaDesdeCuad, centroscosto: $scope.cajaSeleccionada.codCentroCosto,
    												usuario: $scope.usuarioCuad, corrcaja:  $scope.cajaSeleccionada.corrCaja, criterio: 0} ,successCallback, $scope.errorCallback);
    		}
	}

    

	$scope.cuadraturaPorCajaArbol = function(){

		

    		var successCallback = function(data,responseHeaders){
    			console.log(data);
    			$scope.cuadraturaGnrl = data;
    			$scope.cuadraturaGnrl.centroCosto = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.glosaCC = $scope.objSeleccionado.centroCosto;
    			$scope.cuadraturaGnrl.fechaDesde = $scope.fechaDesde;
    			$scope.cuadraturaGnrl.fechaHasta = $scope.fechaHasta;
    			$scope.cuadXCcs = false;
    			$scope.disableForm = false;
    			$scope.tipoCuadratura = "caja";

    			
				$scope.fechaDesdeCuad = $scope.cuadraturaGnrl.fechaDesde;
				$scope.fechaHastaCuad = $scope.cuadraturaGnrl.fechaHasta;
				$scope.usuarioCuad = $scope.objSeleccionado.usuario;
				$scope.centroCostoCuad = $scope.objSeleccionado.centroCosto;
    			

    			if(data.movimientos.length == 0){
    				swal('Alerta',"No se encontrarón registros",'error');
    			}

				angular.forEach($scope.cuadraturaGnrl.movimientos, function (item) {

					//boletas
					$scope.totales.totBrutoNumMovBoleta = $scope.totales.totBrutoNumMovBoleta + item.boletaMovimiento;
					$scope.totales.totBrutoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta + item.montoTransaccionBoleta;
					$scope.totales.totIvaTransaccionBoleta = $scope.totales.totIvaTransaccionBoleta + (item.montoTransaccionBoleta * 0.19);

					$scope.totales.totBrutoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta + item.montoCantidadBoleta;
					$scope.totales.totIvaCanceladoBoleta = $scope.totales.totIvaCanceladoBoleta + (item.montoCantidadBoleta * 0.19);

					$scope.totales.totBrutoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta + item.diferenciaBoleta;
					$scope.totales.totIvaDiferenciaBoleta = $scope.totales.totIvaDiferenciaBoleta + (item.diferenciaBoleta * 0.19);
		           // facturas

		           	$scope.totales.totBrutoNumMovFactura =    $scope.totales.totBrutoNumMovFactura + item.facturaMovimientos;
					$scope.totales.totBrutoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura + item.montoTransaccionFactura;
					$scope.totales.totIvaTransaccionFactura = $scope.totales.totIvaTransaccionFactura + (item.montoTransaccionFactura * 0.19);

					$scope.totales.totBrutoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura + item.montoCantidadFactura;
					$scope.totales.totIvaCanceladoFactura = $scope.totales.totIvaCanceladoFactura + (item.montoCantidadFactura * 0.19);

					$scope.totales.totBrutoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura + item.diferenciaFactura;
					$scope.totales.totIvaDiferenciaFactura = $scope.totales.totIvaDiferenciaFactura + (item.diferenciaFactura * 0.19);



		        });

				$scope.totales.totNetoTransaccionBoleta = $scope.totales.totBrutoTransaccionBoleta - $scope.totales.totIvaTransaccionBoleta;
				$scope.totales.totNetoCanceladoBoleta = $scope.totales.totBrutoCanceladoBoleta - $scope.totales.totIvaCanceladoBoleta;
				$scope.totales.totNetoDiferenciaBoleta = $scope.totales.totBrutoDiferenciaBoleta - $scope.totales.totIvaDiferenciaBoleta;


				$scope.totales.totNetoTransaccionFactura = $scope.totales.totBrutoTransaccionFactura - $scope.totales.totIvaTransaccionFactura;
				$scope.totales.totNetoCanceladoFactura = $scope.totales.totBrutoCanceladoFactura - $scope.totales.totIvaCanceladoFactura;
				$scope.totales.totNetoDiferenciaFactura = $scope.totales.totBrutoDiferenciaFactura - $scope.totales.totIvaDiferenciaFactura;
				console.log($scope.totales);  
				$scope.cuadraturaGnrl.totales = $scope.totales;

    			$scope.buscaCajas();
    			angular.element("#cuadratura").modal();
    		}
    		CuadraturaResource.cuadraturaPorCaja({centrocosto: $scope.objSeleccionado.centroCosto, fechaDesde: $scope.fechaDesde, usuario: $scope.objSeleccionado.usuario, 
    											corrcaja:  $scope.objSeleccionado.corrCaja, criterio: 0} ,successCallback, $scope.errorCallback);



	}



    
    $scope.cerrarCuadratura = function(){

    	$scope.cajaSeleccionada = {};
		$scope.centroSeleccionado = {};
		$scope.resetTotales();
		$scope.cuadraturaGnrl.movimientos = [];
		$scope.cuadXCss = false;
		$scope.fechaDesdeCuad = $filter('date')(new Date(), "ddMMyyyy");
		$scope.cerraDetalleBoleta();
		$scope.cuadraturaGnrl.impresion = 0;
		angular.element("#cuadraturaindex").modal('hide');
    }
    
    
    $scope.resetTotales = function(){

	  	$scope.totales = {
			totBrutoNumMovBoleta : 0,
			totBrutoTransaccionBoleta : 0,
			totNetoTransaccionBoleta : 0,
			totIvaTransaccionBoleta : 0,
			totBrutoCanceladoBoleta : 0,
			totNetoCanceladoBoleta : 0,
			totIvaCanceladoBoleta : 0,
			totBrutoDiferenciaBoleta : 0,
			totNetoDiferenciaBoleta : 0,
			totIvaDiferenciaBoleta : 0,

			totBrutoNumMovFactura: 0,
			totBrutoTransaccionFactura : 0,
			totNetoTransaccionFactura : 0,
			totIvaTransaccionFactura : 0,
			totBrutoCanceladoFactura : 0,
			totNetoCanceladoFactura : 0,
			totIvaCanceladoFactura : 0,
			totBrutoDiferenciaFactura : 0,
			totNetoDiferenciaFactura : 0,
			totIvaDiferenciaFactura : 0
		}  	

    }


    $scope.buscaCajas = function(){
    	
    	$scope.resetTotales();
    	$scope.cuadraturaGnrl.movimientos = [];
    	$scope.cuadraturaGnrl.impresion = 0;
    	$scope.cuadraturaGnrl.movimientos = [];
		$scope.cuadraturaGnrl.totalAviPago = 0;
		$scope.cuadraturaGnrl.totalBoletas = 0;
		$scope.cuadraturaGnrl.totalDiferencia = 0;
		$scope.cuadraturaGnrl.totalFacturas = 0;
		$scope.cuadraturaGnrl.totalMovimientos = 0;
		$scope.cuadraturaGnrl.totalPagado = 0;
		$scope.cuadraturaGnrl.totalTransacciones = 0;
		
    	if($scope.fechaDesdeCuad === undefined || $scope.fechaDesdeCuad === ""){
    		swal('Alerta',"Ingrese fecha de busqueda.",'error');
    		return false;
    	}
    	
    	
    	var successCallback = function(data,responseHeaders){
    		$scope.cuadraturaGnrl.listadoCajas = data;
    	}
    	
    	CuadraturaResource.cargaDatosCaja({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad} ,successCallback, $scope.errorCallback);
    }



    $scope.imprimeCuadratura = function(value){

    	console.log(value);
    	
      	if(value.impresion == 0){
			swal('Alerta','Realice una búsqueda de cuadratura para imprimir','error');
			return false;
    	}
      	
    	value.usuario = $scope.usuarioCuad;
    	if((value.corrCaja === undefined || value.corrCaja === null) && $scope.centroSeleccionado.numeroCaja !== undefined){
    		value.corrCaja = $scope.centroSeleccionado.numeroCaja; 
    	}
    	
    	var successCallback = function(data,responseHeaders){
    		console.log(data);
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "cuadraturaGeneral.pdf");

    	}
    	CuadraturaResource.imprimeCuadratura(value,successCallback, $scope.errorCallback);
    }



    $scope.cargaIva = function(){
    	var successCallbackIVA = function(data, responseHeaders){
			console.log('successCallbackIVA');
			console.log(data);
			if(data.idControl !== 0) {
       			 swal({ title: 'Error', text: 'No se puede obtener el dato IVA!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false})
       			 .then(function(){
          			return false;	
        		});
        		
			}
    		$scope.iva = parseFloat(data.msgControl)/100;
		}
		CuadraturaResource.obtenerIVA(successCallbackIVA, $scope.errorCallback);
		
    }
     $scope.cargaIva();
     
     
     $scope.detalleBoleta = function(){


    	 $scope.detalleBoletaObj = {};

    	if(angular.equals({}, $scope.cajaSeleccionada)){
    		swal({ title: 'Error', text: "Seleccione una Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
    		return false;
    	}

    	var successCallback = function(data, responseHeaders){
			console.log(data);
			$scope.detalleBoletaObj = data;
			$scope.detalleBoletaObj.corrCaja = $scope.cajaSeleccionada.corrCaja;
			var valorIva = Math.round(data.totalCalculado / (1 + $scope.iva));

			$scope.detalleBoletaObj.totalTransaccionNetoBoletas = Math.round(data.totalCalculado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalCanceladoNetoBoletas = Math.round(data.totalPagado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalDiferenciaNetoBoletas = Math.round(data.totalDiferencia / (1 + $scope.iva));

			$scope.detalleBoletaObj.totalTransaccionIvaBoletas = data.totalCalculado - Math.round(data.totalCalculado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalCanceladoIvaBoletas = data.totalPagado - Math.round(data.totalPagado / (1 + $scope.iva));
			$scope.detalleBoletaObj.totalDiferenciaIvaBoletas = data.totalDiferencia - Math.round(data.totalDiferencia / (1 + $scope.iva));
			//$scope.cajaSeleccionada = {};
		}
		CuadraturaResource.detalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0 } ,successCallback, $scope.errorCallback);


    	 angular.element("#detalleboletaindex").modal();
     }
     
     $scope.cerraDetalleBoleta = function(){
    	 
      	if(!angular.equals({}, $scope.cuadraturaGnrl.listadoCajas)){
    		if($scope.cuadraturaGnrl.listadoCajas !== undefined && $scope.cuadraturaGnrl.listadoCajas !== undefined){
	        	$scope.cuadraturaGnrl.listadoCajas.forEach(function (item, i) {
	                item.centroSeleccionadocheck = false;
	            });
    		}
    	}
    	$scope.cajaSeleccionada = {};
      	$scope.centroSeleccionado = {};
      	$scope.resetTotales();
    	angular.element("#detalleboletaindex").modal('hide');
     }
     


     $scope.detalleServicios = function(){
     	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);
     	
    	if(angular.equals({}, $scope.cajaSeleccionada)){
    		swal({ title: 'Error', text: "Seleccione una Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
    		return false;
    	}
    	var successCallback = function(data, responseHeaders){
    		console.log("-----------------------------");
			console.log(data);
			$scope.detalleServiciosDto = data;


			$scope.tableParams = new NgTableParams({}, { dataset: $scope.detalleServiciosDto.servicios});
			angular.element("#detalleserviciosindex").modal();
		}


		CuadraturaResource.detalleServicio({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0, ordenamiento: 0} ,successCallback, $scope.errorCallback);


    	 
     }

     
     $scope.imprimeDetalleServicios = function(){
     	 console.log($scope.cuadraturaGnrl);
    	 console.log($scope.cajaSeleccionada);

    	var successCallback = function(data, responseHeaders){

			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "detalleServicios.pdf");
		}


		CuadraturaResource.imprimeDetalleServicio({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0, ordenamiento: 0} ,successCallback, $scope.errorCallback);


    	 
     }
     
     
     
     $scope.cerrarServicios = function(){
    	 angular.element("#detalleserviciosindex").modal('hide');
     }

     $scope.detalleBoletaModal = function(){
    	 
    	console.log($scope.centroSeleccionado) ;

		if(angular.equals({}, $scope.centroSeleccionado)){
			swal('Alerta','Seleccione una boleta','error');
			return false;
		}
    	
    	
    	$scope.detalleboletaDto = {};
    	$scope.detalleboletaDto.corrCaja = $scope.centroSeleccionado.corrCaja;
    	$scope.detalleboletaDto.nroBoletaFactura = 	$scope.centroSeleccionado.numBoletaFactura



     	var successCallback = function(data, responseHeaders){
			console.log(data);
			$scope.detalleboletaDto = data;
			if($scope.detalleboletaDto.boletaFactura == "B"){
				angular.element("#detalletransaccionboletaindex").modal();	
			}
			if($scope.detalleboletaDto.boletaFactura == "F"){
				dateArray = $scope.detalleboletaDto.detalleFactura.ubicacion.split("\\");
				
				$scope.detalleboletaDto.detalleFactura.direccion = dateArray[0];
				$scope.detalleboletaDto.detalleFactura.ciudad = dateArray[2];
				$scope.detalleboletaDto.detalleFactura.comuna = dateArray[1];
				angular.element("#detalletransaccionfacturaindex").modal();	
			}			

		}
		CuadraturaResource.modalBoletaCuad($scope.detalleboletaDto,successCallback, $scope.errorCallback);

     	
     }


     
     $scope.imprimeDetalleBoleta = function(){

    	//console.log($scope.cuadraturaGnrl);
    	//console.log($scope.centroSeleccionado);
    	//if(angular.equals({}, $scope.cajaSeleccionada)){
    	//	swal({ title: 'Error', text: "Seleccione una Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
    	//	return false;
    	//}
    	
		console.log($scope.cuadraturaGnrl);
		console.log($scope.cajaSeleccionada);
		if($scope.tipoCuadratura !== "cc"){
			if(angular.equals({}, $scope.cajaSeleccionada)){
				swal({ title: 'Error', text: "Seleccione una Caja", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
				return false;
			}  
		} 
    	
    	var successCallback = function(data, responseHeaders){
			// base64 string
			var base64str = data.reporte;

			// decode base64 string, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			for (var i = 0; i < len; i++) {
			    view[i] = binary.charCodeAt(i);
			}

			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "detalleBoletaFactura.pdf");
			
			
			//$scope.cajaSeleccionada = {};
          	//if(!angular.equals({}, $scope.cuadraturaGnrl.listadoCajas)){
        	//	if($scope.cuadraturaGnrl.listadoCajas !== undefined && $scope.cuadraturaGnrl.listadoCajas !== undefined){
    	    //   	$scope.cuadraturaGnrl.listadoCajas.forEach(function (item, i) {
    	    //            item.centroSeleccionadocheck = false;
    	    //        });
        	//	}
        	//} 			

		}
    	//CuadraturaResource.imprimeDetalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
		//	  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0 } ,successCallback, $scope.errorCallback);
		if($scope.tipoCuadratura !== "cc"){
			CuadraturaResource.imprimeDetalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja, criterio:0 } ,successCallback, $scope.errorCallback);
		}
		else{
			CuadraturaResource.imprimeDetalleBoleta({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: 0, criterio:1 } ,successCallback, $scope.errorCallback);
			
		}

    	
     	
     }

     $scope.imprimeValidador = function(){
    	 
    	console.log($scope.cuadraturaGnrl);
    	console.log($scope.cajaSeleccionada);
    	 
    	 
     	if(angular.equals({}, $scope.cajaSeleccionada)){
			swal('Alerta','Seleccione una Caja','error');
			return false;
    	}
    	    	 
    	var salida1 = 0;
    	var salida2 = 0;
    	
    	var successCallback = function(data, responseHeaders){
			// base64 string
    		
    		if(data.idControl == 1){
    			salida1 = data.idControl;
    		}
    		
    		else{
    		
    		
				var base64str = data.reporte;
	
				// decode base64 string, remove space for IE compatibility
				var binary = atob(base64str.replace(/\s/g, ''));
				var len = binary.length;
				var buffer = new ArrayBuffer(len);
				var view = new Uint8Array(buffer);
				for (var i = 0; i < len; i++) {
				    view[i] = binary.charCodeAt(i);
				}
	
				var blob = new Blob( [view], { type: "application/pdf" });
				saveAs(blob, "validadorAclaraciones.pdf");
			
    		}
			
			
			var successCallbackSolicitud = function(data, responseHeaders){

	    		if(data.idControl == 1){
	    			salida2 = data.idControl;
	    		}
	    		
	    		else{
				
					var base64sol = data.reporte;
	
					// decode base64 string, remove space for IE compatibility
					var binarySol = atob(base64sol.replace(/\s/g, ''));
					var len = binarySol.length;
					var buffer = new ArrayBuffer(len);
					var view = new Uint8Array(buffer);
					for (var i = 0; i < len; i++) {
					    view[i] = binarySol.charCodeAt(i);
					}
	
					var blob = new Blob( [view], { type: "application/pdf" });
					saveAs(blob, "solicitudAclaraciones.pdf");
	    		}
	    		
	    		
	    		if(salida1 && salida2 == 1){
	    			swal({ title: 'Error', text: "No existen datos.", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	    		} 
			}
			
			CuadraturaResource.imprimeSolReclamo({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
				  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja} ,successCallbackSolicitud, $scope.errorCallback);
			

		}
		CuadraturaResource.imprimeValidacion({fechaDesde: $scope.fechaDesdeCuad, centrocosto: $scope.centroCostoCuad,
			  usuario: $scope.usuarioCuad, corrcaja: $scope.cajaSeleccionada.corrCaja} ,successCallback, $scope.errorCallback);
		
		
     }
     
     $scope.cierraBoletaModal = function(){
     	angular.element("#detalletransaccionboletaindex").modal('hide');
     	$scope.detalleboletaDto = {};

     }

     $scope.cerrarDetalleFactura= function(){
      	angular.element("#detalletransaccionfacturaindex").modal('hide');
     	$scope.detalleboletaDto = {};    	 
     }
     
     $("#cuadraturaindex").on('shown.bs.modal', function () {

    	$scope.cuadraturaGnrl.movimientos = [];
		$scope.cuadraturaGnrl.totalAviPago = 0;
		$scope.cuadraturaGnrl.totalBoletas = 0;
		$scope.cuadraturaGnrl.totalDiferencia = 0;
		$scope.cuadraturaGnrl.totalFacturas = 0;
		$scope.cuadraturaGnrl.totalMovimientos = 0;
		$scope.cuadraturaGnrl.totalPagado = 0;
		$scope.cuadraturaGnrl.totalTransacciones = 0;  
		//$scope.resetTotales();
		$scope.cuadraturaGeneral(3);
 		$scope.fechaDesdeCuad = $filter('date')(new Date(), "ddMMyyyy");
 		$(this).off('shown.bs.modal');
 	});
    $("#cuadraturaindex").on('hidden.bs.modal', function(){
    		if($scope.fechaDesdeCuad === undefined || $scope.fechaDesdeCuad === ""){
    			$scope.fechaDesdeCuad = $filter('date')(new Date(), "ddMMyyyy");
    		}
	    	$scope.cuadraturaGnrl.movimientos = [];
			$scope.cuadraturaGnrl.totalAviPago = 0;
			$scope.cuadraturaGnrl.totalBoletas = 0;
			$scope.cuadraturaGnrl.totalDiferencia = 0;
			$scope.cuadraturaGnrl.totalFacturas = 0;
			$scope.cuadraturaGnrl.totalMovimientos = 0;
			$scope.cuadraturaGnrl.totalPagado = 0;
			$scope.cuadraturaGnrl.totalTransacciones = 0;        		
    });

});
