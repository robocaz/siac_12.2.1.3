package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_DISTRIBUIDOR database table.
 * 
 */
@Entity
@Table(name="TBL_DISTRIBUIDOR")
@NamedQuery(name="TblDistribuidor.findAll", query="SELECT t FROM TblDistribuidor t")
public class TblDistribuidor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODDISTRIBUIDOR")
	private String fldCoddistribuidor;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_GLOSADISTRIBUIDOR")
	private String fldGlosadistribuidor;

	@Column(name="FLD_RUTDISTRIBUIDOR")
	private String fldRutdistribuidor;

	public TblDistribuidor() {
	}

	public String getFldCoddistribuidor() {
		return this.fldCoddistribuidor;
	}

	public void setFldCoddistribuidor(String fldCoddistribuidor) {
		this.fldCoddistribuidor = fldCoddistribuidor;
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public String getFldGlosadistribuidor() {
		return this.fldGlosadistribuidor;
	}

	public void setFldGlosadistribuidor(String fldGlosadistribuidor) {
		this.fldGlosadistribuidor = fldGlosadistribuidor;
	}

	public String getFldRutdistribuidor() {
		return this.fldRutdistribuidor;
	}

	public void setFldRutdistribuidor(String fldRutdistribuidor) {
		this.fldRutdistribuidor = fldRutdistribuidor;
	}

}