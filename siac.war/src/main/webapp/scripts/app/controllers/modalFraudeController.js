angular.module('ccs').controller('ModalFraudeController', function($scope, $rootScope, DetectorFraudeResource){
	
    $scope.autorizacion = function() {
//    	console.log("AUTORIZACION ModalFraudeController");
//		console.log($rootScope.fraude);
//		console.log($rootScope.usuario.caja);
		
		var successCallbackFraude = function(dataFraude, responseHeadersFraude) {
			console.log(dataFraude);
			
			var poseeAutorizacion = !!(dataFraude.idControl);
			
			if (poseeAutorizacion) {
				angular.element('#detectorFraude').modal("hide");
				var persona = $rootScope.fraude.persona;
				$rootScope.$emit("CargarPersona", $rootScope.fraude.persona);
			} else {
				swal({
					text: 'El ingreso de este RUT, debe ser autorizado por la CCS. Informe al supervisor para que gestione la autorización', 
					type: 'warning', 
					showCloseButton: false, 
					confirmButtonText: 'Aceptar'
				}).then(function () { 
					$rootScope.$emit("LimpiarPersona", {});
					angular.element('#detectorFraude').modal("hide");
				});
			}
			
		}
		var errorCallbackFraude = function(responseFraude) {
			$scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
		}
		
		var dataFraudeDTO = {
				rut: $rootScope.fraude.persona.rut,
				corrCaja: $rootScope.usuario.caja
		}
		
		DetectorFraudeResource.validaAutorizacion(dataFraudeDTO, successCallbackFraude, errorCallbackFraude);
    }
    
    $scope.formatDate = function(date) {
    	return new Date(date).getTime();
    }
    
    $scope.salir = function() {
  
    	$rootScope.$emit("LimpiarPersona", {});
    	angular.element('#detectorFraude').modal("hide");
    }
    
});
