package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_VCDetPareadosAnterior database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VCDetPareadosAnterior.findAll", query="SELECT t FROM Tbl_VCDetPareadosAnterior t")
public class Tbl_VCDetPareadosAnterior implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisorVen")
	private String fld_CodEmisorVen;

	@Column(name="Fld_CorrEnvioVC")
	private BigDecimal fld_CorrEnvioVC;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FolioVC")
	private int fld_FolioVC;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoEmisorVen")
	private String fld_TipoEmisorVen;

	public Tbl_VCDetPareadosAnterior() {
	}

	public String getFld_CodEmisorVen() {
		return this.fld_CodEmisorVen;
	}

	public void setFld_CodEmisorVen(String fld_CodEmisorVen) {
		this.fld_CodEmisorVen = fld_CodEmisorVen;
	}

	public BigDecimal getFld_CorrEnvioVC() {
		return this.fld_CorrEnvioVC;
	}

	public void setFld_CorrEnvioVC(BigDecimal fld_CorrEnvioVC) {
		this.fld_CorrEnvioVC = fld_CorrEnvioVC;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public int getFld_FolioVC() {
		return this.fld_FolioVC;
	}

	public void setFld_FolioVC(int fld_FolioVC) {
		this.fld_FolioVC = fld_FolioVC;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_TipoEmisorVen() {
		return this.fld_TipoEmisorVen;
	}

	public void setFld_TipoEmisorVen(String fld_TipoEmisorVen) {
		this.fld_TipoEmisorVen = fld_TipoEmisorVen;
	}

}