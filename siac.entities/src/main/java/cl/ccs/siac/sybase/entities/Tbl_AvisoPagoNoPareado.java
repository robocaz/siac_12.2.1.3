package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AvisoPagoNoPareado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AvisoPagoNoPareado.findAll", query="SELECT t FROM Tbl_AvisoPagoNoPareado t")
public class Tbl_AvisoPagoNoPareado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CorrAvisoPago_Id")
	private BigDecimal fld_CorrAvisoPago_Id;

	@Column(name="Fld_CorrEnvioAcl")
	private BigDecimal fld_CorrEnvioAcl;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroIntentos")
	private byte fld_NroIntentos;

	@Column(name="Fld_NroOper3Dig")
	private short fld_NroOper3Dig;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_AvisoPagoNoPareado() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CorrAvisoPago_Id() {
		return this.fld_CorrAvisoPago_Id;
	}

	public void setFld_CorrAvisoPago_Id(BigDecimal fld_CorrAvisoPago_Id) {
		this.fld_CorrAvisoPago_Id = fld_CorrAvisoPago_Id;
	}

	public BigDecimal getFld_CorrEnvioAcl() {
		return this.fld_CorrEnvioAcl;
	}

	public void setFld_CorrEnvioAcl(BigDecimal fld_CorrEnvioAcl) {
		this.fld_CorrEnvioAcl = fld_CorrEnvioAcl;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public byte getFld_NroIntentos() {
		return this.fld_NroIntentos;
	}

	public void setFld_NroIntentos(byte fld_NroIntentos) {
		this.fld_NroIntentos = fld_NroIntentos;
	}

	public short getFld_NroOper3Dig() {
		return this.fld_NroOper3Dig;
	}

	public void setFld_NroOper3Dig(short fld_NroOper3Dig) {
		this.fld_NroOper3Dig = fld_NroOper3Dig;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}