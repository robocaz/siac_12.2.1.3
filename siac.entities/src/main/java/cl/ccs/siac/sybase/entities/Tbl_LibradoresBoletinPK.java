package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_LibradoresBoletin database table.
 * 
 */
@Embeddable
public class Tbl_LibradoresBoletinPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_CorrAcl")
	private long fld_CorrAcl;

	public Tbl_LibradoresBoletinPK() {
	}
	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}
	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}
	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}
	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}
	public long getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}
	public void setFld_CorrAcl(long fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_LibradoresBoletinPK)) {
			return false;
		}
		Tbl_LibradoresBoletinPK castOther = (Tbl_LibradoresBoletinPK)other;
		return 
			(this.fld_NroBoletin == castOther.fld_NroBoletin)
			&& this.fld_RutAfectado.equals(castOther.fld_RutAfectado)
			&& (this.fld_CorrAcl == castOther.fld_CorrAcl);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) this.fld_NroBoletin);
		hash = hash * prime + this.fld_RutAfectado.hashCode();
		hash = hash * prime + ((int) (this.fld_CorrAcl ^ (this.fld_CorrAcl >>> 32)));
		
		return hash;
	}
}