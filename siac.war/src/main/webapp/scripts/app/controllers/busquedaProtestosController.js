angular.module('ccs').controller('BusquedaProtestosController', function($scope, ProtestosResource, $location, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.show = false;
	$scope.campo = {};
	$scope.campo.aclarados = false;
	$scope.campo.vigentes = true;
	$scope.campo.omisiones = false;
	$scope.campo.count = 0;
	$scope.lista = {};
	$scope.detalle = {};
	$scope.acl = {};
	$scope.tram = {};

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$('#busquedaprotestos').on('show.bs.modal', function (e) {
		console.log("-> Búsqueda Protestos..");
		$timeout(function(){
			if ( !checkPermisos() ) {
				swal('Atención','No tiene el permiso para ejecutar esta funcionalidad.','warning');
				$('#busquedaprotestos').modal('hide');
			} else {
				$scope.init();
			}
		});
	});

	function checkPermisos() {
		console.log("check permisos..");
		if ( $rootScope.usuario.esConsultaprotesto ) {
			$scope.show = true;
		} else {
			$scope.show = false;
		}
		return $scope.show;
	}

	$scope.init = function() {
		
	}
	
	$scope.buscar = function() {
		limpiaBusqueda();
		
		if ( !validaDatos() ) {
			return;
		}
		
		$scope.params = {
			rut		: $scope.campo.rut,
			opcAcl	: $scope.campo.aclarados,
			opcPrt	: $scope.campo.vigentes,
			opcOmi	: $scope.campo.omisiones,
			codUser : $rootScope.usuario.username
		}
		var successCallback = function (data, responseHeaders) {
			$scope.campo.nombreAfectado = data.nombreAfectado;
			$scope.lista = data.lista;
			if ( data.lista == null || data.lista.length == 0 ) {
				$scope.campo.nombreAfectado = null;
				swal('Alerta', 'No existen Registros para la Consulta', 'error');
			} else {
				$scope.campo.count = data.lista.length;
			}
		}
		ProtestosResource.buscar($scope.params, successCallback, $scope.errorCallback);
	};
	
	function limpiaBusqueda() {
		$scope.lista = {};
		$scope.campo.count = 0;
		$scope.campo.nombreAfectado = null;
		$scope.campo.indice = null;
	}
	
	function reset() {
		limpiaBusqueda();
		$scope.campo.rut = undefined;
		$scope.campo.aclarados = false;
		$scope.campo.vigentes = true;
		$scope.campo.omisiones = false;
	}
	
	function validaDatos() {
		//let rutRaw = angular.element("#rutBusquedaProtestos").val();
		
		if ( isUndefinedNullEmpty($scope.campo.rut) ) {
			swal('Alerta','Debe ingresar RUT','error');
			return false;
		}
		
		if ( !esRutValido($scope.campo.rut) ) {
			swal('Alerta','Rut no es válido','error');
			$scope.campo.rut = undefined;
			//document.getElementById("rutBusquedaProtestos").value = undefined;
			return false;
		}
		
		if ( !$scope.campo.aclarados && !$scope.campo.vigentes && !$scope.campo.omisiones ) {
			swal('Alerta','Debe seleccionar la(s) opciones de búsqueda','error');
			return false;
		}
		
		return true;
	}

	$scope.validaRut = function() {
		let rut = $scope.campo.rut;
		
		if ( rut === undefined ) {
			return false;
		}
		if ( rut === null ) {
			$scope.campo.rut = undefined;
			swal('Error','Rut no es válido','error');
			return false;
		}
		if ( !esRutValido(rut) ) {
			$scope.campo.rut = undefined;
			swal('Error','Rut no es válido','error');
			return false;
		}
		return true;
	};

	function esRutValido(rut) {
		var rutNumerico = (rut == undefined) ? 0 : parseInt(rut.slice(0,rut.length-1).trim());
		if ( RutHelper.validate(rut) && rutNumerico > rutMinimo ) {
			return true;
		} else {
			return false;
		}
	}

	$scope.protesto = function() {
		
		if ( isUndefinedNullEmpty($scope.campo.indice) ) {
			swal('Alerta','Debe seleccionar registro','error');
			return;
		}
		
		let index = parseInt($scope.campo.indice);
		let corrProt = $scope.lista[index].corrProt;
		
		if ( corrProt === 0 ) {
			swal('Alerta','No existe Protesto, para la Omisión seleccionada','error');
			return;
		}
		
		var successCallback = function (data, responseHeaders) {
			console.log("-> Data Protesto: ");
			console.log(data);
			$scope.detalle = data;
			$scope.detalle.corrProt = corrProt;
			angular.element("#detalleprotesto").modal();
		}
		
		ProtestosResource.detalle(corrProt, successCallback, $scope.errorCallback);
	};
	
	$scope.cerrar = function(flag) {
		switch(flag) {
		case 0:
			reset();
			break;
		case 1:
			$scope.active = 0;
			angular.element("#detalleprotesto").modal("hide");
			break;
		case 2:
			$scope.active = 0;
			angular.element("#detalleaclaracion").modal("hide");
			break;
		}
	};
	
	$scope.aclaracion = function() {
		
		if ( isUndefinedNullEmpty($scope.campo.indice) ) {
			swal('Alerta','Debe seleccionar registro','error');
			return;
		}
		
		let index = parseInt($scope.campo.indice);
		let corrAcl = $scope.lista[index].corrAclId;
		
		if ( corrAcl === 0 ) {
			swal('Alerta','No existe aclaración, para protesto seleccionado','error');
			return;
		}
		
		var successCallback = function (data, responseHeaders) {
			console.log("-> Data Aclaracion: ");
			console.log(data);
			$scope.acl = data;
			$scope.acl.corrAcl = corrAcl;
			buscaTramitante($scope.acl.rutTramitante);
			angular.element("#detalleaclaracion").modal();
		}
		
		ProtestosResource.aclaracion(corrAcl, successCallback, $scope.errorCallback);
	};
	
	function buscaTramitante(rut) {
		var successCallback = function(data, responseHeaders) {
			console.log("-> Data Tramitante: ");
			console.log(data);
			if ( data.correlativo == "0" ){
				swal('Alerta','Tramitante no encontrado','error');
			} else {
				$scope.tram = data;
			}
			$scope.tram.rutTramitante = $scope.acl.rutTramitante.replace(/^0+/, '');
		};
		ProtestosResource.tramitante(rut, successCallback, $scope.errorCallback);
	}

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

	$scope.countDecimal = function(num) {
		var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
		if (!match) {
			return 0;
		}
		return (match[1] ? match[1].length : 0);
	};

});
