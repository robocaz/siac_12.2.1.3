package cl.exe.ccs.dto.certificados;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CertificadoILPPaginacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6070283665179960961L;
	private Integer pagina;
	private int bc_inicio;
	private int bc_termino;
	private List<CertificadoILPDTO> bc_coleccion;

	public CertificadoILPPaginacionDTO() {
		this.pagina = 0;
		this.bc_coleccion = new ArrayList<CertificadoILPDTO>();
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public int getBc_inicio() {
		return bc_inicio;
	}

	public void setBc_inicio(int bc_inicio) {
		this.bc_inicio = bc_inicio;
	}

	public int getBc_termino() {
		return bc_termino;
	}

	public void setBc_termino(int bc_termino) {
		this.bc_termino = bc_termino;
	}

	public List<CertificadoILPDTO> getBc_coleccion() {
		return bc_coleccion;
	}

	public void setBc_coleccion(List<CertificadoILPDTO> bc_coleccion) {
		this.bc_coleccion = bc_coleccion;
	}

	@Override
	public String toString() {
		return "CertificadoICTPaginacionDTO{" + "pagina=" + pagina + ", bc_inicio=" + bc_inicio + ", bc_termino="
				+ bc_termino + ", bc_coleccion=" + bc_coleccion.size() + '}';
	}
}
