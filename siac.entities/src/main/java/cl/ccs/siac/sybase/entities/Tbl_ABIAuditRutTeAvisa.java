package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABIAuditRutTeAvisa database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIAuditRutTeAvisa.findAll", query="SELECT t FROM Tbl_ABIAuditRutTeAvisa t")
public class Tbl_ABIAuditRutTeAvisa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrContrato")
	private BigDecimal fld_CorrContrato;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecInicioContrato")
	private Timestamp fld_FecInicioContrato;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_FecUltimoMail")
	private Timestamp fld_FecUltimoMail;

	@Column(name="Fld_FlagLey20575")
	private boolean fld_FlagLey20575;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_ABIAuditRutTeAvisa() {
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrContrato() {
		return this.fld_CorrContrato;
	}

	public void setFld_CorrContrato(BigDecimal fld_CorrContrato) {
		this.fld_CorrContrato = fld_CorrContrato;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecInicioContrato() {
		return this.fld_FecInicioContrato;
	}

	public void setFld_FecInicioContrato(Timestamp fld_FecInicioContrato) {
		this.fld_FecInicioContrato = fld_FecInicioContrato;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public Timestamp getFld_FecUltimoMail() {
		return this.fld_FecUltimoMail;
	}

	public void setFld_FecUltimoMail(Timestamp fld_FecUltimoMail) {
		this.fld_FecUltimoMail = fld_FecUltimoMail;
	}

	public boolean getFld_FlagLey20575() {
		return this.fld_FlagLey20575;
	}

	public void setFld_FlagLey20575(boolean fld_FlagLey20575) {
		this.fld_FlagLey20575 = fld_FlagLey20575;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}