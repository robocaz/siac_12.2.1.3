package cl.exe.ccs.dto;

import java.io.Serializable;

public class ProtestoInDTO implements Serializable {

	private static final long serialVersionUID = 6818688829172191758L;

	private String rut;
	private Boolean opcAcl;
	private Boolean opcPrt;
	private Boolean opcOmi;
	private String codUser;

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Boolean getOpcAcl() {
		return opcAcl;
	}

	public void setOpcAcl(Boolean opcAcl) {
		this.opcAcl = opcAcl;
	}

	public Boolean getOpcPrt() {
		return opcPrt;
	}

	public void setOpcPrt(Boolean opcPrt) {
		this.opcPrt = opcPrt;
	}

	public Boolean getOpcOmi() {
		return opcOmi;
	}

	public void setOpcOmi(Boolean opcOmi) {
		this.opcOmi = opcOmi;
	}

	public String getCodUser() {
		return codUser;
	}

	public void setCodUser(String codUser) {
		this.codUser = codUser;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProtestoInDTO [rut=");
		builder.append(rut);
		builder.append(", opcAcl=");
		builder.append(opcAcl);
		builder.append(", opcPrt=");
		builder.append(opcPrt);
		builder.append(", opcOmi=");
		builder.append(opcOmi);
		builder.append(", codUser=");
		builder.append(codUser);
		builder.append("]");
		return builder.toString();
	}

}
