package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_COMUNA database table.
 * 
 */
@Entity
@Table(name="TBL_COMUNA")
@NamedQuery(name="TblComuna.findAll", query="SELECT t FROM TblComuna t")
public class TblComuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_COMUNA_FLDCODCOMUNA_GENERATOR", sequenceName="FLD_CODCOMUNA")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_COMUNA_FLDCODCOMUNA_GENERATOR")
	@Column(name="FLD_CODCOMUNA")
	private long fldCodcomuna;

	@Column(name="FLD_CODREGION")
	private BigDecimal fldCodregion;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSACOMUNA")
	private String fldGlosacomuna;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblDireccioncomuna
	@OneToMany(mappedBy="tblComuna")
	private List<TblDireccioncomuna> tblDireccioncomunas;

	public TblComuna() {
	}

	public long getFldCodcomuna() {
		return this.fldCodcomuna;
	}

	public void setFldCodcomuna(long fldCodcomuna) {
		this.fldCodcomuna = fldCodcomuna;
	}

	public BigDecimal getFldCodregion() {
		return this.fldCodregion;
	}

	public void setFldCodregion(BigDecimal fldCodregion) {
		this.fldCodregion = fldCodregion;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosacomuna() {
		return this.fldGlosacomuna;
	}

	public void setFldGlosacomuna(String fldGlosacomuna) {
		this.fldGlosacomuna = fldGlosacomuna;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblDireccioncomuna> getTblDireccioncomunas() {
		return this.tblDireccioncomunas;
	}

	public void setTblDireccioncomunas(List<TblDireccioncomuna> tblDireccioncomunas) {
		this.tblDireccioncomunas = tblDireccioncomunas;
	}

	public TblDireccioncomuna addTblDireccioncomuna(TblDireccioncomuna tblDireccioncomuna) {
		getTblDireccioncomunas().add(tblDireccioncomuna);
		tblDireccioncomuna.setTblComuna(this);

		return tblDireccioncomuna;
	}

	public TblDireccioncomuna removeTblDireccioncomuna(TblDireccioncomuna tblDireccioncomuna) {
		getTblDireccioncomunas().remove(tblDireccioncomuna);
		tblDireccioncomuna.setTblComuna(null);

		return tblDireccioncomuna;
	}

}