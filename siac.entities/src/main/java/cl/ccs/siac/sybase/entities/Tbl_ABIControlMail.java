package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABIControlMail database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIControlMail.findAll", query="SELECT t FROM Tbl_ABIControlMail t")
public class Tbl_ABIControlMail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecEnvio")
	private Timestamp fld_FecEnvio;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	@Column(name="Fld_TotDetMailCalculados")
	private int fld_TotDetMailCalculados;

	@Column(name="Fld_TotDetMailGrabados")
	private int fld_TotDetMailGrabados;

	@Column(name="Fld_TotDetMailNoEnviados")
	private int fld_TotDetMailNoEnviados;

	@Column(name="Fld_TotMailCalculados")
	private int fld_TotMailCalculados;

	@Column(name="Fld_TotMailGrabados")
	private int fld_TotMailGrabados;

	@Column(name="Fld_TotMailNoEnviados")
	private int fld_TotMailNoEnviados;

	public Tbl_ABIControlMail() {
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecEnvio() {
		return this.fld_FecEnvio;
	}

	public void setFld_FecEnvio(Timestamp fld_FecEnvio) {
		this.fld_FecEnvio = fld_FecEnvio;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

	public int getFld_TotDetMailCalculados() {
		return this.fld_TotDetMailCalculados;
	}

	public void setFld_TotDetMailCalculados(int fld_TotDetMailCalculados) {
		this.fld_TotDetMailCalculados = fld_TotDetMailCalculados;
	}

	public int getFld_TotDetMailGrabados() {
		return this.fld_TotDetMailGrabados;
	}

	public void setFld_TotDetMailGrabados(int fld_TotDetMailGrabados) {
		this.fld_TotDetMailGrabados = fld_TotDetMailGrabados;
	}

	public int getFld_TotDetMailNoEnviados() {
		return this.fld_TotDetMailNoEnviados;
	}

	public void setFld_TotDetMailNoEnviados(int fld_TotDetMailNoEnviados) {
		this.fld_TotDetMailNoEnviados = fld_TotDetMailNoEnviados;
	}

	public int getFld_TotMailCalculados() {
		return this.fld_TotMailCalculados;
	}

	public void setFld_TotMailCalculados(int fld_TotMailCalculados) {
		this.fld_TotMailCalculados = fld_TotMailCalculados;
	}

	public int getFld_TotMailGrabados() {
		return this.fld_TotMailGrabados;
	}

	public void setFld_TotMailGrabados(int fld_TotMailGrabados) {
		this.fld_TotMailGrabados = fld_TotMailGrabados;
	}

	public int getFld_TotMailNoEnviados() {
		return this.fld_TotMailNoEnviados;
	}

	public void setFld_TotMailNoEnviados(int fld_TotMailNoEnviados) {
		this.fld_TotMailNoEnviados = fld_TotMailNoEnviados;
	}

}