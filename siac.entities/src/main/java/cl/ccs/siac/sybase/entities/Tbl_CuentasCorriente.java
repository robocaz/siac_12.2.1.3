package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CuentasCorrientes database table.
 * 
 */
@Entity
@Table(name="Tbl_CuentasCorrientes")
@NamedQuery(name="Tbl_CuentasCorriente.findAll", query="SELECT t FROM Tbl_CuentasCorriente t")
public class Tbl_CuentasCorriente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_NroCtaCte")
	private String fld_NroCtaCte;

	public Tbl_CuentasCorriente() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_NroCtaCte() {
		return this.fld_NroCtaCte;
	}

	public void setFld_NroCtaCte(String fld_NroCtaCte) {
		this.fld_NroCtaCte = fld_NroCtaCte;
	}

}