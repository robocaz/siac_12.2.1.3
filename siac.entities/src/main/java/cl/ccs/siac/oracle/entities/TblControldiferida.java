package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONTROLDIFERIDA database table.
 * 
 */
@Entity
@Table(name="TBL_CONTROLDIFERIDA")
@NamedQuery(name="TblControldiferida.findAll", query="SELECT t FROM TblControldiferida t")
public class TblControldiferida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ANOMES")
	private BigDecimal fldAnomes;

	@Column(name="FLD_ARCHIVO")
	private String fldArchivo;

	@Column(name="FLD_CORRCONTROLDIF_ID")
	private BigDecimal fldCorrcontroldifId;

	@Column(name="FLD_DISTRIBUIDOR")
	private String fldDistribuidor;

	@Column(name="FLD_ESTADO")
	private BigDecimal fldEstado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIO")
	private Date fldFechainicio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHALLEGADA")
	private Date fldFechallegada;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATERMINO")
	private Date fldFechatermino;

	@Column(name="FLD_REGISTROS")
	private BigDecimal fldRegistros;

	@Column(name="FLD_REGPROCESADOS")
	private BigDecimal fldRegprocesados;

	@Column(name="FLD_REGRESPUESTA")
	private BigDecimal fldRegrespuesta;

	public TblControldiferida() {
	}

	public BigDecimal getFldAnomes() {
		return this.fldAnomes;
	}

	public void setFldAnomes(BigDecimal fldAnomes) {
		this.fldAnomes = fldAnomes;
	}

	public String getFldArchivo() {
		return this.fldArchivo;
	}

	public void setFldArchivo(String fldArchivo) {
		this.fldArchivo = fldArchivo;
	}

	public BigDecimal getFldCorrcontroldifId() {
		return this.fldCorrcontroldifId;
	}

	public void setFldCorrcontroldifId(BigDecimal fldCorrcontroldifId) {
		this.fldCorrcontroldifId = fldCorrcontroldifId;
	}

	public String getFldDistribuidor() {
		return this.fldDistribuidor;
	}

	public void setFldDistribuidor(String fldDistribuidor) {
		this.fldDistribuidor = fldDistribuidor;
	}

	public BigDecimal getFldEstado() {
		return this.fldEstado;
	}

	public void setFldEstado(BigDecimal fldEstado) {
		this.fldEstado = fldEstado;
	}

	public Date getFldFechainicio() {
		return this.fldFechainicio;
	}

	public void setFldFechainicio(Date fldFechainicio) {
		this.fldFechainicio = fldFechainicio;
	}

	public Date getFldFechallegada() {
		return this.fldFechallegada;
	}

	public void setFldFechallegada(Date fldFechallegada) {
		this.fldFechallegada = fldFechallegada;
	}

	public Date getFldFechatermino() {
		return this.fldFechatermino;
	}

	public void setFldFechatermino(Date fldFechatermino) {
		this.fldFechatermino = fldFechatermino;
	}

	public BigDecimal getFldRegistros() {
		return this.fldRegistros;
	}

	public void setFldRegistros(BigDecimal fldRegistros) {
		this.fldRegistros = fldRegistros;
	}

	public BigDecimal getFldRegprocesados() {
		return this.fldRegprocesados;
	}

	public void setFldRegprocesados(BigDecimal fldRegprocesados) {
		this.fldRegprocesados = fldRegprocesados;
	}

	public BigDecimal getFldRegrespuesta() {
		return this.fldRegrespuesta;
	}

	public void setFldRegrespuesta(BigDecimal fldRegrespuesta) {
		this.fldRegrespuesta = fldRegrespuesta;
	}

}