angular.module('ccs').controller('IngresoFiniquitoController', function($scope, PersonaResource, FiniquitoResource, $location, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.show = false;
	$scope.afectado = {};
	$scope.campo = {};

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esProrroga = false;
	$scope.esIngreso = false;

	$scope.esReIngreso = false;
	$scope.esBloqueo = false;
	$scope.campoTmp = {};

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;
	$scope.esRutEmpJuridico = false;

	$('#ingresofiniquitos').on('show.bs.modal', function (e) {
		$timeout(function(){
			$scope.afectado.rut = undefined;
			$scope.limpiarAfectado();
			if ( !existeCaja() ) {
				swal('Atención','Debe iniciar una caja y cargar data de tramitante válida previo al proceso','warning');
				$('#ingresofiniquitos').modal('hide');
				return;
			}
			if ( !existeTramitante() ) {
				swal('Atención','Debe tener un tramitante para realizar esta operación.','warning');
				$('#ingresofiniquitos').modal('hide');
				return;
			}
			$scope.init();
			$("#rutAfectadoInforme").focus();
		});
	});

	$scope.init = function () {
		console.log("-> Finiquito..");
		console.log("Correlativo caja: " + $rootScope.tramitante.corrCaja);
		console.log($rootScope.tramitante);
		var successCallback = function (data, responseHeaders) {
			$scope.campo.cmbDocs = data;
			$scope.campo.tipoDoc = $scope.campo.cmbDocs[0];
		}
		FiniquitoResource.cmbdocs(successCallback, $scope.errorCallback);
	}

	function existeCaja() {
		if ( isUndefinedNullEmpty($rootScope.usuario.caja) || $rootScope.usuario.caja === '0' ) {
			$scope.show = false;
		} else {
			$scope.show = true;
		}
		return $scope.show;
	}

	function existeTramitante() {
		if ( isUndefinedOrNull($rootScope.tramitante) || isUndefinedNullEmpty($rootScope.tramitante.rut) || 
				isUndefinedNullEmpty($rootScope.tramitante.nombres) ) {
			$scope.show = false;
		} else {
			$scope.show = true;
		}
		return $scope.show;
	}

	function cleanFoco(flag) {
		switch(flag) {
		case 1:
			$scope.campo.fechaContrato = null;
			angular.element("#fecha_contrato_finiquito").val("");
			$("#fecha_contrato_finiquito").focus();
			break;
		case 2:
			$scope.campo.fechaTermino = null;
			angular.element("#fec_term_contrato_finiquito").val("");
			$("#fec_term_contrato_finiquito").focus();
			break;
		case 3:
			$scope.campo.fechaFiniquito = null;
			angular.element("#fecha_finiquito").val("");
			$("#fecha_finiquito").focus();
			break;
		case 4:
			$scope.campo.fechaDeclaracion = null;
			angular.element("#fecha_declaracion_jurada").val("");
			$("#fecha_declaracion_jurada").focus();
			break;
		}
	}

	$scope.esFecha = function(fechaStr, flag) {
		if ( isUndefinedNullEmpty(fechaStr) ) {
			return;
		}
		if ( !isValidDate(fechaStr) ) {
			errorFechaInvalida(flag);
		}
		if ( esFechaFutura(fechaStr) ) {
			errorFechaFutura(flag);
		}
		checkValFechas(fechaStr, flag);
	}

	function esFechaFutura(fechaStr) {
		var fecha = getDateStrFecha(fechaStr);
		var hoy = new Date();
		if ( fecha > hoy ) {
			return true;
		}
		return false;
	}

	function errorFechaFutura(flag) {
		swal({title:'Alerta', text:'Fecha no puede ser mayor a la fecha actual', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
			function() {
				switch(flag) {
				case 1:
					$scope.campo.fechaContrato = null;
					$("#fecha_contrato_finiquito").focus();
					break;
				case 2:
					$scope.campo.fechaTermino = null;
					$("#fec_term_contrato_finiquito").focus();
					break;
				case 3:
					$scope.campo.fechaFiniquito = null;
					$("#fecha_finiquito").focus();
					break;
				case 4:
					$scope.campo.fechaDeclaracion = null;
					$("#fecha_declaracion_jurada").focus();
					break;
				}
			});
	}

	function checkValFechas(fechaStr, flag) {
		switch(flag) {
		case 1:
			if ( !isUndefinedNullEmpty($scope.campo.fechaTermino) ) {
				var fechaContrato = getDateStrFecha(fechaStr);
				var fechaTerminoContrato = getDateStrFecha($scope.campo.fechaTermino);
				if ( fechaTerminoContrato < fechaContrato ) {
					swal({title:'Alerta', text:'Fec Term Contrato no puede ser menor a Fecha Contrato', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
							function() {
								cleanFoco(flag);
							}
						);
					return;
				}
			}
			break;
		case 2:
			var fechaTerminoContrato = getDateStrFecha(fechaStr);
			if ( !isUndefinedNullEmpty($scope.campo.fechaContrato) ) {
				var fechaContrato = getDateStrFecha($scope.campo.fechaContrato);
				if ( fechaTerminoContrato < fechaContrato ) {
					swal({title:'Alerta', text:'Fec Term Contrato no puede ser menor a Fecha Contrato', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
							function() {
								cleanFoco(flag);
							}
						);
					return;
				}
			}
			var fechaPasada = addMesesAFecha(new Date(), -6);
			if ( fechaTerminoContrato < fechaPasada ) {
				swal({title:'Alerta', text:'Fecha Término Contrato no debe tener más de seis meses, el Finiquito no será Beneficiado', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
						function() {
							cleanFoco(flag);
						}
					);
				return;
			}
			break;
		case 3:
			if ( !isUndefinedNullEmpty($scope.campo.fechaTermino) ) {
				var fechaFiniquito = getDateStrFecha(fechaStr);
				var fechaTerminoContrato = getDateStrFecha($scope.campo.fechaTermino);
				if ( fechaFiniquito < fechaTerminoContrato ) {
					swal({title:'Alerta', text:'Fecha Finiquito no puede ser menor a Fec Term Contrato', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
							function() {
								cleanFoco(flag);
							}
						);
					return;
				}
			}
			break;
		case 4:
			if ( !isUndefinedNullEmpty($scope.campo.fechaTermino) ) {
				var fechaDeclaracion = getDateStrFecha(fechaStr);
				var fechaTerminoContrato = getDateStrFecha($scope.campo.fechaTermino);
				fechaTerminoContrato.setMonth(fechaTerminoContrato.getMonth() + 2);
				if ( fechaDeclaracion < fechaTerminoContrato ) {
					swal({title:'Alerta', text:'Fecha Declaración Jurada no puede ser menor a la fecha Término Contrato + 2 meses', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
							function() {
								cleanFoco(flag);
							}
						);
					return;
				}
			}
			break;
		}
	}

	function addMesesAFecha(fecha, add) {
		let fechaFinal = new Date(fecha);
		fechaFinal.setMonth(fecha.getMonth() + parseInt(add));
		return fechaFinal;
	}

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

	function isValidDate(fechaStr) {
		if ( isUndefinedNullEmpty(fechaStr) ) {
			return false;
		}
		if ( fechaStr.length < 8 ) {
			return false;
		}
		fechaStr = fechaStr.replace(/-/g, "");
		var fechaArray = fechaStr.split("");
		var dia = fechaArray[0] + fechaArray[1];
		var mes = fechaArray[2] + fechaArray[3];
		var anio = fechaArray[4] + fechaArray[5] + fechaArray[6] + fechaArray[7];
		var dateStr = mes + "/" + dia + "/" + anio;
		var d = new Date(dateStr);
		return (d && d.getDate() == Number(dia) && (d.getMonth() + 1) == mes && d.getFullYear() == Number(anio));
	}

	function errorFechaInvalida(flag) {
		switch(flag) {
		case 1:
			$scope.campo.fechaContrato = null;
			swal({title:'Alerta', text:'Fecha Contrato Inválida', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
				function() {
					$("#fecha_contrato_finiquito").focus();
				});
			break;
		case 2:
			$scope.campo.fechaTermino = null;
			swal({title:'Alerta', text:'Fec Term Contrato Inválida', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
				function() {
					$("#fec_term_contrato_finiquito").focus();
				});
			break;
		case 3:
			$scope.campo.fechaFiniquito = null;
			swal({title:'Alerta', text:'Fecha Finiquito Inválida', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
				function() {
					$("#fecha_finiquito").focus();
				});
			break;
		case 4:
			$scope.campo.fechaDeclaracion = null;
			swal({title:'Alerta', text:'Fecha Declaración Jurada Inválida', type:'error', showCloseButton:true, confirmButtonText:'OK'}).then(
				function() {
					$("#fecha_declaracion_jurada").focus();
				});
			break;
		}
	}

	$scope.validaRut = function(rut) {
		if ( rut === undefined ) {
			return false;
		}
		if ( rut === null ) {
			swal('Error','Rut no es válido','error');
			return false;
		}
		if ( !esRutValido(rut) ) {
			swal('Error','Rut no es válido','error');
			return false;
		}
		return true;
	};

	function esRutValido(rut) {
		var rutNumerico	= (rut == undefined) ? 0 : parseInt(rut.slice(0,rut.length-1).trim());
		if ( RutHelper.validate(rut) && rutNumerico > rutMinimo ) {
			return true;
		} else {
			return false;
		}
	}

	function esRutJuridico(rut) {
		var rutNumerico = (rut == undefined) ? 0 : parseInt(rut.slice(0, rut.length-1).trim());
		if ( rutNumerico >= rutJuridico ) {
			return true;
		}
		return  false;
	}

	$scope.buscaEmpleador = function() {
		
		if ( !$scope.validaRut($scope.campo.rutEmpleador) ) {
			$scope.campo.rutEmpleador = undefined;
			$scope.campo.nombres = null;
			$scope.campo.apellidop = null;
			$scope.campo.apellidom = null;
			return;
		}
		
		if ( esRutJuridico($scope.campo.rutEmpleador) ) {
			$scope.esRutEmpJuridico = true;
		} else {
			$scope.esRutEmpJuridico = false;
		}
		
		var trx = {
			rut : $scope.campo.rutEmpleador
		}
		
		var successCallback = function(data, responseHeaders) {
			console.log("< Data empleador");
			console.log(data);
			
			if ( data.correlativo === 0 ){
				$scope.campo.nombres = null;
				$scope.campo.apellidop = null;
				$scope.campo.apellidom = null;
				swal('Alerta','Empleador no encontrado','error');
				
			} else {
				if ( $scope.esRutEmpJuridico ) {
					$scope.campo.nombres = data.apellidoPaterno + data.apellidoMaterno + data.nombres;
				} else {
					$scope.campo.nombres = data.nombres;
					$scope.campo.apellidop = data.apellidoPaterno;
					$scope.campo.apellidom = data.apellidoMaterno;
				}
			}
		};
		PersonaResource.obtenerPersona(trx, successCallback, $scope.errorCallback);
	};

	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		$scope.buscaAfectado();
	}

	$scope.limpiarAfectado = function() {
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
		$scope.afectado.nombreafectado	= null;
		$scope.esrutjuridico = false;
		$scope.editable = false;
	};

	$scope.buscaAfectado = function() {
		$scope.limpiarAfectado();
		$scope.esvalido = false;

		if ( $scope.afectado.rut === undefined ) {
			return;
		}
		if ( $scope.afectado.rut === null ) {
			$scope.afectado.rut = undefined;
			swal({ title:'Error', text:'Rut no es válido', type:'error', showCloseButton:true , confirmButtonText:'cerrar'}).then(
				function() {
					$("#rutAfectadoInforme").focus();
				});
			return;
		}

		if ( esRutValido($scope.afectado.rut) ) {
			
			if ( !esRutJuridico($scope.afectado.rut) ) {
				$scope.esrutjuridico = false;
				$scope.obtenerAfectadoTramitante();
			} else {
				$scope.esrutjuridico = true;
				swal({ title:'Error', text:'Solo puede ingresar RUT de personas naturales', type:'error', showCloseButton:true, confirmButtonText:'cerrar'});
				$scope.afectado.rut = null;
			}
			
		} else {
			swal({ title:'Error', text:'Rut no es válido', type:'error', showCloseButton:true, confirmButtonText:'cerrar'});
			$scope.afectado.rut = null;
		}
	}

	$scope.obtenerAfectadoTramitante = function() {
		var trx = {
			rut : $scope.afectado.rut
		}
		var successCallback = function(data,responseHeaders){
			$scope.esvalido = true;
			console.log("> Correlativo afectado: " + data.correlativo);
			if ( data.correlativo == "0" ){
				$scope.editable = true;
				swal('Alerta','Afectado no encontrado','error');
				$('#rutAfectadoInforme').focus();
			} else {
				$scope.editable = false;
				if ( $scope.esrutjuridico ) {
					$scope.afectado.nombreafectado = data.apellidoPaterno + data.apellidoMaterno + data.nombres;
				} else {
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}
				$('#ap_pat_pag').focus();
			}
		};
		PersonaResource.obtenerPersona(trx, successCallback, $scope.errorCallback);
	};

	$scope.confirmarAfectado = function() {
		if ( isUndefinedNullEmpty($scope.afectado.rut) ) {
			swal('Alerta','Debe ingresar un Afectado','error');
			return;
		}
		$scope.esBloqueo = false;
		$scope.consultarFiniquitos();
	}

	function limpiaIngreso() {
		$scope.esIngreso = true;
		$scope.esReIngreso = false;
		$scope.esProrroga = false;
		$scope.campo.correlativo = 0;
		$scope.campo.codAfp = 0;
		$scope.campo.rutEmpleador = undefined;
		$scope.campo.apellidop = null;
		$scope.campo.apellidom = null;
		$scope.campo.nombres = null;
		$scope.campo.fechaContrato = null;
		$scope.campo.fechaTermino = null;
		$scope.campo.fechaFiniquito = null;
		$scope.campo.tipoDoc = $scope.campo.cmbDocs[0];
		$scope.campo.fechaDeclaracion = null;
	}

	$scope.consultarFiniquitos = function () {
		
		var successCallback = function(data, responseHeaders) {
			console.log("> Data finiquito:");
			console.log(data);
			limpiaIngreso();
			
			if ( data.corrFiniquito != "0" ) {
				$scope.campo.rutEmpleador = data.rutEmpresa;
				
				if ( esRutJuridico(data.rutEmpresa) ) {
					$scope.esRutEmpJuridico = true;
					$scope.campo.nombres = data.nombreApPatEmp + data.nombreApMatEmp + data.nombreNombresEmp;
				} else {
					$scope.esRutEmpJuridico = false;
					$scope.campo.apellidop = data.nombreApPatEmp;
					$scope.campo.apellidom = data.nombreApMatEmp;
					$scope.campo.nombres = data.nombreNombresEmp;
				}
				
				$scope.campo.fechaContrato = data.fecIniContrato;
				$scope.campo.fechaTermino = data.fecFinContrato;
				$scope.campo.fechaFiniquito = data.fecFiniquito;
				$scope.campo.tipoDoc = getOptionFromCode(data.docPresentadoFnq);
				
				if ( !data.marcaProrroga ) {
					swal({
						title: 'Alerta',
						text: "Ya existe finiquito para RUT de Afectado ¿Desea prorrogarlo?",
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Si',
						cancelButtonText: 'No'
						
					}).then( function() {
						$scope.esProrroga = true;
						$scope.campo.correlativo = data.corrFiniquito;
						$scope.campo.fechaDeclaracion = data.fecDecJurada;
						$scope.$digest();
						
					}, function (dismiss) {
						$scope.esProrroga = false;
						limpiaIngreso();
						$scope.$digest();
					})
				} else {
					swal({
						title: 'Alerta',
						text: "Este Finiquito ya fue prorrogado, Desea ingresar Otro Finiquito para el mismo Rut Afectado?",
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Si',
						cancelButtonText: 'No'
						
					}).then( function() {
						limpiaIngreso();
						$scope.$digest();
						
					}, function (dismiss) {
						$scope.esBloqueo = true;
						$scope.$digest();
					})
				}
			}
		};
		FiniquitoResource.consulta($scope.afectado.rut, successCallback, $scope.errorCallback);
	};

	function getOptionFromCode(cod) {
		for (var i=0; i < $scope.campo.cmbDocs.length; i++) {
			var opt = $scope.campo.cmbDocs[i];
			if ( opt.codigo == cod ) {
				return opt;
			}
		}
		return null;
	}

	function getLabelFromCode(cod) {
		for (var i=0; i < $scope.campo.cmbDocs.length; i++) {
			var opt = $scope.campo.cmbDocs[i];
			if ( opt.codigo == cod ) {
				return opt.descripcion;
			}
		}
		return null;
	}


	function verificaReIngreso() {
		if ( $scope.campoTmp.fechaContrato != $scope.campo.fechaContrato ) {
			return false;
		}
		if ( $scope.campoTmp.fechaTermino != $scope.campo.fechaTermino ) {
			return false;
		}
		if ( $scope.campoTmp.fechaFiniquito != $scope.campo.fechaFiniquito ) {
			return false;
		}
		if ( $scope.campoTmp.codigo != $scope.campo.tipoDoc.codigo ) {
			return false;
		}
		return true;
	}

	function limpiarReIngreso() {
		$scope.campoTmp.fechaContrato = null;
		$scope.campoTmp.fechaTermino = null;
		$scope.campoTmp.fechaFiniquito = null;
		$scope.campoTmp.codigo = null;
		$scope.campo.fechaContrato = null;
		$scope.campo.fechaTermino = null;
		$scope.campo.fechaFiniquito = null;
		$scope.campo.tipoDoc = $scope.campo.cmbDocs[0];
	}

	$scope.guardarDatos = function() {
		
		if ( $scope.validaDatos() ) {
			
			if ( !$scope.esReIngreso && ($scope.afectado.rut == $scope.campo.rutEmpleador) ) {
				
				swal({
					title: 'Alerta',
					text: "RUT de Afectado y Empleador son iguales, ¿Desea continuar?",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Aceptar',
					cancelButtonText: 'Cancelar'
					
				}).then( function() {
					guardarDatosImpl();
					$scope.$digest();
				}, function (dismiss) {
					$scope.campo.rutEmpleador = null;
					$scope.campo.apellidop = null;
					$scope.campo.apellidom = null;
					$scope.campo.nombres = null;
					$("#rut_finiquito").focus();
					$scope.$digest();
					return;
				})
				
			} else {
				guardarDatosImpl();
			}
			
		}
	};

	function guardarDatosImpl() {
		if ( !$scope.esProrroga ) {
			if ( !$scope.esReIngreso ) {
				$scope.preGuardado();
				swal('Alerta','Ingrese los datos del finiquito nuevamente','warning');
				$scope.esReIngreso = true;
				return;
			} else {
				if ( !validaIngresoDatos() ) {
					return;
				}
				if ( !verificaReIngreso() ) {
					swal('Error','Información ingresada no es igual a la primera instancia','error');
					limpiarReIngreso();
					$scope.esReIngreso = false;
					return;
				} else {
					swal({
						title: 'Confirmación',
						text: '¿Desea ingresar el finiquito?',
						type: 'question',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Si',
						cancelButtonText: 'No'
					}).then( function() {
						$scope.guardar();
					}, function(dismiss) {
						
					})
				}
			}
		} else {
			swal({
				title: 'Confirmación',
				text: '¿Desea ingresar la prórroga?',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si',
				cancelButtonText: 'No'
			}).then( function() {
				$scope.guardar();
			}, function(dismiss) {
				
			})
		}
	}
	
	function validaIngresoDatos() {
		
		if ( !validaFecha($scope.campo.fechaContrato, 1) ) {
			return false;
		}
		
		if ( !validaFecha($scope.campo.fechaTermino, 2) ) {
			return false;
		}
		
		if ( !validaFecha($scope.campo.fechaFiniquito, 3) ) {
			return false;
		}
		
		if ( $scope.campo.tipoDoc === null || $scope.campo.tipoDoc === undefined || $scope.campo.tipoDoc.codigo == null) {
			swal('Alerta','Debe seleccionar un documento','error');
			$("#doc_presentado").focus();
			return false;
		}
		
		return true;
	}

	$scope.preGuardado = function() {
		$scope.campoTmp.fechaContrato = $scope.campo.fechaContrato;
		$scope.campoTmp.fechaTermino = $scope.campo.fechaTermino;
		$scope.campoTmp.fechaFiniquito = $scope.campo.fechaFiniquito;
		$scope.campoTmp.codigo = $scope.campo.tipoDoc.codigo;
		$scope.campo.fechaContrato = null;
		$scope.campo.fechaTermino = null;
		$scope.campo.fechaFiniquito = null;
		$scope.campo.tipoDoc = {};
	};

	$scope.validaDatos = function() {
		console.log("> Validando datos..");
		
		if ( $scope.esrutjuridico ) {
			swal('Error','Debe ingresar un Afectado válido','error');
			return false;
		}
		if ( $scope.afectado.rut === null ) {
			swal('Alerta','Debe ingresar un Afectado','error');
			return false;
		}
		
		if ( !$scope.esProrroga ) {
			
			if ( isUndefinedNullEmpty($scope.campo.rutEmpleador) ) {
				swal('Alerta','Debe ingresar RUT','error');
				return false;
			}
			if ( !esRutValido($scope.campo.rutEmpleador) ) {
				swal('Alerta','RUT no es válido','error');
				return false;
			}
			if ( !$scope.esRutEmpJuridico ) {
				if ( isUndefinedNullEmpty($scope.campo.apellidop) ) {
					swal('Alerta','Debe ingresar Ap. Paterno','error');
					return false;
				}
				if ( isUndefinedNullEmpty($scope.campo.apellidom) ) {
					swal('Alerta','Debe ingresar Ap. Materno','error');
					return false;
				}
			}
			if ( isUndefinedNullEmpty($scope.campo.nombres) ) {
				swal('Alerta','Debe ingresar Nombres','error');
				return false;
			}
			
			if ( !validaFecha($scope.campo.fechaContrato, 1) ) {
				return false;
			}
			if ( esFechaFutura($scope.campo.fechaContrato) ) {
				swal('Alerta','Fecha Contrato no puede ser mayor a la fecha actual','error');
				return false;
			}
			if ( !validaFecha($scope.campo.fechaTermino, 2) ) {
				return false;
			}
			var fechaContrato = getDateStrFecha($scope.campo.fechaContrato);
			var hoy = new Date();
			var fechaTerminoContrato = getDateStrFecha($scope.campo.fechaTermino);
			if ( fechaTerminoContrato < fechaContrato ) {
				swal('Alerta','Fec Term Contrato no puede ser menor a Fecha Contrato','error');
				return false;
			}
			var fechaPasada = addMesesAFecha(hoy, -6);
			if ( fechaTerminoContrato < fechaPasada ) {
				swal('Alerta','Fecha Término Contrato no debe tener más de seis meses, el Finiquito no será Beneficiado','error');
				return false;
			}
			if ( fechaTerminoContrato > hoy ) {
				swal('Alerta','Fec Term Contrato no puede ser mayor a la fecha actual','error');
				return false;
			}
			if ( !validaFecha($scope.campo.fechaFiniquito, 3) ) {
				return false;
			}
			var fechaFiniquito = getDateStrFecha($scope.campo.fechaFiniquito);
			if ( fechaFiniquito < fechaTerminoContrato ) {
				swal('Alerta','Fecha Finiquito no puede ser menor a Fec Term Contrato','error');
				return false;
			}
			if ( fechaFiniquito > hoy ) {
				swal('Alerta','Fecha Finiquito no puede ser mayor a la fecha actual','error');
				return false;
			}
			if ( $scope.campo.tipoDoc === null || $scope.campo.tipoDoc === undefined ) {
				swal('Alerta','Debe seleccionar un documento','error');
				$("#doc_presentado").focus();
				return false;
			}
			
		} else {
			if ( !validaFecha($scope.campo.fechaDeclaracion, 4) ) {
				return false;
			}
			
			if ( esFechaFutura($scope.campo.fechaDeclaracion) ) {
				swal('Alerta','Fecha Declaración Jurada no puede ser una fecha futura','error');
				return false;
			}
			var fechaDeclaracion = getDateStrFecha($scope.campo.fechaDeclaracion);
			var fechaTerminoContrato = getDateStrFecha($scope.campo.fechaTermino);
			fechaTerminoContrato.setMonth(fechaTerminoContrato.getMonth() + 2);
			if ( fechaDeclaracion < fechaTerminoContrato ) {
				swal('Alerta','Fecha Declaración Jurada no puede ser menor a la fecha Término Contrato + 2 meses', 'error');
				return false;
			}
		}
		
		return true;
	};

	function validaFecha(fecha, flag) {
		if ( isUndefinedNullEmpty(fecha) ) {
			errorFechaNula(flag);
			return false;
		}
		if ( !isValidDate(fecha) ) {
			errorFechaInvalida(flag);
			return false;
		}
		return true;
	}

	function errorFechaNula(flag) {
		switch(flag) {
			case 1:
				swal('Alerta','Ingrese Fecha Contrato','error');
				$("#fecha_contrato_finiquito").focus();
				break;
			case 2:
				swal('Alerta','Ingrese Fec Term Contrato','error');
				$("#fec_term_contrato_finiquito").focus();
				break;
			case 3:
				swal('Alerta','Ingrese Fecha Finiquito','error');
				$("#fecha_finiquito").focus();
				break;
			case 4:
				swal('Alerta','Ingrese Fecha Declaración Jurada','error');
				$("#fecha_declaracion_jurada").focus();
				break;
		}
	}

	function getDateStrFecha(strFecha) {
		strFecha = strFecha.replace(/-/g, "");
		let dia = strFecha.substr(0, 2);
		let mes = strFecha.substr(2, 2);
		let anio = strFecha.substr(4, 4);
		let fecha = new Date(anio +'-'+ mes +'-'+ dia);
		return fecha;
	}

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#ingresofiniquitos')),
			size: 'sm'
		});
		modalInstance.result.then(function (respuesta) {
			if(respuesta){
				$scope.guardar();
			}
		});
	};

	$scope.guardar = function(){
		// if($scope.editable){
		$scope.guardarPersona();
		//	}else{
		//$scope.guardarServicio();
		//	}
	}

	$scope.guardarServicio = function() {
		
		if ( isUndefinedNullEmpty($rootScope.tramitante.corrCaja) ) {
			console.log("Correlativo de caja: " + $rootScope.tramitante.corrCaja);
			console.log($rootScope.tramitante);
			swal('Error','No existe correlativo de caja','error');
			return;
		}
		
		var trx = {
			corrFiniquito 		: $scope.campo.correlativo,
			corrCaja 			: $rootScope.tramitante.corrCaja,
			rutAfectado 		: $scope.afectado.rut,
			nombreApPat 		: $scope.afectado.apellidoPaterno,
			nombreApMat 		: $scope.afectado.apellidoMaterno,
			nombreNombres 		: $scope.afectado.nombres,
			rutEmpresa 			: $scope.campo.rutEmpleador,
			nombreApPatEmp 		: $scope.campo.apellidop,
			nombreApMatEmp		: $scope.campo.apellidom,
			nombreNombresEmp 	: $scope.campo.nombres,
			fecIniContrato 		: formatFechaStr($scope.campo.fechaContrato),
			fecFinContrato 		: formatFechaStr($scope.campo.fechaTermino),
			fecFiniquito 		: formatFechaStr($scope.campo.fechaFiniquito),
			marcaProrroga 		: $scope.esProrroga,
			docPresentadoFnq 	: $scope.campo.tipoDoc.codigo,
			rutTramitante 		: $rootScope.tramitante.rut,
			corrNomTram 		: $rootScope.tramitante.correlativo
		}
		
		if ( $scope.esRutEmpJuridico ) {
			trx.nombreApPatEmp = $scope.campo.nombres.substring(0,25);
			trx.nombreApMatEmp = $scope.campo.nombres.substring(25,50);
			trx.nombreNombresEmp = $scope.campo.nombres.substring(50,75);
		}
		
		if ( $scope.esProrroga ) {
			trx.fecDecJurada = formatFechaStr($scope.campo.fechaDeclaracion);
		}
		
		console.log("> Guarda trx");
		console.log(trx);
		
		var successCallback = function(data, responseHeaders) {
			console.log("< Guarda trx");
			console.log(data);
			
			swal('Atención','Recuerde guardar copia de la Cédula de Identidad Vigente','warning');
			
			if ( data.estadoRespuesta === 0 ) {
				swal({
					title: 'Operación Exitosa',
					text: "Actualización realizada exitosamente",
					type: 'success',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Continuar'
				}).then(function() {
					if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
						swal('Atención','Recuerde guardar copia de la Cédula de Identidad Vigente','warning');
					}
					$scope.pdf();
					$scope.limpiarAfectado();
					$scope.afectado.rut = null;
					limpiaIngreso();
					$scope.esIngreso = false;
					$scope.$digest();
				})
			} else if ( data.estadoRespuesta === -1 ) {
				swal('Error', 'Ocurrió un error al procesar su petición', 'error');
			} else {
				swal('Atención', data.mensaje, 'warning');
			}
			
		};
		FiniquitoResource.guarda(trx, successCallback, $scope.errorCallback);
	}

	function formatFechaStr(fecha) {
		fecha = fecha.replace(/-/g, "");
		if ( isUndefinedOrNull(fecha) || !isValidDate(fecha) ) {
			return "";
		}
		let dia = fecha.substr(0, 2);
		let mes = fecha.substr(2, 2);
		let anio = fecha.substr(4, 4);
		return anio +'-'+ mes +'-'+ dia;
	}

	$scope.guardarPersona = function(){
		if ( $scope.esrutjuridico ) {
			$scope.afectado.apellidoPaterno = $scope.afectado.nombreafectado.substring(0, 25);
			$scope.afectado.apellidoMaterno = $scope.afectado.nombreafectado.substring(25, 50);
			$scope.afectado.nombres 		= $scope.afectado.nombreafectado.substring(50, 75);
		}
		console.log("> Guarda afectado");
		console.log($scope.afectado);
		
		var successCallback = function(data,responseHeaders){
			console.log("< Guarda afectado");
			console.log(data);
			
			if ( data.correlativo == "0" ) {
				swal('Alerta','No se ha actualizado el afectado','error');
			} else{
				console.log("Correlativo afectado: " + data.correlativo);
				$scope.afectado.corrNombre = data.correlativo;
				$scope.guardarEmpleador();
			}
		};
		PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};

	$scope.guardarEmpleador = function(){
		var trx = {
			rut				: formatRut($scope.campo.rutEmpleador),
			apellidoPaterno	: $scope.campo.apellidop,
			apellidoMaterno	: $scope.campo.apellidom,
			nombres			: $scope.campo.nombres
		}
		if ( $scope.esRutEmpJuridico ) {
			trx.apellidoPaterno = $scope.campo.nombres.substring(0, 25);
			trx.apellidoMaterno = $scope.campo.nombres.substring(25, 50);
			trx.nombres 		= $scope.campo.nombres.substring(50, 75);
		}
		console.log("> Guarda empleador");
		console.log(trx);
		
		var successCallback = function(data,responseHeaders){
			console.log("< Guarda empleador");
			console.log(data);
			
			if ( data.correlativo == "0" ) {
				swal('Alerta','No se ha actualizado el empleador','error');
			} else{
				console.log("Correlativo empleador: " + data.correlativo);
				$scope.guardarServicio();
			}
		};
		PersonaResource.actualizar(trx, successCallback, $scope.errorCallback);
	};

	$scope.cambiaFoco = function(){
		$("#aceptarBoton").focus();
	}

	$scope.cancelar = function() {
		$scope.esProrroga = false;
		$scope.esIngreso = false;
	}

	$scope.cerrar = function() {
		$scope.afectado = {};
		$scope.campo = {};
		$scope.esProrroga = false;
		$scope.esIngreso = false;
		$scope.esvalido = false;
		$scope.editable = false;
		$scope.esrutjuridico = false;
		$scope.show = false;
		angular.element("#ingresofiniquitos").modal('hide');
	}

	$scope.pdf = function () {
		console.log("-> Crear PDF..");
		var trx = {
			marcaProrroga	 : $scope.esProrroga,
			rutTramitante	 : formatRut($rootScope.tramitante.rut),
			rutAfectado		 : formatRut($scope.afectado.rut),
			nombreApPat		 : $scope.afectado.apellidoPaterno,
			nombreApMat		 : $scope.afectado.apellidoMaterno,
			nombreNombres	 : $scope.afectado.nombres,
			rutEmpresa		 : formatRut($scope.campo.rutEmpleador),
			nombreApPatEmp	 : $scope.campo.apellidop,
			nombreApMatEmp	 : $scope.campo.apellidom,
			nombreNombresEmp : $scope.campo.nombres,
			fecFinContrato	 : formatoFecha($scope.campo.fechaTermino),
			fecFiniquito	 : formatoFecha($scope.campo.fechaFiniquito),
			mensaje			 : getLabelFromCode($scope.campo.tipoDoc.codigo)
		}
		
		trx.nombreTramitante = $rootScope.tramitante.apellidoPaterno +", "+ $rootScope.tramitante.apellidoMaterno +", "+ $rootScope.tramitante.nombres;
		
		if ( $scope.esRutEmpJuridico ) {
			trx.nombreApPatEmp = $scope.campo.nombres.substring(0,25);
			trx.nombreApMatEmp = $scope.campo.nombres.substring(25,50);
			trx.nombreNombresEmp = $scope.campo.nombres.substring(50,75);
		}
		
		var successCallback = function (data, responseHeaders) {
			// Base64 String
			var base64str = data.reporte;
			
			// Decode Base64 String, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			
			for ( var i = 0; i < len; i++ ) {
				view[i] = binary.charCodeAt(i);
			}
			
			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "form_cesantia.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
		FiniquitoResource.pdf(trx, successCallback, $scope.errorCallback);
	}

	function formatRut(_value) {
		_value = cleanRut(_value);
		if ( !_value ) {
			return null;
		}
		if ( _value.length <= 1 ) {
			return _value;
		}
		var result = _value.slice(-4,-1) + '-' + _value.substr(_value.length-1);
		for ( var i=4; i < _value.length; i+=3 ) {
			result = _value.slice(-3-i,-i) + '.' + result;
		}
		return result;
	}

	function cleanRut(_value) {
		return typeof _value === 'string' ? _value.replace(/[^0-9kK]+/g,'').replace(/^0+/, '').toUpperCase() : '';
	}

	function formatoFecha(strFecha) {
		strFecha = strFecha.replace(/-/g, "");
		let dia = strFecha.substr(0, 2);
		let mes = strFecha.substr(2, 2);
		let anio = strFecha.substr(4, 4);
		return  dia +'/'+ mes +'/'+ anio;
	}

});
