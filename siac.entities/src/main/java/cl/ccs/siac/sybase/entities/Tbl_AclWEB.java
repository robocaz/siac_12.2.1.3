package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AclWEB database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclWEB.findAll", query="SELECT t FROM Tbl_AclWEB t")
public class Tbl_AclWEB implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecProtAM")
	private String fld_FecProtAM;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_Marca3Dig")
	private boolean fld_Marca3Dig;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MarcaAclEspecial")
	private boolean fld_MarcaAclEspecial;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_SwDuplicado")
	private boolean fld_SwDuplicado;

	@Column(name="Fld_SwPrescrito")
	private boolean fld_SwPrescrito;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_AclWEB() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public String getFld_FecProtAM() {
		return this.fld_FecProtAM;
	}

	public void setFld_FecProtAM(String fld_FecProtAM) {
		this.fld_FecProtAM = fld_FecProtAM;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public boolean getFld_Marca3Dig() {
		return this.fld_Marca3Dig;
	}

	public void setFld_Marca3Dig(boolean fld_Marca3Dig) {
		this.fld_Marca3Dig = fld_Marca3Dig;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public boolean getFld_MarcaAclEspecial() {
		return this.fld_MarcaAclEspecial;
	}

	public void setFld_MarcaAclEspecial(boolean fld_MarcaAclEspecial) {
		this.fld_MarcaAclEspecial = fld_MarcaAclEspecial;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public boolean getFld_SwDuplicado() {
		return this.fld_SwDuplicado;
	}

	public void setFld_SwDuplicado(boolean fld_SwDuplicado) {
		this.fld_SwDuplicado = fld_SwDuplicado;
	}

	public boolean getFld_SwPrescrito() {
		return this.fld_SwPrescrito;
	}

	public void setFld_SwPrescrito(boolean fld_SwPrescrito) {
		this.fld_SwPrescrito = fld_SwPrescrito;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}