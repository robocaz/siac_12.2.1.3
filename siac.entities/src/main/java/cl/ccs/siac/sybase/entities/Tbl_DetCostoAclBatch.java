package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetCostoAclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetCostoAclBatch.findAll", query="SELECT t FROM Tbl_DetCostoAclBatch t")
public class Tbl_DetCostoAclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_MontoTramo")
	private int fld_MontoTramo;

	@Column(name="Fld_TipoAclRech")
	private boolean fld_TipoAclRech;

	@Column(name="Fld_TotalMovimientos")
	private int fld_TotalMovimientos;

	@Column(name="Fld_TotDiferencias")
	private BigDecimal fld_TotDiferencias;

	@Column(name="Fld_TotMontoInformado")
	private BigDecimal fld_TotMontoInformado;

	public Tbl_DetCostoAclBatch() {
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public int getFld_MontoTramo() {
		return this.fld_MontoTramo;
	}

	public void setFld_MontoTramo(int fld_MontoTramo) {
		this.fld_MontoTramo = fld_MontoTramo;
	}

	public boolean getFld_TipoAclRech() {
		return this.fld_TipoAclRech;
	}

	public void setFld_TipoAclRech(boolean fld_TipoAclRech) {
		this.fld_TipoAclRech = fld_TipoAclRech;
	}

	public int getFld_TotalMovimientos() {
		return this.fld_TotalMovimientos;
	}

	public void setFld_TotalMovimientos(int fld_TotalMovimientos) {
		this.fld_TotalMovimientos = fld_TotalMovimientos;
	}

	public BigDecimal getFld_TotDiferencias() {
		return this.fld_TotDiferencias;
	}

	public void setFld_TotDiferencias(BigDecimal fld_TotDiferencias) {
		this.fld_TotDiferencias = fld_TotDiferencias;
	}

	public BigDecimal getFld_TotMontoInformado() {
		return this.fld_TotMontoInformado;
	}

	public void setFld_TotMontoInformado(BigDecimal fld_TotMontoInformado) {
		this.fld_TotMontoInformado = fld_TotMontoInformado;
	}

}