package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutsRectifAclEsp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutsRectifAclEsp.findAll", query="SELECT t FROM Tbl_RutsRectifAclEsp t")
public class Tbl_RutsRectifAclEsp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrDocum")
	private BigDecimal fld_CorrDocum;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApMat_DD")
	private String fld_Nombre_ApMat_DD;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_ApPat_DD")
	private String fld_Nombre_ApPat_DD;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Nombre_Nombres_DD")
	private String fld_Nombre_Nombres_DD;

	@Column(name="Fld_Rut_SinFormato")
	private String fld_Rut_SinFormato;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutAfectado_DD")
	private String fld_RutAfectado_DD;

	public Tbl_RutsRectifAclEsp() {
	}

	public BigDecimal getFld_CorrDocum() {
		return this.fld_CorrDocum;
	}

	public void setFld_CorrDocum(BigDecimal fld_CorrDocum) {
		this.fld_CorrDocum = fld_CorrDocum;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApMat_DD() {
		return this.fld_Nombre_ApMat_DD;
	}

	public void setFld_Nombre_ApMat_DD(String fld_Nombre_ApMat_DD) {
		this.fld_Nombre_ApMat_DD = fld_Nombre_ApMat_DD;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_ApPat_DD() {
		return this.fld_Nombre_ApPat_DD;
	}

	public void setFld_Nombre_ApPat_DD(String fld_Nombre_ApPat_DD) {
		this.fld_Nombre_ApPat_DD = fld_Nombre_ApPat_DD;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_Nombre_Nombres_DD() {
		return this.fld_Nombre_Nombres_DD;
	}

	public void setFld_Nombre_Nombres_DD(String fld_Nombre_Nombres_DD) {
		this.fld_Nombre_Nombres_DD = fld_Nombre_Nombres_DD;
	}

	public String getFld_Rut_SinFormato() {
		return this.fld_Rut_SinFormato;
	}

	public void setFld_Rut_SinFormato(String fld_Rut_SinFormato) {
		this.fld_Rut_SinFormato = fld_Rut_SinFormato;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutAfectado_DD() {
		return this.fld_RutAfectado_DD;
	}

	public void setFld_RutAfectado_DD(String fld_RutAfectado_DD) {
		this.fld_RutAfectado_DD = fld_RutAfectado_DD;
	}

}