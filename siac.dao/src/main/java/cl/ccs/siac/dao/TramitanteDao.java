package cl.ccs.siac.dao;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteCajaDTO;
import cl.exe.ccs.dto.TramitanteDTO;

public interface TramitanteDao {
	
	public TramitanteDTO obtenerTramitante(String rut);
	
	public TramitanteDTO actualizarTramitante(TramitanteDTO tramitante);
	
	public TramitanteCajaDTO obtenerTramitanteCaja(Long corrCaja);
	
	public ResponseDTO insertarTramitanteCaja(TramitanteCajaDTO tramitanteCajaDTO);
	
	public ResponseDTO actualizarTramitanteCaja(TramitanteCajaDTO tramitanteCajaDTO);
	
	public ResponseDTO deleteTramitanteCaja(Long corrCaja);

	public TramitanteCajaDTO obtieneDatosDeUso(TramitanteCajaDTO tramitanteCajaDTO);

}
