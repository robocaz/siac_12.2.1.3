package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoAclResumen database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoAclResumen.findAll", query="SELECT t FROM Tbl_TipoAclResumen t")
public class Tbl_TipoAclResumen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaAclaracion")
	private String fld_GlosaAclaracion;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_TipoAclResumen() {
	}

	public String getFld_GlosaAclaracion() {
		return this.fld_GlosaAclaracion;
	}

	public void setFld_GlosaAclaracion(String fld_GlosaAclaracion) {
		this.fld_GlosaAclaracion = fld_GlosaAclaracion;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}