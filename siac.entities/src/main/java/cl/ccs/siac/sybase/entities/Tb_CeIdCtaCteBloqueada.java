package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdCtaCteBloqueada database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdCtaCteBloqueada.findAll", query="SELECT t FROM Tb_CeIdCtaCteBloqueada t")
public class Tb_CeIdCtaCteBloqueada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDCTACTEBLOQUEADA_FLD_CORRCTACTEBLQ_ID_GENERATOR", sequenceName="FLD_CORRCTACTEBLQ_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDCTACTEBLOQUEADA_FLD_CORRCTACTEBLQ_ID_GENERATOR")
	@Column(name="Fld_CorrCtaCteBlq_Id")
	private long fld_CorrCtaCteBlq_Id;

	@Column(name="Fld_CodBanco")
	private int fld_CodBanco;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_NroCtaCte")
	private String fld_NroCtaCte;

	@Column(name="Fld_NroCtaCteReal")
	private String fld_NroCtaCteReal;

	//bi-directional many-to-one association to Tb_CeIdRelRutCtaBloqueada
	@OneToMany(mappedBy="tbCeIdCtaCteBloqueada")
	private List<Tb_CeIdRelRutCtaBloqueada> tbCeIdRelRutCtaBloqueadas;

	//bi-directional many-to-one association to Tb_CeIdSegtoBloqueoCtaCte
	@OneToMany(mappedBy="tbCeIdCtaCteBloqueada")
	private List<Tb_CeIdSegtoBloqueoCtaCte> tbCeIdSegtoBloqueoCtaCtes;

	public Tb_CeIdCtaCteBloqueada() {
	}

	public long getFld_CorrCtaCteBlq_Id() {
		return this.fld_CorrCtaCteBlq_Id;
	}

	public void setFld_CorrCtaCteBlq_Id(long fld_CorrCtaCteBlq_Id) {
		this.fld_CorrCtaCteBlq_Id = fld_CorrCtaCteBlq_Id;
	}

	public int getFld_CodBanco() {
		return this.fld_CodBanco;
	}

	public void setFld_CodBanco(int fld_CodBanco) {
		this.fld_CodBanco = fld_CodBanco;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_NroCtaCte() {
		return this.fld_NroCtaCte;
	}

	public void setFld_NroCtaCte(String fld_NroCtaCte) {
		this.fld_NroCtaCte = fld_NroCtaCte;
	}

	public String getFld_NroCtaCteReal() {
		return this.fld_NroCtaCteReal;
	}

	public void setFld_NroCtaCteReal(String fld_NroCtaCteReal) {
		this.fld_NroCtaCteReal = fld_NroCtaCteReal;
	}

	public List<Tb_CeIdRelRutCtaBloqueada> getTbCeIdRelRutCtaBloqueadas() {
		return this.tbCeIdRelRutCtaBloqueadas;
	}

	public void setTbCeIdRelRutCtaBloqueadas(List<Tb_CeIdRelRutCtaBloqueada> tbCeIdRelRutCtaBloqueadas) {
		this.tbCeIdRelRutCtaBloqueadas = tbCeIdRelRutCtaBloqueadas;
	}

	public Tb_CeIdRelRutCtaBloqueada addTbCeIdRelRutCtaBloqueada(Tb_CeIdRelRutCtaBloqueada tbCeIdRelRutCtaBloqueada) {
		getTbCeIdRelRutCtaBloqueadas().add(tbCeIdRelRutCtaBloqueada);
		tbCeIdRelRutCtaBloqueada.setTbCeIdCtaCteBloqueada(this);

		return tbCeIdRelRutCtaBloqueada;
	}

	public Tb_CeIdRelRutCtaBloqueada removeTbCeIdRelRutCtaBloqueada(Tb_CeIdRelRutCtaBloqueada tbCeIdRelRutCtaBloqueada) {
		getTbCeIdRelRutCtaBloqueadas().remove(tbCeIdRelRutCtaBloqueada);
		tbCeIdRelRutCtaBloqueada.setTbCeIdCtaCteBloqueada(null);

		return tbCeIdRelRutCtaBloqueada;
	}

	public List<Tb_CeIdSegtoBloqueoCtaCte> getTbCeIdSegtoBloqueoCtaCtes() {
		return this.tbCeIdSegtoBloqueoCtaCtes;
	}

	public void setTbCeIdSegtoBloqueoCtaCtes(List<Tb_CeIdSegtoBloqueoCtaCte> tbCeIdSegtoBloqueoCtaCtes) {
		this.tbCeIdSegtoBloqueoCtaCtes = tbCeIdSegtoBloqueoCtaCtes;
	}

	public Tb_CeIdSegtoBloqueoCtaCte addTbCeIdSegtoBloqueoCtaCte(Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte) {
		getTbCeIdSegtoBloqueoCtaCtes().add(tbCeIdSegtoBloqueoCtaCte);
		tbCeIdSegtoBloqueoCtaCte.setTbCeIdCtaCteBloqueada(this);

		return tbCeIdSegtoBloqueoCtaCte;
	}

	public Tb_CeIdSegtoBloqueoCtaCte removeTbCeIdSegtoBloqueoCtaCte(Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte) {
		getTbCeIdSegtoBloqueoCtaCtes().remove(tbCeIdSegtoBloqueoCtaCte);
		tbCeIdSegtoBloqueoCtaCte.setTbCeIdCtaCteBloqueada(null);

		return tbCeIdSegtoBloqueoCtaCte;
	}

}