package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Fuentes database table.
 * 
 */
@Entity
@Table(name="Tbl_Fuentes")
@NamedQuery(name="Tbl_Fuente.findAll", query="SELECT t FROM Tbl_Fuente t")
public class Tbl_Fuente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Fuente")
	private byte fld_Fuente;

	@Column(name="Fld_GlosaFuente")
	private String fld_GlosaFuente;

	@Column(name="Fld_Prioridad")
	private byte fld_Prioridad;

	public Tbl_Fuente() {
	}

	public byte getFld_Fuente() {
		return this.fld_Fuente;
	}

	public void setFld_Fuente(byte fld_Fuente) {
		this.fld_Fuente = fld_Fuente;
	}

	public String getFld_GlosaFuente() {
		return this.fld_GlosaFuente;
	}

	public void setFld_GlosaFuente(String fld_GlosaFuente) {
		this.fld_GlosaFuente = fld_GlosaFuente;
	}

	public byte getFld_Prioridad() {
		return this.fld_Prioridad;
	}

	public void setFld_Prioridad(byte fld_Prioridad) {
		this.fld_Prioridad = fld_Prioridad;
	}

}