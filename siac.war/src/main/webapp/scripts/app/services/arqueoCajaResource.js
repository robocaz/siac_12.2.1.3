angular.module('ccs').factory('ArqueoCajaResource', function ($resource) {
	var resource = $resource('rest/arqueocaja/', { nocache : new Date().getTime()},
	{
		'tipopago'  : { method:'POST', isArray:false, url:'rest/arqueocaja/tipopago'  },
		'condcto'   : { method:'POST', isArray:false, url:'rest/arqueocaja/condcto'   },
		'pdf'       : { method:'POST', isArray:false, url:'rest/arqueocaja/pdf'       }
	});
	return resource;
});
