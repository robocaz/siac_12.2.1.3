angular.module('ccs').controller('ModalCambiarContrasenaController', function($scope, $rootScope, LoginResource){

  var ctrl = this;
  $scope.form = {};

  $scope.aceptar = function(){
    if ( $scope.form.contrasena === '' ) {
      swal({ title: 'Error', text: 'Contraseña Actual es Obligatoria!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#frm_contrasena_actual").focus();	});
      return;
    }
    if ( $scope.form.contrasena_nueva === '' ) {
      swal({ title: 'Error', text: 'Nueva Contraseña es Obligatoria!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#frm_nueva_contrasena").focus();	});
      return;
    }
    if ( $scope.form.contrasena_nueva.lenght < 6 || $scope.form.contrasena_nueva.lenght > 11 ) {
      swal({ title: 'Error', text: 'Nueva Contraseña debe tener mínimo 6 y máximo 11 caracteres!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#frm_nueva_contrasena").focus();	});
      return;
    }
    var regexPasswd = /^[A-Za-z0-9\-'ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ.&,\(\)]{6,30}$/;
    if ( !regexPasswd.test($scope.form.contrasena_nueva) ) {
      swal({ title: 'Error', text: 'Nueva Contraseña es inválida! Campo alfanumérico, permite caracteres especiales. No permite espacios', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#frm_nueva_contrasena").focus();	});
      return;
    }
    if ( $scope.form.contrasena_nueva !== $scope.form.contrasena_nueva_confirmar ) {
      swal({ title: 'Error', text: 'Existe diferencia entre nueva contraseña y su confirmación!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#frm_nueva_contrasena").focus();	});
      return;
    }

    var login = {username: $rootScope.usuario.username, password: $scope.form.contrasena};
    var successCallback = function(data) {
      console.log('CambiarClave - login ('+login.username+')');
      console.log(data);
      if( data.username !== login.username ){
        swal({ title: 'Error', text: 'Contraseña actual no es válida!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
          function () { angular.element("#frm_contrasena_actual").focus();	});
        return;
      }

      // cambiar contraseña
      var cambiarClave = {username: $rootScope.usuario.username, password: $scope.form.contrasena, passwordAux: $scope.form.contrasena_nueva};
      var successCallbackCambiarClave = function(data) {
        if ( data.idControl === 0 ) {
          swal({ title: 'Éxito', text: 'Contraseña actualizada exitosamente!', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
            function () {
              angular.element('#cambiarcontrasena').modal("hide");
            });
          $scope.form = {};
          return;
        }else{
          swal({ title: 'Error', text: data.msgControl, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
            function () { angular.element("#frm_contrasena_actual").focus();	});
          return;
        }
      }
      LoginResource.cambiarClave(cambiarClave, successCallbackCambiarClave, $scope.errorCallback);
    };
    LoginResource.login(login, successCallback, $scope.errorCallback);

    /*swal({
    text: '¿Está seguro que desea actualizar su contraseña?',
    type: 'warning',
    showCloseButton: false,
    confirmButtonText: 'Aceptar'
  }).then(function () {

    // $rootScope.$emit("LimpiarPersona", {});
    // angular.element('#detectorFraude').modal("hide");
  });*/
  }

  $scope.cerrar = function () {
    $scope.form = {};
    ctrl.formulario.$setPristine();
    ctrl.formulario.$setUntouched();
    angular.element('#cambiarcontrasena').modal("hide");
  }

  $scope.init = function () {
    angular.element("#cambiarcontrasena").on('shown.bs.modal', function () {
      angular.element("#frm_contrasena_actual").focus();
    });
  }

  $scope.validaFormato = function(pass, tipo){
	  
		var value = "";  
		if(pass !== ""){
			var regex = /^(?=(?:.*\d){1})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){2})\S{8,}$/;
		  	if( !regex.test(pass) ) {
				this.value = null;
		        swal({ title: 'Error', text: 'La contraseña no cumple con el formato requerido. Se requiere al menos un número y una letra mayúscula y ,minúscula, además el largo es entre 8 y 30 caracteres ', type: 'error', showCloseButton: true, confirmButtonText: 'Cerrar'}).then(
	              function () {
	      			if(tipo === "nueva1"){
	    				$scope.form.contrasena_nueva = "";
	    				$("#frm_nueva_contrasena").focus();
	    				
	    			}
	    			if(tipo === "nueva2"){
	    				$scope.form.contrasena_nueva_confirmar = "";
	    				$("#frm_confirmar_nueva_contrasena").focus();
	    			}
	           });
				
			}
		}
	  	
  }
  
  
  $scope.init();

});
