package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the t1 database table.
 * 
 */
@Entity
@Table(name="t1")
@NamedQuery(name="T1.findAll", query="SELECT t FROM T1 t")
public class T1 implements Serializable {
	private static final long serialVersionUID = 1L;

	private String texto;

	public T1() {
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}