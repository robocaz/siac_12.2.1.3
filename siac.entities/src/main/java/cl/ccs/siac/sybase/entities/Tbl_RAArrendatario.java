package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RAArrendatario database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RAArrendatario.findAll", query="SELECT t FROM Tbl_RAArrendatario t")
public class Tbl_RAArrendatario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrArrendatario_Id")
	private BigDecimal fld_CorrArrendatario_Id;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrSolicArriendo")
	private BigDecimal fld_CorrSolicArriendo;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoArrendatario")
	private String fld_TipoArrendatario;

	public Tbl_RAArrendatario() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrArrendatario_Id() {
		return this.fld_CorrArrendatario_Id;
	}

	public void setFld_CorrArrendatario_Id(BigDecimal fld_CorrArrendatario_Id) {
		this.fld_CorrArrendatario_Id = fld_CorrArrendatario_Id;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrSolicArriendo() {
		return this.fld_CorrSolicArriendo;
	}

	public void setFld_CorrSolicArriendo(BigDecimal fld_CorrSolicArriendo) {
		this.fld_CorrSolicArriendo = fld_CorrSolicArriendo;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_TipoArrendatario() {
		return this.fld_TipoArrendatario;
	}

	public void setFld_TipoArrendatario(String fld_TipoArrendatario) {
		this.fld_TipoArrendatario = fld_TipoArrendatario;
	}

}