package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlAclDiarias database table.
 * 
 */
@Entity
@Table(name="Tbl_ControlAclDiarias")
@NamedQuery(name="Tbl_ControlAclDiaria.findAll", query="SELECT t FROM Tbl_ControlAclDiaria t")
public class Tbl_ControlAclDiaria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrDoc")
	private BigDecimal fld_CorrDoc;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	public Tbl_ControlAclDiaria() {
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrDoc() {
		return this.fld_CorrDoc;
	}

	public void setFld_CorrDoc(BigDecimal fld_CorrDoc) {
		this.fld_CorrDoc = fld_CorrDoc;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

}