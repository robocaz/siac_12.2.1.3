package cl.exe.ccs.dto;

import java.io.Serializable;

public class ArqueoCajaInDTO implements Serializable {

	private static final long serialVersionUID = -1942858006149055727L;

	private String fechaInicio;
	private String codUsuario;
	private Integer codCC;

	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public Integer getCodCC() {
		return codCC;
	}
	public void setCodCC(Integer codCC) {
		this.codCC = codCC;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaInDTO [fechaInicio=");
		builder.append(fechaInicio);
		builder.append(", codUsuario=");
		builder.append(codUsuario);
		builder.append(", codCC=");
		builder.append(codCC);
		builder.append("]");
		return builder.toString();
	}

}
