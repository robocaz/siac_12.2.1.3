package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_SISTMOTIVO database table.
 * 
 */
@Entity
@Table(name="TBL_SISTMOTIVO")
@NamedQuery(name="TblSistmotivo.findAll", query="SELECT t FROM TblSistmotivo t")
public class TblSistmotivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODMOTIVO")
	private BigDecimal fldCodmotivo;

	@Column(name="FLD_CODSISTEMA")
	private BigDecimal fldCodsistema;

	public TblSistmotivo() {
	}

	public BigDecimal getFldCodmotivo() {
		return this.fldCodmotivo;
	}

	public void setFldCodmotivo(BigDecimal fldCodmotivo) {
		this.fldCodmotivo = fldCodmotivo;
	}

	public BigDecimal getFldCodsistema() {
		return this.fldCodsistema;
	}

	public void setFldCodsistema(BigDecimal fldCodsistema) {
		this.fldCodsistema = fldCodsistema;
	}

}