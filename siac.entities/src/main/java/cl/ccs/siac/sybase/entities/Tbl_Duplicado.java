package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Duplicados database table.
 * 
 */
@Entity
@Table(name="Tbl_Duplicados")
@NamedQuery(name="Tbl_Duplicado.findAll", query="SELECT t FROM Tbl_Duplicado t")
public class Tbl_Duplicado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvioAcl")
	private BigDecimal fld_CorrEnvioAcl;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	public Tbl_Duplicado() {
	}

	public BigDecimal getFld_CorrEnvioAcl() {
		return this.fld_CorrEnvioAcl;
	}

	public void setFld_CorrEnvioAcl(BigDecimal fld_CorrEnvioAcl) {
		this.fld_CorrEnvioAcl = fld_CorrEnvioAcl;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

}