package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EnviosProtPB database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosProtPB.findAll", query="SELECT t FROM Tbl_EnviosProtPB t")
public class Tbl_EnviosProtPB implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FecAsignacionEnvio")
	private Timestamp fld_FecAsignacionEnvio;

	@Column(name="Fld_FecElaboracionEnvio")
	private Timestamp fld_FecElaboracionEnvio;

	@Column(name="Fld_FecInicioCalculada")
	private Timestamp fld_FecInicioCalculada;

	@Column(name="Fld_FecInicioInformado")
	private Timestamp fld_FecInicioInformado;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_FecTerminoCalculada")
	private Timestamp fld_FecTerminoCalculada;

	@Column(name="Fld_FecTerminoInformado")
	private Timestamp fld_FecTerminoInformado;

	@Column(name="Fld_Firma_ApMat")
	private String fld_Firma_ApMat;

	@Column(name="Fld_Firma_ApPat")
	private String fld_Firma_ApPat;

	@Column(name="Fld_Firma_Nombres")
	private String fld_Firma_Nombres;

	@Column(name="Fld_Flag_EnvioCentralizado")
	private boolean fld_Flag_EnvioCentralizado;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_MarcaReasignacion")
	private boolean fld_MarcaReasignacion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroNomina")
	private double fld_NroNomina;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	@Column(name="Fld_TotMediosMagneticos")
	private byte fld_TotMediosMagneticos;

	@Column(name="Fld_TotRegistrosInformados")
	private int fld_TotRegistrosInformados;

	public Tbl_EnviosProtPB() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public Timestamp getFld_FecAsignacionEnvio() {
		return this.fld_FecAsignacionEnvio;
	}

	public void setFld_FecAsignacionEnvio(Timestamp fld_FecAsignacionEnvio) {
		this.fld_FecAsignacionEnvio = fld_FecAsignacionEnvio;
	}

	public Timestamp getFld_FecElaboracionEnvio() {
		return this.fld_FecElaboracionEnvio;
	}

	public void setFld_FecElaboracionEnvio(Timestamp fld_FecElaboracionEnvio) {
		this.fld_FecElaboracionEnvio = fld_FecElaboracionEnvio;
	}

	public Timestamp getFld_FecInicioCalculada() {
		return this.fld_FecInicioCalculada;
	}

	public void setFld_FecInicioCalculada(Timestamp fld_FecInicioCalculada) {
		this.fld_FecInicioCalculada = fld_FecInicioCalculada;
	}

	public Timestamp getFld_FecInicioInformado() {
		return this.fld_FecInicioInformado;
	}

	public void setFld_FecInicioInformado(Timestamp fld_FecInicioInformado) {
		this.fld_FecInicioInformado = fld_FecInicioInformado;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public Timestamp getFld_FecTerminoCalculada() {
		return this.fld_FecTerminoCalculada;
	}

	public void setFld_FecTerminoCalculada(Timestamp fld_FecTerminoCalculada) {
		this.fld_FecTerminoCalculada = fld_FecTerminoCalculada;
	}

	public Timestamp getFld_FecTerminoInformado() {
		return this.fld_FecTerminoInformado;
	}

	public void setFld_FecTerminoInformado(Timestamp fld_FecTerminoInformado) {
		this.fld_FecTerminoInformado = fld_FecTerminoInformado;
	}

	public String getFld_Firma_ApMat() {
		return this.fld_Firma_ApMat;
	}

	public void setFld_Firma_ApMat(String fld_Firma_ApMat) {
		this.fld_Firma_ApMat = fld_Firma_ApMat;
	}

	public String getFld_Firma_ApPat() {
		return this.fld_Firma_ApPat;
	}

	public void setFld_Firma_ApPat(String fld_Firma_ApPat) {
		this.fld_Firma_ApPat = fld_Firma_ApPat;
	}

	public String getFld_Firma_Nombres() {
		return this.fld_Firma_Nombres;
	}

	public void setFld_Firma_Nombres(String fld_Firma_Nombres) {
		this.fld_Firma_Nombres = fld_Firma_Nombres;
	}

	public boolean getFld_Flag_EnvioCentralizado() {
		return this.fld_Flag_EnvioCentralizado;
	}

	public void setFld_Flag_EnvioCentralizado(boolean fld_Flag_EnvioCentralizado) {
		this.fld_Flag_EnvioCentralizado = fld_Flag_EnvioCentralizado;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public boolean getFld_MarcaReasignacion() {
		return this.fld_MarcaReasignacion;
	}

	public void setFld_MarcaReasignacion(boolean fld_MarcaReasignacion) {
		this.fld_MarcaReasignacion = fld_MarcaReasignacion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public double getFld_NroNomina() {
		return this.fld_NroNomina;
	}

	public void setFld_NroNomina(double fld_NroNomina) {
		this.fld_NroNomina = fld_NroNomina;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

	public byte getFld_TotMediosMagneticos() {
		return this.fld_TotMediosMagneticos;
	}

	public void setFld_TotMediosMagneticos(byte fld_TotMediosMagneticos) {
		this.fld_TotMediosMagneticos = fld_TotMediosMagneticos;
	}

	public int getFld_TotRegistrosInformados() {
		return this.fld_TotRegistrosInformados;
	}

	public void setFld_TotRegistrosInformados(int fld_TotRegistrosInformados) {
		this.fld_TotRegistrosInformados = fld_TotRegistrosInformados;
	}

}