package cl.ccs.siac.combos;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.CombosDao;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ImpresoraDTO;
import cl.exe.ccs.dto.MonedaDTO;
import cl.exe.ccs.dto.ServicioDTO;
import cl.exe.ccs.dto.SucursalDTO;
import cl.exe.ccs.dto.TarifaDTO;

@Named("combosBusiness")
public class CombosBusinessImpl implements CombosBusiness{

	
	@Inject
	private CombosDao combosDao;
	
	public List<ComboDTO> obtenerRegiones(){
		List<ComboDTO> regionCombo = new ArrayList<ComboDTO>();
		regionCombo = combosDao.obtenerRegiones();
		return regionCombo;
	}
	
	public List<ComboDTO> obtenerComunas(Integer idRegion){
		List<ComboDTO> comunasCombo = new ArrayList<ComboDTO>();
		comunasCombo = combosDao.obtenerComunas(idRegion);
		return comunasCombo;
	}
	
	public List<ServicioDTO> obtenerServicios(){
		List<ServicioDTO> serviciosCombo = new ArrayList<ServicioDTO>();
		serviciosCombo = combosDao.obtenerServicios();
		return serviciosCombo;
	}
	
	public List<TarifaDTO> obtenerTarifas(){
		List<TarifaDTO> tarifasCombo = new ArrayList<TarifaDTO>();
		tarifasCombo = combosDao.obtenerTarifas();
		return tarifasCombo;
	}
	
	public List<ComboDTO> obtenerObservaciones(){
		return combosDao.obtenerObservaciones();
	}
	
		
	public List<ComboDTO> obtenerEmisores(String codigoTipoEmisor){
		List<ComboDTO> emisores = new ArrayList<ComboDTO>();
		emisores = combosDao.obtenerEmisores(codigoTipoEmisor);
		return emisores;
	}

	public List<SucursalDTO> obtenerSucursales(ComboDTO banco){
		List<SucursalDTO> sucursales = new ArrayList<SucursalDTO>();
		sucursales = combosDao.obtenerSucursales(banco);
		return sucursales;
	}
	
	public List<ImpresoraDTO> obtenerImpresoras(Integer centroCosto){
		List<ImpresoraDTO> impresoras = new ArrayList<ImpresoraDTO>();
		impresoras = combosDao.obtenerImpresoras(centroCosto);
		return impresoras;
		
	}
	
	public List<ComboDTO> obtenerTiposEmisores(){
		return combosDao.obtenerTiposEmisores();
	}
	
	public List<ComboDTO> obtenerTiposEmisoresPorDoc(String tipoDocumento){
		return combosDao.obtenerTiposEmisoresPorDoc(tipoDocumento);
	}
	
	public List<ComboDTO> obtenerDocsAclaracion(String tipoDoc){
		return combosDao.obtenerDocsAclaracion(tipoDoc);
	}
	
	
	public List<SucursalDTO> obtenerSucursalesEmisor(String tipoEmisor, String  codEmisor){
		return combosDao.obtenerSucursalesEmisor(tipoEmisor, codEmisor);
		
	}
	
	public List<ComboDTO> tiposDocumentos(){
		return combosDao.tiposDocumentos();
	}
	
	public List<MonedaDTO> obtenerMonedas(){
		return combosDao.obtenerMonedas();
	}
	
	public List<ComboDTO> obtenerMotivosDeclaracionUso(){
		return combosDao.obtenerMotivosDeclaracionUso();
	}
	
	public List<ComboDTO> cmbDocPresentado(){
		return combosDao.cmbDocPresentado();
	}
	
	public List<ComboDTO> cmbBancos(){
		return combosDao.cmbBancos();
	}
	
	public List<ComboDTO> cmbCtasCtes(String codEmisor){
		return combosDao.cmbCtasCtes(codEmisor);
	}
	
	@Override
	public List<ComboDTO> cmbTipoSolicRecl(){
		return combosDao.cmbTipoSolicRecl();
	}
	
	@Override
	public List<ComboDTO> cmbCategoriaSolic(Integer idSolic){
		return combosDao.cmbCategoriaSolic(idSolic);
	}

}
