package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class FacturaDTO extends ResponseDTO implements Serializable{
	
	private TramitanteDTO tramitante;
	private String tipoFacturacion;
	private Long numeroFactura;
	private String rutAfectado;
	private String rutFacturacion;
	private String razonSocial;
	private String giro;
	private String direccion;
	private Integer codRegion;
	private Integer codComuna;
	private String comunaDesc;
	private java.math.BigDecimal montoBruto;
	private String medioPago;
	private String ordenCompra;
	private String corrCaja;
	private Integer idCentroCosto;
	private Long fecha;
	private List<FacturaTransaccionDTO> transacciones;
	private List<FacturaDTO> facturas;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private java.math.BigDecimal total;
	private String usuario;
	
	public TramitanteDTO getTramitante() {
		return tramitante;
	}
	public void setTramitante(TramitanteDTO tramitante) {
		this.tramitante = tramitante;
	}
	public String getTipoFacturacion() {
		return tipoFacturacion;
	}
	public void setTipoFacturacion(String tipoFacturacion) {
		this.tipoFacturacion = tipoFacturacion;
	}
	public Long getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(Long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getRutFacturacion() {
		return rutFacturacion;
	}
	public void setRutFacturacion(String rutFacturacion) {
		this.rutFacturacion = rutFacturacion;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getCodRegion() {
		return codRegion;
	}
	public void setCodRegion(Integer codRegion) {
		this.codRegion = codRegion;
	}
	public Integer getCodComuna() {
		return codComuna;
	}
	public void setCodComuna(Integer codComuna) {
		this.codComuna = codComuna;
	}
	public String getComunaDesc() {
		return comunaDesc;
	}
	public void setComunaDesc(String comunaDesc) {
		this.comunaDesc = comunaDesc;
	}
	public java.math.BigDecimal getMontoBruto() {
		return montoBruto;
	}
	public void setMontoBruto(java.math.BigDecimal montoBruto) {
		this.montoBruto = montoBruto;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	public String getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public String getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(String corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getIdCentroCosto() {
		return idCentroCosto;
	}
	public void setIdCentroCosto(Integer idCentroCosto) {
		this.idCentroCosto = idCentroCosto;
	}
	public Long getFecha() {
		return fecha;
	}
	public void setFecha(Long fecha) {
		this.fecha = fecha;
	}
	public List<FacturaTransaccionDTO> getTransacciones() {
		return transacciones;
	}
	public void setTransacciones(List<FacturaTransaccionDTO> transacciones) {
		this.transacciones = transacciones;
	}
	public List<FacturaDTO> getFacturas() {
		return facturas;
	}
	public void setFacturas(List<FacturaDTO> facturas) {
		this.facturas = facturas;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public java.math.BigDecimal getTotal() {
		return total;
	}
	public void setTotal(java.math.BigDecimal total) {
		this.total = total;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
