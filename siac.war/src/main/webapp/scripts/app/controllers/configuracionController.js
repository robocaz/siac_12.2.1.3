angular.module('ccs').controller('ConfiguracionController', function($rootScope, $scope, LoginResource, InicioResource, $location) {

	$scope.configuracion = {};
	
	
	
	$scope.aceptarConfiguracion = function(){
		console.log($scope.configuracion);
		
		var successCallback = function(data,responseHeaders){
			console.log(data);
		};
		
		InicioResource.configInicial($scope.configuracion, successCallback, $scope.errorCallback);
		
	}
	
	
	
	$rootScope.$on('iniciarConf', function(event, data) { 
		$scope.configuracion = data;
		angular.element("#configuraciones").modal();
		console.log(data);
	});
});
