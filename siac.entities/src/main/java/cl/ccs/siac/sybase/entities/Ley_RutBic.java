package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ley_RutBic database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_RutBic.findAll", query="SELECT l FROM Ley_RutBic l")
public class Ley_RutBic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Rut")
	private String rut;

	public Ley_RutBic() {
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}