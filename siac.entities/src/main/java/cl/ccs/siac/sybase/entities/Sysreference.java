package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysreferences database table.
 * 
 */
@Entity
@Table(name="sysreferences")
@NamedQuery(name="Sysreference.findAll", query="SELECT s FROM Sysreference s")
public class Sysreference implements Serializable {
	private static final long serialVersionUID = 1L;

	private int constrid;

	private short fokey1;

	private short fokey10;

	private short fokey11;

	private short fokey12;

	private short fokey13;

	private short fokey14;

	private short fokey15;

	private short fokey16;

	private short fokey2;

	private short fokey3;

	private short fokey4;

	private short fokey5;

	private short fokey6;

	private short fokey7;

	private short fokey8;

	private short fokey9;

	private short frgndbid;

	private String frgndbname;

	private short indexid;

	private short keycnt;

	private short pmrydbid;

	private String pmrydbname;

	private short refkey1;

	private short refkey10;

	private short refkey11;

	private short refkey12;

	private short refkey13;

	private short refkey14;

	private short refkey15;

	private short refkey16;

	private short refkey2;

	private short refkey3;

	private short refkey4;

	private short refkey5;

	private short refkey6;

	private short refkey7;

	private short refkey8;

	private short refkey9;

	private int reftabid;

	private int spare2;

	private short status;

	private int tableid;

	public Sysreference() {
	}

	public int getConstrid() {
		return this.constrid;
	}

	public void setConstrid(int constrid) {
		this.constrid = constrid;
	}

	public short getFokey1() {
		return this.fokey1;
	}

	public void setFokey1(short fokey1) {
		this.fokey1 = fokey1;
	}

	public short getFokey10() {
		return this.fokey10;
	}

	public void setFokey10(short fokey10) {
		this.fokey10 = fokey10;
	}

	public short getFokey11() {
		return this.fokey11;
	}

	public void setFokey11(short fokey11) {
		this.fokey11 = fokey11;
	}

	public short getFokey12() {
		return this.fokey12;
	}

	public void setFokey12(short fokey12) {
		this.fokey12 = fokey12;
	}

	public short getFokey13() {
		return this.fokey13;
	}

	public void setFokey13(short fokey13) {
		this.fokey13 = fokey13;
	}

	public short getFokey14() {
		return this.fokey14;
	}

	public void setFokey14(short fokey14) {
		this.fokey14 = fokey14;
	}

	public short getFokey15() {
		return this.fokey15;
	}

	public void setFokey15(short fokey15) {
		this.fokey15 = fokey15;
	}

	public short getFokey16() {
		return this.fokey16;
	}

	public void setFokey16(short fokey16) {
		this.fokey16 = fokey16;
	}

	public short getFokey2() {
		return this.fokey2;
	}

	public void setFokey2(short fokey2) {
		this.fokey2 = fokey2;
	}

	public short getFokey3() {
		return this.fokey3;
	}

	public void setFokey3(short fokey3) {
		this.fokey3 = fokey3;
	}

	public short getFokey4() {
		return this.fokey4;
	}

	public void setFokey4(short fokey4) {
		this.fokey4 = fokey4;
	}

	public short getFokey5() {
		return this.fokey5;
	}

	public void setFokey5(short fokey5) {
		this.fokey5 = fokey5;
	}

	public short getFokey6() {
		return this.fokey6;
	}

	public void setFokey6(short fokey6) {
		this.fokey6 = fokey6;
	}

	public short getFokey7() {
		return this.fokey7;
	}

	public void setFokey7(short fokey7) {
		this.fokey7 = fokey7;
	}

	public short getFokey8() {
		return this.fokey8;
	}

	public void setFokey8(short fokey8) {
		this.fokey8 = fokey8;
	}

	public short getFokey9() {
		return this.fokey9;
	}

	public void setFokey9(short fokey9) {
		this.fokey9 = fokey9;
	}

	public short getFrgndbid() {
		return this.frgndbid;
	}

	public void setFrgndbid(short frgndbid) {
		this.frgndbid = frgndbid;
	}

	public String getFrgndbname() {
		return this.frgndbname;
	}

	public void setFrgndbname(String frgndbname) {
		this.frgndbname = frgndbname;
	}

	public short getIndexid() {
		return this.indexid;
	}

	public void setIndexid(short indexid) {
		this.indexid = indexid;
	}

	public short getKeycnt() {
		return this.keycnt;
	}

	public void setKeycnt(short keycnt) {
		this.keycnt = keycnt;
	}

	public short getPmrydbid() {
		return this.pmrydbid;
	}

	public void setPmrydbid(short pmrydbid) {
		this.pmrydbid = pmrydbid;
	}

	public String getPmrydbname() {
		return this.pmrydbname;
	}

	public void setPmrydbname(String pmrydbname) {
		this.pmrydbname = pmrydbname;
	}

	public short getRefkey1() {
		return this.refkey1;
	}

	public void setRefkey1(short refkey1) {
		this.refkey1 = refkey1;
	}

	public short getRefkey10() {
		return this.refkey10;
	}

	public void setRefkey10(short refkey10) {
		this.refkey10 = refkey10;
	}

	public short getRefkey11() {
		return this.refkey11;
	}

	public void setRefkey11(short refkey11) {
		this.refkey11 = refkey11;
	}

	public short getRefkey12() {
		return this.refkey12;
	}

	public void setRefkey12(short refkey12) {
		this.refkey12 = refkey12;
	}

	public short getRefkey13() {
		return this.refkey13;
	}

	public void setRefkey13(short refkey13) {
		this.refkey13 = refkey13;
	}

	public short getRefkey14() {
		return this.refkey14;
	}

	public void setRefkey14(short refkey14) {
		this.refkey14 = refkey14;
	}

	public short getRefkey15() {
		return this.refkey15;
	}

	public void setRefkey15(short refkey15) {
		this.refkey15 = refkey15;
	}

	public short getRefkey16() {
		return this.refkey16;
	}

	public void setRefkey16(short refkey16) {
		this.refkey16 = refkey16;
	}

	public short getRefkey2() {
		return this.refkey2;
	}

	public void setRefkey2(short refkey2) {
		this.refkey2 = refkey2;
	}

	public short getRefkey3() {
		return this.refkey3;
	}

	public void setRefkey3(short refkey3) {
		this.refkey3 = refkey3;
	}

	public short getRefkey4() {
		return this.refkey4;
	}

	public void setRefkey4(short refkey4) {
		this.refkey4 = refkey4;
	}

	public short getRefkey5() {
		return this.refkey5;
	}

	public void setRefkey5(short refkey5) {
		this.refkey5 = refkey5;
	}

	public short getRefkey6() {
		return this.refkey6;
	}

	public void setRefkey6(short refkey6) {
		this.refkey6 = refkey6;
	}

	public short getRefkey7() {
		return this.refkey7;
	}

	public void setRefkey7(short refkey7) {
		this.refkey7 = refkey7;
	}

	public short getRefkey8() {
		return this.refkey8;
	}

	public void setRefkey8(short refkey8) {
		this.refkey8 = refkey8;
	}

	public short getRefkey9() {
		return this.refkey9;
	}

	public void setRefkey9(short refkey9) {
		this.refkey9 = refkey9;
	}

	public int getReftabid() {
		return this.reftabid;
	}

	public void setReftabid(int reftabid) {
		this.reftabid = reftabid;
	}

	public int getSpare2() {
		return this.spare2;
	}

	public void setSpare2(int spare2) {
		this.spare2 = spare2;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public int getTableid() {
		return this.tableid;
	}

	public void setTableid(int tableid) {
		this.tableid = tableid;
	}

}