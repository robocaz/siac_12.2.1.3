package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the syslogs database table.
 * 
 */
@Entity
@Table(name="syslogs")
@NamedQuery(name="Syslog.findAll", query="SELECT s FROM Syslog s")
public class Syslog implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte op;

	private byte[] xactid;

	public Syslog() {
	}

	public byte getOp() {
		return this.op;
	}

	public void setOp(byte op) {
		this.op = op;
	}

	public byte[] getXactid() {
		return this.xactid;
	}

	public void setXactid(byte[] xactid) {
		this.xactid = xactid;
	}

}