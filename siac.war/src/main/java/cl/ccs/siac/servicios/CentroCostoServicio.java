package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.caja.CajaBusiness;
import cl.ccs.siac.centrocosto.CentroCostoBusiness;
import cl.ccs.siac.usuario.UsuarioBusiness;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/centrocosto")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class CentroCostoServicio {

	private static final Logger log = LogManager.getLogger(CentroCostoServicio.class);

	@Inject
	private CentroCostoBusiness centroCostoBusiness;

	@Inject
	private CajaBusiness cajaBusiness;

	@Inject
	private UsuarioBusiness usuarioBusiness;

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar(@Context HttpServletRequest request, ComboDTO centroCosto) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: modificar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");

			ResponseDTO respuesta = centroCostoBusiness.modificar(userPrincipal.getName(), centroCosto.getCodigo());

			if (respuesta.getIdControl() == 0) {
				if (usuarioDTO.getCaja() != null && usuarioDTO.getCaja() != "0") {
					cajaBusiness.cerrarCajaTemporal(usuarioDTO.getCaja(), userPrincipal.getName());
				}

				usuarioDTO = usuarioBusiness.obtenerUsuario(usuarioDTO.getUsername());

				request.getSession().setAttribute("usuario", usuarioDTO);

				log.info("Metodo modificar Centro de Costo, Usuario  " + userPrincipal.getName() + "  - Message: " 	+ respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
				return Response.ok(respuesta).build();
			} else {

				log.warn("Modificar Centro de Costo, Usuario  " + userPrincipal.getName() + " - Message: " + respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (Exception ex) {
			log.error("Listar Centro de Costo, Usuario  " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	public Response listar(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: listar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			ResponseListDTO<ComboDTO> respuesta = centroCostoBusiness.listar(userPrincipal.getName());

			List<ComboDTO> list = respuesta.getLista();
			if (respuesta.getIdControl() == 0) {
				log.info("Listar Centro de Costo, Usuario " + request.getUserPrincipal() + "  - Message: "+ respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
				return Response.ok(list).build();
			} else {
				log.error("Listar Centro de Costo, Usuario " + request.getUserPrincipal() + "  - Message: " + respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}
		} catch (Exception ex) {
			log.error("Listar Centro de Costo, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
