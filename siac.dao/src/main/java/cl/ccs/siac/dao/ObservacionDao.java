package cl.ccs.siac.dao;

import cl.exe.ccs.dto.ObservacionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface ObservacionDao {
	public ResponseListDTO<ObservacionDTO> listar(String rutTramitante);
	public ResponseDTO crear(ObservacionDTO observacionDTO);
}	
 