package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_BCEstadoPublicacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCEstadoPublicacion.findAll", query="SELECT t FROM Tbl_BCEstadoPublicacion t")
public class Tbl_BCEstadoPublicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_GlosaEstadoPubl")
	private String fld_GlosaEstadoPubl;

	public Tbl_BCEstadoPublicacion() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_GlosaEstadoPubl() {
		return this.fld_GlosaEstadoPubl;
	}

	public void setFld_GlosaEstadoPubl(String fld_GlosaEstadoPubl) {
		this.fld_GlosaEstadoPubl = fld_GlosaEstadoPubl;
	}

}