package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Productos database table.
 * 
 */
@Entity
@Table(name="Tbl_Productos")
@NamedQuery(name="Tbl_Producto.findAll", query="SELECT t FROM Tbl_Producto t")
public class Tbl_Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_GlosaProducto")
	private String fld_GlosaProducto;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorBase")
	private BigDecimal fld_ValorBase;

	public Tbl_Producto() {
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public String getFld_GlosaProducto() {
		return this.fld_GlosaProducto;
	}

	public void setFld_GlosaProducto(String fld_GlosaProducto) {
		this.fld_GlosaProducto = fld_GlosaProducto;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorBase() {
		return this.fld_ValorBase;
	}

	public void setFld_ValorBase(BigDecimal fld_ValorBase) {
		this.fld_ValorBase = fld_ValorBase;
	}

}