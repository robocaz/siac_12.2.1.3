package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ProtBeneficioCesantia database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ProtBeneficioCesantia.findAll", query="SELECT t FROM Tbl_ProtBeneficioCesantia t")
public class Tbl_ProtBeneficioCesantia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrEnvioNvo")
	private BigDecimal fld_CorrEnvioNvo;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletinNvo")
	private short fld_NroBoletinNvo;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_ProtBeneficioCesantia() {
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrEnvioNvo() {
		return this.fld_CorrEnvioNvo;
	}

	public void setFld_CorrEnvioNvo(BigDecimal fld_CorrEnvioNvo) {
		this.fld_CorrEnvioNvo = fld_CorrEnvioNvo;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletinNvo() {
		return this.fld_NroBoletinNvo;
	}

	public void setFld_NroBoletinNvo(short fld_NroBoletinNvo) {
		this.fld_NroBoletinNvo = fld_NroBoletinNvo;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}