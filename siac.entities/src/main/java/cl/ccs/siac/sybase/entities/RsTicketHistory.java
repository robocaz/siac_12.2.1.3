package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the rs_ticket_history database table.
 * 
 */
@Entity
@Table(name="rs_ticket_history")
@NamedQuery(name="RsTicketHistory.findAll", query="SELECT r FROM RsTicketHistory r")
public class RsTicketHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal cnt;

	@Column(name="conn_id")
	private int connId;

	@Column(name="dist_t")
	private Timestamp distT;

	@Column(name="dsi_cmd")
	private BigDecimal dsiCmd;

	@Column(name="dsi_t")
	private Timestamp dsiT;

	@Column(name="dsi_tnx")
	private BigDecimal dsiTnx;

	@Column(name="exec_b")
	private BigDecimal execB;

	@Column(name="exec_t")
	private Timestamp execT;

	private String h1;

	private String h2;

	private String h3;

	private String h4;

	private String pdb;

	@Column(name="pdb_t")
	private Timestamp pdbT;

	private String prs;

	private String rdb;

	@Column(name="rdb_t")
	private Timestamp rdbT;

	private String rrs;

	@Column(name="rsi_b")
	private BigDecimal rsiB;

	@Column(name="rsi_t")
	private Timestamp rsiT;

	private String ticket;

	public RsTicketHistory() {
	}

	public BigDecimal getCnt() {
		return this.cnt;
	}

	public void setCnt(BigDecimal cnt) {
		this.cnt = cnt;
	}

	public int getConnId() {
		return this.connId;
	}

	public void setConnId(int connId) {
		this.connId = connId;
	}

	public Timestamp getDistT() {
		return this.distT;
	}

	public void setDistT(Timestamp distT) {
		this.distT = distT;
	}

	public BigDecimal getDsiCmd() {
		return this.dsiCmd;
	}

	public void setDsiCmd(BigDecimal dsiCmd) {
		this.dsiCmd = dsiCmd;
	}

	public Timestamp getDsiT() {
		return this.dsiT;
	}

	public void setDsiT(Timestamp dsiT) {
		this.dsiT = dsiT;
	}

	public BigDecimal getDsiTnx() {
		return this.dsiTnx;
	}

	public void setDsiTnx(BigDecimal dsiTnx) {
		this.dsiTnx = dsiTnx;
	}

	public BigDecimal getExecB() {
		return this.execB;
	}

	public void setExecB(BigDecimal execB) {
		this.execB = execB;
	}

	public Timestamp getExecT() {
		return this.execT;
	}

	public void setExecT(Timestamp execT) {
		this.execT = execT;
	}

	public String getH1() {
		return this.h1;
	}

	public void setH1(String h1) {
		this.h1 = h1;
	}

	public String getH2() {
		return this.h2;
	}

	public void setH2(String h2) {
		this.h2 = h2;
	}

	public String getH3() {
		return this.h3;
	}

	public void setH3(String h3) {
		this.h3 = h3;
	}

	public String getH4() {
		return this.h4;
	}

	public void setH4(String h4) {
		this.h4 = h4;
	}

	public String getPdb() {
		return this.pdb;
	}

	public void setPdb(String pdb) {
		this.pdb = pdb;
	}

	public Timestamp getPdbT() {
		return this.pdbT;
	}

	public void setPdbT(Timestamp pdbT) {
		this.pdbT = pdbT;
	}

	public String getPrs() {
		return this.prs;
	}

	public void setPrs(String prs) {
		this.prs = prs;
	}

	public String getRdb() {
		return this.rdb;
	}

	public void setRdb(String rdb) {
		this.rdb = rdb;
	}

	public Timestamp getRdbT() {
		return this.rdbT;
	}

	public void setRdbT(Timestamp rdbT) {
		this.rdbT = rdbT;
	}

	public String getRrs() {
		return this.rrs;
	}

	public void setRrs(String rrs) {
		this.rrs = rrs;
	}

	public BigDecimal getRsiB() {
		return this.rsiB;
	}

	public void setRsiB(BigDecimal rsiB) {
		this.rsiB = rsiB;
	}

	public Timestamp getRsiT() {
		return this.rsiT;
	}

	public void setRsiT(Timestamp rsiT) {
		this.rsiT = rsiT;
	}

	public String getTicket() {
		return this.ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}