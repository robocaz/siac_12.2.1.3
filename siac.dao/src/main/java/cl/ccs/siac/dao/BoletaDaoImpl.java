package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.BoletaDTO;
import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ControlBoletaDTO;
import cl.exe.ccs.dto.FacturaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("boletaDao")
@Stateless
public class BoletaDaoImpl implements BoletaDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private Logger log = LogManager.getLogger(BoletaDaoImpl.class);

	public BoletaDTO obtenerBoleta(BoletaDTO boletaDTO) {
		try {
			
			log.info("Ejecutando SQ_BoletaDeCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_BoletaDeCaja_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", java.math.BigDecimal.class,ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Telefono", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoBruto", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoIva", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoNeto", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.OUT);

			log.info("Fld_CorrCaja : " + boletaDTO.getCorrCaja());
			log.info("Fld_NroBoletaFactura : " + boletaDTO.getNumeroBoleta());
			
			storedProcedure.setParameter("Fld_CorrCaja", java.math.BigDecimal.valueOf(boletaDTO.getCorrCaja()));
			storedProcedure.setParameter("Fld_NroBoletaFactura",Integer.parseInt(boletaDTO.getNumeroBoleta().toString()));

			storedProcedure.execute();

			List<FacturaTransaccionDTO> transacciones = new ArrayList<FacturaTransaccionDTO>();
			List<Object[]> listaSP = storedProcedure.getResultList();
			int index = 0;

			for (Object item : listaSP) {

				Object[] datos = (Object[]) item;
				FacturaTransaccionDTO transaccion = new FacturaTransaccionDTO();

				transaccion.setNroLinea(++index);
				transaccion.setCantidadServicio(Integer.parseInt(datos[0].toString()));
				transaccion.setTipoServicio(datos[1].toString());
				transaccion.setCostoServicio(BigDecimal.valueOf(Double.parseDouble(datos[2].toString())));
				transaccion.setCodServicio(datos[4].toString());
				transaccion.setCodExterno(datos[4].toString());

				transacciones.add(transaccion);
			}

			boletaDTO.setTransacciones(transacciones);

			boletaDTO.setCodCCosto(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodCCosto").toString()));
			boletaDTO.setFecEmision(storedProcedure.getOutputParameterValue("Fld_FecEmision").toString());
			boletaDTO.setRutAfectado(storedProcedure.getOutputParameterValue("Fld_RutAfectado").toString());
			boletaDTO.setCorrNombre(((Double) Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_CorrNombre").toString())).longValue());
			boletaDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApPat").toString());
			boletaDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApMat").toString());
			boletaDTO.setNombres(storedProcedure.getOutputParameterValue("Fld_Nombre_Nombres").toString());
			boletaDTO.setGiro(storedProcedure.getOutputParameterValue("Fld_Giro").toString());
			boletaDTO.setDireccion(storedProcedure.getOutputParameterValue("Fld_Direccion").toString());
			boletaDTO.setCodComuna(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodComuna").toString()));
			boletaDTO.setCodRegion(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodRegion").toString()));
			boletaDTO.setTelefono(storedProcedure.getOutputParameterValue("Fld_Telefono").toString());
			boletaDTO.setMontoBruto(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoBruto").toString())));
			boletaDTO.setMontoIva(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoIva").toString())));
			boletaDTO.setMontoNeto(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoNeto").toString())));
			boletaDTO.setTipoPago(storedProcedure.getOutputParameterValue("Fld_TipoPago").toString());

			boletaDTO.setIdControl(0);

			return boletaDTO;

		} catch (Exception e) {

			log.error("Error en el SP SQ_BoletaDeCaja_SW2 " + e.getMessage());
			boletaDTO.setIdControl(1);
			return boletaDTO;
		}
	}

	public BoletaDTO obtenerBoletaTransaccion(BoletaDTO boletaDTO) {
		try {
			
			log.info("Ejecutando SQ_TranFolio0ParaFac_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TranFolio0ParaFac_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecEmision", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Telefono", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoBruto", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoIva", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoNeto", java.math.BigDecimal.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			log.info("Fld_CorrTransaccion : " + boletaDTO.getCorrTransaccion());
			
			storedProcedure.setParameter("Fld_CorrTransaccion", boletaDTO.getCorrTransaccion());

			storedProcedure.execute();

			List<FacturaTransaccionDTO> transacciones = new ArrayList<FacturaTransaccionDTO>();
			List<Object[]> listaSP = storedProcedure.getResultList();
			int index = 0;

			for (Object item : listaSP) {

				Object[] datos = (Object[]) item;
				FacturaTransaccionDTO transaccion = new FacturaTransaccionDTO();

				transaccion.setNroLinea(++index);
				transaccion.setCantidadServicio(Integer.parseInt(datos[0].toString()));
				transaccion.setTipoServicio(datos[1].toString());
				transaccion.setCostoServicio(BigDecimal.valueOf(Double.parseDouble(datos[2].toString())));
				//transaccion.setCodServicio(datos[4].toString());
				transaccion.setCodExterno(datos[4].toString());

				transacciones.add(transaccion);
			}

			boletaDTO.setTransacciones(transacciones);

			boletaDTO.setCodCCosto(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodCCosto").toString()));
			boletaDTO.setFecEmision(storedProcedure.getOutputParameterValue("Fld_FecEmision").toString());
			boletaDTO.setRutAfectado(storedProcedure.getOutputParameterValue("Fld_RutAfectado").toString());
			boletaDTO.setCorrNombre(((Double) Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_CorrNombre").toString())).longValue());
			boletaDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApPat").toString());
			boletaDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApMat").toString());
			boletaDTO.setNombres(storedProcedure.getOutputParameterValue("Fld_Nombre_Nombres").toString());
			boletaDTO.setGiro(storedProcedure.getOutputParameterValue("Fld_Giro").toString());
			boletaDTO.setDireccion(storedProcedure.getOutputParameterValue("Fld_Direccion").toString());
			boletaDTO.setCodComuna(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodComuna").toString()));
			if(storedProcedure.getOutputParameterValue("Fld_CodRegion") != null){
				boletaDTO.setCodRegion(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodRegion").toString()));
			}
			else{
				boletaDTO.setCodRegion(0);
			}
			
			boletaDTO.setTelefono(storedProcedure.getOutputParameterValue("Fld_Telefono").toString());
			boletaDTO.setMontoBruto(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoBruto").toString())));
			boletaDTO.setMontoIva(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoIva").toString())));
			boletaDTO.setMontoNeto(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoNeto").toString())));
			boletaDTO.setTipoPago(storedProcedure.getOutputParameterValue("Fld_TipoPago").toString());

			boletaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			boletaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return boletaDTO;

		} catch (Exception e) {

			log.error("Error en el SP SQ_TranFolio0ParaFac_SW2 " + e.getMessage());
			boletaDTO.setIdControl(1);
			return boletaDTO;
		}
	}

	public ResponseListDTO<Integer> obtenerBoletasFolioCero(BoletaDTO boletaDTO) {

		ResponseListDTO<Integer> response = new ResponseListDTO<Integer>();
		try {
			
			log.info("Ejecutando SQ_LisTranFolio0_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_LisTranFolio0_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);

			log.info("SQ_LisTranFolio0_SW2 : " + boletaDTO.getCorrCaja());
			
			storedProcedure.setParameter("Fld_CorrCaja", boletaDTO.getCorrCaja());

			storedProcedure.execute();

			List<Integer> boletas = new ArrayList<Integer>();
			List<Object[]> listaSP = storedProcedure.getResultList();

			for (Object item : listaSP) {
				// Object[] datos = (Object[]) item;
				boletas.add(Integer.parseInt(item.toString()));
			}

			response.setLista(boletas);
			response.setIdControl(0);

			return response;

		} catch (Exception e) {

			log.error("Error en el SP SQ_LisTranFolio0_SW2 " + e.getMessage());
			response.setIdControl(1);
			return response;
		}
	}

	public ControlBoletaDTO consultaBoleta(ControlBoletaDTO boletaDTO) {

		try {

			log.info("Ejecutando SQ_RecuperaBoleta_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_RecuperaBoleta_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NombrePC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaDesde", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaHasta", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaActual", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);

			log.info("Fld_CodCCosto : " + boletaDTO.getIdCentroCosto());
			log.info("Fld_NombrePC : " + boletaDTO.getNombreMaquina());
			
			storedProcedure.setParameter("Fld_CodCCosto", boletaDTO.getIdCentroCosto());
			storedProcedure.setParameter("Fld_NombrePC", boletaDTO.getNombreMaquina());
			//
			storedProcedure.execute();
			if (storedProcedure.getOutputParameterValue("Fld_NroBoletaActual") != null) {
				boletaDTO.setActualBoleta(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletaActual").toString()));
			} else {
				boletaDTO.setActualBoleta(0);
			}
			if (storedProcedure.getOutputParameterValue("Fld_NroBoletaDesde") != null) {
				boletaDTO.setInicioBoleta(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletaDesde").toString()));
			} else {
				boletaDTO.setInicioBoleta(0);
			}
			if (storedProcedure.getOutputParameterValue("Fld_NroBoletaHasta") != null) {
				boletaDTO.setFinBoleta(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletaHasta").toString()));
			} else {
				boletaDTO.setFinBoleta(0);
			}

			log.info(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());
			boletaDTO.setIdControl(0);

			return boletaDTO;
		} catch (Exception e) {

			log.error("Error en el SP SQ_RecuperaBoleta_SW2 " + e.getMessage());
			boletaDTO.setIdControl(1);
			return boletaDTO;
		}

	}

	public ControlBoletaDTO inicializaBoleta(ControlBoletaDTO boletaDTO) {

		try {

			log.info("Ejecutando SI_RangoBoleta_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_RangoBoleta_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NombrePC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaDesde", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaHasta", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);


			log.info("Fld_CodCCosto " + boletaDTO.getIdCentroCosto());
			log.info("Fld_NombrePC  " + boletaDTO.getNombreMaquina());
			log.info("Fld_NroBoletaDesde " + boletaDTO.getInicioBoleta());
			log.info("Fld_NroBoletaHasta " + boletaDTO.getFinBoleta());
			log.info("Fld_CodUsuario " + boletaDTO.getCodUsuario());
			
			storedProcedure.setParameter("Fld_CodCCosto", boletaDTO.getIdCentroCosto());
			storedProcedure.setParameter("Fld_NombrePC", boletaDTO.getNombreMaquina());
			storedProcedure.setParameter("Fld_NroBoletaDesde", boletaDTO.getInicioBoleta());
			storedProcedure.setParameter("Fld_NroBoletaHasta", boletaDTO.getFinBoleta());
			storedProcedure.setParameter("Fld_CodUsuario", boletaDTO.getCodUsuario());
			//
			storedProcedure.execute();

			log.info(storedProcedure.getOutputParameterValue("Fld_Retorno").toString());

			boletaDTO.setIdControl(0);

			return boletaDTO;
		} catch (Exception e) {

			log.error("Error en el SP SI_RangoBoleta_SW2 " + e.getMessage());
			boletaDTO.setIdControl(1);
			return boletaDTO;
		}

	}

	public ResponseDTO actualizaRangoBoleta(BoletaTransaccionDTO boletaDTO) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			log.info("Ejecutando SU_ActualizaBoletaActual_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_ActualizaBoletaActual_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NombrePC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaActual", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);

			log.info("Fld_CodCCosto " + boletaDTO.getIdCentroCosto());
			log.info("Fld_NombrePC " + boletaDTO.getNombreMaquina());
			log.info("Fld_NroBoletaActual " + boletaDTO.getNumeroBoleta());
			
			storedProcedure.setParameter("Fld_CodCCosto", boletaDTO.getIdCentroCosto());
			storedProcedure.setParameter("Fld_NombrePC", boletaDTO.getNombreMaquina());
			storedProcedure.setParameter("Fld_NroBoletaActual", boletaDTO.getNumeroBoleta());
			//
			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

			return respuesta;
		} catch (Exception e) {

			log.error("Error en el SP SU_ActualizaBoletaActual_SW2 " + e.getMessage());
			respuesta.setIdControl(1);
			return respuesta;
		}

	}

	public ResponseDTO anularBoleta(ControlBoletaDTO boletaDTO) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			log.info("Ejecutando SI_BoletasNulas_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_BoletasNulas_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_BoletaFactura", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			log.info("Fld_CorrCaja " + boletaDTO.getIdCaja());
			log.info("Fld_BoletaFactura " + boletaDTO.getTipo());
			log.info("Fld_NroBoletaFactura " + boletaDTO.getActualBoleta());
			log.info("Fld_CodUsuario " + boletaDTO.getCodUsuario());
			
			
			storedProcedure.setParameter("Fld_CorrCaja", boletaDTO.getIdCaja());
			storedProcedure.setParameter("Fld_BoletaFactura", boletaDTO.getTipo());
			storedProcedure.setParameter("Fld_NroBoletaFactura", boletaDTO.getActualBoleta());
			storedProcedure.setParameter("Fld_CodUsuario", boletaDTO.getCodUsuario());

			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

			if (respuesta.getIdControl() != 0) {
				respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

			return respuesta;

		} catch (Exception e) {

			log.error("Error en el SP SI_BoletasNulas_SW2 " + e.getMessage());
			respuesta.setIdControl(1);
			return respuesta;
		}

	}

}
