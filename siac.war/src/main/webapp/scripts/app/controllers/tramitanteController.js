angular.module('ccs').controller('TramitanteController', function($interval, $scope, TramitanteResource, PagosResource, $location,RutHelper, $rootScope, $cookies, $filter,$localStorage, 
		DetectorFraudeResource) {

	var stop;

	 $scope.tramitante = {};
	 $scope.regiones = TramitanteResource.cargaRegiones();
	 $scope.rutValido = false;
	 var rutMinimo	= parseInt(1000);
	 var rutJuridico = parseInt(50000000);
	 $scope.rutSearch = '';

	 if(!$rootScope.isback){
	 	$rootScope.rutDisbale = false;
	 }
	 
	 angular.element("#listado").modal("hide");
	 
	 $scope.cargaRegiones = function(){
		 var successCallback = function(data,responseHeaders){
			 $scope.regiones = data;
			 if($scope.tramitante!=null && $scope.tramitante.comuna!=null){
				 $scope.cargarDatosComunaRegion();
			 }
		 }
		 TramitanteResource.cargaRegiones(successCallback,  $scope.errorCallback)
	 }

	 $scope.cargatramitante = function() {
		
	    $scope.$emit('logout');
	    
	 	var rutRaw = angular.element("#rutTramitante").val();

		$interval.cancel(stop);
		if( $scope.tramitante.rut !== undefined && ($scope.tramitante.rut == null || $scope.tramitante.rut == "")){
			$scope.tramitante = {};
			swal({ title: 'Error', text: 'Ingrese un rut valido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
	  		function () { $("#rutTramitante").focus();	});
			return false;
		}
		else{

//			if ($scope.rutSearch === $scope.tramitante.rut) {
//				 return false;
//			}	
		
			$scope.rutSearch = $scope.tramitante.rut;
			var rutTramitante = $scope.tramitante.rut;

		     	$scope.rutValido = RutHelper.validate(rutTramitante);

				if($scope.rutValido == true){

			     	var rutTramitanteSocial = rutTramitante.replace(".", "");
			     	rutTramitanteSocial = rutTramitanteSocial.substring(0, rutTramitanteSocial.length-1);
			     	if(parseInt(rutTramitanteSocial) < rutMinimo){
			     		$scope.tramitante = {};

						swal({ title: 'Error', text: 'Ingrese un rut valido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'})
						.then(
			  				function () { $("#rutTramitante").focus();	
			  			});

			     		return false;
			     	}
			     	if(parseInt(rutTramitanteSocial) > rutJuridico){
			     		$scope.tramitante = {};
						swal({ title: 'Error', text: 'Rut debe ser persona natural!',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
			  				function () { $("#rutTramitante").focus();	});
						$scope.rutSearch = "";	
			     		return false;
			     	}
			     	angular.element("#cargando").modal();
				 	var successCallback = function(data,responseHeaders){
				 		angular.element("#rutTramitante").val("");
				 		angular.element("#cargando").modal('hide');
			        	$scope.valido = false;
			        	if(data.correlativo == "0"){
							$scope.tramitante = {
								rut : rutTramitante,
								telefono : "56",
								tieneDeclaracionUso : false
							};
							swal({ title: 'Alerta', text: 'Persona no encontrada',type: 'warning',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				  				function () { $("#tramitanteApellidoPaterno").focus();	});
			        	}
			        	else{
			        		
			        		//Revisar fraude usuario
			        		var successCallbackFraude = function(dataFraude, responseHeadersFraude){
			        			var poseeFraude = !!(dataFraude.idControl);
			        			
			        			if (poseeFraude) {
			        				var persona = data;
			        				persona.rut = rutTramitante;
						        	if(persona.telefono.length <= 9){
						        		persona.telefono = "56"+persona.telefono; 
						        	}
						        	persona.tieneDeclaracionUso = false;
			        				
			        				$rootScope.fraude = {
			        						persona: persona,
			        						dataFraude : dataFraude.lista
			        				}
			        				//$scope.limpiaForm();
			        				angular.element('#detectorFraude').modal();
			        				
			        			} else {
						        	$scope.tramitante = data;
						        	$scope.tramitante.rut = rutTramitante;
						        	if($scope.tramitante.telefono.length <= 9){
						        		$scope.tramitante.telefono = "56"+$scope.tramitante.telefono; 
						        	}
									$scope.tramitante.tieneDeclaracionUso = false;
									if ( angular.isUndefined($scope.tramitante.region) || $scope.tramitante.region == null ) {
										$scope.tramitante.comuna = {};
										$scope.comunas = {};
									} else {
										$scope.cargarDatosComunaRegion();
									}
									
			        			}
			        		}
			        		
			        		var errorCallbackFraude = function(responseFraude) {
			        			$scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
			        		}
			        		
			        		DetectorFraudeResource.detallesFraudePorRut($scope.tramitante, successCallbackFraude, errorCallbackFraude);
			        	}
			        };
			        var errorCallback = function(response) {
			            $scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
			        };

			        TramitanteResource.carga({rut: $scope.tramitante.rut}, successCallback, errorCallback);
				}
				else{
					$scope.valido = true;
					$scope.tramitante = {};
					angular.element("#rutTramitante").val("");
					$scope.rutSearch = '';
					rutRaw = "";	 //$scope.tramitante = {};
				}
		}
	 }
	 
	 $scope.autorizacion = function(){
	    console.log("AUTORIZACION TramitanteController");
	 }

	 $scope.cargarDatosComunaRegion = function(){
		 if($scope.tramitante.comuna != null && $scope.tramitante.comuna.codigo != null && $scope.tramitante.region != null && $scope.tramitante.region.codigo != null  ){
			 $scope.tramitante.region = $filter('filter')($scope.regiones, {codigo: $scope.tramitante.region.codigo}) [0];
			 var successCallback2 = function(data,responseHeaders){
				 $scope.comunas = data;
				 $scope.tramitante.comuna = $filter('filter')($scope.comunas, {codigo: $scope.tramitante.comuna.codigo}) [0];
			 }
			 TramitanteResource.cargaComunas({idRegion: $scope.tramitante.region.codigo}, successCallback2, $scope.errorCallback);
		 }
	 }

	 $scope.cargaComunas = function(){
		 $scope.tramitante.comuna = "";
		 $scope.comunas = TramitanteResource.cargaComunas({idRegion: $scope.tramitante.region.codigo});
		 $scope.$broadcast('UiSelectComuna');
	 }

	 $scope.cambiarFocus = function(){
		 $scope.$broadcast('SelectIngresar');
	 }


	 //actualizo tramitante
	 $scope.actualizoTramitante = function(){
		 
		$scope.$emit('logout');
		 
		var mensaje = "";

		if($scope.tramitante.email !== undefined &&  $scope.tramitante.email != null && $scope.tramitante.email.length > 0){
			var res = $scope.validateEmail($scope.tramitante.email);
			if(!res){
				swal({ title: 'Error', text: 'Email inválido!',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
					function () { $("#emailTramitante").focus();	});
				return false;
			}
		}else{
			$scope.tramitante.email = "";
		}

		if($scope.tramitante.nombres == null){
			swal({ title: 'Error', text: 'Ingrese Nombres de Tramitante',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("#tramitanteNombres").focus();	});
			return false;
		}

		if($scope.tramitante.apellidoPaterno == null){
			swal({ title: 'Error', text: 'Ingrese Apellido Paterno de Tramitante',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("#tramitanteApellidoPaterno").focus();	});
			return false;
		}

		if($scope.tramitante.telefono == null){
			swal({ title: 'Error', text: 'Ingrese Teléfono de Tramitante',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("#tramitanteTelefono").focus();	});
			return false;
		}else if($scope.tramitante.telefono.length < 11){
			swal({ title: 'Error', text: 'Teléfono de Tramitante incorrecto',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("#tramitanteTelefono").focus();	});
			return false;
		}

		if(($scope.tramitante.region === undefined ||  $scope.tramitante.region == null || $scope.tramitante.region == "")){
			swal({ title: 'Error', text: 'Si selecciona Comuna, debe seleccionar Región',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("div[focus-on=UiSelectRegion] .ui-select-focusser").focus();	});
			return false;
		}

		if(($scope.tramitante.comuna === undefined || $scope.tramitante.comuna == null || $scope.tramitante.comuna == "") && $scope.tramitante.region != null){
			swal({ title: 'Error', text: 'Si selecciona Región, debe seleccionar Comuna',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
				function () { $("div[focus-on=UiSelectComuna] .ui-select-focusser").focus();	});
			return false;
		}
		if($scope.tramitante.direccion === undefined || $scope.tramitante.direccion === null){
			$scope.tramitante.direccion = "";
		}


		 $scope.setTramitante($scope.tramitante);
		 if($scope.tramitante.comuna == ""){
			 $scope.tramitante.comuna = null;
		 }
		 console.log($rootScope.tramitante);

	 	var successCallback = function(data,responseHeaders){
        	$cookies.putObject("tramitante", $scope.tramitante);
        	angular.element("#rutTramitante").val("");
        	$rootScope.tramitante = data;
        	$scope.rutSearch = "";
        	//$localStorage.rutDisbale = false;
        	//$scope.rutDisbale = $localStorage.rutDisbale
        	$location.path("/servicios");
        };
        var errorCallback = function(response) {
            $scope.tramitante.msgControl = "Servicio de Aplicación no se encuentra disponible";
        };
		$scope.tramitante.corrCaja = $rootScope.usuario.caja;
		TramitanteResource.actualizaTramitante($scope.tramitante, successCallback, errorCallback);

	 }

	 $scope.validateEmail = function(emailField){

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false){
            return false;
        }
        return true;
	}


	$scope.obtenerTramitante = function(){
		$scope.tramitante = $rootScope.tramitante;
		if($scope.tramitante.rut !== undefined){
			$rootScope.rutDisbale = true;
		}
		else{
			$rootScope.rutDisbale = false;
		}
		$scope.cargarDatosComunaRegion();
	}

    
	stop = $interval(function() {
		if($rootScope.tramitante != null ){
			$scope.obtenerTramitante();
			$scope.valido = false;
			$interval.cancel(stop);
		}
	}, 100);


    $scope.cancelarConfirm = function(){
    	
    	
   	 swal({
            title: 'Aviso',
            text: "¿Desea limpiar el formulario?, Se perderán todos los datos del carro de compra.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then(function () {
       	   $scope.limpiaForm();           
          }, function(dismiss) {
             return false;                     
          });  
   }
    
    
    
	$scope.limpiaForm = function(){
		
		$scope.aceptaTransaccion = {};
        $scope.aceptaTransaccion.idCaja = $rootScope.usuario.caja;
        $scope.aceptaTransaccion.rutTramitante = $rootScope.tramitante.rut;
        
         var successCallback2 = function(data,responseHeaders){
            if(data.idControl == 0){
                 angular.element("#listado").modal("hide");
                $scope.tramitante = {};
                $rootScope.tramitante = {};
                $rootScope.disableTram = false;
                $rootScope.rutDisbale = false;
                $localStorage.generadas = undefined;
                $localStorage.montoingrado = undefined;
                $rootScope.transacciones = [];
                $scope.rutSearch = '';
              
            } 
            else{
            	$scope.tramitante = {};
                $rootScope.tramitante = {};
                $rootScope.disableTram = false;
                $rootScope.rutDisbale = false;
                $rootScope.transacciones = [];
                $scope.rutSearch = '';
            }
         }
         PagosResource.cancelaTransaccion($scope.aceptaTransaccion, successCallback2, $scope.errorCallback);
	}
	
	$scope.noCeros = function(valor){
        if(parseInt(valor, 11) === 0){
        	swal({ title: 'Error', text: "Número Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        	$scope.tramitante.telefono = "";
        }
	}
	
	$rootScope.$on('limpiaTramitante', function(event) {
		$scope.tramitante = {};
		$scope.readOnly = false;
		$scope.rutDisbale = false;
	});
	
	 $rootScope.$on("CargarPersona", function(event, data) {
		 $scope.tramitante = data;
		 $scope.tramitante.tieneDeclaracionUso = false;
		 $scope.cargarDatosComunaRegion();
		 $("#tramitanteApellidoPaterno").focus();
     });
	 
	 $rootScope.$on("LimpiarPersona", function(event, data) {
		 $scope.limpiaForm();
		 $scope.rutSearch = '';
     });
	 
	 
	
});
