angular.module('ccs').factory('TarifaResource', function ($resource) {
    var resource = $resource('rest/listado',{
        nocache : new Date().getTime()
    },{
        'cargaTarifas' :  {  method : 'GET', isArray : true, url: 'rest/listado/tarifas'}
    });
    return resource;
});