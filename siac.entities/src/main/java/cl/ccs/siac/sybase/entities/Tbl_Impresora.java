package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Impresoras database table.
 * 
 */
@Entity
@Table(name="Tbl_Impresoras")
@NamedQuery(name="Tbl_Impresora.findAll", query="SELECT t FROM Tbl_Impresora t")
public class Tbl_Impresora implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodImpresora")
	private String fld_CodImpresora;

	@Column(name="Fld_GlosaImpresora")
	private String fld_GlosaImpresora;

	@Column(name="Fld_TipoImpresora")
	private String fld_TipoImpresora;

	public Tbl_Impresora() {
	}

	public String getFld_CodImpresora() {
		return this.fld_CodImpresora;
	}

	public void setFld_CodImpresora(String fld_CodImpresora) {
		this.fld_CodImpresora = fld_CodImpresora;
	}

	public String getFld_GlosaImpresora() {
		return this.fld_GlosaImpresora;
	}

	public void setFld_GlosaImpresora(String fld_GlosaImpresora) {
		this.fld_GlosaImpresora = fld_GlosaImpresora;
	}

	public String getFld_TipoImpresora() {
		return this.fld_TipoImpresora;
	}

	public void setFld_TipoImpresora(String fld_TipoImpresora) {
		this.fld_TipoImpresora = fld_TipoImpresora;
	}

}