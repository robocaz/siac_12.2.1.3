package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdNombres database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdNombres")
@NamedQuery(name="Tb_CeIdNombre.findAll", query="SELECT t FROM Tb_CeIdNombre t")
public class Tb_CeIdNombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tb_CeIdNombrePK id;

	@Column(name="Fld_CodFuente")
	private short fld_CodFuente;

	@Column(name="Fld_CodProveedor")
	private short fld_CodProveedor;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoFuente")
	private byte fld_TipoFuente;

	@Column(name="Fld_TipoNombre")
	private boolean fld_TipoNombre;

	public Tb_CeIdNombre() {
	}

	public Tb_CeIdNombrePK getId() {
		return this.id;
	}

	public void setId(Tb_CeIdNombrePK id) {
		this.id = id;
	}

	public short getFld_CodFuente() {
		return this.fld_CodFuente;
	}

	public void setFld_CodFuente(short fld_CodFuente) {
		this.fld_CodFuente = fld_CodFuente;
	}

	public short getFld_CodProveedor() {
		return this.fld_CodProveedor;
	}

	public void setFld_CodProveedor(short fld_CodProveedor) {
		this.fld_CodProveedor = fld_CodProveedor;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoFuente() {
		return this.fld_TipoFuente;
	}

	public void setFld_TipoFuente(byte fld_TipoFuente) {
		this.fld_TipoFuente = fld_TipoFuente;
	}

	public boolean getFld_TipoNombre() {
		return this.fld_TipoNombre;
	}

	public void setFld_TipoNombre(boolean fld_TipoNombre) {
		this.fld_TipoNombre = fld_TipoNombre;
	}

}