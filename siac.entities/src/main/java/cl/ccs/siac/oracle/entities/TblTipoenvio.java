package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_TIPOENVIO database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOENVIO")
@NamedQuery(name="TblTipoenvio.findAll", query="SELECT t FROM TblTipoenvio t")
public class TblTipoenvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOENVIO_FLDCODTIPOENVIO_GENERATOR", sequenceName="FLD_CODTIPOENVIO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOENVIO_FLDCODTIPOENVIO_GENERATOR")
	@Column(name="FLD_CODTIPOENVIO")
	private long fldCodtipoenvio;

	@Column(name="FLD_GLOSATIPOENVIO")
	private String fldGlosatipoenvio;

	public TblTipoenvio() {
	}

	public long getFldCodtipoenvio() {
		return this.fldCodtipoenvio;
	}

	public void setFldCodtipoenvio(long fldCodtipoenvio) {
		this.fldCodtipoenvio = fldCodtipoenvio;
	}

	public String getFldGlosatipoenvio() {
		return this.fldGlosatipoenvio;
	}

	public void setFldGlosatipoenvio(String fldGlosatipoenvio) {
		this.fldGlosatipoenvio = fldGlosatipoenvio;
	}

}