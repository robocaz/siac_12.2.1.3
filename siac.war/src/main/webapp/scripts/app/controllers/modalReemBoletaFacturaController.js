angular.module('ccs').controller('ModalReemBoletaFacturaController', function($scope, $rootScope, $location, $filter, $document, $uibModal, CajaResource, BoletaResource, FacturaResource, TramitanteResource, PersonaResource, TransaccionResource, FacturaUtil, RutHelper){

  var ctrl = this;

  $scope.form = {};
  $scope.cajas = [];
  $scope.boleta = {};
  $scope.boletas = [];
  $scope.factura = {};
  $scope.regiones = [];
  $scope.comunas = [];
  $scope.boletaActual = 0;
  $scope.siguienteHabilitado = false;
  $scope.esAgencia = $rootScope.usuario.esAgencia;
  $scope.iva;

  var rutMinimo	= parseInt(1000);

  /*
   * esRutValido
   * Método que valida un Rut
   */
  $scope.esRutValido = function(rut){
    rutNumerico = rut.indexOf("-") === -1 ? rut.substring(0, rut.length-1) : rut.substring(0, rut.indexOf("-"));
    rutNumerico = rutNumerico.indexOf(".") !== -1 ? rutNumerico.replace(/\./g, "") : rutNumerico;
    if(RutHelper.validate(rut.replace(/\./g, "")) && rutNumerico > rutMinimo ){
      return true;
    }else{
      return false;
    }
  };

  /*
   * onBuscar
   * Método gatillado por el botón Buscar
   */
  $scope.onBuscar = function() {
    // Sistema valida que usuario ha seleccionado una caja
    // Mensaje: Debe seleccionar una Caja antes de continuar
	  
	  $scope.factura = {};
    if( typeof($scope.form.corrCaja) === 'undefined' || $scope.form.corrCaja === null || $scope.form.corrCaja === 0 ) {
      swal({ title: 'Error', text: 'Debe seleccionar una Caja antes de continuar', type: 'error', showCloseButton: true, confirmButtonText: 'Cerrar', allowOutsideClick: false});
      return;
    }

    // Sistema valida que ha ingresado un N° de Boleta Válido >= 0
    // Mensaje: Debe ingresar un N° de Boleta
    if( typeof($scope.form.numeroBoleta) === 'undefined' || $scope.form.numeroBoleta === null || $scope.form.numeroBoleta < 0 ) {
      swal({ title: 'Error', text: 'Debe ingresar un N° de Boleta', type: 'error', showCloseButton: true, confirmButtonText: 'Cerrar', allowOutsideClick: false});
      return;
    }

    // Sistema llama al procedimiento almacenado SQ_BoletaDeCaja_S32 realizando las siguientes validaciones:
    //
    // 1. N° de Boleta existe para la Caja seleccionada
    // 2. N° de Boleta corresponde a la fecha que se requiere reemplazar por factura
    // 3. N° de Boleta no se encuentra nula
    if ( parseInt($scope.form.numeroBoleta) > 0 ) {
      $scope.cargarBoleta($scope.form.numeroBoleta, 0);
    }

    // Sistema valida que el N° de boleta ingresado es cero y llama al Procedimiento almacenado SQ_LisTranFolio0_S32
    // Si hay más de una boleta con número cero se habilita el botón Siguiente.
    // Botón Siguiente: Muestra en pantalla la siguiente transacción con boleta cero encontrada para la caja seleccionada.
    // El botón se deshabilita cuando no existan más boletas cero que mostrar.
    if ( parseInt($scope.form.numeroBoleta) === 0 ) {
      var obtenerBoletasFolioCero = {
        corrCaja: $scope.form.corrCaja
      };
      var successCallbackObtenerBoletasFolioCero = function( data ) {
        console.log('successCallbackObtenerBoletasFolioCero');
        console.log(data);

        // validamos la respuesta del Servidor
        if( data.idControl !== 0 || typeof(data.lista) === 'undefined' || data.lista.length === 0 ) {
          swal({ title: 'Error', text:'No existen boletas sin folio generadas hoy día!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
          return;
        }

        $scope.boletas = data.lista;
        $scope.boletaActual = 0;
        $scope.cargarBoleta(0, $scope.boletas[$scope.boletaActual]);
        $scope.siguienteHabilitado = $scope.boletaActual < ($scope.boletas.length - 1);
        // $scope.boleta = $scope.boletas[$scope.boletaActual];
      };
      BoletaResource.obtenerBoletasFolioCero(obtenerBoletasFolioCero, successCallbackObtenerBoletasFolioCero, $scope.errorCallback);
    }

  };

  /*
     * cargarBoleta
     * Método que carga una Boleta
     */
  $scope.cargarBoleta = function(numeroBoleta, numeroTransaccion) {
    var boletaValida = true;

    var successCallbackObtenerBoleta = function( data ) {
      console.log('successCallbackObtenerBoleta');
      console.log(data);

      // validamos la respuesta del Servidor
      if( data.idControl !== 0 ) {
        swal({ title: 'Error', text:( typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró obtener la boleta!' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }

      // N° de Boleta existe para la Caja seleccionada
      if( numeroBoleta !== 0 && Number(data.corrCaja) !== Number($scope.form.corrCaja) ) {
        swal({ title: 'Error', text: 'Boleta no existe para la caja seleccionada', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
        boletaValida = false;
      }

      // TODO
      // N° de Boleta corresponde a la fecha que se requiere reemplazar por factura


      // N° de Boleta no se encuentra nula
      if( numeroBoleta !== 0 && ( typeof(data.numeroBoleta) === 'undefined' || data.numeroBoleta === null || data.numeroBoleta <= 0 ) ) {
        swal({ title: 'Error', text: 'Boleta ingresada se encuentra anulada', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
        boletaValida = false;
      }

      if( boletaValida ) {
        $scope.onBoletaCarga(data);
      }

    };

    if( numeroBoleta !== 0 && numeroTransaccion === 0 ) {
      var obtenerBoleta = {
        corrCaja: $scope.form.corrCaja,
        numeroBoleta: numeroBoleta
      };
      BoletaResource.obtenerBoleta(obtenerBoleta, successCallbackObtenerBoleta, $scope.errorCallback);
    }else if( numeroBoleta === 0 && numeroTransaccion !== 0  ) {
      var obtenerBoletaTransaccion = {
        corrTransaccion: numeroTransaccion
      };
      BoletaResource.obtenerBoletaTransaccion(obtenerBoletaTransaccion, successCallbackObtenerBoleta, $scope.errorCallback);
    }
  };

  /*
     * onBoletaCarga
     * Método que carga una boleta en el formulario
     */
  $scope.onBoletaCarga = function( boleta ) {
    console.log('onBoletaCarga');
    console.log(boleta);
    // Sistema recupera los datos del afectado de persona natural, llamando al procedimiento almacenado y llenando los siguientes campos del formulario:
    //
    //  a. N° de Factura
    //  b. Rut (Rut identificado en la boleta)
    //  c. Razón Social (Sistema recupera Nombres, paterno y Materno, si es persona natural, y la razón social es empresa)
    //  d. Giro
    //  e. Dirección
    //  f. Región
    //  g. Comuna
    //
    console.log('boleta!');
    $scope.boleta = boleta;

    // Incializamos la factura a partir de los datos de la boleta
    console.log('factura!');
    $scope.factura = boleta;

    $scope.factura.idCentroCosto = Number($rootScope.usuario.centroCostoActivo.codigo);
    $scope.factura.rutFacturacion = $scope.boleta.rutAfectado;
    $scope.factura.tipoPersona = RutHelper.tipoPersona($scope.factura.rutFacturacion);
	if( $scope.factura.tipoPersona === 'J' ) {
		console.log('J');
	    $scope.factura.razonSocial = boleta.nombres + boleta.apellidoPaterno + boleta.apellidoMaterno;
	}else{
		console.log('N');
		$scope.factura.nombres = boleta.nombres;
		$scope.factura.apellidoPaterno = boleta.apellidoPaterno;
		$scope.factura.apellidoMaterno = boleta.apellidoMaterno;
	}
    $scope.factura.medioPago = $scope.factura.tipoPago;

    // Tramitante
    var successCallbackTramitante = function(dataTramitante){
      console.log('successCallbackTramitante');
      console.log(dataTramitante);

      // Revisamos errores
      if (dataTramitante.idControl !== 0 && dataTramitante.msgControl !== null) {
        swal({ title: 'Error', text: 'No se logró obtener el tramitante!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }

      $scope.factura.tramitante = dataTramitante;

      $scope.form.region = $filter('filter')($scope.regiones, {codigo: dataTramitante.region.codigo})[0];
      var successCallbackComunas = function(dataComunas){
        console.log('successCallbackComunas');
        console.log(dataComunas);

        $scope.comunas = dataComunas;
        $scope.form.comuna = $filter('filter')($scope.comunas, {codigo: dataTramitante.comuna.codigo})[0];
      };
      TramitanteResource.cargaComunas({idRegion: dataTramitante.region.codigo}, successCallbackComunas, $scope.errorCallback);
    };
    TramitanteResource.carga({rut: $scope.factura.rutFacturacion}, successCallbackTramitante, $scope.errorCallback);

    // Sistema recupera el detalle de la grilla de la boleta llamando al procedimiento almacenado, cargando los servicios asociados a la boleta
    //
    //  a. Cantidad
    //  b. Nombre del servicio
    //  c. Precio Unitario
    //  d. Monto Total
    //  e. Cód. Producto
    //
    // $scope.factura.transacciones;

    // De las misma manera, a través del procedimiento almacenado se obtienen los totales de la factura
    //
    //  a. Monto Neto $ (montoBruto)
    //  b. I.V.A. $ (iva)
    //  c. Monto Bruto $ (total)
    //
    // * Utiliza también: (cantidadServicio), (costoServicio)
    //
    // $scope.factura = FacturaUtil.montosFactura( $scope.factura );

    // Sistema habilita el botón Modificar del formulario
    $scope.factura.esNueva = true;
    console.log('onBoletaCarga Ready!');
    console.log($scope.factura);
  };

  /*
   * onSiguiente
   * Método gatillado por el botón Siguiente
   */
  $scope.onSiguiente = function () {
    console.log('onSiguiente');
    if( $scope.boletaActual < ($scope.boletas.length - 1) ) {
      $scope.boletaActual += 1;
      $scope.cargarBoleta(0, $scope.boletas[$scope.boletaActual]);
      $scope.siguienteHabilitado = $scope.boletaActual < ($scope.boletas.length - 1);
    }
  };

  /*
   * onOrdenCompraCambia
   * Método gatillado cuando cambia el radio button de OC
   */
  $scope.onOrdenCompraCambia = function () {
    console.log($scope.form.oc);
    if( !$scope.form.oc ) {
      $scope.factura.ordenCompra = '';
    }
  };

  /*
   * onRutCambia
   * Método que valida el Rut al perder el foco
   */
  $scope.onRutCambia = function () {
    console.log('getDatosRut');
    console.log($scope.factura.rutFacturacion);

    // si no es válido se alerta al usuario, de lo contrario busca información en procedimiento almacenado
    if( typeof($scope.factura.rutFacturacion) === 'undefined' || $scope.factura.rutFacturacion === null || $scope.factura.rutFacturacion.trim() === '' || !$scope.esRutValido($scope.factura.rutFacturacion) ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }

    $scope.getDatosRut();
  };

  /*
   * getTipoPersonaRut
   * Método que obtiene el tipo de rut
   */
  $scope.getTipoPersonaRut = function (rut) {
    console.log('getTipoPersonaRut: ' + rut);
    $scope.factura.tipoPersona = FacturaUtil.getTipoPersonaRut(rut);
  };

  /*
   * getDatosRut
   * Método que obtiene información del rut en procedimiento almacenado
   */
  $scope.getDatosRut = function () {
    console.log('getDatosRut');
    $scope.getTipoPersonaRut($scope.factura.rutFacturacion);
    $scope.factura.rutFacturacion = RutHelper.format($scope.factura.rutFacturacion);
    console.log($scope.factura.rutFacturacion);

    var successCallback = function(data, responseHeaders){
      console.log(data);

      /*if( data.idControl !== 0 ) {
        swal({ title: 'Error', text: ( data.msgControl === ' ' ? 'No se logró obtener datos rut!' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }*/

      if( $scope.factura.tipoPersona === 'J' ) {
        $scope.factura.razonSocial = data.razonSocial;
      }else{
        $scope.factura.nombres = data.tramitante.nombres;
        $scope.factura.apellidoPaterno = data.tramitante.apellidoPaterno;
        $scope.factura.apellidoMaterno = data.tramitante.apellidoMaterno;
      }
      $scope.factura.giro = data.giro;
      $scope.factura.direccion = data.direccion;
      $scope.factura.codRegion = data.codRegion;
      $scope.factura.codComuna = data.codComuna;

      if( typeof($scope.factura.codRegion) !== 'undefined' && $scope.factura.codRegion !== null && $scope.factura.codRegion !== 0 ) {
        $scope.form.region = $filter('filter')($scope.regiones, {codigo: $scope.factura.codRegion}) [0];
        var successCallbackComunas = function(data, responseHeaders){
          $scope.comunas = data;
          if( typeof($scope.factura.codComuna) !== 'undefined' && $scope.factura.codComuna !== null && $scope.factura.codComuna !== 0 ) {
            $scope.form.comuna = $filter('filter')($scope.comunas, {codigo: $scope.factura.codComuna}) [0];
          }else{
            $scope.form.comuna = null;
          }
        };
        TramitanteResource.cargaComunas({idRegion: $scope.factura.codRegion}, successCallbackComunas, $scope.errorCallback);
      }else{
        $scope.form.region = null;
        $scope.form.comuna = null;
      }
    };
    $scope.factura.tramitante = $scope.tramitante;
    FacturaResource.rut($scope.factura, successCallback, $scope.errorCallback);
  };

  /*
   * onRegionCambia
   * Método gatillado cuando cambia el select de Región
   */
  $scope.onRegionCambia = function () {
    var successCallbackComunas = function(data, responseHeaders){
      $scope.comunas = data;
    };
    TramitanteResource.cargaComunas({idRegion: $scope.form.region.codigo}, successCallbackComunas, $scope.errorCallback);
  };

  /*
   * modificar
   * Método gatillado por el botón Modificar
   */
  $scope.modificar = function() {


    //se valida monto de boleta

    if($scope.factura.montoBruto == 0){
         swal({ title: 'Error', text: 'No se pueden reemplazar boletas con monto 0', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
         return;
    }


    // Sistema valida que la casilla Orden de Compra se encuentra marcada y el campo N° de orden no está vacío
    var regexOC = /^[A-Za-z0-9]{1,20}$/;
    if( $scope.form.oc && ( typeof($scope.factura.ordenCompra) === 'undefined' || $scope.factura.ordenCompra === null || !regexOC.test($scope.factura.ordenCompra) ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el N° de orden de Compra válida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    // Sistema valida que el usuario es Agencia y solicita el número de Factura
    //var regexNumeroFactura = /[0-9]{1,8}/;
    if( $scope.esAgencia && ( typeof($scope.factura.numeroFactura) === 'undefined' || $scope.factura.numeroFactura === null || isNaN($scope.factura.numeroFactura) || $scope.factura.numeroFactura <= 0 || $scope.factura.numeroFactura.toString().length > 8 ) ) {
      swal({ title: 'Error', text: 'Debe Ingresar el número de la factura', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    // Sistema valida que los siguientes campos no se encuentran vacíos:
    //
    //  a. Rut: Debe ingresar un Rut valido para Facturar
    //  b. Apellido paterno (para persona natural): Debe ingresar Apellido Paterno valido
    //  c. Nombres o Razón Social: Debe ingresar Nombre | Debe ingresar Razón Social valida
    //  d. Giro: Debe ingresar Giro
    //  e. Dirección: Debe ingresar Dirección
    //  f. Región: Debe seleccionar la Región
    //  g. Comuna: Debe seleccionar la Comuna
    //
    if( typeof($scope.factura.rutFacturacion) === 'undefined' || $scope.factura.rutFacturacion === null || $scope.factura.rutFacturacion.trim() === '' || !$scope.esRutValido($scope.factura.rutFacturacion) ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }
    if( $scope.factura.tipoPersona === 'N' && ( typeof($scope.factura.nombres) === 'undefined' || $scope.factura.nombres === null || $scope.factura.nombres.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.factura.tipoPersona === 'N' && ( typeof($scope.factura.apellidoPaterno) === 'undefined' || $scope.factura.apellidoPaterno === null || $scope.factura.apellidoPaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Apellido Paterno válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.factura.tipoPersona === 'N' && ( typeof($scope.factura.apellidoMaterno) === 'undefined' || $scope.factura.apellidoMaterno === null || $scope.factura.apellidoMaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Apellido Materno válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.factura.tipoPersona === 'J' && ( typeof($scope.factura.razonSocial) === 'undefined' || $scope.factura.razonSocial === null || $scope.factura.razonSocial.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Razón Social válida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.factura.giro) === 'undefined' || $scope.factura.giro === null || $scope.factura.giro.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Giro', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.factura.direccion) === 'undefined' || $scope.factura.direccion === null || $scope.factura.direccion.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Dirección', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.region) === 'undefined' || $scope.form.region === null || typeof($scope.form.region.codigo) === 'undefined' || $scope.form.region.codigo === null || $scope.form.region.codigo === 0 || $scope.form.region.codigo === '0' || $scope.form.region.codigo.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Región', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.comuna) === 'undefined' || $scope.form.comuna === null || typeof($scope.form.comuna.codigo) === 'undefined' || $scope.form.comuna.codigo === null || $scope.form.comuna.codigo === 0 || $scope.form.comuna.codigo === '0' || $scope.form.comuna.codigo.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Comuna', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    if ( !ctrl.formulario.$valid ) {
      swal({ title: 'Error', text: 'El formulario presenta errores!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if(!$scope.form.oc){
    	$scope.factura.ordenCompra = "";
    }
    	
    

    // Mensaje de Confirmación
    var mensaje = "¿Está seguro que quiere reemplazar la Boleta " + ($scope.boleta.numeroBoleta === null ? "folio 0" : $scope.boleta.numeroBoleta) + " por la Factura?";
    swal({
      title: 'Aviso',
      text: mensaje,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {

      // Se llama al Caso de uso “Autorización del Supervisor”
      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modalloginsupervisor.html',
        controller: 'ModalLoginSupervisorController',
        appendTo: angular.element($document[0].querySelector('#reemboletafactura')),
        size: 'sm'
      });
      modalInstance.result.then(function( respuesta ) {

        if(!respuesta){
          return;
        }

        // Actualizamos Tramitante
        var actualizaTramitante = {
          rut: RutHelper.getRutSP($scope.factura.rutFacturacion),
          apellidoPaterno: $scope.factura.apellidoPaterno,
          apellidoMaterno: $scope.factura.apellidoMaterno,
          nombres: $scope.factura.nombres,
          direccion: $scope.factura.direccion,
          telefono: $scope.factura.tramitante.telefono,
          giro: $scope.factura.giro,
          region: $scope.form.region,
          comuna: $scope.form.comuna,
          email: $scope.factura.tramitante.email,
        };
        var successCallbackActualizaTramitante = function(dataTramitante) {
          console.log('successCallbackActualizaTramitante');
          console.log(dataTramitante);

          // Revisamos errores
          /*if (dataTramitante.idControl !== 0) {
            swal({ title: 'Error', text: 'No se logró actualizar el tramitante!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
            return;
          }*/

          if( $scope.factura.tipoPersona === 'N' ) {
            $scope.factura.razonSocial =  $scope.factura.nombres.trim() + ' ' + $scope.factura.apellidoPaterno.trim() + ' ' + $scope.factura.apellidoMaterno.trim();
          }
          $scope.factura.codRegion = Number($scope.form.region.codigo);
          $scope.factura.codComuna = Number($scope.form.comuna.codigo);
          $scope.factura.comunaDesc = $scope.form.comuna.descripcion;
          $scope.factura.corrNombre = dataTramitante.correlativo;
          $scope.factura.usuario = $rootScope.usuario.username;

          // Usuario es Sucursal o Agencia?
          if( $scope.esAgencia ) {

            // Si es Agencia continua con el proceso
            $scope.reemplazarBoletaFactura($scope.factura);

          }else if( !$scope.esAgencia ) {

            // Si es Sucursal, se llama al CU WS de Facturación, si es correcta la emisión de la factura, continua con el proceso
            console.log('es Sucursal!');

            // modal cargando
            angular.element("#cargando").modal({
              backdrop: 'static',
              keyboard: false
            });

            // Primero Solicitamos Folio
            var successCallbackFolio = function (dataFactura) {
              console.log('successCallbackFolioT');
              console.log(dataFactura);

              // Revisamos errores
              if (dataFactura.idControl !== 0 && dataFactura.msgControl !== "") {
                angular.element("#cargando").modal("hide");
                swal({ title: 'Error', text: dataFactura.msgControl, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
                return;
              }
              if(dataFactura.idControl !== 0 && dataFactura.msgControl === ""){
                angular.element("#cargando").modal("hide");
                swal({ title: 'Error', text: 'No se logró obtener folio para la factura', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
                return;
              }

              var successCallbackEmision = function (dataEmision) {
                console.log('successCallbackEmisionT');
                console.log(dataEmision);

                // Revisamos errores
                if (dataEmision.idControl !== 0) {
                  angular.element("#cargando").modal("hide");
                  swal({ title: 'Error', text: dataEmision.msgControl, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
                  return;
                }

                angular.element("#cargando").modal("hide");

                dataFactura.pdf = dataEmision.msgControl;
                dataFactura.corrNombre = dataTramitante.correlativo;

                $scope.reemplazarBoletaFactura(dataFactura);

              };
              FacturaResource.emitir(dataFactura, successCallbackEmision, $scope.errorCallback);

            };
            FacturaResource.folio($scope.factura, successCallbackFolio, $scope.errorCallback);

          }

        };
        TramitanteResource.actualizaTramitante(actualizaTramitante, successCallbackActualizaTramitante, $scope.errorCallback);

      });

    }, function(dismiss) {
      return false;
    });
  };

  /*
   * reemplazarBoletaFactura
   * Método que Reemplaza Boleta por Factura
   */
  $scope.reemplazarBoletaFactura = function (factura) {

    // En este punto, la factura fue emitida, se detiene la edición de la factura
    $scope.factura = factura;
    $scope.factura.esNueva = false;

    // Datos para reemplazar Boleta por Factura
    var reemplazarBoleta = {
      codCCosto: Number($rootScope.usuario.centroCostoActivo.codigo),
      corrCaja: $scope.form.corrCaja,
      corrTransaccion: $scope.boleta.corrTransaccion,
      numeroBoleta: $scope.boleta.numeroBoleta,
      numeroFactura: $scope.factura.numeroFactura,
      fecEmision: new Date().getTime(),
      rutAfectado: RutHelper.getRutSP($scope.factura.rutFacturacion),
      corrNombre: $scope.factura.corrNombre,
      montoBruto: $scope.factura.montoBruto,
      codUsuario: $rootScope.usuario.username,
      codOrigen: $scope.esAgencia ? 'ON' : 'FES',
      ordenCompra: (typeof($scope.factura.ordenCompra) === 'undefined' || $scope.factura.ordenCompra === null ? '' : $scope.factura.ordenCompra)
    };

    // Error en SP, factura ya generada
    var errorCallbackFactura = function() {
      swal({ title: 'Error', text: 'Error al realizar Reemplazo de Boleta por Factura. Informe a su Supervisor para que se genere la nota de Crédito de la Factura Emitida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
    };

    // Número de la Boleta?
    if( $scope.boleta.numeroBoleta === null || $scope.boleta.numeroBoleta === 0 ) {

      // Si es cero se llama al procedimiento almacenado SP_ReempFactBolCero2_S32, reemplazando la boleta por una factura
      var successCallbackReemplazarBoletaCero = function(data) {
        console.log('successCallbackReemplazarBoletaCero');
        console.log(data);

        // Revisamos errores
        if (data.idControl !== null && data.idControl !== 0) {
          swal({ title: 'Error', text: 'Error al realizar Reemplazo de Boleta por Factura. Informe a su Supervisor para que se genere la nota de Crédito de la Factura Emitida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
          return;
        }

        $scope.onBoletaReemplazada();

      };
      FacturaResource.reemplazarBoletaCero(reemplazarBoleta, successCallbackReemplazarBoletaCero, errorCallbackFactura);

    }else if( $scope.boleta.numeroBoleta > 0 ) {

      // Se llama al procedimiento almacenado si el número de la boleta > 0 SP_ReemplazaFactura2_S32
      var successCallbackReemplazarBoleta = function(data) {
        console.log('successCallbackReemplazarBoleta');
        console.log(data);

        // Revisamos errores
        if (data.idControl !== null && data.idControl !== 0) {
          swal({ title: 'Error', text: 'Error al realizar Reemplazo de Boleta por Factura. Informe a su Supervisor para que se genere la nota de Crédito de la Factura Emitida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
          return;
        }

        $scope.onBoletaReemplazada();

      };
      FacturaResource.reemplazarBoleta(reemplazarBoleta, successCallbackReemplazarBoleta, errorCallbackFactura);

    }


  };

  /*
   * onBoletaReemplazada
   * Método que es gatillado después de reemplazar una Boleta por Factura
   */
  $scope.onBoletaReemplazada = function () {

    // Usuario es Sucursal o Agencia?
    if( $scope.esAgencia ) {
      // Sistema despliega el siguiente mensaje de confirmación junto al botón Cerrar para la Agencia: Mensaje: Boleta reemplazada por factura exitosamente
      swal({ title: 'Éxito', text: 'Boleta reemplazada por factura exitosamente', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function(){
        $scope.cerrar();
      });
    }else if( !$scope.esAgencia ) {
      // Sistema despliega el siguiente mensaje de confirmación junto al botón Cerrar para la Agencia: Mensaje: Boleta reemplazada por factura exitosamente
      swal({ title: 'Éxito', text: 'Boleta reemplazada por factura exitosamente, por favor imprima las copias de la factura', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});

      // Sistema muestra las copias de las facturas en PDF
      angular.element("#reemboletafacturadescarga").modal();
    }
  };

  /*
   * cerrar
   * Método gatillado por el botón Cerrar y otros métodos del controlador
   */
  $scope.cerrar = function () {
    angular.element("#reemboletafactura").on('hide.bs.modal', function () {
      $scope.form = {
        corrCaja: '',
        numeroBoleta: 0,
        oc: false
      };

      $scope.boleta = {};
      $scope.boletas = [];
      $scope.factura = {};

      ctrl.formulario.$setPristine();
      ctrl.formulario.$setUntouched();
    });

    angular.element('#reemboletafactura').modal("hide");
  };

  /*
   * cerrar
   * Método gatillado por el botón Cerrar y Aceptar del modal de Descarga
   */
  $scope.cerrarDescarga = function () {
    angular.element('#reemboletafacturadescarga').modal("hide");
    $scope.cerrar();
  };

  /*
   * init
   * Método gatillado al inciio del Controlador
   */
  $scope.init = function () {
    // Validación Emisión Facturas
    if( !$scope.emiteFactura ) {
      swal({ title: 'Error', text: 'Usuario no puede emitir facturas', type: 'error', showCloseButton: true, confirmButtonText: 'Cerrar', allowOutsideClick: false}).then(function(){
        $scope.cerrar();
      });
      return;
    }

    // Validación Carro Vacío
    if( $rootScope.transacciones.length > 0 ) {
      swal({ title: 'Error', text: 'Existen transacciones sin procesar, no es posible reemplazar Boleta por Factura.', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function(){
        $scope.cerrar();
      });
      return;
    }
    
    $scope.esAgencia = $rootScope.usuario.esAgencia;
    console.log('esAgencia = '+$scope.esAgencia);

    $scope.form = {
      corrCaja: '',
      numeroBoleta: 0,
      oc: false
    };

    var successCallbackIVA = function(data, responseHeaders){
      console.log('successCallbackIVA');
      console.log(data);
      if(data.idControl !== 0) {
        swal({ title: 'Error', text: 'No se puede obtener el dato IVA.', type: 'error', showCloseButton: true, confirmButtonText: 'Cerrar', allowOutsideClick: false}).then(function(){
          $scope.cerrar();
        });
        return;
      }
      $scope.iva = parseFloat(data.msgControl);
    };
    FacturaResource.obtenerIVA(successCallbackIVA, $scope.errorCallback);

    // Cargar Listado de Cajas del Día
    var successCallbackListarCajasDia = function (data) {
      console.log('successCallbackListarCajasDia');
      console.log(data);
      $scope.cajas = data;
    };
    CajaResource.listarCajasDia(successCallbackListarCajasDia, $scope.errorCallback);

    // Cargar Listado de Cajas
    /*$scope.obtieneNombreMaquina();
    $scope.form.centrocosto = $rootScope.usuario.centroCostoActivo.descripcion;
    $scope.boleta.nombreMaquina = $rootScope.nombreMaquina;
    $scope.boleta.idCentroCosto = Number($rootScope.usuario.centroCostoActivo.codigo);
    var successCallbackCajas = function(data){
      $scope.cajas = data;
    };
    CajaResource.cajasabiertas(successCallbackCajas, $scope.errorCallback);*/

    // Cargar Listado de Regiones
    var successCallbackRegiones = function(data, responseHeaders){
      $scope.regiones = data;
    };
    TramitanteResource.cargaRegiones(successCallbackRegiones, $scope.errorCallback);
  };

  $scope.abrir = function(){
    console.log('ModalReemBoletaFacturaController - abrir');
    $scope.factura = {};
    angular.element("#reemboletafactura").modal();
  };

  $scope.$on('onReemBoletaFacturaInicia', function(event, args) {
    console.log('ModalReemBoletaFacturaController - onReemBoletaFacturaInicia');
    console.log(args);
  });

  angular.element("#reemboletafacturadescarga").on('hide.bs.modal', function () {
    $scope.cerrar();
  });

  // Se llama al método inicial del controlador
  angular.element("#reemboletafactura").on('show.bs.modal', function () {
    $scope.init();
  });

});
