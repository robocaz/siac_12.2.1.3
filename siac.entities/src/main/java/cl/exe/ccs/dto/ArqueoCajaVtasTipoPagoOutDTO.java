package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ArqueoCajaVtasTipoPagoOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 8563868465936704890L;

	private List<ArqueoCajaTipoPagoData> lista;
	private Integer totalTrxs;
	private Integer totalPagado;
	private Integer totalCalc;
	private Integer totalDiff;
	private Integer totalDctos;

	public List<ArqueoCajaTipoPagoData> getLista() {
		return lista;
	}
	public void setLista(List<ArqueoCajaTipoPagoData> lista) {
		this.lista = lista;
	}
	public Integer getTotalTrxs() {
		return totalTrxs;
	}
	public void setTotalTrxs(Integer totalTrxs) {
		this.totalTrxs = totalTrxs;
	}
	public Integer getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(Integer totalPagado) {
		this.totalPagado = totalPagado;
	}
	public Integer getTotalCalc() {
		return totalCalc;
	}
	public void setTotalCalc(Integer totalCalc) {
		this.totalCalc = totalCalc;
	}
	public Integer getTotalDiff() {
		return totalDiff;
	}
	public void setTotalDiff(Integer totalDiff) {
		this.totalDiff = totalDiff;
	}
	public Integer getTotalDctos() {
		return totalDctos;
	}
	public void setTotalDctos(Integer totalDctos) {
		this.totalDctos = totalDctos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaVtasTipoPagoOutDTO [lista=");
		builder.append(lista);
		builder.append(", totalTrxs=");
		builder.append(totalTrxs);
		builder.append(", totalPagado=");
		builder.append(totalPagado);
		builder.append(", totalCalc=");
		builder.append(totalCalc);
		builder.append(", totalDiff=");
		builder.append(totalDiff);
		builder.append(", totalDctos=");
		builder.append(totalDctos);
		builder.append(", msgControl=");
		builder.append(msgControl);
		builder.append(", idControl=");
		builder.append(idControl);
		builder.append("]");
		return builder.toString();
	}

}
