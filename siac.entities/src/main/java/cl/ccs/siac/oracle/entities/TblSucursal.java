package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_SUCURSAL database table.
 * 
 */
@Entity
@Table(name="TBL_SUCURSAL")
@NamedQuery(name="TblSucursal.findAll", query="SELECT t FROM TblSucursal t")
public class TblSucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TblSucursalPK id;

	@Column(name="FLD_CODLOCALIDAD")
	private BigDecimal fldCodlocalidad;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSASUCURSAL")
	private String fldGlosasucursal;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblMorhisto
	@OneToMany(mappedBy="tblSucursal")
	private List<TblMorhisto> tblMorhistos;

	//bi-directional many-to-one association to TblMorosidad
	@OneToMany(mappedBy="tblSucursal")
	private List<TblMorosidad> tblMorosidads;

	//bi-directional many-to-one association to TblEmisor
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FLD_CODEMISOR", referencedColumnName="FLD_CODEMISOR"),
		@JoinColumn(name="FLD_TIPOEMISOR", referencedColumnName="FLD_TIPOEMISOR")
		})
	private TblEmisor tblEmisor;

	public TblSucursal() {
	}

	public TblSucursalPK getId() {
		return this.id;
	}

	public void setId(TblSucursalPK id) {
		this.id = id;
	}

	public BigDecimal getFldCodlocalidad() {
		return this.fldCodlocalidad;
	}

	public void setFldCodlocalidad(BigDecimal fldCodlocalidad) {
		this.fldCodlocalidad = fldCodlocalidad;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosasucursal() {
		return this.fldGlosasucursal;
	}

	public void setFldGlosasucursal(String fldGlosasucursal) {
		this.fldGlosasucursal = fldGlosasucursal;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblMorhisto> getTblMorhistos() {
		return this.tblMorhistos;
	}

	public void setTblMorhistos(List<TblMorhisto> tblMorhistos) {
		this.tblMorhistos = tblMorhistos;
	}

	public TblMorhisto addTblMorhisto(TblMorhisto tblMorhisto) {
		getTblMorhistos().add(tblMorhisto);
		tblMorhisto.setTblSucursal(this);

		return tblMorhisto;
	}

	public TblMorhisto removeTblMorhisto(TblMorhisto tblMorhisto) {
		getTblMorhistos().remove(tblMorhisto);
		tblMorhisto.setTblSucursal(null);

		return tblMorhisto;
	}

	public List<TblMorosidad> getTblMorosidads() {
		return this.tblMorosidads;
	}

	public void setTblMorosidads(List<TblMorosidad> tblMorosidads) {
		this.tblMorosidads = tblMorosidads;
	}

	public TblMorosidad addTblMorosidad(TblMorosidad tblMorosidad) {
		getTblMorosidads().add(tblMorosidad);
		tblMorosidad.setTblSucursal(this);

		return tblMorosidad;
	}

	public TblMorosidad removeTblMorosidad(TblMorosidad tblMorosidad) {
		getTblMorosidads().remove(tblMorosidad);
		tblMorosidad.setTblSucursal(null);

		return tblMorosidad;
	}

	public TblEmisor getTblEmisor() {
		return this.tblEmisor;
	}

	public void setTblEmisor(TblEmisor tblEmisor) {
		this.tblEmisor = tblEmisor;
	}

}