package cl.ccs.siac.transaccion;

import cl.exe.ccs.dto.AceptaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.TransaccionDTO;
import cl.exe.ccs.dto.TransaccionGrillaDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;

public interface TransaccionBusiness {
	public ResponseDTO agregarServicio(TransaccionDTO transaccionDTO);
	
	public ResponseListDTO<TransaccionGrillaDTO> listarServicio(Long corrCaja);
	
	public ResponseDTO cancelarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO);
	
	public ResponseDTO aceptarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO);
	
	public ResponseListDTO<CertificadoDTO> certificadosTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO);
	
	public ResponseDTO eliminarServicio(Long idCaja, Long corrMov);
}
