package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_LOGACLARACION database table.
 * 
 */
@Entity
@Table(name="TBL_LOGACLARACION")
@NamedQuery(name="TblLogaclaracion.findAll", query="SELECT t FROM TblLogaclaracion t")
public class TblLogaclaracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_LOGACLARACION_FLDCORRLOGACLID_GENERATOR", sequenceName="FLD_CORRLOGACL_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_LOGACLARACION_FLDCORRLOGACLID_GENERATOR")
	@Column(name="FLD_CORRLOGACL_ID")
	private long fldCorrlogaclId;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	@Column(name="FLD_FECHATRANSACCION")
	private Timestamp fldFechatransaccion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_RESULTADO")
	private BigDecimal fldResultado;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_TIPORELACION")
	private String fldTiporelacion;

	//bi-directional many-to-one association to TblUsuariomol
	@ManyToOne
	@JoinColumn(name="FLD_USUARIOACLARACION")
	private TblUsuariomol tblUsuariomol;

	//bi-directional many-to-one association to TblTipotransaccion
	@ManyToOne
	@JoinColumn(name="FLD_CODTRANSACCION")
	private TblTipotransaccion tblTipotransaccion;

	public TblLogaclaracion() {
	}

	public long getFldCorrlogaclId() {
		return this.fldCorrlogaclId;
	}

	public void setFldCorrlogaclId(long fldCorrlogaclId) {
		this.fldCorrlogaclId = fldCorrlogaclId;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}

	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public Timestamp getFldFechatransaccion() {
		return this.fldFechatransaccion;
	}

	public void setFldFechatransaccion(Timestamp fldFechatransaccion) {
		this.fldFechatransaccion = fldFechatransaccion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public BigDecimal getFldResultado() {
		return this.fldResultado;
	}

	public void setFldResultado(BigDecimal fldResultado) {
		this.fldResultado = fldResultado;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public String getFldTiporelacion() {
		return this.fldTiporelacion;
	}

	public void setFldTiporelacion(String fldTiporelacion) {
		this.fldTiporelacion = fldTiporelacion;
	}

	public TblUsuariomol getTblUsuariomol() {
		return this.tblUsuariomol;
	}

	public void setTblUsuariomol(TblUsuariomol tblUsuariomol) {
		this.tblUsuariomol = tblUsuariomol;
	}

	public TblTipotransaccion getTblTipotransaccion() {
		return this.tblTipotransaccion;
	}

	public void setTblTipotransaccion(TblTipotransaccion tblTipotransaccion) {
		this.tblTipotransaccion = tblTipotransaccion;
	}

}