package cl.exe.ccs.dto;

import java.io.Serializable;

public class TipoDTO implements Serializable{
	
	private String codigoTipo;
	private String descripcionTipo;
	
	
	public String getCodigoTipo() {
		return codigoTipo;
	}
	public void setCodigoTipo(String codigoTipo) {
		this.codigoTipo = codigoTipo;
	}
	public String getDescripcionTipo() {
		return descripcionTipo;
	}
	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}
	

	
	
	

}
