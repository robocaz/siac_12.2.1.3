package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_VtlDetalle database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VtlDetalle.findAll", query="SELECT t FROM Tbl_VtlDetalle t")
public class Tbl_VtlDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecAntePenultimo")
	private Timestamp fld_FecAntePenultimo;

	@Column(name="Fld_FecMasAntigua")
	private int fld_FecMasAntigua;

	@Column(name="Fld_FecPenultimo")
	private Timestamp fld_FecPenultimo;

	@Column(name="Fld_FecUltimo")
	private Timestamp fld_FecUltimo;

	@Column(name="Fld_NombrePersona")
	private String fld_NombrePersona;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TotalCH")
	private short fld_TotalCH;

	@Column(name="Fld_TotalCM")
	private short fld_TotalCM;

	@Column(name="Fld_TotalCMDH")
	private short fld_TotalCMDH;

	@Column(name="Fld_TotalFS")
	private short fld_TotalFS;

	@Column(name="Fld_TotalLT")
	private short fld_TotalLT;

	@Column(name="Fld_TotalOtrasCM")
	private short fld_TotalOtrasCM;

	@Column(name="Fld_TotalPG")
	private short fld_TotalPG;

	@Column(name="Fld_TotalRA")
	private short fld_TotalRA;

	@Column(name="Fld_VMontoCH")
	private BigDecimal fld_VMontoCH;

	@Column(name="Fld_VMontoCM")
	private BigDecimal fld_VMontoCM;

	@Column(name="Fld_VMontoCMDH")
	private BigDecimal fld_VMontoCMDH;

	@Column(name="Fld_VMontoFS")
	private BigDecimal fld_VMontoFS;

	@Column(name="Fld_VMontoLT")
	private BigDecimal fld_VMontoLT;

	@Column(name="Fld_VMontoOtrasCM")
	private BigDecimal fld_VMontoOtrasCM;

	@Column(name="Fld_VMontoPG")
	private BigDecimal fld_VMontoPG;

	@Column(name="Fld_VMontoRA")
	private BigDecimal fld_VMontoRA;

	public Tbl_VtlDetalle() {
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecAntePenultimo() {
		return this.fld_FecAntePenultimo;
	}

	public void setFld_FecAntePenultimo(Timestamp fld_FecAntePenultimo) {
		this.fld_FecAntePenultimo = fld_FecAntePenultimo;
	}

	public int getFld_FecMasAntigua() {
		return this.fld_FecMasAntigua;
	}

	public void setFld_FecMasAntigua(int fld_FecMasAntigua) {
		this.fld_FecMasAntigua = fld_FecMasAntigua;
	}

	public Timestamp getFld_FecPenultimo() {
		return this.fld_FecPenultimo;
	}

	public void setFld_FecPenultimo(Timestamp fld_FecPenultimo) {
		this.fld_FecPenultimo = fld_FecPenultimo;
	}

	public Timestamp getFld_FecUltimo() {
		return this.fld_FecUltimo;
	}

	public void setFld_FecUltimo(Timestamp fld_FecUltimo) {
		this.fld_FecUltimo = fld_FecUltimo;
	}

	public String getFld_NombrePersona() {
		return this.fld_NombrePersona;
	}

	public void setFld_NombrePersona(String fld_NombrePersona) {
		this.fld_NombrePersona = fld_NombrePersona;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public short getFld_TotalCH() {
		return this.fld_TotalCH;
	}

	public void setFld_TotalCH(short fld_TotalCH) {
		this.fld_TotalCH = fld_TotalCH;
	}

	public short getFld_TotalCM() {
		return this.fld_TotalCM;
	}

	public void setFld_TotalCM(short fld_TotalCM) {
		this.fld_TotalCM = fld_TotalCM;
	}

	public short getFld_TotalCMDH() {
		return this.fld_TotalCMDH;
	}

	public void setFld_TotalCMDH(short fld_TotalCMDH) {
		this.fld_TotalCMDH = fld_TotalCMDH;
	}

	public short getFld_TotalFS() {
		return this.fld_TotalFS;
	}

	public void setFld_TotalFS(short fld_TotalFS) {
		this.fld_TotalFS = fld_TotalFS;
	}

	public short getFld_TotalLT() {
		return this.fld_TotalLT;
	}

	public void setFld_TotalLT(short fld_TotalLT) {
		this.fld_TotalLT = fld_TotalLT;
	}

	public short getFld_TotalOtrasCM() {
		return this.fld_TotalOtrasCM;
	}

	public void setFld_TotalOtrasCM(short fld_TotalOtrasCM) {
		this.fld_TotalOtrasCM = fld_TotalOtrasCM;
	}

	public short getFld_TotalPG() {
		return this.fld_TotalPG;
	}

	public void setFld_TotalPG(short fld_TotalPG) {
		this.fld_TotalPG = fld_TotalPG;
	}

	public short getFld_TotalRA() {
		return this.fld_TotalRA;
	}

	public void setFld_TotalRA(short fld_TotalRA) {
		this.fld_TotalRA = fld_TotalRA;
	}

	public BigDecimal getFld_VMontoCH() {
		return this.fld_VMontoCH;
	}

	public void setFld_VMontoCH(BigDecimal fld_VMontoCH) {
		this.fld_VMontoCH = fld_VMontoCH;
	}

	public BigDecimal getFld_VMontoCM() {
		return this.fld_VMontoCM;
	}

	public void setFld_VMontoCM(BigDecimal fld_VMontoCM) {
		this.fld_VMontoCM = fld_VMontoCM;
	}

	public BigDecimal getFld_VMontoCMDH() {
		return this.fld_VMontoCMDH;
	}

	public void setFld_VMontoCMDH(BigDecimal fld_VMontoCMDH) {
		this.fld_VMontoCMDH = fld_VMontoCMDH;
	}

	public BigDecimal getFld_VMontoFS() {
		return this.fld_VMontoFS;
	}

	public void setFld_VMontoFS(BigDecimal fld_VMontoFS) {
		this.fld_VMontoFS = fld_VMontoFS;
	}

	public BigDecimal getFld_VMontoLT() {
		return this.fld_VMontoLT;
	}

	public void setFld_VMontoLT(BigDecimal fld_VMontoLT) {
		this.fld_VMontoLT = fld_VMontoLT;
	}

	public BigDecimal getFld_VMontoOtrasCM() {
		return this.fld_VMontoOtrasCM;
	}

	public void setFld_VMontoOtrasCM(BigDecimal fld_VMontoOtrasCM) {
		this.fld_VMontoOtrasCM = fld_VMontoOtrasCM;
	}

	public BigDecimal getFld_VMontoPG() {
		return this.fld_VMontoPG;
	}

	public void setFld_VMontoPG(BigDecimal fld_VMontoPG) {
		this.fld_VMontoPG = fld_VMontoPG;
	}

	public BigDecimal getFld_VMontoRA() {
		return this.fld_VMontoRA;
	}

	public void setFld_VMontoRA(BigDecimal fld_VMontoRA) {
		this.fld_VMontoRA = fld_VMontoRA;
	}

}