package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Profile database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Profile.findAll", query="SELECT t FROM Tbl_Profile t")
public class Tbl_Profile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_LocalAccessLevel")
	private boolean fld_LocalAccessLevel;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	@Column(name="Fld_ProfileDescription")
	private String fld_ProfileDescription;

	@Column(name="Fld_ProfileName")
	private String fld_ProfileName;

	@Column(name="Fld_ProfileTimeStamp")
	private byte[] fld_ProfileTimeStamp;

	public Tbl_Profile() {
	}

	public boolean getFld_LocalAccessLevel() {
		return this.fld_LocalAccessLevel;
	}

	public void setFld_LocalAccessLevel(boolean fld_LocalAccessLevel) {
		this.fld_LocalAccessLevel = fld_LocalAccessLevel;
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

	public String getFld_ProfileDescription() {
		return this.fld_ProfileDescription;
	}

	public void setFld_ProfileDescription(String fld_ProfileDescription) {
		this.fld_ProfileDescription = fld_ProfileDescription;
	}

	public String getFld_ProfileName() {
		return this.fld_ProfileName;
	}

	public void setFld_ProfileName(String fld_ProfileName) {
		this.fld_ProfileName = fld_ProfileName;
	}

	public byte[] getFld_ProfileTimeStamp() {
		return this.fld_ProfileTimeStamp;
	}

	public void setFld_ProfileTimeStamp(byte[] fld_ProfileTimeStamp) {
		this.fld_ProfileTimeStamp = fld_ProfileTimeStamp;
	}

}