package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_rutcp2 database table.
 * 
 */
@Entity
@Table(name="tmp_rutcp2")
@NamedQuery(name="TmpRutcp2.findAll", query="SELECT t FROM TmpRutcp2 t")
public class TmpRutcp2 implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal cp;

	@Column(name="Rut")
	private String rut;

	public TmpRutcp2() {
	}

	public BigDecimal getCp() {
		return this.cp;
	}

	public void setCp(BigDecimal cp) {
		this.cp = cp;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}