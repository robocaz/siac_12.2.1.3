package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_OrdenProcesoEnvios database table.
 * 
 */
@Entity
@Table(name="Tbl_OrdenProcesoEnvios")
@NamedQuery(name="Tbl_OrdenProcesoEnvio.findAll", query="SELECT t FROM Tbl_OrdenProcesoEnvio t")
public class Tbl_OrdenProcesoEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ClasificacionOProceso")
	private boolean fld_ClasificacionOProceso;

	@Column(name="Fld_CodAreaDestino")
	private String fld_CodAreaDestino;

	@Column(name="Fld_CodAreaOrigen")
	private String fld_CodAreaOrigen;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodProceso_1")
	private byte fld_CodProceso_1;

	@Column(name="Fld_CodProceso_2")
	private byte fld_CodProceso_2;

	@Column(name="Fld_CodProceso_3")
	private byte fld_CodProceso_3;

	@Column(name="Fld_CodProceso_4")
	private byte fld_CodProceso_4;

	@Column(name="Fld_CodProceso_5")
	private byte fld_CodProceso_5;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CodUsuarioDespacho")
	private String fld_CodUsuarioDespacho;

	@Column(name="Fld_CodUsuarioEjecucion")
	private String fld_CodUsuarioEjecucion;

	@Column(name="Fld_CodUsuarioRecepAreaOrigen")
	private String fld_CodUsuarioRecepAreaOrigen;

	@Column(name="Fld_CodUsuarioRecepcion")
	private String fld_CodUsuarioRecepcion;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrOProc_Id")
	private BigDecimal fld_CorrOProc_Id;

	@Column(name="Fld_FecCreacionOProceso")
	private Timestamp fld_FecCreacionOProceso;

	@Column(name="Fld_FecDespachoOProceso")
	private Timestamp fld_FecDespachoOProceso;

	@Column(name="Fld_FecEjecucionOProceso")
	private Timestamp fld_FecEjecucionOProceso;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_FecRecepcionAreaOrigen")
	private Timestamp fld_FecRecepcionAreaOrigen;

	@Column(name="Fld_FecRecepcionOProceso")
	private Timestamp fld_FecRecepcionOProceso;

	@Column(name="Fld_FlagCodProceso_1")
	private byte fld_FlagCodProceso_1;

	@Column(name="Fld_FlagCodProceso_2")
	private byte fld_FlagCodProceso_2;

	@Column(name="Fld_FlagCodProceso_3")
	private byte fld_FlagCodProceso_3;

	@Column(name="Fld_FlagCodProceso_4")
	private byte fld_FlagCodProceso_4;

	@Column(name="Fld_FlagCodProceso_5")
	private byte fld_FlagCodProceso_5;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_IdMedioMagnetico_1")
	private String fld_IdMedioMagnetico_1;

	@Column(name="Fld_IdMedioMagnetico_2")
	private String fld_IdMedioMagnetico_2;

	@Column(name="Fld_IdMedioMagnetico_3")
	private String fld_IdMedioMagnetico_3;

	@Column(name="Fld_IdMedioMagnetico_4")
	private String fld_IdMedioMagnetico_4;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tbl_OrdenProcesoEnvio() {
	}

	public boolean getFld_ClasificacionOProceso() {
		return this.fld_ClasificacionOProceso;
	}

	public void setFld_ClasificacionOProceso(boolean fld_ClasificacionOProceso) {
		this.fld_ClasificacionOProceso = fld_ClasificacionOProceso;
	}

	public String getFld_CodAreaDestino() {
		return this.fld_CodAreaDestino;
	}

	public void setFld_CodAreaDestino(String fld_CodAreaDestino) {
		this.fld_CodAreaDestino = fld_CodAreaDestino;
	}

	public String getFld_CodAreaOrigen() {
		return this.fld_CodAreaOrigen;
	}

	public void setFld_CodAreaOrigen(String fld_CodAreaOrigen) {
		this.fld_CodAreaOrigen = fld_CodAreaOrigen;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodProceso_1() {
		return this.fld_CodProceso_1;
	}

	public void setFld_CodProceso_1(byte fld_CodProceso_1) {
		this.fld_CodProceso_1 = fld_CodProceso_1;
	}

	public byte getFld_CodProceso_2() {
		return this.fld_CodProceso_2;
	}

	public void setFld_CodProceso_2(byte fld_CodProceso_2) {
		this.fld_CodProceso_2 = fld_CodProceso_2;
	}

	public byte getFld_CodProceso_3() {
		return this.fld_CodProceso_3;
	}

	public void setFld_CodProceso_3(byte fld_CodProceso_3) {
		this.fld_CodProceso_3 = fld_CodProceso_3;
	}

	public byte getFld_CodProceso_4() {
		return this.fld_CodProceso_4;
	}

	public void setFld_CodProceso_4(byte fld_CodProceso_4) {
		this.fld_CodProceso_4 = fld_CodProceso_4;
	}

	public byte getFld_CodProceso_5() {
		return this.fld_CodProceso_5;
	}

	public void setFld_CodProceso_5(byte fld_CodProceso_5) {
		this.fld_CodProceso_5 = fld_CodProceso_5;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public String getFld_CodUsuarioDespacho() {
		return this.fld_CodUsuarioDespacho;
	}

	public void setFld_CodUsuarioDespacho(String fld_CodUsuarioDespacho) {
		this.fld_CodUsuarioDespacho = fld_CodUsuarioDespacho;
	}

	public String getFld_CodUsuarioEjecucion() {
		return this.fld_CodUsuarioEjecucion;
	}

	public void setFld_CodUsuarioEjecucion(String fld_CodUsuarioEjecucion) {
		this.fld_CodUsuarioEjecucion = fld_CodUsuarioEjecucion;
	}

	public String getFld_CodUsuarioRecepAreaOrigen() {
		return this.fld_CodUsuarioRecepAreaOrigen;
	}

	public void setFld_CodUsuarioRecepAreaOrigen(String fld_CodUsuarioRecepAreaOrigen) {
		this.fld_CodUsuarioRecepAreaOrigen = fld_CodUsuarioRecepAreaOrigen;
	}

	public String getFld_CodUsuarioRecepcion() {
		return this.fld_CodUsuarioRecepcion;
	}

	public void setFld_CodUsuarioRecepcion(String fld_CodUsuarioRecepcion) {
		this.fld_CodUsuarioRecepcion = fld_CodUsuarioRecepcion;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrOProc_Id() {
		return this.fld_CorrOProc_Id;
	}

	public void setFld_CorrOProc_Id(BigDecimal fld_CorrOProc_Id) {
		this.fld_CorrOProc_Id = fld_CorrOProc_Id;
	}

	public Timestamp getFld_FecCreacionOProceso() {
		return this.fld_FecCreacionOProceso;
	}

	public void setFld_FecCreacionOProceso(Timestamp fld_FecCreacionOProceso) {
		this.fld_FecCreacionOProceso = fld_FecCreacionOProceso;
	}

	public Timestamp getFld_FecDespachoOProceso() {
		return this.fld_FecDespachoOProceso;
	}

	public void setFld_FecDespachoOProceso(Timestamp fld_FecDespachoOProceso) {
		this.fld_FecDespachoOProceso = fld_FecDespachoOProceso;
	}

	public Timestamp getFld_FecEjecucionOProceso() {
		return this.fld_FecEjecucionOProceso;
	}

	public void setFld_FecEjecucionOProceso(Timestamp fld_FecEjecucionOProceso) {
		this.fld_FecEjecucionOProceso = fld_FecEjecucionOProceso;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public Timestamp getFld_FecRecepcionAreaOrigen() {
		return this.fld_FecRecepcionAreaOrigen;
	}

	public void setFld_FecRecepcionAreaOrigen(Timestamp fld_FecRecepcionAreaOrigen) {
		this.fld_FecRecepcionAreaOrigen = fld_FecRecepcionAreaOrigen;
	}

	public Timestamp getFld_FecRecepcionOProceso() {
		return this.fld_FecRecepcionOProceso;
	}

	public void setFld_FecRecepcionOProceso(Timestamp fld_FecRecepcionOProceso) {
		this.fld_FecRecepcionOProceso = fld_FecRecepcionOProceso;
	}

	public byte getFld_FlagCodProceso_1() {
		return this.fld_FlagCodProceso_1;
	}

	public void setFld_FlagCodProceso_1(byte fld_FlagCodProceso_1) {
		this.fld_FlagCodProceso_1 = fld_FlagCodProceso_1;
	}

	public byte getFld_FlagCodProceso_2() {
		return this.fld_FlagCodProceso_2;
	}

	public void setFld_FlagCodProceso_2(byte fld_FlagCodProceso_2) {
		this.fld_FlagCodProceso_2 = fld_FlagCodProceso_2;
	}

	public byte getFld_FlagCodProceso_3() {
		return this.fld_FlagCodProceso_3;
	}

	public void setFld_FlagCodProceso_3(byte fld_FlagCodProceso_3) {
		this.fld_FlagCodProceso_3 = fld_FlagCodProceso_3;
	}

	public byte getFld_FlagCodProceso_4() {
		return this.fld_FlagCodProceso_4;
	}

	public void setFld_FlagCodProceso_4(byte fld_FlagCodProceso_4) {
		this.fld_FlagCodProceso_4 = fld_FlagCodProceso_4;
	}

	public byte getFld_FlagCodProceso_5() {
		return this.fld_FlagCodProceso_5;
	}

	public void setFld_FlagCodProceso_5(byte fld_FlagCodProceso_5) {
		this.fld_FlagCodProceso_5 = fld_FlagCodProceso_5;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public String getFld_IdMedioMagnetico_1() {
		return this.fld_IdMedioMagnetico_1;
	}

	public void setFld_IdMedioMagnetico_1(String fld_IdMedioMagnetico_1) {
		this.fld_IdMedioMagnetico_1 = fld_IdMedioMagnetico_1;
	}

	public String getFld_IdMedioMagnetico_2() {
		return this.fld_IdMedioMagnetico_2;
	}

	public void setFld_IdMedioMagnetico_2(String fld_IdMedioMagnetico_2) {
		this.fld_IdMedioMagnetico_2 = fld_IdMedioMagnetico_2;
	}

	public String getFld_IdMedioMagnetico_3() {
		return this.fld_IdMedioMagnetico_3;
	}

	public void setFld_IdMedioMagnetico_3(String fld_IdMedioMagnetico_3) {
		this.fld_IdMedioMagnetico_3 = fld_IdMedioMagnetico_3;
	}

	public String getFld_IdMedioMagnetico_4() {
		return this.fld_IdMedioMagnetico_4;
	}

	public void setFld_IdMedioMagnetico_4(String fld_IdMedioMagnetico_4) {
		this.fld_IdMedioMagnetico_4 = fld_IdMedioMagnetico_4;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}