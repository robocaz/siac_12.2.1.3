package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_InformesGestion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_InformesGestion.findAll", query="SELECT t FROM Tbl_InformesGestion t")
public class Tbl_InformesGestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_Descripcion")
	private String fld_Descripcion;

	@Column(name="Fld_Nombre")
	private String fld_Nombre;

	@Column(name="Fld_Tipo")
	private byte fld_Tipo;

	public Tbl_InformesGestion() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_Descripcion() {
		return this.fld_Descripcion;
	}

	public void setFld_Descripcion(String fld_Descripcion) {
		this.fld_Descripcion = fld_Descripcion;
	}

	public String getFld_Nombre() {
		return this.fld_Nombre;
	}

	public void setFld_Nombre(String fld_Nombre) {
		this.fld_Nombre = fld_Nombre;
	}

	public byte getFld_Tipo() {
		return this.fld_Tipo;
	}

	public void setFld_Tipo(byte fld_Tipo) {
		this.fld_Tipo = fld_Tipo;
	}

}