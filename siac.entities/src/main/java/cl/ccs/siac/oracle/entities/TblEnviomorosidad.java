package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_ENVIOMOROSIDAD database table.
 * 
 */
@Entity
@Table(name="TBL_ENVIOMOROSIDAD")
@NamedQuery(name="TblEnviomorosidad.findAll", query="SELECT t FROM TblEnviomorosidad t")
public class TblEnviomorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ENVIOMOROSIDAD_FLDCORRENVIODIGID_GENERATOR", sequenceName="FLD_CORRENVIODIG_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ENVIOMOROSIDAD_FLDCORRENVIODIGID_GENERATOR")
	@Column(name="FLD_CORRENVIODIG_ID")
	private long fldCorrenviodigId;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHACREACION")
	private Date fldFechacreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAESTADO")
	private Date fldFechaestado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAMODIFICACION")
	private Date fldFechamodificacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHARECEPCION")
	private Date fldFecharecepcion;

	@Column(name="FLD_MEDIOENVIO")
	private BigDecimal fldMedioenvio;

	@Column(name="FLD_REGACLARADOS")
	private BigDecimal fldRegaclarados;

	@Column(name="FLD_REGALTAS")
	private BigDecimal fldRegaltas;

	@Column(name="FLD_REGBAJAS")
	private BigDecimal fldRegbajas;

	@Column(name="FLD_REGBENEFICIOLEY")
	private BigDecimal fldRegbeneficioley;

	@Column(name="FLD_REGBLOQUEADOS")
	private BigDecimal fldRegbloqueados;

	@Column(name="FLD_REGERR")
	private BigDecimal fldRegerr;

	@Column(name="FLD_REGOK")
	private BigDecimal fldRegok;

	@Column(name="FLD_REGPUBLICADOS")
	private BigDecimal fldRegpublicados;

	@Column(name="FLD_REGSTOCKOLD")
	private BigDecimal fldRegstockold;

	@Column(name="FLD_REGUPDATES")
	private BigDecimal fldRegupdates;

	@Column(name="FLD_REGVALIDADOS")
	private BigDecimal fldRegvalidados;

	@Column(name="FLD_REGVENCIDOS")
	private BigDecimal fldRegvencidos;

	@Column(name="FLD_REGVIGENTES")
	private BigDecimal fldRegvigentes;

	@Column(name="FLD_SUMAMONTOS")
	private BigDecimal fldSumamontos;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_TIPOENVIO")
	private BigDecimal fldTipoenvio;

	@Column(name="FLD_TOTALREGDIGITADOS")
	private BigDecimal fldTotalregdigitados;

	@Column(name="FLD_TOTALREGINFORMADOS")
	private BigDecimal fldTotalreginformados;

	@Column(name="FLD_USUARIOCREACION")
	private String fldUsuariocreacion;

	@Column(name="FLD_USUARIOMODIFICACION")
	private String fldUsuariomodificacion;

	@Column(name="FLD_USUARIORESPONSABLE")
	private String fldUsuarioresponsable;

	public TblEnviomorosidad() {
	}

	public long getFldCorrenviodigId() {
		return this.fldCorrenviodigId;
	}

	public void setFldCorrenviodigId(long fldCorrenviodigId) {
		this.fldCorrenviodigId = fldCorrenviodigId;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public Date getFldFechacreacion() {
		return this.fldFechacreacion;
	}

	public void setFldFechacreacion(Date fldFechacreacion) {
		this.fldFechacreacion = fldFechacreacion;
	}

	public Date getFldFechaestado() {
		return this.fldFechaestado;
	}

	public void setFldFechaestado(Date fldFechaestado) {
		this.fldFechaestado = fldFechaestado;
	}

	public Date getFldFechamodificacion() {
		return this.fldFechamodificacion;
	}

	public void setFldFechamodificacion(Date fldFechamodificacion) {
		this.fldFechamodificacion = fldFechamodificacion;
	}

	public Date getFldFecharecepcion() {
		return this.fldFecharecepcion;
	}

	public void setFldFecharecepcion(Date fldFecharecepcion) {
		this.fldFecharecepcion = fldFecharecepcion;
	}

	public BigDecimal getFldMedioenvio() {
		return this.fldMedioenvio;
	}

	public void setFldMedioenvio(BigDecimal fldMedioenvio) {
		this.fldMedioenvio = fldMedioenvio;
	}

	public BigDecimal getFldRegaclarados() {
		return this.fldRegaclarados;
	}

	public void setFldRegaclarados(BigDecimal fldRegaclarados) {
		this.fldRegaclarados = fldRegaclarados;
	}

	public BigDecimal getFldRegaltas() {
		return this.fldRegaltas;
	}

	public void setFldRegaltas(BigDecimal fldRegaltas) {
		this.fldRegaltas = fldRegaltas;
	}

	public BigDecimal getFldRegbajas() {
		return this.fldRegbajas;
	}

	public void setFldRegbajas(BigDecimal fldRegbajas) {
		this.fldRegbajas = fldRegbajas;
	}

	public BigDecimal getFldRegbeneficioley() {
		return this.fldRegbeneficioley;
	}

	public void setFldRegbeneficioley(BigDecimal fldRegbeneficioley) {
		this.fldRegbeneficioley = fldRegbeneficioley;
	}

	public BigDecimal getFldRegbloqueados() {
		return this.fldRegbloqueados;
	}

	public void setFldRegbloqueados(BigDecimal fldRegbloqueados) {
		this.fldRegbloqueados = fldRegbloqueados;
	}

	public BigDecimal getFldRegerr() {
		return this.fldRegerr;
	}

	public void setFldRegerr(BigDecimal fldRegerr) {
		this.fldRegerr = fldRegerr;
	}

	public BigDecimal getFldRegok() {
		return this.fldRegok;
	}

	public void setFldRegok(BigDecimal fldRegok) {
		this.fldRegok = fldRegok;
	}

	public BigDecimal getFldRegpublicados() {
		return this.fldRegpublicados;
	}

	public void setFldRegpublicados(BigDecimal fldRegpublicados) {
		this.fldRegpublicados = fldRegpublicados;
	}

	public BigDecimal getFldRegstockold() {
		return this.fldRegstockold;
	}

	public void setFldRegstockold(BigDecimal fldRegstockold) {
		this.fldRegstockold = fldRegstockold;
	}

	public BigDecimal getFldRegupdates() {
		return this.fldRegupdates;
	}

	public void setFldRegupdates(BigDecimal fldRegupdates) {
		this.fldRegupdates = fldRegupdates;
	}

	public BigDecimal getFldRegvalidados() {
		return this.fldRegvalidados;
	}

	public void setFldRegvalidados(BigDecimal fldRegvalidados) {
		this.fldRegvalidados = fldRegvalidados;
	}

	public BigDecimal getFldRegvencidos() {
		return this.fldRegvencidos;
	}

	public void setFldRegvencidos(BigDecimal fldRegvencidos) {
		this.fldRegvencidos = fldRegvencidos;
	}

	public BigDecimal getFldRegvigentes() {
		return this.fldRegvigentes;
	}

	public void setFldRegvigentes(BigDecimal fldRegvigentes) {
		this.fldRegvigentes = fldRegvigentes;
	}

	public BigDecimal getFldSumamontos() {
		return this.fldSumamontos;
	}

	public void setFldSumamontos(BigDecimal fldSumamontos) {
		this.fldSumamontos = fldSumamontos;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public BigDecimal getFldTipoenvio() {
		return this.fldTipoenvio;
	}

	public void setFldTipoenvio(BigDecimal fldTipoenvio) {
		this.fldTipoenvio = fldTipoenvio;
	}

	public BigDecimal getFldTotalregdigitados() {
		return this.fldTotalregdigitados;
	}

	public void setFldTotalregdigitados(BigDecimal fldTotalregdigitados) {
		this.fldTotalregdigitados = fldTotalregdigitados;
	}

	public BigDecimal getFldTotalreginformados() {
		return this.fldTotalreginformados;
	}

	public void setFldTotalreginformados(BigDecimal fldTotalreginformados) {
		this.fldTotalreginformados = fldTotalreginformados;
	}

	public String getFldUsuariocreacion() {
		return this.fldUsuariocreacion;
	}

	public void setFldUsuariocreacion(String fldUsuariocreacion) {
		this.fldUsuariocreacion = fldUsuariocreacion;
	}

	public String getFldUsuariomodificacion() {
		return this.fldUsuariomodificacion;
	}

	public void setFldUsuariomodificacion(String fldUsuariomodificacion) {
		this.fldUsuariomodificacion = fldUsuariomodificacion;
	}

	public String getFldUsuarioresponsable() {
		return this.fldUsuarioresponsable;
	}

	public void setFldUsuarioresponsable(String fldUsuarioresponsable) {
		this.fldUsuarioresponsable = fldUsuarioresponsable;
	}

}