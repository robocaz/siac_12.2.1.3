angular.module('ccs').factory('ImpresoraResource', function ($resource) {
    var resource = $resource('rest/listado',{
        nocache : new Date().getTime()
    },{
        'cargaImpresoras' :  {  method : 'GET', isArray : true, url: 'rest/listado/impresoras'}
    });
    return resource;
});