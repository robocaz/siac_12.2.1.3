package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysalternates database table.
 * 
 */
@Entity
@Table(name="sysalternates")
@NamedQuery(name="Sysalternate.findAll", query="SELECT s FROM Sysalternate s")
public class Sysalternate implements Serializable {
	private static final long serialVersionUID = 1L;

	private int altsuid;

	private int suid;

	public Sysalternate() {
	}

	public int getAltsuid() {
		return this.altsuid;
	}

	public void setAltsuid(int altsuid) {
		this.altsuid = altsuid;
	}

	public int getSuid() {
		return this.suid;
	}

	public void setSuid(int suid) {
		this.suid = suid;
	}

}