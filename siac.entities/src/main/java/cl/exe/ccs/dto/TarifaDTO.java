package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TarifaDTO extends ResponseDTO implements Serializable{
	
	
	private String codServicio;
	private String glosaServicio;
	private Double costoBase;
	private Date fechTermino;
	
	
	
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public Double getCostoBase() {
		return costoBase;
	}
	public void setCostoBase(Double costoBase) {
		this.costoBase = costoBase;
	}
	public Date getFechTermino() {
		return fechTermino;
	}
	public void setFechTermino(Date fechTermino) {
		this.fechTermino = fechTermino;
	}
	
	

}
