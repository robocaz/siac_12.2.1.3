package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_OrdenTipoEmisorFir database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_OrdenTipoEmisorFir.findAll", query="SELECT t FROM Tbl_OrdenTipoEmisorFir t")
public class Tbl_OrdenTipoEmisorFir implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Orden")
	private byte fld_Orden;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_OrdenTipoEmisorFir() {
	}

	public byte getFld_Orden() {
		return this.fld_Orden;
	}

	public void setFld_Orden(byte fld_Orden) {
		this.fld_Orden = fld_Orden;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}