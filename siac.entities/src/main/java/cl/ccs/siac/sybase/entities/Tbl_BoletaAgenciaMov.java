package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BoletaAgenciaMov database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BoletaAgenciaMov.findAll", query="SELECT t FROM Tbl_BoletaAgenciaMov t")
public class Tbl_BoletaAgenciaMov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CCosto")
	private short fld_CCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_NroBoleta_Id")
	private BigDecimal fld_NroBoleta_Id;

	public Tbl_BoletaAgenciaMov() {
	}

	public short getFld_CCosto() {
		return this.fld_CCosto;
	}

	public void setFld_CCosto(short fld_CCosto) {
		this.fld_CCosto = fld_CCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public BigDecimal getFld_NroBoleta_Id() {
		return this.fld_NroBoleta_Id;
	}

	public void setFld_NroBoleta_Id(BigDecimal fld_NroBoleta_Id) {
		this.fld_NroBoleta_Id = fld_NroBoleta_Id;
	}

}