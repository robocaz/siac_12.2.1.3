package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CertNacimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CertNacimiento.findAll", query="SELECT t FROM Tbl_CertNacimiento t")
public class Tbl_CertNacimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_CERTNACIMIENTO_FLD_CORRCERTIFICADO_ID_GENERATOR", sequenceName="FLD_CORRCERTIFICADO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_CERTNACIMIENTO_FLD_CORRCERTIFICADO_ID_GENERATOR")
	@Column(name="Fld_CorrCertificado_Id")
	private long fld_CorrCertificado_Id;

	@Column(name="Fld_CodUsuarioIngreso")
	private String fld_CodUsuarioIngreso;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_Contenedor")
	private int fld_Contenedor;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_TotalPaginas")
	private byte fld_TotalPaginas;

	public Tbl_CertNacimiento() {
	}

	public long getFld_CorrCertificado_Id() {
		return this.fld_CorrCertificado_Id;
	}

	public void setFld_CorrCertificado_Id(long fld_CorrCertificado_Id) {
		this.fld_CorrCertificado_Id = fld_CorrCertificado_Id;
	}

	public String getFld_CodUsuarioIngreso() {
		return this.fld_CodUsuarioIngreso;
	}

	public void setFld_CodUsuarioIngreso(String fld_CodUsuarioIngreso) {
		this.fld_CodUsuarioIngreso = fld_CodUsuarioIngreso;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public int getFld_Contenedor() {
		return this.fld_Contenedor;
	}

	public void setFld_Contenedor(int fld_Contenedor) {
		this.fld_Contenedor = fld_Contenedor;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public byte getFld_TotalPaginas() {
		return this.fld_TotalPaginas;
	}

	public void setFld_TotalPaginas(byte fld_TotalPaginas) {
		this.fld_TotalPaginas = fld_TotalPaginas;
	}

}