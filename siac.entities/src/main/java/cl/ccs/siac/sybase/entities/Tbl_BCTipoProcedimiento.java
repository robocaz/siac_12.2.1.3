package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_BCTipoProcedimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCTipoProcedimiento.findAll", query="SELECT t FROM Tbl_BCTipoProcedimiento t")
public class Tbl_BCTipoProcedimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoProcedimiento")
	private String fld_GlosaTipoProcedimiento;

	@Column(name="Fld_TipoProcedimiento")
	private byte fld_TipoProcedimiento;

	public Tbl_BCTipoProcedimiento() {
	}

	public String getFld_GlosaTipoProcedimiento() {
		return this.fld_GlosaTipoProcedimiento;
	}

	public void setFld_GlosaTipoProcedimiento(String fld_GlosaTipoProcedimiento) {
		this.fld_GlosaTipoProcedimiento = fld_GlosaTipoProcedimiento;
	}

	public byte getFld_TipoProcedimiento() {
		return this.fld_TipoProcedimiento;
	}

	public void setFld_TipoProcedimiento(byte fld_TipoProcedimiento) {
		this.fld_TipoProcedimiento = fld_TipoProcedimiento;
	}

}