package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolicAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SolicAER.findAll", query="SELECT t FROM Tbl_SolicAER t")
public class Tbl_SolicAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodBanco")
	private String fld_CodBanco;

	@Column(name="Fld_CodComunaSolicitante")
	private int fld_CodComunaSolicitante;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEtapa")
	private short fld_CodEtapa;

	@Column(name="Fld_CodNotaria")
	private String fld_CodNotaria;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrSolic_Id")
	private BigDecimal fld_CorrSolic_Id;

	@Column(name="Fld_DireccionSolicitante")
	private String fld_DireccionSolicitante;

	@Column(name="Fld_FecAbono")
	private Timestamp fld_FecAbono;

	@Column(name="Fld_FecDocum")
	private Timestamp fld_FecDocum;

	@Column(name="Fld_FecElaboracionSolic")
	private Timestamp fld_FecElaboracionSolic;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_Flag_CCS")
	private boolean fld_Flag_CCS;

	@Column(name="Fld_FonoSolicitante")
	private String fld_FonoSolicitante;

	@Column(name="Fld_FormaPagoSolicAER")
	private byte fld_FormaPagoSolicAER;

	@Column(name="Fld_MarcaAutorizNotarial")
	private boolean fld_MarcaAutorizNotarial;

	@Column(name="Fld_MontoAbonado")
	private int fld_MontoAbonado;

	@Column(name="Fld_NomFirma_ApMat")
	private String fld_NomFirma_ApMat;

	@Column(name="Fld_NomFirma_ApPat")
	private String fld_NomFirma_ApPat;

	@Column(name="Fld_NomFirma_Nombres")
	private String fld_NomFirma_Nombres;

	@Column(name="Fld_NomSolicitante_ApMat")
	private String fld_NomSolicitante_ApMat;

	@Column(name="Fld_NomSolicitante_ApPat")
	private String fld_NomSolicitante_ApPat;

	@Column(name="Fld_NomSolicitante_Nombres")
	private String fld_NomSolicitante_Nombres;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroCheque")
	private String fld_NroCheque;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_RazonSocial")
	private String fld_RazonSocial;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoPagoSolicAER")
	private byte fld_TipoPagoSolicAER;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	@Column(name="Fld_TotDocCalc")
	private int fld_TotDocCalc;

	@Column(name="Fld_TotDocSolic")
	private int fld_TotDocSolic;

	public Tbl_SolicAER() {
	}

	public String getFld_CodBanco() {
		return this.fld_CodBanco;
	}

	public void setFld_CodBanco(String fld_CodBanco) {
		this.fld_CodBanco = fld_CodBanco;
	}

	public int getFld_CodComunaSolicitante() {
		return this.fld_CodComunaSolicitante;
	}

	public void setFld_CodComunaSolicitante(int fld_CodComunaSolicitante) {
		this.fld_CodComunaSolicitante = fld_CodComunaSolicitante;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEtapa() {
		return this.fld_CodEtapa;
	}

	public void setFld_CodEtapa(short fld_CodEtapa) {
		this.fld_CodEtapa = fld_CodEtapa;
	}

	public String getFld_CodNotaria() {
		return this.fld_CodNotaria;
	}

	public void setFld_CodNotaria(String fld_CodNotaria) {
		this.fld_CodNotaria = fld_CodNotaria;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrSolic_Id() {
		return this.fld_CorrSolic_Id;
	}

	public void setFld_CorrSolic_Id(BigDecimal fld_CorrSolic_Id) {
		this.fld_CorrSolic_Id = fld_CorrSolic_Id;
	}

	public String getFld_DireccionSolicitante() {
		return this.fld_DireccionSolicitante;
	}

	public void setFld_DireccionSolicitante(String fld_DireccionSolicitante) {
		this.fld_DireccionSolicitante = fld_DireccionSolicitante;
	}

	public Timestamp getFld_FecAbono() {
		return this.fld_FecAbono;
	}

	public void setFld_FecAbono(Timestamp fld_FecAbono) {
		this.fld_FecAbono = fld_FecAbono;
	}

	public Timestamp getFld_FecDocum() {
		return this.fld_FecDocum;
	}

	public void setFld_FecDocum(Timestamp fld_FecDocum) {
		this.fld_FecDocum = fld_FecDocum;
	}

	public Timestamp getFld_FecElaboracionSolic() {
		return this.fld_FecElaboracionSolic;
	}

	public void setFld_FecElaboracionSolic(Timestamp fld_FecElaboracionSolic) {
		this.fld_FecElaboracionSolic = fld_FecElaboracionSolic;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public boolean getFld_Flag_CCS() {
		return this.fld_Flag_CCS;
	}

	public void setFld_Flag_CCS(boolean fld_Flag_CCS) {
		this.fld_Flag_CCS = fld_Flag_CCS;
	}

	public String getFld_FonoSolicitante() {
		return this.fld_FonoSolicitante;
	}

	public void setFld_FonoSolicitante(String fld_FonoSolicitante) {
		this.fld_FonoSolicitante = fld_FonoSolicitante;
	}

	public byte getFld_FormaPagoSolicAER() {
		return this.fld_FormaPagoSolicAER;
	}

	public void setFld_FormaPagoSolicAER(byte fld_FormaPagoSolicAER) {
		this.fld_FormaPagoSolicAER = fld_FormaPagoSolicAER;
	}

	public boolean getFld_MarcaAutorizNotarial() {
		return this.fld_MarcaAutorizNotarial;
	}

	public void setFld_MarcaAutorizNotarial(boolean fld_MarcaAutorizNotarial) {
		this.fld_MarcaAutorizNotarial = fld_MarcaAutorizNotarial;
	}

	public int getFld_MontoAbonado() {
		return this.fld_MontoAbonado;
	}

	public void setFld_MontoAbonado(int fld_MontoAbonado) {
		this.fld_MontoAbonado = fld_MontoAbonado;
	}

	public String getFld_NomFirma_ApMat() {
		return this.fld_NomFirma_ApMat;
	}

	public void setFld_NomFirma_ApMat(String fld_NomFirma_ApMat) {
		this.fld_NomFirma_ApMat = fld_NomFirma_ApMat;
	}

	public String getFld_NomFirma_ApPat() {
		return this.fld_NomFirma_ApPat;
	}

	public void setFld_NomFirma_ApPat(String fld_NomFirma_ApPat) {
		this.fld_NomFirma_ApPat = fld_NomFirma_ApPat;
	}

	public String getFld_NomFirma_Nombres() {
		return this.fld_NomFirma_Nombres;
	}

	public void setFld_NomFirma_Nombres(String fld_NomFirma_Nombres) {
		this.fld_NomFirma_Nombres = fld_NomFirma_Nombres;
	}

	public String getFld_NomSolicitante_ApMat() {
		return this.fld_NomSolicitante_ApMat;
	}

	public void setFld_NomSolicitante_ApMat(String fld_NomSolicitante_ApMat) {
		this.fld_NomSolicitante_ApMat = fld_NomSolicitante_ApMat;
	}

	public String getFld_NomSolicitante_ApPat() {
		return this.fld_NomSolicitante_ApPat;
	}

	public void setFld_NomSolicitante_ApPat(String fld_NomSolicitante_ApPat) {
		this.fld_NomSolicitante_ApPat = fld_NomSolicitante_ApPat;
	}

	public String getFld_NomSolicitante_Nombres() {
		return this.fld_NomSolicitante_Nombres;
	}

	public void setFld_NomSolicitante_Nombres(String fld_NomSolicitante_Nombres) {
		this.fld_NomSolicitante_Nombres = fld_NomSolicitante_Nombres;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public String getFld_NroCheque() {
		return this.fld_NroCheque;
	}

	public void setFld_NroCheque(String fld_NroCheque) {
		this.fld_NroCheque = fld_NroCheque;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public String getFld_RazonSocial() {
		return this.fld_RazonSocial;
	}

	public void setFld_RazonSocial(String fld_RazonSocial) {
		this.fld_RazonSocial = fld_RazonSocial;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoPagoSolicAER() {
		return this.fld_TipoPagoSolicAER;
	}

	public void setFld_TipoPagoSolicAER(byte fld_TipoPagoSolicAER) {
		this.fld_TipoPagoSolicAER = fld_TipoPagoSolicAER;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

	public int getFld_TotDocCalc() {
		return this.fld_TotDocCalc;
	}

	public void setFld_TotDocCalc(int fld_TotDocCalc) {
		this.fld_TotDocCalc = fld_TotDocCalc;
	}

	public int getFld_TotDocSolic() {
		return this.fld_TotDocSolic;
	}

	public void setFld_TotDocSolic(int fld_TotDocSolic) {
		this.fld_TotDocSolic = fld_TotDocSolic;
	}

}