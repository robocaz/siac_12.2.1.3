package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class CuadraturaCentroCostoDTO implements Serializable{
	
	private Long montoResuTransacciones;
	private Long montoResuIngCajera;
	private Long montoResuDiferencia;
	private List<ResultadoPorCCDTO> resultadoCentros;
	private String fechaDesde;
	private String fechaHasta;
	
	
	public Long getMontoResuTransacciones() {
		return montoResuTransacciones;
	}
	public void setMontoResuTransacciones(Long montoResuTransacciones) {
		this.montoResuTransacciones = montoResuTransacciones;
	}
	public Long getMontoResuIngCajera() {
		return montoResuIngCajera;
	}
	public void setMontoResuIngCajera(Long montoResuIngCajera) {
		this.montoResuIngCajera = montoResuIngCajera;
	}
	public Long getMontoResuDiferencia() {
		return montoResuDiferencia;
	}
	public void setMontoResuDiferencia(Long montoResuDiferencia) {
		this.montoResuDiferencia = montoResuDiferencia;
	}
	public List<ResultadoPorCCDTO> getResultadoCentros() {
		return resultadoCentros;
	}
	public void setResultadoCentros(List<ResultadoPorCCDTO> resultadoCentros) {
		this.resultadoCentros = resultadoCentros;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
	
	
	
	
	
	

}
