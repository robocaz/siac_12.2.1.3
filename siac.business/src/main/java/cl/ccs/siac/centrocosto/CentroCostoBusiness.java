package cl.ccs.siac.centrocosto;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface CentroCostoBusiness {
	
	public ResponseListDTO<ComboDTO> listar(String username) throws Exception ;
	
	public ResponseDTO modificar(String username, String codigoCentroCosto) throws Exception;
}
