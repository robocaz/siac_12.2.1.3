package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutCorrRCivil database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutCorrRCivil.findAll", query="SELECT t FROM Tbl_RutCorrRCivil t")
public class Tbl_RutCorrRCivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_RutCorrRCivil() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}