package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PorcentajeIVA database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PorcentajeIVA.findAll", query="SELECT t FROM Tbl_PorcentajeIVA t")
public class Tbl_PorcentajeIVA implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodPeriodo")
	private byte fld_CodPeriodo;

	@Column(name="Fld_FecDesde")
	private Timestamp fld_FecDesde;

	@Column(name="Fld_FecHasta")
	private Timestamp fld_FecHasta;

	@Column(name="Fld_PorcentajeIVA")
	private double fld_PorcentajeIVA;

	public Tbl_PorcentajeIVA() {
	}

	public byte getFld_CodPeriodo() {
		return this.fld_CodPeriodo;
	}

	public void setFld_CodPeriodo(byte fld_CodPeriodo) {
		this.fld_CodPeriodo = fld_CodPeriodo;
	}

	public Timestamp getFld_FecDesde() {
		return this.fld_FecDesde;
	}

	public void setFld_FecDesde(Timestamp fld_FecDesde) {
		this.fld_FecDesde = fld_FecDesde;
	}

	public Timestamp getFld_FecHasta() {
		return this.fld_FecHasta;
	}

	public void setFld_FecHasta(Timestamp fld_FecHasta) {
		this.fld_FecHasta = fld_FecHasta;
	}

	public double getFld_PorcentajeIVA() {
		return this.fld_PorcentajeIVA;
	}

	public void setFld_PorcentajeIVA(double fld_PorcentajeIVA) {
		this.fld_PorcentajeIVA = fld_PorcentajeIVA;
	}

}