package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCDetalleEstado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCDetalleEstado.findAll", query="SELECT t FROM Tbl_BCDetalleEstado t")
public class Tbl_BCDetalleEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrProcedimiento")
	private BigDecimal fld_CorrProcedimiento;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	public Tbl_BCDetalleEstado() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrProcedimiento() {
		return this.fld_CorrProcedimiento;
	}

	public void setFld_CorrProcedimiento(BigDecimal fld_CorrProcedimiento) {
		this.fld_CorrProcedimiento = fld_CorrProcedimiento;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

}