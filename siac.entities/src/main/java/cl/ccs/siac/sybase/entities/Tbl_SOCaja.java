package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SOCaja database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SOCaja.findAll", query="SELECT t FROM Tbl_SOCaja t")
public class Tbl_SOCaja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrCajaSO")
	private BigDecimal fld_CorrCajaSO;

	@Column(name="Fld_CorrOffline")
	private BigDecimal fld_CorrOffline;

	@Column(name="Fld_FecCierreCaja")
	private Timestamp fld_FecCierreCaja;

	@Column(name="Fld_FecInicioCaja")
	private Timestamp fld_FecInicioCaja;

	@Column(name="Fld_TotalBoletas")
	private int fld_TotalBoletas;

	@Column(name="Fld_TotalCalculado")
	private BigDecimal fld_TotalCalculado;

	@Column(name="Fld_TotalFacturas")
	private int fld_TotalFacturas;

	@Column(name="Fld_TotalPagado")
	private BigDecimal fld_TotalPagado;

	@Column(name="Fld_TotMovimientos")
	private int fld_TotMovimientos;

	@Column(name="Fld_TotTransacciones")
	private int fld_TotTransacciones;

	public Tbl_SOCaja() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrCajaSO() {
		return this.fld_CorrCajaSO;
	}

	public void setFld_CorrCajaSO(BigDecimal fld_CorrCajaSO) {
		this.fld_CorrCajaSO = fld_CorrCajaSO;
	}

	public BigDecimal getFld_CorrOffline() {
		return this.fld_CorrOffline;
	}

	public void setFld_CorrOffline(BigDecimal fld_CorrOffline) {
		this.fld_CorrOffline = fld_CorrOffline;
	}

	public Timestamp getFld_FecCierreCaja() {
		return this.fld_FecCierreCaja;
	}

	public void setFld_FecCierreCaja(Timestamp fld_FecCierreCaja) {
		this.fld_FecCierreCaja = fld_FecCierreCaja;
	}

	public Timestamp getFld_FecInicioCaja() {
		return this.fld_FecInicioCaja;
	}

	public void setFld_FecInicioCaja(Timestamp fld_FecInicioCaja) {
		this.fld_FecInicioCaja = fld_FecInicioCaja;
	}

	public int getFld_TotalBoletas() {
		return this.fld_TotalBoletas;
	}

	public void setFld_TotalBoletas(int fld_TotalBoletas) {
		this.fld_TotalBoletas = fld_TotalBoletas;
	}

	public BigDecimal getFld_TotalCalculado() {
		return this.fld_TotalCalculado;
	}

	public void setFld_TotalCalculado(BigDecimal fld_TotalCalculado) {
		this.fld_TotalCalculado = fld_TotalCalculado;
	}

	public int getFld_TotalFacturas() {
		return this.fld_TotalFacturas;
	}

	public void setFld_TotalFacturas(int fld_TotalFacturas) {
		this.fld_TotalFacturas = fld_TotalFacturas;
	}

	public BigDecimal getFld_TotalPagado() {
		return this.fld_TotalPagado;
	}

	public void setFld_TotalPagado(BigDecimal fld_TotalPagado) {
		this.fld_TotalPagado = fld_TotalPagado;
	}

	public int getFld_TotMovimientos() {
		return this.fld_TotMovimientos;
	}

	public void setFld_TotMovimientos(int fld_TotMovimientos) {
		this.fld_TotMovimientos = fld_TotMovimientos;
	}

	public int getFld_TotTransacciones() {
		return this.fld_TotTransacciones;
	}

	public void setFld_TotTransacciones(int fld_TotTransacciones) {
		this.fld_TotTransacciones = fld_TotTransacciones;
	}

}