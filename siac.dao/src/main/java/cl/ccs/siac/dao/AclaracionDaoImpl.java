package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.sybase.entities.Tbl_Firma;
import cl.exe.ccs.dto.AclaracionDTO;
import cl.exe.ccs.dto.BipersonalDTO;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ConsultaCAADTO;
import cl.exe.ccs.dto.DetalleFirmaDTO;
import cl.exe.ccs.dto.FirmanteDTO;
import cl.exe.ccs.dto.MonedaDTO;
import cl.exe.ccs.dto.NumeroConfirmatorioDTO;
import cl.exe.ccs.dto.NumeroFirmasDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.VentaCarteraDTO;

@Named("aclaracionDao")
@Stateless
public class AclaracionDaoImpl implements AclaracionDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;

	private static final Logger LOG = LogManager.getLogger(AclaracionDaoImpl.class);

	public ResponseListDTO<DetalleFirmaDTO> consultaFirmas(DetalleFirmaDTO detalle) {

		ResponseListDTO<DetalleFirmaDTO> respuesta = new ResponseListDTO<DetalleFirmaDTO>();
		try {
			
			LOG.info("Ejecutando SQ_GrillaFirmas_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_GrillaFirmas_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodSucursal", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutFirma", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomFirma_ApPat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomFirma_ApMat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomFirma_Nombres", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RowCount", Integer.class, ParameterMode.OUT);

			if (detalle.getTipoEmisorAux() == null) {
				storedProcedure.setParameter("Fld_TipoEmisor", "");
			} else {
				storedProcedure.setParameter("Fld_TipoEmisor", detalle.getTipoEmisorAux().trim());
			}
			if (detalle.getCodEmisorAux() == null) {
				storedProcedure.setParameter("Fld_CodEmisor", "");
			} else {
				storedProcedure.setParameter("Fld_CodEmisor", detalle.getCodEmisorAux().trim());
			}
			if (detalle.getCodSucursalAux() == null) {
				storedProcedure.setParameter("Fld_CodSucursal", new Long(0));
			} else {
				storedProcedure.setParameter("Fld_CodSucursal", detalle.getCodSucursalAux());
			}
			if (detalle.getFirmante().getRut() == null) {
				storedProcedure.setParameter("Fld_RutFirma", "");
			} else {
				storedProcedure.setParameter("Fld_RutFirma", detalle.getFirmante().getRut());
			}
			if (detalle.getFirmante().getApellidoPaterno() == null) {
				storedProcedure.setParameter("Fld_NomFirma_ApPat", "");
			} else {
				storedProcedure.setParameter("Fld_NomFirma_ApPat", detalle.getFirmante().getApellidoPaterno());
			}
			if (detalle.getFirmante().getApellidoMaterno() == null) {
				storedProcedure.setParameter("Fld_NomFirma_ApMat", "");
			} else {
				storedProcedure.setParameter("Fld_NomFirma_ApMat", detalle.getFirmante().getApellidoMaterno());
			}
			if (detalle.getFirmante().getNombres() == null) {
				storedProcedure.setParameter("Fld_NomFirma_Nombres", "");
			} else {
				storedProcedure.setParameter("Fld_NomFirma_Nombres", detalle.getFirmante().getNombres());
			}
			storedProcedure.setParameter("Fld_Criterio", detalle.getCriterio());

			storedProcedure.execute();

			String cantidad = storedProcedure.getOutputParameterValue("Fld_RowCount").toString();

			List<DetalleFirmaDTO> lista = new ArrayList<DetalleFirmaDTO>();

			List<Object[]> firmas = storedProcedure.getResultList();

			for (Object item : firmas) {
				Object[] datos = (Object[]) item;
				DetalleFirmaDTO grilla = new DetalleFirmaDTO();
				FirmanteDTO firmante = new FirmanteDTO();
				firmante.setRut(datos[0].toString());
				firmante.setApellidoPaterno(datos[1].toString());
				firmante.setApellidoMaterno(datos[2].toString());
				firmante.setNombres(datos[3].toString());
				grilla.setFirmante(firmante);
				grilla.setCargoFirma(datos[4].toString());
				grilla.setGlosaEmisor(datos[5].toString());
				grilla.setGlosaSucursal(datos[6].toString());
				grilla.setFlagCertifAcl(Boolean.parseBoolean(datos[7].toString()));
				grilla.setFecVcto(datos[8].toString());
				grilla.setCorrFirma(Long.parseLong(datos[9].toString()));
				grilla.setCodSucursalAux(Long.parseLong(datos[10].toString()));
				grilla.setTipoEmisorAux(datos[11].toString());
				grilla.setCodEmisorAux(datos[12].toString());
				grilla.setFlagCertifAclEsp(Boolean.parseBoolean(datos[13].toString()));
				grilla.setFlagCertifRectif(Boolean.parseBoolean(datos[14].toString()));
				grilla.setEstado(Integer.parseInt(datos[15].toString()));

				lista.add(grilla);
			}

			respuesta.setLista(lista);
			return respuesta;

		} catch (Exception e) {
			LOG.error("Error en el SP SQ_GrillaFirmas_SW2 " + e.getMessage());
			return respuesta;
		}

	}

	public Tbl_Firma obtieneFirma(Long corrFirma) {

		Tbl_Firma firma = null;

		try {
			String sql = "SELECT a.Fld_CorrFirma, a.Fld_Firma FROM Tbl_Firmas a where Fld_CorrFirma = ?";

			Query query = em.createNativeQuery(sql, Tbl_Firma.class);
			query.setParameter(1, corrFirma);
			firma = (Tbl_Firma) query.getSingleResult();
		} catch (Exception e) {
			LOG.error(e.getMessage());

		}

		return firma;

	}

	public NumeroConfirmatorioDTO confimaNumero(NumeroConfirmatorioDTO numero) {

		try {
			
			
			LOG.info("Ejecutando SQ_NroConfirmatorio_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_NroConfirmatorio_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_NroConfirmatorio", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FolioNro", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrVigNC", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodSucursal", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_NroConfirmatorio", numero.getNroConfirmatorio());
			storedProcedure.setParameter("Fld_FolioNro", numero.getFolioNro());
			storedProcedure.setParameter("Fld_CorrVigNC", numero.getSerie());
			storedProcedure.setParameter("Fld_TipoEmisor", numero.getTipoEmisor());
			storedProcedure.setParameter("Fld_CodEmisor", numero.getCodEmisor());

			storedProcedure.execute();

			if (storedProcedure.getOutputParameterValue("Fld_CodSucursal") != null) {
				numero.setCodSucursal(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CodSucursal").toString()));
			} else {
				numero.setCodSucursal(0L);
			}
			// numero.setCorrVigNC(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CorrVigNC").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Retorno") == null) {
				numero.setIdControl(0);
				numero.setMsgControl("");
			} else {
				numero.setIdControl(
						Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
				numero.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			numero.setIdControl(1);
			LOG.debug("Error en la ejecucion del SP SQ_NroConfirmatorio_SW2 " +  e.getMessage());
		}

		return numero;

	}

	public NumeroFirmasDTO obtieneConfirmatorio(NumeroFirmasDTO numero) {

		try {
			
			LOG.info("Ejecutando SQ_NroFirmasNroConf_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_NroFirmasNroConf_SW2");

			storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);

			System.out.println(numero.getTipoEmisor());
			System.out.println(numero.getCodEmisor());
			System.out.println(numero.getTipoDocAclaracion());
			System.out.println(numero.getTipoDocumento());

			storedProcedure.setParameter(1, numero.getTipoEmisor());
			storedProcedure.setParameter(2, numero.getCodEmisor());
			storedProcedure.setParameter(3, numero.getTipoDocAclaracion());
			if (numero.getTipoDocumento() == null) {
				storedProcedure.setParameter(4, "");
			} else {
				storedProcedure.setParameter(4, numero.getTipoDocumento());
			}

			storedProcedure.execute();

			numero.setNroFirmas(Integer.parseInt(storedProcedure.getOutputParameterValue(5).toString()));
			numero.setUsaNrosConfirmatorios(Integer.parseInt(storedProcedure.getOutputParameterValue(6).toString()));

			numero.setFlagFirmaNroConf(Integer.parseInt(storedProcedure.getOutputParameterValue(7).toString()));
			numero.setFlagUsoOpcional(Integer.parseInt(storedProcedure.getOutputParameterValue(8).toString()));

			numero.setIdControl(0);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejeccion del SP SQ_NroFirmasNroConf_SW2 " + e.getMessage());
			numero.setIdControl(1);

		}

		return numero;
	}

	public AclaracionDTO aceptaAclaracion(AclaracionDTO aclaracion) {

		try {

			LOG.info("Ejecutando SP_Aclara3_SW2");
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			GregorianCalendar gc = new GregorianCalendar();
			df.setCalendar(gc);
			Date date = df.parse(aclaracion.getFechaprotesto());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_Aclara3_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Long.class, ParameterMode.IN);// num
																												// operacion
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", Double.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);

			storedProcedure.registerStoredProcedureParameter("Fld_CodRetAcl", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaRetAcl", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_PtjePareo", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPareo", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoMovimiento", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MarcaAclaracion", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletin", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_PagBoletin", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPublicacion", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NombreLibrador", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodLocPub", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumentoImpago", String.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4DigAux", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMonedaAux", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProtAux", Float.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RowCount", Long.class, ParameterMode.OUT);
			Timestamp timestamp = new java.sql.Timestamp(date.getTime());
			
			LOG.info("Parametros SP SP_Aclara3_SW2");
			LOG.info(aclaracion.getAfectado().getRut());
			LOG.info(aclaracion.getCorrcaja());
			LOG.info(aclaracion.getTipoemisor().getCodigo());
			LOG.info(aclaracion.getEmisor().getCodigo());
			LOG.info(aclaracion.getTipodocumento().getCodigo());
			LOG.info(aclaracion.getNumoperacion());
			LOG.info(new Long(aclaracion.getTipomoneda().getCodMoneda()));
			LOG.info(Double.valueOf(aclaracion.getMonto().trim().replace(",", ".")));
			LOG.info(timestamp);
			
			
			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrCaja", aclaracion.getCorrcaja());
			storedProcedure.setParameter("Fld_TipoEmisor", aclaracion.getTipoemisor().getCodigo());
			storedProcedure.setParameter("Fld_CodEmisor", aclaracion.getEmisor().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumento", aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_NroOper4Dig", aclaracion.getNumoperacion());
			storedProcedure.setParameter("Fld_CodMoneda", new Long(aclaracion.getTipomoneda().getCodMoneda()));
			
			double fMonto = Double.valueOf(aclaracion.getMonto().trim().replace(",", "."));
			storedProcedure.setParameter("Fld_MontoProt", fMonto);

			

			storedProcedure.setParameter("Fld_FecProt", timestamp);

			storedProcedure.execute();

			aclaracion.setCodRetAcl(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CodRetAcl").toString()));
			aclaracion.setGlosaRetAcl(storedProcedure.getOutputParameterValue("Fld_GlosaRetAcl").toString());
			aclaracion.setPtjePareo(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_PtjePareo").toString()));
			aclaracion.setTipoPareo(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TipoPareo").toString()));
			aclaracion.setMontoMovimiento(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_MontoMovimiento").toString()));
			aclaracion.setCorrProt(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CorrProt").toString()));
			aclaracion.setMarcaAclaracion(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_MarcaAclaracion").toString()));
			aclaracion.setNroBoletin(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_NroBoletin").toString()));
			aclaracion.setPagBoletin(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_PagBoletin").toString()));
			aclaracion.setFecPublicacion(df.parse(storedProcedure.getOutputParameterValue("Fld_FecPublicacion").toString()));
			aclaracion.setNombreLibrador(storedProcedure.getOutputParameterValue("Fld_NombreLibrador").toString());
			aclaracion.setCodLocPub(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CodLocPub").toString()));
			aclaracion.setTipoDocumentoImpago(storedProcedure.getOutputParameterValue("Fld_TipoDocumentoImpago").toString());
			aclaracion.setNroOper4DigAux(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_NroOper4DigAux").toString()));
			aclaracion.setCodMonedaAux(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_CodMonedaAux").toString()));
			aclaracion.setMontoProtAux(storedProcedure.getOutputParameterValue("Fld_MontoProtAux").toString());
			aclaracion.setCantFilas(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_RowCount").toString()));

			aclaracion.setIdControl(0);
			return aclaracion;

		} catch (Exception e) {
			e.printStackTrace();
			aclaracion.setIdControl(1);
			aclaracion.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SP_Aclara3_SW2 " + e.getMessage());
			return aclaracion;
		}

	}

	public Integer existeDocumento(AclaracionDTO aclaracion) {

		Integer existe = 0;
		try {

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			GregorianCalendar gc = new GregorianCalendar();
			df.setCalendar(gc);
			Date date = df.parse(aclaracion.getFechaprotesto());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			LOG.info("Ejecutando SQ_ValSiExisteDocTemp_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValSiExisteDocTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Existe", Long.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrCaja", aclaracion.getCorrcaja());
			storedProcedure.setParameter("Fld_TipoEmisor", aclaracion.getTipoemisor().getCodigo());
			storedProcedure.setParameter("Fld_CodEmisor", aclaracion.getEmisor().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumento", aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_NroOper4Dig", aclaracion.getNumoperacion());
			storedProcedure.setParameter("Fld_CodMoneda", new Long(aclaracion.getTipomoneda().getCodMoneda()));
			float fMonto = Float.valueOf(aclaracion.getMonto().trim().replace(",", ".")).floatValue();
			storedProcedure.setParameter("Fld_MontoProt", fMonto);

			Timestamp timestamp = new java.sql.Timestamp(date.getTime());

			storedProcedure.setParameter("Fld_FecProt", timestamp);

			storedProcedure.execute();

			existe = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Existe").toString());

			return existe;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_ValSiExisteDocTemp_SW2 " + e.getMessage());
			return 0;
		}
	}

	public AclaracionDTO aclaracionTemp(AclaracionDTO aclaracion) {

		try {

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			GregorianCalendar gc = new GregorianCalendar();
			df.setCalendar(gc);

			LOG.info("Ejecutando SI_AclaracionTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_AclaracionTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumentoImpago", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", Float.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Long.class, ParameterMode.IN);// num
																												// operacion
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocAclaracion", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPago", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MarcaAclaracion", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoCert", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPareo", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_PtjePareo", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletin", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_PagBoletin", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NombreLibrador", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodLocPub", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrVigNC", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FolioNro", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroConfirmatorio", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_ListaRut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPublicacion", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Titular", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TempCorrAcl", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", aclaracion.getCorrcaja());
			storedProcedure.setParameter("Fld_CodCCosto", Long.parseLong(aclaracion.getCentroCosto().getCodigo()));
			storedProcedure.setParameter("Fld_CodServicio", aclaracion.getCodServicio()); // revisar
																							// va
																							// null
																							// en
																							// candidato
			storedProcedure.setParameter("Fld_RutTramitante", aclaracion.getTramitante().getRut());
			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrProt", aclaracion.getCorrProt());
			storedProcedure.setParameter("Fld_TipoEmisor", aclaracion.getTipoemisor().getCodigo());
			storedProcedure.setParameter("Fld_CodEmisor", aclaracion.getEmisor().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumento", aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumentoImpago", aclaracion.getTipoDocumentoImpago());
			storedProcedure.setParameter("Fld_CodMoneda", new Long(aclaracion.getTipomoneda().getCodMoneda()));
			float fMonto = Float.valueOf(aclaracion.getMonto().trim().replace(",", ".")).floatValue();
			storedProcedure.setParameter("Fld_MontoProt", fMonto);
			System.out.println(aclaracion.getFechaprotesto());
			Date date = df.parse(aclaracion.getFechaprotesto());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			Timestamp timestampProt = new java.sql.Timestamp(date.getTime());
			storedProcedure.setParameter("Fld_FecProt", timestampProt);
			storedProcedure.setParameter("Fld_NroOper4Dig", aclaracion.getNumoperacion());
			storedProcedure.setParameter("Fld_TipoDocAclaracion", aclaracion.getNumerofirmas().getTipoDocAclaracion());

			if (aclaracion.getFechapago() == null || aclaracion.getFechapago()=="") {
				Date datePago = df.parse("1900-01-01");
				Calendar calPago = Calendar.getInstance();
				calPago.setTime(datePago);
				Timestamp timestampPago = new java.sql.Timestamp(datePago.getTime());
				storedProcedure.setParameter("Fld_FecPago", timestampPago);
			} else {
				System.out.println("Fecha de Pago: "+aclaracion.getFechapago());
				String dates="";
				Date datePago = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				if(!(aclaracion.getFechapago().length()>8)){
					dates = formatearFechaString(aclaracion.getFechapago());
					datePago  = formatter.parse(dates);
				}else{
					datePago  = formatter.parse(aclaracion.getFechapago());
				}
				Calendar calPago = Calendar.getInstance();
				calPago.setTime(datePago);
				Timestamp timestampPago = new java.sql.Timestamp(datePago.getTime());
				storedProcedure.setParameter("Fld_FecPago", timestampPago);
			}

			if (aclaracion.getAfectado().getCorrelativo() == null) {
				storedProcedure.setParameter("Fld_CorrNombre", 0L);
			} else {
				storedProcedure.setParameter("Fld_CorrNombre", aclaracion.getAfectado().getCorrelativo());
			}

			storedProcedure.setParameter("Fld_MarcaAclaracion", aclaracion.getMarcaAclaracion());
			if (aclaracion.getTipodocumento().getCodigo().equals("CM")) {
				if (aclaracion.getEstadocuota() == null) {
					storedProcedure.setParameter("Fld_TipoCert", 0L);
				} else {
					storedProcedure.setParameter("Fld_TipoCert", aclaracion.getEstadocuota());
				}

			} else {
				storedProcedure.setParameter("Fld_TipoCert", 0L);
			}

			storedProcedure.setParameter("Fld_TipoPareo", aclaracion.getTipoPareo());
			storedProcedure.setParameter("Fld_PtjePareo", aclaracion.getPtjePareo());
			storedProcedure.setParameter("Fld_NroBoletin", aclaracion.getNroBoletin());
			storedProcedure.setParameter("Fld_PagBoletin", aclaracion.getPagBoletin());
			if (aclaracion.getNombreLibrador() == null || aclaracion.getNombreLibrador().length() == 0) {
				storedProcedure.setParameter("Fld_NombreLibrador", " ");
			} else {
				storedProcedure.setParameter("Fld_NombreLibrador", aclaracion.getNombreLibrador());
			}

			storedProcedure.setParameter("Fld_CodLocPub", aclaracion.getCodLocPub());

			if (aclaracion.getNumeroconfirmatorio() == null || aclaracion.getNumeroconfirmatorio().getSerie() == null) {
				storedProcedure.setParameter("Fld_CorrVigNC", 0L);
			} else {
				storedProcedure.setParameter("Fld_CorrVigNC", aclaracion.getNumeroconfirmatorio().getSerie());
			}
			if (aclaracion.getNumeroconfirmatorio() == null
					|| aclaracion.getNumeroconfirmatorio().getFolioNro() == null) {
				storedProcedure.setParameter("Fld_FolioNro", 0L);
			} else {
				storedProcedure.setParameter("Fld_FolioNro", aclaracion.getNumeroconfirmatorio().getFolioNro());
			}
			if (aclaracion.getNumeroconfirmatorio() == null
					|| aclaracion.getNumeroconfirmatorio().getNroConfirmatorio() == null) {
				storedProcedure.setParameter("Fld_NroConfirmatorio", 0L);
			} else {
				storedProcedure.setParameter("Fld_NroConfirmatorio",
						aclaracion.getNumeroconfirmatorio().getNroConfirmatorio());
			}
			if (aclaracion.getListaRut() == null || aclaracion.getListaRut().length() == 0) {
				storedProcedure.setParameter("Fld_ListaRut", " ");
			} else {
				storedProcedure.setParameter("Fld_ListaRut", aclaracion.getListaRut());
			}

			Timestamp timestampPub = new java.sql.Timestamp(aclaracion.getFecPublicacion().getTime());
			storedProcedure.setParameter("Fld_FecPublicacion", timestampPub);
			storedProcedure.setParameter("Fld_Titular", "T");

			storedProcedure.execute();

			aclaracion.setTempCorrAcl(Long.parseLong(storedProcedure.getOutputParameterValue("Fld_TempCorrAcl").toString()));
			aclaracion.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			aclaracion.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return aclaracion;

		} catch (Exception e) {
			e.printStackTrace();
			aclaracion.setIdControl(1);
			aclaracion.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SI_AclaracionTemp_SW2 " + e.getMessage());
			return aclaracion;
		}

	}

	public List<AclaracionDTO> listaCandidatos(AclaracionDTO aclaracion) {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar gc = new GregorianCalendar();
		df.setCalendar(gc);

		List<AclaracionDTO> candidatos = new ArrayList<AclaracionDTO>();
		try {

			LOG.info("Ejecutando SQ_CandidatosAcl_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CandidatosAcl_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", Float.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RowCount", Long.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getAfectado().getRut());
			storedProcedure.setParameter("Fld_TipoEmisor", aclaracion.getTipoemisor().getCodigo());
			storedProcedure.setParameter("Fld_CodEmisor", aclaracion.getEmisor().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumento", aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_NroOper4Dig", aclaracion.getNumoperacion());
			storedProcedure.setParameter("Fld_CodMoneda", aclaracion.getTipomoneda().getCodMoneda());
			float fMonto = Float.valueOf(aclaracion.getMonto().trim().replace(",", ".")).floatValue();
			storedProcedure.setParameter("Fld_MontoProt", fMonto);


			Date dateProt = df.parse(aclaracion.getFechaprotesto());
			Calendar calProt = Calendar.getInstance();
			calProt.setTime(dateProt);
			Timestamp timestampProt = new java.sql.Timestamp(dateProt.getTime());
			storedProcedure.setParameter("Fld_FecProt", timestampProt);

			storedProcedure.execute();

			Integer cant = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_RowCount").toString());

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {

				AclaracionDTO candidato = new AclaracionDTO();

				ComboDTO emisor = new ComboDTO();
				emisor.setDescripcion(obj[0].toString().trim());
				candidato.setEmisor(emisor);

				ComboDTO tipoDocumento = new ComboDTO();
				tipoDocumento.setCodigo(obj[1].toString().trim());
				candidato.setTipodocumento(tipoDocumento);

				MonedaDTO tipoMoneda = new MonedaDTO();
				tipoMoneda.setGlosaCorta(obj[2].toString().trim());
				tipoMoneda.setCodMoneda(Integer.parseInt(obj[14].toString().trim()));
				candidato.setTipomoneda(tipoMoneda);

				candidato.setMonto(obj[3].toString().trim());
				candidato.setFechaprotesto(obj[4].toString().trim().substring(0, 10));
				candidato.setNumoperacion(Long.parseLong(obj[5].toString().trim()));
				candidato.setNroBoletin(Long.parseLong(obj[6].toString().trim()));
				candidato.setPagBoletin(Long.parseLong(obj[7].toString().trim()));
				candidato.setCostoAclaracion(new BigDecimal(obj[8].toString().trim()));
				candidato.setNombreLibrador(obj[9].toString().trim());
				candidato.setCodLocPub(Long.parseLong(obj[10].toString().trim()));
				candidato.setCorrProt(Long.parseLong(obj[11].toString().trim()));
				candidato.setCodEstado(Long.parseLong(obj[12].toString().trim()));
				candidato.setPtjePareo(Long.parseLong(obj[13].toString().trim()));
				candidato.setTipoDocumentoImpago(obj[15].toString().trim());
				Date datePub = df.parse(obj[16].toString().trim());

				candidato.setFecPublicacion(datePub);
				candidato.setFecPublicacionString(df.format(datePub));
				candidato.setMarcaAclaracion(Long.parseLong(obj[17].toString().trim()));
				candidatos.add(candidato);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_CandidatosAcl_SW2 " + e.getMessage());
		}

		return candidatos;
	}

	public AclaracionDTO desDuplica(AclaracionDTO aclaracion) {

		try {

			LOG.info("Ejecutando SP_DesDuplica_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_DesDuplica_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Criterio", Long.class, ParameterMode.IN);

			storedProcedure.setParameter("Fld_CorrCaja", aclaracion.getCorrcaja());
			storedProcedure.setParameter("Fld_CorrProt", aclaracion.getCorrProt());
			storedProcedure.setParameter("Fld_Criterio", aclaracion.getCriterio());

			storedProcedure.execute();

			aclaracion.setIdControl(0);

			return aclaracion;

		} catch (Exception e) {
			e.printStackTrace();
			aclaracion.setIdControl(1);
			aclaracion.setMsgControl(e.getMessage());
			LOG.error("Error en la ejecucion del SP SP_DesDuplica_SW2 " + e.getMessage());
			return aclaracion;
		}
	}

	public List<AclaracionDTO> buscaCuotasBancoEstado(AclaracionDTO aclaracion) {

		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		GregorianCalendar gc = new GregorianCalendar();
		df.setCalendar(gc);

		List<AclaracionDTO> cuotas = new ArrayList<AclaracionDTO>();
		try {

			LOG.info("Ejecutando SQ_BuscaCMAnt_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_BuscaCMAnt_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPago", Timestamp.class, ParameterMode.IN);

			storedProcedure.setParameter("Fld_CorrProt", aclaracion.getCorrProt());

			Date dateProt = df.parse(aclaracion.getFechaprotesto());
			Calendar calProt = Calendar.getInstance();
			calProt.setTime(dateProt);
			Timestamp timestampProt = new java.sql.Timestamp(dateProt.getTime());
			storedProcedure.setParameter("Fld_FecProt", timestampProt);

			if (aclaracion.getFechapago() == null) {
				Date datePago = df.parse("01011900");
				Calendar calPago = Calendar.getInstance();
				calPago.setTime(datePago);
				Timestamp timestampPago = new java.sql.Timestamp(datePago.getTime());
				storedProcedure.setParameter("Fld_FecPago", timestampPago);
			} else {
				Date datePago = df.parse(aclaracion.getFechapago());
				Calendar calPago = Calendar.getInstance();
				calPago.setTime(datePago);
				Timestamp timestampPago = new java.sql.Timestamp(datePago.getTime());
				storedProcedure.setParameter("Fld_FecPago", timestampPago);
			}

			storedProcedure.execute();

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {

				AclaracionDTO cuota = new AclaracionDTO();

				ComboDTO tipoEmisor = new ComboDTO();
				tipoEmisor.setCodigo(obj[1].toString().trim());
				cuota.setTipoemisor(tipoEmisor);

				ComboDTO emisor = new ComboDTO();
				emisor.setCodigo(obj[16].toString().trim());
				emisor.setDescripcion(obj[2].toString().trim());
				cuota.setEmisor(emisor);

				ComboDTO tipoDocumento = new ComboDTO();
				tipoDocumento.setCodigo(obj[3].toString().trim());
				cuota.setTipodocumento(tipoDocumento);

				MonedaDTO tipoMoneda = new MonedaDTO();
				tipoMoneda.setGlosaCorta(obj[4].toString().trim());
				tipoMoneda.setCodMoneda(Integer.parseInt(obj[18].toString().trim()));
				cuota.setTipomoneda(tipoMoneda);
				Double monto = Double.parseDouble(obj[5].toString().trim());
				cuota.setMonto(monto.toString());
				String fechaProt = obj[6].toString().trim().substring(0, 10).replace("-", "");
				fechaProt = fechaProt.substring(0, 4) + "-" + fechaProt.substring(4, 6) + "-" + fechaProt.substring(6, 8);
				cuota.setFechaprotesto(fechaProt);
				cuota.setNumoperacion(Long.parseLong(obj[7].toString().trim()));
				cuota.setMontoMovimiento(Long.parseLong(obj[8].toString().trim()));
				cuota.setCostoAclaracion(new BigDecimal(obj[9].toString().trim()));
				ComboDTO estado = new ComboDTO();
				estado.setDescripcion(obj[10].toString().trim());
				cuota.setEstadoProtesto(estado);
				cuota.setNroBoletin(Long.parseLong(obj[12].toString().trim()));
				cuota.setPagBoletin(Long.parseLong(obj[13].toString().trim()));
				cuota.setNombreLibrador(obj[14].toString().trim());
				cuota.setCorrProt(Long.parseLong(obj[15].toString().trim()));
				cuota.setMarcaAclaracion(Long.parseLong(obj[17].toString().trim()));
				cuota.setCodLocPub(Long.parseLong(obj[20].toString().trim()));
				cuota.setTipoPareo(Long.parseLong(obj[21].toString().trim()));
				cuota.setPtjePareo(Long.parseLong(obj[22].toString().trim()));
				cuota.setCodEstado(Long.parseLong(obj[16].toString().trim()));
				cuota.setTipoCertificado(Long.parseLong(obj[23].toString().trim()));
				cuotas.add(cuota);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_BuscaCMAnt_SW2 " + e.getMessage());
		}

		return cuotas;
	}

	public AclaracionDTO tieneVentaCartera(AclaracionDTO aclaracion) {

		Integer flag = 0;
		try {

			LOG.info("Ejecutando SQ_ValidaProtVC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ValidaProtVC_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt_Aux", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FlagVC", Integer.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrProt_Aux", aclaracion.getCorrProt());
			storedProcedure.execute();

			flag = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_FlagVC").toString());
			if (flag != null) {
				VentaCarteraDTO venta = new VentaCarteraDTO();
				venta.setFlagVentaCartera(flag);
				aclaracion.setVentaCartera(venta);
			}
			return aclaracion;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_ValidaProtVC_SW2 " + e.getMessage());
			return null;
		}

	}

	public VentaCarteraDTO buscaProtestoVentaCartera(Long corrProt, String tipoEmisor, String emisor) {

		Integer flag = 0;
		try {

			LOG.info("Ejecutando SQ_BuscaProtVC_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_BuscaProtVC_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisorVC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisorVC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FlagVC", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrProt", corrProt);
			storedProcedure.setParameter("Fld_TipoEmisorVC", tipoEmisor);
			storedProcedure.setParameter("Fld_CodEmisorVC", emisor);

			storedProcedure.execute();

			flag = Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_FlagVC").toString());

			VentaCarteraDTO venta = new VentaCarteraDTO();
			venta.setFlagVentaCartera(flag);
			venta.setMensaje(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			return venta;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_BuscaProtVC_SW2 " + e.getMessage());
			return null;
		}

	}

	public ResponseDTO eliminaProtestoTemp(Long corrProt, Long corrCaja) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			LOG.info("Ejecutando SD_EliminaAclTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_EliminaAclTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			storedProcedure.setParameter("Fld_CorrProt", corrProt);

			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return respuesta;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SD_EliminaAclTemp_SW2 " + e.getMessage());
			return null;
		}

	}

	public ResponseDTO guardaBipersonalTemp(BipersonalDTO bipersonal) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			LOG.info("Ejecutando SI_BipAclaracionTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_BipAclaracionTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrAclTemp", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrAclTemp", bipersonal.getCorrAclTemp());
			storedProcedure.setParameter("Fld_RutAfectado", bipersonal.getRut());
			storedProcedure.setParameter("Fld_CorrNombre", bipersonal.getCorrelativo());

			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return respuesta;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SI_BipAclaracionTemp_SW2 " + e.getMessage());
			return null;
		}

	}

	public ResponseListDTO<PersonaDTO> listaAclaraciones(Long corrCaja, String rutTramitante) {

		Integer respuesta = 0;
		String mensaje = "";
		ResponseListDTO<PersonaDTO> response = new ResponseListDTO<PersonaDTO>();

		List<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		try {
			
			LOG.info("Ejecutando SQ_ListaRutAclTemp_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ListaRutAclTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			storedProcedure.setParameter("Fld_RutTramitante", rutTramitante);

			storedProcedure.execute();

			response.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			response.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				PersonaDTO persona = new PersonaDTO();
				persona.setRut(obj[0].toString().trim());
				persona.setNombres(obj[1].toString().trim());
				persona.setApellidoPaterno(obj[2].toString().trim());
				persona.setApellidoMaterno(obj[3].toString().trim());
				persona.setCorrelativo(Long.parseLong(obj[4].toString().trim()));
				personas.add(persona);
			}
			response.setLista(personas);

			return response;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_ListaRutAclTemp_SW2 " + e.getMessage());
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see cl.ccs.siac.dao.AclaracionDao#consultaRutFecha(cl.exe.ccs.dto.
	 * ConsultaCAADTO)
	 */
	public ResponseListDTO<ConsultaCAADTO> consultaRutFecha(ConsultaCAADTO consulta) {
		ResponseListDTO<ConsultaCAADTO> response = new ResponseListDTO<ConsultaCAADTO>();
		List<ConsultaCAADTO> listas = new ArrayList<ConsultaCAADTO>();
		response.setLista(listas);
		try {
			LOG.info("Ejecutando SQ_TransAclsRutFec2_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TransAclsRutFec2_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_FecAclaracion", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_FecAclaracion", new Timestamp(consulta.getFecAclaracion()));
			storedProcedure.setParameter("Fld_RutAfectado", consulta.getRutAfectado());

			storedProcedure.execute();

			response.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				response.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

			if (response.getIdControl() == 0) {
				List<Object[]> rs = storedProcedure.getResultList();
				for (Object[] obj : rs) {
					ConsultaCAADTO resultadoConsulta = new ConsultaCAADTO();

					resultadoConsulta.setCorrCaja(Long.parseLong(obj[0].toString().trim()));
					resultadoConsulta.setNroBoletaFactura(Integer.parseInt(obj[1].toString().trim()));
					resultadoConsulta.setApellidoPaterno(obj[2].toString().trim());
					resultadoConsulta.setApellidoMaterno(obj[3].toString().trim());
					resultadoConsulta.setNombres(obj[4].toString().trim());
					resultadoConsulta.setCorrTransaccion(Long.parseLong(obj[5].toString().trim()));

					listas.add(resultadoConsulta);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_TransAclsRutFec2_SW2 " + e.getMessage());
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * cl.ccs.siac.dao.AclaracionDao#aclaracionesParaCertificado(cl.exe.ccs.dto.
	 * AclaracionDTO)
	 */
	public AclaracionDTO aclaracionesParaCertificado(AclaracionDTO aclaracion) {
		try {
			
			LOG.info("Ejecutando SQ_AclsParaCertif_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_AclsParaCertif_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getTramitante().getRut());
			storedProcedure.setParameter("Fld_TipoDocumento", aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_MontoProt",BigDecimal.valueOf(Double.parseDouble(aclaracion.getMonto())));

			storedProcedure.execute();

			aclaracion.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				aclaracion.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

			if (aclaracion.getIdControl() == 0) {
				List<Object[]> rs = storedProcedure.getResultList();
				List<AclaracionDTO> lista = new ArrayList<AclaracionDTO>();
				for (Object[] obj : rs) {
					AclaracionDTO acl = new AclaracionDTO();

					ComboDTO emisor = new ComboDTO();
					acl.setEmisor(emisor);
					ComboDTO tipoDoc = new ComboDTO();
					tipoDoc.setCodigo(aclaracion.getTipodocumento().getCodigo());
					acl.setTipodocumento(tipoDoc);
					MonedaDTO tipoMoneda = new MonedaDTO();
					acl.setTipomoneda(tipoMoneda);

					acl.setCorrProt(Long.parseLong(obj[0].toString().trim()));
					acl.getEmisor().setDescripcion(obj[1].toString().trim());
					acl.getTipodocumento().setDescripcion(obj[2].toString().trim());
					acl.setTipoDocumentoImpago(obj[3].toString().trim());
					// acl.setNroOper4DigAux(Long.parseLong(obj[4].toString().trim()));
					acl.setNumoperacion(Long.parseLong(obj[4].toString().trim()));
					acl.getTipomoneda().setGlosaCorta(obj[5].toString().trim());
					acl.setMonto(obj[6].toString().trim());
					acl.setNombreLibrador(obj[7].toString().trim());
					acl.setFechaprotesto(obj[8].toString().trim());
					acl.setFechaAclaracion(obj[9].toString().trim());
					acl.setNroBoletin(Long.parseLong(obj[10].toString().trim()));
					acl.setPagBoletin(Long.parseLong(obj[11].toString().trim()));
					acl.setFecPublicacionString(obj[12].toString().trim());
					acl.setMarcaAclaracion(Long.parseLong(obj[13].toString().trim()));
					acl.setTempCorrAcl(Long.parseLong(obj[14].toString().trim()));

					lista.add(acl);
				}
				aclaracion.setCandidatos(lista);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_AclsParaCertif_SW2 " + e.getMessage());
		}

		return aclaracion;
	}

	
	public ResponseDTO validaAclaracionCarro(AclaracionDTO aclaracion) {
		
		
		ResponseDTO respuesta = new ResponseDTO();
		
		try {
			DateFormat df = new SimpleDateFormat("ddMMyyyy");
			GregorianCalendar gc = new GregorianCalendar();
			df.setCalendar(gc);
			Date date = df.parse(aclaracion.getFechaprotesto());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			
			LOG.info("Ejecutando SQ_BuscaAclCaja_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_BuscaAclCaja_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", Double.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			
			
			storedProcedure.setParameter("Fld_RutAfectado", aclaracion.getAfectado().getRut());
			storedProcedure.setParameter("Fld_CorrCaja", aclaracion.getTramitante().getCorrCaja());
			storedProcedure.setParameter("Fld_TipoEmisor",aclaracion.getTipoemisor().getCodigo());
			storedProcedure.setParameter("Fld_CodEmisor",aclaracion.getEmisor().getCodigo());
			storedProcedure.setParameter("Fld_TipoDocumento",aclaracion.getTipodocumento().getCodigo());
			storedProcedure.setParameter("Fld_NroOper4Dig",aclaracion.getNumoperacion());
			storedProcedure.setParameter("Fld_CodMoneda",aclaracion.getTipomoneda().getCodMoneda());
			storedProcedure.setParameter("Fld_MontoProt",Double.parseDouble(aclaracion.getMonto()));

			storedProcedure.setParameter("Fld_FecProt",new Timestamp(date.getTime()));
			
			
			storedProcedure.execute();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}
			

			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error en la ejecucion del SP SQ_BuscaAclCaja_SW2 " + e.getMessage());
		}		
		
		
		return respuesta;
		
	}

	public String formatearFechaString(String fecha) {
		System.out.println("Fecha de Pago2: "+fecha);
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		Date laFecha = new Date();
		try {
			laFecha = formatter.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new SimpleDateFormat("yyyy-MM-dd").format(laFecha);
	}	
	
	
	
	public static void main(String[] args) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		String dateInString = "31122014";

		try {

			Date date = formatter.parse(dateInString);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
