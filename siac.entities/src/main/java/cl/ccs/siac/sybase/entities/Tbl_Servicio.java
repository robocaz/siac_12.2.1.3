package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Servicios database table.
 * 
 */
@Entity
@Table(name="Tbl_Servicios")
@NamedQuery(name="Tbl_Servicio.findAll", query="SELECT t FROM Tbl_Servicio t")
public class Tbl_Servicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_SERVICIOS_FLD_CODSERVICIO_GENERATOR", sequenceName="FLD_CODSERVICIO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_SERVICIOS_FLD_CODSERVICIO_GENERATOR")
	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CodExterno")
	private int fld_CodExterno;

	@Column(name="Fld_FlagMeson")
	private boolean fld_FlagMeson;

	@Column(name="Fld_GlosaLargaServ")
	private String fld_GlosaLargaServ;

	@Column(name="Fld_GlosaServicio")
	private String fld_GlosaServicio;

	@Column(name="Fld_Orden")
	private byte fld_Orden;

	@Column(name="Fld_OrigenProd")
	private byte fld_OrigenProd;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_Tipo")
	private byte fld_Tipo;

	public Tbl_Servicio() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public int getFld_CodExterno() {
		return this.fld_CodExterno;
	}

	public void setFld_CodExterno(int fld_CodExterno) {
		this.fld_CodExterno = fld_CodExterno;
	}

	public boolean getFld_FlagMeson() {
		return this.fld_FlagMeson;
	}

	public void setFld_FlagMeson(boolean fld_FlagMeson) {
		this.fld_FlagMeson = fld_FlagMeson;
	}

	public String getFld_GlosaLargaServ() {
		return this.fld_GlosaLargaServ;
	}

	public void setFld_GlosaLargaServ(String fld_GlosaLargaServ) {
		this.fld_GlosaLargaServ = fld_GlosaLargaServ;
	}

	public String getFld_GlosaServicio() {
		return this.fld_GlosaServicio;
	}

	public void setFld_GlosaServicio(String fld_GlosaServicio) {
		this.fld_GlosaServicio = fld_GlosaServicio;
	}

	public byte getFld_Orden() {
		return this.fld_Orden;
	}

	public void setFld_Orden(byte fld_Orden) {
		this.fld_Orden = fld_Orden;
	}

	public byte getFld_OrigenProd() {
		return this.fld_OrigenProd;
	}

	public void setFld_OrigenProd(byte fld_OrigenProd) {
		this.fld_OrigenProd = fld_OrigenProd;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_Tipo() {
		return this.fld_Tipo;
	}

	public void setFld_Tipo(byte fld_Tipo) {
		this.fld_Tipo = fld_Tipo;
	}

}