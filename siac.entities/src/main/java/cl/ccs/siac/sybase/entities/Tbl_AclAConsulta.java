package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AclAConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AclAConsulta.findAll", query="SELECT t FROM Tbl_AclAConsulta t")
public class Tbl_AclAConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodConsulta")
	private byte fld_CodConsulta;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodOrigenConsulta")
	private byte fld_CodOrigenConsulta;

	@Column(name="Fld_CodResponsable")
	private byte fld_CodResponsable;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrAConsulta_Id")
	private BigDecimal fld_CorrAConsulta_Id;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FiguraBic")
	private String fld_FiguraBic;

	@Column(name="Fld_NroDocs")
	private byte fld_NroDocs;

	@Column(name="Fld_Observacion")
	private String fld_Observacion;

	@Column(name="Fld_ProcAnterior")
	private short fld_ProcAnterior;

	@Column(name="Fld_TipoAclCons")
	private String fld_TipoAclCons;

	public Tbl_AclAConsulta() {
	}

	public byte getFld_CodConsulta() {
		return this.fld_CodConsulta;
	}

	public void setFld_CodConsulta(byte fld_CodConsulta) {
		this.fld_CodConsulta = fld_CodConsulta;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodOrigenConsulta() {
		return this.fld_CodOrigenConsulta;
	}

	public void setFld_CodOrigenConsulta(byte fld_CodOrigenConsulta) {
		this.fld_CodOrigenConsulta = fld_CodOrigenConsulta;
	}

	public byte getFld_CodResponsable() {
		return this.fld_CodResponsable;
	}

	public void setFld_CodResponsable(byte fld_CodResponsable) {
		this.fld_CodResponsable = fld_CodResponsable;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrAConsulta_Id() {
		return this.fld_CorrAConsulta_Id;
	}

	public void setFld_CorrAConsulta_Id(BigDecimal fld_CorrAConsulta_Id) {
		this.fld_CorrAConsulta_Id = fld_CorrAConsulta_Id;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public String getFld_FiguraBic() {
		return this.fld_FiguraBic;
	}

	public void setFld_FiguraBic(String fld_FiguraBic) {
		this.fld_FiguraBic = fld_FiguraBic;
	}

	public byte getFld_NroDocs() {
		return this.fld_NroDocs;
	}

	public void setFld_NroDocs(byte fld_NroDocs) {
		this.fld_NroDocs = fld_NroDocs;
	}

	public String getFld_Observacion() {
		return this.fld_Observacion;
	}

	public void setFld_Observacion(String fld_Observacion) {
		this.fld_Observacion = fld_Observacion;
	}

	public short getFld_ProcAnterior() {
		return this.fld_ProcAnterior;
	}

	public void setFld_ProcAnterior(short fld_ProcAnterior) {
		this.fld_ProcAnterior = fld_ProcAnterior;
	}

	public String getFld_TipoAclCons() {
		return this.fld_TipoAclCons;
	}

	public void setFld_TipoAclCons(String fld_TipoAclCons) {
		this.fld_TipoAclCons = fld_TipoAclCons;
	}

}