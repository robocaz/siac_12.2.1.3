package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_VCDetPareados database table.
 * 
 */
@Entity
@Table(name="Tbl_VCDetPareados")
@NamedQuery(name="Tbl_VCDetPareado.findAll", query="SELECT t FROM Tbl_VCDetPareado t")
public class Tbl_VCDetPareado implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_VCDetPareadoPK id;

	@Column(name="Fld_CodEmisorVen")
	private String fld_CodEmisorVen;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoEmisorVen")
	private String fld_TipoEmisorVen;

	public Tbl_VCDetPareado() {
	}

	public Tbl_VCDetPareadoPK getId() {
		return this.id;
	}

	public void setId(Tbl_VCDetPareadoPK id) {
		this.id = id;
	}

	public String getFld_CodEmisorVen() {
		return this.fld_CodEmisorVen;
	}

	public void setFld_CodEmisorVen(String fld_CodEmisorVen) {
		this.fld_CodEmisorVen = fld_CodEmisorVen;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_TipoEmisorVen() {
		return this.fld_TipoEmisorVen;
	}

	public void setFld_TipoEmisorVen(String fld_TipoEmisorVen) {
		this.fld_TipoEmisorVen = fld_TipoEmisorVen;
	}

}