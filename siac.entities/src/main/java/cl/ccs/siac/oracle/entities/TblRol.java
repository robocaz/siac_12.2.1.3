package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_ROL database table.
 * 
 */
@Entity
@Table(name="TBL_ROL")
@NamedQuery(name="TblRol.findAll", query="SELECT t FROM TblRol t")
public class TblRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ROL_FLDCODIGOROL_GENERATOR", sequenceName="FLD_CODIGOROL")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ROL_FLDCODIGOROL_GENERATOR")
	@Column(name="FLD_CODIGOROL")
	private String fldCodigorol;

	@Column(name="FLD_APLICACION")
	private String fldAplicacion;

	@Column(name="FLD_DESCRIPCIONROL")
	private String fldDescripcionrol;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_MODULOROL")
	private String fldModulorol;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblRol() {
	}

	public String getFldCodigorol() {
		return this.fldCodigorol;
	}

	public void setFldCodigorol(String fldCodigorol) {
		this.fldCodigorol = fldCodigorol;
	}

	public String getFldAplicacion() {
		return this.fldAplicacion;
	}

	public void setFldAplicacion(String fldAplicacion) {
		this.fldAplicacion = fldAplicacion;
	}

	public String getFldDescripcionrol() {
		return this.fldDescripcionrol;
	}

	public void setFldDescripcionrol(String fldDescripcionrol) {
		this.fldDescripcionrol = fldDescripcionrol;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldModulorol() {
		return this.fldModulorol;
	}

	public void setFldModulorol(String fldModulorol) {
		this.fldModulorol = fldModulorol;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}