package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class DetalleBoletaDTO extends ResponseDTO implements Serializable{
	
	
	private Long totalCalculado;
	private Long totalPagado;
	private Long totalDiferencia;
	private Integer totalBoletas;
	private Integer totalFacturas;
	private List<ItemBoletaDTO> items;
	
	
	
	public Long getTotalCalculado() {
		return totalCalculado;
	}
	public void setTotalCalculado(Long totalCalculado) {
		this.totalCalculado = totalCalculado;
	}
	public Long getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(Long totalPagado) {
		this.totalPagado = totalPagado;
	}
	public Long getTotalDiferencia() {
		return totalDiferencia;
	}
	public void setTotalDiferencia(Long totalDiferencia) {
		this.totalDiferencia = totalDiferencia;
	}
	public Integer getTotalBoletas() {
		return totalBoletas;
	}
	public void setTotalBoletas(Integer totalBoletas) {
		this.totalBoletas = totalBoletas;
	}
	public Integer getTotalFacturas() {
		return totalFacturas;
	}
	public void setTotalFacturas(Integer totalFacturas) {
		this.totalFacturas = totalFacturas;
	}
	public List<ItemBoletaDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemBoletaDTO> items) {
		this.items = items;
	}
	
	
	
	

}
