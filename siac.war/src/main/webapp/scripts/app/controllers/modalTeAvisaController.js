angular.module('ccs').controller('TeAvisaController', function($rootScope, $scope, LoginResource, TramitanteResource, TeAvisaResource, PersonaResource, $location, RutHelper, $uibModal, $document) {

  
	$scope.opcion = "";
	
	
	$scope.eleccion = function(){
		console.log($scope.opcion);
		$scope.listaContratos = {};
		if($scope.opcion == "R"){
			angular.element("#bcbusqueda").modal();
		}
		if($scope.opcion == "C"){
			angular.element("#boletincontrato").modal();
		}
	}
	

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;
	$scope.soloLectura = true;
	$scope.afectado = {};
	$scope.boletin = {}
	$scope.contratoSeleccionado = {};
	$scope.contratoSeleccionadoRen = {};
	$scope.tipobusqueda = "";
	$scope.busquedaForm = {
		rut:"",
		tipobusqueda:'',
		correlativo:''
	};
	

	$scope.getTramitante = function(){
		$scope.limpiarAfectado();
	};

	$scope.esRutValido = function(){

		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ){
			return true;
		}else{
			return false;
		}
	};

	$scope.esRutJuridico = function (rut) {
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};

	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		$scope.buscaAfectado("tramitante");

	}

	$scope.limpiarAfectado = function(){
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
        $scope.afectado.nombreafectado = null;
        $scope.esrutjuridico = false;
        $scope.editable = false;
	};

	$scope.buscaAfectado = function (tipoPersona) {
        $scope.limpiarAfectado();
		$scope.esvalido = false;

		if($scope.afectado.rut === undefined ){
			return;
		}
        if($scope.afectado.rut === null ){
			$scope.afectado.rut  = undefined;
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
  				function () {
					$("#rutAfectadoInforme").focus();
				});
			return;
		}

		if($scope.esRutValido()){
			if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
				swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
			}else{
				if(tipoPersona == "tramitante"){
					$scope.obtenerAfectadoTramitante();
				}
				else{
					$scope.obtenerAfectado();
				}
			}
		}else{
            swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'});
			$scope.afectado.rut == null;
		}
	}

	$scope.obtenerAfectado = function(){
		var successCallback = function(data,responseHeaders){
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');	
                return false;
                
			} else {
                $scope.editable = false;
                $scope.soloLectura = true;
				if($scope.esrutjuridico){
					$scope.afectado.nombres = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
					$scope.afectado.correlativo 	= data.correlativo;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
					$scope.afectado.correlativo 	= data.correlativo;
					$scope.validaContrato();
				}
				$('#ap_pat_pag').focus();
			}
		};
        PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
	};


	$scope.obtenerAfectadoTramitante = function(){
		var successCallback = function(data,responseHeaders){
            $scope.esvalido = true;
			if(data.correlativo == "0"){
                $scope.editable = true;
                swal('Alerta','Tramitante no encontrado','error');

			} else {
                $scope.editable = false;
                $scope.soloLectura = true;
				if($scope.esrutjuridico){
					$scope.afectado.nombres = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
					$scope.afectado.correlativo 	= data.correlativo;
				}else{
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
					$scope.afectado.correlativo 	= data.correlativo;
					$scope.validaContrato();
				}
				$('#ap_pat_pag').focus();
			}
		};
        TramitanteResource.carga({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
	};
	
	 $scope.validateEmail = function(emailField, tipo){

	 	if(emailField !== null && emailField !== undefined && emailField !== ""){
	        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	        if (emailField !== "" && reg.test(emailField) == false){
	        	
	        	if(tipo == 'email1'){
	        		
	        		$scope.boletin.email = "";
	        		$("#correo1_bc").focus();
	        		swal('Alerta','Email invalido','error');
	        		return false;
	        	}
	        	if(tipo == 'email2'){
	        		
	        		$scope.boletin.email2 = "";
	        		$("#correo2_bc").focus();
	        		swal('Alerta','Email confirmación invalido','error');
	        		return false;
	        	}
	            
	        }
	        
	        return true;
	    }
	}
	
	 $scope.validateEmailRen = function(emailField, tipo){

	 	if(emailField !== null && emailField !== undefined && emailField !== ""){
	        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	        if (emailField !== "" && reg.test(emailField) == false){
	        	
	        	if(tipo == 'email1'){
	        		
	        		$scope.contratoSeleccionadoRen.email = "";
	        		$("#correo1_bc").focus();
	        		swal('Alerta','Email invalido','error');
	        		return false;
	        	}
	        	if(tipo == 'email2'){
	        		
	        		$scope.contratoSeleccionadoRen.email2 = "";
	        		$("#correo2_bc").focus();
	        		swal('Alerta','Email confirmación invalido','error');
	        		return false;
	        	}
	            
	        }
	        
	        return true;
	    }
	}


	$scope.validarContrato = function(){

		console.log($scope.boletin);
		$scope.boletin.afectado = $scope.afectado;
		$scope.boletin.tramitante = $rootScope.tramitante;
		
		
		
		
		
		if(angular.equals({}, $scope.boletin.afectado)){
			swal('Alerta','Ingrese Rut Afectado','error');
			$timeout(function () {$("#rutAfectadoInforme").focus();});
			return false;
		}

		if(!angular.equals({}, $scope.boletin.afectado)){
			if($scope.esRutJuridico()){
				if($scope.boletin.afectado.nombres === undefined || $scope.boletin.afectado.nombres === ""){
					swal('Alerta','Ingrese Nombre Empresa','error');
					$timeout(function () {$("#nombres_afectado").focus();});
					return false;
				}else{
					$scope.boletin.afectado.apellidoPaterno = $scope.boletin.afectado.nombres.substring(0,25);
					$scope.boletin.afectado.apellidoMaterno = $scope.boletin.afectado.nombres.substring(26,50);
					$scope.boletin.afectado.nombres = $scope.boletin.afectado.nombres.substring(51,100);
				}
			}
					
		}
		
		if(!angular.equals({}, $scope.boletin.afectado)){
			
			if($scope.boletin.afectado.rut === undefined || $scope.boletin.afectado.rut === ""){
				swal('Alerta','Ingrese Rut Afectado','error');
				$timeout(function () {$("#rutAfectadoInforme").focus();});
				return false;				
			}
			if(($scope.boletin.afectado.apellidoPaterno === undefined || $scope.boletin.afectado.apellidoPaterno === "") && !$scope.esRutJuridico()){
				swal('Alerta','Ingrese Apellido Paterno','error');
				$timeout(function () {$("#ap_pat_pag").focus();});
				return false;				
			}
			if(($scope.boletin.afectado.apellidoMaterno === undefined || $scope.boletin.afectado.apellidoMaterno === "")  && !$scope.esRutJuridico()){
				swal('Alerta','Ingrese Apellido Materno','error');
				$timeout(function () {$("#ap_mat_pag").focus();});
				return false;				
			}			
			if(($scope.boletin.afectado.nombres === undefined || $scope.boletin.afectado.nombres === "") && !$scope.esRutJuridico()){
				swal('Alerta','Ingrese Nombre','error');
				$timeout(function () {$("#nombres").focus();});
				return false;				
			}	
		}
		

		if($scope.boletin.direccion === undefined || $scope.boletin.direccion === ""){
			swal('Alerta','Ingrese Dirección','error');
			$timeout(function () {$("#direccion_bc").focus();});
			return false;
		}


		//if($scope.boletin.telfijo === undefined || $scope.boletin.telfijo === ""){
		//	swal('Alerta','Número Teléfono Fijo  invalido','error');
		//	$timeout(function () {$("#telefonof_bc").focus();});
		//	return false;
		//}
		//else 
			if($scope.boletin.telfijo !== undefined && $scope.boletin.telfijo !== ""){

			if($scope.boletin.telfijo.length < 11 ){
				$scope.boletin.telfijo = "";
				swal('Alerta','Número Teléfono Fijo  invalido','error');
				$timeout(function () {$("#telefonof_bc").focus();});
				return false;
			}

		}
		
		if($scope.boletin.telcel === undefined || $scope.boletin.telcel === ""){
			$scope.boletin.telcel = "";
			swal('Alerta','Número Teléfono Móvil invalido','error');
			$timeout(function () {$("#telefonom_bc").focus();});
			return false;			
		}
		else if($scope.boletin.telcel !== undefined && $scope.boletin.telcel !== ""){

			if($scope.boletin.telcel.length < 11 ){
				$scope.boletin.telcel = "";
				swal('Alerta','Número Teléfono Móvil invalido','error');
				$timeout(function () {$("#telefonom_bc").focus();});
				return false;
			}

		}
		
		
		
		
		



		if(!$scope.boletin.servsms && !$scope.boletin.servcorreo){
			swal('Alerta','Debe seleccionar un servicio.','error');
			
			return false;
		}
		

		if($scope.boletin.servsms){
			if($scope.boletin.telcel === undefined || $scope.boletin.telcel === ""){
				swal('Alerta','Ingrese Teléfono Móvil','error');
				$timeout(function () {$("#telefonom_bc").focus();});
				return false;
			}
		}
		


		if($scope.boletin.email === undefined || $scope.boletin.email === ""){
			swal('Alerta','Ingrese Email','error');
			$timeout(function () {$("#correo1_bc").focus();});
			return false;
		}		
		
		if($scope.boletin.email2 === undefined || $scope.boletin.email2 === ""){
			swal('Alerta','Ingrese Email Confirmación','error');
			$timeout(function () {$("#correo2_bc").focus();});
			return false;
		}

		if( ($scope.boletin.email !== undefined && $scope.boletin.email !== "") && ($scope.boletin.email2 !== undefined || $scope.boletin.email2 !== ""))   {

			if($scope.boletin.email !== $scope.boletin.email2){
				swal('Alerta','Email no coinciden','error');
				$scope.boletin.email = "";
				$scope.boletin.email2 = "";
				$("#correo1_bc").focus();
				$timeout(function () {$("#correo1_bc").focus();});
				return false;
			}
		}
		
		
	    if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso && $scope.afectado.rut != $rootScope.tramitante.rut){
	            $scope.authSupervisor();
	        }else{
	            $scope.contratar();
	    }
	}


	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			controller: 'ModalLoginSupervisorController',
			templateUrl: 'views/modal/modalloginsupervisor.html',
			appendTo: angular.element($document[0].querySelector('#boletincontrato')),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){
				  $scope.contratar();
			  }
		  });
	};


	$scope.contratar = function(){
		

		var successCallback = function(data,responseHeaders){
			
			if(data.idControl == 0){
				swal({
					  title: 'Atención',
					  text: "Contrato agregado exitosamente",
					  type: 'success',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						$rootScope.pendienteFlag = true;
						$scope.boletin = {};
						$scope.afectado = {};
						angular.element("#boletincontrato").modal('hide');
						$scope.listarServicioTramitante();
						if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
							swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
						}
					})
			}
			else{
				swal({
					  title: 'Error',
					  text: "Ocurrio un en error en la contratación",
					  type: 'error',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						return false;
					})				
			}
		}

		TeAvisaResource.contratar($scope.boletin, successCallback, $scope.errorCallback);
	}
	
	
	
	$scope.validaContrato = function(){
		
		var successCallback = function(data,responseHeaders){
			console.log(data);
			if(data.idControl != 0){
				swal({
					  title: 'Atención',
					  text: "El Rut ya registra servicio Te Avisa, no se puede realizar nueva venta. Elija opción Renovar",
					  type: 'warning',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						$scope.afectado = {};
						$scope.listarServicioTramitante();
						$scope.$apply();
					})
			}
		}
		
		TeAvisaResource.validaContrato({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
	}


	$scope.tipoDeBusqueda = 0;
	
	$scope.consultaContratos = function(tipo){
		
		$scope.listaContratos = {};
		$scope.tipoDeBusqueda = tipo;
		
		if(tipo === 1){
			$scope.busquedaForm.correlativo = "";
			if($scope.busquedaForm.rut === ""){
				swal('Alerta','Ingrese Rut','error');
				return false;
			}
			if($scope.busquedaForm.tipobusqueda === undefined || $scope.busquedaForm.tipobusqueda === ""){
				swal('Alerta','Seleccione un tipo de busqueda','error');
				return false;
			}	
		}
		if(tipo == 2){
			$scope.busquedaForm.tipobusqueda = 2;
			$scope.busquedaForm.rut = "";
			if($scope.busquedaForm.correlativo === undefined || $scope.busquedaForm.correlativo === "" || $scope.busquedaForm.correlativo === "0"){
				swal('Alerta','Ingrese número de contrato','error');
				return false;				
			}

		}
		if($scope.busquedaForm.correlativo !== "" && $scope.busquedaForm.correlativo !== 0){
			$scope.busquedaForm.tipobusqueda = 2;
			
		}
		if(($scope.busquedaForm.rut !== undefined && $scope.busquedaForm.rut !== "") && $scope.busquedaForm.tipobusqueda !== undefined && $scope.busquedaForm.tipobusqueda !== ""){
			$scope.busquedaForm.correlativo = '0';
		}

		var successCallback = function(data,responseHeaders){
			console.log(data);
			console.log(data.length);
			if(data.length > 0){
				$scope.listaContratos = data;
			}
			else{
				if(tipo == 1){
					swal('Alerta','No existen datos para ese Rut','error');
					return false;
				}
				if(tipo == 2){
					swal('Alerta','No existen datos para ese número de contrato','error');
					return false;					
				}
			}
		}
		
		TeAvisaResource.buscaContratosRenovacion({rut: $scope.busquedaForm.rut, criterio: $scope.busquedaForm.tipobusqueda, correlativo: $scope.busquedaForm.correlativo}, successCallback, $scope.errorCallback);
		
	}


	$scope.buscaUpgrade = function(){
		
		$scope.$emit('logout');

		$scope.listaContratos = {};
		$scope.boletin.tramitante = $rootScope.tramitante;
		
		var successCallback = function(data,responseHeaders){
			console.log(data);
			console.log(data.length);
			if(data.length > 0){
				$scope.listaContratos = data;
				angular.element("#productosvigentes").modal();
			}
			else{
				swal('Alerta','Tramitante no registra productos vigentes','error');
			}
			
		}
		
		TeAvisaResource.buscaContratos({rut: $rootScope.tramitante.rut}, successCallback, $scope.errorCallback);
		
	}
	
	
	$scope.getValor = function(value){
		$scope.contratoSeleccionado = value;
	}

	$scope.renOriginal = {};

	$scope.getValorRen = function(value){
		$scope.renOriginal =  JSON.parse(JSON.stringify( value ));;
		$scope.contratoSeleccionadoRen = value;
	}

	$scope.cargaRenovacion = function(){

		if(angular.equals({}, $scope.contratoSeleccionadoRen)){
			swal('Alerta','Seleccione un contrato','error');
			return false;
		}
		else{
			angular.element("#boletinrenovacion").modal();
		}
	}

	
	$scope.renovar = function(){
		console.log($scope.contratoSeleccionadoRen);
		
		$scope.contratoSeleccionadoRen.tramitante = $rootScope.tramitante;
		
		if($scope.contratoSeleccionadoRen.direccion === undefined || $scope.contratoSeleccionadoRen.direccion === ""){
			swal('Alerta','Ingrese Dirección','error');
			$timeout(function () {$("#direccion_bc").focus();});
			return false;
		}

		if($scope.contratoSeleccionadoRen.telfijo !== undefined && $scope.contratoSeleccionadoRen.telfijo !== ""){

			if($scope.contratoSeleccionadoRen.telfijo.length < 11 ){
				swal('Alerta','Número Teléfono Fijo  invalido','error');
				$timeout(function () {$("#telefonof_bc").focus();});
				return false;
			}

		}
//		if($scope.contratoSeleccionadoRen.telfijo === undefined || $scope.contratoSeleccionadoRen.telfijo === ""){
//
//			swal('Alerta','Ingrese Número Teléfono Fijo','error');
//			$timeout(function () {$("#telefonof_bc").focus();});
//			return false;
//			
//		}
		
		if($scope.contratoSeleccionadoRen.telcel !== undefined && $scope.contratoSeleccionadoRen.telcel !== ""){

			if($scope.contratoSeleccionadoRen.telcel.length < 11 ){
				swal('Alerta','Número Teléfono Móvil invalido','error');
				$timeout(function () {$("#telefonom_bc").focus();});
				return false;
			}

		}
	
		if($scope.contratoSeleccionadoRen.telcel === undefined || $scope.contratoSeleccionadoRen.telcel === ""){
			swal('Alerta','Ingrese Número Teléfono Móvil','error');
			$timeout(function () {$("#telefonom_bc").focus();});
			return false;
		}
		
		
		if($scope.contratoSeleccionadoRen.email === undefined || $scope.contratoSeleccionadoRen.email === ""){
			swal('Alerta','Ingrese Email','error');
			$timeout(function () {$("#correo1_bc").focus();});
			return false;
		}		
		
		if($scope.contratoSeleccionadoRen.email2 === undefined || $scope.contratoSeleccionadoRen.email2 === ""){
			swal('Alerta','Ingrese Email Confirmación','error');
			$timeout(function () {$("#correo2_bc").focus();});
			return false;
		}

		if( ($scope.contratoSeleccionadoRen.email !== undefined && $scope.contratoSeleccionadoRen.email !== "") && ($scope.contratoSeleccionadoRen.email2 !== undefined || $scope.contratoSeleccionadoRen.email2 !== ""))   {

			if($scope.contratoSeleccionadoRen.email !== $scope.contratoSeleccionadoRen.email2){
				swal('Alerta','Email no coinciden','error');
				//$scope.contratoSeleccionadoRen.email = "";
				$scope.contratoSeleccionadoRen.email2 = "";
				$("#correo1_bc").focus();
				$timeout(function () {$("#correo1_bc").focus();});
				return false;
			}
		}

		var successCallback = function(data,responseHeaders){
			
			if(data.idControl == 0){
				swal({
					  title: 'Atención',
					  text: "Contrato agregado exitosamente",
					  type: 'success',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						$rootScope.pendienteFlag = true;
						$scope.boletin = {};
						angular.element("#boletinrenovacion").modal('hide');
						$scope.listarServicioTramitante();
						if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
							swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
						}
				    	if(!angular.equals({}, $scope.listaContratos)){
				    		if($scope.listaContratos !== undefined){
					        	$scope.listaContratos.forEach(function (item, i) {
					                item.contratoSeleccionadoCheck = false;
					            });
				    		}
				    	}

					})
			}
			else{
				swal({
					  title: 'Error',
					  text: "Ocurrio un en error en la contratación",
					  type: 'error',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						return false;
					})				
			}
		}

		TeAvisaResource.contratar($scope.contratoSeleccionadoRen, successCallback, $scope.errorCallback);		
		
		
	}
	
	
	
	$scope.cargaUpgrade = function(){
		console.log($scope.contratoSeleccionado);
		if(angular.equals({}, $scope.contratoSeleccionado)){
			swal('Alerta','Seleccione un contrato.','error');
			return false;
		}
		else{
			
			if($scope.contratoSeleccionado.servcorreo && $scope.contratoSeleccionado.servsms){
				$scope.contratoSeleccionado.checked = false;	

				swal('Alerta','No se puede hacer upgrade al contrato seleccionado','error');
			}
			else{
				angular.element("#upgradeteavisa").modal();
			}
		}
	}
	
	
	$scope.cierraUpgrade = function(){
		$scope.contratoSeleccionado = {};
		angular.element("#upgradeteavisa").modal('hide');		
	}
	
	
	
	$scope.upgradeSeleccionado = function(){
		
		console.log($scope.contratoSeleccionado);
		
		$scope.contratoSeleccionado.tramitante = $rootScope.tramitante;
		$scope.contratoSeleccionado.upgrade = true;
		
		
		

		if($scope.contratoSeleccionado.servcorreo && $scope.contratoSeleccionado.servsms){
			swal('Alerta','No se puede hacer upgrade al contrato seleccionado','error');
			$scope.contratoSeleccionado = {};
			angular.element("#upgradeteavisa").modal('hide');
			return false;
		}


		if($scope.contratoSeleccionado.telcel !== undefined && $scope.contratoSeleccionado.telcel !== ""){

			if($scope.contratoSeleccionado.telcel.length < 11 ){
				swal('Alerta','Número Teléfono Móvil invalido','error');
				$timeout(function () {$("#movil_te_avisa").focus();});
				return false;
			}

		}

		if($scope.contratoSeleccionado.telcel === undefined || $scope.contratoSeleccionado.telcel === ""){

				swal('Alerta','Ingrese Teléfono Móvil invalido','error');
				$timeout(function () {$("#movil_te_avisa").focus();});
				return false;
			

		}
		if($scope.contratoSeleccionado.email === undefined || $scope.contratoSeleccionado.email === ""){
			swal('Alerta','Ingrese Email','error');
			$timeout(function () {$("#correo1_bc").focus();});
			return false;
		}		
		
		if($scope.contratoSeleccionado.email2 === undefined || $scope.contratoSeleccionado.email2 === "" || $scope.contratoSeleccionado.email2 === null){
			swal('Alerta','Ingrese Email Confirmación','error');
			$timeout(function () {$("#correo2_bc").focus();});
			return false;
		}

		if( ($scope.contratoSeleccionado.email !== undefined && $scope.contratoSeleccionado.email !== "") && ($scope.contratoSeleccionado.email2 !== undefined || $scope.contratoSeleccionado.email2 !== ""))   {

			if($scope.contratoSeleccionado.email !== $scope.contratoSeleccionado.email2){
				swal('Alerta','Email no coinciden','error');
				$scope.contratoSeleccionado.email = "";
				$scope.contratoSeleccionado.email2 = "";
				$("#correo1_bc").focus();
				$timeout(function () {$("#correo1_bc").focus();});
				return false;
			}
		}

		var successCallback = function(data,responseHeaders){
			
			if(data.idControl == 0){
				swal({
					  title: 'Atención',
					  text: "Contrato agregado exitosamente",
					  type: 'success',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						$scope.boletin = {};
						
						$rootScope.pendienteFlag = true;
						$scope.listaContratos = {};
						$scope.listarServicioTramitante();
						if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso){
							swal('Atención','Recuerde guardar copia de la Cedúla de Identidad Vigente','warning');
							angular.element("#productosvigentes").modal('hide');
							angular.element("#upgradeteavisa").modal('hide');
						}
					})
			}
			else{
				swal({
					  title: 'Error',
					  text: "Ocurrio un en error en la contratación",
					  type: 'error',
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Aceptar',
					  allowOutsideClick: false
					}).then(function () {
						return false;
					})				
			}
		}

		TeAvisaResource.contratar($scope.contratoSeleccionado, successCallback, $scope.errorCallback);	
		
	}
	
	$scope.validaRut = function(){
		
		if( $scope.busquedaForm.rut === undefined || $scope.busquedaForm.rut === null){
			$scope.busquedaForm.rut = "";
			swal({ title: 'Error', text: 'Ingrese un rut valido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
	  		function () { $("#bc_rut").focus();	});
			return false;
		}
		else if($scope.busquedaForm.rut !== undefined && $scope.busquedaForm.rut !== null && parseInt($scope.busquedaForm.rut) < rutMinimo){
			$scope.busquedaForm.rut = "";
			swal({ title: 'Error', text: 'Ingrese un rut valido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
			function () { $("#bc_rut").focus();	});
			return false;
		}
	}
	
	
	
	$scope.noCeros = function(valor, inputId){
		
		//var s = valor; s = s.replace(/^0+/, ''); alert(s); 
		
		if(valor !== undefined && valor !== null && valor !== ""){
			if(valor.length > 0 && valor.length < 11){
				
				if(inputId === "ren_telefono_fijo"){
					$scope.contratoSeleccionadoRen.telfijo = "";
				}
				if(inputId === "ren_telefono_movil"){
					$scope.contratoSeleccionadoRen.telcel = "";
				}     
				if(inputId === "telefonof_bc"){
				//   	$scope.boletin.telfijo = "";
				}
				if(inputId === "telefonom_bc"){
					$scope.boletin.telcel = "";
				}  
				if(inputId === "movil_te_avisa"){
					$scope.contratoSeleccionado.telcel = "";
				}   
				$("#"+inputId).focus();
				swal({ title: 'Error', text: "Número Invalido", type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
			}
		}
	}
	
	
	$scope.cerrarUpgrade = function(){
		$scope.$emit('logout');
		$scope.listaContratos = {};
		angular.element("#productosvigentes").modal('hide');
	}
	
	
	$scope.cerrarBusqueda = function(){
		$scope.busquedaForm = {};
		$scope.contratoSeleccionadoRen = {};
		angular.element("#bcbusqueda").modal('hide');
	}
	
	
	
	$scope.cerrarRenovacion = function(){
		
		//$scope.contratoSeleccionadoRen.contratoSeleccionadoCheck = {};
    	if(!angular.equals({}, $scope.listaContratos)){
    		if($scope.listaContratos !== undefined){
	        	$scope.listaContratos.forEach(function (item, i) {
	                item.contratoSeleccionadoCheck = false;
	            });
    		}
    	}
    	$scope.contratoSeleccionadoRen = {};
    	$scope.consultaContratos($scope.tipoDeBusqueda);

		angular.element("#boletinrenovacion").modal('hide');
	}	

	$scope.cerrarContrato = function(){
		$scope.boletin = {};
		$scope.afectado = {};
		$scope.esrutjuridico = false;                                                                                                                                                                                                                                                                                       
		angular.element("#boletincontrato").modal('hide');
	}

	
	$rootScope.$on('vigenteEvent', function(event) {
		$scope.buscaUpgrade();
		
	});
});
