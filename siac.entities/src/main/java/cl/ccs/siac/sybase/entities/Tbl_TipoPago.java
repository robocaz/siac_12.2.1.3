package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoPago database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoPago.findAll", query="SELECT t FROM Tbl_TipoPago t")
public class Tbl_TipoPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaPago")
	private String fld_GlosaPago;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoPago")
	private String fld_TipoPago;

	public Tbl_TipoPago() {
	}

	public String getFld_GlosaPago() {
		return this.fld_GlosaPago;
	}

	public void setFld_GlosaPago(String fld_GlosaPago) {
		this.fld_GlosaPago = fld_GlosaPago;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoPago() {
		return this.fld_TipoPago;
	}

	public void setFld_TipoPago(String fld_TipoPago) {
		this.fld_TipoPago = fld_TipoPago;
	}

}