package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BloqueoProtestos database table.
 * 
 */
@Entity
@Table(name="Tbl_BloqueoProtestos")
@NamedQuery(name="Tbl_BloqueoProtesto.findAll", query="SELECT t FROM Tbl_BloqueoProtesto t")
public class Tbl_BloqueoProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEstadoAnt")
	private short fld_CodEstadoAnt;

	@Column(name="Fld_CodProceso")
	private BigDecimal fld_CodProceso;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecExpiracion")
	private Timestamp fld_FecExpiracion;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	public Tbl_BloqueoProtesto() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEstadoAnt() {
		return this.fld_CodEstadoAnt;
	}

	public void setFld_CodEstadoAnt(short fld_CodEstadoAnt) {
		this.fld_CodEstadoAnt = fld_CodEstadoAnt;
	}

	public BigDecimal getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(BigDecimal fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecExpiracion() {
		return this.fld_FecExpiracion;
	}

	public void setFld_FecExpiracion(Timestamp fld_FecExpiracion) {
		this.fld_FecExpiracion = fld_FecExpiracion;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

}