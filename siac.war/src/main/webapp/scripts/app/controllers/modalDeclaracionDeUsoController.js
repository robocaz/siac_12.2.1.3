angular.module('ccs').controller('ModalDeclaracionDeUsoController', function($scope, $rootScope, $location, $filter, RutHelper, TramitanteResource, PagosResource, PersonaResource){

  var ctrl = this;
  $scope.form = {
    tipoRutJuridico: false,
    motivo: {},
    motivos: []
  };
  $scope.tramitante = $rootScope.tramitante;
  $scope.afectado = {}; // rut, apellidoPaterno, apellidoMaterno, nombres, razonSocial
  $scope.servicios = [];

  $scope.correlativoTramitante = "";
  $scope.correlativoAfectado = "";
  
  $scope.cargaTramitante = function() {
    $scope.afectado.rut				= $scope.tramitante.rut;
    $scope.afectado.apellidoPaterno = $scope.tramitante.apellidoPaterno;
    $scope.afectado.apellidoMaterno = $scope.tramitante.apellidoMaterno;
    $scope.afectado.nombres 		= $scope.tramitante.nombres;
    $scope.correlativoTramitante = $scope.tramitante.correlativo;
    $scope.correlativoAfectado = $scope.tramitante.correlativo;
  };

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);
  
  $scope.correlativoAfectado = function(){
		var successCallbackPersona = function(data,responseHeaders){
			
			if(data.correlativo == "0"){
	            swal('Alerta','No se ha actualizado el tramitante','error');
			}
		};
	    PersonaResource.actualizar($scope.afectado, successCallbackPersona, $scope.errorCallback);
  }
  
	$scope.esRutValido = function(){
	
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico >= rutMinimo ){
			return true;
		}else{
			return false;
		}
	};
	
	
  $scope.buscaAfectado = function() {

	  var esValido = $scope.esRutValido();
	  if(esValido){
			if($scope.afectado.rut === null || $scope.afectado.rut.length < 5){
		        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		        $scope.afectado = {};
		        return false;		
			}
			  
		    var rutRaw = $scope.afectado.rut !== '' ? $scope.afectado.rut : angular.element("#rutAfectadoInforme").val();
		    if(rutRaw !== undefined && (rutRaw === null || rutRaw === "") && rutRaw.length < 8){
		        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		        $scope.afectado = {};
		        return false;
		    }
		    else{
		    	$scope.form.tipoRutJuridico = RutHelper.esJuridico(rutRaw);
		    	rutRaw = RutHelper.getRutSP(rutRaw);
		    }
		
		    if( rutRaw !== undefined && (rutRaw === null || rutRaw === "")){
		      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		      return false;
		    }
		
		    if( !RutHelper.validate(rutRaw) ) {
		      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		      return false;
		    }
		
		    var successCallbackPersona = function(data, responseHeaders){
		      if ( data.idControl === 1 ) {
		        swal({ title: 'Alerta', text: 'Persona no encontrada',type: 'warning',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		          function () { $("#rutAfectadoInforme").focus();
		          });
		        return;
		      }
		      $scope.afectado = data;
		      $scope.correlativoTramitante = $scope.tramitante.correlativo;
		      $scope.correlativoAfectado = $scope.afectado.correlativo;
		      if( $scope.form.tipoRutJuridico ) {
		    	$scope.esJuridico = true;
		        $scope.afectado.razonSocial   = $scope.afectado.nombres.trim() + ' ' + $scope.afectado.apellidoPaterno.trim() + ' ' + $scope.afectado.apellidoMaterno.trim();
		      }
		    };
		
		    PersonaResource.obtenerPersona({rut: rutRaw}, successCallbackPersona, $scope.errorCallback);
	  }
	  else{
	        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
	        function () { $("#rutAfectadoInforme").focus();	});
	        $scope.afectado = {};
	        return false;		
	  }
  };

  
  $scope.buscaNombreAfectado = function() {

	  var esValido = $scope.esRutValido();
	  if(esValido){
			if($scope.afectado.rut === null || $scope.afectado.rut.length < 5){
		        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		        $scope.afectado = {};
		        return false;		
			}
			  
		    var rutRaw = $scope.afectado.rut !== '' ? $scope.afectado.rut : angular.element("#rutAfectadoInforme").val();
		    if(rutRaw !== undefined && (rutRaw === null || rutRaw === "") && rutRaw.length < 8){
		        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		        $scope.afectado = {};
		        return false;
		    }
		    else{
		    	$scope.form.tipoRutJuridico = RutHelper.esJuridico(rutRaw);
		    	rutRaw = RutHelper.getRutSP(rutRaw);
		    }
		
		    if( rutRaw !== undefined && (rutRaw === null || rutRaw === "")){
		      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		      return false;
		    }
		
		    if( !RutHelper.validate(rutRaw) ) {
		      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		        function () { $("#rutAfectadoInforme").focus();	});
		      return false;
		    }
		
		    var successCallbackPersona = function(data, responseHeaders){
		      if ( data.idControl === 1 ) {
		        swal({ title: 'Alerta', text: 'Persona no encontrada',type: 'warning',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
		          function () { $("#rutAfectadoInforme").focus();
		          });
		        return;
		      }
		      $scope.afectado = data;
		      $scope.correlativoTramitante = $scope.tramitante.correlativo;
		      $scope.correlativoAfectado = $scope.afectado.correlativo;
		      if( $scope.form.tipoRutJuridico ) {
		        $scope.afectado.razonSocial   = $scope.afectado.nombres.trim() + ' ' + $scope.afectado.apellidoPaterno.trim() + ' ' + $scope.afectado.apellidoMaterno.trim();
		      }
		    };
		
		    PersonaResource.obtenerPersonaNombre({rut: rutRaw, correlativo: $scope.afectado.correlativo}, successCallbackPersona, $scope.errorCallback);
	  }
	  else{
	        swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
	        function () { $("#rutAfectadoInforme").focus();	});
	        $scope.afectado = {};
	        return false;		
	  }
  };
  
  
  $scope.aceptar = function() {
    console.log('aceptar');

    /**
    if( $scope.tramitante.declaracionUso ) {
      $scope.cerrar();
      return;
    }
	**/
    
    var rutRaw = $scope.afectado.rut !== '' ? $scope.afectado.rut : angular.element("#rutAfectadoInforme").val();
    rutRaw = RutHelper.getRutSP(rutRaw);

    // Validaciones
    //if( rutRaw !== undefined && (rutRaw === null || rutRaw === "")){
    if( !RutHelper.validate(rutRaw) ) {
      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { $("#rutAfectadoInforme").focus();	});
      return false;
    }

    if( !RutHelper.validate(rutRaw) ) {
      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { $("#rutAfectadoInforme").focus();	});
      return false;
    }

    if ( !$scope.form.tipoRutJuridico && ( $scope.afectado.apellidoPaterno === '' || $scope.afectado.nombres === '' )) {
      swal({ title: 'Error', text: 'Ingrese un rut válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { $("#ap_pat_pag").focus();	});
      return false;
    }

    if ( $scope.form.tipoRutJuridico && $scope.afectado.razonSocial === '' ) {
      swal({ title: 'Error', text: 'Ingrese una razón social válida',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { $("#razonsocial").focus();	});
      return false;
    }

    if (!$scope.form.tipoRutJuridico && $scope.afectado.apellidoPaterno.trim() === '' ) {
        swal({ title: 'Error', text: 'Ingrese un apellido paterno',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
          function () { $("#ap_pat_pag").focus();	});
        return false;
    } 
    
    if (!$scope.form.tipoRutJuridico && $scope.afectado.apellidoMaterno.trim() === '' ) {
        swal({ title: 'Error', text: 'Ingrese un apellido materno',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
          function () { $("#ap_mat_pag").focus();	});
        return false;
    } 
    
    if (!$scope.form.tipoRutJuridico && $scope.afectado.nombres.trim() === '' ) {
        swal({ title: 'Error', text: 'Ingrese un nombre',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
          function () { $("#nombres").focus();	});
        return false;
    }  
    

    
    
    // Validación de Servicios en la Grilla
    var serviciosNoAutorizados = false;
    if ( $scope.transacciones.length > 0 ) {
      for ( var t=0; t < $scope.transacciones.length; t++ ) {
        if ( $scope.transacciones[t].codServicio !== 'ISA' && $scope.transacciones[t].codServicio !== 'ICT' ) {
          serviciosNoAutorizados = true;
        }
      }
    }
    if ( serviciosNoAutorizados ) {
      swal({ title: 'Error', text: 'Existen en la grilla servicios no autorizados, para el ingreso de Declaración de Uso de la Información.',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { $("#rutAfectadoInforme").focus();	});
      return false;
    }


    if($scope.form.motivo.codigo === undefined || $scope.form.motivo.codigo === "" || $scope.form.motivo.codigo === 0){
      swal({ title: 'Error', text: 'Seleccione un motivo.',type: 'error',  showCloseButton: true, confirmButtonText: 'Cerrar'}).then(
        function () { $("#select_declaracion").focus(); });
      return false;
    }
    // Declaración de Uso
    var tramitanteDeclaracionUso = {
      rutTramitante: RutHelper.getRutSP($rootScope.tramitante.rut),
      rutRequirente: RutHelper.getRutSP(rutRaw),
      corrCaja: $rootScope.usuario.caja,
      tieneDeclaracionUso: true,
      corrTramitante: $scope.correlativoTramitante, 
      corrRequirente: $scope.afectado.corrNombre,
      codMotivo: Number($scope.form.motivo.codigo)
    };
    
    
    var successCallbackDeclaracionUso = function(data, responseHeaders) {
      console.log('successCallbackDeclaracionUso');
      console.log(data);
      
      if( data.idControl !== 0 ) {
        swal({ title: 'Error', text: ( typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró ingresar la Declaración de Uso' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }

      swal({ title: 'Éxito', text: 'Declaración aceptada!', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar'}).then(function () {
        $rootScope.tramitante.declaracionUso = true;
        $rootScope.tramitante.tramitanteDeclaracionUso = tramitanteDeclaracionUso;
        $scope.cerrar();
      });
      //$scope.guardarPersona();
    };
    TramitanteResource.declaracionUso(tramitanteDeclaracionUso, successCallbackDeclaracionUso, $scope.errorCallback);

  };

	$scope.guardarPersona = function(){

		if($scope.form.tipoRutJuridico){
			$scope.afectado.apellidoPaterno = $scope.afectado.razonSocial.substring(0, 25);
			$scope.afectado.apellidoMaterno = $scope.afectado.razonSocial.substring(25, 50);
			$scope.afectado.nombres 		= $scope.afectado.razonSocial.substring(50, 75);
		}


		var successCallback = function(data,responseHeaders){

			if(data.correlativo == "0"){
                swal('Alerta','No se ha actualizado el tramitante','error');
			} else{
				$scope.afectado.corrNombre = data.correlativo;
				$scope.afectado.correlativo = data.correlativo;
				//$scope.guardarServicio();
				$scope.aceptar();
			}
		};
        PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};
	
	
	
  $scope.eliminar = function () {
    /*
      Botón Eliminar: Elimina la declaración de Uso. Muestra mensaje: ¿Está seguro de eliminar la Declaración de Uso? Esto eliminará los servicios ingresados.
      Botón SI: Elimina todos los servicios de la grilla y los datos del Tramitante, para el ingreso de una nueva venta
      Botón NO: no realiza ninguna acción y cierra el mensaje.
    */
    swal({
      title: 'Aviso',
      text: "¿Está seguro de eliminar la Declaración de Uso? Esto eliminará los servicios ingresados.",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(function () {

      var cancelarTransaccion = {
        idCaja: $rootScope.usuario.caja,
        rutTramitante: $scope.tramitante.rut
      };

      var successCallbackCancelarTransaccion = function(data,responseHeaders){
        if(data.idControl == 0){

          $rootScope.tramitante = {};
          $rootScope.disableTram = false;
          $rootScope.rutDisbale = false;
          $rootScope.transacciones = [];

          $scope.tramitante = {};
          $scope.afectado = {};
          $scope.form.motivo = $filter('filter')($scope.form.motivos, {codigo: 1}) [0];
          
          $scope.cerrar();
          $location.path("/atencion");
        }
      };
      PagosResource.cancelaTransaccion(cancelarTransaccion, successCallbackCancelarTransaccion, $scope.errorCallback);

    }, function(dismiss) {
      return false;
    });
  };

  $scope.cerrar = function () {
	  
    angular.element('#declaracion').modal("hide");
  };

  angular.element("#declaracion").on('hide.bs.modal', function () {
    //$scope.form.tipoRutJuridico = false;
    $scope.form.motivo = {};
    $scope.afectado = {};
    $scope.form.tipoRutJuridico = false;
    ctrl.formulario.$setPristine();
    ctrl.formulario.$setUntouched();
    $scope.init();
    $rootScope.$broadcast('onDeclaracionUsoTermina', $scope.tramitante);
  });

  $scope.$on('onDeclaracionUsoInicia', function(event, args) {
	  
    
    console.log('modalDeclaracionDeUsoController - onDeclaracionUsoInicia');
    console.log(args);

    $scope.tramitante = args[0];
    $scope.transacciones = args[1];

    if( $scope.tramitante.declaracionUso ) {
      var obtenerDeclaracionUso = {
        corrCaja: Number($rootScope.usuario.caja)
      };
      var successCallbackObtenerDeclaracionUso = function(data,responseHeaders){
        console.log('successCallbackObtenerDeclaracionUso');
        console.log(data);

        $scope.afectado.rut             = data.rutRequirente;
        $scope.afectado.correlativo     = data.corrRequirente;
        $scope.buscaNombreAfectado();

        $scope.form.motivo              = $filter('filter')($scope.form.motivos, {codigo: data.codMotivo}) [0];
        $scope.form.tipoRutJuridico     = RutHelper.esJuridico($scope.afectado.rut);

        if( $scope.form.tipoRutJuridico ) {
          $scope.afectado.razonSocial   = $scope.afectado.nombres.trim() + ' ' + $scope.afectado.apellidoPaterno.trim() + ' ' + $scope.afectado.apellidoMaterno.trim();
        }
      };
      TramitanteResource.obtenerDeclaracionUso(obtenerDeclaracionUso, successCallbackObtenerDeclaracionUso, $scope.errorCallback);
    }
  });

  $scope.init = function () {
    var successCallbackMotivos = function(data,responseHeaders){
      console.log('successCallbackMotivos');
      console.log(data);

      $scope.form.motivos = data;
      var motivo = {
      	codigo: 0,
      	descripcion : "Seleccione Motivo"
      }
      $scope.form.motivos.push(motivo);
      $scope.form.motivo = $filter('filter')($scope.form.motivos, {codigo: 0}) [4];
    };
    TramitanteResource.cargaMotivos(successCallbackMotivos, $scope.errorCallback);
  };

  $scope.init();

});
