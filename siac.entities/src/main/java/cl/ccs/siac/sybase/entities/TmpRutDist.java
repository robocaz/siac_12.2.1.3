package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmpRutDist database table.
 * 
 */
@Entity
@Table(name="tmpRutDist")
@NamedQuery(name="TmpRutDist.findAll", query="SELECT t FROM TmpRutDist t")
public class TmpRutDist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Boletin")
	private short boletin;

	@Column(name="CantDoc")
	private int cantDoc;

	private int cantRut;

	@Column(name="TipoDoc")
	private String tipoDoc;

	private String tipoRut;

	public TmpRutDist() {
	}

	public short getBoletin() {
		return this.boletin;
	}

	public void setBoletin(short boletin) {
		this.boletin = boletin;
	}

	public int getCantDoc() {
		return this.cantDoc;
	}

	public void setCantDoc(int cantDoc) {
		this.cantDoc = cantDoc;
	}

	public int getCantRut() {
		return this.cantRut;
	}

	public void setCantRut(int cantRut) {
		this.cantRut = cantRut;
	}

	public String getTipoDoc() {
		return this.tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getTipoRut() {
		return this.tipoRut;
	}

	public void setTipoRut(String tipoRut) {
		this.tipoRut = tipoRut;
	}

}