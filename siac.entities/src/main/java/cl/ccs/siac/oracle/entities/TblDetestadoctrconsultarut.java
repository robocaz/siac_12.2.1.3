package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_DETESTADOCTRCONSULTARUT database table.
 * 
 */
@Entity
@Table(name="TBL_DETESTADOCTRCONSULTARUT")
@NamedQuery(name="TblDetestadoctrconsultarut.findAll", query="SELECT t FROM TblDetestadoctrconsultarut t")
public class TblDetestadoctrconsultarut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CORRCONTROLCAR")
	private BigDecimal fldCorrcontrolcar;

	@Column(name="FLD_ESTADO")
	private BigDecimal fldEstado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECESTADO")
	private Date fldFecestado;

	public TblDetestadoctrconsultarut() {
	}

	public BigDecimal getFldCorrcontrolcar() {
		return this.fldCorrcontrolcar;
	}

	public void setFldCorrcontrolcar(BigDecimal fldCorrcontrolcar) {
		this.fldCorrcontrolcar = fldCorrcontrolcar;
	}

	public BigDecimal getFldEstado() {
		return this.fldEstado;
	}

	public void setFldEstado(BigDecimal fldEstado) {
		this.fldEstado = fldEstado;
	}

	public Date getFldFecestado() {
		return this.fldFecestado;
	}

	public void setFldFecestado(Date fldFecestado) {
		this.fldFecestado = fldFecestado;
	}

}