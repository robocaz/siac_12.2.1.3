package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABITmpMorosidad database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABITmpMorosidad.findAll", query="SELECT t FROM Tbl_ABITmpMorosidad t")
public class Tbl_ABITmpMorosidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	public Tbl_ABITmpMorosidad() {
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

}