package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp3 database table.
 * 
 */
@Entity
@Table(name="tmp3")
@NamedQuery(name="Tmp3.findAll", query="SELECT t FROM Tmp3 t")
public class Tmp3 implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal corr;

	private short flag;

	public Tmp3() {
	}

	public BigDecimal getCorr() {
		return this.corr;
	}

	public void setCorr(BigDecimal corr) {
		this.corr = corr;
	}

	public short getFlag() {
		return this.flag;
	}

	public void setFlag(short flag) {
		this.flag = flag;
	}

}