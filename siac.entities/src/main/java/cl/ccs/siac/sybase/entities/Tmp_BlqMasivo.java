package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_BlqMasivo database table.
 * 
 */
@Entity
@Table(name="tmp_BlqMasivo")
@NamedQuery(name="Tmp_BlqMasivo.findAll", query="SELECT t FROM Tmp_BlqMasivo t")
public class Tmp_BlqMasivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Corr")
	private BigDecimal corr;

	@Column(name="CorrProt")
	private BigDecimal corrProt;

	public Tmp_BlqMasivo() {
	}

	public BigDecimal getCorr() {
		return this.corr;
	}

	public void setCorr(BigDecimal corr) {
		this.corr = corr;
	}

	public BigDecimal getCorrProt() {
		return this.corrProt;
	}

	public void setCorrProt(BigDecimal corrProt) {
		this.corrProt = corrProt;
	}

}