package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ClaveTarjBIC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ClaveTarjBIC.findAll", query="SELECT t FROM Tbl_ClaveTarjBIC t")
public class Tbl_ClaveTarjBIC implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_ClaveTarjBICPK id;

	@Column(name="Fld_Corr_Id")
	private BigDecimal fld_Corr_Id;

	@Column(name="Fld_CorrMovimiento")
	private int fld_CorrMovimiento;

	@Column(name="Fld_CorrTransaccion")
	private int fld_CorrTransaccion;

	@Column(name="Fld_Estados")
	private short fld_Estados;

	@Column(name="Fld_FecAct")
	private Timestamp fld_FecAct;

	@Column(name="Fld_FecGeneracion")
	private Timestamp fld_FecGeneracion;

	@Column(name="Fld_NConsultas")
	private int fld_NConsultas;

	@Column(name="Fld_RutActivacion")
	private String fld_RutActivacion;

	@Column(name="Fld_Serie")
	private BigDecimal fld_Serie;

	public Tbl_ClaveTarjBIC() {
	}

	public Tbl_ClaveTarjBICPK getId() {
		return this.id;
	}

	public void setId(Tbl_ClaveTarjBICPK id) {
		this.id = id;
	}

	public BigDecimal getFld_Corr_Id() {
		return this.fld_Corr_Id;
	}

	public void setFld_Corr_Id(BigDecimal fld_Corr_Id) {
		this.fld_Corr_Id = fld_Corr_Id;
	}

	public int getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(int fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public int getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(int fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public short getFld_Estados() {
		return this.fld_Estados;
	}

	public void setFld_Estados(short fld_Estados) {
		this.fld_Estados = fld_Estados;
	}

	public Timestamp getFld_FecAct() {
		return this.fld_FecAct;
	}

	public void setFld_FecAct(Timestamp fld_FecAct) {
		this.fld_FecAct = fld_FecAct;
	}

	public Timestamp getFld_FecGeneracion() {
		return this.fld_FecGeneracion;
	}

	public void setFld_FecGeneracion(Timestamp fld_FecGeneracion) {
		this.fld_FecGeneracion = fld_FecGeneracion;
	}

	public int getFld_NConsultas() {
		return this.fld_NConsultas;
	}

	public void setFld_NConsultas(int fld_NConsultas) {
		this.fld_NConsultas = fld_NConsultas;
	}

	public String getFld_RutActivacion() {
		return this.fld_RutActivacion;
	}

	public void setFld_RutActivacion(String fld_RutActivacion) {
		this.fld_RutActivacion = fld_RutActivacion;
	}

	public BigDecimal getFld_Serie() {
		return this.fld_Serie;
	}

	public void setFld_Serie(BigDecimal fld_Serie) {
		this.fld_Serie = fld_Serie;
	}

}