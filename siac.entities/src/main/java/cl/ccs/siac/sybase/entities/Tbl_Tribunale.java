package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Tribunales database table.
 * 
 */
@Entity
@Table(name="Tbl_Tribunales")
@NamedQuery(name="Tbl_Tribunale.findAll", query="SELECT t FROM Tbl_Tribunale t")
public class Tbl_Tribunale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodTribunal")
	private String fld_CodTribunal;

	@Column(name="Fld_GlosaCortaTribunal")
	private String fld_GlosaCortaTribunal;

	@Column(name="Fld_GlosaTribunal")
	private String fld_GlosaTribunal;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Tribunale() {
	}

	public String getFld_CodTribunal() {
		return this.fld_CodTribunal;
	}

	public void setFld_CodTribunal(String fld_CodTribunal) {
		this.fld_CodTribunal = fld_CodTribunal;
	}

	public String getFld_GlosaCortaTribunal() {
		return this.fld_GlosaCortaTribunal;
	}

	public void setFld_GlosaCortaTribunal(String fld_GlosaCortaTribunal) {
		this.fld_GlosaCortaTribunal = fld_GlosaCortaTribunal;
	}

	public String getFld_GlosaTribunal() {
		return this.fld_GlosaTribunal;
	}

	public void setFld_GlosaTribunal(String fld_GlosaTribunal) {
		this.fld_GlosaTribunal = fld_GlosaTribunal;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}