package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BeneficioCesantiaCalculo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BeneficioCesantiaCalculo.findAll", query="SELECT t FROM Tbl_BeneficioCesantiaCalculo t")
public class Tbl_BeneficioCesantiaCalculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_BeneficioBIC")
	private boolean fld_BeneficioBIC;

	@Column(name="Fld_BeneficioMOL")
	private boolean fld_BeneficioMOL;

	@Column(name="Fld_CampoOriginal")
	private String fld_CampoOriginal;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CorrCalculo_Id")
	private BigDecimal fld_CorrCalculo_Id;

	@Column(name="Fld_Extractado")
	private boolean fld_Extractado;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecBaja")
	private Timestamp fld_FecBaja;

	@Column(name="Fld_FecCalculo")
	private Timestamp fld_FecCalculo;

	@Column(name="Fld_FecFinBeneficio")
	private Timestamp fld_FecFinBeneficio;

	@Column(name="Fld_FecFiniquito")
	private Timestamp fld_FecFiniquito;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_Origen")
	private String fld_Origen;

	@Column(name="Fld_Prorroga")
	private boolean fld_Prorroga;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutEmpleador")
	private String fld_RutEmpleador;

	public Tbl_BeneficioCesantiaCalculo() {
	}

	public boolean getFld_BeneficioBIC() {
		return this.fld_BeneficioBIC;
	}

	public void setFld_BeneficioBIC(boolean fld_BeneficioBIC) {
		this.fld_BeneficioBIC = fld_BeneficioBIC;
	}

	public boolean getFld_BeneficioMOL() {
		return this.fld_BeneficioMOL;
	}

	public void setFld_BeneficioMOL(boolean fld_BeneficioMOL) {
		this.fld_BeneficioMOL = fld_BeneficioMOL;
	}

	public String getFld_CampoOriginal() {
		return this.fld_CampoOriginal;
	}

	public void setFld_CampoOriginal(String fld_CampoOriginal) {
		this.fld_CampoOriginal = fld_CampoOriginal;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrCalculo_Id() {
		return this.fld_CorrCalculo_Id;
	}

	public void setFld_CorrCalculo_Id(BigDecimal fld_CorrCalculo_Id) {
		this.fld_CorrCalculo_Id = fld_CorrCalculo_Id;
	}

	public boolean getFld_Extractado() {
		return this.fld_Extractado;
	}

	public void setFld_Extractado(boolean fld_Extractado) {
		this.fld_Extractado = fld_Extractado;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecBaja() {
		return this.fld_FecBaja;
	}

	public void setFld_FecBaja(Timestamp fld_FecBaja) {
		this.fld_FecBaja = fld_FecBaja;
	}

	public Timestamp getFld_FecCalculo() {
		return this.fld_FecCalculo;
	}

	public void setFld_FecCalculo(Timestamp fld_FecCalculo) {
		this.fld_FecCalculo = fld_FecCalculo;
	}

	public Timestamp getFld_FecFinBeneficio() {
		return this.fld_FecFinBeneficio;
	}

	public void setFld_FecFinBeneficio(Timestamp fld_FecFinBeneficio) {
		this.fld_FecFinBeneficio = fld_FecFinBeneficio;
	}

	public Timestamp getFld_FecFiniquito() {
		return this.fld_FecFiniquito;
	}

	public void setFld_FecFiniquito(Timestamp fld_FecFiniquito) {
		this.fld_FecFiniquito = fld_FecFiniquito;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public String getFld_Origen() {
		return this.fld_Origen;
	}

	public void setFld_Origen(String fld_Origen) {
		this.fld_Origen = fld_Origen;
	}

	public boolean getFld_Prorroga() {
		return this.fld_Prorroga;
	}

	public void setFld_Prorroga(boolean fld_Prorroga) {
		this.fld_Prorroga = fld_Prorroga;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutEmpleador() {
		return this.fld_RutEmpleador;
	}

	public void setFld_RutEmpleador(String fld_RutEmpleador) {
		this.fld_RutEmpleador = fld_RutEmpleador;
	}

}