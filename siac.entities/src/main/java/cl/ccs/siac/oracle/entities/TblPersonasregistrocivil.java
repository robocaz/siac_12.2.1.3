package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_PERSONASREGISTROCIVIL database table.
 * 
 */
@Entity
@Table(name="TBL_PERSONASREGISTROCIVIL")
@NamedQuery(name="TblPersonasregistrocivil.findAll", query="SELECT t FROM TblPersonasregistrocivil t")
public class TblPersonasregistrocivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_CORRNOMBRE")
	private BigDecimal fldCorrnombre;

	@Column(name="FLD_ESTADOCIVIL")
	private String fldEstadocivil;

	@Column(name="FLD_FECACTUALIZACION")
	private Timestamp fldFecactualizacion;

	@Column(name="FLD_FECCARGA")
	private Timestamp fldFeccarga;

	@Column(name="FLD_FECDEFUNCION")
	private Timestamp fldFecdefuncion;

	@Column(name="FLD_FECMATRIMONIO")
	private Timestamp fldFecmatrimonio;

	@Column(name="FLD_FECNACIMIENTO")
	private Timestamp fldFecnacimiento;

	@Column(name="FLD_NACIONALIDAD")
	private String fldNacionalidad;

	@Column(name="FLD_RUTPERSONA")
	private String fldRutpersona;

	@Column(name="FLD_SEXO_GENERO")
	private String fldSexoGenero;

	public TblPersonasregistrocivil() {
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public BigDecimal getFldCorrnombre() {
		return this.fldCorrnombre;
	}

	public void setFldCorrnombre(BigDecimal fldCorrnombre) {
		this.fldCorrnombre = fldCorrnombre;
	}

	public String getFldEstadocivil() {
		return this.fldEstadocivil;
	}

	public void setFldEstadocivil(String fldEstadocivil) {
		this.fldEstadocivil = fldEstadocivil;
	}

	public Timestamp getFldFecactualizacion() {
		return this.fldFecactualizacion;
	}

	public void setFldFecactualizacion(Timestamp fldFecactualizacion) {
		this.fldFecactualizacion = fldFecactualizacion;
	}

	public Timestamp getFldFeccarga() {
		return this.fldFeccarga;
	}

	public void setFldFeccarga(Timestamp fldFeccarga) {
		this.fldFeccarga = fldFeccarga;
	}

	public Timestamp getFldFecdefuncion() {
		return this.fldFecdefuncion;
	}

	public void setFldFecdefuncion(Timestamp fldFecdefuncion) {
		this.fldFecdefuncion = fldFecdefuncion;
	}

	public Timestamp getFldFecmatrimonio() {
		return this.fldFecmatrimonio;
	}

	public void setFldFecmatrimonio(Timestamp fldFecmatrimonio) {
		this.fldFecmatrimonio = fldFecmatrimonio;
	}

	public Timestamp getFldFecnacimiento() {
		return this.fldFecnacimiento;
	}

	public void setFldFecnacimiento(Timestamp fldFecnacimiento) {
		this.fldFecnacimiento = fldFecnacimiento;
	}

	public String getFldNacionalidad() {
		return this.fldNacionalidad;
	}

	public void setFldNacionalidad(String fldNacionalidad) {
		this.fldNacionalidad = fldNacionalidad;
	}

	public String getFldRutpersona() {
		return this.fldRutpersona;
	}

	public void setFldRutpersona(String fldRutpersona) {
		this.fldRutpersona = fldRutpersona;
	}

	public String getFldSexoGenero() {
		return this.fldSexoGenero;
	}

	public void setFldSexoGenero(String fldSexoGenero) {
		this.fldSexoGenero = fldSexoGenero;
	}

}