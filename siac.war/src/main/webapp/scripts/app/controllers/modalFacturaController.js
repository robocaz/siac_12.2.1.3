angular.module('ccs').controller('ModalFacturaController', function($scope, $rootScope, $filter, FacturaResource, TramitanteResource, FacturaUtil, RutHelper){

  var ctrl = this;
  $scope.form = {};
  $scope.tramitante = {};
  $scope.transacciones = [];
  $scope.afectados = [];
  $scope.facturas = [];
  $scope.regiones = [];
  $scope.comunas = [];
  $scope.esAgencia = $rootScope.usuario.esAgencia;
  $scope.iva;

  var rutMinimo = parseInt(1000);

  /*
   * esRutValido
   * Método que valida un Rut
   */
  $scope.esRutValido = function(rut){
    rutNumerico = rut.indexOf("-") === -1 ? rut.substring(0, rut.length-1) : rut.substring(0, rut.indexOf("-"));
    rutNumerico = rutNumerico.indexOf(".") !== -1 ? rutNumerico.replace(/\./g, "") : rutNumerico;
    if(RutHelper.validate(rut.replace(/\./g, "")) && rutNumerico > rutMinimo ){
      return true;
    }else{
      return false;
    }
  };

  $scope.formulario = function (form) {
    console.log(form);
  };

  $scope.onTipoCambia = function () {
    console.log('onTipoCambia('+$scope.form.tipo+')');
    $scope.form.afectado = [];
    if( $scope.form.tipo === 'Tercero' ) {
      var factura = {esNueva: true};

      if ( $scope.facturas.length > 0 ) {
        factura = $scope.facturas[0];
      }

      if ( factura.esNueva && $scope.transacciones.length > 0 ) {
        factura.transacciones = [];
        for (var i=0; i < $scope.transacciones.length; i++) {
          factura = $scope.agregarTransaccionFactura(factura, $scope.transacciones[i]);
        }
      }

      factura = $scope.recalcularFactura(factura);

      console.log(factura);
      $scope.form.factura = factura;
    }else if( $scope.form.tipo === 'Rut' ) {
      $scope.form.afectado = [$scope.afectados[0]];
      $scope.onAfectadoCambia();
    }
  };

  $scope.onAfectadoCambia = function () {
    console.log('onSeleccionaAfectado('+$scope.form.afectado+')');
    console.log(FacturaUtil.getTipoPersonaRut($scope.form.afectado[0]));
    if( $scope.form.tipo === 'Rut' ) {
      var factura = {esNueva: true};

      if ( $scope.facturas.length > 0 ) {
        for (var i=0; i < $scope.facturas.length; i++) {
          if ( $scope.facturas[i].rutAfectado === $scope.form.afectado[0] ) {
            factura = $scope.facturas[i];
            factura.esNueva = false;
          }
        }
      }

      if ( /*factura.esNueva &&*/ $scope.transacciones.length > 0 ) {
        factura.transacciones = [];
        for (var i=0; i < $scope.transacciones.length; i++) {
          if ( $scope.form.afectado[0] === $scope.transacciones[i].rutAfectado ) {
        	if( $scope.form.factura.tipoPersona === 'J' ) {
        		$scope.form.factura.razonSocial = factura.razonSocial;
        	}else{
        		factura.razonSocial = $scope.transacciones[i].nombres.trim() + ' ' + $scope.transacciones[i].apellidoPaterno.trim() + ' ' + $scope.transacciones[i].apellidoMaterno.trim();
        	}
            factura.rutFacturacion = $scope.form.afectado[0];
            
            factura = $scope.agregarTransaccionFactura(factura, $scope.transacciones[i]);
          }
        }
      }

      // OC
      $scope.form.oc = false;
      if ( !factura.esNueva && factura.ordenCompra !== '' ) {
        $scope.form.oc = true;
      }

      // Region
      factura.region = $filter('filter')($scope.regiones, {codigo: factura.codRegion}) [0];
      var successCallback = function(data,responseHeaders){
        console.log('comunas...');
        $scope.comunas = data;
        factura.comuna = $filter('filter')($scope.comunas, {codigo: factura.codComuna}) [0];
      }
      TramitanteResource.cargaComunas({idRegion: factura.region.codigo}, successCallback, $scope.errorCallback);

      factura = $scope.recalcularFactura(factura);

      console.log(factura);
      $scope.form.factura = factura;

      $scope.getTipoPersonaRut($scope.form.factura.rutFacturacion);

      if ( $scope.form.factura.esNueva ) {
        $scope.getDatosRut();
      }else{
        if( $scope.form.factura.tipoPersona === 'J' ) {
          $scope.form.factura.razonSocial = factura.razonSocial;
        }else{
          $scope.form.factura.nombres = factura.tramitante.nombres;
          $scope.form.factura.apellidoPaterno = factura.tramitante.apellidoPaterno;
          $scope.form.factura.apellidoMaterno = factura.tramitante.apellidoMaterno;
        }
      }
    }
  };

  $scope.agregarTransaccionFactura = function (factura, transac) {
    var transaccion = JSON.parse(JSON.stringify(transac));
    transaccion.costoServicio = transaccion.costoServicio;
    if( typeof factura.transacciones !== 'undefined' && !Array.isArray(factura.transacciones) ) {
      factura.transacciones = [];
    }
    if ( factura.transacciones.length > 0 ) {
      for (var i=0; i < factura.transacciones.length; i++) {
        if ( factura.transacciones[i].codServicio == transaccion.codServicio ) {
          factura.transacciones[i].cantidadServicio += 1;
          return factura;
        }
      }
    }
    transaccion.cantidadServicio = 1;
    factura.transacciones.push(transaccion);
    return factura;
  };

  $scope.recalcularFactura = function (factura) {
    factura.montoBruto = 0;
    factura.iva = 0;
    factura.total = 0;
    var totalCosto = 0;
    if ( factura.transacciones.length > 0 ) {
      for (var i=0; i < factura.transacciones.length; i++) {
        totalCosto += factura.transacciones[i].cantidadServicio * factura.transacciones[i].costoServicio;
      }
    }

      factura.iva =   totalCosto - (totalCosto > 0 ? Math.round(totalCosto / (1 + $scope.iva / 100)) : 0);
      factura.montoBruto = totalCosto - factura.iva;
     
     factura.total = totalCosto;
    //factura.montoBruto = factura.montoBruto - factura.iva;

    return factura;
  };

  $scope.onOrdenCompraCambia = function () {
    if( !$scope.form.oc ) {
      $scope.form.factura.ordenCompra = '';
    }
  };

  /*
   * onRutCambia
   * Método que valida el Rut al perder el foco
   */
  $scope.onRutCambia = function () {
    console.log('getDatosRut');
    console.log($scope.form.factura.rutFacturacion);

    // si no es válido se alerta al usuario, de lo contrario busca información en procedimiento almacenado
    if( typeof($scope.form.factura.rutFacturacion) === 'undefined' || $scope.form.factura.rutFacturacion === null || $scope.form.factura.rutFacturacion.trim() === '' || !$scope.esRutValido($scope.form.factura.rutFacturacion) ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }

    $scope.getDatosRut();
  };

  /*
   * getDatosRut
   * Método que obtiene información del rut en procedimiento almacenado
   */
  $scope.getDatosRut = function () {
    console.log('getDatosRut');
    $scope.getTipoPersonaRut($scope.form.factura.rutFacturacion);
    $scope.form.factura.rutFacturacion = RutHelper.format($scope.form.factura.rutFacturacion);
    console.log($scope.form.factura.rutFacturacion);

    var successCallback = function(data, responseHeaders){
      console.log(data);

      /*if( data.idControl !== 0 ) {
        swal({ title: 'Error', text: ( data.msgControl === ' ' ? 'No se logró obtener datos rut!' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
        return;
      }*/

      if( $scope.form.factura.tipoPersona === 'J' ) {
        $scope.form.factura.razonSocial = data.razonSocial;
      }else{
        $scope.form.factura.nombres = data.tramitante.nombres;
        $scope.form.factura.apellidoPaterno = data.tramitante.apellidoPaterno;
        $scope.form.factura.apellidoMaterno = data.tramitante.apellidoMaterno;
      }
      $scope.form.factura.giro = data.giro;
      $scope.form.factura.direccion = data.direccion;
      $scope.form.factura.codRegion = data.codRegion;
      $scope.form.factura.codComuna = data.codComuna;

      if( typeof($scope.form.factura.codRegion) !== 'undefined' && $scope.form.factura.codRegion !== null && $scope.form.factura.codRegion !== 0 ) {
        $scope.form.factura.region = $filter('filter')($scope.regiones, {codigo: $scope.form.factura.codRegion}) [0];
        var successCallbackComunas = function(data, responseHeaders){
          $scope.comunas = data;
          if( typeof($scope.form.factura.codComuna) !== 'undefined' && $scope.form.factura.codComuna !== null && $scope.form.factura.codComuna !== 0 ) {
            $scope.form.factura.comuna = $filter('filter')($scope.comunas, {codigo: $scope.form.factura.codComuna}) [0];
          }else{
            $scope.form.factura.comuna = null;
          }
        };
        TramitanteResource.cargaComunas({idRegion: $scope.form.factura.codRegion}, successCallbackComunas, $scope.errorCallback);
      }else{
        $scope.form.factura.region = null;
        $scope.form.factura.comuna = null;
      }
    };
    $scope.form.factura.tramitante = $scope.tramitante;
    FacturaResource.rut($scope.form.factura, successCallback, $scope.errorCallback);
  };

  $scope.getTipoPersonaRut = function (rut) {
    console.log('getTipoPersonaRut: ' + rut);
    $scope.form.factura.tipoPersona = FacturaUtil.getTipoPersonaRut(rut);
  };

  $scope.onRegionCambia = function () {
    var successCallbackComunas = function(data, responseHeaders){
      $scope.comunas = data;
    };
    TramitanteResource.cargaComunas({idRegion: $scope.form.factura.region.codigo}, successCallbackComunas, $scope.errorCallback);
  };

  $scope.grabar = function () {
    console.log('grabar');
    console.log($scope.form);

    // Facturas Monto 0
    if( Number($scope.form.factura.total) === 0 ){
      swal({ title: 'Error', text: 'No puede ingresar facturas con monto cero', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    // Sistema valida que la casilla Orden de Compra se encuentra marcada y el campo N° de orden no está vacío
    var regexOC = /^[A-Za-z0-9]{1,20}$/;
    if( $scope.form.oc && ( typeof($scope.form.factura.ordenCompra) === 'undefined' || $scope.form.factura.ordenCompra === null || !regexOC.test($scope.form.factura.ordenCompra) ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el N° de orden de Compra válida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    // Sistema valida que el usuario es Agencia y solicita el número de Factura
    var regexNumeroFactura = /[0-9]{1,8}/;
    if( $scope.esAgencia && ( typeof($scope.form.factura.numeroFactura) === 'undefined' || $scope.form.factura.numeroFactura === null || !regexNumeroFactura.test($scope.form.factura.numeroFactura) || parseInt($scope.form.factura.numeroFactura) <= 0 ) ) {
      swal({ title: 'Error', text: 'Debe Ingresar un número de factura válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    // Sistema valida que los siguientes campos no se encuentran vacíos:
    //
    //  a. Rut: Debe ingresar un Rut valido para Facturar
    //  b. Apellido paterno (para persona natural): Debe ingresar Apellido Paterno valido
    //  c. Nombres o Razón Social: Debe ingresar Nombre | Debe ingresar Razón Social valida
    //  d. Giro: Debe ingresar Giro
    //  e. Dirección: Debe ingresar Dirección
    //  f. Región: Debe seleccionar la Región
    //  g. Comuna: Debe seleccionar la Comuna
    //
    if( typeof($scope.form.factura.rutFacturacion) === 'undefined' || $scope.form.factura.rutFacturacion === null || $scope.form.factura.rutFacturacion.trim() === '' || !$scope.esRutValido($scope.form.factura.rutFacturacion) ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }
    if( $scope.form.factura.tipoPersona === 'N' && ( typeof($scope.form.factura.nombres) === 'undefined' || $scope.form.factura.nombres === null || $scope.form.factura.nombres.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.form.factura.tipoPersona === 'N' && ( typeof($scope.form.factura.apellidoPaterno) === 'undefined' || $scope.form.factura.apellidoPaterno === null || $scope.form.factura.apellidoPaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Apellido Paterno válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.form.factura.tipoPersona === 'N' && ( typeof($scope.form.factura.apellidoMaterno) === 'undefined' || $scope.form.factura.apellidoMaterno === null || $scope.form.factura.apellidoMaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Apellido Materno válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( $scope.form.factura.tipoPersona === 'J' && ( typeof($scope.form.factura.razonSocial) === 'undefined' || $scope.form.factura.razonSocial === null || $scope.form.factura.razonSocial.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar Razón Social válida', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.factura.giro) === 'undefined' || $scope.form.factura.giro === null || $scope.form.factura.giro.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Giro', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.factura.direccion) === 'undefined' || $scope.form.factura.direccion === null || $scope.form.factura.direccion.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Dirección', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.factura.region) === 'undefined' || $scope.form.factura.region === null || typeof($scope.form.factura.region.codigo) === 'undefined' || $scope.form.factura.region.codigo === null || $scope.form.factura.region.codigo === 0 || $scope.form.factura.region.codigo === '0' || $scope.form.factura.region.codigo.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Región', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    if( typeof($scope.form.factura.comuna) === 'undefined' || $scope.form.factura.comuna === null || typeof($scope.form.factura.comuna.codigo) === 'undefined' || $scope.form.factura.comuna.codigo === null || $scope.form.factura.comuna.codigo === 0 || $scope.form.factura.comuna.codigo === '0' || $scope.form.factura.comuna.codigo.trim() === '' ) {
      swal({ title: 'Error', text: 'Debe ingresar Comuna', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    // Se valida el formulario general
    if ( !ctrl.formulario.$valid ) {
      swal({ title: 'Error', text: 'El formulario presenta errores!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }

    var successCallback = function(data) {
      console.log('Factura - grabar');
      console.log(data);

      if( data.idControl !== 0 ) {
        swal({ title: 'Error', text: ( typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró grabar la factura!' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
        return;
      }

      $scope.form.factura.esNueva = false;
      $scope.facturas.push($scope.form.factura);

      swal({ title: 'Éxito', text: 'Se ha grabado con éxito la factura seleccionada', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar'});
    };
    var dt = new Date();

    $scope.form.factura.esNueva = false;
    $scope.form.factura.rutFacturacion = RutHelper.getRutSP($scope.form.factura.rutFacturacion);
    $scope.form.factura.tipoFacturacion = $scope.form.tipo;
    $scope.form.factura.rutAfectado = $scope.form.tipo === 'Rut' ? $scope.form.afectado[0] : '000000000-0';
    $scope.form.factura.codRegion = Number($scope.form.factura.region.codigo);
    $scope.form.factura.codComuna = Number($scope.form.factura.comuna.codigo);
    $scope.form.factura.comunaDesc = $scope.form.factura.comuna.descripcion;
    $scope.form.factura.corrCaja = $rootScope.usuario.caja;
    $scope.form.factura.idCentroCosto = Number($rootScope.usuario.centroCostoActivo.codigo);
    $scope.form.factura.fecha = dt.getTime();
    $scope.form.factura.numeroFactura = typeof $scope.form.factura.numeroFactura === 'undefined' ? 0 : Number($scope.form.factura.numeroFactura);
    $scope.form.factura.ordenCompra = typeof $scope.form.factura.ordenCompra === 'undefined' ? '' : $scope.form.factura.ordenCompra;

    console.log($scope.form.factura);

    FacturaResource.grabar($scope.form.factura, successCallback, $scope.errorCallback);
  };

  $scope.eliminar = function () {
    console.log('eliminar');

    if( !$scope.form.factura || $scope.form.factura.esNueva !== false ) {
      swal({ title: 'Error', text: 'La factura no está grabada!', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
      return;
    }

    swal({
      title: 'Aviso',
      text: "¿Desea eliminar la factura seleccionada?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI',
      cancelButtonText: 'NO'
    }).then(function () {

      var successCallback = function(data) {
        console.log('Factura - eliminar');
        console.log(data);

        if( data.idControl !== 0 ) {
          swal({ title: 'Error', text: ( typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró eliminar la factura!' : data.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false});
          return;
        }

        if( $scope.form.tipo === 'Tercero' ) {
          $scope.facturas = [];
        }else{
          if ( $scope.facturas.length === 1 ) {
            $scope.facturas = [];
          }else if ( $scope.facturas.length > 1 ) {
            var facturaEliminar = 0;
            for (var i=0; i < $scope.facturas.length; i++) {
              if ( $scope.facturas[i].rutAfectado === $scope.form.factura.rutAfectado ) {
                facturaEliminar = i;
              }
            }
            $scope.facturas.splice(facturaEliminar, 1);
          }
        }
        $scope.form.factura = {esNueva: true};
        $scope.form.afectado = [];

        swal({ title: 'Éxito', text: 'Se ha eliminado con éxito la factura seleccionada', type: 'success', showCloseButton: true, confirmButtonText: 'cerrar'});
      };

      $scope.form.factura.corrCaja = Number($rootScope.usuario.caja);
      $scope.form.factura.rutFacturacion = RutHelper.getRutSP($scope.form.factura.rutFacturacion);
      console.log($scope.form.factura);
      FacturaResource.eliminar($scope.form.factura, successCallback, $scope.errorCallback);

    }, function(dismiss) {
      return false;
    });
  };

  $scope.salir = function () {
    // $scope.form = {};
    ctrl.formulario.$setPristine();
    ctrl.formulario.$setUntouched();
    angular.element('#factura').modal("hide");
  };

  angular.element("#factura").on('hide.bs.modal', function () {
    $rootScope.$broadcast('onFacturarTermina', $scope.facturas);
  });

  $scope.$on('onFacturar', function(event, args) {
    console.log('modalFacturaController - scope - onFacturar');
    console.log(args);

    $scope.tramitante = args[0];
    $scope.facturas = args[1];
    $scope.transacciones = args[2];

    if($rootScope.tramitante.declaracionUso){
	    $scope.form = {
	      factura: false,
	      tipo: 'Tercero',
	      afectado: '',
	      oc: false
	    };
	    $scope.onTipoCambia();
    }
    else{
	    $scope.form = {
	  	      factura: false,
	  	      tipo: 'Rut',
	  	      afectado: '',
	  	      oc: false
	  	    };    	
    }

    $scope.afectados = [];
    if ( $scope.transacciones.length > 0 ) {
      for (var i=0; i < $scope.transacciones.length; i++) {
        if ( $scope.afectados.indexOf($scope.transacciones[i].rutAfectado) === -1) {
          $scope.afectados.push($scope.transacciones[i].rutAfectado);
        }
      }
    }

    if ( $scope.facturas.length === 1 && $scope.facturas[0].tipoFacturacion === "Tercero" ) {
      $scope.form.tipo = 'Tercero';
      $scope.form.factura = $scope.facturas[0];
      $scope.form.factura.esNueva = false;
      console.log(FacturaUtil.getTipoPersonaRut($scope.facturas[0].rutFacturacion));
      $scope.form.factura.tipoPersona = FacturaUtil.getTipoPersonaRut($scope.facturas[0].rutFacturacion);
      
      if( $scope.form.factura.tipoPersona === 'J' ) {
          $scope.form.factura.razonSocial = $scope.facturas[0].razonSocial;
        }else{
          $scope.form.factura.nombres = $scope.facturas[0].tramitante.nombres;
          $scope.form.factura.apellidoPaterno = $scope.facturas[0].tramitante.apellidoPaterno;
          $scope.form.factura.apellidoMaterno = $scope.facturas[0].tramitante.apellidoMaterno;
      }
      // OC
      $scope.form.oc = false;
      if ( !$scope.facturas[0].esNueva && $scope.facturas[0].ordenCompra !== '' ) {
        $scope.form.oc = true;
      }

      // Region
      $scope.facturas[0].region = $filter('filter')($scope.regiones, {codigo: $scope.facturas[0].codRegion}) [0];
      var successCallback = function(data,responseHeaders){
        console.log('comunas...');
        $scope.comunas = data;
        $scope.facturas[0].comuna = $filter('filter')($scope.comunas, {codigo: $scope.facturas[0].codComuna}) [0];
      };
      TramitanteResource.cargaComunas({idRegion: $scope.facturas[0].region.codigo}, successCallback, $scope.errorCallback);
    }

    $scope.$apply();
    angular.element("#factura_tipo").focus();
  });

  $scope.init = function () {
    $scope.esAgencia = $rootScope.usuario.esAgencia;
    
    $scope.form = {
      factura: false,
      tipo: 'Rut',
      afectado: '',
      oc: false
    };

    var successCallbackRegiones = function(data, responseHeaders){
      $scope.regiones = data;
    };
    TramitanteResource.cargaRegiones(successCallbackRegiones, $scope.errorCallback);

    var successCallbackIVA = function(data, responseHeaders){
      console.log('successCallbackIVA');
      console.log(data);
      if(data.idControl !== 0) {
        swal({ title: 'Error', text: 'No se puede obtener el dato IVA', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function(){
          $scope.salir();
        });
        return;
      }
      $scope.iva = parseFloat(data.msgControl);
    };
    FacturaResource.obtenerIVA(successCallbackIVA, $scope.errorCallback);
  };

  // Se llama al método inicial del controlador
  angular.element("#factura").on('show.bs.modal', function () {
    $scope.init();
    
  });
  
  //slight update to account for browsers not supporting e.which 
	//function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); }; 
	// To disable f5
	/* jQuery < 1.7 */ //$(document).bind("keydown", disableF5); 
	/* OR jQuery >= 1.7 */ //$(document).on("keydown", disableF5); 
	// To re-enable f5 
	/* jQuery < 1.7 */ //$(document).unbind("keydown", disableF5); 
	/* OR jQuery >= 1.7 */ //$(document).off("keydown", disableF5); 

});
