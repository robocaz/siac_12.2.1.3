package cl.exe.ccs.dto;

import java.io.Serializable;

public class DatoBoletaFacturaDTO implements Serializable{

	private Integer codigo;
	private Integer cantidad;
	private String glosaServicio;
	private Double totalCalculado;
	
	
	
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public Double getTotalCalculado() {
		return totalCalculado;
	}
	public void setTotalCalculado(Double totalCalculado) {
		this.totalCalculado = totalCalculado;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
	
	
}
