package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdCausalBloqueo database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdCausalBloqueo.findAll", query="SELECT t FROM Tp_CeIdCausalBloqueo t")
public class Tp_CeIdCausalBloqueo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalBloqueo")
	private byte fld_CodCausalBloqueo;

	@Column(name="Fld_GlosaCausalBloqueo")
	private String fld_GlosaCausalBloqueo;

	public Tp_CeIdCausalBloqueo() {
	}

	public byte getFld_CodCausalBloqueo() {
		return this.fld_CodCausalBloqueo;
	}

	public void setFld_CodCausalBloqueo(byte fld_CodCausalBloqueo) {
		this.fld_CodCausalBloqueo = fld_CodCausalBloqueo;
	}

	public String getFld_GlosaCausalBloqueo() {
		return this.fld_GlosaCausalBloqueo;
	}

	public void setFld_GlosaCausalBloqueo(String fld_GlosaCausalBloqueo) {
		this.fld_GlosaCausalBloqueo = fld_GlosaCausalBloqueo;
	}

}