package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DocumentosAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DocumentosAER.findAll", query="SELECT t FROM Tbl_DocumentosAER t")
public class Tbl_DocumentosAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CambioBaseDatos")
	private boolean fld_CambioBaseDatos;

	@Column(name="Fld_CambioGeneral")
	private boolean fld_CambioGeneral;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodCausalProt_DD")
	private String fld_CodCausalProt_DD;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEmisor_DD")
	private String fld_CodEmisor_DD;

	@Column(name="Fld_CodEstadoSolicAER")
	private short fld_CodEstadoSolicAER;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodMoneda_DD")
	private byte fld_CodMoneda_DD;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrDocum_Id")
	private BigDecimal fld_CorrDocum_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_FecDesde")
	private Timestamp fld_FecDesde;

	@Column(name="Fld_FecDocum")
	private Timestamp fld_FecDocum;

	@Column(name="Fld_FecHasta")
	private Timestamp fld_FecHasta;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecPago_DD")
	private Timestamp fld_FecPago_DD;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecProt_DD")
	private Timestamp fld_FecProt_DD;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_FecVcto_DD")
	private Timestamp fld_FecVcto_DD;

	@Column(name="Fld_MarcaProceso")
	private boolean fld_MarcaProceso;

	@Column(name="Fld_MarcaVisada")
	private byte fld_MarcaVisada;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_MontoProt_DD")
	private BigDecimal fld_MontoProt_DD;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NombreLibrador_DD")
	private String fld_NombreLibrador_DD;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletin_DD")
	private short fld_NroBoletin_DD;

	@Column(name="Fld_NroBoletinAE")
	private short fld_NroBoletinAE;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_NroOperacion_DD")
	private String fld_NroOperacion_DD;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_PagBoletin_DD")
	private short fld_PagBoletin_DD;

	@Column(name="Fld_PagBoletinAE")
	private short fld_PagBoletinAE;

	@Column(name="Fld_Rectif_NomAfectado")
	private boolean fld_Rectif_NomAfectado;

	@Column(name="Fld_Rectif_RutAfectado")
	private boolean fld_Rectif_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumento_DD")
	private String fld_TipoDocumento_DD;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	@Column(name="Fld_TomoBoletinAE")
	private short fld_TomoBoletinAE;

	public Tbl_DocumentosAER() {
	}

	public boolean getFld_CambioBaseDatos() {
		return this.fld_CambioBaseDatos;
	}

	public void setFld_CambioBaseDatos(boolean fld_CambioBaseDatos) {
		this.fld_CambioBaseDatos = fld_CambioBaseDatos;
	}

	public boolean getFld_CambioGeneral() {
		return this.fld_CambioGeneral;
	}

	public void setFld_CambioGeneral(boolean fld_CambioGeneral) {
		this.fld_CambioGeneral = fld_CambioGeneral;
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodCausalProt_DD() {
		return this.fld_CodCausalProt_DD;
	}

	public void setFld_CodCausalProt_DD(String fld_CodCausalProt_DD) {
		this.fld_CodCausalProt_DD = fld_CodCausalProt_DD;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_CodEmisor_DD() {
		return this.fld_CodEmisor_DD;
	}

	public void setFld_CodEmisor_DD(String fld_CodEmisor_DD) {
		this.fld_CodEmisor_DD = fld_CodEmisor_DD;
	}

	public short getFld_CodEstadoSolicAER() {
		return this.fld_CodEstadoSolicAER;
	}

	public void setFld_CodEstadoSolicAER(short fld_CodEstadoSolicAER) {
		this.fld_CodEstadoSolicAER = fld_CodEstadoSolicAER;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_CodMoneda_DD() {
		return this.fld_CodMoneda_DD;
	}

	public void setFld_CodMoneda_DD(byte fld_CodMoneda_DD) {
		this.fld_CodMoneda_DD = fld_CodMoneda_DD;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrDocum_Id() {
		return this.fld_CorrDocum_Id;
	}

	public void setFld_CorrDocum_Id(BigDecimal fld_CorrDocum_Id) {
		this.fld_CorrDocum_Id = fld_CorrDocum_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public Timestamp getFld_FecDesde() {
		return this.fld_FecDesde;
	}

	public void setFld_FecDesde(Timestamp fld_FecDesde) {
		this.fld_FecDesde = fld_FecDesde;
	}

	public Timestamp getFld_FecDocum() {
		return this.fld_FecDocum;
	}

	public void setFld_FecDocum(Timestamp fld_FecDocum) {
		this.fld_FecDocum = fld_FecDocum;
	}

	public Timestamp getFld_FecHasta() {
		return this.fld_FecHasta;
	}

	public void setFld_FecHasta(Timestamp fld_FecHasta) {
		this.fld_FecHasta = fld_FecHasta;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecPago_DD() {
		return this.fld_FecPago_DD;
	}

	public void setFld_FecPago_DD(Timestamp fld_FecPago_DD) {
		this.fld_FecPago_DD = fld_FecPago_DD;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecProt_DD() {
		return this.fld_FecProt_DD;
	}

	public void setFld_FecProt_DD(Timestamp fld_FecProt_DD) {
		this.fld_FecProt_DD = fld_FecProt_DD;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public Timestamp getFld_FecVcto_DD() {
		return this.fld_FecVcto_DD;
	}

	public void setFld_FecVcto_DD(Timestamp fld_FecVcto_DD) {
		this.fld_FecVcto_DD = fld_FecVcto_DD;
	}

	public boolean getFld_MarcaProceso() {
		return this.fld_MarcaProceso;
	}

	public void setFld_MarcaProceso(boolean fld_MarcaProceso) {
		this.fld_MarcaProceso = fld_MarcaProceso;
	}

	public byte getFld_MarcaVisada() {
		return this.fld_MarcaVisada;
	}

	public void setFld_MarcaVisada(byte fld_MarcaVisada) {
		this.fld_MarcaVisada = fld_MarcaVisada;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public BigDecimal getFld_MontoProt_DD() {
		return this.fld_MontoProt_DD;
	}

	public void setFld_MontoProt_DD(BigDecimal fld_MontoProt_DD) {
		this.fld_MontoProt_DD = fld_MontoProt_DD;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public String getFld_NombreLibrador_DD() {
		return this.fld_NombreLibrador_DD;
	}

	public void setFld_NombreLibrador_DD(String fld_NombreLibrador_DD) {
		this.fld_NombreLibrador_DD = fld_NombreLibrador_DD;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletin_DD() {
		return this.fld_NroBoletin_DD;
	}

	public void setFld_NroBoletin_DD(short fld_NroBoletin_DD) {
		this.fld_NroBoletin_DD = fld_NroBoletin_DD;
	}

	public short getFld_NroBoletinAE() {
		return this.fld_NroBoletinAE;
	}

	public void setFld_NroBoletinAE(short fld_NroBoletinAE) {
		this.fld_NroBoletinAE = fld_NroBoletinAE;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public String getFld_NroOperacion_DD() {
		return this.fld_NroOperacion_DD;
	}

	public void setFld_NroOperacion_DD(String fld_NroOperacion_DD) {
		this.fld_NroOperacion_DD = fld_NroOperacion_DD;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public short getFld_PagBoletin_DD() {
		return this.fld_PagBoletin_DD;
	}

	public void setFld_PagBoletin_DD(short fld_PagBoletin_DD) {
		this.fld_PagBoletin_DD = fld_PagBoletin_DD;
	}

	public short getFld_PagBoletinAE() {
		return this.fld_PagBoletinAE;
	}

	public void setFld_PagBoletinAE(short fld_PagBoletinAE) {
		this.fld_PagBoletinAE = fld_PagBoletinAE;
	}

	public boolean getFld_Rectif_NomAfectado() {
		return this.fld_Rectif_NomAfectado;
	}

	public void setFld_Rectif_NomAfectado(boolean fld_Rectif_NomAfectado) {
		this.fld_Rectif_NomAfectado = fld_Rectif_NomAfectado;
	}

	public boolean getFld_Rectif_RutAfectado() {
		return this.fld_Rectif_RutAfectado;
	}

	public void setFld_Rectif_RutAfectado(boolean fld_Rectif_RutAfectado) {
		this.fld_Rectif_RutAfectado = fld_Rectif_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumento_DD() {
		return this.fld_TipoDocumento_DD;
	}

	public void setFld_TipoDocumento_DD(String fld_TipoDocumento_DD) {
		this.fld_TipoDocumento_DD = fld_TipoDocumento_DD;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

	public short getFld_TomoBoletinAE() {
		return this.fld_TomoBoletinAE;
	}

	public void setFld_TomoBoletinAE(short fld_TomoBoletinAE) {
		this.fld_TomoBoletinAE = fld_TomoBoletinAE;
	}

}