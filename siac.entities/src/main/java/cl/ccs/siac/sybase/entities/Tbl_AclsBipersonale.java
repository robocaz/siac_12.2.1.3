package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_AclsBipersonales database table.
 * 
 */
@Entity
@Table(name="Tbl_AclsBipersonales")
@NamedQuery(name="Tbl_AclsBipersonale.findAll", query="SELECT t FROM Tbl_AclsBipersonale t")
public class Tbl_AclsBipersonale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_AclsBipersonale() {
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}