package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_USUARIOCCOSTO database table.
 * 
 */
@Entity
@Table(name="TBL_USUARIOCCOSTO")
@NamedQuery(name="TblUsuarioccosto.findAll", query="SELECT t FROM TblUsuarioccosto t")
public class TblUsuarioccosto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ACTIVO")
	private BigDecimal fldActivo;

	@Column(name="FLD_CODCCOSTO")
	private BigDecimal fldCodccosto;

	@Column(name="FLD_FECHAACTIVACION")
	private Timestamp fldFechaactivacion;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	public TblUsuarioccosto() {
	}

	public BigDecimal getFldActivo() {
		return this.fldActivo;
	}

	public void setFldActivo(BigDecimal fldActivo) {
		this.fldActivo = fldActivo;
	}

	public BigDecimal getFldCodccosto() {
		return this.fldCodccosto;
	}

	public void setFldCodccosto(BigDecimal fldCodccosto) {
		this.fldCodccosto = fldCodccosto;
	}

	public Timestamp getFldFechaactivacion() {
		return this.fldFechaactivacion;
	}

	public void setFldFechaactivacion(Timestamp fldFechaactivacion) {
		this.fldFechaactivacion = fldFechaactivacion;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

}