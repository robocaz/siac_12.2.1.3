package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutVtlMccBloVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutVtlMccBloVCE.findAll", query="SELECT t FROM Tbl_RutVtlMccBloVCE t")
public class Tbl_RutVtlMccBloVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRegistro")
	private BigDecimal fld_CorrRegistro;

	@Column(name="Fld_FlagRutBloqueado")
	private boolean fld_FlagRutBloqueado;

	@Column(name="Fld_FlagRutErroneo")
	private boolean fld_FlagRutErroneo;

	@Column(name="Fld_Nombre")
	private String fld_Nombre;

	@Column(name="Fld_NroMCC")
	private short fld_NroMCC;

	@Column(name="Fld_NroProtBIC")
	private short fld_NroProtBIC;

	@Column(name="Fld_NroProtCH")
	private short fld_NroProtCH;

	@Column(name="Fld_NroProtCM")
	private short fld_NroProtCM;

	@Column(name="Fld_NroProtLT")
	private short fld_NroProtLT;

	@Column(name="Fld_NroProtPG")
	private short fld_NroProtPG;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TotMtoBIC")
	private BigDecimal fld_TotMtoBIC;

	@Column(name="Fld_TotMtoCH")
	private BigDecimal fld_TotMtoCH;

	@Column(name="Fld_TotMtoCM")
	private BigDecimal fld_TotMtoCM;

	@Column(name="Fld_TotMtoLT")
	private BigDecimal fld_TotMtoLT;

	@Column(name="Fld_TotMtoMCC")
	private BigDecimal fld_TotMtoMCC;

	@Column(name="Fld_TotMtoPG")
	private BigDecimal fld_TotMtoPG;

	public Tbl_RutVtlMccBloVCE() {
	}

	public BigDecimal getFld_CorrRegistro() {
		return this.fld_CorrRegistro;
	}

	public void setFld_CorrRegistro(BigDecimal fld_CorrRegistro) {
		this.fld_CorrRegistro = fld_CorrRegistro;
	}

	public boolean getFld_FlagRutBloqueado() {
		return this.fld_FlagRutBloqueado;
	}

	public void setFld_FlagRutBloqueado(boolean fld_FlagRutBloqueado) {
		this.fld_FlagRutBloqueado = fld_FlagRutBloqueado;
	}

	public boolean getFld_FlagRutErroneo() {
		return this.fld_FlagRutErroneo;
	}

	public void setFld_FlagRutErroneo(boolean fld_FlagRutErroneo) {
		this.fld_FlagRutErroneo = fld_FlagRutErroneo;
	}

	public String getFld_Nombre() {
		return this.fld_Nombre;
	}

	public void setFld_Nombre(String fld_Nombre) {
		this.fld_Nombre = fld_Nombre;
	}

	public short getFld_NroMCC() {
		return this.fld_NroMCC;
	}

	public void setFld_NroMCC(short fld_NroMCC) {
		this.fld_NroMCC = fld_NroMCC;
	}

	public short getFld_NroProtBIC() {
		return this.fld_NroProtBIC;
	}

	public void setFld_NroProtBIC(short fld_NroProtBIC) {
		this.fld_NroProtBIC = fld_NroProtBIC;
	}

	public short getFld_NroProtCH() {
		return this.fld_NroProtCH;
	}

	public void setFld_NroProtCH(short fld_NroProtCH) {
		this.fld_NroProtCH = fld_NroProtCH;
	}

	public short getFld_NroProtCM() {
		return this.fld_NroProtCM;
	}

	public void setFld_NroProtCM(short fld_NroProtCM) {
		this.fld_NroProtCM = fld_NroProtCM;
	}

	public short getFld_NroProtLT() {
		return this.fld_NroProtLT;
	}

	public void setFld_NroProtLT(short fld_NroProtLT) {
		this.fld_NroProtLT = fld_NroProtLT;
	}

	public short getFld_NroProtPG() {
		return this.fld_NroProtPG;
	}

	public void setFld_NroProtPG(short fld_NroProtPG) {
		this.fld_NroProtPG = fld_NroProtPG;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public BigDecimal getFld_TotMtoBIC() {
		return this.fld_TotMtoBIC;
	}

	public void setFld_TotMtoBIC(BigDecimal fld_TotMtoBIC) {
		this.fld_TotMtoBIC = fld_TotMtoBIC;
	}

	public BigDecimal getFld_TotMtoCH() {
		return this.fld_TotMtoCH;
	}

	public void setFld_TotMtoCH(BigDecimal fld_TotMtoCH) {
		this.fld_TotMtoCH = fld_TotMtoCH;
	}

	public BigDecimal getFld_TotMtoCM() {
		return this.fld_TotMtoCM;
	}

	public void setFld_TotMtoCM(BigDecimal fld_TotMtoCM) {
		this.fld_TotMtoCM = fld_TotMtoCM;
	}

	public BigDecimal getFld_TotMtoLT() {
		return this.fld_TotMtoLT;
	}

	public void setFld_TotMtoLT(BigDecimal fld_TotMtoLT) {
		this.fld_TotMtoLT = fld_TotMtoLT;
	}

	public BigDecimal getFld_TotMtoMCC() {
		return this.fld_TotMtoMCC;
	}

	public void setFld_TotMtoMCC(BigDecimal fld_TotMtoMCC) {
		this.fld_TotMtoMCC = fld_TotMtoMCC;
	}

	public BigDecimal getFld_TotMtoPG() {
		return this.fld_TotMtoPG;
	}

	public void setFld_TotMtoPG(BigDecimal fld_TotMtoPG) {
		this.fld_TotMtoPG = fld_TotMtoPG;
	}

}