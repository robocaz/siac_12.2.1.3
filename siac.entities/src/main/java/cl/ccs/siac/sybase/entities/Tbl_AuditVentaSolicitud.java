package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditVentaSolicitud database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditVentaSolicitud.findAll", query="SELECT t FROM Tbl_AuditVentaSolicitud t")
public class Tbl_AuditVentaSolicitud implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_AuditVentaSolicitudPK id;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	public Tbl_AuditVentaSolicitud() {
	}

	public Tbl_AuditVentaSolicitudPK getId() {
		return this.id;
	}

	public void setId(Tbl_AuditVentaSolicitudPK id) {
		this.id = id;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

}