package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_NrosConfirmVig database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_NrosConfirmVig.findAll", query="SELECT t FROM Tbl_NrosConfirmVig t")
public class Tbl_NrosConfirmVig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrVigNC_Id")
	private BigDecimal fld_CorrVigNC_Id;

	@Column(name="Fld_Estado")
	private boolean fld_Estado;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecInicioVigencia")
	private Timestamp fld_FecInicioVigencia;

	@Column(name="Fld_FecTerminoVigencia")
	private Timestamp fld_FecTerminoVigencia;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_NrosConfirmVig() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrVigNC_Id() {
		return this.fld_CorrVigNC_Id;
	}

	public void setFld_CorrVigNC_Id(BigDecimal fld_CorrVigNC_Id) {
		this.fld_CorrVigNC_Id = fld_CorrVigNC_Id;
	}

	public boolean getFld_Estado() {
		return this.fld_Estado;
	}

	public void setFld_Estado(boolean fld_Estado) {
		this.fld_Estado = fld_Estado;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecInicioVigencia() {
		return this.fld_FecInicioVigencia;
	}

	public void setFld_FecInicioVigencia(Timestamp fld_FecInicioVigencia) {
		this.fld_FecInicioVigencia = fld_FecInicioVigencia;
	}

	public Timestamp getFld_FecTerminoVigencia() {
		return this.fld_FecTerminoVigencia;
	}

	public void setFld_FecTerminoVigencia(Timestamp fld_FecTerminoVigencia) {
		this.fld_FecTerminoVigencia = fld_FecTerminoVigencia;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}