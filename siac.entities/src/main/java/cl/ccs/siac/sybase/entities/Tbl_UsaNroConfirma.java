package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsaNroConfirma database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsaNroConfirma.findAll", query="SELECT t FROM Tbl_UsaNroConfirma t")
public class Tbl_UsaNroConfirma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsaNroConfirma")
	private boolean fld_CodUsaNroConfirma;

	@Column(name="Fld_GlosaUsaNroConfirma")
	private String fld_GlosaUsaNroConfirma;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_UsaNroConfirma() {
	}

	public boolean getFld_CodUsaNroConfirma() {
		return this.fld_CodUsaNroConfirma;
	}

	public void setFld_CodUsaNroConfirma(boolean fld_CodUsaNroConfirma) {
		this.fld_CodUsaNroConfirma = fld_CodUsaNroConfirma;
	}

	public String getFld_GlosaUsaNroConfirma() {
		return this.fld_GlosaUsaNroConfirma;
	}

	public void setFld_GlosaUsaNroConfirma(String fld_GlosaUsaNroConfirma) {
		this.fld_GlosaUsaNroConfirma = fld_GlosaUsaNroConfirma;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}