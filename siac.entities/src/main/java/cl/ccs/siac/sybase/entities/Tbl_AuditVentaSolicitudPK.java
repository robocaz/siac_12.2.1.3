package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_AuditVentaSolicitud database table.
 * 
 */
@Embeddable
public class Tbl_AuditVentaSolicitudPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRegistro")
	private long fld_CorrRegistro;

	@Column(name="Fld_CorrSolicitud")
	private long fld_CorrSolicitud;

	public Tbl_AuditVentaSolicitudPK() {
	}
	public long getFld_CorrRegistro() {
		return this.fld_CorrRegistro;
	}
	public void setFld_CorrRegistro(long fld_CorrRegistro) {
		this.fld_CorrRegistro = fld_CorrRegistro;
	}
	public long getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}
	public void setFld_CorrSolicitud(long fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_AuditVentaSolicitudPK)) {
			return false;
		}
		Tbl_AuditVentaSolicitudPK castOther = (Tbl_AuditVentaSolicitudPK)other;
		return 
			(this.fld_CorrRegistro == castOther.fld_CorrRegistro)
			&& (this.fld_CorrSolicitud == castOther.fld_CorrSolicitud);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_CorrRegistro ^ (this.fld_CorrRegistro >>> 32)));
		hash = hash * prime + ((int) (this.fld_CorrSolicitud ^ (this.fld_CorrSolicitud >>> 32)));
		
		return hash;
	}
}