package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABIReactivacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIReactivacion.findAll", query="SELECT t FROM Tbl_ABIReactivacion t")
public class Tbl_ABIReactivacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FecTerminoVigencia")
	private Timestamp fld_FecTerminoVigencia;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_ABIReactivacion() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public Timestamp getFld_FecTerminoVigencia() {
		return this.fld_FecTerminoVigencia;
	}

	public void setFld_FecTerminoVigencia(Timestamp fld_FecTerminoVigencia) {
		this.fld_FecTerminoVigencia = fld_FecTerminoVigencia;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}