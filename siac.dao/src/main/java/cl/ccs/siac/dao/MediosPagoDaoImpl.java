package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.MedioPagoDTO;

@Named("mediosPagoDao")
@Stateless
public class MediosPagoDaoImpl implements MediosPagoDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(MediosPagoDaoImpl.class);

	public List<ComboDTO> obtenerMediosPago() {

		List<ComboDTO> mediosPago = new ArrayList<ComboDTO>();

		try {

			log.info("Ejecutando SQ_TipoPago_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TipoPago_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoPago", String.class, ParameterMode.OUT);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO medioPago = new ComboDTO();
				medioPago.setCodigo(obj[0].toString().trim());
				medioPago.setDescripcion(obj[1].toString().trim());
				mediosPago.add(medioPago);
			}

			return mediosPago;
		} catch (Exception e) {
			log.error("Error en el SP SQ_TipoPago_SW2 " + e.getMessage());
			return null;
		}

	}

	public MedioPagoDTO obtenerBoletas(MedioPagoDTO medioPagoDTO) {

		try {
			
			log.info("Ejecutando SQ_MedioPagoFolio_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_MedioPagoFolio_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoIngresadoCajera", Integer.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaPago", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", medioPagoDTO.getIdCaja());
			storedProcedure.setParameter("Fld_CodCCosto", medioPagoDTO.getIdCCosto());
			storedProcedure.setParameter("Fld_NroBoletaFactura", medioPagoDTO.getNumboletafac());

			boolean rs = storedProcedure.execute();

			medioPagoDTO.setMontoIngresadoCajera(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_MontoIngresadoCajera").toString()));
			ComboDTO medioDB = new ComboDTO();
			medioDB.setCodigo(storedProcedure.getOutputParameterValue("Fld_TipoPago").toString().trim());
			medioDB.setDescripcion(storedProcedure.getOutputParameterValue("Fld_GlosaPago").toString().trim());
			medioPagoDTO.setMediospago(medioDB);
			medioPagoDTO.setGlosaPago(storedProcedure.getOutputParameterValue("Fld_GlosaPago").toString());

			medioPagoDTO.setIdControl(0);

			return medioPagoDTO;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_MedioPagoFolio_SW2 " + e.getMessage());
			medioPagoDTO.setIdControl(1);
			medioPagoDTO.setMsgControl(e.getMessage());
			return medioPagoDTO;
		}

	}

	public MedioPagoDTO actualizaMedioDePago(MedioPagoDTO medioPagoDTO) {

		try {
			
			log.info("Ejecutando SU_MedioPagoFolio_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_MedioPagoFolio_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodCCosto", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletaFactura", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodSucursal", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NroCheque", String.class, ParameterMode.IN);

			storedProcedure.setParameter("Fld_CorrCaja", medioPagoDTO.getIdCaja());
			storedProcedure.setParameter("Fld_CodCCosto", medioPagoDTO.getIdCCosto());
			storedProcedure.setParameter("Fld_NroBoletaFactura", medioPagoDTO.getNumboletafac());
			storedProcedure.setParameter("Fld_TipoPago", medioPagoDTO.getMediospago().getCodigo());

			if (medioPagoDTO.getEmisor() != null && medioPagoDTO.getEmisor().getCodigo() != null) {
				storedProcedure.setParameter("Fld_CodEmisor", medioPagoDTO.getEmisor().getCodigo());
			} else {
				storedProcedure.setParameter("Fld_CodEmisor", "");
			}
			if (medioPagoDTO.getSucursal() != null && medioPagoDTO.getSucursal().getCodSucursal() != null) {
				storedProcedure.setParameter("Fld_CodSucursal", medioPagoDTO.getSucursal().getCodSucursal());
			} else {
				storedProcedure.setParameter("Fld_CodSucursal", new Integer(0));
			}
			if (medioPagoDTO.getNumerocheque() != null && !medioPagoDTO.getNumerocheque().equals("")) {
				storedProcedure.setParameter("Fld_NroCheque", medioPagoDTO.getNumerocheque());
			} else {
				storedProcedure.setParameter("Fld_NroCheque", "");
			}

			storedProcedure.execute();

			medioPagoDTO.setIdControl(0);

			return medioPagoDTO;
		}

		catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SU_MedioPagoFolio_SW2 " + e.getMessage());
			medioPagoDTO.setIdControl(1);
			medioPagoDTO.setMsgControl(e.getMessage());
			return medioPagoDTO;
		}
	}

}
