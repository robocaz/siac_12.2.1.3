package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LibradoresEmail database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LibradoresEmail.findAll", query="SELECT t FROM Tbl_LibradoresEmail t")
public class Tbl_LibradoresEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_LibradoresEmailPK id;

	@Column(name="Fld_CargoFuncionario")
	private String fld_CargoFuncionario;

	@Column(name="Fld_CodTipoEmail")
	private short fld_CodTipoEmail;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_Nombre")
	private String fld_Nombre;

	@Column(name="Fld_TelefonoContacto")
	private String fld_TelefonoContacto;

	public Tbl_LibradoresEmail() {
	}

	public Tbl_LibradoresEmailPK getId() {
		return this.id;
	}

	public void setId(Tbl_LibradoresEmailPK id) {
		this.id = id;
	}

	public String getFld_CargoFuncionario() {
		return this.fld_CargoFuncionario;
	}

	public void setFld_CargoFuncionario(String fld_CargoFuncionario) {
		this.fld_CargoFuncionario = fld_CargoFuncionario;
	}

	public short getFld_CodTipoEmail() {
		return this.fld_CodTipoEmail;
	}

	public void setFld_CodTipoEmail(short fld_CodTipoEmail) {
		this.fld_CodTipoEmail = fld_CodTipoEmail;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public String getFld_Nombre() {
		return this.fld_Nombre;
	}

	public void setFld_Nombre(String fld_Nombre) {
		this.fld_Nombre = fld_Nombre;
	}

	public String getFld_TelefonoContacto() {
		return this.fld_TelefonoContacto;
	}

	public void setFld_TelefonoContacto(String fld_TelefonoContacto) {
		this.fld_TelefonoContacto = fld_TelefonoContacto;
	}

}