package cl.exe.ccs.dto.certificados;

import java.io.Serializable;

public class CertificadoISAbcDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1861169180211118965L;
	private String boletin;
	private String fechaProtesto;
	private String fechaRecepcion;
	private String tipoDocumento;
	private String tipoDocumentoImpago;
	private String nroOperacion;
	private String codEmisor;
	private String emisor;
	private String moneda;
	private String monto;
	private String ciudad;
	private String librador;
	
	public String getBoletin() {
		return boletin;
	}
	public void setBoletin(String boletin) {
		this.boletin = boletin;
	}
	public String getFechaProtesto() {
		return fechaProtesto;
	}
	public void setFechaProtesto(String fechaProtesto) {
		this.fechaProtesto = fechaProtesto;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getTipoDocumentoImpago() {
		return tipoDocumentoImpago;
	}
	public void setTipoDocumentoImpago(String tipoDocumentoImpago) {
		this.tipoDocumentoImpago = tipoDocumentoImpago;
	}
	public String getNroOperacion() {
		return nroOperacion;
	}
	public void setNroOperacion(String nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getLibrador() {
		return librador;
	}
	public void setLibrador(String librador) {
		this.librador = librador;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	
	
}
