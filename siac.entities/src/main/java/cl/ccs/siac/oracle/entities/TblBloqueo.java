package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_BLOQUEO database table.
 * 
 */
@Entity
@Table(name="TBL_BLOQUEO")
@NamedQuery(name="TblBloqueo.findAll", query="SELECT t FROM TblBloqueo t")
public class TblBloqueo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_BLOQUEO_FLDCORRBLOQUEOID_GENERATOR", sequenceName="FLD_CORRBLOQUEO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_BLOQUEO_FLDCORRBLOQUEOID_GENERATOR")
	@Column(name="FLD_CORRBLOQUEO_ID")
	private long fldCorrbloqueoId;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHABLOQUEO")
	private Date fldFechabloqueo;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAEXPIRACION")
	private Date fldFechaexpiracion;

	@Column(name="FLD_FLAGVIGENCIA")
	private BigDecimal fldFlagvigencia;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_USUARIOBLOQUEO")
	private String fldUsuariobloqueo;

	@Column(name="FLD_USUARIODESBLOQUEO")
	private String fldUsuariodesbloqueo;

	//bi-directional many-to-one association to TblMorosidad
	@ManyToOne
	@JoinColumn(name="FLD_CORRMOROSIDAD")
	private TblMorosidad tblMorosidad;

	public TblBloqueo() {
	}

	public long getFldCorrbloqueoId() {
		return this.fldCorrbloqueoId;
	}

	public void setFldCorrbloqueoId(long fldCorrbloqueoId) {
		this.fldCorrbloqueoId = fldCorrbloqueoId;
	}

	public Date getFldFechabloqueo() {
		return this.fldFechabloqueo;
	}

	public void setFldFechabloqueo(Date fldFechabloqueo) {
		this.fldFechabloqueo = fldFechabloqueo;
	}

	public Date getFldFechaexpiracion() {
		return this.fldFechaexpiracion;
	}

	public void setFldFechaexpiracion(Date fldFechaexpiracion) {
		this.fldFechaexpiracion = fldFechaexpiracion;
	}

	public BigDecimal getFldFlagvigencia() {
		return this.fldFlagvigencia;
	}

	public void setFldFlagvigencia(BigDecimal fldFlagvigencia) {
		this.fldFlagvigencia = fldFlagvigencia;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldUsuariobloqueo() {
		return this.fldUsuariobloqueo;
	}

	public void setFldUsuariobloqueo(String fldUsuariobloqueo) {
		this.fldUsuariobloqueo = fldUsuariobloqueo;
	}

	public String getFldUsuariodesbloqueo() {
		return this.fldUsuariodesbloqueo;
	}

	public void setFldUsuariodesbloqueo(String fldUsuariodesbloqueo) {
		this.fldUsuariodesbloqueo = fldUsuariodesbloqueo;
	}

	public TblMorosidad getTblMorosidad() {
		return this.tblMorosidad;
	}

	public void setTblMorosidad(TblMorosidad tblMorosidad) {
		this.tblMorosidad = tblMorosidad;
	}

}