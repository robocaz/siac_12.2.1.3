package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_RESULTADOLOG database table.
 * 
 */
@Entity
@Table(name="TBL_RESULTADOLOG")
@NamedQuery(name="TblResultadolog.findAll", query="SELECT t FROM TblResultadolog t")
public class TblResultadolog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODTRANSACCION")
	private BigDecimal fldCodtransaccion;

	@Column(name="FLD_GLOSARESULTADO")
	private String fldGlosaresultado;

	@Column(name="FLD_RESULTADO")
	private BigDecimal fldResultado;

	public TblResultadolog() {
	}

	public BigDecimal getFldCodtransaccion() {
		return this.fldCodtransaccion;
	}

	public void setFldCodtransaccion(BigDecimal fldCodtransaccion) {
		this.fldCodtransaccion = fldCodtransaccion;
	}

	public String getFldGlosaresultado() {
		return this.fldGlosaresultado;
	}

	public void setFldGlosaresultado(String fldGlosaresultado) {
		this.fldGlosaresultado = fldGlosaresultado;
	}

	public BigDecimal getFldResultado() {
		return this.fldResultado;
	}

	public void setFldResultado(BigDecimal fldResultado) {
		this.fldResultado = fldResultado;
	}

}