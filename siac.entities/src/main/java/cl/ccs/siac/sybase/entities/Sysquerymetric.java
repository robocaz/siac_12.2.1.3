package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysquerymetrics database table.
 * 
 */
@Entity
@Table(name="sysquerymetrics")
@NamedQuery(name="Sysquerymetric.findAll", query="SELECT s FROM Sysquerymetric s")
public class Sysquerymetric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="abort_cnt")
	private int abortCnt;

	private int cnt;

	@Column(name="elap_avg")
	private int elapAvg;

	@Column(name="elap_max")
	private int elapMax;

	@Column(name="elap_min")
	private int elapMin;

	@Column(name="exec_avg")
	private int execAvg;

	@Column(name="exec_max")
	private int execMax;

	@Column(name="exec_min")
	private int execMin;

	private int gid;

	private int hashkey;

	private int id;

	@Column(name="lio_avg")
	private int lioAvg;

	@Column(name="lio_max")
	private int lioMax;

	@Column(name="lio_min")
	private int lioMin;

	@Column(name="pio_avg")
	private int pioAvg;

	@Column(name="pio_max")
	private int pioMax;

	@Column(name="pio_min")
	private int pioMin;

	private String qtext;

	private short sequence;

	private int uid;

	public Sysquerymetric() {
	}

	public int getAbortCnt() {
		return this.abortCnt;
	}

	public void setAbortCnt(int abortCnt) {
		this.abortCnt = abortCnt;
	}

	public int getCnt() {
		return this.cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public int getElapAvg() {
		return this.elapAvg;
	}

	public void setElapAvg(int elapAvg) {
		this.elapAvg = elapAvg;
	}

	public int getElapMax() {
		return this.elapMax;
	}

	public void setElapMax(int elapMax) {
		this.elapMax = elapMax;
	}

	public int getElapMin() {
		return this.elapMin;
	}

	public void setElapMin(int elapMin) {
		this.elapMin = elapMin;
	}

	public int getExecAvg() {
		return this.execAvg;
	}

	public void setExecAvg(int execAvg) {
		this.execAvg = execAvg;
	}

	public int getExecMax() {
		return this.execMax;
	}

	public void setExecMax(int execMax) {
		this.execMax = execMax;
	}

	public int getExecMin() {
		return this.execMin;
	}

	public void setExecMin(int execMin) {
		this.execMin = execMin;
	}

	public int getGid() {
		return this.gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public int getHashkey() {
		return this.hashkey;
	}

	public void setHashkey(int hashkey) {
		this.hashkey = hashkey;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLioAvg() {
		return this.lioAvg;
	}

	public void setLioAvg(int lioAvg) {
		this.lioAvg = lioAvg;
	}

	public int getLioMax() {
		return this.lioMax;
	}

	public void setLioMax(int lioMax) {
		this.lioMax = lioMax;
	}

	public int getLioMin() {
		return this.lioMin;
	}

	public void setLioMin(int lioMin) {
		this.lioMin = lioMin;
	}

	public int getPioAvg() {
		return this.pioAvg;
	}

	public void setPioAvg(int pioAvg) {
		this.pioAvg = pioAvg;
	}

	public int getPioMax() {
		return this.pioMax;
	}

	public void setPioMax(int pioMax) {
		this.pioMax = pioMax;
	}

	public int getPioMin() {
		return this.pioMin;
	}

	public void setPioMin(int pioMin) {
		this.pioMin = pioMin;
	}

	public String getQtext() {
		return this.qtext;
	}

	public void setQtext(String qtext) {
		this.qtext = qtext;
	}

	public short getSequence() {
		return this.sequence;
	}

	public void setSequence(short sequence) {
		this.sequence = sequence;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}