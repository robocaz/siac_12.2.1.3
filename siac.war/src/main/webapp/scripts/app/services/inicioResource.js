angular.module('ccs').factory('InicioResource', function ($resource) {
    var resource = $resource('rest/usuario',{
        nocache : new Date().getTime()
    },{
        'datos' :  {  method : 'POST', isArray : false},
        'cargaTarifas' :  {  method : 'GET', isArray : true, url: 'rest/listado/tarifas'},
        'nombreMaquina' :  {  method : 'GET', isArray : false, url: 'http://localhost:8080/terminal/siac/boletas'},
        'cargaConfig' :  {  method : 'GET', isArray : false, url: 'http://localhost:8080/terminal/config/inicio'},
        'emiteBoleta' :  {  method : 'GET', isArray : false, params:{emiteboleta:'emiteboleta'}, url: 'http://localhost:8080/terminal/config/emiteboleta'},
        'emiteFactura' :  {  method : 'GET', isArray : false, params:{emitefactura:'emitefactura'}, url: 'http://localhost:8080/terminal/config/emitefactura'},
        'configInicial' :  {  method : 'POST', isArray : false, url: 'http://localhost:8080/terminal/config/confinicial'},
        'maquinaSesion' :  {  method : 'GET',  params:{nombremaquina:'nombremaquina'}, isArray : false, url: 'rest/login/nombremaquinasesion'},
    });
    return resource;
});