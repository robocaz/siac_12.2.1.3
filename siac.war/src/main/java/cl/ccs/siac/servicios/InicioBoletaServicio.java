package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.BoletaDao;
import cl.ccs.siac.utils.NombreClientePC;
import cl.exe.ccs.dto.BoletaDTO;
import cl.exe.ccs.dto.ControlBoletaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/boleta")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class InicioBoletaServicio {

	private static final Logger log = LogManager.getLogger(InicioBoletaServicio.class);

	@Inject
	private BoletaDao boletaDao;

	@Inject
	private NombreClientePC nombreClientePC;

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/obtenerBoleta")
	public Response obtenerBoleta(@Context HttpServletRequest request, BoletaDTO boletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		return Response.ok(boletaDao.obtenerBoleta(boletaDTO)).build();
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/obtenerBoletaTransaccion")
	public Response obtenerBoletaTransaccion(@Context HttpServletRequest request, BoletaDTO boletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerBoletaTransaccion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		try{
			BoletaDTO boleta = boletaDao.obtenerBoletaTransaccion(boletaDTO);
			return Response.ok(boleta).build();
		}
		catch(Exception e){
			log.error("Metodo obtenerBoletaTransaccion - Message: Error del Sistema " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/obtenerBoletasFolioCero")
	public Response obtenerBoletasFolioCero(@Context HttpServletRequest request, BoletaDTO boletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: obtenerBoletasFolioCero - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
        try{
        	return Response.ok(boletaDao.obtenerBoletasFolioCero(boletaDTO)).build();
        }
		catch(Exception e){
			log.error("Metodo obtenerBoletasFolioCero - Message: Error del Sistema " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/consulta")
	public Response consultaRangoBoletas(@Context HttpServletRequest request, ControlBoletaDTO controlBoletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: consultaRangoBoletas - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try{
			
			String nombreMaquina = request.getSession().getAttribute("nombremaquina").toString();
			
			if((controlBoletaDTO.getNombreMaquina() == null || controlBoletaDTO.getNombreMaquina().equals("")) && (nombreMaquina != null && !nombreMaquina.equals(""))){
				controlBoletaDTO.setNombreMaquina(nombreMaquina);
			}
			
			ResponseDTO respuesta = boletaDao.consultaBoleta(controlBoletaDTO);
	
			if (respuesta.getIdControl() == 0) {
				return Response.ok(respuesta).build();
			} else {
				respuesta.setMsgControl("Error en la aplicación");
				return Response.ok(respuesta).build();
			}
		}
		catch(Exception e){
			log.error("Metodo consultaRangoBoletas - Message: Error del Sistema " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/iniciar")
	public Response iniciaRangoBoletas(@Context HttpServletRequest request, ControlBoletaDTO controlBoletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: iniciaRangoBoletas - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try{
			ResponseDTO respuesta = boletaDao.inicializaBoleta(controlBoletaDTO);
	
			if (respuesta.getIdControl() == 0) {
				return Response.ok(respuesta).build();
			} else {
				respuesta.setMsgControl("Error en la aplicación");
				return Response.ok(respuesta).build();
			}
		}
		catch(Exception e){
			log.error("Metodo iniciaRangoBoletas - Message: Error del Sistema ", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/anular")
	public Response anularBoleta(@Context HttpServletRequest request, ControlBoletaDTO controlBoletaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		UsuarioDTO dto = (UsuarioDTO) request.getSession().getAttribute("usuario");
		if (userPrincipal == null || dto == null) {
			log.error("Error en el metodo: anularBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try{
			ResponseDTO respuesta = boletaDao.anularBoleta(controlBoletaDTO);
			return Response.ok(respuesta).build();
		}
		catch(Exception e){
			log.error("Metodo anularBoleta - Message: Error del Sistema ", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
