package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ValorMonedaVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ValorMonedaVCE.findAll", query="SELECT t FROM Tbl_ValorMonedaVCE t")
public class Tbl_ValorMonedaVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_FecDesde")
	private Timestamp fld_FecDesde;

	@Column(name="Fld_FecHasta")
	private Timestamp fld_FecHasta;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_ValorMoneda")
	private BigDecimal fld_ValorMoneda;

	public Tbl_ValorMonedaVCE() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public Timestamp getFld_FecDesde() {
		return this.fld_FecDesde;
	}

	public void setFld_FecDesde(Timestamp fld_FecDesde) {
		this.fld_FecDesde = fld_FecDesde;
	}

	public Timestamp getFld_FecHasta() {
		return this.fld_FecHasta;
	}

	public void setFld_FecHasta(Timestamp fld_FecHasta) {
		this.fld_FecHasta = fld_FecHasta;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public BigDecimal getFld_ValorMoneda() {
		return this.fld_ValorMoneda;
	}

	public void setFld_ValorMoneda(BigDecimal fld_ValorMoneda) {
		this.fld_ValorMoneda = fld_ValorMoneda;
	}

}