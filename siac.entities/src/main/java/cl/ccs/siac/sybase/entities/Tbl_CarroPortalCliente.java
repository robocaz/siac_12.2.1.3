package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CarroPortalCliente database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CarroPortalCliente.findAll", query="SELECT t FROM Tbl_CarroPortalCliente t")
public class Tbl_CarroPortalCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrCliente")
	private BigDecimal fld_CorrCliente;

	@Column(name="Fld_CorrContrato")
	private BigDecimal fld_CorrContrato;

	@Column(name="Fld_CorrMail")
	private BigDecimal fld_CorrMail;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_Monto")
	private BigDecimal fld_Monto;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_CarroPortalCliente() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrCliente() {
		return this.fld_CorrCliente;
	}

	public void setFld_CorrCliente(BigDecimal fld_CorrCliente) {
		this.fld_CorrCliente = fld_CorrCliente;
	}

	public BigDecimal getFld_CorrContrato() {
		return this.fld_CorrContrato;
	}

	public void setFld_CorrContrato(BigDecimal fld_CorrContrato) {
		this.fld_CorrContrato = fld_CorrContrato;
	}

	public BigDecimal getFld_CorrMail() {
		return this.fld_CorrMail;
	}

	public void setFld_CorrMail(BigDecimal fld_CorrMail) {
		this.fld_CorrMail = fld_CorrMail;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public BigDecimal getFld_Monto() {
		return this.fld_Monto;
	}

	public void setFld_Monto(BigDecimal fld_Monto) {
		this.fld_Monto = fld_Monto;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}