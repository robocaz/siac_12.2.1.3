package cl.exe.ccs.dto;

import java.io.Serializable;

public class TeAvisaDTO extends ResponseDTO implements Serializable{

	
	private PersonaDTO afectado;
	private PersonaDTO tramitante;
	private String direccion;
	private String telfijo;
	private String telcel;
	private String email;
	private String email2;
	private boolean servcorreo;
	private boolean servsms;
	private Integer corrCaja;
	private Integer centroCosto;
	private String tipoServicio;
	private Integer montoServicio;
	private String folioAbi;
	private Integer tipoTA;
	private Long corrContrato;
	private String glosaServicio;
	private PersonaDTO persTeAvisa;
	private PersonaDTO contratante;
	private String fechaInicioContrato;
	private String fechaFinContrato;
	private Integer avisoRenovacion;
	private String codServicioAux;
	private boolean upgrade;
	private Long corrMovimiento;
	private Long corrTransaccion;
	private String codAutenticacionBic;
	
	

	public String getCodAutenticacionBic() {
		return codAutenticacionBic;
	}
	public void setCodAutenticacionBic(String codAutenticacionBic) {
		this.codAutenticacionBic = codAutenticacionBic;
	}
	public Long getCorrMovimiento() {
		return corrMovimiento;
	}
	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}
	public Long getCorrTransaccion() {
		return corrTransaccion;
	}
	public void setCorrTransaccion(Long corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}
	public boolean isUpgrade() {
		return upgrade;
	}
	public void setUpgrade(boolean upgrade) {
		this.upgrade = upgrade;
	}
	public PersonaDTO getContratante() {
		return contratante;
	}
	public void setContratante(PersonaDTO contratante) {
		this.contratante = contratante;
	}
	public PersonaDTO getPersTeAvisa() {
		return persTeAvisa;
	}
	public void setPersTeAvisa(PersonaDTO persTeAvisa) {
		this.persTeAvisa = persTeAvisa;
	}
	public PersonaDTO getAfectado() {
		return afectado;
	}
	public void setAfectado(PersonaDTO afectado) {
		this.afectado = afectado;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelfijo() {
		return telfijo;
	}
	public void setTelfijo(String telfijo) {
		this.telfijo = telfijo;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public boolean getServcorreo() {
		return servcorreo;
	}
	public void setServcorreo(boolean servcorreo) {
		this.servcorreo = servcorreo;
	}
	public boolean getServsms() {
		return servsms;
	}
	public void setServsms(boolean servsms) {
		this.servsms = servsms;
	}
	public String getTelcel() {
		return telcel;
	}
	public void setTelcel(String telcel) {
		this.telcel = telcel;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public PersonaDTO getTramitante() {
		return tramitante;
	}
	public void setTramitante(PersonaDTO tramitante) {
		this.tramitante = tramitante;
	}
	public Integer getMontoServicio() {
		return montoServicio;
	}
	public void setMontoServicio(Integer montoServicio) {
		this.montoServicio = montoServicio;
	}
	public String getFolioAbi() {
		return folioAbi;
	}
	public void setFolioAbi(String folioAbi) {
		this.folioAbi = folioAbi;
	}
	public Integer getTipoTA() {
		return tipoTA;
	}
	public void setTipoTA(Integer tipoTA) {
		this.tipoTA = tipoTA;
	}
	public Long getCorrContrato() {
		return corrContrato;
	}
	public void setCorrContrato(Long corrContrato) {
		this.corrContrato = corrContrato;
	}
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public String getFechaInicioContrato() {
		return fechaInicioContrato;
	}
	public void setFechaInicioContrato(String fechaInicioContrato) {
		this.fechaInicioContrato = fechaInicioContrato;
	}
	public String getFechaFinContrato() {
		return fechaFinContrato;
	}
	public void setFechaFinContrato(String fechaFinContrato) {
		this.fechaFinContrato = fechaFinContrato;
	}
	public Integer getAvisoRenovacion() {
		return avisoRenovacion;
	}
	public void setAvisoRenovacion(Integer avisoRenovacion) {
		this.avisoRenovacion = avisoRenovacion;
	}
	public String getCodServicioAux() {
		return codServicioAux;
	}
	public void setCodServicioAux(String codServicioAux) {
		this.codServicioAux = codServicioAux;
	}
	
	
	
	
	
	
}
