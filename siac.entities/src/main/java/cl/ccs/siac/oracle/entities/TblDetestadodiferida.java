package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_DETESTADODIFERIDA database table.
 * 
 */
@Entity
@Table(name="TBL_DETESTADODIFERIDA")
@NamedQuery(name="TblDetestadodiferida.findAll", query="SELECT t FROM TblDetestadodiferida t")
public class TblDetestadodiferida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CORRCONTROLDIF")
	private BigDecimal fldCorrcontroldif;

	@Column(name="FLD_ESTADO")
	private BigDecimal fldEstado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECESTADO")
	private Date fldFecestado;

	public TblDetestadodiferida() {
	}

	public BigDecimal getFldCorrcontroldif() {
		return this.fldCorrcontroldif;
	}

	public void setFldCorrcontroldif(BigDecimal fldCorrcontroldif) {
		this.fldCorrcontroldif = fldCorrcontroldif;
	}

	public BigDecimal getFldEstado() {
		return this.fldEstado;
	}

	public void setFldEstado(BigDecimal fldEstado) {
		this.fldEstado = fldEstado;
	}

	public Date getFldFecestado() {
		return this.fldFecestado;
	}

	public void setFldFecestado(Date fldFecestado) {
		this.fldFecestado = fldFecestado;
	}

}