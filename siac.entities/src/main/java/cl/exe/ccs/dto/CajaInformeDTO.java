package cl.exe.ccs.dto;

import java.io.Serializable;

public class CajaInformeDTO extends TipoDTO implements Serializable{
	
	
	private String corrCaja;
	private String inicioCaja;
	private Integer centroCosto;
	
	
	
	public String getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(String corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getInicioCaja() {
		return inicioCaja;
	}
	public void setInicioCaja(String inicioCaja) {
		this.inicioCaja = inicioCaja;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	
	
	

}
