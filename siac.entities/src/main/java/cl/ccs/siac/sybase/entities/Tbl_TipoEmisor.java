package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoEmisor database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoEmisor.findAll", query="SELECT t FROM Tbl_TipoEmisor t")
public class Tbl_TipoEmisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoEmisor")
	private String fld_GlosaTipoEmisor;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_TipoEmisor() {
	}

	public String getFld_GlosaTipoEmisor() {
		return this.fld_GlosaTipoEmisor;
	}

	public void setFld_GlosaTipoEmisor(String fld_GlosaTipoEmisor) {
		this.fld_GlosaTipoEmisor = fld_GlosaTipoEmisor;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}