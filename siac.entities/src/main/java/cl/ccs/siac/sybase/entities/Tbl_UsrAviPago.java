package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_UsrAviPago database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsrAviPago.findAll", query="SELECT t FROM Tbl_UsrAviPago t")
public class Tbl_UsrAviPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrFirma")
	private BigDecimal fld_CorrFirma;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecModPassword")
	private Timestamp fld_FecModPassword;

	@Column(name="Fld_FecUltimoIngreso")
	private Timestamp fld_FecUltimoIngreso;

	@Column(name="Fld_FlagModPassword")
	private boolean fld_FlagModPassword;

	@Column(name="Fld_Password")
	private String fld_Password;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoUsuario")
	private byte fld_TipoUsuario;

	public Tbl_UsrAviPago() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrFirma() {
		return this.fld_CorrFirma;
	}

	public void setFld_CorrFirma(BigDecimal fld_CorrFirma) {
		this.fld_CorrFirma = fld_CorrFirma;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecModPassword() {
		return this.fld_FecModPassword;
	}

	public void setFld_FecModPassword(Timestamp fld_FecModPassword) {
		this.fld_FecModPassword = fld_FecModPassword;
	}

	public Timestamp getFld_FecUltimoIngreso() {
		return this.fld_FecUltimoIngreso;
	}

	public void setFld_FecUltimoIngreso(Timestamp fld_FecUltimoIngreso) {
		this.fld_FecUltimoIngreso = fld_FecUltimoIngreso;
	}

	public boolean getFld_FlagModPassword() {
		return this.fld_FlagModPassword;
	}

	public void setFld_FlagModPassword(boolean fld_FlagModPassword) {
		this.fld_FlagModPassword = fld_FlagModPassword;
	}

	public String getFld_Password() {
		return this.fld_Password;
	}

	public void setFld_Password(String fld_Password) {
		this.fld_Password = fld_Password;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoUsuario() {
		return this.fld_TipoUsuario;
	}

	public void setFld_TipoUsuario(byte fld_TipoUsuario) {
		this.fld_TipoUsuario = fld_TipoUsuario;
	}

}