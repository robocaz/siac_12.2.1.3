package cl.ccs.siac.exceptions;

public class ReportNotCreatedException extends ReportException {

	private static final long serialVersionUID = -4016990125365070509L;

	/**
	 * Constructor.
	 */
	public ReportNotCreatedException() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 */
	public ReportNotCreatedException(final String message) {
		super(message);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportNotCreatedException(final Throwable throwable) {
		super(throwable);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportNotCreatedException(final String message, final Throwable throwable) {
		super(message, throwable);
	}

}
