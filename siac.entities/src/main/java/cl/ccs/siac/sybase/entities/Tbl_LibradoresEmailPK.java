package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_LibradoresEmail database table.
 * 
 */
@Embeddable
public class Tbl_LibradoresEmailPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Corr_Id")
	private long fld_Corr_Id;

	@Column(name="Fld_CorrLib")
	private long fld_CorrLib;

	public Tbl_LibradoresEmailPK() {
	}
	public long getFld_Corr_Id() {
		return this.fld_Corr_Id;
	}
	public void setFld_Corr_Id(long fld_Corr_Id) {
		this.fld_Corr_Id = fld_Corr_Id;
	}
	public long getFld_CorrLib() {
		return this.fld_CorrLib;
	}
	public void setFld_CorrLib(long fld_CorrLib) {
		this.fld_CorrLib = fld_CorrLib;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_LibradoresEmailPK)) {
			return false;
		}
		Tbl_LibradoresEmailPK castOther = (Tbl_LibradoresEmailPK)other;
		return 
			(this.fld_Corr_Id == castOther.fld_Corr_Id)
			&& (this.fld_CorrLib == castOther.fld_CorrLib);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_Corr_Id ^ (this.fld_Corr_Id >>> 32)));
		hash = hash * prime + ((int) (this.fld_CorrLib ^ (this.fld_CorrLib >>> 32)));
		
		return hash;
	}
}