angular.module('ccs').factory('PersonaResource', function ($resource) {
    var resource = $resource( 'rest/persona/',  { nocache : new Date().getTime()},
    {
      'obtenerTramitante': { method:  'POST', url: 'rest/persona/obtenertramitante' },
      'obtenerPersona': { method: 'POST', url: 'rest/persona/obtenerpersona' },
      'obtenerPersonaNombre': { method: 'POST', url: 'rest/persona/obtenerpersonanombre' },
      'actualizar': { method: 'POST', url: 'rest/persona/actualizar' },
      'verificar': { method: 'POST', url: 'rest/persona/verificar' }
    });
    return resource;
});
