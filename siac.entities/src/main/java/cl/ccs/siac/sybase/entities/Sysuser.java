package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysusers database table.
 * 
 */
@Entity
@Table(name="sysusers")
@NamedQuery(name="Sysuser.findAll", query="SELECT s FROM Sysuser s")
public class Sysuser implements Serializable {
	private static final long serialVersionUID = 1L;

	private String environ;

	private int gid;

	private String name;

	private int suid;

	private int uid;

	public Sysuser() {
	}

	public String getEnviron() {
		return this.environ;
	}

	public void setEnviron(String environ) {
		this.environ = environ;
	}

	public int getGid() {
		return this.gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSuid() {
		return this.suid;
	}

	public void setSuid(int suid) {
		this.suid = suid;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}