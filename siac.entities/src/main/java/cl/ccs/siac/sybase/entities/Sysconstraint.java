package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysconstraints database table.
 * 
 */
@Entity
@Table(name="sysconstraints")
@NamedQuery(name="Sysconstraint.findAll", query="SELECT s FROM Sysconstraint s")
public class Sysconstraint implements Serializable {
	private static final long serialVersionUID = 1L;

	private short colid;

	private int constrid;

	private int error;

	private int spare2;

	private int status;

	private int tableid;

	public Sysconstraint() {
	}

	public short getColid() {
		return this.colid;
	}

	public void setColid(short colid) {
		this.colid = colid;
	}

	public int getConstrid() {
		return this.constrid;
	}

	public void setConstrid(int constrid) {
		this.constrid = constrid;
	}

	public int getError() {
		return this.error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public int getSpare2() {
		return this.spare2;
	}

	public void setSpare2(int spare2) {
		this.spare2 = spare2;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getTableid() {
		return this.tableid;
	}

	public void setTableid(int tableid) {
		this.tableid = tableid;
	}

}