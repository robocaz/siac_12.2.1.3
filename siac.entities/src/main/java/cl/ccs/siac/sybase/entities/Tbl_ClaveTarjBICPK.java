package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_ClaveTarjBIC database table.
 * 
 */
@Embeddable
public class Tbl_ClaveTarjBICPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Clave")
	private String fld_Clave;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_ClaveTarjBICPK() {
	}
	public String getFld_Clave() {
		return this.fld_Clave;
	}
	public void setFld_Clave(String fld_Clave) {
		this.fld_Clave = fld_Clave;
	}
	public String getFld_Rut() {
		return this.fld_Rut;
	}
	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_ClaveTarjBICPK)) {
			return false;
		}
		Tbl_ClaveTarjBICPK castOther = (Tbl_ClaveTarjBICPK)other;
		return 
			this.fld_Clave.equals(castOther.fld_Clave)
			&& this.fld_Rut.equals(castOther.fld_Rut);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fld_Clave.hashCode();
		hash = hash * prime + this.fld_Rut.hashCode();
		
		return hash;
	}
}