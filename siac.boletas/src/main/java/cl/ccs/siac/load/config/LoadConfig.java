package cl.ccs.siac.load.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.ejb.Singleton;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class LoadConfig implements ServletContextListener {

	private static final Logger LOG = LogManager.getLogger(LoadConfig.class);

	private static LoadConfig instance = null;

	private Properties props;

	public LoadConfig() {

		String configHome = System.getProperty("TERMINAL_HOME");

		if (configHome == null) {
			LOG.error("No se ha creado variable del sistema para archivo de propiedades.");
		} else {

			String filePath = configHome + "/config/config.properties";
			try {

				this.props = new Properties();
				props.load(new FileInputStream(new File(filePath)));
			} catch (FileNotFoundException e) {
				LOG.error("Archivo de propiedades no encontrado");
				e.printStackTrace();
			} catch (IOException e) {
				LOG.error("Error de I/O al cargar archivo de propiedades.");
				e.printStackTrace();
			}

		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.info("Cargando las configuraciones con fecha " + new Date());
		getInstance();

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.info("Finalizando terminal en progreso con fecha " + new Date());

	}

	public static LoadConfig getInstance() {
		if (instance == null) {
			instance = new LoadConfig();
		}
		return instance;
	}

	public String getValue(String key) {
		return props.getProperty(key);
	}

}
