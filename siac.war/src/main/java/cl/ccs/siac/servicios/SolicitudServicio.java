package cl.ccs.siac.servicios;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.solicitud.SolicitudBusiness;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.PDFResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarInDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoComprInDTO;
import cl.exe.ccs.dto.SolicitudReclamoIngresoOutDTO;
import cl.exe.ccs.resources.Constants;
import cl.exe.ccs.dto.SolicitudReclamoIngresoInDTO;

@Path("/solicitud")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class SolicitudServicio {

	private static final Logger LOG = LogManager.getLogger(SolicitudServicio.class);

	@Inject
	private SolicitudBusiness business;

	@Inject
	private CombosBusiness combosBusiness;

	private boolean validaUsuario(HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if ( userPrincipal == null || userPrincipal.getName() == null ) {
			return false;
		}
		return true;
	}

	@GET
	@Path("/cmbtipo")
	public Response cmbtipo(@Context HttpServletRequest request) {
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			List<ComboDTO> combo = combosBusiness.cmbTipoSolicRecl();
			
			if (combo == null || combo.size() == 0) {
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(combo).build();
			}
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@GET
	@Path("/cmbcateg")
	public Response cmbcateg(@Context HttpServletRequest request, @QueryParam("idSolic") Integer idSolic) {
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			List<ComboDTO> combo = combosBusiness.cmbCategoriaSolic(idSolic);
			
			if (combo == null || combo.size() == 0) {
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(combo).build();
			}
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/ingreso")
	public Response ingreso(@Context HttpServletRequest request, SolicitudReclamoIngresoInDTO params) {
		LOG.info("-> Ingreso SolicitudReclamo: " + params);
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			SolicitudReclamoIngresoOutDTO response = business.ingreso(params);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/buscar")
	public Response buscar(@Context HttpServletRequest request, SolicitudReclamoBuscarInDTO params) {
		LOG.info("-> Buscar SolicitudReclamo: " + params);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			SolicitudReclamoBuscarOutDTO response = business.buscar(params);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/historial")
	public Response historial(@Context HttpServletRequest request, Integer corrSolic) {
		LOG.info("-> Historial SolicitudReclamo: " + corrSolic);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ResponseListDTO<String[]> response = business.historial(corrSolic);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/compr")
	public Response compr(@Context HttpServletRequest request, SolicitudReclamoComprInDTO dto) {
		LOG.info("-> Compr SolicitudReclamo: " + dto);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		GeneradorPDF generador = new GeneradorPDF();
		Map<String, Object> mapa = obtenerMapaCompr(dto);
		
		try {
			byte[] archivo = generador.create("SolicReclCompr", new HashSet(), mapa);
			
			// PDF generado para el servidor
			LOG.info("> Generando PDF en el Servidor..");
			final OutputStream stream = new FileOutputStream(Constants.DIR_CERTIFICATES + '/' + "SR_" + dto.getNumero() + ".pdf");
			stream.write(archivo);
			stream.close();
			
			// PDF de respuesta al usuario
			LOG.info("> Generando PDF de respuesta..");
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);
			pdf.setIdControl(1);
			pdf.setMsgControl("OK");
			
			return Response.ok(pdf).build();
			
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	private Map<String, Object> obtenerMapaCompr(SolicitudReclamoComprInDTO dto) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		params.put("NUMERO", dto.getNumero());
		params.put("TITU_NOMBRE", dto.getTituNombre());
		params.put("TITU_RUT", dto.getTituRut());
		params.put("TITU_FONO", dto.getTituFono());
		params.put("TITU_EMAIL", dto.getTituEmail());
		params.put("TITU_DIREC", dto.getTituDirec());
		params.put("TITU_COMUNA", dto.getTituComuna());
		params.put("TRAM_NOMBRE", dto.getTramNombre());
		params.put("TRAM_RUT", dto.getTramRut());
		params.put("TRAM_FONO", dto.getTramFono());
		params.put("TRAM_EMAIL", dto.getTramEmail());
		params.put("TRAM_DIREC", dto.getTramDirec());
		params.put("TRAM_COMUNA", dto.getTramComuna());
		params.put("CANT_DOCS", dto.getCantDoc());
		params.put("TEXTO", dto.getTexto());
		params.put("COD_USER", dto.getCodUser());
		
		return params;
	}

	@POST
	@Path("/form")
	public Response form(@Context HttpServletRequest request, SolicitudReclamoComprInDTO dto) {
		LOG.info("-> Form SolicitudReclamo: " + dto.getNumero());
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		GeneradorPDF generador = new GeneradorPDF();
		Map<String, Object> mapa = obtenerMapaForm(dto);
		
		try {
			byte[] archivo = generador.create("SolicReclForm", new HashSet(), mapa);
			
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);
			pdf.setIdControl(1);
			pdf.setMsgControl("OK");
			
			return Response.ok(pdf).build();
			
		} catch (Exception e) {
			LOG.error("SolicitudReclamo - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	private Map<String, Object> obtenerMapaForm(SolicitudReclamoComprInDTO dto) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		params.put("TIPO", dto.getNumero());
		params.put("TRAM_NOMBRE", dto.getTramNombre());
		params.put("TRAM_RUT", dto.getTramRut());
		params.put("TRAM_FONO", dto.getTramFono());
		params.put("TRAM_EMAIL", dto.getTramEmail());
		params.put("TRAM_DIREC", dto.getTramDirec() + " " + dto.getTramComuna());
		
		return params;
	}

}
