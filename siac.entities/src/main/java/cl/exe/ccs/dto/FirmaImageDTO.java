package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FirmaImageDTO extends ResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4304366087548962301L;

	private Long idFirma;
	private byte[] imagen;
	
	
	public Long getIdFirma() {
		return idFirma;
	}
	public void setIdFirma(Long idFirma) {
		this.idFirma = idFirma;
	}
	public byte[] getImagen() {
		return imagen;
	}
	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	
	
}
