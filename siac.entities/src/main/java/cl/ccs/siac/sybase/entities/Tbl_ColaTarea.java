package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ColaTareas database table.
 * 
 */
@Entity
@Table(name="Tbl_ColaTareas")
@NamedQuery(name="Tbl_ColaTarea.findAll", query="SELECT t FROM Tbl_ColaTarea t")
public class Tbl_ColaTarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaOrigen")
	private String fld_CodAreaOrigen;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodProceso")
	private short fld_CodProceso;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_Corr")
	private BigDecimal fld_Corr;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrPrecedente")
	private BigDecimal fld_CorrPrecedente;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	@Column(name="Fld_FlagProceso")
	private int fld_FlagProceso;

	@Column(name="Fld_NombreProceso")
	private String fld_NombreProceso;

	@Column(name="Fld_Tarea")
	private String fld_Tarea;

	public Tbl_ColaTarea() {
	}

	public String getFld_CodAreaOrigen() {
		return this.fld_CodAreaOrigen;
	}

	public void setFld_CodAreaOrigen(String fld_CodAreaOrigen) {
		this.fld_CodAreaOrigen = fld_CodAreaOrigen;
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(short fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_Corr() {
		return this.fld_Corr;
	}

	public void setFld_Corr(BigDecimal fld_Corr) {
		this.fld_Corr = fld_Corr;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrPrecedente() {
		return this.fld_CorrPrecedente;
	}

	public void setFld_CorrPrecedente(BigDecimal fld_CorrPrecedente) {
		this.fld_CorrPrecedente = fld_CorrPrecedente;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

	public int getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(int fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

	public String getFld_NombreProceso() {
		return this.fld_NombreProceso;
	}

	public void setFld_NombreProceso(String fld_NombreProceso) {
		this.fld_NombreProceso = fld_NombreProceso;
	}

	public String getFld_Tarea() {
		return this.fld_Tarea;
	}

	public void setFld_Tarea(String fld_Tarea) {
		this.fld_Tarea = fld_Tarea;
	}

}