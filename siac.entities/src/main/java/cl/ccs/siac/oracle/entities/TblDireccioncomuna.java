package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_DIRECCIONCOMUNA database table.
 * 
 */
@Entity
@Table(name="TBL_DIRECCIONCOMUNA")
@NamedQuery(name="TblDireccioncomuna.findAll", query="SELECT t FROM TblDireccioncomuna t")
public class TblDireccioncomuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_DIRECCIONCOMUNA_FLDCORRCOMUNAID_GENERATOR", sequenceName="FLD_CORRCOMUNA_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_DIRECCIONCOMUNA_FLDCORRCOMUNAID_GENERATOR")
	@Column(name="FLD_CORRCOMUNA_ID")
	private long fldCorrcomunaId;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSACOMUNA")
	private String fldGlosacomuna;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblDireccion
	@OneToMany(mappedBy="tblDireccioncomuna")
	private List<TblDireccion> tblDireccions;

	//bi-directional many-to-one association to TblComuna
	@ManyToOne
	@JoinColumn(name="FLD_CODCOMUNA")
	private TblComuna tblComuna;

	public TblDireccioncomuna() {
	}

	public long getFldCorrcomunaId() {
		return this.fldCorrcomunaId;
	}

	public void setFldCorrcomunaId(long fldCorrcomunaId) {
		this.fldCorrcomunaId = fldCorrcomunaId;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosacomuna() {
		return this.fldGlosacomuna;
	}

	public void setFldGlosacomuna(String fldGlosacomuna) {
		this.fldGlosacomuna = fldGlosacomuna;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblDireccion> getTblDireccions() {
		return this.tblDireccions;
	}

	public void setTblDireccions(List<TblDireccion> tblDireccions) {
		this.tblDireccions = tblDireccions;
	}

	public TblDireccion addTblDireccion(TblDireccion tblDireccion) {
		getTblDireccions().add(tblDireccion);
		tblDireccion.setTblDireccioncomuna(this);

		return tblDireccion;
	}

	public TblDireccion removeTblDireccion(TblDireccion tblDireccion) {
		getTblDireccions().remove(tblDireccion);
		tblDireccion.setTblDireccioncomuna(null);

		return tblDireccion;
	}

	public TblComuna getTblComuna() {
		return this.tblComuna;
	}

	public void setTblComuna(TblComuna tblComuna) {
		this.tblComuna = tblComuna;
	}

}