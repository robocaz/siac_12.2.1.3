package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.Date;

public class UsuariosInformeDTO extends TipoDTO implements Serializable{
	
	private String usuario;
	private Integer centroCosto;
	private Date fechaDesde;
	private Date fechaHasta;
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
	
	

}
