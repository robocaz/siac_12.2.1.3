package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_CCOSTO database table.
 * 
 */
@Entity
@Table(name="TBL_CCOSTO")
@NamedQuery(name="TblCcosto.findAll", query="SELECT t FROM TblCcosto t")
public class TblCcosto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CLASIFICACIONCCOSTO")
	private BigDecimal fldClasificacionccosto;

	@Column(name="FLD_CODCCOSTO")
	private BigDecimal fldCodccosto;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FLAGVIGENCIA")
	private BigDecimal fldFlagvigencia;

	@Column(name="FLD_GLOSACCOSTO")
	private String fldGlosaccosto;

	@Column(name="FLD_RAZONSOCIALCCOSTO")
	private String fldRazonsocialccosto;

	@Column(name="FLD_RUTAGENTE")
	private String fldRutagente;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblCcosto() {
	}

	public BigDecimal getFldClasificacionccosto() {
		return this.fldClasificacionccosto;
	}

	public void setFldClasificacionccosto(BigDecimal fldClasificacionccosto) {
		this.fldClasificacionccosto = fldClasificacionccosto;
	}

	public BigDecimal getFldCodccosto() {
		return this.fldCodccosto;
	}

	public void setFldCodccosto(BigDecimal fldCodccosto) {
		this.fldCodccosto = fldCodccosto;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public BigDecimal getFldFlagvigencia() {
		return this.fldFlagvigencia;
	}

	public void setFldFlagvigencia(BigDecimal fldFlagvigencia) {
		this.fldFlagvigencia = fldFlagvigencia;
	}

	public String getFldGlosaccosto() {
		return this.fldGlosaccosto;
	}

	public void setFldGlosaccosto(String fldGlosaccosto) {
		this.fldGlosaccosto = fldGlosaccosto;
	}

	public String getFldRazonsocialccosto() {
		return this.fldRazonsocialccosto;
	}

	public void setFldRazonsocialccosto(String fldRazonsocialccosto) {
		this.fldRazonsocialccosto = fldRazonsocialccosto;
	}

	public String getFldRutagente() {
		return this.fldRutagente;
	}

	public void setFldRutagente(String fldRutagente) {
		this.fldRutagente = fldRutagente;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}