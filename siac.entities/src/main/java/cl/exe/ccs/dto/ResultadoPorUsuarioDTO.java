package cl.exe.ccs.dto;

import java.io.Serializable;

public class ResultadoPorUsuarioDTO implements Serializable{
	
	private Integer corrCaja;
	private String estadoCaja;
	private String fechaInicio;
	private Long montoCalculado;
	private Long montoIngresadoCajera;
	private Long diferenciaPos;
	private Long diferenciaNeg;
	
	
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getEstadoCaja() {
		return estadoCaja;
	}
	public void setEstadoCaja(String estadoCaja) {
		this.estadoCaja = estadoCaja;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Long getMontoCalculado() {
		return montoCalculado;
	}
	public void setMontoCalculado(Long montoCalculado) {
		this.montoCalculado = montoCalculado;
	}
	public Long getMontoIngresadoCajera() {
		return montoIngresadoCajera;
	}
	public void setMontoIngresadoCajera(Long montoIngresadoCajera) {
		this.montoIngresadoCajera = montoIngresadoCajera;
	}
	public Long getDiferenciaPos() {
		return diferenciaPos;
	}
	public void setDiferenciaPos(Long diferenciaPos) {
		this.diferenciaPos = diferenciaPos;
	}
	public Long getDiferenciaNeg() {
		return diferenciaNeg;
	}
	public void setDiferenciaNeg(Long diferenciaNeg) {
		this.diferenciaNeg = diferenciaNeg;
	}
	
	
	
	

}
