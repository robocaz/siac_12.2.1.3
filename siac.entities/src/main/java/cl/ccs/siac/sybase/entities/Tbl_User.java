package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_User database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_User.findAll", query="SELECT t FROM Tbl_User t")
public class Tbl_User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaResponsable")
	private String fld_CodAreaResponsable;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_Flag_AE")
	private boolean fld_Flag_AE;

	@Column(name="Fld_LocalAccessLevel")
	private boolean fld_LocalAccessLevel;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_UserCode")
	private String fld_UserCode;

	@Column(name="Fld_UserPassword")
	private String fld_UserPassword;

	@Column(name="Fld_UserState")
	private String fld_UserState;

	@Column(name="Fld_UserTimeStamp")
	private byte[] fld_UserTimeStamp;

	public Tbl_User() {
	}

	public String getFld_CodAreaResponsable() {
		return this.fld_CodAreaResponsable;
	}

	public void setFld_CodAreaResponsable(String fld_CodAreaResponsable) {
		this.fld_CodAreaResponsable = fld_CodAreaResponsable;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public boolean getFld_Flag_AE() {
		return this.fld_Flag_AE;
	}

	public void setFld_Flag_AE(boolean fld_Flag_AE) {
		this.fld_Flag_AE = fld_Flag_AE;
	}

	public boolean getFld_LocalAccessLevel() {
		return this.fld_LocalAccessLevel;
	}

	public void setFld_LocalAccessLevel(boolean fld_LocalAccessLevel) {
		this.fld_LocalAccessLevel = fld_LocalAccessLevel;
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_UserCode() {
		return this.fld_UserCode;
	}

	public void setFld_UserCode(String fld_UserCode) {
		this.fld_UserCode = fld_UserCode;
	}

	public String getFld_UserPassword() {
		return this.fld_UserPassword;
	}

	public void setFld_UserPassword(String fld_UserPassword) {
		this.fld_UserPassword = fld_UserPassword;
	}

	public String getFld_UserState() {
		return this.fld_UserState;
	}

	public void setFld_UserState(String fld_UserState) {
		this.fld_UserState = fld_UserState;
	}

	public byte[] getFld_UserTimeStamp() {
		return this.fld_UserTimeStamp;
	}

	public void setFld_UserTimeStamp(byte[] fld_UserTimeStamp) {
		this.fld_UserTimeStamp = fld_UserTimeStamp;
	}

}