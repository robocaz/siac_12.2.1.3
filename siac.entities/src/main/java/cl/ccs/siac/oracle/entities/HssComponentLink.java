package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the HSS_COMPONENT_LINKS database table.
 * 
 */
@Entity
@Table(name="HSS_COMPONENT_LINKS")
@NamedQuery(name="HssComponentLink.findAll", query="SELECT h FROM HssComponentLink h")
public class HssComponentLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FROM_COMPONENT_ID")
	private String fromComponentId;

	@Column(name="LINK_TYPE")
	private BigDecimal linkType;

	@Column(name="TO_COMPONENT_ID")
	private String toComponentId;

	public HssComponentLink() {
	}

	public String getFromComponentId() {
		return this.fromComponentId;
	}

	public void setFromComponentId(String fromComponentId) {
		this.fromComponentId = fromComponentId;
	}

	public BigDecimal getLinkType() {
		return this.linkType;
	}

	public void setLinkType(BigDecimal linkType) {
		this.linkType = linkType;
	}

	public String getToComponentId() {
		return this.toComponentId;
	}

	public void setToComponentId(String toComponentId) {
		this.toComponentId = toComponentId;
	}

}