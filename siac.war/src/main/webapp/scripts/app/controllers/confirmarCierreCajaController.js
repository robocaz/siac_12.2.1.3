angular.module('ccs').controller('ConfirmarCierreCajaController', function($scope, CajaResource, $rootScope, $location) {

    $scope.seleccionado = "listado";

    $scope.aceptar = function(){
        angular.element('#confirmarcierrecaja').modal("hide");
        angular.element('#listacierrecaja').modal();
    }

    $scope.cancelar = function(){
        var successCallback = function(data){
            $rootScope.usuario.caja = data.caja;
            angular.element('#confirmarcierrecaja').modal("hide");
            $location.path("/inicio");
        };
        CajaResource.cerrarcajatemporal($rootScope.usuario.caja,  successCallback, $scope.errorCallback);
    }
});
