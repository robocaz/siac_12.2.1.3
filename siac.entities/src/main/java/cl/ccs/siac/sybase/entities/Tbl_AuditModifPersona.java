package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditModifPersona database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditModifPersona.findAll", query="SELECT t FROM Tbl_AuditModifPersona t")
public class Tbl_AuditModifPersona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecModificacionPersona")
	private Timestamp fld_FecModificacionPersona;

	@Column(name="Fld_FolioProt")
	private int fld_FolioProt;

	@Column(name="Fld_MarcaDigVerErroneo")
	private boolean fld_MarcaDigVerErroneo;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_AuditModifPersona() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecModificacionPersona() {
		return this.fld_FecModificacionPersona;
	}

	public void setFld_FecModificacionPersona(Timestamp fld_FecModificacionPersona) {
		this.fld_FecModificacionPersona = fld_FecModificacionPersona;
	}

	public int getFld_FolioProt() {
		return this.fld_FolioProt;
	}

	public void setFld_FolioProt(int fld_FolioProt) {
		this.fld_FolioProt = fld_FolioProt;
	}

	public boolean getFld_MarcaDigVerErroneo() {
		return this.fld_MarcaDigVerErroneo;
	}

	public void setFld_MarcaDigVerErroneo(boolean fld_MarcaDigVerErroneo) {
		this.fld_MarcaDigVerErroneo = fld_MarcaDigVerErroneo;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}