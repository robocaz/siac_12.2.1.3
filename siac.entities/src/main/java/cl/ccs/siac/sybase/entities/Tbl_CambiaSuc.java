package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CambiaSuc database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CambiaSuc.findAll", query="SELECT t FROM Tbl_CambiaSuc t")
public class Tbl_CambiaSuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodLocPubNew")
	private int fld_CodLocPubNew;

	@Column(name="Fld_CodSucNew")
	private BigDecimal fld_CodSucNew;

	@Column(name="Fld_CodSucOld")
	private BigDecimal fld_CodSucOld;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	public Tbl_CambiaSuc() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodLocPubNew() {
		return this.fld_CodLocPubNew;
	}

	public void setFld_CodLocPubNew(int fld_CodLocPubNew) {
		this.fld_CodLocPubNew = fld_CodLocPubNew;
	}

	public BigDecimal getFld_CodSucNew() {
		return this.fld_CodSucNew;
	}

	public void setFld_CodSucNew(BigDecimal fld_CodSucNew) {
		this.fld_CodSucNew = fld_CodSucNew;
	}

	public BigDecimal getFld_CodSucOld() {
		return this.fld_CodSucOld;
	}

	public void setFld_CodSucOld(BigDecimal fld_CodSucOld) {
		this.fld_CodSucOld = fld_CodSucOld;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

}