package cl.exe.ccs.dto.certificados;

import java.io.Serializable;
import java.util.List;

public class ConsCertificadoILPDTO implements Serializable {

	private static final long serialVersionUID = 1631027277270529882L;

	private String codUsuario;
	private String sucursal;
	private String rutAfectado;
	private Long corrMovimiento;
	private String appPat;
	private String appMat;
	private String nombres;
	private int tipoNombre;
	private int numeroBoletin;
	private String fechaPubLP;
	private String codAutenticBic;
	private int resultado;
	private String mensaje;
	private List<CertificadoILPDTO> certILPResult;

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getRutAfectado() {
		return rutAfectado;
	}

	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}

	public Long getCorrMovimiento() {
		return corrMovimiento;
	}

	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}

	public String getAppPat() {
		return appPat;
	}

	public void setAppPat(String appPat) {
		this.appPat = appPat;
	}

	public String getAppMat() {
		return appMat;
	}

	public void setAppMat(String appMat) {
		this.appMat = appMat;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public int getTipoNombre() {
		return tipoNombre;
	}

	public void setTipoNombre(int tipoNombre) {
		this.tipoNombre = tipoNombre;
	}

	public int getNumeroBoletin() {
		return numeroBoletin;
	}

	public void setNumeroBoletin(int numeroBoletin) {
		this.numeroBoletin = numeroBoletin;
	}

	public String getFechaPubLP() {
		return fechaPubLP;
	}

	public void setFechaPubLP(String fechaPubLP) {
		this.fechaPubLP = fechaPubLP;
	}

	public String getCodAutenticBic() {
		return codAutenticBic;
	}

	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<CertificadoILPDTO> getCertILPResult() {
		return certILPResult;
	}

	public void setCertILPResult(List<CertificadoILPDTO> certILPResult) {
		this.certILPResult = certILPResult;
	}

}
