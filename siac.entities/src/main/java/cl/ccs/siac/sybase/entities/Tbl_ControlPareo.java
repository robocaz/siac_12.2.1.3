package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ControlPareo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlPareo.findAll", query="SELECT t FROM Tbl_ControlPareo t")
public class Tbl_ControlPareo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Contador")
	private int fld_Contador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_ControlPareo() {
	}

	public int getFld_Contador() {
		return this.fld_Contador;
	}

	public void setFld_Contador(int fld_Contador) {
		this.fld_Contador = fld_Contador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}