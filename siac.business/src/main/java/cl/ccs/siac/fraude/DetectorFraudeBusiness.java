package cl.ccs.siac.fraude;

import cl.exe.ccs.dto.DetectorFraudeDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface DetectorFraudeBusiness {

	ResponseListDTO<DetectorFraudeDTO> getDetallesFraudePorRut(String rut);

	ResponseDTO validaAutorizacion(DetectorFraudeDTO dataFraude);

}
