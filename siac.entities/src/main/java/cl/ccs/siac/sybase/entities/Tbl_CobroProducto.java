package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CobroProductos database table.
 * 
 */
@Entity
@Table(name="Tbl_CobroProductos")
@NamedQuery(name="Tbl_CobroProducto.findAll", query="SELECT t FROM Tbl_CobroProducto t")
public class Tbl_CobroProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda_CBase")
	private byte fld_CodMoneda_CBase;

	@Column(name="Fld_CodMoneda_CMin")
	private byte fld_CodMoneda_CMin;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_CorrCobroProd_Id")
	private BigDecimal fld_CorrCobroProd_Id;

	@Column(name="Fld_CostoBase")
	private BigDecimal fld_CostoBase;

	@Column(name="Fld_CostoMinimo")
	private BigDecimal fld_CostoMinimo;

	@Column(name="Fld_DescripCobro")
	private String fld_DescripCobro;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CobroProducto() {
	}

	public byte getFld_CodMoneda_CBase() {
		return this.fld_CodMoneda_CBase;
	}

	public void setFld_CodMoneda_CBase(byte fld_CodMoneda_CBase) {
		this.fld_CodMoneda_CBase = fld_CodMoneda_CBase;
	}

	public byte getFld_CodMoneda_CMin() {
		return this.fld_CodMoneda_CMin;
	}

	public void setFld_CodMoneda_CMin(byte fld_CodMoneda_CMin) {
		this.fld_CodMoneda_CMin = fld_CodMoneda_CMin;
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public BigDecimal getFld_CorrCobroProd_Id() {
		return this.fld_CorrCobroProd_Id;
	}

	public void setFld_CorrCobroProd_Id(BigDecimal fld_CorrCobroProd_Id) {
		this.fld_CorrCobroProd_Id = fld_CorrCobroProd_Id;
	}

	public BigDecimal getFld_CostoBase() {
		return this.fld_CostoBase;
	}

	public void setFld_CostoBase(BigDecimal fld_CostoBase) {
		this.fld_CostoBase = fld_CostoBase;
	}

	public BigDecimal getFld_CostoMinimo() {
		return this.fld_CostoMinimo;
	}

	public void setFld_CostoMinimo(BigDecimal fld_CostoMinimo) {
		this.fld_CostoMinimo = fld_CostoMinimo;
	}

	public String getFld_DescripCobro() {
		return this.fld_DescripCobro;
	}

	public void setFld_DescripCobro(String fld_DescripCobro) {
		this.fld_DescripCobro = fld_DescripCobro;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}