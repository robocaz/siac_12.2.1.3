package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CorrUnico database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CorrUnico.findAll", query="SELECT t FROM Tbl_CorrUnico t")
public class Tbl_CorrUnico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrUnico_Id")
	private BigDecimal fld_CorrUnico_Id;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	public Tbl_CorrUnico() {
	}

	public BigDecimal getFld_CorrUnico_Id() {
		return this.fld_CorrUnico_Id;
	}

	public void setFld_CorrUnico_Id(BigDecimal fld_CorrUnico_Id) {
		this.fld_CorrUnico_Id = fld_CorrUnico_Id;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

}