package cl.ccs.siac.dao;

import cl.exe.ccs.dto.FacturaDTO;
import cl.exe.ccs.dto.ReemplazaBoletaFacturaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ConsultaRangoBolFacDTO;

public interface FacturaDao {

	public FacturaDTO consultaFacturas(FacturaDTO facturaDTO);
	public FacturaDTO grabaFactura(FacturaDTO facturaDTO);
	public FacturaDTO eliminaFactura(FacturaDTO facturaDTO);
	public FacturaDTO actualizarTransaccion(FacturaDTO facturaDTO);
	public FacturaDTO folioFactura(FacturaDTO facturaDTO);
	public FacturaDTO consultaRut(FacturaDTO facturaDTO);
	public ConsultaRangoBolFacDTO consultaRangoBF(ConsultaRangoBolFacDTO consultaRango);
	public ResponseDTO reemplazarBoleta(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO);
	public ResponseDTO reemplazarBoletaCero(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO);
	public ResponseDTO obtenerIVA();
}
