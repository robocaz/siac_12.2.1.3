package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the Tbl_Firmas database table.
 * 
 */
@Entity
@Table(name="Tbl_Firmas")
@NamedQuery(name="Tbl_Firma.findAll", query="SELECT t FROM Tbl_Firma t")
public class Tbl_Firma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Fld_CorrFirma")
	private BigDecimal fld_CorrFirma;

	@Lob
	@Column(name="Fld_Firma")
	private byte[] fld_Firma;

	public Tbl_Firma() {
	}

	public BigDecimal getFld_CorrFirma() {
		return this.fld_CorrFirma;
	}

	public void setFld_CorrFirma(BigDecimal fld_CorrFirma) {
		this.fld_CorrFirma = fld_CorrFirma;
	}

	public byte[] getFld_Firma() {
		return this.fld_Firma;
	}

	public void setFld_Firma(byte[] fld_Firma) {
		this.fld_Firma = fld_Firma;
	}

}