angular.module("ccs").directive("focusElement", function($timeout) {
  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      scope.$on(attrs.focusElement, function(e) {
        $timeout((function() {
          element[0].focus();
        }), 10);
      });
    }
  };
});
