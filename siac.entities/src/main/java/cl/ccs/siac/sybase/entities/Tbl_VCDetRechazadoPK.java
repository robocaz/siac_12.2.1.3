package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_VCDetRechazados database table.
 * 
 */
@Embeddable
public class Tbl_VCDetRechazadoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvioVC")
	private long fld_CorrEnvioVC;

	@Column(name="Fld_FolioVC")
	private int fld_FolioVC;

	public Tbl_VCDetRechazadoPK() {
	}
	public long getFld_CorrEnvioVC() {
		return this.fld_CorrEnvioVC;
	}
	public void setFld_CorrEnvioVC(long fld_CorrEnvioVC) {
		this.fld_CorrEnvioVC = fld_CorrEnvioVC;
	}
	public int getFld_FolioVC() {
		return this.fld_FolioVC;
	}
	public void setFld_FolioVC(int fld_FolioVC) {
		this.fld_FolioVC = fld_FolioVC;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_VCDetRechazadoPK)) {
			return false;
		}
		Tbl_VCDetRechazadoPK castOther = (Tbl_VCDetRechazadoPK)other;
		return 
			(this.fld_CorrEnvioVC == castOther.fld_CorrEnvioVC)
			&& (this.fld_FolioVC == castOther.fld_FolioVC);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_CorrEnvioVC ^ (this.fld_CorrEnvioVC >>> 32)));
		hash = hash * prime + this.fld_FolioVC;
		
		return hash;
	}
}