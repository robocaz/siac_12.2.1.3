package cl.ccs.siac.printer;

import cl.ccs.siac.dto.BoletaTransaccionDTO;


public interface Impresion {
	
	public String imprimeBoleta(BoletaTransaccionDTO boletaTransaccionDTO);
	public String imprimeBoletaPortCom(BoletaTransaccionDTO boletaTransaccionDTO);
	public String imprimeBoletaPrueba();

}
