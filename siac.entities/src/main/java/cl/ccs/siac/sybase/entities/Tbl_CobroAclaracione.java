package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CobroAclaraciones database table.
 * 
 */
@Entity
@Table(name="Tbl_CobroAclaraciones")
@NamedQuery(name="Tbl_CobroAclaracione.findAll", query="SELECT t FROM Tbl_CobroAclaracione t")
public class Tbl_CobroAclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrTablaAcl_Id")
	private BigDecimal fld_CorrTablaAcl_Id;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoCobro")
	private String fld_TipoCobro;

	public Tbl_CobroAclaracione() {
	}

	public BigDecimal getFld_CorrTablaAcl_Id() {
		return this.fld_CorrTablaAcl_Id;
	}

	public void setFld_CorrTablaAcl_Id(BigDecimal fld_CorrTablaAcl_Id) {
		this.fld_CorrTablaAcl_Id = fld_CorrTablaAcl_Id;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoCobro() {
		return this.fld_TipoCobro;
	}

	public void setFld_TipoCobro(String fld_TipoCobro) {
		this.fld_TipoCobro = fld_TipoCobro;
	}

}