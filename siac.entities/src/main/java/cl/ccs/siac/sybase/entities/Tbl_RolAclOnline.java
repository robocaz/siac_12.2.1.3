package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RolAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RolAclOnline.findAll", query="SELECT t FROM Tbl_RolAclOnline t")
public class Tbl_RolAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodigoRol")
	private String fld_CodigoRol;

	@Column(name="Fld_GlosaRol")
	private String fld_GlosaRol;

	public Tbl_RolAclOnline() {
	}

	public String getFld_CodigoRol() {
		return this.fld_CodigoRol;
	}

	public void setFld_CodigoRol(String fld_CodigoRol) {
		this.fld_CodigoRol = fld_CodigoRol;
	}

	public String getFld_GlosaRol() {
		return this.fld_GlosaRol;
	}

	public void setFld_GlosaRol(String fld_GlosaRol) {
		this.fld_GlosaRol = fld_GlosaRol;
	}

}