package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_DeudaVigRUT database table.
 * 
 */
@Entity
@NamedQuery(name="Ley_DeudaVigRUT.findAll", query="SELECT l FROM Ley_DeudaVigRUT l")
public class Ley_DeudaVigRUT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_Flag1")
	private boolean fld_Flag1;

	@Column(name="Fld_Flag2")
	private boolean fld_Flag2;

	@Column(name="Fld_Flag3")
	private boolean fld_Flag3;

	@Column(name="Fld_Flag4")
	private boolean fld_Flag4;

	@Column(name="Fld_MontoMCC")
	private BigDecimal fld_MontoMCC;

	@Column(name="Fld_MontoMCCPost")
	private BigDecimal fld_MontoMCCPost;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_MontoProtPost")
	private BigDecimal fld_MontoProtPost;

	@Column(name="Fld_NroMCC")
	private short fld_NroMCC;

	@Column(name="Fld_NroMCCPost")
	private short fld_NroMCCPost;

	@Column(name="Fld_NroProt")
	private short fld_NroProt;

	@Column(name="Fld_NroProtPost")
	private short fld_NroProtPost;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Ley_DeudaVigRUT() {
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public boolean getFld_Flag1() {
		return this.fld_Flag1;
	}

	public void setFld_Flag1(boolean fld_Flag1) {
		this.fld_Flag1 = fld_Flag1;
	}

	public boolean getFld_Flag2() {
		return this.fld_Flag2;
	}

	public void setFld_Flag2(boolean fld_Flag2) {
		this.fld_Flag2 = fld_Flag2;
	}

	public boolean getFld_Flag3() {
		return this.fld_Flag3;
	}

	public void setFld_Flag3(boolean fld_Flag3) {
		this.fld_Flag3 = fld_Flag3;
	}

	public boolean getFld_Flag4() {
		return this.fld_Flag4;
	}

	public void setFld_Flag4(boolean fld_Flag4) {
		this.fld_Flag4 = fld_Flag4;
	}

	public BigDecimal getFld_MontoMCC() {
		return this.fld_MontoMCC;
	}

	public void setFld_MontoMCC(BigDecimal fld_MontoMCC) {
		this.fld_MontoMCC = fld_MontoMCC;
	}

	public BigDecimal getFld_MontoMCCPost() {
		return this.fld_MontoMCCPost;
	}

	public void setFld_MontoMCCPost(BigDecimal fld_MontoMCCPost) {
		this.fld_MontoMCCPost = fld_MontoMCCPost;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public BigDecimal getFld_MontoProtPost() {
		return this.fld_MontoProtPost;
	}

	public void setFld_MontoProtPost(BigDecimal fld_MontoProtPost) {
		this.fld_MontoProtPost = fld_MontoProtPost;
	}

	public short getFld_NroMCC() {
		return this.fld_NroMCC;
	}

	public void setFld_NroMCC(short fld_NroMCC) {
		this.fld_NroMCC = fld_NroMCC;
	}

	public short getFld_NroMCCPost() {
		return this.fld_NroMCCPost;
	}

	public void setFld_NroMCCPost(short fld_NroMCCPost) {
		this.fld_NroMCCPost = fld_NroMCCPost;
	}

	public short getFld_NroProt() {
		return this.fld_NroProt;
	}

	public void setFld_NroProt(short fld_NroProt) {
		this.fld_NroProt = fld_NroProt;
	}

	public short getFld_NroProtPost() {
		return this.fld_NroProtPost;
	}

	public void setFld_NroProtPost(short fld_NroProtPost) {
		this.fld_NroProtPost = fld_NroProtPost;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}