package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetCtaCte.findAll", query="SELECT t FROM Tb_CeIdDetCtaCte t")
public class Tb_CeIdDetCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETCTACTE_FLD_CORRCTACTE_ID_GENERATOR", sequenceName="FLD_CORRCTACTE_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETCTACTE_FLD_CORRCTACTE_ID_GENERATOR")
	@Column(name="Fld_CorrCtaCte_Id")
	private long fld_CorrCtaCte_Id;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodigoBanco")
	private int fld_CodigoBanco;

	@Column(name="Fld_CodPlazaBancaria")
	private short fld_CodPlazaBancaria;

	@Column(name="Fld_CodSucBco")
	private int fld_CodSucBco;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecAperturaCta")
	private Timestamp fld_FecAperturaCta;

	@Column(name="Fld_FecCierreCta")
	private Timestamp fld_FecCierreCta;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_MarcaBipersonal")
	private boolean fld_MarcaBipersonal;

	@Column(name="Fld_MarcaBloqueo")
	private boolean fld_MarcaBloqueo;

	@Column(name="Fld_MarcaEstadoCtaCte")
	private boolean fld_MarcaEstadoCtaCte;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_NroCtaCteFiltrado")
	private String fld_NroCtaCteFiltrado;

	@Column(name="Fld_NroCtaCteReal")
	private String fld_NroCtaCteReal;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdDetErrCtasCte
	@OneToMany(mappedBy="tbCeIdDetCtaCte")
	private List<Tb_CeIdDetErrCtasCte> tbCeIdDetErrCtasCtes;

	//bi-directional many-to-one association to Tb_CeIdRelNombreCtaCte
	@OneToMany(mappedBy="tbCeIdDetCtaCte")
	private List<Tb_CeIdRelNombreCtaCte> tbCeIdRelNombreCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdSegtoCtaCte
	@OneToMany(mappedBy="tbCeIdDetCtaCte")
	private List<Tb_CeIdSegtoCtaCte> tbCeIdSegtoCtaCtes;

	public Tb_CeIdDetCtaCte() {
	}

	public long getFld_CorrCtaCte_Id() {
		return this.fld_CorrCtaCte_Id;
	}

	public void setFld_CorrCtaCte_Id(long fld_CorrCtaCte_Id) {
		this.fld_CorrCtaCte_Id = fld_CorrCtaCte_Id;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public int getFld_CodigoBanco() {
		return this.fld_CodigoBanco;
	}

	public void setFld_CodigoBanco(int fld_CodigoBanco) {
		this.fld_CodigoBanco = fld_CodigoBanco;
	}

	public short getFld_CodPlazaBancaria() {
		return this.fld_CodPlazaBancaria;
	}

	public void setFld_CodPlazaBancaria(short fld_CodPlazaBancaria) {
		this.fld_CodPlazaBancaria = fld_CodPlazaBancaria;
	}

	public int getFld_CodSucBco() {
		return this.fld_CodSucBco;
	}

	public void setFld_CodSucBco(int fld_CodSucBco) {
		this.fld_CodSucBco = fld_CodSucBco;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecAperturaCta() {
		return this.fld_FecAperturaCta;
	}

	public void setFld_FecAperturaCta(Timestamp fld_FecAperturaCta) {
		this.fld_FecAperturaCta = fld_FecAperturaCta;
	}

	public Timestamp getFld_FecCierreCta() {
		return this.fld_FecCierreCta;
	}

	public void setFld_FecCierreCta(Timestamp fld_FecCierreCta) {
		this.fld_FecCierreCta = fld_FecCierreCta;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public boolean getFld_MarcaBipersonal() {
		return this.fld_MarcaBipersonal;
	}

	public void setFld_MarcaBipersonal(boolean fld_MarcaBipersonal) {
		this.fld_MarcaBipersonal = fld_MarcaBipersonal;
	}

	public boolean getFld_MarcaBloqueo() {
		return this.fld_MarcaBloqueo;
	}

	public void setFld_MarcaBloqueo(boolean fld_MarcaBloqueo) {
		this.fld_MarcaBloqueo = fld_MarcaBloqueo;
	}

	public boolean getFld_MarcaEstadoCtaCte() {
		return this.fld_MarcaEstadoCtaCte;
	}

	public void setFld_MarcaEstadoCtaCte(boolean fld_MarcaEstadoCtaCte) {
		this.fld_MarcaEstadoCtaCte = fld_MarcaEstadoCtaCte;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public String getFld_NroCtaCteFiltrado() {
		return this.fld_NroCtaCteFiltrado;
	}

	public void setFld_NroCtaCteFiltrado(String fld_NroCtaCteFiltrado) {
		this.fld_NroCtaCteFiltrado = fld_NroCtaCteFiltrado;
	}

	public String getFld_NroCtaCteReal() {
		return this.fld_NroCtaCteReal;
	}

	public void setFld_NroCtaCteReal(String fld_NroCtaCteReal) {
		this.fld_NroCtaCteReal = fld_NroCtaCteReal;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public List<Tb_CeIdDetErrCtasCte> getTbCeIdDetErrCtasCtes() {
		return this.tbCeIdDetErrCtasCtes;
	}

	public void setTbCeIdDetErrCtasCtes(List<Tb_CeIdDetErrCtasCte> tbCeIdDetErrCtasCtes) {
		this.tbCeIdDetErrCtasCtes = tbCeIdDetErrCtasCtes;
	}

	public Tb_CeIdDetErrCtasCte addTbCeIdDetErrCtasCte(Tb_CeIdDetErrCtasCte tbCeIdDetErrCtasCte) {
		getTbCeIdDetErrCtasCtes().add(tbCeIdDetErrCtasCte);
		tbCeIdDetErrCtasCte.setTbCeIdDetCtaCte(this);

		return tbCeIdDetErrCtasCte;
	}

	public Tb_CeIdDetErrCtasCte removeTbCeIdDetErrCtasCte(Tb_CeIdDetErrCtasCte tbCeIdDetErrCtasCte) {
		getTbCeIdDetErrCtasCtes().remove(tbCeIdDetErrCtasCte);
		tbCeIdDetErrCtasCte.setTbCeIdDetCtaCte(null);

		return tbCeIdDetErrCtasCte;
	}

	public List<Tb_CeIdRelNombreCtaCte> getTbCeIdRelNombreCtaCtes() {
		return this.tbCeIdRelNombreCtaCtes;
	}

	public void setTbCeIdRelNombreCtaCtes(List<Tb_CeIdRelNombreCtaCte> tbCeIdRelNombreCtaCtes) {
		this.tbCeIdRelNombreCtaCtes = tbCeIdRelNombreCtaCtes;
	}

	public Tb_CeIdRelNombreCtaCte addTbCeIdRelNombreCtaCte(Tb_CeIdRelNombreCtaCte tbCeIdRelNombreCtaCte) {
		getTbCeIdRelNombreCtaCtes().add(tbCeIdRelNombreCtaCte);
		tbCeIdRelNombreCtaCte.setTbCeIdDetCtaCte(this);

		return tbCeIdRelNombreCtaCte;
	}

	public Tb_CeIdRelNombreCtaCte removeTbCeIdRelNombreCtaCte(Tb_CeIdRelNombreCtaCte tbCeIdRelNombreCtaCte) {
		getTbCeIdRelNombreCtaCtes().remove(tbCeIdRelNombreCtaCte);
		tbCeIdRelNombreCtaCte.setTbCeIdDetCtaCte(null);

		return tbCeIdRelNombreCtaCte;
	}

	public List<Tb_CeIdSegtoCtaCte> getTbCeIdSegtoCtaCtes() {
		return this.tbCeIdSegtoCtaCtes;
	}

	public void setTbCeIdSegtoCtaCtes(List<Tb_CeIdSegtoCtaCte> tbCeIdSegtoCtaCtes) {
		this.tbCeIdSegtoCtaCtes = tbCeIdSegtoCtaCtes;
	}

	public Tb_CeIdSegtoCtaCte addTbCeIdSegtoCtaCte(Tb_CeIdSegtoCtaCte tbCeIdSegtoCtaCte) {
		getTbCeIdSegtoCtaCtes().add(tbCeIdSegtoCtaCte);
		tbCeIdSegtoCtaCte.setTbCeIdDetCtaCte(this);

		return tbCeIdSegtoCtaCte;
	}

	public Tb_CeIdSegtoCtaCte removeTbCeIdSegtoCtaCte(Tb_CeIdSegtoCtaCte tbCeIdSegtoCtaCte) {
		getTbCeIdSegtoCtaCtes().remove(tbCeIdSegtoCtaCte);
		tbCeIdSegtoCtaCte.setTbCeIdDetCtaCte(null);

		return tbCeIdSegtoCtaCte;
	}

}