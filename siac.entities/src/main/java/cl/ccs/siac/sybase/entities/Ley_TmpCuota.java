package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Ley_TmpCuotas database table.
 * 
 */
@Entity
@Table(name="Ley_TmpCuotas")
@NamedQuery(name="Ley_TmpCuota.findAll", query="SELECT l FROM Ley_TmpCuota l")
public class Ley_TmpCuota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Cuo_CodCliente")
	private String cuo_CodCliente;

	@Column(name="Cuo_FecVenc")
	private Timestamp cuo_FecVenc;

	@Column(name="Cuo_Moneda")
	private String cuo_Moneda;

	@Column(name="Cuo_Monto")
	private BigDecimal cuo_Monto;

	@Column(name="Cuo_NumDocu")
	private short cuo_NumDocu;

	@Column(name="Cuo_Rut")
	private String cuo_Rut;

	@Column(name="Cuo_RutRel")
	private String cuo_RutRel;

	@Column(name="Cuo_TipoDocu")
	private String cuo_TipoDocu;

	@Column(name="Cuo_TipoInfo")
	private String cuo_TipoInfo;

	@Column(name="Cuo_TipoRelacion")
	private String cuo_TipoRelacion;

	@Column(name="Cuo_Vigencia")
	private String cuo_Vigencia;

	public Ley_TmpCuota() {
	}

	public String getCuo_CodCliente() {
		return this.cuo_CodCliente;
	}

	public void setCuo_CodCliente(String cuo_CodCliente) {
		this.cuo_CodCliente = cuo_CodCliente;
	}

	public Timestamp getCuo_FecVenc() {
		return this.cuo_FecVenc;
	}

	public void setCuo_FecVenc(Timestamp cuo_FecVenc) {
		this.cuo_FecVenc = cuo_FecVenc;
	}

	public String getCuo_Moneda() {
		return this.cuo_Moneda;
	}

	public void setCuo_Moneda(String cuo_Moneda) {
		this.cuo_Moneda = cuo_Moneda;
	}

	public BigDecimal getCuo_Monto() {
		return this.cuo_Monto;
	}

	public void setCuo_Monto(BigDecimal cuo_Monto) {
		this.cuo_Monto = cuo_Monto;
	}

	public short getCuo_NumDocu() {
		return this.cuo_NumDocu;
	}

	public void setCuo_NumDocu(short cuo_NumDocu) {
		this.cuo_NumDocu = cuo_NumDocu;
	}

	public String getCuo_Rut() {
		return this.cuo_Rut;
	}

	public void setCuo_Rut(String cuo_Rut) {
		this.cuo_Rut = cuo_Rut;
	}

	public String getCuo_RutRel() {
		return this.cuo_RutRel;
	}

	public void setCuo_RutRel(String cuo_RutRel) {
		this.cuo_RutRel = cuo_RutRel;
	}

	public String getCuo_TipoDocu() {
		return this.cuo_TipoDocu;
	}

	public void setCuo_TipoDocu(String cuo_TipoDocu) {
		this.cuo_TipoDocu = cuo_TipoDocu;
	}

	public String getCuo_TipoInfo() {
		return this.cuo_TipoInfo;
	}

	public void setCuo_TipoInfo(String cuo_TipoInfo) {
		this.cuo_TipoInfo = cuo_TipoInfo;
	}

	public String getCuo_TipoRelacion() {
		return this.cuo_TipoRelacion;
	}

	public void setCuo_TipoRelacion(String cuo_TipoRelacion) {
		this.cuo_TipoRelacion = cuo_TipoRelacion;
	}

	public String getCuo_Vigencia() {
		return this.cuo_Vigencia;
	}

	public void setCuo_Vigencia(String cuo_Vigencia) {
		this.cuo_Vigencia = cuo_Vigencia;
	}

}