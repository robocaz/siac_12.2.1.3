package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_FUENTENOMBRE database table.
 * 
 */
@Entity
@Table(name="TBL_FUENTENOMBRE")
@NamedQuery(name="TblFuentenombre.findAll", query="SELECT t FROM TblFuentenombre t")
public class TblFuentenombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODFUENTE")
	private BigDecimal fldCodfuente;

	@Column(name="FLD_GLOSAFUENTE")
	private String fldGlosafuente;

	@Column(name="FLD_PRIORIDAD")
	private BigDecimal fldPrioridad;

	public TblFuentenombre() {
	}

	public BigDecimal getFldCodfuente() {
		return this.fldCodfuente;
	}

	public void setFldCodfuente(BigDecimal fldCodfuente) {
		this.fldCodfuente = fldCodfuente;
	}

	public String getFldGlosafuente() {
		return this.fldGlosafuente;
	}

	public void setFldGlosafuente(String fldGlosafuente) {
		this.fldGlosafuente = fldGlosafuente;
	}

	public BigDecimal getFldPrioridad() {
		return this.fldPrioridad;
	}

	public void setFldPrioridad(BigDecimal fldPrioridad) {
		this.fldPrioridad = fldPrioridad;
	}

}