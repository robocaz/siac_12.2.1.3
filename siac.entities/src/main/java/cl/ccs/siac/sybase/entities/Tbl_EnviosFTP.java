package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EnviosFTP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosFTP.findAll", query="SELECT t FROM Tbl_EnviosFTP t")
public class Tbl_EnviosFTP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FecGeneraError")
	private Timestamp fld_FecGeneraError;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_GeneraError")
	private boolean fld_GeneraError;

	@Column(name="Fld_NomArchivo")
	private String fld_NomArchivo;

	@Column(name="Fld_TipoProceso")
	private String fld_TipoProceso;

	@Column(name="Fld_TotByteFTP")
	private int fld_TotByteFTP;

	@Column(name="Fld_TotRegErroneos")
	private int fld_TotRegErroneos;

	@Column(name="Fld_TotRegFTP")
	private int fld_TotRegFTP;

	public Tbl_EnviosFTP() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public Timestamp getFld_FecGeneraError() {
		return this.fld_FecGeneraError;
	}

	public void setFld_FecGeneraError(Timestamp fld_FecGeneraError) {
		this.fld_FecGeneraError = fld_FecGeneraError;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_GeneraError() {
		return this.fld_GeneraError;
	}

	public void setFld_GeneraError(boolean fld_GeneraError) {
		this.fld_GeneraError = fld_GeneraError;
	}

	public String getFld_NomArchivo() {
		return this.fld_NomArchivo;
	}

	public void setFld_NomArchivo(String fld_NomArchivo) {
		this.fld_NomArchivo = fld_NomArchivo;
	}

	public String getFld_TipoProceso() {
		return this.fld_TipoProceso;
	}

	public void setFld_TipoProceso(String fld_TipoProceso) {
		this.fld_TipoProceso = fld_TipoProceso;
	}

	public int getFld_TotByteFTP() {
		return this.fld_TotByteFTP;
	}

	public void setFld_TotByteFTP(int fld_TotByteFTP) {
		this.fld_TotByteFTP = fld_TotByteFTP;
	}

	public int getFld_TotRegErroneos() {
		return this.fld_TotRegErroneos;
	}

	public void setFld_TotRegErroneos(int fld_TotRegErroneos) {
		this.fld_TotRegErroneos = fld_TotRegErroneos;
	}

	public int getFld_TotRegFTP() {
		return this.fld_TotRegFTP;
	}

	public void setFld_TotRegFTP(int fld_TotRegFTP) {
		this.fld_TotRegFTP = fld_TotRegFTP;
	}

}