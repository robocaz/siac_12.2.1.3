package cl.exe.ccs.dto;

import java.io.Serializable;

public class DetalleAclaracionDTO extends ResponseDTO implements Serializable{
	
	
	
	private Long corrAcl;
	private String titular;
	private Long corrProt;
	private String codServicio;
	private String glosaEmisor;
	private String rutAfectado;
	private String nombre;
	private String tipoDocAclaracion;
	private String tipoDocumento;
	private String glosaCorta;
	private Double montoProt;
	private String fechaProt;
	private String fechaPago;
	private Integer nroOper4Dig;
	private Double ptjPareo;
	private Long montoMovimiento;
	private Long montoDiferencia;
	private String boletaFactura;
	private Integer numeroBoletaFactura;
	
	
	public Long getCorrAcl() {
		return corrAcl;
	}
	public void setCorrAcl(Long corrAcl) {
		this.corrAcl = corrAcl;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public Long getCorrProt() {
		return corrProt;
	}
	public void setCorrProt(Long corrProt) {
		this.corrProt = corrProt;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getGlosaEmisor() {
		return glosaEmisor;
	}
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoDocAclaracion() {
		return tipoDocAclaracion;
	}
	public void setTipoDocAclaracion(String tipoDocAclaracion) {
		this.tipoDocAclaracion = tipoDocAclaracion;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getGlosaCorta() {
		return glosaCorta;
	}
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
	public Double getMontoProt() {
		return montoProt;
	}
	public void setMontoProt(Double montoProt) {
		this.montoProt = montoProt;
	}
	public String getFechaProt() {
		return fechaProt;
	}
	public void setFechaProt(String fechaProt) {
		this.fechaProt = fechaProt;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}
	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}
	public Double getPtjPareo() {
		return ptjPareo;
	}
	public void setPtjPareo(Double ptjPareo) {
		this.ptjPareo = ptjPareo;
	}
	public Long getMontoMovimiento() {
		return montoMovimiento;
	}
	public void setMontoMovimiento(Long montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}
	public Long getMontoDiferencia() {
		return montoDiferencia;
	}
	public void setMontoDiferencia(Long montoDiferencia) {
		this.montoDiferencia = montoDiferencia;
	}
	public String getBoletaFactura() {
		return boletaFactura;
	}
	public void setBoletaFactura(String boletaFactura) {
		this.boletaFactura = boletaFactura;
	}
	public Integer getNumeroBoletaFactura() {
		return numeroBoletaFactura;
	}
	public void setNumeroBoletaFactura(Integer numeroBoletaFactura) {
		this.numeroBoletaFactura = numeroBoletaFactura;
	}
	
	
	
	

}
