package cl.ccs.siac.dto;

public class SucursalDTO extends ResponseDTO{
	
	
	private String tipoEmisor;
	private String codEmisor;
	private Integer codSucursal;
	private String glosaSucursal;
	
	
	public String getTipoEmisor() {
		return tipoEmisor;
	}
	public void setTipoEmisor(String tipoEmisor) {
		this.tipoEmisor = tipoEmisor;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public Integer getCodSucursal() {
		return codSucursal;
	}
	public void setCodSucursal(Integer codSucursal) {
		this.codSucursal = codSucursal;
	}
	public String getGlosaSucursal() {
		return glosaSucursal;
	}
	public void setGlosaSucursal(String glosaSucursal) {
		this.glosaSucursal = glosaSucursal;
	}
	

	
	
}
