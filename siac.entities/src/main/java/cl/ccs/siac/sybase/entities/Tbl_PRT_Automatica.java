package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the Tbl_PRT_Automaticas database table.
 * 
 */
@Entity
@Table(name="Tbl_PRT_Automaticas")
@NamedQuery(name="Tbl_PRT_Automatica.findAll", query="SELECT t FROM Tbl_PRT_Automatica t")
public class Tbl_PRT_Automatica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_AnoFabricacion")
	private short fld_AnoFabricacion;

	@Column(name="Fld_CodPta")
	private String fld_CodPta;

	@Column(name="Fld_Estacion01")
	private String fld_Estacion01;

	@Column(name="Fld_Estacion02")
	private String fld_Estacion02;

	@Column(name="Fld_Estacion03")
	private String fld_Estacion03;

	@Column(name="Fld_Estacion04")
	private String fld_Estacion04;

	@Column(name="Fld_Estacion05")
	private String fld_Estacion05;

	@Column(name="Fld_Estacion06")
	private String fld_Estacion06;

	@Column(name="Fld_Estacion07")
	private String fld_Estacion07;

	@Column(name="Fld_Estacion08")
	private String fld_Estacion08;

	@Column(name="Fld_Estacion09")
	private String fld_Estacion09;

	@Column(name="Fld_Estacion10")
	private String fld_Estacion10;

	@Column(name="Fld_Estacion11")
	private String fld_Estacion11;

	@Temporal(TemporalType.DATE)
	@Column(name="Fld_FecRevision")
	private Date fld_FecRevision;

	@Temporal(TemporalType.DATE)
	@Column(name="Fld_FecVencimiento")
	private Date fld_FecVencimiento;

	@Column(name="Fld_Kilometraje")
	private int fld_Kilometraje;

	@Column(name="Fld_Marca")
	private String fld_Marca;

	@Column(name="Fld_Modelo")
	private String fld_Modelo;

	@Column(name="Fld_NumCertificado")
	private String fld_NumCertificado;

	@Column(name="Fld_NumChasis")
	private String fld_NumChasis;

	@Column(name="Fld_NumMotor")
	private String fld_NumMotor;

	@Column(name="Fld_Placa")
	private String fld_Placa;

	@Column(name="Fld_ResultadoPrt")
	private String fld_ResultadoPrt;

	@Column(name="Fld_Vin")
	private String fld_Vin;

	public Tbl_PRT_Automatica() {
	}

	public short getFld_AnoFabricacion() {
		return this.fld_AnoFabricacion;
	}

	public void setFld_AnoFabricacion(short fld_AnoFabricacion) {
		this.fld_AnoFabricacion = fld_AnoFabricacion;
	}

	public String getFld_CodPta() {
		return this.fld_CodPta;
	}

	public void setFld_CodPta(String fld_CodPta) {
		this.fld_CodPta = fld_CodPta;
	}

	public String getFld_Estacion01() {
		return this.fld_Estacion01;
	}

	public void setFld_Estacion01(String fld_Estacion01) {
		this.fld_Estacion01 = fld_Estacion01;
	}

	public String getFld_Estacion02() {
		return this.fld_Estacion02;
	}

	public void setFld_Estacion02(String fld_Estacion02) {
		this.fld_Estacion02 = fld_Estacion02;
	}

	public String getFld_Estacion03() {
		return this.fld_Estacion03;
	}

	public void setFld_Estacion03(String fld_Estacion03) {
		this.fld_Estacion03 = fld_Estacion03;
	}

	public String getFld_Estacion04() {
		return this.fld_Estacion04;
	}

	public void setFld_Estacion04(String fld_Estacion04) {
		this.fld_Estacion04 = fld_Estacion04;
	}

	public String getFld_Estacion05() {
		return this.fld_Estacion05;
	}

	public void setFld_Estacion05(String fld_Estacion05) {
		this.fld_Estacion05 = fld_Estacion05;
	}

	public String getFld_Estacion06() {
		return this.fld_Estacion06;
	}

	public void setFld_Estacion06(String fld_Estacion06) {
		this.fld_Estacion06 = fld_Estacion06;
	}

	public String getFld_Estacion07() {
		return this.fld_Estacion07;
	}

	public void setFld_Estacion07(String fld_Estacion07) {
		this.fld_Estacion07 = fld_Estacion07;
	}

	public String getFld_Estacion08() {
		return this.fld_Estacion08;
	}

	public void setFld_Estacion08(String fld_Estacion08) {
		this.fld_Estacion08 = fld_Estacion08;
	}

	public String getFld_Estacion09() {
		return this.fld_Estacion09;
	}

	public void setFld_Estacion09(String fld_Estacion09) {
		this.fld_Estacion09 = fld_Estacion09;
	}

	public String getFld_Estacion10() {
		return this.fld_Estacion10;
	}

	public void setFld_Estacion10(String fld_Estacion10) {
		this.fld_Estacion10 = fld_Estacion10;
	}

	public String getFld_Estacion11() {
		return this.fld_Estacion11;
	}

	public void setFld_Estacion11(String fld_Estacion11) {
		this.fld_Estacion11 = fld_Estacion11;
	}

	public Date getFld_FecRevision() {
		return this.fld_FecRevision;
	}

	public void setFld_FecRevision(Date fld_FecRevision) {
		this.fld_FecRevision = fld_FecRevision;
	}

	public Date getFld_FecVencimiento() {
		return this.fld_FecVencimiento;
	}

	public void setFld_FecVencimiento(Date fld_FecVencimiento) {
		this.fld_FecVencimiento = fld_FecVencimiento;
	}

	public int getFld_Kilometraje() {
		return this.fld_Kilometraje;
	}

	public void setFld_Kilometraje(int fld_Kilometraje) {
		this.fld_Kilometraje = fld_Kilometraje;
	}

	public String getFld_Marca() {
		return this.fld_Marca;
	}

	public void setFld_Marca(String fld_Marca) {
		this.fld_Marca = fld_Marca;
	}

	public String getFld_Modelo() {
		return this.fld_Modelo;
	}

	public void setFld_Modelo(String fld_Modelo) {
		this.fld_Modelo = fld_Modelo;
	}

	public String getFld_NumCertificado() {
		return this.fld_NumCertificado;
	}

	public void setFld_NumCertificado(String fld_NumCertificado) {
		this.fld_NumCertificado = fld_NumCertificado;
	}

	public String getFld_NumChasis() {
		return this.fld_NumChasis;
	}

	public void setFld_NumChasis(String fld_NumChasis) {
		this.fld_NumChasis = fld_NumChasis;
	}

	public String getFld_NumMotor() {
		return this.fld_NumMotor;
	}

	public void setFld_NumMotor(String fld_NumMotor) {
		this.fld_NumMotor = fld_NumMotor;
	}

	public String getFld_Placa() {
		return this.fld_Placa;
	}

	public void setFld_Placa(String fld_Placa) {
		this.fld_Placa = fld_Placa;
	}

	public String getFld_ResultadoPrt() {
		return this.fld_ResultadoPrt;
	}

	public void setFld_ResultadoPrt(String fld_ResultadoPrt) {
		this.fld_ResultadoPrt = fld_ResultadoPrt;
	}

	public String getFld_Vin() {
		return this.fld_Vin;
	}

	public void setFld_Vin(String fld_Vin) {
		this.fld_Vin = fld_Vin;
	}

}