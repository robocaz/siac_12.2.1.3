package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.protestos.ProtestosBusiness;
import cl.exe.ccs.dto.ProtestoAclaracionDTO;
import cl.exe.ccs.dto.ProtestoDetalleDTO;
import cl.exe.ccs.dto.ProtestoOutDTO;
import cl.exe.ccs.dto.ProtestoTramitanteDTO;
import cl.exe.ccs.dto.ProtestoInDTO;

@Path("/protestos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class ProtestosServicio {

	private static final Logger LOG = LogManager.getLogger(ProtestosServicio.class);

	@Inject
	private ProtestosBusiness protestosBusiness;

	private boolean validaUsuario(HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if ( userPrincipal == null || userPrincipal.getName() == null ) {
			return false;
		}
		return true;
	}

	@POST
	@Path("/buscar")
	public Response buscar(@Context HttpServletRequest request, ProtestoInDTO params) {
		LOG.info("-> Buscar Protestos: " + params);
		
		if ( !validaUsuario(request) ) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ProtestoOutDTO response = protestosBusiness.buscar(params);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Protestos - Message: Error del Sistema", e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/detalle")
	public Response detalle(@Context HttpServletRequest request, Integer corrProt) {
		LOG.info("-> Detalle Protesto: " + corrProt);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ProtestoDetalleDTO response = protestosBusiness.detalle(corrProt);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Protestos - Message: Error del Sistema", e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/aclaracion")
	public Response aclaracion(@Context HttpServletRequest request, Integer corrAcl) {
		LOG.info("-> Detalle Aclaracion: " + corrAcl);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ProtestoAclaracionDTO response = protestosBusiness.aclaracion(corrAcl);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Protestos - Message: Error del Sistema", e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@POST
	@Path("/tramitante")
	public Response tramitante(@Context HttpServletRequest request, String rut) {
		LOG.info("-> Detalle Tramitante: " + rut);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			ProtestoTramitanteDTO response = protestosBusiness.tramitante(rut);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Protestos - Message: Error del Sistema", e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

}
