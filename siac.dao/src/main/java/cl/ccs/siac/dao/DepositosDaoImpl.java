package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.DepositoDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("depositosDao")
@Stateless
public class DepositosDaoImpl implements DepositosDao {
	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;
	
	private static final Logger LOG = LogManager.getLogger(DepositosDaoImpl.class);
	
	@Override
	public List<DepositoDTO> consultaDepositos(Integer codCC, String desde, String hasta) {
		LOG.debug("> DAO: consultaDepositos - SP: SQ_Depositos_SW2");
		
		List<DepositoDTO> lista = new ArrayList<DepositoDTO>();
		
		try {
			
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_Depositos_SW2");
			
			spq.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, Timestamp.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, codCC);
			spq.setParameter(2, Timestamp.valueOf(desde));
			spq.setParameter(3, Timestamp.valueOf(hasta));
			LOG.debug("> Params: [codCC=" + codCC + ", desde=" + desde + ", hasta=" + hasta + "]");
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			for ( Object item : result ) {
				Object[] datos = (Object[]) item;
				DepositoDTO depo = new DepositoDTO();
				
				depo.setFlagCierreFinal(Boolean.parseBoolean(datos[0].toString()));
				depo.setFecDepositoAux(datos[1].toString());
				depo.setNroDepositoAux(datos[2].toString().trim());
				depo.setNroCtaCteAux(datos[3].toString().trim());
				depo.setMontoDepositoAux((BigDecimal) datos[4]);
				depo.setGlosaEmisorAux(datos[5].toString().trim());
				depo.setNombre(datos[6].toString());
				depo.setFecRegDeposito(datos[7].toString());
				depo.setFecCierreFinal(datos[8].toString());
				depo.setCorrCuentaId((BigDecimal) datos[9]);
				
				lista.add(depo);
			}
			
		} catch (Exception e) {
			LOG.error("Error en el SP SQ_Depositos_SW2 " + e.getMessage());
		}
		
		return lista;
	}
	
	@Override
	public ResponseDTO guardaDeposito(DepositoDTO depo) {
		LOG.debug("> DAO: guardaDeposito - SP: SI_Deposito_SW2");
		
		ResponseDTO resp = new ResponseDTO();
		
		try {
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SI_Deposito_SW2");
			
			spq.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(8, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(9, BigDecimal.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(10, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(11, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, depo.getCodigoCC());
			spq.setParameter(2, Timestamp.valueOf(depo.getFecRegDeposito()));
			spq.setParameter(3, depo.getCodUser());
			spq.setParameter(4, depo.getRutPersona());
			spq.setParameter(5, depo.getCodEmisor());
			spq.setParameter(6, depo.getNroDepositoAux());
			spq.setParameter(7, depo.getNroCtaCteAux());
			spq.setParameter(8, depo.getTipoPago());
			spq.setParameter(9, depo.getMontoDepositoAux());
			LOG.debug("> Params: " + depo.toString());
			
			spq.execute();
			
			resp.setIdControl((Integer) spq.getOutputParameterValue(10));
			resp.setMsgControl(spq.getOutputParameterValue(11).toString());
			
		} catch (Exception e) {
			resp.setIdControl(-1);
			resp.setMsgControl("ERR");
			LOG.error("Error en el SP SI_Deposito_SW2 " + e.getMessage());
		}
		
		return resp;
	}
	
	@Override
	public ResponseDTO cerrarDeposito(BigDecimal corrCtaId) {
		LOG.debug("> DAO: cerrarDeposito - SP: SU_CierreDepositos_SW2");
		
		ResponseDTO resp = new ResponseDTO();
		try {
			
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SU_CierreDepositos_SW2");
			
			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, corrCtaId);
			LOG.debug("> Params: " + corrCtaId.toString());
			
			spq.execute();
			
			resp.setIdControl((Integer) spq.getOutputParameterValue(2));
			resp.setMsgControl(spq.getOutputParameterValue(3).toString());
			
		} catch (Exception e) {
			resp.setIdControl(-1);
			resp.setMsgControl("ERR");
			LOG.error("Error en el SP SU_CierreDepositos_SW2 " + e.getMessage());
		}
		
		return resp;
	}

}
