package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DepositoDTO implements Serializable {
	
	private static final long serialVersionUID = -8895431558876072266L;

	private Integer codigoCC;
	private String fechaDesde;
	private String fechaHasta;
	private Boolean flagCierreFinal;
	private String fecDepositoAux;
	private String nroDepositoAux;
	private String nroCtaCteAux;
	private BigDecimal montoDepositoAux;
	private String glosaEmisorAux;
	private String rutPersona;
	private String nombre;
	private String codEmisor;
	private String fecRegDeposito;
	private String fecCierreFinal;
	private BigDecimal corrCuentaId;
	private String codUser;
	private String tipoPago;

	public Integer getCodigoCC() {
		return codigoCC;
	}
	public void setCodigoCC(Integer codigoCC) {
		this.codigoCC = codigoCC;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Boolean getFlagCierreFinal() {
		return flagCierreFinal;
	}
	public void setFlagCierreFinal(Boolean flagCierreFinal) {
		this.flagCierreFinal = flagCierreFinal;
	}
	public String getFecDepositoAux() {
		return fecDepositoAux;
	}
	public void setFecDepositoAux(String fecDepositoAux) {
		this.fecDepositoAux = fecDepositoAux;
	}
	public String getNroDepositoAux() {
		return nroDepositoAux;
	}
	public void setNroDepositoAux(String nroDepositoAux) {
		this.nroDepositoAux = nroDepositoAux;
	}
	public String getNroCtaCteAux() {
		return nroCtaCteAux;
	}
	public void setNroCtaCteAux(String nroCtaCteAux) {
		this.nroCtaCteAux = nroCtaCteAux;
	}
	public BigDecimal getMontoDepositoAux() {
		return montoDepositoAux;
	}
	public void setMontoDepositoAux(BigDecimal montoDepositoAux) {
		this.montoDepositoAux = montoDepositoAux;
	}
	public String getGlosaEmisorAux() {
		return glosaEmisorAux;
	}
	public void setGlosaEmisorAux(String glosaEmisorAux) {
		this.glosaEmisorAux = glosaEmisorAux;
	}
	public String getRutPersona() {
		return rutPersona;
	}
	public void setRutPersona(String rutPersona) {
		this.rutPersona = rutPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public String getFecRegDeposito() {
		return fecRegDeposito;
	}
	public void setFecRegDeposito(String fecRegDeposito) {
		this.fecRegDeposito = fecRegDeposito;
	}
	public String getFecCierreFinal() {
		return fecCierreFinal;
	}
	public void setFecCierreFinal(String fecCierreFinal) {
		this.fecCierreFinal = fecCierreFinal;
	}
	public BigDecimal getCorrCuentaId() {
		return corrCuentaId;
	}
	public void setCorrCuentaId(BigDecimal corrCuentaId) {
		this.corrCuentaId = corrCuentaId;
	}
	public String getCodUser() {
		return codUser;
	}
	public void setCodUser(String codUser) {
		this.codUser = codUser;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DepositoDTO [codigoCC=");
		builder.append(codigoCC);
		builder.append(", fechaDesde=");
		builder.append(fechaDesde);
		builder.append(", fechaHasta=");
		builder.append(fechaHasta);
		builder.append(", flagCierreFinal=");
		builder.append(flagCierreFinal);
		builder.append(", fecDepositoAux=");
		builder.append(fecDepositoAux);
		builder.append(", nroDepositoAux=");
		builder.append(nroDepositoAux);
		builder.append(", nroCtaCteAux=");
		builder.append(nroCtaCteAux);
		builder.append(", montoDepositoAux=");
		builder.append(montoDepositoAux);
		builder.append(", glosaEmisorAux=");
		builder.append(glosaEmisorAux);
		builder.append(", rutPersona=");
		builder.append(rutPersona);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", codEmisor=");
		builder.append(codEmisor);
		builder.append(", fecRegDeposito=");
		builder.append(fecRegDeposito);
		builder.append(", fecCierreFinal=");
		builder.append(fecCierreFinal);
		builder.append(", corrCuentaId=");
		builder.append(corrCuentaId);
		builder.append(", codUser=");
		builder.append(codUser);
		builder.append(", tipoPago=");
		builder.append(tipoPago);
		builder.append("]");
		return builder.toString();
	}

}
