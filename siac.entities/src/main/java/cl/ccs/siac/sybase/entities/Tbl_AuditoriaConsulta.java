package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditoriaConsultas database table.
 * 
 */
@Entity
@Table(name="Tbl_AuditoriaConsultas")
@NamedQuery(name="Tbl_AuditoriaConsulta.findAll", query="SELECT t FROM Tbl_AuditoriaConsulta t")
public class Tbl_AuditoriaConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Cantidad")
	private short fld_Cantidad;

	@Column(name="Fld_CodConsulta")
	private byte fld_CodConsulta;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrRegistro_Id")
	private BigDecimal fld_CorrRegistro_Id;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_RutConsultado")
	private String fld_RutConsultado;

	public Tbl_AuditoriaConsulta() {
	}

	public short getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(short fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public byte getFld_CodConsulta() {
		return this.fld_CodConsulta;
	}

	public void setFld_CodConsulta(byte fld_CodConsulta) {
		this.fld_CodConsulta = fld_CodConsulta;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrRegistro_Id() {
		return this.fld_CorrRegistro_Id;
	}

	public void setFld_CorrRegistro_Id(BigDecimal fld_CorrRegistro_Id) {
		this.fld_CorrRegistro_Id = fld_CorrRegistro_Id;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_RutConsultado() {
		return this.fld_RutConsultado;
	}

	public void setFld_RutConsultado(String fld_RutConsultado) {
		this.fld_RutConsultado = fld_RutConsultado;
	}

}