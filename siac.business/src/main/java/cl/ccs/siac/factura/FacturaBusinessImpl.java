package cl.ccs.siac.factura;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.FacturaDao;
import cl.exe.ccs.dto.FacturaDTO;
import cl.exe.ccs.dto.ReemplazaBoletaFacturaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ConsultaRangoBolFacDTO;

@Named("facturaBusiness")
public class FacturaBusinessImpl implements FacturaBusiness{

	@Inject
	private FacturaDao facturaDao;
	
    
	public FacturaDTO consultaFacturas(FacturaDTO facturaDTO){
		return facturaDao.consultaFacturas(facturaDTO);
	}

	public FacturaDTO grabaFactura(FacturaDTO facturaDTO){
		return facturaDao.grabaFactura(facturaDTO);
	}
	
	public FacturaDTO eliminaFactura(FacturaDTO facturaDTO){
		return facturaDao.eliminaFactura(facturaDTO);
	}
	
	public FacturaDTO actualizarTransaccion(FacturaDTO facturaDTO){
		return facturaDao.actualizarTransaccion(facturaDTO);
	}
	
	public FacturaDTO folioFactura(FacturaDTO facturaDTO){
		return facturaDao.folioFactura(facturaDTO);
	}
	
	public FacturaDTO consultaRut(FacturaDTO facturaDTO){
		return facturaDao.consultaRut(facturaDTO);
	}

	public ConsultaRangoBolFacDTO consultaRangoBF(ConsultaRangoBolFacDTO consultaRango){
	    return facturaDao.consultaRangoBF(consultaRango);
	}
	
	public ResponseDTO reemplazarBoleta(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO){
		return facturaDao.reemplazarBoleta(reemplazaBoletaFacturaDTO);
	}
	
	public ResponseDTO reemplazarBoletaCero(ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO){
		return facturaDao.reemplazarBoletaCero(reemplazaBoletaFacturaDTO);
	}

	public ResponseDTO obtenerIVA(){
		return facturaDao.obtenerIVA();
	}
}
