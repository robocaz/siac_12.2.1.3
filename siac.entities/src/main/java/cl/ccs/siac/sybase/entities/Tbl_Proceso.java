package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Procesos database table.
 * 
 */
@Entity
@Table(name="Tbl_Procesos")
@NamedQuery(name="Tbl_Proceso.findAll", query="SELECT t FROM Tbl_Proceso t")
public class Tbl_Proceso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ClasificacionEnvio")
	private boolean fld_ClasificacionEnvio;

	@Column(name="Fld_CodProceso")
	private byte fld_CodProceso;

	@Column(name="Fld_GlosaProceso")
	private String fld_GlosaProceso;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tbl_Proceso() {
	}

	public boolean getFld_ClasificacionEnvio() {
		return this.fld_ClasificacionEnvio;
	}

	public void setFld_ClasificacionEnvio(boolean fld_ClasificacionEnvio) {
		this.fld_ClasificacionEnvio = fld_ClasificacionEnvio;
	}

	public byte getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(byte fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public String getFld_GlosaProceso() {
		return this.fld_GlosaProceso;
	}

	public void setFld_GlosaProceso(String fld_GlosaProceso) {
		this.fld_GlosaProceso = fld_GlosaProceso;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}