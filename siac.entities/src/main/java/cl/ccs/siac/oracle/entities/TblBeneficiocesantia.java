package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TBL_BENEFICIOCESANTIA database table.
 * 
 */
@Entity
@Table(name="TBL_BENEFICIOCESANTIA")
@NamedQuery(name="TblBeneficiocesantia.findAll", query="SELECT t FROM TblBeneficiocesantia t")
public class TblBeneficiocesantia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATERMINOCONTRATO")
	private Date fldFechaterminocontrato;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_RUTEMPLEADOR")
	private String fldRutempleador;

	public TblBeneficiocesantia() {
	}

	public Date getFldFechaterminocontrato() {
		return this.fldFechaterminocontrato;
	}

	public void setFldFechaterminocontrato(Date fldFechaterminocontrato) {
		this.fldFechaterminocontrato = fldFechaterminocontrato;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldRutempleador() {
		return this.fldRutempleador;
	}

	public void setFldRutempleador(String fldRutempleador) {
		this.fldRutempleador = fldRutempleador;
	}

}