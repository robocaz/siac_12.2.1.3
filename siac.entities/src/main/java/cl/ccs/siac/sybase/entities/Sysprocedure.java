package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysprocedures database table.
 * 
 */
@Entity
@Table(name="sysprocedures")
@NamedQuery(name="Sysprocedure.findAll", query="SELECT s FROM Sysprocedure s")
public class Sysprocedure implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private short number;

	@Column(name="qp_setting")
	private byte[] qpSetting;

	private int sequence;

	private short status;

	private short type;

	private int version;

	public Sysprocedure() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getNumber() {
		return this.number;
	}

	public void setNumber(short number) {
		this.number = number;
	}

	public byte[] getQpSetting() {
		return this.qpSetting;
	}

	public void setQpSetting(byte[] qpSetting) {
		this.qpSetting = qpSetting;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}