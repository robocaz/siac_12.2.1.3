package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlBoletin database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlBoletin.findAll", query="SELECT t FROM Tbl_ControlBoletin t")
public class Tbl_ControlBoletin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCBOL_Id")
	private BigDecimal fld_CorrCBOL_Id;

	@Column(name="Fld_EstadoBOL")
	private String fld_EstadoBOL;

	@Column(name="Fld_FecCierre")
	private Timestamp fld_FecCierre;

	@Column(name="Fld_FecFin")
	private Timestamp fld_FecFin;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecPaginacion")
	private Timestamp fld_FecPaginacion;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_NroAcl")
	private int fld_NroAcl;

	@Column(name="Fld_NroAclCH")
	private int fld_NroAclCH;

	@Column(name="Fld_NroAclCM")
	private int fld_NroAclCM;

	@Column(name="Fld_NroAclCMP")
	private int fld_NroAclCMP;

	@Column(name="Fld_NroAclCMR")
	private int fld_NroAclCMR;

	@Column(name="Fld_NroAclFS")
	private int fld_NroAclFS;

	@Column(name="Fld_NroAclLT")
	private int fld_NroAclLT;

	@Column(name="Fld_NroAclPCH")
	private int fld_NroAclPCH;

	@Column(name="Fld_NroAclPCM")
	private int fld_NroAclPCM;

	@Column(name="Fld_NroAclPG")
	private int fld_NroAclPG;

	@Column(name="Fld_NroAclPLT")
	private int fld_NroAclPLT;

	@Column(name="Fld_NroAclPPG")
	private int fld_NroAclPPG;

	@Column(name="Fld_NroAclPres")
	private int fld_NroAclPres;

	@Column(name="Fld_NroAclRA")
	private int fld_NroAclRA;

	@Column(name="Fld_NroAE")
	private int fld_NroAE;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroElemIndice")
	private int fld_NroElemIndice;

	@Column(name="Fld_NroEnvAclPres")
	private int fld_NroEnvAclPres;

	@Column(name="Fld_NroEnviosAcl")
	private int fld_NroEnviosAcl;

	@Column(name="Fld_NroEnviosProt")
	private int fld_NroEnviosProt;

	@Column(name="Fld_NroImpresores")
	private short fld_NroImpresores;

	@Column(name="Fld_NroPaginas")
	private short fld_NroPaginas;

	@Column(name="Fld_NroProt")
	private int fld_NroProt;

	@Column(name="Fld_NroProtCH")
	private int fld_NroProtCH;

	@Column(name="Fld_NroProtCM")
	private int fld_NroProtCM;

	@Column(name="Fld_NroProtFS")
	private int fld_NroProtFS;

	@Column(name="Fld_NroProtLT")
	private int fld_NroProtLT;

	@Column(name="Fld_NroProtPG")
	private int fld_NroProtPG;

	@Column(name="Fld_NroProtQI")
	private int fld_NroProtQI;

	@Column(name="Fld_NroProtRA")
	private int fld_NroProtRA;

	@Column(name="Fld_NroR")
	private int fld_NroR;

	@Column(name="Fld_NroRSR")
	private int fld_NroRSR;

	@Column(name="Fld_NroSolicAE")
	private int fld_NroSolicAE;

	@Column(name="Fld_NroSolicR")
	private int fld_NroSolicR;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoGeneracion")
	private String fld_TipoGeneracion;

	@Column(name="Fld_TotFilas")
	private short fld_TotFilas;

	@Column(name="Fld_TotRegistrosBOL")
	private int fld_TotRegistrosBOL;

	@Column(name="Fld_UsoBOL")
	private String fld_UsoBOL;

	public Tbl_ControlBoletin() {
	}

	public BigDecimal getFld_CorrCBOL_Id() {
		return this.fld_CorrCBOL_Id;
	}

	public void setFld_CorrCBOL_Id(BigDecimal fld_CorrCBOL_Id) {
		this.fld_CorrCBOL_Id = fld_CorrCBOL_Id;
	}

	public String getFld_EstadoBOL() {
		return this.fld_EstadoBOL;
	}

	public void setFld_EstadoBOL(String fld_EstadoBOL) {
		this.fld_EstadoBOL = fld_EstadoBOL;
	}

	public Timestamp getFld_FecCierre() {
		return this.fld_FecCierre;
	}

	public void setFld_FecCierre(Timestamp fld_FecCierre) {
		this.fld_FecCierre = fld_FecCierre;
	}

	public Timestamp getFld_FecFin() {
		return this.fld_FecFin;
	}

	public void setFld_FecFin(Timestamp fld_FecFin) {
		this.fld_FecFin = fld_FecFin;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecPaginacion() {
		return this.fld_FecPaginacion;
	}

	public void setFld_FecPaginacion(Timestamp fld_FecPaginacion) {
		this.fld_FecPaginacion = fld_FecPaginacion;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public int getFld_NroAcl() {
		return this.fld_NroAcl;
	}

	public void setFld_NroAcl(int fld_NroAcl) {
		this.fld_NroAcl = fld_NroAcl;
	}

	public int getFld_NroAclCH() {
		return this.fld_NroAclCH;
	}

	public void setFld_NroAclCH(int fld_NroAclCH) {
		this.fld_NroAclCH = fld_NroAclCH;
	}

	public int getFld_NroAclCM() {
		return this.fld_NroAclCM;
	}

	public void setFld_NroAclCM(int fld_NroAclCM) {
		this.fld_NroAclCM = fld_NroAclCM;
	}

	public int getFld_NroAclCMP() {
		return this.fld_NroAclCMP;
	}

	public void setFld_NroAclCMP(int fld_NroAclCMP) {
		this.fld_NroAclCMP = fld_NroAclCMP;
	}

	public int getFld_NroAclCMR() {
		return this.fld_NroAclCMR;
	}

	public void setFld_NroAclCMR(int fld_NroAclCMR) {
		this.fld_NroAclCMR = fld_NroAclCMR;
	}

	public int getFld_NroAclFS() {
		return this.fld_NroAclFS;
	}

	public void setFld_NroAclFS(int fld_NroAclFS) {
		this.fld_NroAclFS = fld_NroAclFS;
	}

	public int getFld_NroAclLT() {
		return this.fld_NroAclLT;
	}

	public void setFld_NroAclLT(int fld_NroAclLT) {
		this.fld_NroAclLT = fld_NroAclLT;
	}

	public int getFld_NroAclPCH() {
		return this.fld_NroAclPCH;
	}

	public void setFld_NroAclPCH(int fld_NroAclPCH) {
		this.fld_NroAclPCH = fld_NroAclPCH;
	}

	public int getFld_NroAclPCM() {
		return this.fld_NroAclPCM;
	}

	public void setFld_NroAclPCM(int fld_NroAclPCM) {
		this.fld_NroAclPCM = fld_NroAclPCM;
	}

	public int getFld_NroAclPG() {
		return this.fld_NroAclPG;
	}

	public void setFld_NroAclPG(int fld_NroAclPG) {
		this.fld_NroAclPG = fld_NroAclPG;
	}

	public int getFld_NroAclPLT() {
		return this.fld_NroAclPLT;
	}

	public void setFld_NroAclPLT(int fld_NroAclPLT) {
		this.fld_NroAclPLT = fld_NroAclPLT;
	}

	public int getFld_NroAclPPG() {
		return this.fld_NroAclPPG;
	}

	public void setFld_NroAclPPG(int fld_NroAclPPG) {
		this.fld_NroAclPPG = fld_NroAclPPG;
	}

	public int getFld_NroAclPres() {
		return this.fld_NroAclPres;
	}

	public void setFld_NroAclPres(int fld_NroAclPres) {
		this.fld_NroAclPres = fld_NroAclPres;
	}

	public int getFld_NroAclRA() {
		return this.fld_NroAclRA;
	}

	public void setFld_NroAclRA(int fld_NroAclRA) {
		this.fld_NroAclRA = fld_NroAclRA;
	}

	public int getFld_NroAE() {
		return this.fld_NroAE;
	}

	public void setFld_NroAE(int fld_NroAE) {
		this.fld_NroAE = fld_NroAE;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public int getFld_NroElemIndice() {
		return this.fld_NroElemIndice;
	}

	public void setFld_NroElemIndice(int fld_NroElemIndice) {
		this.fld_NroElemIndice = fld_NroElemIndice;
	}

	public int getFld_NroEnvAclPres() {
		return this.fld_NroEnvAclPres;
	}

	public void setFld_NroEnvAclPres(int fld_NroEnvAclPres) {
		this.fld_NroEnvAclPres = fld_NroEnvAclPres;
	}

	public int getFld_NroEnviosAcl() {
		return this.fld_NroEnviosAcl;
	}

	public void setFld_NroEnviosAcl(int fld_NroEnviosAcl) {
		this.fld_NroEnviosAcl = fld_NroEnviosAcl;
	}

	public int getFld_NroEnviosProt() {
		return this.fld_NroEnviosProt;
	}

	public void setFld_NroEnviosProt(int fld_NroEnviosProt) {
		this.fld_NroEnviosProt = fld_NroEnviosProt;
	}

	public short getFld_NroImpresores() {
		return this.fld_NroImpresores;
	}

	public void setFld_NroImpresores(short fld_NroImpresores) {
		this.fld_NroImpresores = fld_NroImpresores;
	}

	public short getFld_NroPaginas() {
		return this.fld_NroPaginas;
	}

	public void setFld_NroPaginas(short fld_NroPaginas) {
		this.fld_NroPaginas = fld_NroPaginas;
	}

	public int getFld_NroProt() {
		return this.fld_NroProt;
	}

	public void setFld_NroProt(int fld_NroProt) {
		this.fld_NroProt = fld_NroProt;
	}

	public int getFld_NroProtCH() {
		return this.fld_NroProtCH;
	}

	public void setFld_NroProtCH(int fld_NroProtCH) {
		this.fld_NroProtCH = fld_NroProtCH;
	}

	public int getFld_NroProtCM() {
		return this.fld_NroProtCM;
	}

	public void setFld_NroProtCM(int fld_NroProtCM) {
		this.fld_NroProtCM = fld_NroProtCM;
	}

	public int getFld_NroProtFS() {
		return this.fld_NroProtFS;
	}

	public void setFld_NroProtFS(int fld_NroProtFS) {
		this.fld_NroProtFS = fld_NroProtFS;
	}

	public int getFld_NroProtLT() {
		return this.fld_NroProtLT;
	}

	public void setFld_NroProtLT(int fld_NroProtLT) {
		this.fld_NroProtLT = fld_NroProtLT;
	}

	public int getFld_NroProtPG() {
		return this.fld_NroProtPG;
	}

	public void setFld_NroProtPG(int fld_NroProtPG) {
		this.fld_NroProtPG = fld_NroProtPG;
	}

	public int getFld_NroProtQI() {
		return this.fld_NroProtQI;
	}

	public void setFld_NroProtQI(int fld_NroProtQI) {
		this.fld_NroProtQI = fld_NroProtQI;
	}

	public int getFld_NroProtRA() {
		return this.fld_NroProtRA;
	}

	public void setFld_NroProtRA(int fld_NroProtRA) {
		this.fld_NroProtRA = fld_NroProtRA;
	}

	public int getFld_NroR() {
		return this.fld_NroR;
	}

	public void setFld_NroR(int fld_NroR) {
		this.fld_NroR = fld_NroR;
	}

	public int getFld_NroRSR() {
		return this.fld_NroRSR;
	}

	public void setFld_NroRSR(int fld_NroRSR) {
		this.fld_NroRSR = fld_NroRSR;
	}

	public int getFld_NroSolicAE() {
		return this.fld_NroSolicAE;
	}

	public void setFld_NroSolicAE(int fld_NroSolicAE) {
		this.fld_NroSolicAE = fld_NroSolicAE;
	}

	public int getFld_NroSolicR() {
		return this.fld_NroSolicR;
	}

	public void setFld_NroSolicR(int fld_NroSolicR) {
		this.fld_NroSolicR = fld_NroSolicR;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoGeneracion() {
		return this.fld_TipoGeneracion;
	}

	public void setFld_TipoGeneracion(String fld_TipoGeneracion) {
		this.fld_TipoGeneracion = fld_TipoGeneracion;
	}

	public short getFld_TotFilas() {
		return this.fld_TotFilas;
	}

	public void setFld_TotFilas(short fld_TotFilas) {
		this.fld_TotFilas = fld_TotFilas;
	}

	public int getFld_TotRegistrosBOL() {
		return this.fld_TotRegistrosBOL;
	}

	public void setFld_TotRegistrosBOL(int fld_TotRegistrosBOL) {
		this.fld_TotRegistrosBOL = fld_TotRegistrosBOL;
	}

	public String getFld_UsoBOL() {
		return this.fld_UsoBOL;
	}

	public void setFld_UsoBOL(String fld_UsoBOL) {
		this.fld_UsoBOL = fld_UsoBOL;
	}

}