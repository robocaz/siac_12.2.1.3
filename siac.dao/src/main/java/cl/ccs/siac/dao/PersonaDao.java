package cl.ccs.siac.dao;

import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;

public interface PersonaDao {
	public PersonaDTO obtener(String rut);
	public PersonaDTO actualizar(PersonaDTO personaDTO);
	public ResponseDTO verificar(String rut);
	public PersonaDTO getNombre(String rut, Long correlativo);
}
