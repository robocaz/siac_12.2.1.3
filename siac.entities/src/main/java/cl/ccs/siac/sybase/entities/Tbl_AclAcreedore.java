package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_AclAcreedores database table.
 * 
 */
@Entity
@Table(name="Tbl_AclAcreedores")
@NamedQuery(name="Tbl_AclAcreedore.findAll", query="SELECT t FROM Tbl_AclAcreedore t")
public class Tbl_AclAcreedore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ApMaterno")
	private String fld_ApMaterno;

	@Column(name="Fld_ApPaterno")
	private String fld_ApPaterno;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Nombres")
	private String fld_Nombres;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	public Tbl_AclAcreedore() {
	}

	public String getFld_ApMaterno() {
		return this.fld_ApMaterno;
	}

	public void setFld_ApMaterno(String fld_ApMaterno) {
		this.fld_ApMaterno = fld_ApMaterno;
	}

	public String getFld_ApPaterno() {
		return this.fld_ApPaterno;
	}

	public void setFld_ApPaterno(String fld_ApPaterno) {
		this.fld_ApPaterno = fld_ApPaterno;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Nombres() {
		return this.fld_Nombres;
	}

	public void setFld_Nombres(String fld_Nombres) {
		this.fld_Nombres = fld_Nombres;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

}