package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_RETORNOACLBATCH database table.
 * 
 */
@Entity
@Table(name="TBL_RETORNOACLBATCH")
@NamedQuery(name="TblRetornoaclbatch.findAll", query="SELECT t FROM TblRetornoaclbatch t")
public class TblRetornoaclbatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_RETORNOACLBATCH_FLDCODRETACL_GENERATOR", sequenceName="FLD_CODRETACL")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_RETORNOACLBATCH_FLDCODRETACL_GENERATOR")
	@Column(name="FLD_CODRETACL")
	private long fldCodretacl;

	@Column(name="FLD_GLOSARETACL")
	private String fldGlosaretacl;

	public TblRetornoaclbatch() {
	}

	public long getFldCodretacl() {
		return this.fldCodretacl;
	}

	public void setFldCodretacl(long fldCodretacl) {
		this.fldCodretacl = fldCodretacl;
	}

	public String getFldGlosaretacl() {
		return this.fldGlosaretacl;
	}

	public void setFldGlosaretacl(String fldGlosaretacl) {
		this.fldGlosaretacl = fldGlosaretacl;
	}

}