package cl.ccs.siac.servicios;

import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.transaccion.TransaccionBusiness;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.TramitanteDTO;
import cl.exe.ccs.dto.TransaccionDTO;
import cl.exe.ccs.dto.TransaccionGrillaDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/transaccion")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class TransaccionServicio {

	private static final Logger log = LogManager.getLogger(TransaccionServicio.class);

	@Inject
	private TransaccionBusiness transaccionBusiness;

	@POST
	@Path("/listarservicio")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarServicio(@Context HttpServletRequest request) throws ServletException {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			ResponseListDTO<TransaccionGrillaDTO> transacciones = transaccionBusiness.listarServicio(Long.parseLong(usuarioDTO.getCaja()));
			return Response.ok(transacciones.getLista()).build();

		} catch (Exception ex) {
			log.error("Metodo  listarServicio" + userPrincipal.getName() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/agregarservicio")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response agregarServicio(@Context HttpServletRequest request, TransaccionDTO transaccionDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			TramitanteDTO tramitanteDTO = (TramitanteDTO) request.getSession().getAttribute("tramitante");

			transaccionDTO.setCorrCaja(new Long(usuarioDTO.getCaja()));
			transaccionDTO.setRutTramitante(tramitanteDTO.getRut());
			transaccionDTO.setCorrNombre(transaccionDTO.getCorrNombre());

			if (transaccionDTO.getCorrAclaracion() != null && transaccionDTO.getCorrAclaracion() != 0) {
				transaccionDTO.setCorrAclaracion(transaccionDTO.getCorrAclaracion());
			} else {
				transaccionDTO.setCorrAclaracion(0L);
			}

			if (transaccionDTO.getCorrProtesto() != null && transaccionDTO.getCorrProtesto() != 0) {
				transaccionDTO.setCorrProtesto(transaccionDTO.getCorrProtesto());
			} else {
				transaccionDTO.setCorrProtesto(0L);
			}

			ResponseDTO transaccion = transaccionBusiness.agregarServicio(transaccionDTO);
			return Response.ok(transaccion).build();

		} catch (Exception e) {
			log.error("Metodo agregarServicio - Agregar Servicio " + userPrincipal.getName() + " - Message: Error del Sistema", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
