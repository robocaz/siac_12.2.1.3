package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoObservacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoObservacion.findAll", query="SELECT t FROM Tbl_TipoObservacion t")
public class Tbl_TipoObservacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodObservacion")
	private byte fld_CodObservacion;

	@Column(name="Fld_GlosaObservacion")
	private String fld_GlosaObservacion;

	public Tbl_TipoObservacion() {
	}

	public byte getFld_CodObservacion() {
		return this.fld_CodObservacion;
	}

	public void setFld_CodObservacion(byte fld_CodObservacion) {
		this.fld_CodObservacion = fld_CodObservacion;
	}

	public String getFld_GlosaObservacion() {
		return this.fld_GlosaObservacion;
	}

	public void setFld_GlosaObservacion(String fld_GlosaObservacion) {
		this.fld_GlosaObservacion = fld_GlosaObservacion;
	}

}