package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LocCCostoLP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LocCCostoLP.findAll", query="SELECT t FROM Tbl_LocCCostoLP t")
public class Tbl_LocCCostoLP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodLocPub")
	private int fld_CodLocPub;

	public Tbl_LocCCostoLP() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public int getFld_CodLocPub() {
		return this.fld_CodLocPub;
	}

	public void setFld_CodLocPub(int fld_CodLocPub) {
		this.fld_CodLocPub = fld_CodLocPub;
	}

}