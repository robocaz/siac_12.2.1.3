package cl.ccs.siac.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ComboDTO {
    private String codigo;
    private String descripcion;
    
    public ComboDTO(){
    	
    }
    
    public ComboDTO(String codigo, String descripcion){
    	this.codigo = codigo;
    	this.descripcion = descripcion;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
