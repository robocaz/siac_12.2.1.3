package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ConsultaID database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ConsultaID.findAll", query="SELECT t FROM Tbl_ConsultaID t")
public class Tbl_ConsultaID implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrId")
	private BigDecimal fld_CorrId;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_ConsultaID() {
	}

	public BigDecimal getFld_CorrId() {
		return this.fld_CorrId;
	}

	public void setFld_CorrId(BigDecimal fld_CorrId) {
		this.fld_CorrId = fld_CorrId;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}