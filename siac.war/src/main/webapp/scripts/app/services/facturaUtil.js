angular.module('ccs').service('FacturaUtil', function() {
    this.getTipoPersonaRut = function (rut) {
    	// rut = rut.toString().trim();
    	console.log(rut);
    	rut = rut.toString().indexOf("-") === -1 ? rut.toString().substring(0, rut.length-1) : rut.toString().substring(0, rut.toString().indexOf("-"));
    	rut = rut.indexOf(".") !== -1 ? rut.replace(/\./g, "") : rut;
    	rut = Number(rut);
    	console.log(rut);
        return rut < 50000000 ? 'N' : 'J';
    }

    this.existeRutEnFactura = function (rut, factura) {
    	console.log('existeRutEnFactura - '+rut);
    	console.log(factura);

    	if( typeof(rut) !== 'string' || rut === null || rut === "" ) {
    		return false
    	}
    	if ( typeof(factura) === 'undefined' || factura === null ) {
    		return false;
    	}
    	if ( typeof(factura.rutAfectado) === 'undefined' || factura.rutAfectado === null || factura.rutAfectado === "" ) {
    		return false;
    	}

    	// factura a terceros, incluye todos los afectados! o rut afectado de factura, coincide con el consultado!
    	if ( factura.rutAfectado === "000000000-0" || factura.rutAfectado === rut ) {
    		return true;
    	}

    	return false;
    }

    this.existeRutEnFacturas = function (rut, facturas) {
    	console.log('existeRutEnFacturas - '+rut);
    	console.log(facturas);

    	if( typeof(rut) !== 'string' || rut === null || rut === "" ) {
    		return false
    	}
    	if ( typeof(facturas) === 'undefined' || facturas === null || facturas.length === 0 ) {
    		return false;
    	}

    	for( var i=0; i < facturas.length; i++) {
    		if ( this.existeRutEnFactura(rut, facturas[i]) ) {
    			return true;
    		}
    	}

    	return false;
    }

    this.getFolioFacturaRutAfectado = function (rut, facturas) {
    	if( !this.existeRutEnFacturas ) {
    		return 0;
    	}
    	for( var i=0; i < facturas.length; i++) {
    		if ( facturas[i].rutAfectado === rut ) {
    			return facturas[i].numeroFactura;
    		}
    	}
    	return 0;
    }

    this.montosFactura = function (factura) {
      factura.montoBruto = 0;
      factura.iva = 0;
      factura.total = 0;
      if ( factura.transacciones.length > 0 ) {
        for (var i=0; i < factura.transacciones.length; i++) {
          factura.montoBruto += factura.transacciones[i].cantidadServicio * factura.transacciones[i].costoServicio;
        }
      }
      factura.iva = factura.montoBruto > 0 ? Math.round(factura.montoBruto * 0.19) : 0;
      factura.total = factura.montoBruto + factura.iva;

      return factura;
    }
});
