package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RAEstado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RAEstado.findAll", query="SELECT t FROM Tbl_RAEstado t")
public class Tbl_RAEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_GlosaEstado")
	private String fld_GlosaEstado;

	public Tbl_RAEstado() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_GlosaEstado() {
		return this.fld_GlosaEstado;
	}

	public void setFld_GlosaEstado(String fld_GlosaEstado) {
		this.fld_GlosaEstado = fld_GlosaEstado;
	}

}