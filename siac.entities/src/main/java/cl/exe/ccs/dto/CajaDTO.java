package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CajaDTO  extends ResponseDTO implements Serializable{
	
	private String corrCaja;
	private String fechaInicio;
	private String fechaTermino;
	private Integer cajasAbiertas;
	private Integer cajasPermitidas;
	private String estado;
	private String hora;
	private byte[] timestamp;
	private String centroCostoGlosa;
	private String codOrigen;
	
	
	public CajaDTO(String corrCaja, String fechaInicio, String fechaTermino, byte[] timestamp, String estado){
		this.corrCaja = corrCaja;
		this.fechaInicio = fechaInicio;
		this.fechaTermino = fechaTermino;
		this.timestamp = timestamp;
		this.estado = estado;
	}
	
	public CajaDTO(String corrCaja, String centroCostoGlosa, String estado, String codOrigen, String fechaInicio, String fechaTermino){
		this.corrCaja = corrCaja;
		this.centroCostoGlosa = centroCostoGlosa;
		this.fechaInicio = fechaInicio;
		this.fechaTermino = fechaTermino;
		this.codOrigen = codOrigen;
		this.estado = estado;
	}
	
	public CajaDTO(String corrCaja) {
		this.corrCaja = corrCaja;
	}
	
	public CajaDTO(){

	}
	
	public String getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(String corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	public Integer getCajasAbiertas() {
		return cajasAbiertas;
	}
	public void setCajasAbiertas(Integer cajasAbiertas) {
		this.cajasAbiertas = cajasAbiertas;
	}
	public Integer getCajasPermitidas() {
		return cajasPermitidas;
	}
	public void setCajasPermitidas(Integer cajasPermitidas) {
		this.cajasPermitidas = cajasPermitidas;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public byte[] getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(byte[] timestamp) {
		this.timestamp = timestamp;
	}

	public String getCentroCostoGlosa() {
		return centroCostoGlosa;
	}

	public void setCentroCostoGlosa(String centroCostoGlosa) {
		this.centroCostoGlosa = centroCostoGlosa;
	}

	public String getCodOrigen() {
		return codOrigen;
	}

	public void setCodOrigen(String codOrigen) {
		this.codOrigen = codOrigen;
	}
}
