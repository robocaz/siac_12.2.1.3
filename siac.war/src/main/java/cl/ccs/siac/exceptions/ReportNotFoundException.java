package cl.ccs.siac.exceptions;

public class ReportNotFoundException extends ReportException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3810245138917793312L;

	/**
	 * Constructor.
	 */
	public ReportNotFoundException() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 */
	public ReportNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportNotFoundException(final Throwable throwable) {
		super(throwable);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param message
	 *            {@link String}
	 * @param throwable
	 *            {@link Throwable}
	 */
	public ReportNotFoundException(final String message, Throwable throwable) {
		super(message, throwable);
	}

}
