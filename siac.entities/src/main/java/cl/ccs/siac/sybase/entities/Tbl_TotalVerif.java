package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TotalVerif database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TotalVerif.findAll", query="SELECT t FROM Tbl_TotalVerif t")
public class Tbl_TotalVerif implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_TotalGrabados")
	private int fld_TotalGrabados;

	@Column(name="Fld_TotalLeidos")
	private int fld_TotalLeidos;

	@Column(name="Fld_TotalMonedasInvalidas")
	private int fld_TotalMonedasInvalidas;

	@Column(name="Fld_TotalNoGrabados")
	private int fld_TotalNoGrabados;

	@Column(name="Fld_TotalRutBuenos")
	private int fld_TotalRutBuenos;

	@Column(name="Fld_TotalRutConProtVig")
	private int fld_TotalRutConProtVig;

	@Column(name="Fld_TotalRutErroneos")
	private int fld_TotalRutErroneos;

	@Column(name="Fld_TotalRutSinProtVig")
	private int fld_TotalRutSinProtVig;

	public Tbl_TotalVerif() {
	}

	public int getFld_TotalGrabados() {
		return this.fld_TotalGrabados;
	}

	public void setFld_TotalGrabados(int fld_TotalGrabados) {
		this.fld_TotalGrabados = fld_TotalGrabados;
	}

	public int getFld_TotalLeidos() {
		return this.fld_TotalLeidos;
	}

	public void setFld_TotalLeidos(int fld_TotalLeidos) {
		this.fld_TotalLeidos = fld_TotalLeidos;
	}

	public int getFld_TotalMonedasInvalidas() {
		return this.fld_TotalMonedasInvalidas;
	}

	public void setFld_TotalMonedasInvalidas(int fld_TotalMonedasInvalidas) {
		this.fld_TotalMonedasInvalidas = fld_TotalMonedasInvalidas;
	}

	public int getFld_TotalNoGrabados() {
		return this.fld_TotalNoGrabados;
	}

	public void setFld_TotalNoGrabados(int fld_TotalNoGrabados) {
		this.fld_TotalNoGrabados = fld_TotalNoGrabados;
	}

	public int getFld_TotalRutBuenos() {
		return this.fld_TotalRutBuenos;
	}

	public void setFld_TotalRutBuenos(int fld_TotalRutBuenos) {
		this.fld_TotalRutBuenos = fld_TotalRutBuenos;
	}

	public int getFld_TotalRutConProtVig() {
		return this.fld_TotalRutConProtVig;
	}

	public void setFld_TotalRutConProtVig(int fld_TotalRutConProtVig) {
		this.fld_TotalRutConProtVig = fld_TotalRutConProtVig;
	}

	public int getFld_TotalRutErroneos() {
		return this.fld_TotalRutErroneos;
	}

	public void setFld_TotalRutErroneos(int fld_TotalRutErroneos) {
		this.fld_TotalRutErroneos = fld_TotalRutErroneos;
	}

	public int getFld_TotalRutSinProtVig() {
		return this.fld_TotalRutSinProtVig;
	}

	public void setFld_TotalRutSinProtVig(int fld_TotalRutSinProtVig) {
		this.fld_TotalRutSinProtVig = fld_TotalRutSinProtVig;
	}

}