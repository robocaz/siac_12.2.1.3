angular.module('ccs').controller('ListaCierreCajaController', function($document, $scope, CajaResource, $rootScope, $location, $filter, $timeout, LoginResource, $uibModal) {

    $scope.listaCajasAbiertas = [];

    $scope.caja = {};

    $scope.listar = function(){
        var successCallback = function(data){
            $scope.listaCajasAbiertas = data;
            $scope.caja = {};
        };
        CajaResource.listarcajastodas(successCallback, $scope.errorCallback);
    }
    $('#listacierrecaja').on('show.bs.modal', function (e) {
        $timeout(function(){
          $scope.listar();
        });
    });


    $scope.aceptarSeleccion = function(){
        if($scope.caja.id == null){
            swal('Alerta','Debe seleccionar caja','error')
        }else if($scope.listaCajasAbiertas[$scope.caja.id].estado == 3){
            swal('Alerta','Ya se encuentra en estado Cierre Definitivo','error')
        }else{
			swal({
				title: 'Alerta',
				text: '¿Esta seguro de cerrar la caja?',
				type: 'question',
				confirmButtonText: 'Si',
				showCancelButton: true,
				cancelButtonText: 'No'
			}).then( function() {
					var successCallback = function(data){
						$scope.listaCajasAbiertas[$scope.caja.id].estado = 3;
						if($rootScope.usuario.caja == $scope.listaCajasAbiertas[$scope.caja.id].corrCaja){
							$rootScope.usuario.caja = "0";
							$rootScope.tramitante = {};
							$location.path("/inicio");
						}
					};
					CajaResource.cerrarcajadefinitivo( $scope.listaCajasAbiertas[$scope.caja.id].corrCaja , successCallback, $scope.errorCallback);
				}
			);
        }
    }

    $scope.salir = function(){
    	
        if($rootScope.usuario.caja!="0"){
            var successCallback = function(data){
                $rootScope.usuario.caja = data.caja;
                angular.element('#listacierrecaja').modal("hide");
                $location.path("/inicio");
            };
            CajaResource.cerrarcajatemporal($rootScope.usuario.caja,  successCallback, $scope.errorCallback);
        }else{
            angular.element('#listacierrecaja').modal("hide");
        }
      //$scope.cantidadesabiertasActualiza();
        $scope.cierraCajaActualiza();
    }
    
    $scope.cierraCajaActualiza = function() {
	    var successCallback = function(data){
	
	            $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
	            $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
	    };
	    CajaResource.cierracaja( successCallback, $scope.errorCallback);
    	     
    };

    $scope.cantidadesabiertasActualiza = function() {
        var successCallback = function(data){

                $rootScope.usuario.cajasPermitidas = data.cajasPermitidas;
                $rootScope.usuario.cajasAbiertas = data.cajasAbiertas;
        };
        CajaResource.cantidadabiertas( successCallback, $scope.errorCallback);
      
    };
   

});
