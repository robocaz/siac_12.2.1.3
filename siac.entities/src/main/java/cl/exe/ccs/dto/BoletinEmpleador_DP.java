package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BoletinEmpleador_DP implements Serializable{

	private String institucion;
	private String motivo;
	private Integer boletin;
	private Integer pagina;
	private String fecha;
	private Integer meses;
	private Integer cotizaciones;
	private BigDecimal montoAdeudado;
	
	private List<BoletinDataEmpleador> lista;

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Integer getBoletin() {
		return boletin;
	}

	public void setBoletin(Integer boletin) {
		this.boletin = boletin;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getMeses() {
		return meses;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public Integer getCotizaciones() {
		return cotizaciones;
	}

	public void setCotizaciones(Integer cotizaciones) {
		this.cotizaciones = cotizaciones;
	}

	public BigDecimal getMontoAdeudado() {
		return montoAdeudado;
	}

	public void setMontoAdeudado(BigDecimal montoAdeudado) {
		this.montoAdeudado = montoAdeudado;
	}

	public List<BoletinDataEmpleador> getLista() {
		return lista;
	}

	public void setLista(List<BoletinDataEmpleador> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n institucion: " + institucion);
		str.append(", motivo: " + motivo);
		str.append(", boletin: " + boletin);
		str.append(", pagina: " + pagina);
		str.append(", fecha: " + fecha);
		str.append(", meses: " + meses);
		str.append(", cotizaciones: " + cotizaciones);
		str.append(", montoAdeudado: " + montoAdeudado);
		str.append(", lista: " + lista);
		return str.toString();
	}

}
