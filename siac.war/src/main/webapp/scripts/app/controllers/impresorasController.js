angular.module('ccs').controller('ImpresorasController', function($scope, ImpresoraResource, $rootScope, $localStorage, $location) {

    $scope.impresoras = [];
    
     
    $scope.cargaImpresoras = function() {

        var successCallback = function(data,responseHeaders){
        	
        	console.log(data);
        	$scope.impresoras = data;
        	$scope.impresora = $scope.impresoras[0];
        	
        	
        	
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        ImpresoraResource.cargaImpresoras(successCallback, errorCallback2);
    };
    
	$scope.setImpresora = function() {
		
		swal({
			title: 'Alerta',
			text: '¿Está seguro cambiar de impresora?',
			type: 'question',
			confirmButtonText: 'Si',
			showCancelButton: true,
			cancelButtonText: 'No'
		}).then( function() {
				$localStorage.impresoraSelected = $scope.impresora.modeloImpresora;
				angular.element("#cambioimpresora").modal("hide");
			}
		);
		
	}

});
