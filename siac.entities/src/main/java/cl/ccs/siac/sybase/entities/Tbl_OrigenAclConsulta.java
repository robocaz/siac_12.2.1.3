package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_OrigenAclConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_OrigenAclConsulta.findAll", query="SELECT t FROM Tbl_OrigenAclConsulta t")
public class Tbl_OrigenAclConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodOrigenConsulta")
	private byte fld_CodOrigenConsulta;

	@Column(name="Fld_GlosaOrigenConsulta")
	private String fld_GlosaOrigenConsulta;

	public Tbl_OrigenAclConsulta() {
	}

	public byte getFld_CodOrigenConsulta() {
		return this.fld_CodOrigenConsulta;
	}

	public void setFld_CodOrigenConsulta(byte fld_CodOrigenConsulta) {
		this.fld_CodOrigenConsulta = fld_CodOrigenConsulta;
	}

	public String getFld_GlosaOrigenConsulta() {
		return this.fld_GlosaOrigenConsulta;
	}

	public void setFld_GlosaOrigenConsulta(String fld_GlosaOrigenConsulta) {
		this.fld_GlosaOrigenConsulta = fld_GlosaOrigenConsulta;
	}

}