package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdNoFallecidosRC database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdNoFallecidosRC.findAll", query="SELECT t FROM Tb_CeIdNoFallecidosRC t")
public class Tb_CeIdNoFallecidosRC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	@Column(name="Fld_TotalConsultas")
	private byte fld_TotalConsultas;

	//bi-directional many-to-one association to Tb_CeIdDetObituario
	@ManyToOne
	@JoinColumn(name="Fld_CorrObituario")
	private Tb_CeIdDetObituario tbCeIdDetObituario;

	public Tb_CeIdNoFallecidosRC() {
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public byte getFld_TotalConsultas() {
		return this.fld_TotalConsultas;
	}

	public void setFld_TotalConsultas(byte fld_TotalConsultas) {
		this.fld_TotalConsultas = fld_TotalConsultas;
	}

	public Tb_CeIdDetObituario getTbCeIdDetObituario() {
		return this.tbCeIdDetObituario;
	}

	public void setTbCeIdDetObituario(Tb_CeIdDetObituario tbCeIdDetObituario) {
		this.tbCeIdDetObituario = tbCeIdDetObituario;
	}

}