package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the sysobjects database table.
 * 
 */
@Entity
@Table(name="sysobjects")
@NamedQuery(name="Sysobject.findAll", query="SELECT s FROM Sysobject s")
public class Sysobject implements Serializable {
	private static final long serialVersionUID = 1L;

	private int audflags;

	private short cache;

	private int ckfirst;

	private Timestamp crdate;

	private int deltrig;

	private byte[] erlchgts;

	private Timestamp expdate;

	private int id;

	private BigDecimal identburnmax;

	private short indexdel;

	private int instrig;

	@Column(name="lobcomp_lvl")
	private byte lobcompLvl;

	private String loginame;

	private String name;

	private int objspare;

	private short schemacnt;

	private int seltrig;

	private short spacestate;

	private short sysstat;

	private int sysstat2;

	private int sysstat3;

	private String type;

	private int uid;

	private int updtrig;

	private short userstat;

	private byte[] versionts;

	public Sysobject() {
	}

	public int getAudflags() {
		return this.audflags;
	}

	public void setAudflags(int audflags) {
		this.audflags = audflags;
	}

	public short getCache() {
		return this.cache;
	}

	public void setCache(short cache) {
		this.cache = cache;
	}

	public int getCkfirst() {
		return this.ckfirst;
	}

	public void setCkfirst(int ckfirst) {
		this.ckfirst = ckfirst;
	}

	public Timestamp getCrdate() {
		return this.crdate;
	}

	public void setCrdate(Timestamp crdate) {
		this.crdate = crdate;
	}

	public int getDeltrig() {
		return this.deltrig;
	}

	public void setDeltrig(int deltrig) {
		this.deltrig = deltrig;
	}

	public byte[] getErlchgts() {
		return this.erlchgts;
	}

	public void setErlchgts(byte[] erlchgts) {
		this.erlchgts = erlchgts;
	}

	public Timestamp getExpdate() {
		return this.expdate;
	}

	public void setExpdate(Timestamp expdate) {
		this.expdate = expdate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getIdentburnmax() {
		return this.identburnmax;
	}

	public void setIdentburnmax(BigDecimal identburnmax) {
		this.identburnmax = identburnmax;
	}

	public short getIndexdel() {
		return this.indexdel;
	}

	public void setIndexdel(short indexdel) {
		this.indexdel = indexdel;
	}

	public int getInstrig() {
		return this.instrig;
	}

	public void setInstrig(int instrig) {
		this.instrig = instrig;
	}

	public byte getLobcompLvl() {
		return this.lobcompLvl;
	}

	public void setLobcompLvl(byte lobcompLvl) {
		this.lobcompLvl = lobcompLvl;
	}

	public String getLoginame() {
		return this.loginame;
	}

	public void setLoginame(String loginame) {
		this.loginame = loginame;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getObjspare() {
		return this.objspare;
	}

	public void setObjspare(int objspare) {
		this.objspare = objspare;
	}

	public short getSchemacnt() {
		return this.schemacnt;
	}

	public void setSchemacnt(short schemacnt) {
		this.schemacnt = schemacnt;
	}

	public int getSeltrig() {
		return this.seltrig;
	}

	public void setSeltrig(int seltrig) {
		this.seltrig = seltrig;
	}

	public short getSpacestate() {
		return this.spacestate;
	}

	public void setSpacestate(short spacestate) {
		this.spacestate = spacestate;
	}

	public short getSysstat() {
		return this.sysstat;
	}

	public void setSysstat(short sysstat) {
		this.sysstat = sysstat;
	}

	public int getSysstat2() {
		return this.sysstat2;
	}

	public void setSysstat2(int sysstat2) {
		this.sysstat2 = sysstat2;
	}

	public int getSysstat3() {
		return this.sysstat3;
	}

	public void setSysstat3(int sysstat3) {
		this.sysstat3 = sysstat3;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getUpdtrig() {
		return this.updtrig;
	}

	public void setUpdtrig(int updtrig) {
		this.updtrig = updtrig;
	}

	public short getUserstat() {
		return this.userstat;
	}

	public void setUserstat(short userstat) {
		this.userstat = userstat;
	}

	public byte[] getVersionts() {
		return this.versionts;
	}

	public void setVersionts(byte[] versionts) {
		this.versionts = versionts;
	}

}