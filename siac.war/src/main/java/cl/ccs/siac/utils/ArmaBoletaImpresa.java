package cl.ccs.siac.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.TransaccionServicioDTO;

public class ArmaBoletaImpresa {
	
	@Inject
	private Impresora impresora;
	
	String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
	
	public StringBuffer armaBoleta(BoletaTransaccionDTO boletaTransaccionDTO){
		

		try{
			String numeroBoleta = boletaTransaccionDTO.getNumeroBoleta().toString();
			String corrCaja = String.format("%09d", boletaTransaccionDTO.getIdCaja());
			String centroCosto = String.format("%04d", boletaTransaccionDTO.getIdCentroCosto());
			String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
			StringBuffer stb = new StringBuffer();
	
			
		
			stb.append("\n\n\n\n\n\n");
			stb.append("                                                            #" + numeroBoleta +"\n");
			stb.append("\n");
			stb.append("   " + corrCaja + " " + centroCosto + "   " + date);
			
			
			
			impresora.setDispositivo("");
			impresora.escribir((char) 27 + "@");
			impresora.setTipoCaracterLatino();
			impresora.setNegro();
			impresora.setFormato(10);
			impresora.escribir(stb.toString());
			impresora.escribir("\n");
			//iteracion
			Long total = new Long(0);
			for(TransaccionServicioDTO transaccion : boletaTransaccionDTO.getTransacciones()){
				int posicion = transaccion.getCostoServicio().indexOf(".");
				impresora.escribir("  " + "1 " + "   " + transaccion.getTipoServicio().trim() + "    "+ transaccion.getCostoServicio().substring(0, posicion));
				total = total + Long.parseLong(transaccion.getCostoServicio().substring(0, posicion));
			}
			impresora.escribir("\n");
			impresora.escribir("                 " + "Total $"+total);
			
			
			impresora.correr(10);
			impresora.cortar();
			impresora.cerrarDispositivo();
			
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		
		return null;		

	}

	
	public String imprimeBoletaJar(BoletaTransaccionDTO boletaTransaccionDTO){

		String status = "";
		try{
			
			
			Gson gs = new Gson();
			String input = gs.toJson(boletaTransaccionDTO);
			Client client = ClientBuilder.newClient();
			WebTarget wt = client.target("http://localhost:8080/terminal/siac/impresion");
			Response response = wt.request(MediaType.APPLICATION_JSON).post(Entity.entity(input, MediaType.APPLICATION_JSON), Response.class);
			status = String.valueOf(response.getStatus());	
			
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		
	
		
		
		return status;
	}
	
	 public static void main(String[] args) throws Exception {
		 ArmaBoletaImpresa am = new ArmaBoletaImpresa();
		 System.out.println(am.armaBoleta(new BoletaTransaccionDTO()));
	 }
}
