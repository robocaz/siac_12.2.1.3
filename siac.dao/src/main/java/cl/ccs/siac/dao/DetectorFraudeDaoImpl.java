package cl.ccs.siac.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.DetectorFraudeDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("detectorFraudeDao")
@Stateless
public class DetectorFraudeDaoImpl implements DetectorFraudeDao {
	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	private static final Logger log = LogManager.getLogger(DetectorFraudeDaoImpl.class);
	
	@Override
	public DetectorFraudeDTO detectarPorRut(final String rut) {
		DetectorFraudeDTO fraudeDTO = new DetectorFraudeDTO();
		try {
			
			log.info("Ejecutando SQ_EnListaNegra_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_EnListaNegra_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_EnLN", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_Rut", rut);
			
			storedProcedure.execute();
			
			String enListaNegra = (String) storedProcedure.getOutputParameterValue("Fld_EnLN");
			
			if (enListaNegra != null && !enListaNegra.isEmpty()) {
				if (DetectorFraudeDTO.EnListaNegra.SI.value.equalsIgnoreCase(enListaNegra)) {
					fraudeDTO.setEnListaNegra(DetectorFraudeDTO.EnListaNegra.SI);
				} else {
					fraudeDTO.setEnListaNegra(DetectorFraudeDTO.EnListaNegra.NO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_EnListaNegra_SW2 " + e.getMessage());
		}
		return fraudeDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DetectorFraudeDTO> getDetalleFraudePorRut(final String rut) {
		List<DetectorFraudeDTO> listDetalles = new ArrayList<>();
		
		try {
			
			log.info("Ejecutando SQ_DetListaNegra_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DetListaNegra_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCausal", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecDigitacion", Timestamp.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_Rut", rut);
			
			storedProcedure.execute();
			
			List<Object[]> listResul = storedProcedure.getResultList();
			
			if (listResul != null && !listResul.isEmpty()) {
				for (Object[] objects : listResul) {
					DetectorFraudeDTO fraudeDTO = new DetectorFraudeDTO();
					fraudeDTO.setCausal(objects[0].toString() != null ? objects[0].toString().trim() : "");
					fraudeDTO.setFechaDigitacion(objects[1].toString());
					
					listDetalles.add(fraudeDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_DetListaNegra_SW2 " + e.getMessage());
		}
		
		return listDetalles;
	}

	@Override
	public ResponseDTO validaAutorizacion(final Long corrCaja, final String rut) {
		ResponseDTO response = new ResponseDTO();
		
		try {
			
			log.info("Ejecutando SQ_AutorizacionLN_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_AutorizacionLN_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Rut", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RetornoAux", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);
			storedProcedure.setParameter("Fld_Rut", rut);
			
			storedProcedure.execute();
			
			String poseeAutorizacion = (String) storedProcedure.getOutputParameterValue("Fld_RetornoAux");
			
			if (poseeAutorizacion != null && !poseeAutorizacion.isEmpty()) {
				if (DetectorFraudeDTO.EnListaNegra.SI.value.equalsIgnoreCase(poseeAutorizacion)) {
					response.setIdControl(ResponseDTO.IdRespuestaBinaria.SI.value);
				} else {
					response.setIdControl(ResponseDTO.IdRespuestaBinaria.NO.value);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_AutorizacionLN_SW2 " + e.getMessage());
		}
		
		return response;
	}
}
