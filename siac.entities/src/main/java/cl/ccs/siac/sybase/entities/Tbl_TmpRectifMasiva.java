package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TmpRectifMasivas database table.
 * 
 */
@Entity
@Table(name="Tbl_TmpRectifMasivas")
@NamedQuery(name="Tbl_TmpRectifMasiva.findAll", query="SELECT t FROM Tbl_TmpRectifMasiva t")
public class Tbl_TmpRectifMasiva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CorrCarga_Id")
	private BigDecimal fld_CorrCarga_Id;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_TmpRectifMasiva() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrCarga_Id() {
		return this.fld_CorrCarga_Id;
	}

	public void setFld_CorrCarga_Id(BigDecimal fld_CorrCarga_Id) {
		this.fld_CorrCarga_Id = fld_CorrCarga_Id;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}