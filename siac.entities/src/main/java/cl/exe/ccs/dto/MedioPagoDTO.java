package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MedioPagoDTO extends ResponseDTO implements Serializable{
	
	private Integer idCaja;
	private Integer idCCosto;
	private Integer numboletafac;
	private ComboDTO mediospago;
	private Integer montoIngresadoCajera;
	private String glosaPago;
	private ComboDTO emisor;
	private SucursalDTO sucursal;
	private String numerocheque;
	
	public Integer getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(Integer idCaja) {
		this.idCaja = idCaja;
	}
	public Integer getIdCCosto() {
		return idCCosto;
	}
	public void setIdCCosto(Integer idCCosto) {
		this.idCCosto = idCCosto;
	}
	public Integer getNumboletafac() {
		return numboletafac;
	}
	public void setNumboletafac(Integer numboletafac) {
		this.numboletafac = numboletafac;
	}
	public ComboDTO getMediospago() {
		return mediospago;
	}
	public void setMediospago(ComboDTO mediospago) {
		this.mediospago = mediospago;
	}
	public Integer getMontoIngresadoCajera() {
		return montoIngresadoCajera;
	}
	public void setMontoIngresadoCajera(Integer montoIngresadoCajera) {
		this.montoIngresadoCajera = montoIngresadoCajera;
	}
	public String getGlosaPago() {
		return glosaPago;
	}
	public void setGlosaPago(String glosaPago) {
		this.glosaPago = glosaPago;
	}
	public ComboDTO getEmisor() {
		return emisor;
	}
	public void setEmisor(ComboDTO emisor) {
		this.emisor = emisor;
	}
	public SucursalDTO getSucursal() {
		return sucursal;
	}
	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}
	public String getNumerocheque() {
		return numerocheque;
	}
	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}
	
	

	
	
	
	
	
  

}
