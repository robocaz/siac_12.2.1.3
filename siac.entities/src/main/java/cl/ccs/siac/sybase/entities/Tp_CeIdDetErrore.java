package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdDetErrores database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdDetErrores")
@NamedQuery(name="Tp_CeIdDetErrore.findAll", query="SELECT t FROM Tp_CeIdDetErrore t")
public class Tp_CeIdDetErrore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_GlosaError")
	private String fld_GlosaError;

	public Tp_CeIdDetErrore() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public String getFld_GlosaError() {
		return this.fld_GlosaError;
	}

	public void setFld_GlosaError(String fld_GlosaError) {
		this.fld_GlosaError = fld_GlosaError;
	}

}