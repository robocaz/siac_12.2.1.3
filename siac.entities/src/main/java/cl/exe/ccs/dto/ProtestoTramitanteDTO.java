package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProtestoTramitanteDTO extends ResponseDTO implements Serializable{

	static final long serialVersionUID = -5517800601298544680L;

	private BigDecimal correlativo;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String direccion;
	private String telefono;
	private String giro;
	private String comuna;
	private String region;
	private String email;

	public BigDecimal getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(BigDecimal correlativo) {
		this.correlativo = correlativo;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}

