package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdCtrExtEnvios database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdCtrExtEnvios")
@NamedQuery(name="Tb_CeIdCtrExtEnvio.findAll", query="SELECT t FROM Tb_CeIdCtrExtEnvio t")
public class Tb_CeIdCtrExtEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecExtraccion")
	private Timestamp fld_FecExtraccion;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tb_CeIdCtrExtEnvio() {
	}

	public Timestamp getFld_FecExtraccion() {
		return this.fld_FecExtraccion;
	}

	public void setFld_FecExtraccion(Timestamp fld_FecExtraccion) {
		this.fld_FecExtraccion = fld_FecExtraccion;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}