package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ImprimeBoletas database table.
 * 
 */
@Entity
@Table(name="Tbl_ImprimeBoletas")
@NamedQuery(name="Tbl_ImprimeBoleta.findAll", query="SELECT t FROM Tbl_ImprimeBoleta t")
public class Tbl_ImprimeBoleta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_FecInicioCaja")
	private Timestamp fld_FecInicioCaja;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_GlosaCCosto")
	private String fld_GlosaCCosto;

	@Column(name="Fld_MontoCancelado")
	private int fld_MontoCancelado;

	@Column(name="Fld_MontoCanceladoAcl")
	private int fld_MontoCanceladoAcl;

	@Column(name="Fld_MontoCanceladoOtros")
	private int fld_MontoCanceladoOtros;

	@Column(name="Fld_NroBoleta")
	private int fld_NroBoleta;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TotalDocAcl")
	private short fld_TotalDocAcl;

	@Column(name="Fld_TotalOtrosServ")
	private short fld_TotalOtrosServ;

	public Tbl_ImprimeBoleta() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public Timestamp getFld_FecInicioCaja() {
		return this.fld_FecInicioCaja;
	}

	public void setFld_FecInicioCaja(Timestamp fld_FecInicioCaja) {
		this.fld_FecInicioCaja = fld_FecInicioCaja;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public String getFld_GlosaCCosto() {
		return this.fld_GlosaCCosto;
	}

	public void setFld_GlosaCCosto(String fld_GlosaCCosto) {
		this.fld_GlosaCCosto = fld_GlosaCCosto;
	}

	public int getFld_MontoCancelado() {
		return this.fld_MontoCancelado;
	}

	public void setFld_MontoCancelado(int fld_MontoCancelado) {
		this.fld_MontoCancelado = fld_MontoCancelado;
	}

	public int getFld_MontoCanceladoAcl() {
		return this.fld_MontoCanceladoAcl;
	}

	public void setFld_MontoCanceladoAcl(int fld_MontoCanceladoAcl) {
		this.fld_MontoCanceladoAcl = fld_MontoCanceladoAcl;
	}

	public int getFld_MontoCanceladoOtros() {
		return this.fld_MontoCanceladoOtros;
	}

	public void setFld_MontoCanceladoOtros(int fld_MontoCanceladoOtros) {
		this.fld_MontoCanceladoOtros = fld_MontoCanceladoOtros;
	}

	public int getFld_NroBoleta() {
		return this.fld_NroBoleta;
	}

	public void setFld_NroBoleta(int fld_NroBoleta) {
		this.fld_NroBoleta = fld_NroBoleta;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public short getFld_TotalDocAcl() {
		return this.fld_TotalDocAcl;
	}

	public void setFld_TotalDocAcl(short fld_TotalDocAcl) {
		this.fld_TotalDocAcl = fld_TotalDocAcl;
	}

	public short getFld_TotalOtrosServ() {
		return this.fld_TotalOtrosServ;
	}

	public void setFld_TotalOtrosServ(short fld_TotalOtrosServ) {
		this.fld_TotalOtrosServ = fld_TotalOtrosServ;
	}

}