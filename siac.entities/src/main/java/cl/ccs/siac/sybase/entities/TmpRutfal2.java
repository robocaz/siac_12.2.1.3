package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutfal2 database table.
 * 
 */
@Entity
@Table(name="tmp_rutfal2")
@NamedQuery(name="TmpRutfal2.findAll", query="SELECT t FROM TmpRutfal2 t")
public class TmpRutfal2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Cta")
	private int cta;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public TmpRutfal2() {
	}

	public int getCta() {
		return this.cta;
	}

	public void setCta(int cta) {
		this.cta = cta;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}