package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutNomFlagBICMCCVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutNomFlagBICMCCVCE.findAll", query="SELECT t FROM Tbl_RutNomFlagBICMCCVCE t")
public class Tbl_RutNomFlagBICMCCVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_FlagExisteACL")
	private boolean fld_FlagExisteACL;

	@Column(name="Fld_FlagExisteBIC")
	private boolean fld_FlagExisteBIC;

	@Column(name="Fld_FlagExisteIAC")
	private boolean fld_FlagExisteIAC;

	@Column(name="Fld_FlagExisteMCC")
	private boolean fld_FlagExisteMCC;

	@Column(name="Fld_FlagRutErroneo")
	private boolean fld_FlagRutErroneo;

	@Column(name="Fld_Nombre")
	private String fld_Nombre;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_RutNomFlagBICMCCVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public boolean getFld_FlagExisteACL() {
		return this.fld_FlagExisteACL;
	}

	public void setFld_FlagExisteACL(boolean fld_FlagExisteACL) {
		this.fld_FlagExisteACL = fld_FlagExisteACL;
	}

	public boolean getFld_FlagExisteBIC() {
		return this.fld_FlagExisteBIC;
	}

	public void setFld_FlagExisteBIC(boolean fld_FlagExisteBIC) {
		this.fld_FlagExisteBIC = fld_FlagExisteBIC;
	}

	public boolean getFld_FlagExisteIAC() {
		return this.fld_FlagExisteIAC;
	}

	public void setFld_FlagExisteIAC(boolean fld_FlagExisteIAC) {
		this.fld_FlagExisteIAC = fld_FlagExisteIAC;
	}

	public boolean getFld_FlagExisteMCC() {
		return this.fld_FlagExisteMCC;
	}

	public void setFld_FlagExisteMCC(boolean fld_FlagExisteMCC) {
		this.fld_FlagExisteMCC = fld_FlagExisteMCC;
	}

	public boolean getFld_FlagRutErroneo() {
		return this.fld_FlagRutErroneo;
	}

	public void setFld_FlagRutErroneo(boolean fld_FlagRutErroneo) {
		this.fld_FlagRutErroneo = fld_FlagRutErroneo;
	}

	public String getFld_Nombre() {
		return this.fld_Nombre;
	}

	public void setFld_Nombre(String fld_Nombre) {
		this.fld_Nombre = fld_Nombre;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}