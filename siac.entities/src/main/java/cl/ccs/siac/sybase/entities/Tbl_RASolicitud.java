package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RASolicitud database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RASolicitud.findAll", query="SELECT t FROM Tbl_RASolicitud t")
public class Tbl_RASolicitud implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodTribunal")
	private String fld_CodTribunal;

	@Column(name="Fld_CodUsuarioModificacion")
	private String fld_CodUsuarioModificacion;

	@Column(name="Fld_CodUsuarioRecep")
	private String fld_CodUsuarioRecep;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrNombreArrendador")
	private BigDecimal fld_CorrNombreArrendador;

	@Column(name="Fld_CorrNombreSolicitante")
	private BigDecimal fld_CorrNombreSolicitante;

	@Column(name="Fld_CorrSolicArriendo_Id")
	private BigDecimal fld_CorrSolicArriendo_Id;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_FecSentencia")
	private Timestamp fld_FecSentencia;

	@Column(name="Fld_Observacion")
	private String fld_Observacion;

	@Column(name="Fld_RolCausa")
	private String fld_RolCausa;

	@Column(name="Fld_RutArrendador")
	private String fld_RutArrendador;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_Telefono")
	private BigDecimal fld_Telefono;

	@Column(name="Fld_TipoAfectado")
	private String fld_TipoAfectado;

	@Column(name="Fld_TipoSolicitud")
	private String fld_TipoSolicitud;

	public Tbl_RASolicitud() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodTribunal() {
		return this.fld_CodTribunal;
	}

	public void setFld_CodTribunal(String fld_CodTribunal) {
		this.fld_CodTribunal = fld_CodTribunal;
	}

	public String getFld_CodUsuarioModificacion() {
		return this.fld_CodUsuarioModificacion;
	}

	public void setFld_CodUsuarioModificacion(String fld_CodUsuarioModificacion) {
		this.fld_CodUsuarioModificacion = fld_CodUsuarioModificacion;
	}

	public String getFld_CodUsuarioRecep() {
		return this.fld_CodUsuarioRecep;
	}

	public void setFld_CodUsuarioRecep(String fld_CodUsuarioRecep) {
		this.fld_CodUsuarioRecep = fld_CodUsuarioRecep;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrNombreArrendador() {
		return this.fld_CorrNombreArrendador;
	}

	public void setFld_CorrNombreArrendador(BigDecimal fld_CorrNombreArrendador) {
		this.fld_CorrNombreArrendador = fld_CorrNombreArrendador;
	}

	public BigDecimal getFld_CorrNombreSolicitante() {
		return this.fld_CorrNombreSolicitante;
	}

	public void setFld_CorrNombreSolicitante(BigDecimal fld_CorrNombreSolicitante) {
		this.fld_CorrNombreSolicitante = fld_CorrNombreSolicitante;
	}

	public BigDecimal getFld_CorrSolicArriendo_Id() {
		return this.fld_CorrSolicArriendo_Id;
	}

	public void setFld_CorrSolicArriendo_Id(BigDecimal fld_CorrSolicArriendo_Id) {
		this.fld_CorrSolicArriendo_Id = fld_CorrSolicArriendo_Id;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public Timestamp getFld_FecSentencia() {
		return this.fld_FecSentencia;
	}

	public void setFld_FecSentencia(Timestamp fld_FecSentencia) {
		this.fld_FecSentencia = fld_FecSentencia;
	}

	public String getFld_Observacion() {
		return this.fld_Observacion;
	}

	public void setFld_Observacion(String fld_Observacion) {
		this.fld_Observacion = fld_Observacion;
	}

	public String getFld_RolCausa() {
		return this.fld_RolCausa;
	}

	public void setFld_RolCausa(String fld_RolCausa) {
		this.fld_RolCausa = fld_RolCausa;
	}

	public String getFld_RutArrendador() {
		return this.fld_RutArrendador;
	}

	public void setFld_RutArrendador(String fld_RutArrendador) {
		this.fld_RutArrendador = fld_RutArrendador;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public BigDecimal getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(BigDecimal fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

	public String getFld_TipoAfectado() {
		return this.fld_TipoAfectado;
	}

	public void setFld_TipoAfectado(String fld_TipoAfectado) {
		this.fld_TipoAfectado = fld_TipoAfectado;
	}

	public String getFld_TipoSolicitud() {
		return this.fld_TipoSolicitud;
	}

	public void setFld_TipoSolicitud(String fld_TipoSolicitud) {
		this.fld_TipoSolicitud = fld_TipoSolicitud;
	}

}