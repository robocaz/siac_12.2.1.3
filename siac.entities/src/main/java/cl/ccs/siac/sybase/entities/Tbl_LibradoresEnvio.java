package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LibradoresEnvios database table.
 * 
 */
@Entity
@Table(name="Tbl_LibradoresEnvios")
@NamedQuery(name="Tbl_LibradoresEnvio.findAll", query="SELECT t FROM Tbl_LibradoresEnvio t")
public class Tbl_LibradoresEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_LibradoresEnvioPK id;

	@Column(name="Fld_EstadoEnvio")
	private byte fld_EstadoEnvio;

	public Tbl_LibradoresEnvio() {
	}

	public Tbl_LibradoresEnvioPK getId() {
		return this.id;
	}

	public void setId(Tbl_LibradoresEnvioPK id) {
		this.id = id;
	}

	public byte getFld_EstadoEnvio() {
		return this.fld_EstadoEnvio;
	}

	public void setFld_EstadoEnvio(byte fld_EstadoEnvio) {
		this.fld_EstadoEnvio = fld_EstadoEnvio;
	}

}