package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_GeneracionCartas database table.
 * 
 */
@Entity
@Table(name="Tbl_GeneracionCartas")
@NamedQuery(name="Tbl_GeneracionCarta.findAll", query="SELECT t FROM Tbl_GeneracionCarta t")
public class Tbl_GeneracionCarta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FlagGeneracion")
	private short fld_FlagGeneracion;

	@Column(name="Fld_TipoCarta")
	private short fld_TipoCarta;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TotDev")
	private int fld_TotDev;

	public Tbl_GeneracionCarta() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public short getFld_FlagGeneracion() {
		return this.fld_FlagGeneracion;
	}

	public void setFld_FlagGeneracion(short fld_FlagGeneracion) {
		this.fld_FlagGeneracion = fld_FlagGeneracion;
	}

	public short getFld_TipoCarta() {
		return this.fld_TipoCarta;
	}

	public void setFld_TipoCarta(short fld_TipoCarta) {
		this.fld_TipoCarta = fld_TipoCarta;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public int getFld_TotDev() {
		return this.fld_TotDev;
	}

	public void setFld_TotDev(int fld_TotDev) {
		this.fld_TotDev = fld_TotDev;
	}

}