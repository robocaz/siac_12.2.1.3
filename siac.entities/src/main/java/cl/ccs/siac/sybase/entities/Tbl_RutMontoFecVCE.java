package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RutMontoFecVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutMontoFecVCE.findAll", query="SELECT t FROM Tbl_RutMontoFecVCE t")
public class Tbl_RutMontoFecVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_FecMasNueva")
	private Timestamp fld_FecMasNueva;

	@Column(name="Fld_NroDocumentos")
	private short fld_NroDocumentos;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TotalMontoProt")
	private BigDecimal fld_TotalMontoProt;

	public Tbl_RutMontoFecVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public Timestamp getFld_FecMasNueva() {
		return this.fld_FecMasNueva;
	}

	public void setFld_FecMasNueva(Timestamp fld_FecMasNueva) {
		this.fld_FecMasNueva = fld_FecMasNueva;
	}

	public short getFld_NroDocumentos() {
		return this.fld_NroDocumentos;
	}

	public void setFld_NroDocumentos(short fld_NroDocumentos) {
		this.fld_NroDocumentos = fld_NroDocumentos;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public BigDecimal getFld_TotalMontoProt() {
		return this.fld_TotalMontoProt;
	}

	public void setFld_TotalMontoProt(BigDecimal fld_TotalMontoProt) {
		this.fld_TotalMontoProt = fld_TotalMontoProt;
	}

}