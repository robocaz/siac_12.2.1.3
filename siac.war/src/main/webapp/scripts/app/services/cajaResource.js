angular.module('ccs').factory('CajaResource', function ($resource) {
    var resource = $resource('rest/caja/',  { nocache : new Date().getTime()},
    {
        'cantidadabiertas': { method : 'GET', isArray: false, url : 'rest/caja/cantidadabiertas' , nocache : new Date().getTime()},
        'cierracaja': { method : 'GET', isArray: false, url : 'rest/caja/cierracaja' , nocache : new Date().getTime()},
        'cajasabiertas'   : { method : 'GET', isArray: true,  url : 'rest/caja/cajasabiertas' },
        'aperturacaja'    : { method : 'GET', isArray: false, url : 'rest/caja/aperturacaja' },
        'cajaseleccionada': { method : 'POST', isArray: false, url : 'rest/caja/seleccionarcaja' },
        'tomarcaja': { method : 'POST', isArray: false, url : 'rest/caja/tomarcaja' },
        'cerrarcajatemporal': { method : 'POST', isArray: false, url : 'rest/caja/cerrarcajatemporal' },
        'cerrarcajadefinitivo': { method : 'POST', isArray: false, url : 'rest/caja/cerrarcajadefinitivo' },
        'listarcajastodas': { method : 'POST', isArray: true, url : 'rest/caja/listarcajastodas' },
        'listarCajasDia': { method: 'GET', isArray:true, url: 'rest/caja/listarCajasDia' },
        'cajapendiente': { method : 'GET', isArray: false, url : 'rest/caja/cajapendiente' }
    });
    return resource;
});
