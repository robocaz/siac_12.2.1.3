package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TBL_GLOSAERROR database table.
 * 
 */
@Embeddable
public class TblGlosaerrorPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_TIPO")
	private long fldTipo;

	@Column(name="FLD_CODIGO")
	private long fldCodigo;

	public TblGlosaerrorPK() {
	}
	public long getFldTipo() {
		return this.fldTipo;
	}
	public void setFldTipo(long fldTipo) {
		this.fldTipo = fldTipo;
	}
	public long getFldCodigo() {
		return this.fldCodigo;
	}
	public void setFldCodigo(long fldCodigo) {
		this.fldCodigo = fldCodigo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TblGlosaerrorPK)) {
			return false;
		}
		TblGlosaerrorPK castOther = (TblGlosaerrorPK)other;
		return 
			(this.fldTipo == castOther.fldTipo)
			&& (this.fldCodigo == castOther.fldCodigo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fldTipo ^ (this.fldTipo >>> 32)));
		hash = hash * prime + ((int) (this.fldCodigo ^ (this.fldCodigo >>> 32)));
		
		return hash;
	}
}