package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class DetalleBoletaFacturaDTO extends ResponseDTO implements Serializable{

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 7950599781741216807L;
	private Integer corrCaja;
    private Integer nroBoletaFactura;
    private String boletaFactura;
    private Integer corrTransaccion;
    private Integer codCCosto;
    private String fecInicioCaja;
    private Long montoPagado;
    private String nombre;
    private List<DatoBoletaFacturaDTO> datosBoletaFactura;
    private DetalleFacturaDTO detalleFactura;
    
    
    
    
    
    
	public DetalleFacturaDTO getDetalleFactura() {
		return detalleFactura;
	}
	public void setDetalleFactura(DetalleFacturaDTO detalleFactura) {
		this.detalleFactura = detalleFactura;
	}

	public List<DatoBoletaFacturaDTO> getDatosBoletaFactura() {
		return datosBoletaFactura;
	}
	public void setDatosBoletaFactura(List<DatoBoletaFacturaDTO> datosBoletaFactura) {
		this.datosBoletaFactura = datosBoletaFactura;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getNroBoletaFactura() {
		return nroBoletaFactura;
	}
	public void setNroBoletaFactura(Integer nroBoletaFactura) {
		this.nroBoletaFactura = nroBoletaFactura;
	}
	public String getBoletaFactura() {
		return boletaFactura;
	}
	public void setBoletaFactura(String boletaFactura) {
		this.boletaFactura = boletaFactura;
	}
	public Integer getCorrTransaccion() {
		return corrTransaccion;
	}
	public void setCorrTransaccion(Integer corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}
	public Integer getCodCCosto() {
		return codCCosto;
	}
	public void setCodCCosto(Integer codCCosto) {
		this.codCCosto = codCCosto;
	}
	public String getFecInicioCaja() {
		return fecInicioCaja;
	}
	public void setFecInicioCaja(String fecInicioCaja) {
		this.fecInicioCaja = fecInicioCaja;
	}
	public Long getMontoPagado() {
		return montoPagado;
	}
	public void setMontoPagado(Long montoPagado) {
		this.montoPagado = montoPagado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
    
    
    
    
  
}
