package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABIMov database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABIMov.findAll", query="SELECT t FROM Tbl_ABIMov t")
public class Tbl_ABIMov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrContrato")
	private BigDecimal fld_CorrContrato;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_CorrVendedorABI")
	private BigDecimal fld_CorrVendedorABI;

	@Column(name="Fld_DuracionContrato")
	private byte fld_DuracionContrato;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_FolioABI")
	private int fld_FolioABI;

	public Tbl_ABIMov() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrContrato() {
		return this.fld_CorrContrato;
	}

	public void setFld_CorrContrato(BigDecimal fld_CorrContrato) {
		this.fld_CorrContrato = fld_CorrContrato;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public BigDecimal getFld_CorrVendedorABI() {
		return this.fld_CorrVendedorABI;
	}

	public void setFld_CorrVendedorABI(BigDecimal fld_CorrVendedorABI) {
		this.fld_CorrVendedorABI = fld_CorrVendedorABI;
	}

	public byte getFld_DuracionContrato() {
		return this.fld_DuracionContrato;
	}

	public void setFld_DuracionContrato(byte fld_DuracionContrato) {
		this.fld_DuracionContrato = fld_DuracionContrato;
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public int getFld_FolioABI() {
		return this.fld_FolioABI;
	}

	public void setFld_FolioABI(int fld_FolioABI) {
		this.fld_FolioABI = fld_FolioABI;
	}

}