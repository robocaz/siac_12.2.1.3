package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.Date;

public class CajaCuadraturaDTO extends TipoDTO implements Serializable{

	
	private Integer centroCosto; 
	private Date fechaDesde;
	private Date fechaHasta;
	private Long totMontoMovimientos;
	private Long totMontoTransacciones;
	private Long totMontoPagado;
	private Long totDifMontoTrans;
	private Long totDifMontoMvntos;
	
	
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Long getTotMontoMovimientos() {
		return totMontoMovimientos;
	}
	public void setTotMontoMovimientos(Long totMontoMovimientos) {
		this.totMontoMovimientos = totMontoMovimientos;
	}
	public Long getTotMontoTransacciones() {
		return totMontoTransacciones;
	}
	public void setTotMontoTransacciones(Long totMontoTransacciones) {
		this.totMontoTransacciones = totMontoTransacciones;
	}
	public Long getTotMontoPagado() {
		return totMontoPagado;
	}
	public void setTotMontoPagado(Long totMontoPagado) {
		this.totMontoPagado = totMontoPagado;
	}
	public Long getTotDifMontoTrans() {
		return totDifMontoTrans;
	}
	public void setTotDifMontoTrans(Long totDifMontoTrans) {
		this.totDifMontoTrans = totDifMontoTrans;
	}
	public Long getTotDifMontoMvntos() {
		return totDifMontoMvntos;
	}
	public void setTotDifMontoMvntos(Long totDifMontoMvntos) {
		this.totDifMontoMvntos = totDifMontoMvntos;
	}
	
	
	
	
	
}
