package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class DetalleServicioDTO implements Serializable{
	
	private String usuario;
	private Integer centroCosto;
	private String fecha;
	private Integer corrCaja;
	private Long totalCalculado;
    private Long totalPagado;
    private Long totalDiferencia;
    private Integer totalTransacciones;
    private Integer totalMovimientos;
    private Integer retorno ;
    private String mensaje;
    private List<DetalleListaServicioDTO> servicios;
    
    
    
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Long getTotalCalculado() {
		return totalCalculado;
	}
	public void setTotalCalculado(Long totalCalculado) {
		this.totalCalculado = totalCalculado;
	}
	public Long getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(Long totalPagado) {
		this.totalPagado = totalPagado;
	}
	public Long getTotalDiferencia() {
		return totalDiferencia;
	}
	public void setTotalDiferencia(Long totalDiferencia) {
		this.totalDiferencia = totalDiferencia;
	}
	public Integer getTotalTransacciones() {
		return totalTransacciones;
	}
	public void setTotalTransacciones(Integer totalTransacciones) {
		this.totalTransacciones = totalTransacciones;
	}
	public Integer getTotalMovimientos() {
		return totalMovimientos;
	}
	public void setTotalMovimientos(Integer totalMovimientos) {
		this.totalMovimientos = totalMovimientos;
	}
	public Integer getRetorno() {
		return retorno;
	}
	public void setRetorno(Integer retorno) {
		this.retorno = retorno;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<DetalleListaServicioDTO> getServicios() {
		return servicios;
	}
	public void setServicios(List<DetalleListaServicioDTO> servicios) {
		this.servicios = servicios;
	}
    
    
    
    

}
