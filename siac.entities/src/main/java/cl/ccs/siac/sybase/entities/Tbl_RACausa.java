package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RACausa database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RACausa.findAll", query="SELECT t FROM Tbl_RACausa t")
public class Tbl_RACausa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausa")
	private String fld_CodCausa;

	@Column(name="Fld_GlosaCausa")
	private String fld_GlosaCausa;

	public Tbl_RACausa() {
	}

	public String getFld_CodCausa() {
		return this.fld_CodCausa;
	}

	public void setFld_CodCausa(String fld_CodCausa) {
		this.fld_CodCausa = fld_CodCausa;
	}

	public String getFld_GlosaCausa() {
		return this.fld_GlosaCausa;
	}

	public void setFld_GlosaCausa(String fld_GlosaCausa) {
		this.fld_GlosaCausa = fld_GlosaCausa;
	}

}