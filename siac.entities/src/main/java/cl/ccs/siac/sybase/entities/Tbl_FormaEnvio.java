package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_FormaEnvio database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FormaEnvio.findAll", query="SELECT t FROM Tbl_FormaEnvio t")
public class Tbl_FormaEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_ClasificacionEnvio")
	private boolean fld_ClasificacionEnvio;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_GlosaFormaEnvio")
	private String fld_GlosaFormaEnvio;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoCarta")
	private short fld_TipoCarta;

	public Tbl_FormaEnvio() {
	}

	public boolean getFld_ClasificacionEnvio() {
		return this.fld_ClasificacionEnvio;
	}

	public void setFld_ClasificacionEnvio(boolean fld_ClasificacionEnvio) {
		this.fld_ClasificacionEnvio = fld_ClasificacionEnvio;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public String getFld_GlosaFormaEnvio() {
		return this.fld_GlosaFormaEnvio;
	}

	public void setFld_GlosaFormaEnvio(String fld_GlosaFormaEnvio) {
		this.fld_GlosaFormaEnvio = fld_GlosaFormaEnvio;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public short getFld_TipoCarta() {
		return this.fld_TipoCarta;
	}

	public void setFld_TipoCarta(short fld_TipoCarta) {
		this.fld_TipoCarta = fld_TipoCarta;
	}

}