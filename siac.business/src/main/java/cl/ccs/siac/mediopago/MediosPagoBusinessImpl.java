package cl.ccs.siac.mediopago;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.MediosPagoDao;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.MedioPagoDTO;

@Named("mediosPagoBusiness")
public class MediosPagoBusinessImpl implements MediosPagoBusiness{

	@Inject
	private MediosPagoDao mediosPago;
	
	public List<ComboDTO> obtieneMediosPagos(){
		
		List<ComboDTO> mediosDePago = mediosPago.obtenerMediosPago();
		return mediosDePago;
	}

	
	public MedioPagoDTO obtenerBoletas(MedioPagoDTO medioPagoDTO){
		MedioPagoDTO boletas = mediosPago.obtenerBoletas(medioPagoDTO);
		return boletas;
	}
	
	
	public MedioPagoDTO actualizaMedioDePago(MedioPagoDTO medioPagoDTO){
		MedioPagoDTO boletaActualizar = mediosPago.actualizaMedioDePago(medioPagoDTO);
		return boletaActualizar;
	}
}
