package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Regiones database table.
 * 
 */
@Entity
@Table(name="Tbl_Regiones")
@NamedQuery(name="Tbl_Regione.findAll", query="SELECT t FROM Tbl_Regione t")
public class Tbl_Regione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_GlosaRegion")
	private String fld_GlosaRegion;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Regione() {
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_GlosaRegion() {
		return this.fld_GlosaRegion;
	}

	public void setFld_GlosaRegion(String fld_GlosaRegion) {
		this.fld_GlosaRegion = fld_GlosaRegion;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}