package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdActEconomica database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdActEconomica.findAll", query="SELECT t FROM Tp_CeIdActEconomica t")
public class Tp_CeIdActEconomica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodActEconomica")
	private int fld_CodActEconomica;

	@Column(name="Fld_GlosaActEconomica")
	private String fld_GlosaActEconomica;

	public Tp_CeIdActEconomica() {
	}

	public int getFld_CodActEconomica() {
		return this.fld_CodActEconomica;
	}

	public void setFld_CodActEconomica(int fld_CodActEconomica) {
		this.fld_CodActEconomica = fld_CodActEconomica;
	}

	public String getFld_GlosaActEconomica() {
		return this.fld_GlosaActEconomica;
	}

	public void setFld_GlosaActEconomica(String fld_GlosaActEconomica) {
		this.fld_GlosaActEconomica = fld_GlosaActEconomica;
	}

}