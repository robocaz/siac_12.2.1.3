package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_Function database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Function.findAll", query="SELECT t FROM Tbl_Function t")
public class Tbl_Function implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FunctionDescription")
	private String fld_FunctionDescription;

	@Column(name="Fld_FunctionName")
	private String fld_FunctionName;

	@Column(name="Fld_FunctionTimeStamp")
	private byte[] fld_FunctionTimeStamp;

	@Column(name="Fld_MayDelete")
	private boolean fld_MayDelete;

	@Column(name="Fld_MayUpdate")
	private boolean fld_MayUpdate;

	public Tbl_Function() {
	}

	public String getFld_FunctionDescription() {
		return this.fld_FunctionDescription;
	}

	public void setFld_FunctionDescription(String fld_FunctionDescription) {
		this.fld_FunctionDescription = fld_FunctionDescription;
	}

	public String getFld_FunctionName() {
		return this.fld_FunctionName;
	}

	public void setFld_FunctionName(String fld_FunctionName) {
		this.fld_FunctionName = fld_FunctionName;
	}

	public byte[] getFld_FunctionTimeStamp() {
		return this.fld_FunctionTimeStamp;
	}

	public void setFld_FunctionTimeStamp(byte[] fld_FunctionTimeStamp) {
		this.fld_FunctionTimeStamp = fld_FunctionTimeStamp;
	}

	public boolean getFld_MayDelete() {
		return this.fld_MayDelete;
	}

	public void setFld_MayDelete(boolean fld_MayDelete) {
		this.fld_MayDelete = fld_MayDelete;
	}

	public boolean getFld_MayUpdate() {
		return this.fld_MayUpdate;
	}

	public void setFld_MayUpdate(boolean fld_MayUpdate) {
		this.fld_MayUpdate = fld_MayUpdate;
	}

}