package cl.exe.ccs.dto;

import java.io.Serializable;

public class DetalleListaServicioDTO implements Serializable{
	
	private Integer corrCaja;
	private Long corrMovimiento;
	private String boletaFactura;
	private Integer nroBoletaFactura;
	private String glosaServicio;
	private String rutTramitante;
	private String rutAfectado;
	private Double montoMovimiento;
	private Double montoCancelado;
	private Double montoDiferencia;
	private String tipoDocumento;
	private String glosaCorta;
	private Double montoProt;
	private String fechaProt;
	private Integer nroOper4Dig;
	private String glosaEmisor;
	private String codServicio;
	
	
	
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Long getCorrMovimiento() {
		return corrMovimiento;
	}
	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}
	public String getBoletaFactura() {
		return boletaFactura;
	}
	public void setBoletaFactura(String boletaFactura) {
		this.boletaFactura = boletaFactura;
	}
	public Integer getNroBoletaFactura() {
		return nroBoletaFactura;
	}
	public void setNroBoletaFactura(Integer nroBoletaFactura) {
		this.nroBoletaFactura = nroBoletaFactura;
	}
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public Double getMontoMovimiento() {
		return montoMovimiento;
	}
	public void setMontoMovimiento(Double montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}
	public Double getMontoCancelado() {
		return montoCancelado;
	}
	public void setMontoCancelado(Double montoCancelado) {
		this.montoCancelado = montoCancelado;
	}
	public Double getMontoDiferencia() {
		return montoDiferencia;
	}
	public void setMontoDiferencia(Double montoDiferencia) {
		this.montoDiferencia = montoDiferencia;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getGlosaCorta() {
		return glosaCorta;
	}
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
	public Double getMontoProt() {
		return montoProt;
	}
	public void setMontoProt(Double montoProt) {
		this.montoProt = montoProt;
	}
	public String getFechaProt() {
		return fechaProt;
	}
	public void setFechaProt(String fechaProt) {
		this.fechaProt = fechaProt;
	}
	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}
	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}
	public String getGlosaEmisor() {
		return glosaEmisor;
	}
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	
	
	
	
	
	
	
	
	
	
	

}
