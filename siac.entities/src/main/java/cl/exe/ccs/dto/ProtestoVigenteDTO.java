package cl.exe.ccs.dto;

import java.io.Serializable;

public class ProtestoVigenteDTO implements Serializable {
    
    private Integer nroBoletin;
    private Integer pagBoletin;
    private String fecProt;
    private String fecVcto;
    private String tipoDocumento;
    private String codEmisor;
    private Integer nroOper4Dig;
    private String glosaCorta;
    private Float montoProt;
    private String glosaLocPub;
    private String glosaEmisor;
    private String nombreLibrador;
    private String fecPub;
    private String tipoDocImpago;
    private String fecRecepAcl;
    private String MarcaAcl;
    
    
    
	public Integer getNroBoletin() {
		return nroBoletin;
	}
	public void setNroBoletin(Integer nroBoletin) {
		this.nroBoletin = nroBoletin;
	}
	public Integer getPagBoletin() {
		return pagBoletin;
	}
	public void setPagBoletin(Integer pagBoletin) {
		this.pagBoletin = pagBoletin;
	}
	public String getFecProt() {
		return fecProt;
	}
	public void setFecProt(String fecProt) {
		this.fecProt = fecProt;
	}
	public String getFecVcto() {
		return fecVcto;
	}
	public void setFecVcto(String fecVcto) {
		this.fecVcto = fecVcto;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}
	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}
	public String getGlosaCorta() {
		return glosaCorta;
	}
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
	public Float getMontoProt() {
		return montoProt;
	}
	public void setMontoProt(Float montoProt) {
		this.montoProt = montoProt;
	}
	public String getGlosaLocPub() {
		return glosaLocPub;
	}
	public void setGlosaLocPub(String glosaLocPub) {
		this.glosaLocPub = glosaLocPub;
	}
	public String getGlosaEmisor() {
		return glosaEmisor;
	}
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}
	public String getNombreLibrador() {
		return nombreLibrador;
	}
	public void setNombreLibrador(String nombreLibrador) {
		this.nombreLibrador = nombreLibrador;
	}
	public String getFecPub() {
		return fecPub;
	}
	public void setFecPub(String fecPub) {
		this.fecPub = fecPub;
	}
	public String getTipoDocImpago() {
		return tipoDocImpago;
	}
	public void setTipoDocImpago(String tipoDocImpago) {
		this.tipoDocImpago = tipoDocImpago;
	}
	public String getFecRecepAcl() {
		return fecRecepAcl;
	}
	public void setFecRecepAcl(String fecRecepAcl) {
		this.fecRecepAcl = fecRecepAcl;
	}
	public String getMarcaAcl() {
		return MarcaAcl;
	}
	public void setMarcaAcl(String marcaAcl) {
		MarcaAcl = marcaAcl;
	}
    
    
    

}
