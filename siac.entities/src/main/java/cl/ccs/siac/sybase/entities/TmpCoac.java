package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_coac database table.
 * 
 */
@Entity
@Table(name="tmp_coac")
@NamedQuery(name="TmpCoac.findAll", query="SELECT t FROM TmpCoac t")
public class TmpCoac implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Cantidad")
	private short cantidad;

	@Column(name="Fecha")
	private String fecha;

	@Column(name="Mes")
	private byte mes;

	@Column(name="RutAfectado")
	private String rutAfectado;

	@Column(name="RutSolicitante")
	private String rutSolicitante;

	@Column(name="Servicio")
	private String servicio;

	public TmpCoac() {
	}

	public short getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(short cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return this.fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public byte getMes() {
		return this.mes;
	}

	public void setMes(byte mes) {
		this.mes = mes;
	}

	public String getRutAfectado() {
		return this.rutAfectado;
	}

	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}

	public String getRutSolicitante() {
		return this.rutSolicitante;
	}

	public void setRutSolicitante(String rutSolicitante) {
		this.rutSolicitante = rutSolicitante;
	}

	public String getServicio() {
		return this.servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

}