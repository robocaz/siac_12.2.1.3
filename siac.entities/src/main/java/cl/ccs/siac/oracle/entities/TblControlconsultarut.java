package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONTROLCONSULTARUT database table.
 * 
 */
@Entity
@Table(name="TBL_CONTROLCONSULTARUT")
@NamedQuery(name="TblControlconsultarut.findAll", query="SELECT t FROM TblControlconsultarut t")
public class TblControlconsultarut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_ANOMES")
	private BigDecimal fldAnomes;

	@Column(name="FLD_ARCHIVO")
	private String fldArchivo;

	@Column(name="FLD_CODTRANSACCION")
	private BigDecimal fldCodtransaccion;

	@Column(name="FLD_CORRCONTROLCAR_ID")
	private BigDecimal fldCorrcontrolcarId;

	@Column(name="FLD_DISTRIBUIDOR")
	private String fldDistribuidor;

	@Column(name="FLD_ESTADO")
	private BigDecimal fldEstado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIO")
	private Date fldFechainicio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHALLEGADA")
	private Date fldFechallegada;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATERMINO")
	private Date fldFechatermino;

	@Column(name="FLD_REGISTROS")
	private BigDecimal fldRegistros;

	@Column(name="FLD_REGPROCESADOS")
	private BigDecimal fldRegprocesados;

	@Column(name="FLD_REGRESPUESTA")
	private BigDecimal fldRegrespuesta;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	public TblControlconsultarut() {
	}

	public BigDecimal getFldAnomes() {
		return this.fldAnomes;
	}

	public void setFldAnomes(BigDecimal fldAnomes) {
		this.fldAnomes = fldAnomes;
	}

	public String getFldArchivo() {
		return this.fldArchivo;
	}

	public void setFldArchivo(String fldArchivo) {
		this.fldArchivo = fldArchivo;
	}

	public BigDecimal getFldCodtransaccion() {
		return this.fldCodtransaccion;
	}

	public void setFldCodtransaccion(BigDecimal fldCodtransaccion) {
		this.fldCodtransaccion = fldCodtransaccion;
	}

	public BigDecimal getFldCorrcontrolcarId() {
		return this.fldCorrcontrolcarId;
	}

	public void setFldCorrcontrolcarId(BigDecimal fldCorrcontrolcarId) {
		this.fldCorrcontrolcarId = fldCorrcontrolcarId;
	}

	public String getFldDistribuidor() {
		return this.fldDistribuidor;
	}

	public void setFldDistribuidor(String fldDistribuidor) {
		this.fldDistribuidor = fldDistribuidor;
	}

	public BigDecimal getFldEstado() {
		return this.fldEstado;
	}

	public void setFldEstado(BigDecimal fldEstado) {
		this.fldEstado = fldEstado;
	}

	public Date getFldFechainicio() {
		return this.fldFechainicio;
	}

	public void setFldFechainicio(Date fldFechainicio) {
		this.fldFechainicio = fldFechainicio;
	}

	public Date getFldFechallegada() {
		return this.fldFechallegada;
	}

	public void setFldFechallegada(Date fldFechallegada) {
		this.fldFechallegada = fldFechallegada;
	}

	public Date getFldFechatermino() {
		return this.fldFechatermino;
	}

	public void setFldFechatermino(Date fldFechatermino) {
		this.fldFechatermino = fldFechatermino;
	}

	public BigDecimal getFldRegistros() {
		return this.fldRegistros;
	}

	public void setFldRegistros(BigDecimal fldRegistros) {
		this.fldRegistros = fldRegistros;
	}

	public BigDecimal getFldRegprocesados() {
		return this.fldRegprocesados;
	}

	public void setFldRegprocesados(BigDecimal fldRegprocesados) {
		this.fldRegprocesados = fldRegprocesados;
	}

	public BigDecimal getFldRegrespuesta() {
		return this.fldRegrespuesta;
	}

	public void setFldRegrespuesta(BigDecimal fldRegrespuesta) {
		this.fldRegrespuesta = fldRegrespuesta;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

}