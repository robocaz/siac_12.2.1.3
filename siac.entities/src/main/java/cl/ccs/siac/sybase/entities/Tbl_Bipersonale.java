package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Bipersonales database table.
 * 
 */
@Entity
@Table(name="Tbl_Bipersonales")
@NamedQuery(name="Tbl_Bipersonale.findAll", query="SELECT t FROM Tbl_Bipersonale t")
public class Tbl_Bipersonale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrDocum")
	private BigDecimal fld_CorrDocum;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_CorrUnico")
	private BigDecimal fld_CorrUnico;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_Bipersonale() {
	}

	public BigDecimal getFld_CorrDocum() {
		return this.fld_CorrDocum;
	}

	public void setFld_CorrDocum(BigDecimal fld_CorrDocum) {
		this.fld_CorrDocum = fld_CorrDocum;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public BigDecimal getFld_CorrUnico() {
		return this.fld_CorrUnico;
	}

	public void setFld_CorrUnico(BigDecimal fld_CorrUnico) {
		this.fld_CorrUnico = fld_CorrUnico;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}