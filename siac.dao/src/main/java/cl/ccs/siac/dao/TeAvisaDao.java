package cl.ccs.siac.dao;

import java.util.List;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TeAvisaDTO;

public interface TeAvisaDao {

	
	TeAvisaDTO contrataTeAvisa(TeAvisaDTO contrato);
	
	ResponseDTO validaContratoTeAvisa(String rutAvisa);
	
	List<TeAvisaDTO> buscaContratos(String rut);
	
	List<TeAvisaDTO> buscaContratosRenovacion(String rut, Integer criterio, Long corrContrato);
	
	Long costoServicioUpgrade(Long corrContrato, String codServicio);
}
