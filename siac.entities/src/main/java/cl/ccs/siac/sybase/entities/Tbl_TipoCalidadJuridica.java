package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoCalidadJuridica database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoCalidadJuridica.findAll", query="SELECT t FROM Tbl_TipoCalidadJuridica t")
public class Tbl_TipoCalidadJuridica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalidadJuridica")
	private String fld_CalidadJuridica;

	@Column(name="Fld_GlosaCalidadJuridica")
	private String fld_GlosaCalidadJuridica;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_TipoCalidadJuridica() {
	}

	public String getFld_CalidadJuridica() {
		return this.fld_CalidadJuridica;
	}

	public void setFld_CalidadJuridica(String fld_CalidadJuridica) {
		this.fld_CalidadJuridica = fld_CalidadJuridica;
	}

	public String getFld_GlosaCalidadJuridica() {
		return this.fld_GlosaCalidadJuridica;
	}

	public void setFld_GlosaCalidadJuridica(String fld_GlosaCalidadJuridica) {
		this.fld_GlosaCalidadJuridica = fld_GlosaCalidadJuridica;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}