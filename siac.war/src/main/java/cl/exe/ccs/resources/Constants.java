package cl.exe.ccs.resources;

import java.util.Locale;
import java.util.Properties;


public class Constants {
    
    private final static PropertiesManager propManager = new PropertiesManager();
    private final static Properties prop = propManager.getPropertiesConstants();
    
    public final static Locale LOCALE = new Locale("es", "CL");

    // URL asignada desde init (web.xml)

    public final static String URL_PORTAL_CLIENTES = getProp("URL_PORTAL_CLIENTES");;  
    
    public final static String JNDI_ORACLE = getProp("JNDI_ORACLE");
    public final static String JNDI_SYBASE = getProp("JNDI_SYBASE");
    public final static String URL_SITIO_PUBLICO = getProp("URL_SITIO_PUBLICO");
    
    
    
    
    public final static String INVALID_SERVICE_CODE = getProp("INVALID_SERVICE_CODE");
    public final static String INVOICE_WS_URL = getProp("INVOICE_WS_URL");
    public final static String INVOICE_WS_USER = getProp("INVOICE_WS_USER");
    public final static String INVOICE_WS_PASSWD = getProp("INVOICE_WS_PASSWD");
    public final static String INVOICE_COST_CENTER_ACTECO = getProp("INVOICE_COST_CENTER_ACTECO");
    public final static String INVOICE_COST_CENTER_ACTIVITY = getProp("INVOICE_COST_CENTER_ACTIVITY");
    public final static String INVOICE_COST_CENTER_CITY = getProp("INVOICE_COST_CENTER_CITY");
    public final static String INVOICE_COST_CENTER_COMMUNE = getProp("INVOICE_COST_CENTER_COMMUNE");
    public final static String INVOICE_COST_CENTER_NAME = getProp("INVOICE_COST_CENTER_NAME");
    public final static String INVOICE_COST_CENTER_RUT = getProp("INVOICE_COST_CENTER_RUT");
    public final static String INVOICE_COST_CENTER_RUT_ONLY = getProp("INVOICE_COST_CENTER_RUT_ONLY");
    public final static String INVOICE_COST_CENTER_STREET_ADDRESS = getProp("INVOICE_COST_CENTER_STREET_ADDRESS");
    public final static String INVOICE_COST_CENTER_SYMBOL = getProp("INVOICE_COST_CENTER_SYMBOL");
    public final static String INVOICE_DOCUMENT_TYPE = getProp("INVOICE_DOCUMENT_TYPE");
    public final static String INVOICE_DOCUMENT_TYPE_INT = getProp("INVOICE_DOCUMENT_TYPE_INT");
    public final static String INVOICE_FILENAME_EXTENSION = getProp("INVOICE_FILENAME_EXTENSION");
    public final static String INVOICE_FILENAME_PATH = getProp("INVOICE_FILENAME_PATH");
    public final static String INVOICE_PAY_ACCOUNT_TYPE = getProp("INVOICE_PAY_ACCOUNT_TYPE");
    public final static String INVOICE_PAYMENT_METHOD = getProp("INVOICE_PAYMENT_METHOD");
    public final static String INVOICE_VERSION = getProp("INVOICE_VERSION");
    public final static String DIR_INVOICES = getProp("DIR_INVOICES");
    public final static String DIR_INVOICES_PDF = getProp("DIR_INVOICES_PDF");
    

    

    
    public final static String DIR_CERTIFICATES = getProp("DIR_CERTIFICATES");
    public final static String DIR_CERTIFICATES_TEMP = getProp("DIR_CERTIFICATES_TEMP");
    public final static String DIR_FIRMAS = getProp("DIR_FIRMAS");

        
    public Constants() {
        super();
    }
        
    public static String getProp(String key) {

        String value = prop.getProperty(key);
        if (null == value) {
            value = "";
        }
        return value;
    } 
}
