angular.module('ccs').factory('CentroCostoResource', function ($resource) {
    var resource = $resource('rest/centrocosto/',  { nocache : new Date().getTime()},
    {
        'modificar' :  { method : 'POST'  },
        'listar':  { method : 'GET'  , isArray: true, nocache : new Date().getTime()}
    });
    return resource;
});
