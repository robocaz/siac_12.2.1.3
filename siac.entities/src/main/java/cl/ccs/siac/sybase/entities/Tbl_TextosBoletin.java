package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TextosBoletin database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TextosBoletin.findAll", query="SELECT t FROM Tbl_TextosBoletin t")
public class Tbl_TextosBoletin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrBOL")
	private BigDecimal fld_CorrBOL;

	@Column(name="Fld_CorrOrdenStr")
	private BigDecimal fld_CorrOrdenStr;

	@Column(name="Fld_CorrOrdenStr_Id")
	private BigDecimal fld_CorrOrdenStr_Id;

	@Column(name="Fld_LargoStr")
	private short fld_LargoStr;

	@Column(name="Fld_PosNL")
	private short fld_PosNL;

	@Column(name="Fld_SubTextoBol")
	private String fld_SubTextoBol;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_TextosBoletin() {
	}

	public BigDecimal getFld_CorrBOL() {
		return this.fld_CorrBOL;
	}

	public void setFld_CorrBOL(BigDecimal fld_CorrBOL) {
		this.fld_CorrBOL = fld_CorrBOL;
	}

	public BigDecimal getFld_CorrOrdenStr() {
		return this.fld_CorrOrdenStr;
	}

	public void setFld_CorrOrdenStr(BigDecimal fld_CorrOrdenStr) {
		this.fld_CorrOrdenStr = fld_CorrOrdenStr;
	}

	public BigDecimal getFld_CorrOrdenStr_Id() {
		return this.fld_CorrOrdenStr_Id;
	}

	public void setFld_CorrOrdenStr_Id(BigDecimal fld_CorrOrdenStr_Id) {
		this.fld_CorrOrdenStr_Id = fld_CorrOrdenStr_Id;
	}

	public short getFld_LargoStr() {
		return this.fld_LargoStr;
	}

	public void setFld_LargoStr(short fld_LargoStr) {
		this.fld_LargoStr = fld_LargoStr;
	}

	public short getFld_PosNL() {
		return this.fld_PosNL;
	}

	public void setFld_PosNL(short fld_PosNL) {
		this.fld_PosNL = fld_PosNL;
	}

	public String getFld_SubTextoBol() {
		return this.fld_SubTextoBol;
	}

	public void setFld_SubTextoBol(String fld_SubTextoBol) {
		this.fld_SubTextoBol = fld_SubTextoBol;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}