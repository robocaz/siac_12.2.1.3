package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RATipoAfectado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RATipoAfectado.findAll", query="SELECT t FROM Tbl_RATipoAfectado t")
public class Tbl_RATipoAfectado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoAfectado")
	private String fld_GlosaTipoAfectado;

	@Column(name="Fld_TipoAfectado")
	private String fld_TipoAfectado;

	public Tbl_RATipoAfectado() {
	}

	public String getFld_GlosaTipoAfectado() {
		return this.fld_GlosaTipoAfectado;
	}

	public void setFld_GlosaTipoAfectado(String fld_GlosaTipoAfectado) {
		this.fld_GlosaTipoAfectado = fld_GlosaTipoAfectado;
	}

	public String getFld_TipoAfectado() {
		return this.fld_TipoAfectado;
	}

	public void setFld_TipoAfectado(String fld_TipoAfectado) {
		this.fld_TipoAfectado = fld_TipoAfectado;
	}

}