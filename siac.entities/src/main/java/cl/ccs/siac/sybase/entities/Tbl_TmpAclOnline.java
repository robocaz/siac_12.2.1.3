package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_TmpAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TmpAclOnline.findAll", query="SELECT t FROM Tbl_TmpAclOnline t")
public class Tbl_TmpAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodRechazo")
	private short fld_CodRechazo;

	@Column(name="Fld_CodUsuarioConfirm")
	private String fld_CodUsuarioConfirm;

	@Column(name="Fld_CodUsuarioIngreso")
	private String fld_CodUsuarioIngreso;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrAclOnline_Id")
	private BigDecimal fld_CorrAclOnline_Id;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecConfirm")
	private Timestamp fld_FecConfirm;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FlagProceso")
	private short fld_FlagProceso;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_TmpAclOnline() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodRechazo() {
		return this.fld_CodRechazo;
	}

	public void setFld_CodRechazo(short fld_CodRechazo) {
		this.fld_CodRechazo = fld_CodRechazo;
	}

	public String getFld_CodUsuarioConfirm() {
		return this.fld_CodUsuarioConfirm;
	}

	public void setFld_CodUsuarioConfirm(String fld_CodUsuarioConfirm) {
		this.fld_CodUsuarioConfirm = fld_CodUsuarioConfirm;
	}

	public String getFld_CodUsuarioIngreso() {
		return this.fld_CodUsuarioIngreso;
	}

	public void setFld_CodUsuarioIngreso(String fld_CodUsuarioIngreso) {
		this.fld_CodUsuarioIngreso = fld_CodUsuarioIngreso;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrAclOnline_Id() {
		return this.fld_CorrAclOnline_Id;
	}

	public void setFld_CorrAclOnline_Id(BigDecimal fld_CorrAclOnline_Id) {
		this.fld_CorrAclOnline_Id = fld_CorrAclOnline_Id;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecConfirm() {
		return this.fld_FecConfirm;
	}

	public void setFld_FecConfirm(Timestamp fld_FecConfirm) {
		this.fld_FecConfirm = fld_FecConfirm;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public short getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(short fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}