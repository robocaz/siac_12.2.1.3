package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BoletaDTO extends ResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6553856350779728298L;
	
	private Long corrCaja;
	private Long corrTransaccion;
	private Integer numeroBoleta;
	private Integer codCCosto;
	private String FecEmision;
	private String rutAfectado;
	private Long corrNombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String giro;
	private String direccion;
	private Integer codComuna;
	private Integer codRegion;
	private String telefono;
	private java.math.BigDecimal montoBruto;
	private java.math.BigDecimal montoIva;
	private java.math.BigDecimal montoNeto;
	private String tipoPago;
	private List<FacturaTransaccionDTO> transacciones;
	
	BoletaDTO(Long corrCaja, Integer numeroBoleta) {
		this.setCorrCaja(corrCaja);
		this.setNumeroBoleta(numeroBoleta);
	}

	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Long getCorrTransaccion() {
		return corrTransaccion;
	}
	public void setCorrTransaccion(Long corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}
	public Integer getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(Integer numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public Integer getCodCCosto() {
		return codCCosto;
	}
	public void setCodCCosto(Integer codCCosto) {
		this.codCCosto = codCCosto;
	}
	public String getFecEmision() {
		return FecEmision;
	}
	public void setFecEmision(String fecEmision) {
		FecEmision = fecEmision;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public Long getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Long corrNombre) {
		this.corrNombre = corrNombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getCodComuna() {
		return codComuna;
	}
	public void setCodComuna(Integer codComuna) {
		this.codComuna = codComuna;
	}
	public Integer getCodRegion() {
		return codRegion;
	}
	public void setCodRegion(Integer codRegion) {
		this.codRegion = codRegion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public java.math.BigDecimal getMontoBruto() {
		return montoBruto;
	}
	public void setMontoBruto(java.math.BigDecimal montoBruto) {
		this.montoBruto = montoBruto;
	}
	public java.math.BigDecimal getMontoIva() {
		return montoIva;
	}
	public void setMontoIva(java.math.BigDecimal montoIva) {
		this.montoIva = montoIva;
	}
	public java.math.BigDecimal getMontoNeto() {
		return montoNeto;
	}
	public void setMontoNeto(java.math.BigDecimal montoNeto) {
		this.montoNeto = montoNeto;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public List<FacturaTransaccionDTO> getTransacciones() {
		return transacciones;
	}
	public void setTransacciones(List<FacturaTransaccionDTO> transacciones) {
		this.transacciones = transacciones;
	}

}
