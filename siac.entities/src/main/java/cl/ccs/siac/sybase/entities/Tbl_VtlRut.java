package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_VtlRut database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VtlRut.findAll", query="SELECT t FROM Tbl_VtlRut t")
public class Tbl_VtlRut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_VtlRut() {
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}