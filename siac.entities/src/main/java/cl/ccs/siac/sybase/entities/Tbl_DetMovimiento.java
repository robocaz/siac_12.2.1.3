package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetMovimientos database table.
 * 
 */
@Entity
@Table(name="Tbl_DetMovimientos")
@NamedQuery(name="Tbl_DetMovimiento.findAll", query="SELECT t FROM Tbl_DetMovimiento t")
public class Tbl_DetMovimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrMovimiento_id")
	private BigDecimal fld_CorrMovimiento_id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_MontoMovimiento")
	private BigDecimal fld_MontoMovimiento;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TotFaltante")
	private BigDecimal fld_TotFaltante;

	@Column(name="Fld_TotSobrante")
	private BigDecimal fld_TotSobrante;

	public Tbl_DetMovimiento() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrMovimiento_id() {
		return this.fld_CorrMovimiento_id;
	}

	public void setFld_CorrMovimiento_id(BigDecimal fld_CorrMovimiento_id) {
		this.fld_CorrMovimiento_id = fld_CorrMovimiento_id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public BigDecimal getFld_MontoMovimiento() {
		return this.fld_MontoMovimiento;
	}

	public void setFld_MontoMovimiento(BigDecimal fld_MontoMovimiento) {
		this.fld_MontoMovimiento = fld_MontoMovimiento;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_TotFaltante() {
		return this.fld_TotFaltante;
	}

	public void setFld_TotFaltante(BigDecimal fld_TotFaltante) {
		this.fld_TotFaltante = fld_TotFaltante;
	}

	public BigDecimal getFld_TotSobrante() {
		return this.fld_TotSobrante;
	}

	public void setFld_TotSobrante(BigDecimal fld_TotSobrante) {
		this.fld_TotSobrante = fld_TotSobrante;
	}

}