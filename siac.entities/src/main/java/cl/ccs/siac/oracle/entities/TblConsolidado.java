package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONSOLIDADO database table.
 * 
 */
@Entity
@Table(name="TBL_CONSOLIDADO")
@NamedQuery(name="TblConsolidado.findAll", query="SELECT t FROM TblConsolidado t")
public class TblConsolidado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_DOCUMENTOS_0001")
	private BigDecimal fldDocumentos0001;

	@Column(name="FLD_DOCUMENTOS_0002")
	private BigDecimal fldDocumentos0002;

	@Column(name="FLD_DOCUMENTOS_0003")
	private BigDecimal fldDocumentos0003;

	@Column(name="FLD_DOCUMENTOS_0004")
	private BigDecimal fldDocumentos0004;

	@Column(name="FLD_DOCUMENTOS_0005")
	private BigDecimal fldDocumentos0005;

	@Column(name="FLD_DOCUMENTOS_0006")
	private BigDecimal fldDocumentos0006;

	@Column(name="FLD_DOCUMENTOS_0007")
	private BigDecimal fldDocumentos0007;

	@Column(name="FLD_DOCUMENTOS_0009")
	private BigDecimal fldDocumentos0009;

	@Column(name="FLD_DOCUMENTOS_0196")
	private BigDecimal fldDocumentos0196;

	@Column(name="FLD_DOCUMENTOS_0202")
	private BigDecimal fldDocumentos0202;

	@Column(name="FLD_DOCUMENTOS_0218")
	private BigDecimal fldDocumentos0218;

	@Column(name="FLD_DOCUMENTOS_0219")
	private BigDecimal fldDocumentos0219;

	@Column(name="FLD_DOCUMENTOS_0233")
	private BigDecimal fldDocumentos0233;

	@Column(name="FLD_DOCUMENTOS_0234")
	private BigDecimal fldDocumentos0234;

	@Column(name="FLD_DOCUMENTOS_0235")
	private BigDecimal fldDocumentos0235;

	@Column(name="FLD_DOCUMENTOS_0236")
	private BigDecimal fldDocumentos0236;

	@Column(name="FLD_DOCUMENTOS_0237")
	private BigDecimal fldDocumentos0237;

	@Column(name="FLD_DOCUMENTOS_0242")
	private BigDecimal fldDocumentos0242;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0001")
	private Date fldFecha0001;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0002")
	private Date fldFecha0002;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0003")
	private Date fldFecha0003;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0004")
	private Date fldFecha0004;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0005")
	private Date fldFecha0005;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0006")
	private Date fldFecha0006;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0007")
	private Date fldFecha0007;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0009")
	private Date fldFecha0009;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0196")
	private Date fldFecha0196;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0202")
	private Date fldFecha0202;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0218")
	private Date fldFecha0218;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0219")
	private Date fldFecha0219;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0233")
	private Date fldFecha0233;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0234")
	private Date fldFecha0234;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0235")
	private Date fldFecha0235;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0236")
	private Date fldFecha0236;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0237")
	private Date fldFecha0237;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHA_0242")
	private Date fldFecha0242;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAACTUALIZACION")
	private Date fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVENCIMIENTO")
	private Date fldFechavencimiento;

	@Column(name="FLD_MONEDA")
	private String fldMoneda;

	@Column(name="FLD_MONTO")
	private BigDecimal fldMonto;

	@Column(name="FLD_MONTO_0001")
	private BigDecimal fldMonto0001;

	@Column(name="FLD_MONTO_0002")
	private BigDecimal fldMonto0002;

	@Column(name="FLD_MONTO_0003")
	private BigDecimal fldMonto0003;

	@Column(name="FLD_MONTO_0004")
	private BigDecimal fldMonto0004;

	@Column(name="FLD_MONTO_0005")
	private BigDecimal fldMonto0005;

	@Column(name="FLD_MONTO_0006")
	private BigDecimal fldMonto0006;

	@Column(name="FLD_MONTO_0007")
	private BigDecimal fldMonto0007;

	@Column(name="FLD_MONTO_0009")
	private BigDecimal fldMonto0009;

	@Column(name="FLD_MONTO_0196")
	private BigDecimal fldMonto0196;

	@Column(name="FLD_MONTO_0202")
	private BigDecimal fldMonto0202;

	@Column(name="FLD_MONTO_0218")
	private BigDecimal fldMonto0218;

	@Column(name="FLD_MONTO_0219")
	private BigDecimal fldMonto0219;

	@Column(name="FLD_MONTO_0233")
	private BigDecimal fldMonto0233;

	@Column(name="FLD_MONTO_0234")
	private BigDecimal fldMonto0234;

	@Column(name="FLD_MONTO_0235")
	private BigDecimal fldMonto0235;

	@Column(name="FLD_MONTO_0236")
	private BigDecimal fldMonto0236;

	@Column(name="FLD_MONTO_0237")
	private BigDecimal fldMonto0237;

	@Column(name="FLD_MONTO_0242")
	private BigDecimal fldMonto0242;

	@Column(name="FLD_NOMBREDEUDOR")
	private String fldNombredeudor;

	@Column(name="FLD_RUTDEUDOR")
	private String fldRutdeudor;

	@Column(name="FLD_TOTALDOCUMENTOS")
	private BigDecimal fldTotaldocumentos;

	public TblConsolidado() {
	}

	public BigDecimal getFldDocumentos0001() {
		return this.fldDocumentos0001;
	}

	public void setFldDocumentos0001(BigDecimal fldDocumentos0001) {
		this.fldDocumentos0001 = fldDocumentos0001;
	}

	public BigDecimal getFldDocumentos0002() {
		return this.fldDocumentos0002;
	}

	public void setFldDocumentos0002(BigDecimal fldDocumentos0002) {
		this.fldDocumentos0002 = fldDocumentos0002;
	}

	public BigDecimal getFldDocumentos0003() {
		return this.fldDocumentos0003;
	}

	public void setFldDocumentos0003(BigDecimal fldDocumentos0003) {
		this.fldDocumentos0003 = fldDocumentos0003;
	}

	public BigDecimal getFldDocumentos0004() {
		return this.fldDocumentos0004;
	}

	public void setFldDocumentos0004(BigDecimal fldDocumentos0004) {
		this.fldDocumentos0004 = fldDocumentos0004;
	}

	public BigDecimal getFldDocumentos0005() {
		return this.fldDocumentos0005;
	}

	public void setFldDocumentos0005(BigDecimal fldDocumentos0005) {
		this.fldDocumentos0005 = fldDocumentos0005;
	}

	public BigDecimal getFldDocumentos0006() {
		return this.fldDocumentos0006;
	}

	public void setFldDocumentos0006(BigDecimal fldDocumentos0006) {
		this.fldDocumentos0006 = fldDocumentos0006;
	}

	public BigDecimal getFldDocumentos0007() {
		return this.fldDocumentos0007;
	}

	public void setFldDocumentos0007(BigDecimal fldDocumentos0007) {
		this.fldDocumentos0007 = fldDocumentos0007;
	}

	public BigDecimal getFldDocumentos0009() {
		return this.fldDocumentos0009;
	}

	public void setFldDocumentos0009(BigDecimal fldDocumentos0009) {
		this.fldDocumentos0009 = fldDocumentos0009;
	}

	public BigDecimal getFldDocumentos0196() {
		return this.fldDocumentos0196;
	}

	public void setFldDocumentos0196(BigDecimal fldDocumentos0196) {
		this.fldDocumentos0196 = fldDocumentos0196;
	}

	public BigDecimal getFldDocumentos0202() {
		return this.fldDocumentos0202;
	}

	public void setFldDocumentos0202(BigDecimal fldDocumentos0202) {
		this.fldDocumentos0202 = fldDocumentos0202;
	}

	public BigDecimal getFldDocumentos0218() {
		return this.fldDocumentos0218;
	}

	public void setFldDocumentos0218(BigDecimal fldDocumentos0218) {
		this.fldDocumentos0218 = fldDocumentos0218;
	}

	public BigDecimal getFldDocumentos0219() {
		return this.fldDocumentos0219;
	}

	public void setFldDocumentos0219(BigDecimal fldDocumentos0219) {
		this.fldDocumentos0219 = fldDocumentos0219;
	}

	public BigDecimal getFldDocumentos0233() {
		return this.fldDocumentos0233;
	}

	public void setFldDocumentos0233(BigDecimal fldDocumentos0233) {
		this.fldDocumentos0233 = fldDocumentos0233;
	}

	public BigDecimal getFldDocumentos0234() {
		return this.fldDocumentos0234;
	}

	public void setFldDocumentos0234(BigDecimal fldDocumentos0234) {
		this.fldDocumentos0234 = fldDocumentos0234;
	}

	public BigDecimal getFldDocumentos0235() {
		return this.fldDocumentos0235;
	}

	public void setFldDocumentos0235(BigDecimal fldDocumentos0235) {
		this.fldDocumentos0235 = fldDocumentos0235;
	}

	public BigDecimal getFldDocumentos0236() {
		return this.fldDocumentos0236;
	}

	public void setFldDocumentos0236(BigDecimal fldDocumentos0236) {
		this.fldDocumentos0236 = fldDocumentos0236;
	}

	public BigDecimal getFldDocumentos0237() {
		return this.fldDocumentos0237;
	}

	public void setFldDocumentos0237(BigDecimal fldDocumentos0237) {
		this.fldDocumentos0237 = fldDocumentos0237;
	}

	public BigDecimal getFldDocumentos0242() {
		return this.fldDocumentos0242;
	}

	public void setFldDocumentos0242(BigDecimal fldDocumentos0242) {
		this.fldDocumentos0242 = fldDocumentos0242;
	}

	public Date getFldFecha0001() {
		return this.fldFecha0001;
	}

	public void setFldFecha0001(Date fldFecha0001) {
		this.fldFecha0001 = fldFecha0001;
	}

	public Date getFldFecha0002() {
		return this.fldFecha0002;
	}

	public void setFldFecha0002(Date fldFecha0002) {
		this.fldFecha0002 = fldFecha0002;
	}

	public Date getFldFecha0003() {
		return this.fldFecha0003;
	}

	public void setFldFecha0003(Date fldFecha0003) {
		this.fldFecha0003 = fldFecha0003;
	}

	public Date getFldFecha0004() {
		return this.fldFecha0004;
	}

	public void setFldFecha0004(Date fldFecha0004) {
		this.fldFecha0004 = fldFecha0004;
	}

	public Date getFldFecha0005() {
		return this.fldFecha0005;
	}

	public void setFldFecha0005(Date fldFecha0005) {
		this.fldFecha0005 = fldFecha0005;
	}

	public Date getFldFecha0006() {
		return this.fldFecha0006;
	}

	public void setFldFecha0006(Date fldFecha0006) {
		this.fldFecha0006 = fldFecha0006;
	}

	public Date getFldFecha0007() {
		return this.fldFecha0007;
	}

	public void setFldFecha0007(Date fldFecha0007) {
		this.fldFecha0007 = fldFecha0007;
	}

	public Date getFldFecha0009() {
		return this.fldFecha0009;
	}

	public void setFldFecha0009(Date fldFecha0009) {
		this.fldFecha0009 = fldFecha0009;
	}

	public Date getFldFecha0196() {
		return this.fldFecha0196;
	}

	public void setFldFecha0196(Date fldFecha0196) {
		this.fldFecha0196 = fldFecha0196;
	}

	public Date getFldFecha0202() {
		return this.fldFecha0202;
	}

	public void setFldFecha0202(Date fldFecha0202) {
		this.fldFecha0202 = fldFecha0202;
	}

	public Date getFldFecha0218() {
		return this.fldFecha0218;
	}

	public void setFldFecha0218(Date fldFecha0218) {
		this.fldFecha0218 = fldFecha0218;
	}

	public Date getFldFecha0219() {
		return this.fldFecha0219;
	}

	public void setFldFecha0219(Date fldFecha0219) {
		this.fldFecha0219 = fldFecha0219;
	}

	public Date getFldFecha0233() {
		return this.fldFecha0233;
	}

	public void setFldFecha0233(Date fldFecha0233) {
		this.fldFecha0233 = fldFecha0233;
	}

	public Date getFldFecha0234() {
		return this.fldFecha0234;
	}

	public void setFldFecha0234(Date fldFecha0234) {
		this.fldFecha0234 = fldFecha0234;
	}

	public Date getFldFecha0235() {
		return this.fldFecha0235;
	}

	public void setFldFecha0235(Date fldFecha0235) {
		this.fldFecha0235 = fldFecha0235;
	}

	public Date getFldFecha0236() {
		return this.fldFecha0236;
	}

	public void setFldFecha0236(Date fldFecha0236) {
		this.fldFecha0236 = fldFecha0236;
	}

	public Date getFldFecha0237() {
		return this.fldFecha0237;
	}

	public void setFldFecha0237(Date fldFecha0237) {
		this.fldFecha0237 = fldFecha0237;
	}

	public Date getFldFecha0242() {
		return this.fldFecha0242;
	}

	public void setFldFecha0242(Date fldFecha0242) {
		this.fldFecha0242 = fldFecha0242;
	}

	public Date getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Date fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechavencimiento() {
		return this.fldFechavencimiento;
	}

	public void setFldFechavencimiento(Date fldFechavencimiento) {
		this.fldFechavencimiento = fldFechavencimiento;
	}

	public String getFldMoneda() {
		return this.fldMoneda;
	}

	public void setFldMoneda(String fldMoneda) {
		this.fldMoneda = fldMoneda;
	}

	public BigDecimal getFldMonto() {
		return this.fldMonto;
	}

	public void setFldMonto(BigDecimal fldMonto) {
		this.fldMonto = fldMonto;
	}

	public BigDecimal getFldMonto0001() {
		return this.fldMonto0001;
	}

	public void setFldMonto0001(BigDecimal fldMonto0001) {
		this.fldMonto0001 = fldMonto0001;
	}

	public BigDecimal getFldMonto0002() {
		return this.fldMonto0002;
	}

	public void setFldMonto0002(BigDecimal fldMonto0002) {
		this.fldMonto0002 = fldMonto0002;
	}

	public BigDecimal getFldMonto0003() {
		return this.fldMonto0003;
	}

	public void setFldMonto0003(BigDecimal fldMonto0003) {
		this.fldMonto0003 = fldMonto0003;
	}

	public BigDecimal getFldMonto0004() {
		return this.fldMonto0004;
	}

	public void setFldMonto0004(BigDecimal fldMonto0004) {
		this.fldMonto0004 = fldMonto0004;
	}

	public BigDecimal getFldMonto0005() {
		return this.fldMonto0005;
	}

	public void setFldMonto0005(BigDecimal fldMonto0005) {
		this.fldMonto0005 = fldMonto0005;
	}

	public BigDecimal getFldMonto0006() {
		return this.fldMonto0006;
	}

	public void setFldMonto0006(BigDecimal fldMonto0006) {
		this.fldMonto0006 = fldMonto0006;
	}

	public BigDecimal getFldMonto0007() {
		return this.fldMonto0007;
	}

	public void setFldMonto0007(BigDecimal fldMonto0007) {
		this.fldMonto0007 = fldMonto0007;
	}

	public BigDecimal getFldMonto0009() {
		return this.fldMonto0009;
	}

	public void setFldMonto0009(BigDecimal fldMonto0009) {
		this.fldMonto0009 = fldMonto0009;
	}

	public BigDecimal getFldMonto0196() {
		return this.fldMonto0196;
	}

	public void setFldMonto0196(BigDecimal fldMonto0196) {
		this.fldMonto0196 = fldMonto0196;
	}

	public BigDecimal getFldMonto0202() {
		return this.fldMonto0202;
	}

	public void setFldMonto0202(BigDecimal fldMonto0202) {
		this.fldMonto0202 = fldMonto0202;
	}

	public BigDecimal getFldMonto0218() {
		return this.fldMonto0218;
	}

	public void setFldMonto0218(BigDecimal fldMonto0218) {
		this.fldMonto0218 = fldMonto0218;
	}

	public BigDecimal getFldMonto0219() {
		return this.fldMonto0219;
	}

	public void setFldMonto0219(BigDecimal fldMonto0219) {
		this.fldMonto0219 = fldMonto0219;
	}

	public BigDecimal getFldMonto0233() {
		return this.fldMonto0233;
	}

	public void setFldMonto0233(BigDecimal fldMonto0233) {
		this.fldMonto0233 = fldMonto0233;
	}

	public BigDecimal getFldMonto0234() {
		return this.fldMonto0234;
	}

	public void setFldMonto0234(BigDecimal fldMonto0234) {
		this.fldMonto0234 = fldMonto0234;
	}

	public BigDecimal getFldMonto0235() {
		return this.fldMonto0235;
	}

	public void setFldMonto0235(BigDecimal fldMonto0235) {
		this.fldMonto0235 = fldMonto0235;
	}

	public BigDecimal getFldMonto0236() {
		return this.fldMonto0236;
	}

	public void setFldMonto0236(BigDecimal fldMonto0236) {
		this.fldMonto0236 = fldMonto0236;
	}

	public BigDecimal getFldMonto0237() {
		return this.fldMonto0237;
	}

	public void setFldMonto0237(BigDecimal fldMonto0237) {
		this.fldMonto0237 = fldMonto0237;
	}

	public BigDecimal getFldMonto0242() {
		return this.fldMonto0242;
	}

	public void setFldMonto0242(BigDecimal fldMonto0242) {
		this.fldMonto0242 = fldMonto0242;
	}

	public String getFldNombredeudor() {
		return this.fldNombredeudor;
	}

	public void setFldNombredeudor(String fldNombredeudor) {
		this.fldNombredeudor = fldNombredeudor;
	}

	public String getFldRutdeudor() {
		return this.fldRutdeudor;
	}

	public void setFldRutdeudor(String fldRutdeudor) {
		this.fldRutdeudor = fldRutdeudor;
	}

	public BigDecimal getFldTotaldocumentos() {
		return this.fldTotaldocumentos;
	}

	public void setFldTotaldocumentos(BigDecimal fldTotaldocumentos) {
		this.fldTotaldocumentos = fldTotaldocumentos;
	}

}