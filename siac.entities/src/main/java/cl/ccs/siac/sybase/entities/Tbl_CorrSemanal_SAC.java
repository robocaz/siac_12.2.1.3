package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CorrSemanal_SAC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CorrSemanal_SAC.findAll", query="SELECT t FROM Tbl_CorrSemanal_SAC t")
public class Tbl_CorrSemanal_SAC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecCambio")
	private Timestamp fld_FecCambio;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FlagProceso")
	private int fld_FlagProceso;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoReg")
	private String fld_TipoReg;

	public Tbl_CorrSemanal_SAC() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecCambio() {
		return this.fld_FecCambio;
	}

	public void setFld_FecCambio(Timestamp fld_FecCambio) {
		this.fld_FecCambio = fld_FecCambio;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public int getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(int fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoReg() {
		return this.fld_TipoReg;
	}

	public void setFld_TipoReg(String fld_TipoReg) {
		this.fld_TipoReg = fld_TipoReg;
	}

}