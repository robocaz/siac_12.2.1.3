package cl.exe.ccs.dto.certificados;

import java.io.Serializable;

public class CertificadoICTmcDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3953886250327307862L;

	private String fechaVencimiento;
	private String tipoCredito;
	private String moneda;
	private String monto;
	private String emisor;
	
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getTipoCredito() {
		return tipoCredito;
	}
	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
}
