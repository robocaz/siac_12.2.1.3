package cl.ccs.siac.depositos;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.DepositosDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.DepositoDTO;
import cl.exe.ccs.dto.ResponseDTO;

@Named("depositosBusiness")
public class DepositosBusinessImpl implements DepositosBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private DepositosDao depositosDao;
	
	@Override
	public List<DepositoDTO> consultaDepositos(Integer codCC, String desde, String hasta) {
		
		desde = utils.addTimeStrFormat(desde);
		hasta = utils.addTimeStrFormat(hasta);
		
		List<DepositoDTO> lista = depositosDao.consultaDepositos(codCC, desde, hasta);
		
		for ( DepositoDTO depo : lista ) {
			depo.setFecDepositoAux(utils.checkTimestampToStrFechaVista(depo.getFecDepositoAux()));
			depo.setFecRegDeposito(utils.checkTimestampToStrFechaVista(depo.getFecRegDeposito()));
			depo.setFecCierreFinal(utils.checkTimestampToStrFechaVista(depo.getFecCierreFinal()));
		}
		
		return lista;
	}
	
	@Override
	public ResponseDTO guardaDeposito(DepositoDTO depo) {
		
		depo.setFecRegDeposito(utils.addTimeStrFormat(depo.getFecRegDeposito()));
		depo.setRutPersona(utils.formatZeroRut(depo.getRutPersona()));
		
		return depositosDao.guardaDeposito(depo);
	}
	
	@Override
	public ResponseDTO cerrarDeposito(BigDecimal corrCtaId) {
		
		return depositosDao.cerrarDeposito(corrCtaId);
	}

}
