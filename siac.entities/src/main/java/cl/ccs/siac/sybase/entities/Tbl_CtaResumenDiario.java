package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtaResumenDiario database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CtaResumenDiario.findAll", query="SELECT t FROM Tbl_CtaResumenDiario t")
public class Tbl_CtaResumenDiario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodBanco")
	private String fld_CodBanco;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrCuenta_Id")
	private BigDecimal fld_CorrCuenta_Id;

	@Column(name="Fld_FecCierreFinal")
	private Timestamp fld_FecCierreFinal;

	@Column(name="Fld_FecContableFinal")
	private Timestamp fld_FecContableFinal;

	@Column(name="Fld_FecContableTemp")
	private Timestamp fld_FecContableTemp;

	@Column(name="Fld_FecDeposito")
	private Timestamp fld_FecDeposito;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecRegDeposito")
	private Timestamp fld_FecRegDeposito;

	@Column(name="Fld_FlagCierreFinal")
	private boolean fld_FlagCierreFinal;

	@Column(name="Fld_FlagContableFinal")
	private boolean fld_FlagContableFinal;

	@Column(name="Fld_FlagContableTemp")
	private boolean fld_FlagContableTemp;

	@Column(name="Fld_FlagRegistroValido")
	private boolean fld_FlagRegistroValido;

	@Column(name="Fld_MontoDeposito")
	private BigDecimal fld_MontoDeposito;

	@Column(name="Fld_MontoPorComision")
	private BigDecimal fld_MontoPorComision;

	@Column(name="Fld_MontoRebaja")
	private BigDecimal fld_MontoRebaja;

	@Column(name="Fld_MontoVentaBruta")
	private BigDecimal fld_MontoVentaBruta;

	@Column(name="Fld_NroCtaCte")
	private String fld_NroCtaCte;

	@Column(name="Fld_NroDeposito")
	private String fld_NroDeposito;

	@Column(name="Fld_RutDeposito")
	private String fld_RutDeposito;

	@Column(name="Fld_TipoPago")
	private String fld_TipoPago;

	@Column(name="Fld_TipoRegistro")
	private String fld_TipoRegistro;

	public Tbl_CtaResumenDiario() {
	}

	public String getFld_CodBanco() {
		return this.fld_CodBanco;
	}

	public void setFld_CodBanco(String fld_CodBanco) {
		this.fld_CodBanco = fld_CodBanco;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrCuenta_Id() {
		return this.fld_CorrCuenta_Id;
	}

	public void setFld_CorrCuenta_Id(BigDecimal fld_CorrCuenta_Id) {
		this.fld_CorrCuenta_Id = fld_CorrCuenta_Id;
	}

	public Timestamp getFld_FecCierreFinal() {
		return this.fld_FecCierreFinal;
	}

	public void setFld_FecCierreFinal(Timestamp fld_FecCierreFinal) {
		this.fld_FecCierreFinal = fld_FecCierreFinal;
	}

	public Timestamp getFld_FecContableFinal() {
		return this.fld_FecContableFinal;
	}

	public void setFld_FecContableFinal(Timestamp fld_FecContableFinal) {
		this.fld_FecContableFinal = fld_FecContableFinal;
	}

	public Timestamp getFld_FecContableTemp() {
		return this.fld_FecContableTemp;
	}

	public void setFld_FecContableTemp(Timestamp fld_FecContableTemp) {
		this.fld_FecContableTemp = fld_FecContableTemp;
	}

	public Timestamp getFld_FecDeposito() {
		return this.fld_FecDeposito;
	}

	public void setFld_FecDeposito(Timestamp fld_FecDeposito) {
		this.fld_FecDeposito = fld_FecDeposito;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecRegDeposito() {
		return this.fld_FecRegDeposito;
	}

	public void setFld_FecRegDeposito(Timestamp fld_FecRegDeposito) {
		this.fld_FecRegDeposito = fld_FecRegDeposito;
	}

	public boolean getFld_FlagCierreFinal() {
		return this.fld_FlagCierreFinal;
	}

	public void setFld_FlagCierreFinal(boolean fld_FlagCierreFinal) {
		this.fld_FlagCierreFinal = fld_FlagCierreFinal;
	}

	public boolean getFld_FlagContableFinal() {
		return this.fld_FlagContableFinal;
	}

	public void setFld_FlagContableFinal(boolean fld_FlagContableFinal) {
		this.fld_FlagContableFinal = fld_FlagContableFinal;
	}

	public boolean getFld_FlagContableTemp() {
		return this.fld_FlagContableTemp;
	}

	public void setFld_FlagContableTemp(boolean fld_FlagContableTemp) {
		this.fld_FlagContableTemp = fld_FlagContableTemp;
	}

	public boolean getFld_FlagRegistroValido() {
		return this.fld_FlagRegistroValido;
	}

	public void setFld_FlagRegistroValido(boolean fld_FlagRegistroValido) {
		this.fld_FlagRegistroValido = fld_FlagRegistroValido;
	}

	public BigDecimal getFld_MontoDeposito() {
		return this.fld_MontoDeposito;
	}

	public void setFld_MontoDeposito(BigDecimal fld_MontoDeposito) {
		this.fld_MontoDeposito = fld_MontoDeposito;
	}

	public BigDecimal getFld_MontoPorComision() {
		return this.fld_MontoPorComision;
	}

	public void setFld_MontoPorComision(BigDecimal fld_MontoPorComision) {
		this.fld_MontoPorComision = fld_MontoPorComision;
	}

	public BigDecimal getFld_MontoRebaja() {
		return this.fld_MontoRebaja;
	}

	public void setFld_MontoRebaja(BigDecimal fld_MontoRebaja) {
		this.fld_MontoRebaja = fld_MontoRebaja;
	}

	public BigDecimal getFld_MontoVentaBruta() {
		return this.fld_MontoVentaBruta;
	}

	public void setFld_MontoVentaBruta(BigDecimal fld_MontoVentaBruta) {
		this.fld_MontoVentaBruta = fld_MontoVentaBruta;
	}

	public String getFld_NroCtaCte() {
		return this.fld_NroCtaCte;
	}

	public void setFld_NroCtaCte(String fld_NroCtaCte) {
		this.fld_NroCtaCte = fld_NroCtaCte;
	}

	public String getFld_NroDeposito() {
		return this.fld_NroDeposito;
	}

	public void setFld_NroDeposito(String fld_NroDeposito) {
		this.fld_NroDeposito = fld_NroDeposito;
	}

	public String getFld_RutDeposito() {
		return this.fld_RutDeposito;
	}

	public void setFld_RutDeposito(String fld_RutDeposito) {
		this.fld_RutDeposito = fld_RutDeposito;
	}

	public String getFld_TipoPago() {
		return this.fld_TipoPago;
	}

	public void setFld_TipoPago(String fld_TipoPago) {
		this.fld_TipoPago = fld_TipoPago;
	}

	public String getFld_TipoRegistro() {
		return this.fld_TipoRegistro;
	}

	public void setFld_TipoRegistro(String fld_TipoRegistro) {
		this.fld_TipoRegistro = fld_TipoRegistro;
	}

}