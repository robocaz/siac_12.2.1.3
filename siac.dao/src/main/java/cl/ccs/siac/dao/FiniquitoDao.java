package cl.ccs.siac.dao;

import cl.exe.ccs.dto.FiniquitoDTO;

public interface FiniquitoDao {

	FiniquitoDTO consultaFiniquito(String rut);
	
	FiniquitoDTO guardaFiniquito(FiniquitoDTO finiquito);
	
}
