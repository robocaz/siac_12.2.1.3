package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetAutenticacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DetAutenticacion.findAll", query="SELECT t FROM Tbl_DetAutenticacion t")
public class Tbl_DetAutenticacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Ciudad")
	private String fld_Ciudad;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CorrAutenticacion")
	private BigDecimal fld_CorrAutenticacion;

	@Column(name="Fld_FecAclaracion")
	private Timestamp fld_FecAclaracion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecPubAcl")
	private Timestamp fld_FecPubAcl;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_GlosaMoneda")
	private String fld_GlosaMoneda;

	@Column(name="Fld_GlosaTipoDocumentoImpago")
	private String fld_GlosaTipoDocumentoImpago;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_NroBoletinProt")
	private short fld_NroBoletinProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_PagBoletinAcl")
	private short fld_PagBoletinAcl;

	@Column(name="Fld_PagBoletinProt")
	private short fld_PagBoletinProt;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	public Tbl_DetAutenticacion() {
	}

	public String getFld_Ciudad() {
		return this.fld_Ciudad;
	}

	public void setFld_Ciudad(String fld_Ciudad) {
		this.fld_Ciudad = fld_Ciudad;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CorrAutenticacion() {
		return this.fld_CorrAutenticacion;
	}

	public void setFld_CorrAutenticacion(BigDecimal fld_CorrAutenticacion) {
		this.fld_CorrAutenticacion = fld_CorrAutenticacion;
	}

	public Timestamp getFld_FecAclaracion() {
		return this.fld_FecAclaracion;
	}

	public void setFld_FecAclaracion(Timestamp fld_FecAclaracion) {
		this.fld_FecAclaracion = fld_FecAclaracion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecPubAcl() {
		return this.fld_FecPubAcl;
	}

	public void setFld_FecPubAcl(Timestamp fld_FecPubAcl) {
		this.fld_FecPubAcl = fld_FecPubAcl;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public String getFld_GlosaMoneda() {
		return this.fld_GlosaMoneda;
	}

	public void setFld_GlosaMoneda(String fld_GlosaMoneda) {
		this.fld_GlosaMoneda = fld_GlosaMoneda;
	}

	public String getFld_GlosaTipoDocumentoImpago() {
		return this.fld_GlosaTipoDocumentoImpago;
	}

	public void setFld_GlosaTipoDocumentoImpago(String fld_GlosaTipoDocumentoImpago) {
		this.fld_GlosaTipoDocumentoImpago = fld_GlosaTipoDocumentoImpago;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public short getFld_NroBoletinProt() {
		return this.fld_NroBoletinProt;
	}

	public void setFld_NroBoletinProt(short fld_NroBoletinProt) {
		this.fld_NroBoletinProt = fld_NroBoletinProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public short getFld_PagBoletinAcl() {
		return this.fld_PagBoletinAcl;
	}

	public void setFld_PagBoletinAcl(short fld_PagBoletinAcl) {
		this.fld_PagBoletinAcl = fld_PagBoletinAcl;
	}

	public short getFld_PagBoletinProt() {
		return this.fld_PagBoletinProt;
	}

	public void setFld_PagBoletinProt(short fld_PagBoletinProt) {
		this.fld_PagBoletinProt = fld_PagBoletinProt;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

}