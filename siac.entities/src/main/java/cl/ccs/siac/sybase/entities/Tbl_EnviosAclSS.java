package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EnviosAclSS database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosAclSS.findAll", query="SELECT t FROM Tbl_EnviosAclSS t")
public class Tbl_EnviosAclSS implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrEnvioAcl_Id")
	private BigDecimal fld_CorrEnvioAcl_Id;

	@Column(name="Fld_FecInicioCaja")
	private Timestamp fld_FecInicioCaja;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_Flag_5")
	private boolean fld_Flag_5;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TotRegistrosEliminados")
	private int fld_TotRegistrosEliminados;

	@Column(name="Fld_TotRegistrosEnvio")
	private int fld_TotRegistrosEnvio;

	@Column(name="Fld_TotRegistrosOk")
	private int fld_TotRegistrosOk;

	@Column(name="Fld_TotRegistrosPendientes")
	private int fld_TotRegistrosPendientes;

	@Column(name="Fld_TotRegPareoNombre")
	private int fld_TotRegPareoNombre;

	@Column(name="Fld_TotRegPareoRut")
	private int fld_TotRegPareoRut;

	public Tbl_EnviosAclSS() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrEnvioAcl_Id() {
		return this.fld_CorrEnvioAcl_Id;
	}

	public void setFld_CorrEnvioAcl_Id(BigDecimal fld_CorrEnvioAcl_Id) {
		this.fld_CorrEnvioAcl_Id = fld_CorrEnvioAcl_Id;
	}

	public Timestamp getFld_FecInicioCaja() {
		return this.fld_FecInicioCaja;
	}

	public void setFld_FecInicioCaja(Timestamp fld_FecInicioCaja) {
		this.fld_FecInicioCaja = fld_FecInicioCaja;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_Flag_5() {
		return this.fld_Flag_5;
	}

	public void setFld_Flag_5(boolean fld_Flag_5) {
		this.fld_Flag_5 = fld_Flag_5;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public int getFld_TotRegistrosEliminados() {
		return this.fld_TotRegistrosEliminados;
	}

	public void setFld_TotRegistrosEliminados(int fld_TotRegistrosEliminados) {
		this.fld_TotRegistrosEliminados = fld_TotRegistrosEliminados;
	}

	public int getFld_TotRegistrosEnvio() {
		return this.fld_TotRegistrosEnvio;
	}

	public void setFld_TotRegistrosEnvio(int fld_TotRegistrosEnvio) {
		this.fld_TotRegistrosEnvio = fld_TotRegistrosEnvio;
	}

	public int getFld_TotRegistrosOk() {
		return this.fld_TotRegistrosOk;
	}

	public void setFld_TotRegistrosOk(int fld_TotRegistrosOk) {
		this.fld_TotRegistrosOk = fld_TotRegistrosOk;
	}

	public int getFld_TotRegistrosPendientes() {
		return this.fld_TotRegistrosPendientes;
	}

	public void setFld_TotRegistrosPendientes(int fld_TotRegistrosPendientes) {
		this.fld_TotRegistrosPendientes = fld_TotRegistrosPendientes;
	}

	public int getFld_TotRegPareoNombre() {
		return this.fld_TotRegPareoNombre;
	}

	public void setFld_TotRegPareoNombre(int fld_TotRegPareoNombre) {
		this.fld_TotRegPareoNombre = fld_TotRegPareoNombre;
	}

	public int getFld_TotRegPareoRut() {
		return this.fld_TotRegPareoRut;
	}

	public void setFld_TotRegPareoRut(int fld_TotRegPareoRut) {
		this.fld_TotRegPareoRut = fld_TotRegPareoRut;
	}

}