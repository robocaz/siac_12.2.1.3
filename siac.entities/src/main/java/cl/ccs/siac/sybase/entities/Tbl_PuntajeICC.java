package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_PuntajeICC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PuntajeICC.findAll", query="SELECT t FROM Tbl_PuntajeICC t")
public class Tbl_PuntajeICC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalificacionICC")
	private byte fld_CalificacionICC;

	@Column(name="Fld_PjeBIC")
	private byte fld_PjeBIC;

	@Column(name="Fld_PjeMOL")
	private byte fld_PjeMOL;

	public Tbl_PuntajeICC() {
	}

	public byte getFld_CalificacionICC() {
		return this.fld_CalificacionICC;
	}

	public void setFld_CalificacionICC(byte fld_CalificacionICC) {
		this.fld_CalificacionICC = fld_CalificacionICC;
	}

	public byte getFld_PjeBIC() {
		return this.fld_PjeBIC;
	}

	public void setFld_PjeBIC(byte fld_PjeBIC) {
		this.fld_PjeBIC = fld_PjeBIC;
	}

	public byte getFld_PjeMOL() {
		return this.fld_PjeMOL;
	}

	public void setFld_PjeMOL(byte fld_PjeMOL) {
		this.fld_PjeMOL = fld_PjeMOL;
	}

}