package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysxtypes database table.
 * 
 */
@Entity
@Table(name="sysxtypes")
@NamedQuery(name="Sysxtype.findAll", query="SELECT s FROM Sysxtype s")
public class Sysxtype implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] xtbinaryinrow;

	@Lob
	private byte[] xtbinaryoffrow;

	private int xtcontainer;

	private int xtid;

	private int xtmetatype;

	private String xtname;

	@Lob
	private String xtsource;

	private int xtstatus;

	private short xtutid;

	public Sysxtype() {
	}

	public byte[] getXtbinaryinrow() {
		return this.xtbinaryinrow;
	}

	public void setXtbinaryinrow(byte[] xtbinaryinrow) {
		this.xtbinaryinrow = xtbinaryinrow;
	}

	public byte[] getXtbinaryoffrow() {
		return this.xtbinaryoffrow;
	}

	public void setXtbinaryoffrow(byte[] xtbinaryoffrow) {
		this.xtbinaryoffrow = xtbinaryoffrow;
	}

	public int getXtcontainer() {
		return this.xtcontainer;
	}

	public void setXtcontainer(int xtcontainer) {
		this.xtcontainer = xtcontainer;
	}

	public int getXtid() {
		return this.xtid;
	}

	public void setXtid(int xtid) {
		this.xtid = xtid;
	}

	public int getXtmetatype() {
		return this.xtmetatype;
	}

	public void setXtmetatype(int xtmetatype) {
		this.xtmetatype = xtmetatype;
	}

	public String getXtname() {
		return this.xtname;
	}

	public void setXtname(String xtname) {
		this.xtname = xtname;
	}

	public String getXtsource() {
		return this.xtsource;
	}

	public void setXtsource(String xtsource) {
		this.xtsource = xtsource;
	}

	public int getXtstatus() {
		return this.xtstatus;
	}

	public void setXtstatus(int xtstatus) {
		this.xtstatus = xtstatus;
	}

	public short getXtutid() {
		return this.xtutid;
	}

	public void setXtutid(short xtutid) {
		this.xtutid = xtutid;
	}

}