package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SegtoAclConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SegtoAclConsulta.findAll", query="SELECT t FROM Tbl_SegtoAclConsulta t")
public class Tbl_SegtoAclConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CorrAconsulta")
	private BigDecimal fld_CorrAconsulta;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	public Tbl_SegtoAclConsulta() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public BigDecimal getFld_CorrAconsulta() {
		return this.fld_CorrAconsulta;
	}

	public void setFld_CorrAconsulta(BigDecimal fld_CorrAconsulta) {
		this.fld_CorrAconsulta = fld_CorrAconsulta;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

}