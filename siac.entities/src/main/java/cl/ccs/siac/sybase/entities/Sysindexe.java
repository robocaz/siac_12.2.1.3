package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sysindexes database table.
 * 
 */
@Entity
@Table(name="sysindexes")
@NamedQuery(name="Sysindexe.findAll", query="SELECT s FROM Sysindexe s")
public class Sysindexe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="base_partition")
	private int basePartition;

	private int conditionid;

	private Timestamp crdate;

	private byte csid;

	private int distribution;

	private int doampg;

	@Column(name="exp_rowsize")
	private short expRowsize;

	@Column(name="fill_factor")
	private short fillFactor;

	private int first;

	private int id;

	private int identitygap;

	private short indid;

	private int ioampg;

	private int ipgtrips;

	private short keycnt;

	private byte[] keys1;

	private byte[] keys2;

	private byte[] keys3;

	private short maxirow;

	private short maxlen;

	private short maxrowsperpage;

	private short minlen;

	private String name;

	private int oampgtrips;

	private short partitiontype;

	@Column(name="res_page_gap")
	private short resPageGap;

	private int root;

	private short segment;

	private byte soid;

	private short status;

	private short status2;

	private short status3;

	private short usagecnt;

	public Sysindexe() {
	}

	public int getBasePartition() {
		return this.basePartition;
	}

	public void setBasePartition(int basePartition) {
		this.basePartition = basePartition;
	}

	public int getConditionid() {
		return this.conditionid;
	}

	public void setConditionid(int conditionid) {
		this.conditionid = conditionid;
	}

	public Timestamp getCrdate() {
		return this.crdate;
	}

	public void setCrdate(Timestamp crdate) {
		this.crdate = crdate;
	}

	public byte getCsid() {
		return this.csid;
	}

	public void setCsid(byte csid) {
		this.csid = csid;
	}

	public int getDistribution() {
		return this.distribution;
	}

	public void setDistribution(int distribution) {
		this.distribution = distribution;
	}

	public int getDoampg() {
		return this.doampg;
	}

	public void setDoampg(int doampg) {
		this.doampg = doampg;
	}

	public short getExpRowsize() {
		return this.expRowsize;
	}

	public void setExpRowsize(short expRowsize) {
		this.expRowsize = expRowsize;
	}

	public short getFillFactor() {
		return this.fillFactor;
	}

	public void setFillFactor(short fillFactor) {
		this.fillFactor = fillFactor;
	}

	public int getFirst() {
		return this.first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdentitygap() {
		return this.identitygap;
	}

	public void setIdentitygap(int identitygap) {
		this.identitygap = identitygap;
	}

	public short getIndid() {
		return this.indid;
	}

	public void setIndid(short indid) {
		this.indid = indid;
	}

	public int getIoampg() {
		return this.ioampg;
	}

	public void setIoampg(int ioampg) {
		this.ioampg = ioampg;
	}

	public int getIpgtrips() {
		return this.ipgtrips;
	}

	public void setIpgtrips(int ipgtrips) {
		this.ipgtrips = ipgtrips;
	}

	public short getKeycnt() {
		return this.keycnt;
	}

	public void setKeycnt(short keycnt) {
		this.keycnt = keycnt;
	}

	public byte[] getKeys1() {
		return this.keys1;
	}

	public void setKeys1(byte[] keys1) {
		this.keys1 = keys1;
	}

	public byte[] getKeys2() {
		return this.keys2;
	}

	public void setKeys2(byte[] keys2) {
		this.keys2 = keys2;
	}

	public byte[] getKeys3() {
		return this.keys3;
	}

	public void setKeys3(byte[] keys3) {
		this.keys3 = keys3;
	}

	public short getMaxirow() {
		return this.maxirow;
	}

	public void setMaxirow(short maxirow) {
		this.maxirow = maxirow;
	}

	public short getMaxlen() {
		return this.maxlen;
	}

	public void setMaxlen(short maxlen) {
		this.maxlen = maxlen;
	}

	public short getMaxrowsperpage() {
		return this.maxrowsperpage;
	}

	public void setMaxrowsperpage(short maxrowsperpage) {
		this.maxrowsperpage = maxrowsperpage;
	}

	public short getMinlen() {
		return this.minlen;
	}

	public void setMinlen(short minlen) {
		this.minlen = minlen;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOampgtrips() {
		return this.oampgtrips;
	}

	public void setOampgtrips(int oampgtrips) {
		this.oampgtrips = oampgtrips;
	}

	public short getPartitiontype() {
		return this.partitiontype;
	}

	public void setPartitiontype(short partitiontype) {
		this.partitiontype = partitiontype;
	}

	public short getResPageGap() {
		return this.resPageGap;
	}

	public void setResPageGap(short resPageGap) {
		this.resPageGap = resPageGap;
	}

	public int getRoot() {
		return this.root;
	}

	public void setRoot(int root) {
		this.root = root;
	}

	public short getSegment() {
		return this.segment;
	}

	public void setSegment(short segment) {
		this.segment = segment;
	}

	public byte getSoid() {
		return this.soid;
	}

	public void setSoid(byte soid) {
		this.soid = soid;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getStatus2() {
		return this.status2;
	}

	public void setStatus2(short status2) {
		this.status2 = status2;
	}

	public short getStatus3() {
		return this.status3;
	}

	public void setStatus3(short status3) {
		this.status3 = status3;
	}

	public short getUsagecnt() {
		return this.usagecnt;
	}

	public void setUsagecnt(short usagecnt) {
		this.usagecnt = usagecnt;
	}

}