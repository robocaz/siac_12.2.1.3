package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

import cl.exe.ccs.dto.certificados.CertificadoAclDTO;

public class ConsultaCerAclDTO extends ResponseDTO implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5945359944116894486L;
	private Long corrCaja;
    private Long corrTransac;
    private String rutAfectado;
    private String nombreAfectado;
    private Long corrNombre;
    private String codUsuario;
    private Long corrMovimiento;
    private String codServicio;
    private String fechaAcl;
    private String codAutenticBic;
    private String sucursal;
    private String tipoCertificado;
    private List<CertificadoAclDTO> detalle;
    
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Long getCorrTransac() {
		return corrTransac;
	}
	public void setCorrTransac(Long corrTransac) {
		this.corrTransac = corrTransac;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getNombreAfectado() {
		return nombreAfectado;
	}
	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}
	public Long getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Long corrNombre) {
		this.corrNombre = corrNombre;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public Long getCorrMovimiento() {
		return corrMovimiento;
	}
	public void setCorrMovimiento(Long corrMovimiento) {
		this.corrMovimiento = corrMovimiento;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getFechaAcl() {
		return fechaAcl;
	}
	public void setFechaAcl(String fechaAcl) {
		this.fechaAcl = fechaAcl;
	}
	public String getCodAutenticBic() {
		return codAutenticBic;
	}
	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getTipoCertificado() {
		return tipoCertificado;
	}
	public void setTipoCertificado(String tipoCertificado) {
		this.tipoCertificado = tipoCertificado;
	}
	public List<CertificadoAclDTO> getDetalle() {
        return detalle;
	}
	public void setDetalle(List<CertificadoAclDTO> detalle) {
        this.detalle = detalle;
	}
    
}
