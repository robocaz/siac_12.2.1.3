package cl.ccs.siac.dao;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteCajaDTO;
import cl.exe.ccs.dto.TramitanteDTO;

@Named("tramitanteDao")
@Stateless
public class TramitanteDaoImpl implements TramitanteDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(TramitanteDaoImpl.class);

	public TramitanteDTO obtenerTramitante(String rut) {

		TramitanteDTO tramitanteDTO = new TramitanteDTO();

		try {
			log.info("Ejecutando SQ_Tramitante_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_Tramitante_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomTram", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Telefono", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Email", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_RutTramitante", rut);
			// execute SP
			storedProcedure.execute();
			// get result

			tramitanteDTO.setRut(rut);
			tramitanteDTO.setNombres(storedProcedure.getOutputParameterValue("Fld_NomTram_Nombres").toString());
			tramitanteDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_NomTram_ApPat").toString());
			tramitanteDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_NomTram_ApMat").toString());
			tramitanteDTO.setDireccion(storedProcedure.getOutputParameterValue("Fld_Direccion").toString());
			tramitanteDTO.setTelefono(storedProcedure.getOutputParameterValue("Fld_Telefono").toString());
			tramitanteDTO.setGiro(storedProcedure.getOutputParameterValue("Fld_Giro").toString());
			ComboDTO comuna = new ComboDTO();
			comuna.setCodigo(storedProcedure.getOutputParameterValue("Fld_CodComuna").toString());
			tramitanteDTO.setComuna(comuna);
			if (storedProcedure.getOutputParameterValue("Fld_CodRegion") != null) {
				ComboDTO region = new ComboDTO();
				region.setCodigo(storedProcedure.getOutputParameterValue("Fld_CodRegion").toString());
				tramitanteDTO.setRegion(region);
			}
			tramitanteDTO.setCorrelativo(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CorrNomTram").toString()));
			if(storedProcedure.getOutputParameterValue("Fld_Email") != null){
				tramitanteDTO.setEmail(storedProcedure.getOutputParameterValue("Fld_Email").toString().trim());
			}
			else{
				tramitanteDTO.setEmail("");
			}

			tramitanteDTO.setIdControl(0);
			return tramitanteDTO;
		} catch (Exception e) {
			log.error("Error en el SP SQ_Tramitante_SW2 " + e.getMessage());
			e.printStackTrace();
			tramitanteDTO.setIdControl(1);
			tramitanteDTO.setMsgControl(e.getMessage());

			return tramitanteDTO;
		}

	}

	public TramitanteDTO actualizarTramitante(TramitanteDTO tramitante) {

		try {

			log.info("Ejecutando SP_Tramitante_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_Tramitante_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApPat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_ApMat", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NomTram_Nombres", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Direccion", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Telefono", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Giro", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Email", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomTram", Integer.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_RutTramitante", tramitante.getRut());
			storedProcedure.setParameter("Fld_NomTram_ApPat", tramitante.getApellidoPaterno());
			storedProcedure.setParameter("Fld_NomTram_ApMat", tramitante.getApellidoMaterno());
			storedProcedure.setParameter("Fld_NomTram_Nombres", tramitante.getNombres());
			storedProcedure.setParameter("Fld_Direccion", tramitante.getDireccion());
			if(tramitante.getTelefono() != null){
				storedProcedure.setParameter("Fld_Telefono", tramitante.getTelefono());
			}
			else{
				storedProcedure.setParameter("Fld_Telefono", "");
			}
			if (tramitante.getGiro() == null) {
				storedProcedure.setParameter("Fld_Giro", "");
			} else {
				storedProcedure.setParameter("Fld_Giro", tramitante.getGiro());
			}
			
			if(tramitante.getEmail() != null){
				storedProcedure.setParameter("Fld_Email", tramitante.getEmail());
			}
			else{
				storedProcedure.setParameter("Fld_Email", "");
			}
			storedProcedure.setParameter("Fld_CodUsuario", tramitante.getUsuario());
			if (tramitante.getComuna() != null) {
				storedProcedure.setParameter("Fld_CodComuna", Integer.parseInt(tramitante.getComuna().getCodigo()));
			} else {
				storedProcedure.setParameter("Fld_CodComuna", 0);
			}

			storedProcedure.execute();

			tramitante.setCorrelativo(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CorrNomTram").toString()));

		} catch (Exception e) {
			log.error("Error en el SP SP_Tramitante_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return tramitante;

	}

	@Override
	public TramitanteCajaDTO obtenerTramitanteCaja(Long corrCaja) {

		TramitanteCajaDTO tramitanteCajaDTO = new TramitanteCajaDTO();

		try {

			log.info("Ejecutando SQ_TramitanteTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_TramitanteTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomTramitante", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_DeclaracionUso", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_RutRequirente", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomRequirente", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMotivo", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);

			storedProcedure.execute();
			tramitanteCajaDTO = new TramitanteCajaDTO();
			tramitanteCajaDTO.setCorrCaja(corrCaja);
			tramitanteCajaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			tramitanteCajaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			if (tramitanteCajaDTO.getIdControl() == 0) {
				tramitanteCajaDTO.setRutTramitante(storedProcedure.getOutputParameterValue("Fld_RutTramitante").toString());
				boolean declUso = storedProcedure.getOutputParameterValue("Fld_DeclaracionUso").toString().equals("1");
				tramitanteCajaDTO.setTieneDeclaracionUso(declUso);
				tramitanteCajaDTO.setRutRequirente(storedProcedure.getOutputParameterValue("Fld_RutRequirente").toString());
				tramitanteCajaDTO.setCodMotivo(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CodMotivo").toString()));
				tramitanteCajaDTO.setCorrRequirente(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CorrNomRequirente").toString()));
				tramitanteCajaDTO.setCorrTramitante(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_CorrNomTramitante").toString()));
			}
			return tramitanteCajaDTO;

		} catch (Exception e) {
			log.error("Error en el SP SQ_TramitanteTmp_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseDTO insertarTramitanteCaja(TramitanteCajaDTO tramitanteCajaDTO) {

		try {

			log.info("Ejecutando SI_TramitanteTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_TramitanteTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_DeclaracionUso", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutRequirente", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMotivo", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", tramitanteCajaDTO.getCorrCaja());
			storedProcedure.setParameter("Fld_RutTramitante", tramitanteCajaDTO.getRutTramitante());
			storedProcedure.setParameter("Fld_DeclaracionUso", tramitanteCajaDTO.getTieneDeclaracionUso() ? 1 : 0);
			storedProcedure.setParameter("Fld_RutRequirente", tramitanteCajaDTO.getRutRequirente());
			storedProcedure.setParameter("Fld_CodMotivo", tramitanteCajaDTO.getCodMotivo());

			storedProcedure.execute();
			ResponseDTO respuesta = new ResponseDTO();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return respuesta;

		} catch (Exception e) {
			log.error("Error en el SP SI_TramitanteTmp_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseDTO actualizarTramitanteCaja(TramitanteCajaDTO tramitanteCajaDTO) {
		try {

			log.info("Ejecutando SU_ActTramitanteTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SU_ActTramitanteTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomTramitante", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutRequirente", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomRequirente", Integer.class, ParameterMode.IN);			
			storedProcedure.registerStoredProcedureParameter("Fld_DeclaracionUso", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMotivo", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			storedProcedure.setParameter("Fld_CorrCaja", tramitanteCajaDTO.getCorrCaja());
			storedProcedure.setParameter("Fld_RutTramitante", tramitanteCajaDTO.getRutTramitante());
			storedProcedure.setParameter("Fld_CorrNomTramitante", tramitanteCajaDTO.getCorrTramitante());
			storedProcedure.setParameter("Fld_RutRequirente", tramitanteCajaDTO.getRutRequirente());
			storedProcedure.setParameter("Fld_CorrNomRequirente", tramitanteCajaDTO.getCorrRequirente());
			storedProcedure.setParameter("Fld_DeclaracionUso", tramitanteCajaDTO.getTieneDeclaracionUso() ? 1 : 0);
			storedProcedure.setParameter("Fld_CodMotivo", tramitanteCajaDTO.getCodMotivo());

			storedProcedure.execute();
			ResponseDTO respuesta = new ResponseDTO();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return respuesta;

		} catch (Exception e) {
			log.error("Error en el SP SU_ActTramitanteTmp_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseDTO deleteTramitanteCaja(Long corrCaja) {
		try {

			log.info("Ejecutando SD_DelTramitanteTmp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_DelTramitanteTmp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);

			storedProcedure.execute();
			ResponseDTO respuesta = new ResponseDTO();

			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());

			return respuesta;

		} catch (Exception e) {
			log.error("Error en el SP SD_DelTramitanteTmp_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public TramitanteCajaDTO obtieneDatosDeUso(TramitanteCajaDTO tramitanteCajaDTO) {
		try {

			log.info("Ejecutando SQ_DeclaracionUsoTemp_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DeclaracionUsoTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutRequirente", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNomRequirente", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_ApPaternoReq", String.class, ParameterMode.OUT);			
			storedProcedure.registerStoredProcedureParameter("Fld_ApMaternoReq", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NombreReq", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_DeclaracionUso", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodMotivo", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", tramitanteCajaDTO.getCorrCaja());
			storedProcedure.setParameter("Fld_RutTramitante", tramitanteCajaDTO.getRutTramitante());

			storedProcedure.execute();
			
			tramitanteCajaDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			tramitanteCajaDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			
			if(tramitanteCajaDTO.getIdControl() == 0){
				
				tramitanteCajaDTO.setRutRequirente(storedProcedure.getOutputParameterValue("Fld_RutRequirente").toString());
				tramitanteCajaDTO.setCorrRequirente(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_RutRequirente").toString()));
				tramitanteCajaDTO.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_ApPaternoReq").toString());
				tramitanteCajaDTO.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_ApMaternoReq").toString());
				tramitanteCajaDTO.setNombre(storedProcedure.getOutputParameterValue("Fld_NombreReq").toString());
				tramitanteCajaDTO.setTieneDeclaracionUso(Boolean.parseBoolean(storedProcedure.getOutputParameterValue("Fld_DeclaracionUso").toString()));
				
			}

			return tramitanteCajaDTO;

		} catch (Exception e) {
			log.error("Error en el SP SQ_DeclaracionUsoTemp_SW2 " + e.getMessage());
			e.printStackTrace();
			return tramitanteCajaDTO;
		}
	}


}
