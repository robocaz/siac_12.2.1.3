angular.module('ccs').factory('TransaccionResource', function ($resource) {
    var resource = $resource('rest/transaccion/',  { nocache : new Date().getTime()},
    {
        'listarservicio' :  { method : 'POST' ,url: 'rest/transaccion/listarservicio', isArray: true},
        'agregarservicio' :  { method : 'POST', url: 'rest/transaccion/agregarservicio'}
    });
    return resource;
});
