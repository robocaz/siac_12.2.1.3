package cl.exe.ccs.dto;

import java.io.Serializable;

public class PersonaDTO extends ResponseDTO implements Serializable{
	
	private Long correlativo;
	private String rut;
    private String apellidoPaterno;
    private String apellidoMaterno ;
    private String nombres ;
    private String calidadJuridica = "N" ;
    private Integer tipoNombre = 0;
    private Integer cantidadFila ;
    
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCalidadJuridica() {
		return calidadJuridica;
	}
	public void setCalidadJuridica(String calidadJuridica) {
		this.calidadJuridica = calidadJuridica;
	}
	public Integer getTipoNombre() {
		return tipoNombre;
	}
	public void setTipoNombre(Integer tipoNombre) {
		this.tipoNombre = tipoNombre;
	}
	public Long getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Long correlativo) {
		this.correlativo = correlativo;
	}
	public Integer getCantidadFila() {
		return cantidadFila;
	}
	public void setCantidadFila(Integer cantidadFila) {
		this.cantidadFila = cantidadFila;
	}
}
