package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_ESTADOUSUARIO database table.
 * 
 */
@Entity
@Table(name="TBL_ESTADOUSUARIO")
@NamedQuery(name="TblEstadousuario.findAll", query="SELECT t FROM TblEstadousuario t")
public class TblEstadousuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ESTADOUSUARIO_FLDCODESTADO_GENERATOR", sequenceName="FLD_CODESTADO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ESTADOUSUARIO_FLDCODESTADO_GENERATOR")
	@Column(name="FLD_CODESTADO")
	private long fldCodestado;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSAESTADO")
	private String fldGlosaestado;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblUsuariomol
	@OneToMany(mappedBy="tblEstadousuario")
	private List<TblUsuariomol> tblUsuariomols;

	public TblEstadousuario() {
	}

	public long getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(long fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosaestado() {
		return this.fldGlosaestado;
	}

	public void setFldGlosaestado(String fldGlosaestado) {
		this.fldGlosaestado = fldGlosaestado;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblUsuariomol> getTblUsuariomols() {
		return this.tblUsuariomols;
	}

	public void setTblUsuariomols(List<TblUsuariomol> tblUsuariomols) {
		this.tblUsuariomols = tblUsuariomols;
	}

	public TblUsuariomol addTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().add(tblUsuariomol);
		tblUsuariomol.setTblEstadousuario(this);

		return tblUsuariomol;
	}

	public TblUsuariomol removeTblUsuariomol(TblUsuariomol tblUsuariomol) {
		getTblUsuariomols().remove(tblUsuariomol);
		tblUsuariomol.setTblEstadousuario(null);

		return tblUsuariomol;
	}

}