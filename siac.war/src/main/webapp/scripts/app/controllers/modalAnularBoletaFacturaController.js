angular.module('ccs').controller('ModalAnularBoletaFacturaController', function($scope, $rootScope, $localStorage, $uibModal, $document, CajaResource, BoletaResource, FacturaResource){

  var ctrl = this;
  $scope.form = {tipodoc: 'B'};
  $scope.boleta = {};
  $scope.cajas = [];

  $scope.anular = function(){
    if ( typeof($scope.form.folio) === 'undefined' || $scope.form.folio === null || isNaN($scope.form.folio) || parseInt($scope.form.folio) <= 0 ) {
      var mensaje = "Debe ingresar un N° de " + ($scope.form.tipodoc === 'B' ? 'Boleta valildo' : 'Factura válido');
      swal({ title: 'Error', text: mensaje, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () { angular.element("#num_folio_anul").focus();	});
      return;
    }

    var mensaje = "¿Desea anular la " + ($scope.form.tipodoc === 'B' ? 'Boleta' : 'Factura') + " N° " + $scope.form.folio + " del sistema?";
    if ( $scope.form.folio < $scope.form.folioinicial || $scope.form.folio > $scope.form.foliofinal ) {
      // swal({ title: 'Error', text: 'Folio Fuera de Rango', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'}).then(
      // function () { angular.element("#num_folio_anul").focus();	});
      // return;
      var mensaje = "La " + ($scope.form.tipodoc === 'B' ? 'Boleta' : 'Factura') + " N° " + $scope.form.folio + " esta fuera de su Rango Asignado. ¿Desea Anular de todas formas?";
    }

    swal({
      title: 'Aviso',
      text: mensaje,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      allowOutsideClick: false
    }).then(function () {

      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modalloginsupervisor.html',
        controller: 'ModalLoginSupervisorController',
        appendTo: angular.element($document[0].querySelector('#anularboletafactura')),
        size: 'sm'
      });
      modalInstance.result.then(function( respuesta ) {
        if( respuesta ) {
          var anularBF = {
            idCaja: $scope.form.caja,
            tipo: $scope.form.tipodoc,
            actualBoleta: $scope.form.folio,
            codUsuario: $rootScope.usuario.username
          };
          var successCallbackAnularBF = function(data, responseHeaders){
            console.log('successCallbackAnularBF');
            console.log(data);

            if( data.idControl !== 0 ) {
              swal({
                title: 'Error',
                text: data.msgControl,
                type: 'error',
                showCloseButton: true,
                confirmButtonText: 'cerrar',
                allowOutsideClick: false
              });
              return;
            }
            var mensaje = ($scope.form.tipodoc === 'B' ? 'Boleta' : 'Factura') + " anulada exitosamente!";
            swal({ title: 'Éxito', text: mensaje, type: 'success', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function() {
            	$scope.form.folio = '';
            	angular.element("#num_folio_anul").val("");
            });
          };
          BoletaResource.anularBoleta(anularBF, successCallbackAnularBF, $scope.errorCallback);
        }
      });

    }, function(dismiss) {
      return false;
    });
  };

  $scope.onCambiaTipoDoc = function(){
    $scope.form.folioinicial = 0;
    $scope.form.foliofinal = 0;
    $scope.form.folio = '';
    console.log($scope.form.caja);
    if( typeof($scope.form.caja) !== 'undefined' && $scope.form.caja !== '' ) {
      $scope.getRangos();
    }
  };

  $scope.onCambiaCaja = function(){
    $scope.getRangos();
  };

  $scope.getRangos = function() {
    $scope.form.folioinicial = 0;
    $scope.form.foliofinal = 0;
    $scope.form.folio = '';
    var consultaRangoBF = { corrCaja: $scope.form.caja, tipoDoc: $scope.form.tipodoc };
    var successCallbackConsultaRangoBF = function(data, responseHeaders) {
      console.log('successCallbackConsultaRangoBF');
      console.log(data);

      if( data.idControl !== 0 ) {
        var mensaje = typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró obtener los rangos para el tipo documento y caja seleccionada' : data.msgControl;
        swal({
          title: 'Error',
          text: mensaje,
          type: 'error',
          showCloseButton: true,
          confirmButtonText: 'cerrar',
          allowOutsideClick: false
        });
        return;
      }

      $scope.form.folioinicial = data.rangoDesde;
      $scope.form.foliofinal = data.rangoHasta;
    };
    FacturaResource.consultaRangoBF(consultaRangoBF, successCallbackConsultaRangoBF, $scope.errorCallback);
  };

  $scope.cerrar = function () {
	$scope.form = {};
    $scope.form = {tipodoc: 'B'};
    ctrl.formulario.$setPristine();
    ctrl.formulario.$setUntouched();
    angular.element('#anularboletafactura').modal("hide");
  };

  $scope.init = function () {

    // Validación Carro Vacío
    if( $rootScope.transacciones.length > 0 ) {
      swal({ title: 'Error', text: 'Existen transacciones sin procesar, no es posible anular Boletas o Facturas.', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function(){
        $scope.cerrar();
      });
      return;
    }

    $scope.form.centrocosto = $rootScope.usuario.centroCostoActivo.descripcion;

    // Cargar Listado de Cajas del Día
    var successCallbackListarCajasDia = function (data) {
      console.log('successCallbackListarCajasDia');
      console.log(data);

      /*if( data.idControl !== 0 ) {
        var mensaje = typeof(data.msgControl) === 'undefined' || data.msgControl === null || data.msgControl.trim() === '' ? 'No se logró obtener las cajas del día' : data.msgControl;
        swal({ title: 'Error', text: mensaje, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar', allowOutsideClick: false}).then(function(){
          $scope.cerrar();
        });
        return;
      }*/

      $scope.cajas = data;
    };
    CajaResource.listarCajasDia(successCallbackListarCajasDia, $scope.errorCallback);

    angular.element("#folio_fin_anul").focus();
  };

  $scope.abrir = function(){
    console.log('ModalAnularBoletaFacturaController - abrir');
    angular.element("#anularboletafactura").modal();
  };

  // Se llama al método inicial del controlador
  angular.element("#anularboletafactura").on('show.bs.modal', function () {
    $scope.init();
  });

});
