package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalProtesto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalProtesto.findAll", query="SELECT t FROM Tbl_CausalProtesto t")
public class Tbl_CausalProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_NroCausalProt")
	private short fld_NroCausalProt;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_CausalProtesto() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public short getFld_NroCausalProt() {
		return this.fld_NroCausalProt;
	}

	public void setFld_NroCausalProt(short fld_NroCausalProt) {
		this.fld_NroCausalProt = fld_NroCausalProt;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}