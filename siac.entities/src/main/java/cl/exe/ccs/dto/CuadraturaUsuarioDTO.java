package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class CuadraturaUsuarioDTO implements Serializable{
	
	private Long montoResuTransacciones;
	private Long montoResuIngCajera;
	private Long montoResuDiferencia;
	private List<ResultadoPorUsuarioDTO> resultadoUsuarios;
	private String fechaDesde;
	private String fechaHasta;
	private String usuario;
	private Integer centroCosto;
	private String golsaCC;
	
	
	
	public String getGolsaCC() {
		return golsaCC;
	}
	public void setGolsaCC(String golsaCC) {
		this.golsaCC = golsaCC;
	}
	public Long getMontoResuTransacciones() {
		return montoResuTransacciones;
	}
	public void setMontoResuTransacciones(Long montoResuTransacciones) {
		this.montoResuTransacciones = montoResuTransacciones;
	}
	public Long getMontoResuIngCajera() {
		return montoResuIngCajera;
	}
	public void setMontoResuIngCajera(Long montoResuIngCajera) {
		this.montoResuIngCajera = montoResuIngCajera;
	}
	public Long getMontoResuDiferencia() {
		return montoResuDiferencia;
	}
	public void setMontoResuDiferencia(Long montoResuDiferencia) {
		this.montoResuDiferencia = montoResuDiferencia;
	}
	public List<ResultadoPorUsuarioDTO> getResultadoUsuarios() {
		return resultadoUsuarios;
	}
	public void setResultadoUsuarios(List<ResultadoPorUsuarioDTO> resultadoUsuarios) {
		this.resultadoUsuarios = resultadoUsuarios;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(Integer centroCosto) {
		this.centroCosto = centroCosto;
	}

	
	
	
}
