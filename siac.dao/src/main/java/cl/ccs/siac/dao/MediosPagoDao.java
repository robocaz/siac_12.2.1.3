package cl.ccs.siac.dao;

import java.util.List;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.MedioPagoDTO;

public interface MediosPagoDao {

	public List<ComboDTO> obtenerMediosPago();
	
	public MedioPagoDTO obtenerBoletas (MedioPagoDTO medioPagoDTO);
	
	public MedioPagoDTO actualizaMedioDePago(MedioPagoDTO medioPagoDTO);
	
}
