package cl.ccs.siac.tramitante;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.dao.TramitanteDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteCajaDTO;
import cl.exe.ccs.dto.TramitanteDTO;

@Named("tramitanteBusiness")
public class TramitanteBusinessImpl implements TramitanteBusiness {

	@Inject
	private TramitanteDao tramitanteDao;

	@Inject
	private Utils utils;

	private Logger log = LogManager.getLogger(TramitanteBusinessImpl.class);

	public TramitanteDTO obtenerTramitante(String rut) {

		String rutParse = "";
		if (rut != null && !rut.equals("")) {

			rutParse = utils.formatearRUT(rut);
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');

			log.info(rutParse);
		}

		TramitanteDTO tramitanteDTO = new TramitanteDTO();
		tramitanteDTO = tramitanteDao.obtenerTramitante(rutParse);

		if (tramitanteDTO.getCorrCaja() != null && tramitanteDTO.getCorrCaja() != 0L && tramitanteDTO.getCorrelativo() != 0) {
			TramitanteCajaDTO tramitanteCajaDTO = tramitanteDao.obtenerTramitanteCaja(tramitanteDTO.getCorrCaja());
			tramitanteDTO.setCorrCaja(tramitanteCajaDTO.getCorrCaja());
			tramitanteDTO.setTieneDeclaracionUso(tramitanteCajaDTO.getTieneDeclaracionUso());
			tramitanteDTO.setRut(tramitanteCajaDTO.getRutTramitante());
		}
		return tramitanteDTO;

	}

	public TramitanteDTO obtenerTramitante(Long corrCaja) {

		TramitanteCajaDTO tramitanteCajaDTO = tramitanteDao.obtenerTramitanteCaja(corrCaja);
		TramitanteDTO tramitanteDTO = new TramitanteDTO();
		if (tramitanteCajaDTO.getIdControl() == 0) {
//			tramitanteDTO.setCorrCaja(tramitanteCajaDTO.getCorrCaja());
//			tramitanteDTO.setTieneDeclaracionUso(new Boolean(tramitanteCajaDTO.getTieneDeclaracionUso()));
//			tramitanteDTO.setDeclaracionUso(tramitanteCajaDTO.getTieneDeclaracionUso().toString());
//			tramitanteDTO.setRut(tramitanteCajaDTO.getRutTramitante());

			tramitanteDTO = tramitanteDao.obtenerTramitante(tramitanteCajaDTO.getRutTramitante());
			
			tramitanteDTO.setCorrCaja(tramitanteCajaDTO.getCorrCaja());
			tramitanteDTO.setTieneDeclaracionUso(new Boolean(tramitanteCajaDTO.getTieneDeclaracionUso()));
			tramitanteDTO.setDeclaracionUso(Boolean.parseBoolean(tramitanteCajaDTO.getTieneDeclaracionUso().toString()));
			tramitanteDTO.setRut(tramitanteCajaDTO.getRutTramitante());

			return tramitanteDTO;
		} else {
			return null;
		}

	}

	public TramitanteDTO actualizarTramitante(TramitanteDTO tramitanteDTO) {

		String rutParse = "";
		if (tramitanteDTO.getRut() != null && !tramitanteDTO.getRut().equals("")) {

			rutParse = utils.formatearRUT(tramitanteDTO.getRut());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');

			tramitanteDTO.setRut(rutParse);
		}

		tramitanteDTO = tramitanteDao.actualizarTramitante(tramitanteDTO);

		if (tramitanteDTO.getCorrCaja() != null && tramitanteDTO.getCorrCaja() != 0L && tramitanteDTO.getCorrelativo() != 0) {
			ResponseDTO respuesta = tramitanteDao.obtenerTramitanteCaja(tramitanteDTO.getCorrCaja());
			
			TramitanteCajaDTO respuestaTramitante = (TramitanteCajaDTO) respuesta;
			
			TramitanteCajaDTO tramitanteCaja = new TramitanteCajaDTO();
			tramitanteCaja.setCorrCaja(tramitanteDTO.getCorrCaja());
			tramitanteCaja.setRutTramitante(tramitanteDTO.getRut());
			tramitanteCaja.setCorrTramitante(tramitanteDTO.getCorrelativo());
			tramitanteCaja.setCodMotivo(respuestaTramitante.getCodMotivo());
			
			if(respuestaTramitante.getCorrRequirente() == null){
				tramitanteCaja.setRutRequirente("000000000-0");
				tramitanteCaja.setCorrRequirente(0);
			}
			else{
				tramitanteCaja.setRutRequirente(respuestaTramitante.getRutRequirente());
				tramitanteCaja.setCorrRequirente(respuestaTramitante.getCorrRequirente());
			}
			
			if(tramitanteCaja.getCodMotivo() != null && tramitanteCaja.getCodMotivo() > 0 ){
				tramitanteDTO.setTieneDeclaracionUso(true);
				tramitanteDTO.setDeclaracionUso(true);
				tramitanteCaja.setCodMotivo(tramitanteCaja.getCodMotivo());
				tramitanteCaja.setTieneDeclaracionUso(true);
//				
//				if(!tramitanteDTO.getTieneDeclaracionUso()){
//					tramitanteCaja.setCodMotivo(0);
//				}
			}
			else{
				tramitanteCaja.setCodMotivo(0);
				tramitanteCaja.setTieneDeclaracionUso(false);
			}

			
			if (respuesta.getIdControl() == 0L) {
				respuesta = tramitanteDao.actualizarTramitanteCaja(tramitanteCaja);
			} else {
				respuesta = tramitanteDao.insertarTramitanteCaja(tramitanteCaja);
			}
			if (respuesta.getIdControl() != 0) {
				tramitanteDTO.setIdControl(1);
			}
		}

		return tramitanteDTO;

	}

	public TramitanteCajaDTO obtenerDeclaracionUso(TramitanteCajaDTO tramitanteCajaDTO) {
		return tramitanteDao.obtenerTramitanteCaja(tramitanteCajaDTO.getCorrCaja());
	}

	public ResponseDTO declaracionUso(TramitanteCajaDTO tramitanteCajaDTO) {
		return tramitanteDao.actualizarTramitanteCaja(tramitanteCajaDTO);
	}

	public TramitanteCajaDTO obtieneDatosDeUso(TramitanteCajaDTO tramitanteCajaDTO){
		return tramitanteDao.obtieneDatosDeUso(tramitanteCajaDTO);
	}
}
