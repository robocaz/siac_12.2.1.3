package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdComunas database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdComunas")
@NamedQuery(name="Tp_CeIdComuna.findAll", query="SELECT t FROM Tp_CeIdComuna t")
public class Tp_CeIdComuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodComuna")
	private int fld_CodComuna;

	@Column(name="Fld_GlosaComuna")
	private String fld_GlosaComuna;

	public Tp_CeIdComuna() {
	}

	public int getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(int fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public String getFld_GlosaComuna() {
		return this.fld_GlosaComuna;
	}

	public void setFld_GlosaComuna(String fld_GlosaComuna) {
		this.fld_GlosaComuna = fld_GlosaComuna;
	}

}