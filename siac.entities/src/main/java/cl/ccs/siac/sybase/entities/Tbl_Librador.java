package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Librador database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Librador.findAll", query="SELECT t FROM Tbl_Librador t")
public class Tbl_Librador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMonedaOriginal")
	private byte fld_CodMonedaOriginal;

	@Column(name="Fld_CorrLib_Id")
	private BigDecimal fld_CorrLib_Id;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_MontoProtOriginal")
	private BigDecimal fld_MontoProtOriginal;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Librador() {
	}

	public byte getFld_CodMonedaOriginal() {
		return this.fld_CodMonedaOriginal;
	}

	public void setFld_CodMonedaOriginal(byte fld_CodMonedaOriginal) {
		this.fld_CodMonedaOriginal = fld_CodMonedaOriginal;
	}

	public BigDecimal getFld_CorrLib_Id() {
		return this.fld_CorrLib_Id;
	}

	public void setFld_CorrLib_Id(BigDecimal fld_CorrLib_Id) {
		this.fld_CorrLib_Id = fld_CorrLib_Id;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public BigDecimal getFld_MontoProtOriginal() {
		return this.fld_MontoProtOriginal;
	}

	public void setFld_MontoProtOriginal(BigDecimal fld_MontoProtOriginal) {
		this.fld_MontoProtOriginal = fld_MontoProtOriginal;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}