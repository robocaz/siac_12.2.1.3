package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_USUARIOMOL database table.
 * 
 */
@Entity
@Table(name="TBL_USUARIOMOL")
@NamedQuery(name="TblUsuariomol.findAll", query="SELECT t FROM TblUsuariomol t")
public class TblUsuariomol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_USUARIOMOL_FLDUSUARIO_GENERATOR", sequenceName="FLD_USUARIO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_USUARIOMOL_FLDUSUARIO_GENERATOR")
	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	@Column(name="FLD_CARGO")
	private String fldCargo;

	@Column(name="FLD_CIUDAD")
	private String fldCiudad;

	@Column(name="FLD_CODAREARESPONSABLE")
	private String fldCodarearesponsable;

	@Column(name="FLD_CODCCOSTO")
	private BigDecimal fldCodccosto;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CORRDIRECCION")
	private BigDecimal fldCorrdireccion;

	@Column(name="FLD_CORRNOMBRE")
	private BigDecimal fldCorrnombre;

	@Column(name="FLD_EMAIL")
	private String fldEmail;

	@Column(name="FLD_EMPRESA")
	private String fldEmpresa;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FECHACAMBIOPWD")
	private Timestamp fldFechacambiopwd;

	@Column(name="FLD_FECHACREACION")
	private Timestamp fldFechacreacion;

	@Column(name="FLD_FLAGCAMBIOPWD")
	private BigDecimal fldFlagcambiopwd;

	@Column(name="FLD_PASSWORD")
	private String fldPassword;

	@Column(name="FLD_RUTPERSONA")
	private String fldRutpersona;

	@Column(name="FLD_TELEFONO")
	private String fldTelefono;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_ULTIMOACCESO")
	private Timestamp fldUltimoacceso;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblLogaclaracion
	@OneToMany(mappedBy="tblUsuariomol")
	private List<TblLogaclaracion> tblLogaclaracions;

	//bi-directional many-to-one association to TblLogconsulta
	@OneToMany(mappedBy="tblUsuariomol")
	private List<TblLogconsulta> tblLogconsultas;

	//bi-directional many-to-one association to TblEstadousuario
	@ManyToOne
	@JoinColumn(name="FLD_CODESTADO")
	private TblEstadousuario tblEstadousuario;

	//bi-directional many-to-one association to TblTipousuario
	@ManyToOne
	@JoinColumn(name="FLD_TIPOUSUARIO")
	private TblTipousuario tblTipousuario;

	//bi-directional many-to-one association to TblPerfil
	@ManyToOne
	@JoinColumn(name="FLD_CORRPERFIL")
	private TblPerfil tblPerfil;

	public TblUsuariomol() {
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

	public String getFldCargo() {
		return this.fldCargo;
	}

	public void setFldCargo(String fldCargo) {
		this.fldCargo = fldCargo;
	}

	public String getFldCiudad() {
		return this.fldCiudad;
	}

	public void setFldCiudad(String fldCiudad) {
		this.fldCiudad = fldCiudad;
	}

	public String getFldCodarearesponsable() {
		return this.fldCodarearesponsable;
	}

	public void setFldCodarearesponsable(String fldCodarearesponsable) {
		this.fldCodarearesponsable = fldCodarearesponsable;
	}

	public BigDecimal getFldCodccosto() {
		return this.fldCodccosto;
	}

	public void setFldCodccosto(BigDecimal fldCodccosto) {
		this.fldCodccosto = fldCodccosto;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCorrdireccion() {
		return this.fldCorrdireccion;
	}

	public void setFldCorrdireccion(BigDecimal fldCorrdireccion) {
		this.fldCorrdireccion = fldCorrdireccion;
	}

	public BigDecimal getFldCorrnombre() {
		return this.fldCorrnombre;
	}

	public void setFldCorrnombre(BigDecimal fldCorrnombre) {
		this.fldCorrnombre = fldCorrnombre;
	}

	public String getFldEmail() {
		return this.fldEmail;
	}

	public void setFldEmail(String fldEmail) {
		this.fldEmail = fldEmail;
	}

	public String getFldEmpresa() {
		return this.fldEmpresa;
	}

	public void setFldEmpresa(String fldEmpresa) {
		this.fldEmpresa = fldEmpresa;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Timestamp getFldFechacambiopwd() {
		return this.fldFechacambiopwd;
	}

	public void setFldFechacambiopwd(Timestamp fldFechacambiopwd) {
		this.fldFechacambiopwd = fldFechacambiopwd;
	}

	public Timestamp getFldFechacreacion() {
		return this.fldFechacreacion;
	}

	public void setFldFechacreacion(Timestamp fldFechacreacion) {
		this.fldFechacreacion = fldFechacreacion;
	}

	public BigDecimal getFldFlagcambiopwd() {
		return this.fldFlagcambiopwd;
	}

	public void setFldFlagcambiopwd(BigDecimal fldFlagcambiopwd) {
		this.fldFlagcambiopwd = fldFlagcambiopwd;
	}

	public String getFldPassword() {
		return this.fldPassword;
	}

	public void setFldPassword(String fldPassword) {
		this.fldPassword = fldPassword;
	}

	public String getFldRutpersona() {
		return this.fldRutpersona;
	}

	public void setFldRutpersona(String fldRutpersona) {
		this.fldRutpersona = fldRutpersona;
	}

	public String getFldTelefono() {
		return this.fldTelefono;
	}

	public void setFldTelefono(String fldTelefono) {
		this.fldTelefono = fldTelefono;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public Timestamp getFldUltimoacceso() {
		return this.fldUltimoacceso;
	}

	public void setFldUltimoacceso(Timestamp fldUltimoacceso) {
		this.fldUltimoacceso = fldUltimoacceso;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblLogaclaracion> getTblLogaclaracions() {
		return this.tblLogaclaracions;
	}

	public void setTblLogaclaracions(List<TblLogaclaracion> tblLogaclaracions) {
		this.tblLogaclaracions = tblLogaclaracions;
	}

	public TblLogaclaracion addTblLogaclaracion(TblLogaclaracion tblLogaclaracion) {
		getTblLogaclaracions().add(tblLogaclaracion);
		tblLogaclaracion.setTblUsuariomol(this);

		return tblLogaclaracion;
	}

	public TblLogaclaracion removeTblLogaclaracion(TblLogaclaracion tblLogaclaracion) {
		getTblLogaclaracions().remove(tblLogaclaracion);
		tblLogaclaracion.setTblUsuariomol(null);

		return tblLogaclaracion;
	}

	public List<TblLogconsulta> getTblLogconsultas() {
		return this.tblLogconsultas;
	}

	public void setTblLogconsultas(List<TblLogconsulta> tblLogconsultas) {
		this.tblLogconsultas = tblLogconsultas;
	}

	public TblLogconsulta addTblLogconsulta(TblLogconsulta tblLogconsulta) {
		getTblLogconsultas().add(tblLogconsulta);
		tblLogconsulta.setTblUsuariomol(this);

		return tblLogconsulta;
	}

	public TblLogconsulta removeTblLogconsulta(TblLogconsulta tblLogconsulta) {
		getTblLogconsultas().remove(tblLogconsulta);
		tblLogconsulta.setTblUsuariomol(null);

		return tblLogconsulta;
	}

	public TblEstadousuario getTblEstadousuario() {
		return this.tblEstadousuario;
	}

	public void setTblEstadousuario(TblEstadousuario tblEstadousuario) {
		this.tblEstadousuario = tblEstadousuario;
	}

	public TblTipousuario getTblTipousuario() {
		return this.tblTipousuario;
	}

	public void setTblTipousuario(TblTipousuario tblTipousuario) {
		this.tblTipousuario = tblTipousuario;
	}

	public TblPerfil getTblPerfil() {
		return this.tblPerfil;
	}

	public void setTblPerfil(TblPerfil tblPerfil) {
		this.tblPerfil = tblPerfil;
	}

}