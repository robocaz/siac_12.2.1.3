package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the syscomments database table.
 * 
 */
@Entity
@Table(name="syscomments")
@NamedQuery(name="Syscomment.findAll", query="SELECT s FROM Syscomment s")
public class Syscomment implements Serializable {
	private static final long serialVersionUID = 1L;

	private short colid;

	private short colid2;

	private int encrkeyid;

	private int id;

	private short language;

	private short number;

	private int partitionid;

	private short status;

	private String text;

	private short texttype;

	private short version;

	public Syscomment() {
	}

	public short getColid() {
		return this.colid;
	}

	public void setColid(short colid) {
		this.colid = colid;
	}

	public short getColid2() {
		return this.colid2;
	}

	public void setColid2(short colid2) {
		this.colid2 = colid2;
	}

	public int getEncrkeyid() {
		return this.encrkeyid;
	}

	public void setEncrkeyid(int encrkeyid) {
		this.encrkeyid = encrkeyid;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getLanguage() {
		return this.language;
	}

	public void setLanguage(short language) {
		this.language = language;
	}

	public short getNumber() {
		return this.number;
	}

	public void setNumber(short number) {
		this.number = number;
	}

	public int getPartitionid() {
		return this.partitionid;
	}

	public void setPartitionid(int partitionid) {
		this.partitionid = partitionid;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public short getTexttype() {
		return this.texttype;
	}

	public void setTexttype(short texttype) {
		this.texttype = texttype;
	}

	public short getVersion() {
		return this.version;
	}

	public void setVersion(short version) {
		this.version = version;
	}

}