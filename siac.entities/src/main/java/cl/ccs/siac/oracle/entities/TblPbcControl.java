package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_PBC_CONTROL database table.
 * 
 */
@Entity
@Table(name="TBL_PBC_CONTROL")
@NamedQuery(name="TblPbcControl.findAll", query="SELECT t FROM TblPbcControl t")
public class TblPbcControl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CANTIDAD")
	private BigDecimal fldCantidad;

	@Column(name="FLD_CODUSUARIO")
	private String fldCodusuario;

	@Column(name="FLD_CORRCONTROLPBC")
	private BigDecimal fldCorrcontrolpbc;

	@Column(name="FLD_FECHA")
	private Timestamp fldFecha;

	@Column(name="FLD_RUT")
	private String fldRut;

	public TblPbcControl() {
	}

	public BigDecimal getFldCantidad() {
		return this.fldCantidad;
	}

	public void setFldCantidad(BigDecimal fldCantidad) {
		this.fldCantidad = fldCantidad;
	}

	public String getFldCodusuario() {
		return this.fldCodusuario;
	}

	public void setFldCodusuario(String fldCodusuario) {
		this.fldCodusuario = fldCodusuario;
	}

	public BigDecimal getFldCorrcontrolpbc() {
		return this.fldCorrcontrolpbc;
	}

	public void setFldCorrcontrolpbc(BigDecimal fldCorrcontrolpbc) {
		this.fldCorrcontrolpbc = fldCorrcontrolpbc;
	}

	public Timestamp getFldFecha() {
		return this.fldFecha;
	}

	public void setFldFecha(Timestamp fldFecha) {
		this.fldFecha = fldFecha;
	}

	public String getFldRut() {
		return this.fldRut;
	}

	public void setFldRut(String fldRut) {
		this.fldRut = fldRut;
	}

}