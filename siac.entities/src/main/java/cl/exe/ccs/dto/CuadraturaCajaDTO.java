package cl.exe.ccs.dto;

import java.io.Serializable;

public class CuadraturaCajaDTO extends ResponseDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5975706967860422809L;
	
	
	private Integer corrCaja;
	private Integer codCentroCosto;
	private String glosaCentroCosto;
	private String horaInicio;
	private String horaTermino;
	
	
	
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getCodCentroCosto() {
		return codCentroCosto;
	}
	public void setCodCentroCosto(Integer codCentroCosto) {
		this.codCentroCosto = codCentroCosto;
	}
	public String getGlosaCentroCosto() {
		return glosaCentroCosto;
	}
	public void setGlosaCentroCosto(String glosaCentroCosto) {
		this.glosaCentroCosto = glosaCentroCosto;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraTermino() {
		return horaTermino;
	}
	public void setHoraTermino(String horaTermino) {
		this.horaTermino = horaTermino;
	}
	 

}
