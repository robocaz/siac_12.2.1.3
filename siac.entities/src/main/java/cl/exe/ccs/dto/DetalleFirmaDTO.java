package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DetalleFirmaDTO extends ResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4836347282648798682L;
	
	
	private FirmanteDTO firmante;
	private String cargoFirma;
    private String glosaEmisor;
    private String glosaSucursal;
    private boolean flagCertifAcl;
    private String fecVcto;
    private Long corrFirma;
    private Long codSucursalAux;
    private String tipoEmisorAux;
    private String codEmisorAux;
    private boolean flagCertifAclEsp;
    private boolean flagCertifRectif;
    private Integer criterio;
    private Integer estado;
    private String rutaImagen;
    private byte[] imageBinary;
    private String codTipoDoc;
    
    
    
    
    
	public String getCodTipoDoc() {
		return codTipoDoc;
	}
	public void setCodTipoDoc(String codTipoDoc) {
		this.codTipoDoc = codTipoDoc;
	}
	public FirmanteDTO getFirmante() {
		return firmante;
	}
	public void setFirmante(FirmanteDTO firmante) {
		this.firmante = firmante;
	}
	public String getCargoFirma() {
		return cargoFirma;
	}
	public void setCargoFirma(String cargoFirma) {
		this.cargoFirma = cargoFirma;
	}
	public String getGlosaEmisor() {
		return glosaEmisor;
	}
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}
	public String getGlosaSucursal() {
		return glosaSucursal;
	}
	public void setGlosaSucursal(String glosaSucursal) {
		this.glosaSucursal = glosaSucursal;
	}
	public boolean isFlagCertifAcl() {
		return flagCertifAcl;
	}
	public void setFlagCertifAcl(boolean flagCertifAcl) {
		this.flagCertifAcl = flagCertifAcl;
	}
	public String getFecVcto() {
		return fecVcto;
	}
	public void setFecVcto(String fecVcto) {
		this.fecVcto = fecVcto;
	}
	public Long getCorrFirma() {
		return corrFirma;
	}
	public void setCorrFirma(Long corrFirma) {
		this.corrFirma = corrFirma;
	}
	public Long getCodSucursalAux() {
		return codSucursalAux;
	}
	public void setCodSucursalAux(Long codSucursalAux) {
		this.codSucursalAux = codSucursalAux;
	}
	public String getTipoEmisorAux() {
		return tipoEmisorAux;
	}
	public void setTipoEmisorAux(String tipoEmisorAux) {
		this.tipoEmisorAux = tipoEmisorAux;
	}
	public String getCodEmisorAux() {
		return codEmisorAux;
	}
	public void setCodEmisorAux(String codEmisorAux) {
		this.codEmisorAux = codEmisorAux;
	}
	public boolean isFlagCertifAclEsp() {
		return flagCertifAclEsp;
	}
	public void setFlagCertifAclEsp(boolean flagCertifAclEsp) {
		this.flagCertifAclEsp = flagCertifAclEsp;
	}
	public boolean isFlagCertifRectif() {
		return flagCertifRectif;
	}
	public void setFlagCertifRectif(boolean flagCertifRectif) {
		this.flagCertifRectif = flagCertifRectif;
	}
	public Integer getCriterio() {
		return criterio;
	}
	public void setCriterio(Integer criterio) {
		this.criterio = criterio;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public String getRutaImagen() {
		return rutaImagen;
	}
	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}
	public byte[] getImageBinary() {
		return imageBinary;
	}
	public void setImageBinary(byte[] imageBinary) {
		this.imageBinary = imageBinary;
	}
    
    
    
    

}
