package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.combos.CombosBusiness;
import cl.ccs.siac.finiquito.FiniquitoBusiness;
import cl.ccs.siac.utils.GeneradorPDF;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.FiniquitoDTO;
import cl.exe.ccs.dto.PDFResponseDTO;

@Path("/finiquito")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class FiniquitoServicio {

	private static final Logger LOG = LogManager.getLogger(FiniquitoServicio.class);

	@Inject
	private FiniquitoBusiness finiquitoBusiness;

	@Inject
	private CombosBusiness combosBusiness;

	@Inject
	private Utils utils;

	private boolean validaUsuario(HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return false;
		}
		return true;
	}

	@POST
	@Path("/consulta")
	public Response consulta(@Context HttpServletRequest request, String rut) {
		LOG.info("-> consulta: " + rut);
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		try {
			FiniquitoDTO response = finiquitoBusiness.consultaFiniquito(rut);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Metodo consulta - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Path("/guarda")
	public Response guarda(@Context HttpServletRequest request, FiniquitoDTO finiquito) {
		LOG.info("-> Rest Finiquito.. " + finiquito.getCorrFiniquito());
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			FiniquitoDTO response = finiquitoBusiness.guardaFiniquito(finiquito);
			return Response.ok(response).build();
		} catch (Exception e) {
			LOG.error("Metodo guarda - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@GET
	@Path("/cmbdocs")
	public Response cmbdocs(@Context HttpServletRequest request) {
		LOG.info("-> cmbdocs.. ");
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			List<ComboDTO> cmbDocs = combosBusiness.cmbDocPresentado();

			if (cmbDocs == null || cmbDocs.size() == 0) {
				return Response.ok(Response.Status.NOT_FOUND).build();
			} else {
				return Response.ok(cmbDocs).build();
			}
		} catch (Exception e) {
			LOG.error("Metodo cmbdocs - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@POST
	@Path("/pdf")
	public Response pdf(@Context HttpServletRequest request, FiniquitoDTO finiq) {
		LOG.info("-> Rest Finiquito PDF..");
		
		if (!validaUsuario(request)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		GeneradorPDF generador = new GeneradorPDF();
		Map<String, Object> mapFiniq = obtenerMapFiniquito(finiq);
		
		try {
			byte[] archivo = generador.create("Finiquito", new HashSet(), mapFiniq);
			
			PDFResponseDTO pdf = new PDFResponseDTO();
			pdf.setReporte(archivo);
			pdf.setIdControl(1);
			pdf.setMsgControl("OK");
			
			return Response.ok(pdf).build();
			
		} catch (Exception e) {
			LOG.error("Metodo pdf - Error Message: " + e.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	private Map<String, Object> obtenerMapFiniquito(FiniquitoDTO finiq) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("isProrroga", finiq.getMarcaProrroga());
		params.put("RUT_TRAM", finiq.getRutTramitante());
		params.put("NOMBRE_TRAM", finiq.getNombreTramitante());
		params.put("FECHA_PRES", utils.formatearFecha(new Date()));
		params.put("RUT_AFEC", finiq.getRutAfectado());
		
		final StringBuffer strTmp = new StringBuffer();
		strTmp.append(finiq.getNombreApPat());
		strTmp.append(", ");
		strTmp.append(finiq.getNombreApMat());
		strTmp.append(", ");
		strTmp.append(finiq.getNombreNombres());
		params.put("NOMBRE_AFEC", strTmp.toString());
		
		params.put("RUT_EMP", finiq.getRutEmpresa());
		final Boolean rutJuridico = esRutJuridico(finiq.getRutEmpresa());
		
		strTmp.setLength(0);
		strTmp.append(finiq.getNombreApPatEmp());
		if ( !rutJuridico ) {
			strTmp.append(", ");
		}
		if ( finiq.getNombreApMatEmp() != null && !finiq.getNombreApMatEmp().isEmpty() ) {
			strTmp.append(finiq.getNombreApMatEmp());
			if ( !rutJuridico ) {
				strTmp.append(", ");
			}
		}
		if ( finiq.getNombreNombresEmp() != null && !finiq.getNombreNombresEmp().isEmpty() ) {
			strTmp.append(finiq.getNombreNombresEmp());
		}
		params.put("NOMBRE_EMP", strTmp.toString());
		
		params.put("FECHA_TERM", finiq.getFecFinContrato());
		params.put("DOC_PRES", finiq.getMensaje());
		params.put("FECHA_FINIQ", finiq.getFecFiniquito());
		
		return params;
	}

	private Boolean esRutJuridico(String rut) {
		rut = rut.replaceAll("\\.", "");
		return Long.parseLong(rut.substring(0, rut.indexOf('-'))) > 50000000 ? true : false;
	}

}
