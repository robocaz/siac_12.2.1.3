package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TipoDocumento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoDocumento.findAll", query="SELECT t FROM Tbl_TipoDocumento t")
public class Tbl_TipoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Calidad")
	private BigDecimal fld_Calidad;

	@Column(name="Fld_CodAreaResponsable")
	private String fld_CodAreaResponsable;

	@Column(name="Fld_GlosaCortaTipoDoc")
	private String fld_GlosaCortaTipoDoc;

	@Column(name="Fld_GlosaTipoDocumento")
	private String fld_GlosaTipoDocumento;

	@Column(name="Fld_GradoDificultad")
	private BigDecimal fld_GradoDificultad;

	@Column(name="Fld_PlazoEstimado")
	private String fld_PlazoEstimado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	public Tbl_TipoDocumento() {
	}

	public BigDecimal getFld_Calidad() {
		return this.fld_Calidad;
	}

	public void setFld_Calidad(BigDecimal fld_Calidad) {
		this.fld_Calidad = fld_Calidad;
	}

	public String getFld_CodAreaResponsable() {
		return this.fld_CodAreaResponsable;
	}

	public void setFld_CodAreaResponsable(String fld_CodAreaResponsable) {
		this.fld_CodAreaResponsable = fld_CodAreaResponsable;
	}

	public String getFld_GlosaCortaTipoDoc() {
		return this.fld_GlosaCortaTipoDoc;
	}

	public void setFld_GlosaCortaTipoDoc(String fld_GlosaCortaTipoDoc) {
		this.fld_GlosaCortaTipoDoc = fld_GlosaCortaTipoDoc;
	}

	public String getFld_GlosaTipoDocumento() {
		return this.fld_GlosaTipoDocumento;
	}

	public void setFld_GlosaTipoDocumento(String fld_GlosaTipoDocumento) {
		this.fld_GlosaTipoDocumento = fld_GlosaTipoDocumento;
	}

	public BigDecimal getFld_GradoDificultad() {
		return this.fld_GradoDificultad;
	}

	public void setFld_GradoDificultad(BigDecimal fld_GradoDificultad) {
		this.fld_GradoDificultad = fld_GradoDificultad;
	}

	public String getFld_PlazoEstimado() {
		return this.fld_PlazoEstimado;
	}

	public void setFld_PlazoEstimado(String fld_PlazoEstimado) {
		this.fld_PlazoEstimado = fld_PlazoEstimado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

}