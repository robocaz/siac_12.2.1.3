package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdSituacionBloqueo database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdSituacionBloqueo.findAll", query="SELECT t FROM Tp_CeIdSituacionBloqueo t")
public class Tp_CeIdSituacionBloqueo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodSitBloqueo")
	private byte fld_CodSitBloqueo;

	@Column(name="Fld_GlosaSitBloqueo")
	private String fld_GlosaSitBloqueo;

	public Tp_CeIdSituacionBloqueo() {
	}

	public byte getFld_CodSitBloqueo() {
		return this.fld_CodSitBloqueo;
	}

	public void setFld_CodSitBloqueo(byte fld_CodSitBloqueo) {
		this.fld_CodSitBloqueo = fld_CodSitBloqueo;
	}

	public String getFld_GlosaSitBloqueo() {
		return this.fld_GlosaSitBloqueo;
	}

	public void setFld_GlosaSitBloqueo(String fld_GlosaSitBloqueo) {
		this.fld_GlosaSitBloqueo = fld_GlosaSitBloqueo;
	}

}