angular.module('ccs').factory('ProtestosResource', function ($resource) {
	var resource = $resource('rest/protestos/', { nocache : new Date().getTime()},
	{
		'buscar'    : { method:'POST', isArray:false, url:'rest/protestos/buscar'     },
		'detalle'   : { method:'POST', isArray:false, url:'rest/protestos/detalle'    },
		'aclaracion': { method:'POST', isArray:false, url:'rest/protestos/aclaracion' },
		'tramitante': { method:'POST', isArray:false, url:'rest/protestos/tramitante' }
	});
	return resource;
});
