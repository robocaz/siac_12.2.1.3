package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Resumen2 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Resumen2.findAll", query="SELECT t FROM Tbl_Resumen2 t")
public class Tbl_Resumen2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausalProt")
	private String fld_CodCausalProt;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrResumen")
	private BigDecimal fld_CorrResumen;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecCarga")
	private Timestamp fld_FecCarga;

	@Column(name="Fld_FecDigAcl")
	private Timestamp fld_FecDigAcl;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_Flag_MFecProtesto")
	private boolean fld_Flag_MFecProtesto;

	@Column(name="Fld_Flag_MMonto")
	private boolean fld_Flag_MMonto;

	@Column(name="Fld_Flag_MNroBolPub")
	private boolean fld_Flag_MNroBolPub;

	@Column(name="Fld_Flag_MNroOperacion")
	private boolean fld_Flag_MNroOperacion;

	@Column(name="Fld_Flag_MTipDoc")
	private boolean fld_Flag_MTipDoc;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_FolioProt")
	private int fld_FolioProt;

	@Column(name="Fld_Marca3Dig")
	private boolean fld_Marca3Dig;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MarcaAclEspecial")
	private boolean fld_MarcaAclEspecial;

	@Column(name="Fld_MarcaDigVerErroneo")
	private boolean fld_MarcaDigVerErroneo;

	@Column(name="Fld_MarcaRectif")
	private byte fld_MarcaRectif;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_NroLote")
	private double fld_NroLote;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_PagBoletin")
	private short fld_PagBoletin;

	@Column(name="Fld_PagBoletinAcl")
	private short fld_PagBoletinAcl;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoCert")
	private byte fld_TipoCert;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoRelacion")
	private byte fld_TipoRelacion;

	public Tbl_Resumen2() {
	}

	public String getFld_CodCausalProt() {
		return this.fld_CodCausalProt;
	}

	public void setFld_CodCausalProt(String fld_CodCausalProt) {
		this.fld_CodCausalProt = fld_CodCausalProt;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrResumen() {
		return this.fld_CorrResumen;
	}

	public void setFld_CorrResumen(BigDecimal fld_CorrResumen) {
		this.fld_CorrResumen = fld_CorrResumen;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecCarga() {
		return this.fld_FecCarga;
	}

	public void setFld_FecCarga(Timestamp fld_FecCarga) {
		this.fld_FecCarga = fld_FecCarga;
	}

	public Timestamp getFld_FecDigAcl() {
		return this.fld_FecDigAcl;
	}

	public void setFld_FecDigAcl(Timestamp fld_FecDigAcl) {
		this.fld_FecDigAcl = fld_FecDigAcl;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public boolean getFld_Flag_MFecProtesto() {
		return this.fld_Flag_MFecProtesto;
	}

	public void setFld_Flag_MFecProtesto(boolean fld_Flag_MFecProtesto) {
		this.fld_Flag_MFecProtesto = fld_Flag_MFecProtesto;
	}

	public boolean getFld_Flag_MMonto() {
		return this.fld_Flag_MMonto;
	}

	public void setFld_Flag_MMonto(boolean fld_Flag_MMonto) {
		this.fld_Flag_MMonto = fld_Flag_MMonto;
	}

	public boolean getFld_Flag_MNroBolPub() {
		return this.fld_Flag_MNroBolPub;
	}

	public void setFld_Flag_MNroBolPub(boolean fld_Flag_MNroBolPub) {
		this.fld_Flag_MNroBolPub = fld_Flag_MNroBolPub;
	}

	public boolean getFld_Flag_MNroOperacion() {
		return this.fld_Flag_MNroOperacion;
	}

	public void setFld_Flag_MNroOperacion(boolean fld_Flag_MNroOperacion) {
		this.fld_Flag_MNroOperacion = fld_Flag_MNroOperacion;
	}

	public boolean getFld_Flag_MTipDoc() {
		return this.fld_Flag_MTipDoc;
	}

	public void setFld_Flag_MTipDoc(boolean fld_Flag_MTipDoc) {
		this.fld_Flag_MTipDoc = fld_Flag_MTipDoc;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public int getFld_FolioProt() {
		return this.fld_FolioProt;
	}

	public void setFld_FolioProt(int fld_FolioProt) {
		this.fld_FolioProt = fld_FolioProt;
	}

	public boolean getFld_Marca3Dig() {
		return this.fld_Marca3Dig;
	}

	public void setFld_Marca3Dig(boolean fld_Marca3Dig) {
		this.fld_Marca3Dig = fld_Marca3Dig;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public boolean getFld_MarcaAclEspecial() {
		return this.fld_MarcaAclEspecial;
	}

	public void setFld_MarcaAclEspecial(boolean fld_MarcaAclEspecial) {
		this.fld_MarcaAclEspecial = fld_MarcaAclEspecial;
	}

	public boolean getFld_MarcaDigVerErroneo() {
		return this.fld_MarcaDigVerErroneo;
	}

	public void setFld_MarcaDigVerErroneo(boolean fld_MarcaDigVerErroneo) {
		this.fld_MarcaDigVerErroneo = fld_MarcaDigVerErroneo;
	}

	public byte getFld_MarcaRectif() {
		return this.fld_MarcaRectif;
	}

	public void setFld_MarcaRectif(byte fld_MarcaRectif) {
		this.fld_MarcaRectif = fld_MarcaRectif;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public double getFld_NroLote() {
		return this.fld_NroLote;
	}

	public void setFld_NroLote(double fld_NroLote) {
		this.fld_NroLote = fld_NroLote;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public short getFld_PagBoletin() {
		return this.fld_PagBoletin;
	}

	public void setFld_PagBoletin(short fld_PagBoletin) {
		this.fld_PagBoletin = fld_PagBoletin;
	}

	public short getFld_PagBoletinAcl() {
		return this.fld_PagBoletinAcl;
	}

	public void setFld_PagBoletinAcl(short fld_PagBoletinAcl) {
		this.fld_PagBoletinAcl = fld_PagBoletinAcl;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte getFld_TipoCert() {
		return this.fld_TipoCert;
	}

	public void setFld_TipoCert(byte fld_TipoCert) {
		this.fld_TipoCert = fld_TipoCert;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public byte getFld_TipoRelacion() {
		return this.fld_TipoRelacion;
	}

	public void setFld_TipoRelacion(byte fld_TipoRelacion) {
		this.fld_TipoRelacion = fld_TipoRelacion;
	}

}