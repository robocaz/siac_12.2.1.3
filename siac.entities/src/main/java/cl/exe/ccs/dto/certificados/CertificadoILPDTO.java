package cl.exe.ccs.dto.certificados;

import java.io.Serializable;

public class CertificadoILPDTO implements Serializable {

	private static final long serialVersionUID = -3393111956315518744L;

	private String rut;
	private int numBoletinProt;
	private int pagBoletinProt;
	private String fechaProt;
	private String tipoDocumento;
	private String codEmisor;
	private int numOperDig;
	private String glosaCorta;
	private String montoProt;
	private String ciudad;
	private String glosaEmisor;
	private String nombreLiberador;
	private String fechaPublicacion;
	private String tipoDocumentoImpago;

	public CertificadoILPDTO() {

	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public int getNumBoletinProt() {
		return numBoletinProt;
	}

	public void setNumBoletinProt(int numBoletinProt) {
		this.numBoletinProt = numBoletinProt;
	}

	public int getPagBoletinProt() {
		return pagBoletinProt;
	}

	public void setPagBoletinProt(int pagBoletinProt) {
		this.pagBoletinProt = pagBoletinProt;
	}

	public String getFechaProt() {
		return fechaProt;
	}

	public void setFechaProt(String fechaProt) {
		this.fechaProt = fechaProt;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getCodEmisor() {
		return codEmisor;
	}

	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}

	public int getNumOperDig() {
		return numOperDig;
	}

	public void setNumOperDig(int numOperDig) {
		this.numOperDig = numOperDig;
	}

	public String getGlosaCorta() {
		return glosaCorta;
	}

	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}

	public String getMontoProt() {
		return montoProt;
	}

	public void setMontoProt(String montoProt) {
		this.montoProt = montoProt;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getGlosaEmisor() {
		return glosaEmisor;
	}

	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}

	public String getNombreLiberador() {
		return nombreLiberador;
	}

	public void setNombreLiberador(String nombreLiberador) {
		this.nombreLiberador = nombreLiberador;
	}

	public String getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getTipoDocumentoImpago() {
		return tipoDocumentoImpago;
	}

	public void setTipoDocumentoImpago(String tipoDocumentoImpago) {
		this.tipoDocumentoImpago = tipoDocumentoImpago;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CertificadoILPDTO other = (CertificadoILPDTO) obj;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null) {
				return false;
			}
		} else if (!tipoDocumento.equals(other.tipoDocumento)) {
			return false;
		}
		return true;
	}

}
