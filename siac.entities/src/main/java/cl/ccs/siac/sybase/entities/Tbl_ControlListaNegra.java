package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlListaNegra database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlListaNegra.findAll", query="SELECT t FROM Tbl_ControlListaNegra t")
public class Tbl_ControlListaNegra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuarioAutoriza")
	private String fld_CodUsuarioAutoriza;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_FecAutorizacion")
	private Timestamp fld_FecAutorizacion;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tbl_ControlListaNegra() {
	}

	public String getFld_CodUsuarioAutoriza() {
		return this.fld_CodUsuarioAutoriza;
	}

	public void setFld_CodUsuarioAutoriza(String fld_CodUsuarioAutoriza) {
		this.fld_CodUsuarioAutoriza = fld_CodUsuarioAutoriza;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public Timestamp getFld_FecAutorizacion() {
		return this.fld_FecAutorizacion;
	}

	public void setFld_FecAutorizacion(Timestamp fld_FecAutorizacion) {
		this.fld_FecAutorizacion = fld_FecAutorizacion;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}