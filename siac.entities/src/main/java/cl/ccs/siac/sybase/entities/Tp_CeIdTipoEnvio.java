package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdTipoEnvio database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdTipoEnvio.findAll", query="SELECT t FROM Tp_CeIdTipoEnvio t")
public class Tp_CeIdTipoEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoEnvio")
	private String fld_GlosaTipoEnvio;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tp_CeIdTipoEnvio() {
	}

	public String getFld_GlosaTipoEnvio() {
		return this.fld_GlosaTipoEnvio;
	}

	public void setFld_GlosaTipoEnvio(String fld_GlosaTipoEnvio) {
		this.fld_GlosaTipoEnvio = fld_GlosaTipoEnvio;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}