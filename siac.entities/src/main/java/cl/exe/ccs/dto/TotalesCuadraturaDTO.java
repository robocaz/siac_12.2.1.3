package cl.exe.ccs.dto;

import java.io.Serializable;

public class TotalesCuadraturaDTO implements Serializable{
	
	
	
	private Integer totBrutoNumMovBoleta;
	private Double totBrutoTransaccionBoleta;
	private Double totNetoTransaccionBoleta;
	private Double totIvaTransaccionBoleta;
	private Double totBrutoCanceladoBoleta;
	private Double totNetoCanceladoBoleta;
	private Double totIvaCanceladoBoleta;
	private Double totBrutoDiferenciaBoleta;
	private Double totNetoDiferenciaBoleta;
	private Double totIvaDiferenciaBoleta;

	private Double totBrutoTransaccionFactura;
	private Double totNetoTransaccionFactura;
	private Double totIvaTransaccionFactura;
	private Double totBrutoCanceladoFactura;
	private Double totNetoCanceladoFactura;
	private Double totIvaCanceladoFactura;
	private Double totBrutoDiferenciaFactura;
	private Double totNetoDiferenciaFactura;
	private Double totIvaDiferenciaFactura;
	private Integer totBrutoNumMovFactura;
	
	
	
	public Integer getTotBrutoNumMovBoleta() {
		return totBrutoNumMovBoleta;
	}
	public void setTotBrutoNumMovBoleta(Integer totBrutoNumMovBoleta) {
		this.totBrutoNumMovBoleta = totBrutoNumMovBoleta;
	}
	public Double getTotBrutoTransaccionBoleta() {
		return totBrutoTransaccionBoleta;
	}
	public void setTotBrutoTransaccionBoleta(Double totBrutoTransaccionBoleta) {
		this.totBrutoTransaccionBoleta = totBrutoTransaccionBoleta;
	}
	public Double getTotNetoTransaccionBoleta() {
		return totNetoTransaccionBoleta;
	}
	public void setTotNetoTransaccionBoleta(Double totNetoTransaccionBoleta) {
		this.totNetoTransaccionBoleta = totNetoTransaccionBoleta;
	}
	public Double getTotIvaTransaccionBoleta() {
		return totIvaTransaccionBoleta;
	}
	public void setTotIvaTransaccionBoleta(Double totIvaTransaccionBoleta) {
		this.totIvaTransaccionBoleta = totIvaTransaccionBoleta;
	}
	public Double getTotBrutoCanceladoBoleta() {
		return totBrutoCanceladoBoleta;
	}
	public void setTotBrutoCanceladoBoleta(Double totBrutoCanceladoBoleta) {
		this.totBrutoCanceladoBoleta = totBrutoCanceladoBoleta;
	}
	public Double getTotNetoCanceladoBoleta() {
		return totNetoCanceladoBoleta;
	}
	public void setTotNetoCanceladoBoleta(Double totNetoCanceladoBoleta) {
		this.totNetoCanceladoBoleta = totNetoCanceladoBoleta;
	}
	public Double getTotIvaCanceladoBoleta() {
		return totIvaCanceladoBoleta;
	}
	public void setTotIvaCanceladoBoleta(Double totIvaCanceladoBoleta) {
		this.totIvaCanceladoBoleta = totIvaCanceladoBoleta;
	}
	public Double getTotBrutoDiferenciaBoleta() {
		return totBrutoDiferenciaBoleta;
	}
	public void setTotBrutoDiferenciaBoleta(Double totBrutoDiferenciaBoleta) {
		this.totBrutoDiferenciaBoleta = totBrutoDiferenciaBoleta;
	}
	public Double getTotNetoDiferenciaBoleta() {
		return totNetoDiferenciaBoleta;
	}
	public void setTotNetoDiferenciaBoleta(Double totNetoDiferenciaBoleta) {
		this.totNetoDiferenciaBoleta = totNetoDiferenciaBoleta;
	}
	public Double getTotIvaDiferenciaBoleta() {
		return totIvaDiferenciaBoleta;
	}
	public void setTotIvaDiferenciaBoleta(Double totIvaDiferenciaBoleta) {
		this.totIvaDiferenciaBoleta = totIvaDiferenciaBoleta;
	}
	public Double getTotBrutoTransaccionFactura() {
		return totBrutoTransaccionFactura;
	}
	public void setTotBrutoTransaccionFactura(Double totBrutoTransaccionFactura) {
		this.totBrutoTransaccionFactura = totBrutoTransaccionFactura;
	}
	public Double getTotNetoTransaccionFactura() {
		return totNetoTransaccionFactura;
	}
	public void setTotNetoTransaccionFactura(Double totNetoTransaccionFactura) {
		this.totNetoTransaccionFactura = totNetoTransaccionFactura;
	}
	public Double getTotIvaTransaccionFactura() {
		return totIvaTransaccionFactura;
	}
	public void setTotIvaTransaccionFactura(Double totIvaTransaccionFactura) {
		this.totIvaTransaccionFactura = totIvaTransaccionFactura;
	}
	public Double getTotBrutoCanceladoFactura() {
		return totBrutoCanceladoFactura;
	}
	public void setTotBrutoCanceladoFactura(Double totBrutoCanceladoFactura) {
		this.totBrutoCanceladoFactura = totBrutoCanceladoFactura;
	}
	public Double getTotNetoCanceladoFactura() {
		return totNetoCanceladoFactura;
	}
	public void setTotNetoCanceladoFactura(Double totNetoCanceladoFactura) {
		this.totNetoCanceladoFactura = totNetoCanceladoFactura;
	}
	public Double getTotIvaCanceladoFactura() {
		return totIvaCanceladoFactura;
	}
	public void setTotIvaCanceladoFactura(Double totIvaCanceladoFactura) {
		this.totIvaCanceladoFactura = totIvaCanceladoFactura;
	}
	public Double getTotBrutoDiferenciaFactura() {
		return totBrutoDiferenciaFactura;
	}
	public void setTotBrutoDiferenciaFactura(Double totBrutoDiferenciaFactura) {
		this.totBrutoDiferenciaFactura = totBrutoDiferenciaFactura;
	}
	public Double getTotNetoDiferenciaFactura() {
		return totNetoDiferenciaFactura;
	}
	public void setTotNetoDiferenciaFactura(Double totNetoDiferenciaFactura) {
		this.totNetoDiferenciaFactura = totNetoDiferenciaFactura;
	}
	public Double getTotIvaDiferenciaFactura() {
		return totIvaDiferenciaFactura;
	}
	public void setTotIvaDiferenciaFactura(Double totIvaDiferenciaFactura) {
		this.totIvaDiferenciaFactura = totIvaDiferenciaFactura;
	}
	public Integer getTotBrutoNumMovFactura() {
		return totBrutoNumMovFactura;
	}
	public void setTotBrutoNumMovFactura(Integer totBrutoNumMovFactura) {
		this.totBrutoNumMovFactura = totBrutoNumMovFactura;
	}
	
	
	
	
	


}
