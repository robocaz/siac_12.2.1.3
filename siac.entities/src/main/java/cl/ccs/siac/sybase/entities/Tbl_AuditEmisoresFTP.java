package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditEmisoresFTP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditEmisoresFTP.findAll", query="SELECT t FROM Tbl_AuditEmisoresFTP t")
public class Tbl_AuditEmisoresFTP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_NombreArchivo")
	private String fld_NombreArchivo;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoProceso")
	private String fld_TipoProceso;

	public Tbl_AuditEmisoresFTP() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public String getFld_NombreArchivo() {
		return this.fld_NombreArchivo;
	}

	public void setFld_NombreArchivo(String fld_NombreArchivo) {
		this.fld_NombreArchivo = fld_NombreArchivo;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_TipoProceso() {
		return this.fld_TipoProceso;
	}

	public void setFld_TipoProceso(String fld_TipoProceso) {
		this.fld_TipoProceso = fld_TipoProceso;
	}

}