package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_ley database table.
 * 
 */
@Entity
@Table(name="tmp_ley")
@NamedQuery(name="TmpLey.findAll", query="SELECT t FROM TmpLey t")
public class TmpLey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CantBIC")
	private short cantBIC;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="SumaBIC")
	private BigDecimal sumaBIC;

	public TmpLey() {
	}

	public short getCantBIC() {
		return this.cantBIC;
	}

	public void setCantBIC(short cantBIC) {
		this.cantBIC = cantBIC;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public BigDecimal getSumaBIC() {
		return this.sumaBIC;
	}

	public void setSumaBIC(BigDecimal sumaBIC) {
		this.sumaBIC = sumaBIC;
	}

}