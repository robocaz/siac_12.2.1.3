angular.module('ccs').controller('TarifasController', function($scope, TarifaResource, $rootScope, $location) {

    $scope.tarifas = [];
    
     
    $scope.cargaTarifas = function() {

        var successCallback = function(data,responseHeaders){
        	
        	console.log(data);
        	$scope.tarifas = data;
        };
        var errorCallback2 = function(response) {
            $scope.error = "Servicio de Aplicación no se encuentra disponible";
        };
        TarifaResource.cargaTarifas(successCallback, errorCallback2);
    };

});
