package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_LibradorProtesto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LibradorProtesto.findAll", query="SELECT t FROM Tbl_LibradorProtesto t")
public class Tbl_LibradorProtesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrLibradorAut")
	private BigDecimal fld_CorrLibradorAut;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	public Tbl_LibradorProtesto() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrLibradorAut() {
		return this.fld_CorrLibradorAut;
	}

	public void setFld_CorrLibradorAut(BigDecimal fld_CorrLibradorAut) {
		this.fld_CorrLibradorAut = fld_CorrLibradorAut;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

}