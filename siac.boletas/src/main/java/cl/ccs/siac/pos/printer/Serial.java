package cl.ccs.siac.pos.printer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;

public class Serial
{
	static String errorString;
	static int comportLive[] = { 0, 0, 0 , 0, 0 };
	static String dataread;
	// 
	// InitialiseSerialPort()
	//
	// Initialises the serial port before use
	//
	// Inputs (int comport, int baudrate, char parity (Even,Odd,None), int stopbits
	// Returns true if ok, false if failure
	public static Boolean InitialiseSerialPort(int comport, int baud, char parity, int stopbits)
	{
		Boolean result = true;
		
		if (comport <= 0 || comport > 4)
		{
			result = false;
			errorString = "Comport (" + comport + ") out of range";
			return(result);
		}
		if (!((baud == 9600) || (baud == 19200)))
		{
			result = false;
			errorString = "Baudrade is not 9600 or 19200 (" + baud + ") ";
			return(result);
		}
		if (!(parity == 'E' || parity == 'O' || parity == 'N'))
		{
			result = false;
			errorString = "Parity is not E, O, N (" + parity + ") ";
			return(result);
		}
		if (!(stopbits == 0 || stopbits == 1))
		{
			result = false;
			errorString = "Stopnits must be 0 or 1 (" + stopbits + ") ";
			return(result);
		}
		comportLive[comport] = 1;
		return result;
	}
	
	//
	// OpenSerialPort
	//
	// Inputs(int comport)
	// Outputs (Boolean success or fail)
	//
	public static Boolean OpenSerialPort(int comport)
	{
		Boolean result = true;
		if (comportLive[comport] != 1)
		{
			result = false;
			errorString = "OpenSerialPort: port not initialised";
			return(result);
			
		}
		return(result);
	}

	//
	// ReadSerialPort
	//
	// Inputs(int comport, length)
	// Outputs (true if written ok, false if not written ok)
	//
	// note the read value is in the string serial.datalength
	//
	public static Boolean ReadSerialPort(int comport, int length)
	{
		double temp;	  
		Boolean result = true;
		
		if (comportLive[comport] != 1)
		{
			result = false;
			errorString = "ReadSerialPort: port not initialised";
			return(result);
			
		}
		
		temp = readTemperature(); 
		temp = round(temp, 2); 
		dataread = " " + temp;
		
		return(result);
	}
	
	//
	// WriteSerialPort
	//
	// Inputs(int comport, String dataToWrite)
	// Outputs (true if written ok, false if not written ok)
	//
	public static Boolean WriteSerialPort(int comport, String dataToWrite)
	{
		Boolean result = true;
		
		if (comportLive[comport] != 1)
		{
			result = false;
			errorString = "ReadSerialPort: port not initialised";
			return(result);
			
		}
		
		try{
			// Create file 
			FileWriter fstream = new FileWriter("COM1");
			
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(dataToWrite);	// write out data
			
			//Close the output stream
			out.close();
		}  
		catch (Exception e)
		{//Catch exception if any
			System.err.println("Write Error: " + e.getMessage());
		}		
		
		return(result);
	}
	
	//
	// CloseSerialPort
	//
	// Inputs(int comport)
	// Outputs (Boolean success or fail) - 
	// needs to be initialised again if closed
	//
	public static Boolean CloseSerialPort(int comport)
	{
		Boolean result = true;
		if (comportLive[comport] != 1)
		{
			result = false;
			errorString = "CloseSerialPort: port not initialised";
			return(result);
			
		}
		comportLive[comport] = 0;
		return(result);
	}
	
	private static double readTemperature() 
	{ 
		// this simulates reading a temperature from a device 
		Random generator = new Random(); 
		double r = generator.nextDouble(); // a random number between 0 and 1 
		double s = generator.nextDouble(); // a random number between 0 and 1 
		return(-10 + (s*20) + r*2); 
	} 
	
	/** * Round a double value to a specified number of decimal * places.
	* 
	* @param val the value to be rounded.
	* @param places the number of decimal places to round to.
	* @return val rounded to places decimal places. 
	*/ 
	public static double round(double val, int places) 
	{ 
		long factor = (long)Math.pow(10,places); // Shift the decimal the correct number of places 
		// to the right. 
		val = val * factor; // Round to the nearest integer. 
		long tmp = Math.round(val); // Shift the decimal the correct number of places 
		// back to the left. 
		return (double)tmp / factor; 
	} 
}
