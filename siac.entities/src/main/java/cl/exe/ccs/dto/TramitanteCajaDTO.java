package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class TramitanteCajaDTO extends ResponseDTO implements Serializable{
    
    private String rutTramitante;
    private Integer corrTramitante;
    private String rutRequirente;
    private Integer corrRequirente;
    private Long corrCaja;
    private Boolean tieneDeclaracionUso;
    private Integer codMotivo;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombre;
    
    
    public TramitanteCajaDTO(){}
    
    public TramitanteCajaDTO(Long corrCaja, String rutTramitante, Boolean tieneDeclaracionUso){
    	this.corrCaja = corrCaja;
    	this.rutTramitante = rutTramitante;
    	this.tieneDeclaracionUso = tieneDeclaracionUso;
    	this.rutRequirente = "000000000-0";
    	this.codMotivo = 0;
    }
    
    public TramitanteCajaDTO(Long corrCaja, String rutTramitante, Boolean tieneDeclaracionUso, String rutRequirente, Integer codMotivo){
    	this.corrCaja = corrCaja;
    	this.rutTramitante = rutTramitante;
    	this.tieneDeclaracionUso = tieneDeclaracionUso;
    	this.rutRequirente = rutRequirente;
    	this.codMotivo = codMotivo;
    }
    
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public String getRutRequirente() {
		return rutRequirente;
	}
	public void setRutRequirente(String rutRequirente) {
		this.rutRequirente = rutRequirente;
	}
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Boolean getTieneDeclaracionUso() {
		return tieneDeclaracionUso;
	}
	public void setTieneDeclaracionUso(Boolean tieneDeclaracionUso) {
		this.tieneDeclaracionUso = tieneDeclaracionUso;
	}
	public Integer getCodMotivo() {
		return codMotivo;
	}
	public void setCodMotivo(Integer codMotivo) {
		this.codMotivo = codMotivo;
	}

	public Integer getCorrTramitante() {
		return corrTramitante;
	}

	public void setCorrTramitante(Integer corrTramitante) {
		this.corrTramitante = corrTramitante;
	}

	public Integer getCorrRequirente() {
		return corrRequirente;
	}

	public void setCorrRequirente(Integer corrRequirente) {
		this.corrRequirente = corrRequirente;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
         
}  

