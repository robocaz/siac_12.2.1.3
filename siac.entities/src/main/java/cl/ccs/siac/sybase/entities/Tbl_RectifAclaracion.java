package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RectifAclaracion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RectifAclaracion.findAll", query="SELECT t FROM Tbl_RectifAclaracion t")
public class Tbl_RectifAclaracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrSolic")
	private BigDecimal fld_CorrSolic;

	@Column(name="Fld_CorrUnico_Id")
	private BigDecimal fld_CorrUnico_Id;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_PagBoletinAcl")
	private short fld_PagBoletinAcl;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	public Tbl_RectifAclaracion() {
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrSolic() {
		return this.fld_CorrSolic;
	}

	public void setFld_CorrSolic(BigDecimal fld_CorrSolic) {
		this.fld_CorrSolic = fld_CorrSolic;
	}

	public BigDecimal getFld_CorrUnico_Id() {
		return this.fld_CorrUnico_Id;
	}

	public void setFld_CorrUnico_Id(BigDecimal fld_CorrUnico_Id) {
		this.fld_CorrUnico_Id = fld_CorrUnico_Id;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public short getFld_PagBoletinAcl() {
		return this.fld_PagBoletinAcl;
	}

	public void setFld_PagBoletinAcl(short fld_PagBoletinAcl) {
		this.fld_PagBoletinAcl = fld_PagBoletinAcl;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

}