package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EstadosOrdenProceso database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadosOrdenProceso.findAll", query="SELECT t FROM Tbl_EstadosOrdenProceso t")
public class Tbl_EstadosOrdenProceso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_GlosaEstado")
	private String fld_GlosaEstado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_EstadosOrdenProceso() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_GlosaEstado() {
		return this.fld_GlosaEstado;
	}

	public void setFld_GlosaEstado(String fld_GlosaEstado) {
		this.fld_GlosaEstado = fld_GlosaEstado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}