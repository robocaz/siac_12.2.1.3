package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the rs_lastcommit database table.
 * 
 */
@Entity
@Table(name="rs_lastcommit")
@NamedQuery(name="RsLastcommit.findAll", query="SELECT r FROM RsLastcommit r")
public class RsLastcommit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="conn_id")
	private int connId;

	@Column(name="dest_commit_time")
	private Timestamp destCommitTime;

	private int origin;

	@Column(name="origin_qid")
	private byte[] originQid;

	@Column(name="origin_time")
	private Timestamp originTime;

	private byte[] pad1;

	private byte[] pad2;

	private byte[] pad3;

	private byte[] pad4;

	private byte[] pad5;

	private byte[] pad6;

	private byte[] pad7;

	private byte[] pad8;

	@Column(name="secondary_qid")
	private byte[] secondaryQid;

	public RsLastcommit() {
	}

	public int getConnId() {
		return this.connId;
	}

	public void setConnId(int connId) {
		this.connId = connId;
	}

	public Timestamp getDestCommitTime() {
		return this.destCommitTime;
	}

	public void setDestCommitTime(Timestamp destCommitTime) {
		this.destCommitTime = destCommitTime;
	}

	public int getOrigin() {
		return this.origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	public byte[] getOriginQid() {
		return this.originQid;
	}

	public void setOriginQid(byte[] originQid) {
		this.originQid = originQid;
	}

	public Timestamp getOriginTime() {
		return this.originTime;
	}

	public void setOriginTime(Timestamp originTime) {
		this.originTime = originTime;
	}

	public byte[] getPad1() {
		return this.pad1;
	}

	public void setPad1(byte[] pad1) {
		this.pad1 = pad1;
	}

	public byte[] getPad2() {
		return this.pad2;
	}

	public void setPad2(byte[] pad2) {
		this.pad2 = pad2;
	}

	public byte[] getPad3() {
		return this.pad3;
	}

	public void setPad3(byte[] pad3) {
		this.pad3 = pad3;
	}

	public byte[] getPad4() {
		return this.pad4;
	}

	public void setPad4(byte[] pad4) {
		this.pad4 = pad4;
	}

	public byte[] getPad5() {
		return this.pad5;
	}

	public void setPad5(byte[] pad5) {
		this.pad5 = pad5;
	}

	public byte[] getPad6() {
		return this.pad6;
	}

	public void setPad6(byte[] pad6) {
		this.pad6 = pad6;
	}

	public byte[] getPad7() {
		return this.pad7;
	}

	public void setPad7(byte[] pad7) {
		this.pad7 = pad7;
	}

	public byte[] getPad8() {
		return this.pad8;
	}

	public void setPad8(byte[] pad8) {
		this.pad8 = pad8;
	}

	public byte[] getSecondaryQid() {
		return this.secondaryQid;
	}

	public void setSecondaryQid(byte[] secondaryQid) {
		this.secondaryQid = secondaryQid;
	}

}