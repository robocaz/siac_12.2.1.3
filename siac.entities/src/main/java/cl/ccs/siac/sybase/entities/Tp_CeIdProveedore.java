package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdProveedores database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdProveedores")
@NamedQuery(name="Tp_CeIdProveedore.findAll", query="SELECT t FROM Tp_CeIdProveedore t")
public class Tp_CeIdProveedore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProveedor")
	private short fld_CodProveedor;

	@Column(name="Fld_GlosaProveedor")
	private String fld_GlosaProveedor;

	public Tp_CeIdProveedore() {
	}

	public short getFld_CodProveedor() {
		return this.fld_CodProveedor;
	}

	public void setFld_CodProveedor(short fld_CodProveedor) {
		this.fld_CodProveedor = fld_CodProveedor;
	}

	public String getFld_GlosaProveedor() {
		return this.fld_GlosaProveedor;
	}

	public void setFld_GlosaProveedor(String fld_GlosaProveedor) {
		this.fld_GlosaProveedor = fld_GlosaProveedor;
	}

}