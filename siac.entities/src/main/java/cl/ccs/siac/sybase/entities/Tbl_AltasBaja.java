package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AltasBajas database table.
 * 
 */
@Entity
@Table(name="Tbl_AltasBajas")
@NamedQuery(name="Tbl_AltasBaja.findAll", query="SELECT t FROM Tbl_AltasBaja t")
public class Tbl_AltasBaja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecTerminoContrato")
	private Timestamp fld_FecTerminoContrato;

	@Column(name="Fld_Movimiento")
	private String fld_Movimiento;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_AltasBaja() {
	}

	public Timestamp getFld_FecTerminoContrato() {
		return this.fld_FecTerminoContrato;
	}

	public void setFld_FecTerminoContrato(Timestamp fld_FecTerminoContrato) {
		this.fld_FecTerminoContrato = fld_FecTerminoContrato;
	}

	public String getFld_Movimiento() {
		return this.fld_Movimiento;
	}

	public void setFld_Movimiento(String fld_Movimiento) {
		this.fld_Movimiento = fld_Movimiento;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}