package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProtestoDetalleDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 2161005227524714867L;

	private List<String[]> lista;
	private String glosaTipoDocumento;
	private String glosaCorta;
	private BigDecimal montoProt;
	private String fecProt;
	private String nroOperacion;
	private String glosaCausalProt;
	private String glosaMarcaAclOmi;
	private String fecPublicacion;
	private Integer pagBoletin;
	private Integer nroBoletin;
	private String glosaTipoEmisor;
	private String glosaEmisor;
	private String glosaSucursal;
	private String fecModificacion;
	private String codUsuarioModif;
	private String fecDigitacion;
	private String codUsuarioDigitacion;
	private String fecVcto;
	private String glosaTipoDocumentoImpago;
	private Integer nroOper4Dig;
	private String glosaCortaAlias;
	private Boolean marcaAclRectificacion;
	private String fecMarcaAclEspecial;
	private String fecMarcaAclaracion;
	private String glosaTipoRelacion;
	private BigDecimal montoProtOriginal;
	private String glosaEstado;
	private BigDecimal corrEnvio;
	private Boolean marcaAclEspAnulada;
	private String fecMarcaAclEspAnulada;
	private String fecMarcaAclRectificacion;
	private Boolean marcaAclEspecial;
	private Double nroLote;
	private Integer nroBoletinAcl;
	private Integer pagBoletinAcl;
	private String glosaLocPub;
	private String nombreLibrador;
	
	public List<String[]> getLista() {
		return lista;
	}

	public void setLista(List<String[]> lista) {
		this.lista = lista;
	}

	public String getGlosaTipoDocumento() {
		return glosaTipoDocumento;
	}

	public void setGlosaTipoDocumento(String glosaTipoDocumento) {
		this.glosaTipoDocumento = glosaTipoDocumento;
	}

	public String getGlosaCorta() {
		return glosaCorta;
	}

	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}

	public BigDecimal getMontoProt() {
		return montoProt;
	}

	public void setMontoProt(BigDecimal montoProt) {
		this.montoProt = montoProt;
	}

	public String getFecProt() {
		return fecProt;
	}

	public void setFecProt(String fecProt) {
		this.fecProt = fecProt;
	}

	public String getNroOperacion() {
		return nroOperacion;
	}

	public void setNroOperacion(String nroOperacion) {
		this.nroOperacion = nroOperacion;
	}

	public String getGlosaCausalProt() {
		return glosaCausalProt;
	}

	public void setGlosaCausalProt(String glosaCausalProt) {
		this.glosaCausalProt = glosaCausalProt;
	}

	public String getGlosaMarcaAclOmi() {
		return glosaMarcaAclOmi;
	}

	public void setGlosaMarcaAclOmi(String glosaMarcaAclOmi) {
		this.glosaMarcaAclOmi = glosaMarcaAclOmi;
	}

	public String getFecPublicacion() {
		return fecPublicacion;
	}

	public void setFecPublicacion(String fecPublicacion) {
		this.fecPublicacion = fecPublicacion;
	}

	public Integer getPagBoletin() {
		return pagBoletin;
	}

	public void setPagBoletin(Integer pagBoletin) {
		this.pagBoletin = pagBoletin;
	}

	public Integer getNroBoletin() {
		return nroBoletin;
	}

	public void setNroBoletin(Integer nroBoletin) {
		this.nroBoletin = nroBoletin;
	}

	public String getGlosaTipoEmisor() {
		return glosaTipoEmisor;
	}

	public void setGlosaTipoEmisor(String glosaTipoEmisor) {
		this.glosaTipoEmisor = glosaTipoEmisor;
	}

	public String getGlosaEmisor() {
		return glosaEmisor;
	}

	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}

	public String getGlosaSucursal() {
		return glosaSucursal;
	}

	public void setGlosaSucursal(String glosaSucursal) {
		this.glosaSucursal = glosaSucursal;
	}

	public String getFecModificacion() {
		return fecModificacion;
	}

	public void setFecModificacion(String fecModificacion) {
		this.fecModificacion = fecModificacion;
	}

	public String getCodUsuarioModif() {
		return codUsuarioModif;
	}

	public void setCodUsuarioModif(String codUsuarioModif) {
		this.codUsuarioModif = codUsuarioModif;
	}

	public String getFecDigitacion() {
		return fecDigitacion;
	}

	public void setFecDigitacion(String fecDigitacion) {
		this.fecDigitacion = fecDigitacion;
	}

	public String getCodUsuarioDigitacion() {
		return codUsuarioDigitacion;
	}

	public void setCodUsuarioDigitacion(String codUsuarioDigitacion) {
		this.codUsuarioDigitacion = codUsuarioDigitacion;
	}

	public String getFecVcto() {
		return fecVcto;
	}

	public void setFecVcto(String fecVcto) {
		this.fecVcto = fecVcto;
	}

	public String getGlosaTipoDocumentoImpago() {
		return glosaTipoDocumentoImpago;
	}

	public void setGlosaTipoDocumentoImpago(String glosaTipoDocumentoImpago) {
		this.glosaTipoDocumentoImpago = glosaTipoDocumentoImpago;
	}

	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}

	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}

	public String getGlosaCortaAlias() {
		return glosaCortaAlias;
	}

	public void setGlosaCortaAlias(String glosaCortaAlias) {
		this.glosaCortaAlias = glosaCortaAlias;
	}

	public Boolean getMarcaAclRectificacion() {
		return marcaAclRectificacion;
	}

	public void setMarcaAclRectificacion(Boolean marcaAclRectificacion) {
		this.marcaAclRectificacion = marcaAclRectificacion;
	}

	public String getFecMarcaAclEspecial() {
		return fecMarcaAclEspecial;
	}

	public void setFecMarcaAclEspecial(String fecMarcaAclEspecial) {
		this.fecMarcaAclEspecial = fecMarcaAclEspecial;
	}

	public String getFecMarcaAclaracion() {
		return fecMarcaAclaracion;
	}

	public void setFecMarcaAclaracion(String fecMarcaAclaracion) {
		this.fecMarcaAclaracion = fecMarcaAclaracion;
	}

	public String getGlosaTipoRelacion() {
		return glosaTipoRelacion;
	}

	public void setGlosaTipoRelacion(String glosaTipoRelacion) {
		this.glosaTipoRelacion = glosaTipoRelacion;
	}

	public BigDecimal getMontoProtOriginal() {
		return montoProtOriginal;
	}

	public void setMontoProtOriginal(BigDecimal montoProtOriginal) {
		this.montoProtOriginal = montoProtOriginal;
	}

	public String getGlosaEstado() {
		return glosaEstado;
	}

	public void setGlosaEstado(String glosaEstado) {
		this.glosaEstado = glosaEstado;
	}

	public BigDecimal getCorrEnvio() {
		return corrEnvio;
	}

	public void setCorrEnvio(BigDecimal corrEnvio) {
		this.corrEnvio = corrEnvio;
	}

	public Boolean getMarcaAclEspAnulada() {
		return marcaAclEspAnulada;
	}

	public void setMarcaAclEspAnulada(Boolean marcaAclEspAnulada) {
		this.marcaAclEspAnulada = marcaAclEspAnulada;
	}

	public String getFecMarcaAclEspAnulada() {
		return fecMarcaAclEspAnulada;
	}

	public void setFecMarcaAclEspAnulada(String fecMarcaAclEspAnulada) {
		this.fecMarcaAclEspAnulada = fecMarcaAclEspAnulada;
	}

	public String getFecMarcaAclRectificacion() {
		return fecMarcaAclRectificacion;
	}

	public void setFecMarcaAclRectificacion(String fecMarcaAclRectificacion) {
		this.fecMarcaAclRectificacion = fecMarcaAclRectificacion;
	}

	public Boolean getMarcaAclEspecial() {
		return marcaAclEspecial;
	}

	public void setMarcaAclEspecial(Boolean marcaAclEspecial) {
		this.marcaAclEspecial = marcaAclEspecial;
	}

	public Double getNroLote() {
		return nroLote;
	}

	public void setNroLote(Double nroLote) {
		this.nroLote = nroLote;
	}

	public Integer getNroBoletinAcl() {
		return nroBoletinAcl;
	}

	public void setNroBoletinAcl(Integer nroBoletinAcl) {
		this.nroBoletinAcl = nroBoletinAcl;
	}

	public Integer getPagBoletinAcl() {
		return pagBoletinAcl;
	}

	public void setPagBoletinAcl(Integer pagBoletinAcl) {
		this.pagBoletinAcl = pagBoletinAcl;
	}

	public String getGlosaLocPub() {
		return glosaLocPub;
	}

	public void setGlosaLocPub(String glosaLocPub) {
		this.glosaLocPub = glosaLocPub;
	}

	public String getNombreLibrador() {
		return nombreLibrador;
	}

	public void setNombreLibrador(String nombreLibrador) {
		this.nombreLibrador = nombreLibrador;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n glosaTipoDocumento: " + glosaTipoDocumento);
		str.append(", glosaCorta: " + glosaCorta);
		str.append(", montoProt: " + montoProt);
		str.append(", fecProt: " + fecProt);
		str.append(", nroOperacion: " + nroOperacion);
		str.append(", glosaCausalProt: " + glosaCausalProt);
		str.append(", glosaMarcaAclOmi: " + glosaMarcaAclOmi);
		str.append(", fecPublicacion: " + fecPublicacion);
		str.append(", pagBoletin: " + pagBoletin);
		str.append(", nroBoletin: " + nroBoletin);
		str.append(", glosaTipoEmisor: " + glosaTipoEmisor);
		str.append(", glosaEmisor: " + glosaEmisor);
		str.append(", glosaSucursal: " + glosaSucursal);
		str.append(", fecModificacion: " + fecModificacion);
		str.append(", codUsuarioModif: " + codUsuarioModif);
		str.append(", fecDigitacion: " + fecDigitacion);
		str.append(", codUsuarioDigitacion: " + codUsuarioDigitacion);
		str.append(", fecVcto: " + fecVcto);
		str.append(", glosaTipoDocumentoImpago: " + glosaTipoDocumentoImpago);
		str.append(", nroOper4Dig: " + nroOper4Dig);
		str.append(", glosaCortaAlias: " + glosaCortaAlias);
		str.append(", marcaAclRectificacion: " + marcaAclRectificacion);
		str.append(", fecMarcaAclEspecial: " + fecMarcaAclEspecial);
		str.append(", fecMarcaAclaracion: " + fecMarcaAclaracion);
		str.append(", glosaTipoRelacion: " + glosaTipoRelacion);
		str.append(", montoProtOriginal: " + montoProtOriginal);
		str.append(", glosaEstado: " + glosaEstado);
		str.append(", corrEnvio: " + corrEnvio);
		str.append(", marcaAclEspAnulada: " + marcaAclEspAnulada);
		str.append(", fecMarcaAclEspAnulada: " + fecMarcaAclEspAnulada);
		str.append(", fecMarcaAclRectificacion: " + fecMarcaAclRectificacion);
		str.append(", marcaAclEspecial: " + marcaAclEspecial);
		str.append(", nroLote: " + nroLote);
		str.append(", nroBoletinAcl: " + nroBoletinAcl);
		str.append(", pagBoletinAcl: " + pagBoletinAcl);
		str.append(", glosaLocPub: " + glosaLocPub);
		str.append(", nombreLibrador: " + nombreLibrador);
		return str.toString();
	}

}
