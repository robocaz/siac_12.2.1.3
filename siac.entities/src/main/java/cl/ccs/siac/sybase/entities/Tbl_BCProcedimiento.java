package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCProcedimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCProcedimiento.findAll", query="SELECT t FROM Tbl_BCProcedimiento t")
public class Tbl_BCProcedimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodTribunal")
	private String fld_CodTribunal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProcedimiento_Id")
	private BigDecimal fld_CorrProcedimiento_Id;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FechaCreacion")
	private Timestamp fld_FechaCreacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_RolCausa")
	private String fld_RolCausa;

	@Column(name="Fld_RutArbitroS")
	private String fld_RutArbitroS;

	@Column(name="Fld_RutArbitroT")
	private String fld_RutArbitroT;

	@Column(name="Fld_RutContador")
	private String fld_RutContador;

	@Column(name="Fld_RutDeudor")
	private String fld_RutDeudor;

	@Column(name="Fld_RutResponsableProcS")
	private String fld_RutResponsableProcS;

	@Column(name="Fld_RutResponsableProcT")
	private String fld_RutResponsableProcT;

	@Column(name="Fld_TipoProcedimiento")
	private byte fld_TipoProcedimiento;

	@Column(name="Fld_TipoResponsableProcS")
	private String fld_TipoResponsableProcS;

	@Column(name="Fld_TipoResponsableProcT")
	private String fld_TipoResponsableProcT;

	public Tbl_BCProcedimiento() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_CodTribunal() {
		return this.fld_CodTribunal;
	}

	public void setFld_CodTribunal(String fld_CodTribunal) {
		this.fld_CodTribunal = fld_CodTribunal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProcedimiento_Id() {
		return this.fld_CorrProcedimiento_Id;
	}

	public void setFld_CorrProcedimiento_Id(BigDecimal fld_CorrProcedimiento_Id) {
		this.fld_CorrProcedimiento_Id = fld_CorrProcedimiento_Id;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FechaCreacion() {
		return this.fld_FechaCreacion;
	}

	public void setFld_FechaCreacion(Timestamp fld_FechaCreacion) {
		this.fld_FechaCreacion = fld_FechaCreacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public String getFld_RolCausa() {
		return this.fld_RolCausa;
	}

	public void setFld_RolCausa(String fld_RolCausa) {
		this.fld_RolCausa = fld_RolCausa;
	}

	public String getFld_RutArbitroS() {
		return this.fld_RutArbitroS;
	}

	public void setFld_RutArbitroS(String fld_RutArbitroS) {
		this.fld_RutArbitroS = fld_RutArbitroS;
	}

	public String getFld_RutArbitroT() {
		return this.fld_RutArbitroT;
	}

	public void setFld_RutArbitroT(String fld_RutArbitroT) {
		this.fld_RutArbitroT = fld_RutArbitroT;
	}

	public String getFld_RutContador() {
		return this.fld_RutContador;
	}

	public void setFld_RutContador(String fld_RutContador) {
		this.fld_RutContador = fld_RutContador;
	}

	public String getFld_RutDeudor() {
		return this.fld_RutDeudor;
	}

	public void setFld_RutDeudor(String fld_RutDeudor) {
		this.fld_RutDeudor = fld_RutDeudor;
	}

	public String getFld_RutResponsableProcS() {
		return this.fld_RutResponsableProcS;
	}

	public void setFld_RutResponsableProcS(String fld_RutResponsableProcS) {
		this.fld_RutResponsableProcS = fld_RutResponsableProcS;
	}

	public String getFld_RutResponsableProcT() {
		return this.fld_RutResponsableProcT;
	}

	public void setFld_RutResponsableProcT(String fld_RutResponsableProcT) {
		this.fld_RutResponsableProcT = fld_RutResponsableProcT;
	}

	public byte getFld_TipoProcedimiento() {
		return this.fld_TipoProcedimiento;
	}

	public void setFld_TipoProcedimiento(byte fld_TipoProcedimiento) {
		this.fld_TipoProcedimiento = fld_TipoProcedimiento;
	}

	public String getFld_TipoResponsableProcS() {
		return this.fld_TipoResponsableProcS;
	}

	public void setFld_TipoResponsableProcS(String fld_TipoResponsableProcS) {
		this.fld_TipoResponsableProcS = fld_TipoResponsableProcS;
	}

	public String getFld_TipoResponsableProcT() {
		return this.fld_TipoResponsableProcT;
	}

	public void setFld_TipoResponsableProcT(String fld_TipoResponsableProcT) {
		this.fld_TipoResponsableProcT = fld_TipoResponsableProcT;
	}

}