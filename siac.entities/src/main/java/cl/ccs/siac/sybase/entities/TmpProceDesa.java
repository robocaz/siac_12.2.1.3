package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_proce_desa database table.
 * 
 */
@Entity
@Table(name="tmp_proce_desa")
@NamedQuery(name="TmpProceDesa.findAll", query="SELECT t FROM TmpProceDesa t")
public class TmpProceDesa implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;

	public TmpProceDesa() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}