package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysgams database table.
 * 
 */
@Entity
@Table(name="sysgams")
@NamedQuery(name="Sysgam.findAll", query="SELECT s FROM Sysgam s")
public class Sysgam implements Serializable {
	private static final long serialVersionUID = 1L;

	public Sysgam() {
	}

}