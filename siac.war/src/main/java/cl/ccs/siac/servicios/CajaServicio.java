package cl.ccs.siac.servicios;

import java.security.Principal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.caja.CajaBusiness;
import cl.exe.ccs.dto.CajaDTO;
import cl.exe.ccs.dto.CajaPendienteDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Path("/caja")
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class CajaServicio {

	private static final Logger log = LogManager.getLogger(CajaServicio.class);

	@Inject
	private CajaBusiness cajaBusiness;

	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/cantidadabiertas")
	public Response cantidadAbiertas(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			CajaDTO cajaDTO = cajaBusiness.cantidadAbiertas(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());
			//CajaDTO cajaDTO = cajaBusiness.numeroCajasAbiertas(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());
 			if (cajaDTO.getIdControl() == 7) {
 				usuarioDTO.setCaja(cajaDTO.getCorrCaja());
				//usuarioDTO.setCaja("0");
				request.getSession().setAttribute("usuario", usuarioDTO);
			}
			log.info("Cantidad Cajas Abiertas, Usuario  " + userPrincipal.getName() + "  - Message: " + cajaDTO.getMsgControl() + " - Codigo " + cajaDTO.getIdControl());
			return Response.ok(cajaDTO).build();

		} catch (Exception ex) {
			log.error("Cantidad Cajas Abiertas, Usuario  " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/cierracaja")
	public Response cierraCaja(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			//CajaDTO cajaDTO = cajaBusiness.cantidadAbiertas(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());
			CajaDTO cajaDTO = cajaBusiness.numeroCajasAbiertas(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());
			if (cajaDTO.getIdControl() == 7) {
				//usuarioDTO.setCaja(cajaDTO.getCorrCaja());
				usuarioDTO.setCaja("0");
			}
			log.info("Cantidad Cajas Abiertas, Usuario  " + userPrincipal.getName() + "  - Message: " + cajaDTO.getMsgControl() + " - Codigo " + cajaDTO.getIdControl());
			return Response.ok(cajaDTO).build();

		} catch (Exception ex) {
			log.error("Cantidad Cajas Abiertas, Usuario  " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cajasabiertas")
	public Response listarCajasAbiertas(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		try {

			

			ResponseListDTO<CajaDTO> respuesta = cajaBusiness.listarCajasAbiertas(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());

			GenericEntity<List<CajaDTO>> list = new GenericEntity<List<CajaDTO>>(respuesta.getLista()) {};

			log.info("Listar Cajas Abiertas, Usuario " + request.getUserPrincipal() + "  - Message: "+ respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
			return Response.ok(list).build();

		} catch (Exception ex) {
			log.error("Listar Cajas Abiertas, Usuario " + request.getUserPrincipal() + "Centro de costo:  " + usuarioDTO.getCentroCostoActivo().getCodigo() + " Message: Error del Sistema",ex.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/aperturacaja")
	public Response aperturaCaja(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		
		try {

			CajaDTO respuesta = cajaBusiness.aperturaCaja(userPrincipal.getName(),usuarioDTO.getCentroCostoActivo().getCodigo());
			usuarioDTO.setCaja(respuesta.getCorrCaja());
			request.getSession().setAttribute("usuario", usuarioDTO);
			log.info("Listar Cajas Abiertas, Usuario " + request.getUserPrincipal() + "  - Message: " + respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
			return Response.ok(respuesta).build();

		} catch (Exception ex) {
			log.error("Listar Cajas Abiertas, Usuario " + request.getUserPrincipal() + " Centro de Costo: " + usuarioDTO.getCentroCostoActivo().getCodigo() +  "Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/seleccionarcaja")
	public Response seleccionarcaja(@Context HttpServletRequest request, String corrCaja) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			usuarioDTO.setCaja(corrCaja);
			request.getSession().setAttribute("usuario", usuarioDTO);

			log.info("Seleccionar caja, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			return Response.ok(usuarioDTO).build();

		} catch (Exception ex) {
			log.info("seleccionarcaja, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			log.error("seleccionarcaja, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/tomarcaja")
	public Response tomarCaja(@Context HttpServletRequest request, CajaDTO cajaDTO) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			cajaBusiness.tomarCaja(cajaDTO.getCorrCaja(), cajaDTO.getTimestamp(), usuarioDTO.getUsername());
			usuarioDTO.setCaja(cajaDTO.getCorrCaja());
			usuarioDTO.setValidaSupervisor(false);
			request.getSession().setAttribute("usuario", usuarioDTO);
			log.info("tomar caja, Usuario " + request.getUserPrincipal() + " - Caja " + cajaDTO.getCorrCaja());
			return Response.ok(usuarioDTO).build();

		} catch (Exception ex) {
			log.info("tomar caja, Usuario " + request.getUserPrincipal() + " - Caja " + cajaDTO.getCorrCaja());
			log.error("tomar caja, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cerrarcajatemporal")
	public Response cerrarcajatemporal(@Context HttpServletRequest request, String corrCaja) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		
		try {

			cajaBusiness.cerrarCajaTemporal(corrCaja, usuarioDTO.getUsername());

			usuarioDTO.setCaja("0");
			request.getSession().setAttribute("usuario", usuarioDTO);

			log.info("cerrarcajatemporal, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			return Response.ok(usuarioDTO).build();

		} catch (Exception ex) {
			log.info("cerrarcajatemporal, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			log.error("cerrarcajatemporal, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cerrarcajadefinitivo")
	public Response cerrarcajadefinitivo(@Context HttpServletRequest request, String corrCaja) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		
		try {

			cajaBusiness.cerrarCajaDefinitivo(corrCaja, usuarioDTO.getUsername());

			usuarioDTO.setCaja("0");
			request.getSession().setAttribute("usuario", usuarioDTO);

			log.info("Metodo cerrar caja definitivo, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			return Response.ok(usuarioDTO).build();

		} catch (Exception ex) {
			log.info("Metodo cerrarcajadefinitivo, Usuario " + request.getUserPrincipal() + " - Caja " + corrCaja);
			log.error("Metodo cerrarcajadefinitivo, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex.getMessage());
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/listarcajastodas")
	public Response listarCajasTodas(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		
		try {

			ResponseListDTO<CajaDTO> respuesta = cajaBusiness.listarCajasTodas(userPrincipal.getName());

			GenericEntity<List<CajaDTO>> list = new GenericEntity<List<CajaDTO>>(respuesta.getLista()) {};

			log.info("Metodo listarCajasTodas, Usuario " + request.getUserPrincipal() + "  - Message: "+ respuesta.getMsgControl() + " - Codigo " + respuesta.getIdControl());
			return Response.ok(list).build();

		} catch (Exception ex) {
			log.error("Metodo listarCajasTodas, Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Produces("application/json")
	@Path("/listarCajasDia")
	public Response listarCajasDelDiaPorPrivilegio(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
		Integer tipoUsuario = 0;
		try {

			
			tipoUsuario = usuarioDTO.getEsVenta() ? 3 : tipoUsuario;
			tipoUsuario = usuarioDTO.getEsSupervisor() ? 2 : tipoUsuario;
			tipoUsuario = usuarioDTO.getEsAdministrador() ? 1 : tipoUsuario;

			ResponseListDTO<CajaDTO> cajas = cajaBusiness.listarCajasDelDiaPorPrivilegio(usuarioDTO.getCentroCostoActivo().getCodigo(), usuarioDTO.getUsername(), tipoUsuario);

			return Response.ok(cajas.getLista()).build();

		} catch (Exception ex) {
			log.info("Metodo listarCajasDelDiaPorPrivilegio , Usuario " + request.getUserPrincipal() + " Centro de Costo: " + usuarioDTO.getCentroCostoActivo().getCodigo() + "Tipo Usuario: " + tipoUsuario);
			log.error("Metodo listarCajasDelDiaPorPrivilegio , Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema",ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/cajapendiente")
	public Response cajaPendiente(@Context HttpServletRequest request) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			
			String codCaja = "";
			
			UsuarioDTO usuarioDTO = (UsuarioDTO) request.getSession().getAttribute("usuario");
			
			codCaja = usuarioDTO.getCaja();
			
			CajaPendienteDTO cajaPendiente = cajaBusiness.cajasAbiertaPendiente(usuarioDTO.getUsername());
			
			
			//if(!codCaja.equalsIgnoreCase("0") && ( cajaPendiente.getIdControl() > 0 || cajaPendiente.getCorrCaja() != null)){
			if(cajaPendiente.getIdControl() == 0 && cajaPendiente.getCorrCaja() != null){
				usuarioDTO.setCaja(cajaPendiente.getCorrCaja());
			}
			else{
				usuarioDTO.setCaja(codCaja);
			}
			
			request.getSession().setAttribute("usuario", usuarioDTO);

			return Response.ok(cajaPendiente).build();

		} catch (Exception ex) {
			log.error("Metodo cajaPendiente , Usuario " + request.getUserPrincipal() + " - Message: Error del Sistema", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
