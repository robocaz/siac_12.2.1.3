package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TBL_RUTMORHISTO database table.
 * 
 */
@Embeddable
public class TblRutmorhistoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CORRMOROSIDAD", insertable=false, updatable=false)
	private long fldCorrmorosidad;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	public TblRutmorhistoPK() {
	}
	public long getFldCorrmorosidad() {
		return this.fldCorrmorosidad;
	}
	public void setFldCorrmorosidad(long fldCorrmorosidad) {
		this.fldCorrmorosidad = fldCorrmorosidad;
	}
	public String getFldRutafectado() {
		return this.fldRutafectado;
	}
	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TblRutmorhistoPK)) {
			return false;
		}
		TblRutmorhistoPK castOther = (TblRutmorhistoPK)other;
		return 
			(this.fldCorrmorosidad == castOther.fldCorrmorosidad)
			&& this.fldRutafectado.equals(castOther.fldRutafectado);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fldCorrmorosidad ^ (this.fldCorrmorosidad >>> 32)));
		hash = hash * prime + this.fldRutafectado.hashCode();
		
		return hash;
	}
}