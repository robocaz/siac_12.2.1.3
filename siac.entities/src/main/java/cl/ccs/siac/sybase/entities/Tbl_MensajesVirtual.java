package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_MensajesVirtual database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_MensajesVirtual.findAll", query="SELECT t FROM Tbl_MensajesVirtual t")
public class Tbl_MensajesVirtual implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CorrVirtual_Id")
	private BigDecimal fld_CorrVirtual_Id;

	@Column(name="Fld_FecActual")
	private Timestamp fld_FecActual;

	@Column(name="Fld_MensajeVirtual")
	private String fld_MensajeVirtual;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_TotalRegistros")
	private int fld_TotalRegistros;

	public Tbl_MensajesVirtual() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CorrVirtual_Id() {
		return this.fld_CorrVirtual_Id;
	}

	public void setFld_CorrVirtual_Id(BigDecimal fld_CorrVirtual_Id) {
		this.fld_CorrVirtual_Id = fld_CorrVirtual_Id;
	}

	public Timestamp getFld_FecActual() {
		return this.fld_FecActual;
	}

	public void setFld_FecActual(Timestamp fld_FecActual) {
		this.fld_FecActual = fld_FecActual;
	}

	public String getFld_MensajeVirtual() {
		return this.fld_MensajeVirtual;
	}

	public void setFld_MensajeVirtual(String fld_MensajeVirtual) {
		this.fld_MensajeVirtual = fld_MensajeVirtual;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public int getFld_TotalRegistros() {
		return this.fld_TotalRegistros;
	}

	public void setFld_TotalRegistros(int fld_TotalRegistros) {
		this.fld_TotalRegistros = fld_TotalRegistros;
	}

}