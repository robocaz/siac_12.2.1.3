angular.module('ccs').factory('ObservacionResource', function ($resource) {
    var resource = $resource('rest/observacion/',  { nocache : new Date().getTime()},
    {
        'crear'                         : { method : 'POST', isArray: false, url : 'rest/observacion/crear' },
        'listarobservacionestramitante' : { method : 'POST', isArray: true,  url : 'rest/observacion/listarobservacionestramitante' },
        'listarobservaciones'           : { method : 'GET', isArray: true, url : 'rest/observacion/listarobservaciones' }
    });
    return resource;
});
