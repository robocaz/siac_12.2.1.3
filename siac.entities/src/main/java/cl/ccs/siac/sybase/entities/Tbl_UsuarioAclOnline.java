package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_UsuarioAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsuarioAclOnline.findAll", query="SELECT t FROM Tbl_UsuarioAclOnline t")
public class Tbl_UsuarioAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioCreacion")
	private String fld_CodUsuarioCreacion;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrFirma")
	private BigDecimal fld_CorrFirma;

	@Column(name="Fld_FecCambioPassword")
	private Timestamp fld_FecCambioPassword;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_Password")
	private String fld_Password;

	public Tbl_UsuarioAclOnline() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioCreacion() {
		return this.fld_CodUsuarioCreacion;
	}

	public void setFld_CodUsuarioCreacion(String fld_CodUsuarioCreacion) {
		this.fld_CodUsuarioCreacion = fld_CodUsuarioCreacion;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrFirma() {
		return this.fld_CorrFirma;
	}

	public void setFld_CorrFirma(BigDecimal fld_CorrFirma) {
		this.fld_CorrFirma = fld_CorrFirma;
	}

	public Timestamp getFld_FecCambioPassword() {
		return this.fld_FecCambioPassword;
	}

	public void setFld_FecCambioPassword(Timestamp fld_FecCambioPassword) {
		this.fld_FecCambioPassword = fld_FecCambioPassword;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public String getFld_Password() {
		return this.fld_Password;
	}

	public void setFld_Password(String fld_Password) {
		this.fld_Password = fld_Password;
	}

}