package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_ley2 database table.
 * 
 */
@Entity
@Table(name="tmp_ley2")
@NamedQuery(name="TmpLey2.findAll", query="SELECT t FROM TmpLey2 t")
public class TmpLey2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CantMol")
	private short cantMol;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="SumaMol")
	private BigDecimal sumaMol;

	public TmpLey2() {
	}

	public short getCantMol() {
		return this.cantMol;
	}

	public void setCantMol(short cantMol) {
		this.cantMol = cantMol;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public BigDecimal getSumaMol() {
		return this.sumaMol;
	}

	public void setSumaMol(BigDecimal sumaMol) {
		this.sumaMol = sumaMol;
	}

}