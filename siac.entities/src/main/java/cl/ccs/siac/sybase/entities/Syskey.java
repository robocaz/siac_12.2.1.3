package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the syskeys database table.
 * 
 */
@Entity
@Table(name="syskeys")
@NamedQuery(name="Syskey.findAll", query="SELECT s FROM Syskey s")
public class Syskey implements Serializable {
	private static final long serialVersionUID = 1L;

	private int depid;

	private short depkey1;

	private short depkey2;

	private short depkey3;

	private short depkey4;

	private short depkey5;

	private short depkey6;

	private short depkey7;

	private short depkey8;

	private int id;

	private short key1;

	private short key2;

	private short key3;

	private short key4;

	private short key5;

	private short key6;

	private short key7;

	private short key8;

	private int keycnt;

	private int size;

	private short spare1;

	private short type;

	public Syskey() {
	}

	public int getDepid() {
		return this.depid;
	}

	public void setDepid(int depid) {
		this.depid = depid;
	}

	public short getDepkey1() {
		return this.depkey1;
	}

	public void setDepkey1(short depkey1) {
		this.depkey1 = depkey1;
	}

	public short getDepkey2() {
		return this.depkey2;
	}

	public void setDepkey2(short depkey2) {
		this.depkey2 = depkey2;
	}

	public short getDepkey3() {
		return this.depkey3;
	}

	public void setDepkey3(short depkey3) {
		this.depkey3 = depkey3;
	}

	public short getDepkey4() {
		return this.depkey4;
	}

	public void setDepkey4(short depkey4) {
		this.depkey4 = depkey4;
	}

	public short getDepkey5() {
		return this.depkey5;
	}

	public void setDepkey5(short depkey5) {
		this.depkey5 = depkey5;
	}

	public short getDepkey6() {
		return this.depkey6;
	}

	public void setDepkey6(short depkey6) {
		this.depkey6 = depkey6;
	}

	public short getDepkey7() {
		return this.depkey7;
	}

	public void setDepkey7(short depkey7) {
		this.depkey7 = depkey7;
	}

	public short getDepkey8() {
		return this.depkey8;
	}

	public void setDepkey8(short depkey8) {
		this.depkey8 = depkey8;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getKey1() {
		return this.key1;
	}

	public void setKey1(short key1) {
		this.key1 = key1;
	}

	public short getKey2() {
		return this.key2;
	}

	public void setKey2(short key2) {
		this.key2 = key2;
	}

	public short getKey3() {
		return this.key3;
	}

	public void setKey3(short key3) {
		this.key3 = key3;
	}

	public short getKey4() {
		return this.key4;
	}

	public void setKey4(short key4) {
		this.key4 = key4;
	}

	public short getKey5() {
		return this.key5;
	}

	public void setKey5(short key5) {
		this.key5 = key5;
	}

	public short getKey6() {
		return this.key6;
	}

	public void setKey6(short key6) {
		this.key6 = key6;
	}

	public short getKey7() {
		return this.key7;
	}

	public void setKey7(short key7) {
		this.key7 = key7;
	}

	public short getKey8() {
		return this.key8;
	}

	public void setKey8(short key8) {
		this.key8 = key8;
	}

	public int getKeycnt() {
		return this.keycnt;
	}

	public void setKeycnt(int keycnt) {
		this.keycnt = keycnt;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public short getSpare1() {
		return this.spare1;
	}

	public void setSpare1(short spare1) {
		this.spare1 = spare1;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

}