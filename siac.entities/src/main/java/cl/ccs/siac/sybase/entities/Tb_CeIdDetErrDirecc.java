package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdDetErrDirecc database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetErrDirecc.findAll", query="SELECT t FROM Tb_CeIdDetErrDirecc t")
public class Tb_CeIdDetErrDirecc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CorrDireccion")
	private BigDecimal fld_CorrDireccion;

	public Tb_CeIdDetErrDirecc() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public BigDecimal getFld_CorrDireccion() {
		return this.fld_CorrDireccion;
	}

	public void setFld_CorrDireccion(BigDecimal fld_CorrDireccion) {
		this.fld_CorrDireccion = fld_CorrDireccion;
	}

}