package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoCambioClave database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoCambioClave.findAll", query="SELECT t FROM Tbl_TipoCambioClave t")
public class Tbl_TipoCambioClave implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoCambio")
	private String fld_GlosaTipoCambio;

	@Column(name="Fld_TipoCambio")
	private byte fld_TipoCambio;

	public Tbl_TipoCambioClave() {
	}

	public String getFld_GlosaTipoCambio() {
		return this.fld_GlosaTipoCambio;
	}

	public void setFld_GlosaTipoCambio(String fld_GlosaTipoCambio) {
		this.fld_GlosaTipoCambio = fld_GlosaTipoCambio;
	}

	public byte getFld_TipoCambio() {
		return this.fld_TipoCambio;
	}

	public void setFld_TipoCambio(byte fld_TipoCambio) {
		this.fld_TipoCambio = fld_TipoCambio;
	}

}