package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtrlCargaRutif database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CtrlCargaRutif.findAll", query="SELECT t FROM Tbl_CtrlCargaRutif t")
public class Tbl_CtrlCargaRutif implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCRut_Id")
	private BigDecimal fld_CorrCRut_Id;

	@Column(name="Fld_FecExtraccion")
	private Timestamp fld_FecExtraccion;

	@Column(name="Fld_FechaDesde")
	private Timestamp fld_FechaDesde;

	@Column(name="Fld_FechaHasta")
	private Timestamp fld_FechaHasta;

	@Column(name="Fld_MarcaCargado")
	private byte fld_MarcaCargado;

	@Column(name="Fld_MarcaExtraccion")
	private byte fld_MarcaExtraccion;

	@Column(name="Fld_MarcaProcesado")
	private byte fld_MarcaProcesado;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_TotRegBIC")
	private int fld_TotRegBIC;

	@Column(name="Fld_TotRegINFOCOM")
	private int fld_TotRegINFOCOM;

	@Column(name="Fld_TotRegistros")
	private int fld_TotRegistros;

	public Tbl_CtrlCargaRutif() {
	}

	public BigDecimal getFld_CorrCRut_Id() {
		return this.fld_CorrCRut_Id;
	}

	public void setFld_CorrCRut_Id(BigDecimal fld_CorrCRut_Id) {
		this.fld_CorrCRut_Id = fld_CorrCRut_Id;
	}

	public Timestamp getFld_FecExtraccion() {
		return this.fld_FecExtraccion;
	}

	public void setFld_FecExtraccion(Timestamp fld_FecExtraccion) {
		this.fld_FecExtraccion = fld_FecExtraccion;
	}

	public Timestamp getFld_FechaDesde() {
		return this.fld_FechaDesde;
	}

	public void setFld_FechaDesde(Timestamp fld_FechaDesde) {
		this.fld_FechaDesde = fld_FechaDesde;
	}

	public Timestamp getFld_FechaHasta() {
		return this.fld_FechaHasta;
	}

	public void setFld_FechaHasta(Timestamp fld_FechaHasta) {
		this.fld_FechaHasta = fld_FechaHasta;
	}

	public byte getFld_MarcaCargado() {
		return this.fld_MarcaCargado;
	}

	public void setFld_MarcaCargado(byte fld_MarcaCargado) {
		this.fld_MarcaCargado = fld_MarcaCargado;
	}

	public byte getFld_MarcaExtraccion() {
		return this.fld_MarcaExtraccion;
	}

	public void setFld_MarcaExtraccion(byte fld_MarcaExtraccion) {
		this.fld_MarcaExtraccion = fld_MarcaExtraccion;
	}

	public byte getFld_MarcaProcesado() {
		return this.fld_MarcaProcesado;
	}

	public void setFld_MarcaProcesado(byte fld_MarcaProcesado) {
		this.fld_MarcaProcesado = fld_MarcaProcesado;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public int getFld_TotRegBIC() {
		return this.fld_TotRegBIC;
	}

	public void setFld_TotRegBIC(int fld_TotRegBIC) {
		this.fld_TotRegBIC = fld_TotRegBIC;
	}

	public int getFld_TotRegINFOCOM() {
		return this.fld_TotRegINFOCOM;
	}

	public void setFld_TotRegINFOCOM(int fld_TotRegINFOCOM) {
		this.fld_TotRegINFOCOM = fld_TotRegINFOCOM;
	}

	public int getFld_TotRegistros() {
		return this.fld_TotRegistros;
	}

	public void setFld_TotRegistros(int fld_TotRegistros) {
		this.fld_TotRegistros = fld_TotRegistros;
	}

}