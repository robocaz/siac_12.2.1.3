angular.module('ccs').controller('ModalLoginSupervisorController', function($scope, $uibModalInstance, LoginResource ){

    $scope.resultado = false;
    $scope.msgerror ="";
    $scope.password = null;
    $scope.username = null;

    $("#username_supervisor").focus();

    $scope.ok = function(){
		
    	$scope.$emit('logout');

    	var successCallback = function(data){
            if(data.idControl == 0){
                $uibModalInstance.close(true);
            }else if(data.idControl==1 || data.idControl==2){
                $scope.msgerror ="Error de Usuario y/o Contraseña";
            }else if(data.idControl==3){
                $scope.msgerror ="Usuario no vigente";
            }else if(data.idControl==4){
                $scope.msgerror ="Usuario no es Supervisor";
            }else if(data.idControl==6){
                $scope.msgerror ="Error al Consultar Supervisor";
            }
        };
        LoginResource.esSupervisor({'username': $scope.username , 'password': $scope.password }, successCallback, $scope.errorCallback)
    }
    $scope.cancel = function () {
        $uibModalInstance.close(false);
    };

});
