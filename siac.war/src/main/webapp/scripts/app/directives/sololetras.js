angular.module('ccs').directive('lettersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
                var regex = /^[A-Za-z\-'ÑñáéíóúäëïöüÁÉÍÓÚÄËÏÖÜ ]*$/;
            	if( !regex.test(this.value) ) {
            		this.value = null;
            	}else{
            		var reCleanSpace = /^\s+|\s+$|(\s)\s+/g;
            		this.value = this.value.replace(reCleanSpace , '$1');
                    this.value = this.value.toUpperCase();
            	}
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();
            });
            element.bind('keypress', function (evt) {
                var regex = /^[A-Za-z\-'ÑñáéíóäëïöüÁÉÍÓÚÄËÏÖÜ ]*$/;

                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                var key2 = theEvent.charCode;
                if (key === 8  || key === 127 || key === 9 || (key === 37 && key2===0)  || (key === 39 && key2===0)  ) {
                   return true;
                }
                key = String.fromCharCode( key );

                if( regex.test(key) ) {
                    return true
                }else{
                    return false
                }
            });

        }
    };
});

angular.module('ccs').directive('lettersOnlyBusiness', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
                var regex = /^[A-Za-z0-9\-' ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ.&,\(\)]*$/;
            	if( !regex.test(this.value) ) {
            		this.value = null;
            	}else{
            		var reCleanSpace = /^\s+|\s+$|(\s)\s+/g;
            		this.value = this.value.replace(reCleanSpace , '$1');
                    this.value = this.value.toUpperCase();
            	}
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();

            });
            element.bind('keypress', function (evt) {
                var regex = /^[A-Za-z0-9\-' ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ.&,\(\)]*$/;

                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                var key2 = theEvent.charCode;
                if (key === 8  || key === 127 || key === 9 || (key === 37 && key2===0)  || (key === 39 && key2===0)  ) {
                   return true;
                }
                key = String.fromCharCode( key );

                if( regex.test(key) ) {
                    return true
                }else{
                    return false
                }
            });
        }
    };
});


angular.module('ccs').directive('ngSpaceless', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
        		var reCleanSpace = /^\s+|\s+$|(\s)\s+/g;
        		this.value = this.value.replace(reCleanSpace , '$1');
                if(this.value==""){
                    this.value = null;
                }
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();
            });
        }
    };
});



angular.module('ccs').directive('lettersNumberOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
                var regex = /^[A-Za-z0-9]{1,}$/;
            	if( !regex.test(this.value) ) {
            		this.value = null;
            	}else{
            		var reCleanSpace = /^\s+|\s+$|(\s)\s+/g;
            		this.value = this.value.replace(reCleanSpace , '$1');
                    this.value = this.value.toUpperCase();
            	}
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();
            });
            element.bind('keypress', function (evt) {
                var regex = /^[A-Za-z0-9]*$/;

                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                var key2 = theEvent.charCode;
                if (key === 8  || key === 127 || key === 9 || (key === 37 && key2===0)  || (key === 39 && key2===0)  ) {
                   return true;
                }
                key = String.fromCharCode( key );

                if( regex.test(key) ) {
                    return true
                }else{
                    return false
                }
            });

        }
    };
});

angular.module('ccs').directive('lettersNumberOnlyNoUpper', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.bind('blur', function () {
                var regex = /^[A-Za-z0-9]{1,}$/;
            	if( !regex.test(this.value) ) {
            		this.value = null;
            	}else{
            		var reCleanSpace = /^\s+|\s+$|(\s)\s+/g;
            		this.value = this.value.replace(reCleanSpace , '$1');
            	}
                ngModelCtrl.$setViewValue(this.value);
                ngModelCtrl.$render();
            });
            element.bind('keypress', function (evt) {
                var regex = /^[A-Za-z0-9]*$/;

                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                var key2 = theEvent.charCode;
                if (key === 8  || key === 127 || key === 9 || (key === 37 && key2===0)  || (key === 39 && key2===0)  ) {
                   return true;
                }
                key = String.fromCharCode( key );

                if( regex.test(key) ) {
                    return true
                }else{
                    return false
                }
            });

        }
    };
});

angular.module('ccs').directive('lettersNoBackslash', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        if (text) {
          var transformedInput = text.replace(/[\\]/g, '');

          if (transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
          }

          return transformedInput;
        }
        return undefined;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
