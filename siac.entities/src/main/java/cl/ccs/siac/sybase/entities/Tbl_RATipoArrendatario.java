package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RATipoArrendatario database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RATipoArrendatario.findAll", query="SELECT t FROM Tbl_RATipoArrendatario t")
public class Tbl_RATipoArrendatario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoArrendatario")
	private String fld_GlosaTipoArrendatario;

	@Column(name="Fld_TipoArrendatario")
	private String fld_TipoArrendatario;

	public Tbl_RATipoArrendatario() {
	}

	public String getFld_GlosaTipoArrendatario() {
		return this.fld_GlosaTipoArrendatario;
	}

	public void setFld_GlosaTipoArrendatario(String fld_GlosaTipoArrendatario) {
		this.fld_GlosaTipoArrendatario = fld_GlosaTipoArrendatario;
	}

	public String getFld_TipoArrendatario() {
		return this.fld_TipoArrendatario;
	}

	public void setFld_TipoArrendatario(String fld_TipoArrendatario) {
		this.fld_TipoArrendatario = fld_TipoArrendatario;
	}

}