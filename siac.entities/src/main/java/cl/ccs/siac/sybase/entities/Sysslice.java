package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysslices database table.
 * 
 */
@Entity
@Table(name="sysslices")
@NamedQuery(name="Sysslice.findAll", query="SELECT s FROM Sysslice s")
public class Sysslice implements Serializable {
	private static final long serialVersionUID = 1L;

	private int controlpage;

	private int firstpage;

	private int id;

	private int partitionid;

	private byte[] spare;

	private short state;

	public Sysslice() {
	}

	public int getControlpage() {
		return this.controlpage;
	}

	public void setControlpage(int controlpage) {
		this.controlpage = controlpage;
	}

	public int getFirstpage() {
		return this.firstpage;
	}

	public void setFirstpage(int firstpage) {
		this.firstpage = firstpage;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPartitionid() {
		return this.partitionid;
	}

	public void setPartitionid(int partitionid) {
		this.partitionid = partitionid;
	}

	public byte[] getSpare() {
		return this.spare;
	}

	public void setSpare(byte[] spare) {
		this.spare = spare;
	}

	public short getState() {
		return this.state;
	}

	public void setState(short state) {
		this.state = state;
	}

}