package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_BlqURepublica database table.
 * 
 */
@Entity
@Table(name="tmp_BlqURepublica")
@NamedQuery(name="Tmp_BlqURepublica.findAll", query="SELECT t FROM Tmp_BlqURepublica t")
public class Tmp_BlqURepublica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CorrProt")
	private BigDecimal corrProt;

	public Tmp_BlqURepublica() {
	}

	public BigDecimal getCorrProt() {
		return this.corrProt;
	}

	public void setCorrProt(BigDecimal corrProt) {
		this.corrProt = corrProt;
	}

}