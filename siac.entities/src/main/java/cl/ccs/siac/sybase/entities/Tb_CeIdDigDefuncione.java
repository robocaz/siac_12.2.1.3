package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdDigDefunciones database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDigDefunciones")
@NamedQuery(name="Tb_CeIdDigDefuncione.findAll", query="SELECT t FROM Tb_CeIdDigDefuncione t")
public class Tb_CeIdDigDefuncione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrDigDef_Id")
	private BigDecimal fld_CorrDigDef_Id;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecDefuncion")
	private Timestamp fld_FecDefuncion;

	@Column(name="Fld_FecNacimiento")
	private Timestamp fld_FecNacimiento;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Tb_CeIdDigDefuncione() {
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrDigDef_Id() {
		return this.fld_CorrDigDef_Id;
	}

	public void setFld_CorrDigDef_Id(BigDecimal fld_CorrDigDef_Id) {
		this.fld_CorrDigDef_Id = fld_CorrDigDef_Id;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecDefuncion() {
		return this.fld_FecDefuncion;
	}

	public void setFld_FecDefuncion(Timestamp fld_FecDefuncion) {
		this.fld_FecDefuncion = fld_FecDefuncion;
	}

	public Timestamp getFld_FecNacimiento() {
		return this.fld_FecNacimiento;
	}

	public void setFld_FecNacimiento(Timestamp fld_FecNacimiento) {
		this.fld_FecNacimiento = fld_FecNacimiento;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}