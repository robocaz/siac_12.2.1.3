package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_Monedas database table.
 * 
 */
@Entity
@Table(name="Tbl_Monedas")
@NamedQuery(name="Tbl_Moneda.findAll", query="SELECT t FROM Tbl_Moneda t")
public class Tbl_Moneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_FlagVig")
	private byte fld_FlagVig;

	@Column(name="Fld_GlosaCorta")
	private String fld_GlosaCorta;

	@Column(name="Fld_GlosaMoneda")
	private String fld_GlosaMoneda;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorUS")
	private BigDecimal fld_ValorUS;

	public Tbl_Moneda() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_FlagVig() {
		return this.fld_FlagVig;
	}

	public void setFld_FlagVig(byte fld_FlagVig) {
		this.fld_FlagVig = fld_FlagVig;
	}

	public String getFld_GlosaCorta() {
		return this.fld_GlosaCorta;
	}

	public void setFld_GlosaCorta(String fld_GlosaCorta) {
		this.fld_GlosaCorta = fld_GlosaCorta;
	}

	public String getFld_GlosaMoneda() {
		return this.fld_GlosaMoneda;
	}

	public void setFld_GlosaMoneda(String fld_GlosaMoneda) {
		this.fld_GlosaMoneda = fld_GlosaMoneda;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorUS() {
		return this.fld_ValorUS;
	}

	public void setFld_ValorUS(BigDecimal fld_ValorUS) {
		this.fld_ValorUS = fld_ValorUS;
	}

}