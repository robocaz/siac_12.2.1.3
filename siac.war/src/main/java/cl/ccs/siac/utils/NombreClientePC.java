package cl.ccs.siac.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.ejb.Stateless;
import javax.inject.Named;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Named("nombreClientePC")
@Stateless
public class NombreClientePC {

	public String obtieneNombreMaquina() {

		JsonParser parser = new JsonParser();
		//
		HttpClient client = HttpClientBuilder.create().build();

		HttpGet request2 = new HttpGet("http://localhost:8080/terminal/siac/boletas");
		HttpResponse response;
		String nombreMaquina = "";
		String salida = "";
		try {
			response = client.execute(request2);
			String line = "";
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((line = rd.readLine()) != null) {
				nombreMaquina = line;
			}

			JsonObject obj = (JsonObject) parser.parse(nombreMaquina);
			salida = String.valueOf(obj.get("nombreEquipo")).replace("\"", "");

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return salida;
	}
}
