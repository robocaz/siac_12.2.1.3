package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditElimAclaraciones database table.
 * 
 */
@Entity
@Table(name="Tbl_AuditElimAclaraciones")
@NamedQuery(name="Tbl_AuditElimAclaracione.findAll", query="SELECT t FROM Tbl_AuditElimAclaracione t")
public class Tbl_AuditElimAclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrAcl")
	private BigDecimal fld_CorrAcl;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecElimAcl")
	private Timestamp fld_FecElimAcl;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoPareo")
	private short fld_TipoPareo;

	public Tbl_AuditElimAclaracione() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrAcl() {
		return this.fld_CorrAcl;
	}

	public void setFld_CorrAcl(BigDecimal fld_CorrAcl) {
		this.fld_CorrAcl = fld_CorrAcl;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecElimAcl() {
		return this.fld_FecElimAcl;
	}

	public void setFld_FecElimAcl(Timestamp fld_FecElimAcl) {
		this.fld_FecElimAcl = fld_FecElimAcl;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public short getFld_TipoPareo() {
		return this.fld_TipoPareo;
	}

	public void setFld_TipoPareo(short fld_TipoPareo) {
		this.fld_TipoPareo = fld_TipoPareo;
	}

}