package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class CuadraturaAclaracionDTO extends ResponseDTO implements Serializable{

	private Integer numeroAclaraciones;
	private Integer numeroAvisos;
	private List<DetalleAclaracionDTO> detalleAclaracion;
	
	
	
	public Integer getNumeroAclaraciones() {
		return numeroAclaraciones;
	}
	public void setNumeroAclaraciones(Integer numeroAclaraciones) {
		this.numeroAclaraciones = numeroAclaraciones;
	}
	public Integer getNumeroAvisos() {
		return numeroAvisos;
	}
	public void setNumeroAvisos(Integer numeroAvisos) {
		this.numeroAvisos = numeroAvisos;
	}
	public List<DetalleAclaracionDTO> getDetalleAclaracion() {
		return detalleAclaracion;
	}
	public void setDetalleAclaracion(List<DetalleAclaracionDTO> detalleAclaracion) {
		this.detalleAclaracion = detalleAclaracion;
	}
	
	
	
	
}
