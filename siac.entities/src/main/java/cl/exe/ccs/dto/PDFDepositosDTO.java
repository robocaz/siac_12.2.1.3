package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PDFDepositosDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = -4780705423085151696L;

	private String fechaDesde;
	private String fechaHasta;
	private String usuario;
	private String centroCosto;
	private Integer totalDepos = 0;
	private BigDecimal totalMontos = BigDecimal.ZERO;
	private List<DepositoDTO> depositos;
	
	
	public BigDecimal calTotalMontos() {
		if ( depositos != null ) {
			for (DepositoDTO d : depositos) {
				if ( d.getMontoDepositoAux() != null ) {
					totalMontos = totalMontos.add(d.getMontoDepositoAux());
				}
			}
		}
		return totalMontos;
	}
	
	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}

	public Integer getTotalDepos() {
		return totalDepos;
	}

	public void setTotalDepos(Integer totalDepos) {
		this.totalDepos = totalDepos;
	}

	public BigDecimal getTotalMontos() {
		return totalMontos;
	}

	public void setTotalMontos(BigDecimal totalMontos) {
		this.totalMontos = totalMontos;
	}

	public List<DepositoDTO> getDepositos() {
		return depositos;
	}

	public void setDepositos(List<DepositoDTO> depositos) {
		this.depositos = depositos;
	}

	@Override
	public String toString() { 
		StringBuilder str = new StringBuilder();
		str.append("FechaDesde: " + fechaDesde);
		str.append(" ,FechaHasta: " + fechaHasta);
		str.append(" ,Usuario: " + usuario);
		str.append(" ,CC: " + centroCosto);
		str.append(" ,TotalDepos: " + totalDepos);
		str.append(" ,TotalMontos: " + totalMontos);
		str.append(" ,ListaDepos: " + depositos);
		return str.toString();
	}

}
