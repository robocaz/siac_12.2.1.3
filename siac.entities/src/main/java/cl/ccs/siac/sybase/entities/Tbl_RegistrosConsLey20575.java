package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RegistrosConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RegistrosConsLey20575.findAll", query="SELECT t FROM Tbl_RegistrosConsLey20575 t")
public class Tbl_RegistrosConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMotivo")
	private short fld_CodMotivo;

	@Column(name="Fld_CodSistema")
	private short fld_CodSistema;

	@Column(name="Fld_CorrRegistro_Id")
	private BigDecimal fld_CorrRegistro_Id;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutRequirente")
	private String fld_RutRequirente;

	public Tbl_RegistrosConsLey20575() {
	}

	public short getFld_CodMotivo() {
		return this.fld_CodMotivo;
	}

	public void setFld_CodMotivo(short fld_CodMotivo) {
		this.fld_CodMotivo = fld_CodMotivo;
	}

	public short getFld_CodSistema() {
		return this.fld_CodSistema;
	}

	public void setFld_CodSistema(short fld_CodSistema) {
		this.fld_CodSistema = fld_CodSistema;
	}

	public BigDecimal getFld_CorrRegistro_Id() {
		return this.fld_CorrRegistro_Id;
	}

	public void setFld_CorrRegistro_Id(BigDecimal fld_CorrRegistro_Id) {
		this.fld_CorrRegistro_Id = fld_CorrRegistro_Id;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutRequirente() {
		return this.fld_RutRequirente;
	}

	public void setFld_RutRequirente(String fld_RutRequirente) {
		this.fld_RutRequirente = fld_RutRequirente;
	}

}