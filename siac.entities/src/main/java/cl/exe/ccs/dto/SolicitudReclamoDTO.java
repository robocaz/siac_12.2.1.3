package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoDTO extends ResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer corrSolicitudReclamo;
	private String glosaTipoSolicitud;
	private String rutTitular;
	private String nombreApePaterno;
	private String nombreApeMaterno;
	private String nombre;
	private String glosaEstado;
	private String fechaEstado;
	private String usuarioEstado;
	private Integer cantDoc;
	private String fechaSolicitud;
	private String telefono;
	private String texto;
	private String glosaCategoria;
	
	
	
	
	public String getGlosaCategoria() {
		return glosaCategoria;
	}
	public void setGlosaCategoria(String glosaCategoria) {
		this.glosaCategoria = glosaCategoria;
	}
	public Integer getCorrSolicitudReclamo() {
		return corrSolicitudReclamo;
	}
	public void setCorrSolicitudReclamo(Integer corrSolicitudReclamo) {
		this.corrSolicitudReclamo = corrSolicitudReclamo;
	}
	public String getGlosaTipoSolicitud() {
		return glosaTipoSolicitud;
	}
	public void setGlosaTipoSolicitud(String glosaTipoSolicitud) {
		this.glosaTipoSolicitud = glosaTipoSolicitud;
	}
	public String getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(String rutTitular) {
		this.rutTitular = rutTitular;
	}
	public String getNombreApePaterno() {
		return nombreApePaterno;
	}
	public void setNombreApePaterno(String nombreApePaterno) {
		this.nombreApePaterno = nombreApePaterno;
	}
	public String getNombreApeMaterno() {
		return nombreApeMaterno;
	}
	public void setNombreApeMaterno(String nombreApeMaterno) {
		this.nombreApeMaterno = nombreApeMaterno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getGlosaEstado() {
		return glosaEstado;
	}
	public void setGlosaEstado(String glosaEstado) {
		this.glosaEstado = glosaEstado;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public String getUsuarioEstado() {
		return usuarioEstado;
	}
	public void setUsuarioEstado(String usuarioEstado) {
		this.usuarioEstado = usuarioEstado;
	}
	public Integer getCantDoc() {
		return cantDoc;
	}
	public void setCantDoc(Integer cantDoc) {
		this.cantDoc = cantDoc;
	}
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
	

	
}
