package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BolLabDetPeriodo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BolLabDetPeriodo.findAll", query="SELECT t FROM Tbl_BolLabDetPeriodo t")
public class Tbl_BolLabDetPeriodo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Cantidad")
	private short fld_Cantidad;

	@Column(name="Fld_CodAcreedor")
	private String fld_CodAcreedor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodInfraccion")
	private String fld_CodInfraccion;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_Periodo")
	private int fld_Periodo;

	@Column(name="Fld_RutInfractor")
	private String fld_RutInfractor;

	public Tbl_BolLabDetPeriodo() {
	}

	public short getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(short fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public String getFld_CodAcreedor() {
		return this.fld_CodAcreedor;
	}

	public void setFld_CodAcreedor(String fld_CodAcreedor) {
		this.fld_CodAcreedor = fld_CodAcreedor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodInfraccion() {
		return this.fld_CodInfraccion;
	}

	public void setFld_CodInfraccion(String fld_CodInfraccion) {
		this.fld_CodInfraccion = fld_CodInfraccion;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public int getFld_Periodo() {
		return this.fld_Periodo;
	}

	public void setFld_Periodo(int fld_Periodo) {
		this.fld_Periodo = fld_Periodo;
	}

	public String getFld_RutInfractor() {
		return this.fld_RutInfractor;
	}

	public void setFld_RutInfractor(String fld_RutInfractor) {
		this.fld_RutInfractor = fld_RutInfractor;
	}

}