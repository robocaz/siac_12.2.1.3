package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the systhresholds database table.
 * 
 */
@Entity
@Table(name="systhresholds")
@NamedQuery(name="Systhreshold.findAll", query="SELECT s FROM Systhreshold s")
public class Systhreshold implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] currauth;

	@Column(name="free_space")
	private int freeSpace;

	@Column(name="proc_name")
	private String procName;

	private short segment;

	private short status;

	private int suid;

	public Systhreshold() {
	}

	public byte[] getCurrauth() {
		return this.currauth;
	}

	public void setCurrauth(byte[] currauth) {
		this.currauth = currauth;
	}

	public int getFreeSpace() {
		return this.freeSpace;
	}

	public void setFreeSpace(int freeSpace) {
		this.freeSpace = freeSpace;
	}

	public String getProcName() {
		return this.procName;
	}

	public void setProcName(String procName) {
		this.procName = procName;
	}

	public short getSegment() {
		return this.segment;
	}

	public void setSegment(short segment) {
		this.segment = segment;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public int getSuid() {
		return this.suid;
	}

	public void setSuid(int suid) {
		this.suid = suid;
	}

}