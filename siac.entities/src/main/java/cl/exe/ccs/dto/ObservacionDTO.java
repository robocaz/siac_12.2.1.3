package cl.exe.ccs.dto;

import java.io.Serializable;

public class ObservacionDTO extends ResponseDTO implements Serializable{
	
	private String rutTramitante;
	private Integer codigoObservacion;
	private String glosaObservacion;
	private String usuario;
	private Integer codigoCentroCosto;
	private String glosaCentroCosto;
	private String fechaCreacion;
	private Boolean esNuevo = false;
	
	public Integer getCodigoObservacion() {
		return codigoObservacion;
	}
	public void setCodigoObservacion(Integer codigoObservacion) {
		this.codigoObservacion = codigoObservacion;
	}
	public String getGlosaObservacion() {
		return glosaObservacion;
	}
	public void setGlosaObservacion(String glosaObservacion) {
		this.glosaObservacion = glosaObservacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getCodigoCentroCosto() {
		return codigoCentroCosto;
	}
	public void setCodigoCentroCosto(Integer codigoCentroCosto) {
		this.codigoCentroCosto = codigoCentroCosto;
	}
	public String getGlosaCentroCosto() {
		return glosaCentroCosto;
	}
	public void setGlosaCentroCosto(String glosaCentroCosto) {
		this.glosaCentroCosto = glosaCentroCosto;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public Boolean getEsNuevo() {
		return esNuevo;
	}
	public void setEsNuevo(Boolean esNuevo) {
		this.esNuevo = esNuevo;
	}
}
