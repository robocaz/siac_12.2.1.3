package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalSolicAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalSolicAER.findAll", query="SELECT t FROM Tbl_CausalSolicAER t")
public class Tbl_CausalSolicAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoSolic")
	private byte fld_TipoSolic;

	public Tbl_CausalSolicAER() {
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoSolic() {
		return this.fld_TipoSolic;
	}

	public void setFld_TipoSolic(byte fld_TipoSolic) {
		this.fld_TipoSolic = fld_TipoSolic;
	}

}