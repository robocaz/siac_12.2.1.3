package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ReclamoDTO extends ResponseDTO implements Serializable{
	
	private Integer aclaracionEspecial;
	private Integer rectificaciones;
	private List<SolicitudReclamoDTO> solReclamos;
	
	
	public Integer getAclaracionEspecial() {
		return aclaracionEspecial;
	}
	public void setAclaracionEspecial(Integer aclaracionEspecial) {
		this.aclaracionEspecial = aclaracionEspecial;
	}
	public Integer getRectificaciones() {
		return rectificaciones;
	}
	public void setRectificaciones(Integer rectificaciones) {
		this.rectificaciones = rectificaciones;
	}
	public List<SolicitudReclamoDTO> getSolReclamos() {
		return solReclamos;
	}
	public void setSolReclamos(List<SolicitudReclamoDTO> solReclamos) {
		this.solReclamos = solReclamos;
	}
	
	

}
