package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolVtaDetEstados database table.
 * 
 */
@Entity
@Table(name="Tbl_SolVtaDetEstados")
@NamedQuery(name="Tbl_SolVtaDetEstado.findAll", query="SELECT t FROM Tbl_SolVtaDetEstado t")
public class Tbl_SolVtaDetEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	public Tbl_SolVtaDetEstado() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

}