package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_BCTipoResponsableProc database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCTipoResponsableProc.findAll", query="SELECT t FROM Tbl_BCTipoResponsableProc t")
public class Tbl_BCTipoResponsableProc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoResponsableProc")
	private String fld_GlosaTipoResponsableProc;

	@Column(name="Fld_TipoResponsableProc")
	private String fld_TipoResponsableProc;

	public Tbl_BCTipoResponsableProc() {
	}

	public String getFld_GlosaTipoResponsableProc() {
		return this.fld_GlosaTipoResponsableProc;
	}

	public void setFld_GlosaTipoResponsableProc(String fld_GlosaTipoResponsableProc) {
		this.fld_GlosaTipoResponsableProc = fld_GlosaTipoResponsableProc;
	}

	public String getFld_TipoResponsableProc() {
		return this.fld_TipoResponsableProc;
	}

	public void setFld_TipoResponsableProc(String fld_TipoResponsableProc) {
		this.fld_TipoResponsableProc = fld_TipoResponsableProc;
	}

}