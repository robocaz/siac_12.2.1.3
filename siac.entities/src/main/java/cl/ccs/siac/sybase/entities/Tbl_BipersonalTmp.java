package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_BipersonalTmp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BipersonalTmp.findAll", query="SELECT t FROM Tbl_BipersonalTmp t")
public class Tbl_BipersonalTmp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrBipersonal")
	private BigDecimal fld_CorrBipersonal;

	@Column(name="Fld_CorrUnico")
	private BigDecimal fld_CorrUnico;

	@Column(name="Fld_RutBipersonal")
	private String fld_RutBipersonal;

	public Tbl_BipersonalTmp() {
	}

	public BigDecimal getFld_CorrBipersonal() {
		return this.fld_CorrBipersonal;
	}

	public void setFld_CorrBipersonal(BigDecimal fld_CorrBipersonal) {
		this.fld_CorrBipersonal = fld_CorrBipersonal;
	}

	public BigDecimal getFld_CorrUnico() {
		return this.fld_CorrUnico;
	}

	public void setFld_CorrUnico(BigDecimal fld_CorrUnico) {
		this.fld_CorrUnico = fld_CorrUnico;
	}

	public String getFld_RutBipersonal() {
		return this.fld_RutBipersonal;
	}

	public void setFld_RutBipersonal(String fld_RutBipersonal) {
		this.fld_RutBipersonal = fld_RutBipersonal;
	}

}