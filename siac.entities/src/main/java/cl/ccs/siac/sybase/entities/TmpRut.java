package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_rut database table.
 * 
 */
@Entity
@Table(name="tmp_rut")
@NamedQuery(name="TmpRut.findAll", query="SELECT t FROM TmpRut t")
public class TmpRut implements Serializable {
	private static final long serialVersionUID = 1L;

	private int cant;

	@Column(name="Rut")
	private String rut;

	private BigDecimal suma;

	public TmpRut() {
	}

	public int getCant() {
		return this.cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public BigDecimal getSuma() {
		return this.suma;
	}

	public void setSuma(BigDecimal suma) {
		this.suma = suma;
	}

}