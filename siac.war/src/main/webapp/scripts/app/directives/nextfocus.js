angular.module('ccs').directive("nextFocus", function () {

    var directive = {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            elem.bind('keypress', function (event) {
                var theEvent = event || window.event;
                var code = theEvent.keyCode || theEvent.which;
                if (code === 13 || code === 09|| code === 11) {
                    try {
                    	theEvent.preventDefault();
                        var fields=$(this).parents('form').eq(0).find('input:not([type=search]), select, button:enabled ');
                        var index=fields.index(this);
    					while(index> -1&&(index+1)<fields.length){
                            if((this.name!="" && this.name!=null && this.name == fields.eq(index+1)[0].name) || fields.eq(index+1)[0].disabled){
                                index = index+1;
                            }else {
                                fields.eq(index+1).focus();
                                index = -1;
                            }
                        }

                    } catch (e) {
                        console.log("error");
                    }
                }
            });
        }
    };
    return directive;

});
