package cl.ccs.siac.utils;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import cl.ccs.siac.pdf.CustomFooter;
import cl.ccs.siac.pdf.FilesUtils;
import cl.ccs.siac.pdf.PdfUtil;
import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.MorosidadVigenteDTO;
import cl.exe.ccs.dto.ProtestoVigenteDTO;
import cl.exe.ccs.resources.Constants;

/**
 * @author rbocaz
 *
 */
public class BuildPDF {

	private static final int REG_PER_PAGE = 20;
	private String codAutenticacion;
	private PdfUtil pdfUtil = new PdfUtil();
	private NumberUtil formateos = NumberUtil.getInstance();
	private int totalPaginas;
	private String rutaPDF;

	/**
	 * Construye los PDF para certificados entregados en la web
	 * 
	 * @param consultaBIC
	 * @param consultaMOLDTO
	 * @param username
	 * @param descCentroCostro
	 * @return
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public String buildPDF(ConsultaBICDTO consultaBIC, ConsultaMOLDTO consultaMOLDTO, String username,
			String descCentroCostro) {

		String nombre = consultaBIC.getApPat().trim() + ", " + consultaBIC.getApMat().trim() + ", "
				+ consultaBIC.getNombres().trim();

		List<ProtestoVigenteDTO> listProtesto = consultaBIC.getProtVigentes();
		List<MorosidadVigenteDTO> listMorosidad = consultaMOLDTO.getListMorosidadVig();

		try {

			Paragraph saltoLinea;
			Paragraph title = new Paragraph(12);
			if (!isJuridico(consultaBIC.getRut())) {
				Font fontTitle = new Font(Font.FontFamily.HELVETICA, 10);
				fontTitle.setStyle(Font.BOLD);
				Chunk chunkText = new Chunk(
						"CERTIFICADO PARA FINES ESPECIALES\nINCLUYE OBLIGACIONES VENCIDAS Y NO PAGADAS EN BOLETÍN COMERCIAL\nY BASE DE DATOS DE MOROSIDAD DE LOS SISTEMAS FINANCIERO/COMERCIAL",
						fontTitle);
				title.setSpacingBefore(72);
				title.add(chunkText);
				title.setAlignment(Element.ALIGN_CENTER);
			} else {

				Font fontTitle = new Font(Font.FontFamily.HELVETICA, 10);
				fontTitle.setStyle(Font.BOLD);
				Chunk chunkText = new Chunk(
						"CERTIFICADO\nINCLUYE OBLIGACIONES VENCIDAS Y NO PAGADAS EN BOLETÍN COMERCIAL\nY BASE DE DATOS DE MOROSIDAD DE LOS SISTEMAS FINANCIERO/COMERCIAL",
						fontTitle);
				title.setSpacingBefore(72);
				title.add(chunkText);
				title.setAlignment(Element.ALIGN_CENTER);
			}

			Document document = new Document(PageSize.LETTER);

			int currentRegPerPage = 0;
			int currentPage = 1;
			int cont = 1;
			int cont1 = 1;
			int cantRegMol = listMorosidad.size();
			int cantRegBic = listProtesto.size();
			int countTotal = 0;
			int cantidadRegistros = cantRegMol + cantRegBic;
			int cantCH = 0;
			int cantCMProt = 0;
			int cantLTPG = 0;
			int cantRA = 0;
			int cantLT = 0;
			int cantPG = 0;
			int cantTC = 0;
			int cantOtMor = 0;
			int totalPages = (cantidadRegistros / REG_PER_PAGE) + ((cantidadRegistros % REG_PER_PAGE) > 0 ? 1 : 0);

			totalPages = totalPages == 0 ? 1 : totalPages;

			setCodAutenticacion(
					consultaBIC.getCodAutenticBic().trim() + "-" + consultaMOLDTO.getCodAutenticMol().trim());
			String outFileName = getCodAutenticacion().replaceAll("-", "_") + ".pdf";
			setRutaPDF(outFileName);

			FileOutputStream file = new FileOutputStream(Constants.DIR_CERTIFICATES + "/" + outFileName);

			setTotalPaginas(totalPages);

			PdfWriter writer = PdfWriter.getInstance(document, file);

			CustomFooter customFooter = new CustomFooter();
			customFooter.setText("Certificado Tipo 2 / Código de Verificación " + getCodAutenticacion());
			writer.setPageEvent(customFooter);

			pdfUtil.setFontSize(8);

			// margins: left, right, top, bottom
			document.setMargins(15f, 15f, 55f, 20f);
			document.open();

			if (totalPages == 1) {
				// document.add(imageFinal);
			} else {
				// document.add(image);
				writeContinue(writer);
			}

			document.add(title);

			saltoLinea = new Paragraph(12);
			saltoLinea.add("\n");
			document.add(saltoLinea);
			PdfPTable headerTable = getPDFHeader(consultaBIC.getRut(), nombre, cantidadRegistros, cantRegBic,
					cantRegMol, 1);
			document.add(headerTable);
			saltoLinea = new Paragraph(12);
			saltoLinea.add("\n");
			document.add(saltoLinea);

			// DATA
			PdfPTable tblData = getPDFHeaderResultBic();
			PdfPTable tblData2 = getPDFHeaderResultMol();

			PdfPCell cell = new PdfPCell();

			SimpleDateFormat fmtDate = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat fmtDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			if (cantRegBic == 0) {

				cell = new PdfPCell();
				cell = pdfUtil.setDisableBorderT(pdfUtil.getCell("No registra información en el Boletín Comercial."));
				cell.setColspan(9);
				tblData.addCell(cell);

			} else {
				for (ProtestoVigenteDTO protesto : listProtesto) {
					countTotal++;
					cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell(
							protesto.getNroBoletin() + "/" + formateos.fillWithZeroPdf(protesto.getPagBoletin())));
					tblData.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(
							pdfUtil.getCell(fmtDate.format(fmtDate1.parse(protesto.getFecProt()))));
					tblData.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(protesto.getTipoDocumento()));
					tblData.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(protesto.getTipoDocImpago()));
					tblData.addCell(cell);

					if (protesto.getTipoDocumento().trim().equalsIgnoreCase("LT")
							|| protesto.getTipoDocumento().trim().equalsIgnoreCase("PG")
							|| protesto.getTipoDocumento().trim().equalsIgnoreCase("RA")) {
						cell = new PdfPCell();
						cell = pdfUtil
								.setDisableBorderTRBL(pdfUtil.getCell(protesto.getCodEmisor(), Element.ALIGN_CENTER));
						tblData.addCell(cell);
					} else {
						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRBL(pdfUtil
								.getCell(formateos.fillWithZeroPdf(protesto.getNroOper4Dig()), Element.ALIGN_CENTER));
						tblData.addCell(cell);
					}

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(protesto.getGlosaCorta()));
					tblData.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(
							pdfUtil.getCell(formateos.amountPdf(protesto.getMontoProt()), Element.ALIGN_RIGHT));
					tblData.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(protesto.getGlosaLocPub()));
					tblData.addCell(cell);

					String EmiLib = "";

					if (protesto.getTipoDocumento().trim().equalsIgnoreCase("LT")
							|| protesto.getTipoDocumento().trim().equalsIgnoreCase("PG")
							|| protesto.getTipoDocumento().trim().equalsIgnoreCase("RA")) {
						EmiLib = protesto.getNombreLibrador();

					} else {
						EmiLib = protesto.getGlosaEmisor();
					}

					if (EmiLib.length() > 30) {
						EmiLib = EmiLib.substring(0, 30);
					}

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell(EmiLib));
					tblData.addCell(cell);

					if (protesto.getTipoDocumento().trim().equalsIgnoreCase("LT")
							|| protesto.getTipoDocumento().trim().equalsIgnoreCase("PG")) {
						cantLTPG = cantLTPG + 1;
					} else if (protesto.getTipoDocumento().trim().equalsIgnoreCase("CH")) {
						cantCH = cantCH + 1;
					} else if (protesto.getTipoDocumento().trim().equalsIgnoreCase("CM")) {
						cantCMProt = cantCMProt + 1;
					} else {
						cantRA = cantRA + 1;
					}

					if (cont == cantRegBic) {

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTB(pdfUtil.getCell("RESUMEN BC", Element.ALIGN_LEFT, Font.BOLD));
						cell.setColspan(9);
						tblData.addCell(cell);

						///////////////////////////////////////////////////////////////////////
						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTR(pdfUtil.getCell("Cheques:  " + cantCH));
						tblData.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell("Letras y Pagarés:  " + cantLTPG));
						cell.setColspan(4);
						tblData.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell("Cuotas Morosas:  " + cantCMProt));
						cell.setColspan(2);
						tblData.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTL(pdfUtil.getCell("Rentas de Arrendamiento:  " + cantRA));
						cell.setColspan(2);
						tblData.addCell(cell);

					}

					++currentRegPerPage;
					++cont;

					if (cont <= cantRegBic && currentRegPerPage == REG_PER_PAGE) {

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTR(pdfUtil.getCell(""));
						tblData.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell(""));
						cell.setColspan(7);
						tblData.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTL(pdfUtil.getCell(""));
						tblData.addCell(cell);

						document.add(tblData);

						document.setMargins(15f, 15f, 10f, 20f);
						document.newPage();

						currentPage++;
						if (currentPage == totalPages) {
							// document.add(imageFinal);
						} else {
							// document.add(image);
							writeContinue(writer);
						}

						document.add(title);

						saltoLinea = new Paragraph(12);
						saltoLinea.add("\n");
						document.add(saltoLinea);

						document.add(getPDFHeader(consultaBIC.getRut(), nombre, cantidadRegistros, cantRegBic,
								cantRegMol, currentPage));

						saltoLinea = new Paragraph(12);
						saltoLinea.add("\n");
						document.add(saltoLinea);

						tblData = getPDFHeaderResultBic();

						currentRegPerPage = 0;

					}

				}
			}
			document.add(tblData);
			saltoLinea = new Paragraph(12);
			saltoLinea.add("\n");
			document.add(saltoLinea);

			if (cantRegMol == 0) {
				cell = new PdfPCell();
				cell = pdfUtil.setDisableBorderT(pdfUtil.getCell(
						"No registra información en Base de Datos de Morosidad de los sistemas financiero/comercial de la Cámara de Comercio de Santiago."));
				cell.setColspan(6);
				tblData2.addCell(cell);
			} else {
				if ((cantRegBic > 0) && (cantRegBic % REG_PER_PAGE) == 0) {

					currentRegPerPage = 0;
					document.newPage();

					currentPage++;
					if (currentPage == totalPages) {
						// document.add(imageFinal);
					} else {
						// document.add(image);
						writeContinue(writer);
					}

					document.add(title);

					saltoLinea = new Paragraph(12);
					saltoLinea.add("\n");
					document.add(saltoLinea);

					document.add(getPDFHeader(consultaBIC.getRut(), nombre, cantidadRegistros, cantRegBic, cantRegMol,
							currentPage));

					saltoLinea = new Paragraph(12);
					saltoLinea.add("\n");
					document.add(saltoLinea);

					tblData = getPDFHeaderResultMol();

				}
				// currentRegPerPage=0
				for (MorosidadVigenteDTO morosidad : listMorosidad) {

					cell = new PdfPCell();
					if (morosidad.getFechaVcto() != null) {
						cell = pdfUtil.setDisableBorderTRB(
								pdfUtil.getCell(fmtDate.format(fmtDate1.parse(morosidad.getFechaVcto()))));
					}
					tblData2.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(morosidad.getTipoCredito()));
					tblData2.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(morosidad.getMoneda()));
					tblData2.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(
							pdfUtil.getCell(formateos.amountPdf(morosidad.getMontoDeuda()), Element.ALIGN_RIGHT));
					tblData2.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(""));
					tblData2.addCell(cell);

					cell = new PdfPCell();
					cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell(morosidad.getNombreEmisor()));
					tblData2.addCell(cell);

					if (morosidad.getTipoCredito().trim().equalsIgnoreCase("TC")) {
						cantTC = cantTC + 1;
					} else if (morosidad.getTipoCredito().trim().equalsIgnoreCase("LT")) {
						cantLT = cantLT + 1;
					} else if (morosidad.getTipoCredito().trim().equalsIgnoreCase("PG")) {
						cantPG = cantPG + 1;
					} else {
						cantOtMor = cantOtMor + 1;
					}

					if (cont1 == cantRegMol) {

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTB(pdfUtil.getCell("RESUMEN MC", Element.ALIGN_LEFT, Font.BOLD));
						cell.setColspan(6);
						tblData2.addCell(cell);

						//////////////////////////////////////////////////////////////////////////////

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell("Tarjetas:  " + cantTC));
						tblData2.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Letras:  " + cantLT));
						cell.setColspan(2);
						tblData2.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Pagar�s:  " + cantPG));
						cell.setColspan(2);
						tblData2.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell("Otras Publicaciones:  " + cantOtMor));
						tblData2.addCell(cell);

						//////////////////////////////////////////////////////////////////////////////

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderT(pdfUtil.getCell(
								"La morosidad detallada corresponde a la información registrada en la Base de Datos de Morosidad de los sistemas financiero/comercial de la Cámara de Comercio de Santiago."));
						cell.setColspan(6);
						tblData2.addCell(cell);

					}

					++currentRegPerPage;
					++cont1;
					if (cont1 <= cantRegMol && currentRegPerPage == REG_PER_PAGE) {

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTR(pdfUtil.getCell(""));
						tblData2.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell(""));
						cell.setColspan(4);
						tblData2.addCell(cell);

						cell = new PdfPCell();
						cell = pdfUtil.setDisableBorderTL(pdfUtil.getCell(""));
						tblData2.addCell(cell);

						document.add(tblData2);
						document.setMargins(15f, 15f, 10f, 20f);
						document.newPage();

						currentPage++;
						if (currentPage == totalPages) {
							// document.add(imageFinal);
						} else {
							// document.add(image);
							writeContinue(writer);
						}
						document.add(title);

						saltoLinea = new Paragraph(12);
						saltoLinea.add("\n");
						document.add(saltoLinea);

						document.add(getPDFHeader(consultaBIC.getRut(), nombre, cantidadRegistros, cantRegBic,
								cantRegMol, currentPage));

						saltoLinea = new Paragraph(12);
						saltoLinea.add("\n");
						document.add(saltoLinea);

						tblData2 = getPDFHeaderResultMol();

						currentRegPerPage = 0;

					}

				}
			}
			document.add(tblData2);

			if (!isJuridico(consultaBIC.getRut())) {

				String footerLey = "Este certificado se emite a solicitud del titular de los datos, de conformidad con el artículo N° de la Ley N°20.575.";
				Font f = new Font(FontFamily.HELVETICA, 10.0f, Font.NORMAL);
				pdfUtil.writeTextPositionEditable(writer, footerLey, 15, 180, f);
				pdfUtil.setFontSize(8);
			}

			String firma1 = username + " - " + descCentroCostro;
			Font firmaFont = new Font(FontFamily.HELVETICA, 8.0f, Font.NORMAL);
			pdfUtil.writeTextPositionEditable(writer, firma1, 15, 100, firmaFont);
			pdfUtil.setFontSize(8);

			String firma2 = "P.P. CÁMARA DE COMERCIO DE SANTIAGO";
			Font firmaFont2 = new Font(FontFamily.HELVETICA, 8.0f, Font.NORMAL);
			pdfUtil.writeTextPositionEditable(writer, firma2, 400, 100, firmaFont2);
			pdfUtil.setFontSize(8);

			String footerText = "Para verificar su validez ingrese este código en www.boletincomercial.cl en la opción Verificación de Certificados.";
			Font f = new Font(FontFamily.HELVETICA, 10.0f, Font.BOLD);
			pdfUtil.writeTextPositionEditable(writer, footerText, 15, 50, f);
			pdfUtil.setFontSize(8);

			document.close();

			file.flush();

			file.close();

			FilesUtils.getInstance().copyFile(outFileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return getCodAutenticacion();

	}

	/**
	 * Setea la ruta fisica para el PDF
	 * 
	 * @param outFileName
	 */
	private void setRutaPDF(String outFileName) {
		// TODO Auto-generated method stub
		this.rutaPDF = outFileName;

	}

	/**
	 * Obtiene la ruta fisica para el PDF
	 * 
	 * @return
	 */
	public String getRutaPDF() {
		return rutaPDF;
	}

	/**
	 * Setea el total de paginas del Informe
	 * 
	 * @param totalPages
	 */
	private void setTotalPaginas(int totalPages) {
		// TODO Auto-generated method stub
		this.totalPaginas = totalPages;
	}

	/**
	 * Setea el Codigo de Autenticacion para imprimir en el PDF
	 * 
	 * @param codAutenticacion
	 */
	private void setCodAutenticacion(String codAutenticacion) {
		this.codAutenticacion = codAutenticacion;
	}

	/**
	 * Obtiene el Codigo de Autenticacion para imprimir en el PDF
	 * 
	 * @return
	 */
	private String getCodAutenticacion() {
		return codAutenticacion;
	}

	/**
	 * Obtiene el nombre del archivo
	 * 
	 * @return
	 */
	public String getFilename() {
		return codAutenticacion.replaceAll("-", "_");
	}

	/**
	 * Obtiene el nombre del archivo PDF
	 * 
	 * @return
	 */
	public String getFilenamePdf() {
		return getFilename() + ".pdf";
	}

	/**
	 * Imprime la continuidad de los informes segun amerite
	 * 
	 * @param writer
	 */
	private void writeContinue(PdfWriter writer) {
		String text = "Continúa ...";
		Font fontContinue = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		Chunk chunkContinue = new Chunk(text);
		chunkContinue.setFont(fontContinue);
		pdfUtil.writeTextPosition(writer, chunkContinue, 250, 200);

	}

	/**
	 * Obtiene la cabecera para el informe PDF de la Base de Datos BIC
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public PdfPTable getPDFHeaderResultBic() throws Exception {
		PdfPTable tblHeaderResult = new PdfPTable(9);

		tblHeaderResult.setWidths(new int[] { 10, // Boletin/Pagina
				9, // Fec Prot
				3, // TD
				3, // TC
				8, // NroOper
				4, // Mda
				16, // Monto
				16, // Ciudad
				31 // Emisor

		});
		tblHeaderResult.setWidthPercentage(100);
		tblHeaderResult.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);

		PdfPCell cell = new PdfPCell();

		cell = pdfUtil
				.setDisableBorderB(pdfUtil.getCell("BOLETÍN COMERCIAL VIGENTES - BC", Element.ALIGN_LEFT, Font.BOLD));
		cell.setColspan(9);
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell("Boletín Publicación", Element.ALIGN_CENTER, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Fecha Prot/Venc.", Element.ALIGN_CENTER, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("TD", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("TC", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("No Ch/ Not/Juz", Element.ALIGN_CENTER, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Mda", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Monto", Element.ALIGN_RIGHT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Ciud./Local.", Element.ALIGN_CENTER, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell("Banco/Librador/Arrendador", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		return tblHeaderResult;
	}

	/**
	 * Obtiene la cabecera para el informe PDF de la Base de Datos MOL
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public PdfPTable getPDFHeaderResultMol() throws Exception {
		PdfPTable tblHeaderResult = new PdfPTable(6);

		tblHeaderResult.setWidths(new int[] { 12, // Fecha
				9, // Tipo Credito
				10, // Mda
				17, // Monto
				2, 50 // Emisor
		});
		tblHeaderResult.setWidthPercentage(100);
		tblHeaderResult.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);

		PdfPCell cell = new PdfPCell();

		cell = pdfUtil.setDisableBorderB(
				pdfUtil.getCell("MOROSIDAD SISTEMAS FINANCIERO/COMERCIAL", Element.ALIGN_LEFT, Font.BOLD));
		cell.setColspan(6);
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell("Fecha Publ/Venc.", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Tipo Credito", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Mda.", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("Monto", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(""));
		tblHeaderResult.addCell(cell);

		cell = new PdfPCell();
		cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell("Emisor", Element.ALIGN_LEFT, Font.BOLD));
		tblHeaderResult.addCell(cell);

		return tblHeaderResult;
	}

	/**
	 * Obtiene la cabecera general para el informe PDF
	 * 
	 * @param rut
	 * @param nombre
	 * @param cantTotalReg
	 * @param cantRegBic
	 * @param cantRegMol
	 * @param currentPage
	 * @return
	 */
	@SuppressWarnings("static-access")
	private PdfPTable getPDFHeader(String rut, String nombre, Integer cantTotalReg, Integer cantRegBic,
			Integer cantRegMol, int currentPage) {

		PdfPTable tblHeader = new PdfPTable(10);
		SimpleDateFormat fmtDate = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaActual = new Date();

		try {
			tblHeader.setWidths(new int[] { 2, // Margin
					5, // Rut label
					2, // :
					22, // Rut value
					2, 5, 28, //
					8, 2, 9 });
			tblHeader.setWidthPercentage(100);
			tblHeader.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);

			PdfPCell cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRB(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell("RUT", Element.ALIGN_LEFT, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell(":", Element.ALIGN_CENTER, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil
					.setDisableBorderRBL(pdfUtil.getCell(formateos.formatRut(rut), Element.ALIGN_LEFT, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell("PÁGINA", Element.ALIGN_LEFT, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderRBL(pdfUtil.getCell(":", Element.ALIGN_CENTER, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderBL(pdfUtil.getCell(currentPage + "/" + totalPaginas));
			tblHeader.addCell(cell);

			//////////////////////////////////////////////////

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("NOMBRE", Element.ALIGN_LEFT, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(":", Element.ALIGN_CENTER, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(nombre, Element.ALIGN_LEFT, Font.BOLD));
			cell.setColspan(4);
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("FECHA", Element.ALIGN_LEFT, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(":", Element.ALIGN_CENTER, Font.BOLD));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell(fechaActual));
			tblHeader.addCell(cell);

			/////////////////////////////////////////////////////////////////

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(
					pdfUtil.getCell("PUBLICACIONES VIGENTES", Element.ALIGN_LEFT, Font.BOLD | Font.UNDERLINE));
			cell.setColspan(3);
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(":"));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(cantTotalReg));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(""));
			cell.setColspan(3);
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			/////////////////////////////////////////////////////////////////

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRB(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell("BOLETÍN COMERCIAL", Element.ALIGN_LEFT, Font.BOLD));
			cell.setColspan(3);
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(":"));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRBL(pdfUtil.getCell(cantRegBic));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTBL(pdfUtil.getCell(""));
			cell.setColspan(4);
			tblHeader.addCell(cell);

			/////////////////////////////////////////////////////////////////

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTR(pdfUtil.getCell(""));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRL(
					pdfUtil.getCell("MOROSIDAD SISTEMAS FINANCIERO/COMERCIAL", Element.ALIGN_LEFT, Font.BOLD));
			cell.setColspan(3);
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell(":"));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTRL(pdfUtil.getCell(cantRegMol));
			tblHeader.addCell(cell);

			cell = new PdfPCell();
			cell = pdfUtil.setDisableBorderTL(pdfUtil.getCell(""));
			cell.setColspan(4);
			tblHeader.addCell(cell);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tblHeader;
	}

	/**
	 * Metodo para retornar un booleano que indica si es persona juridica por su
	 * rut.
	 * 
	 * @param rut
	 * @return
	 */
	public boolean isJuridico(String rut) {

		String[] rutSplit = rut.split("-");
		Integer rutNum = Integer.parseInt(rutSplit[0]);
		if (rutNum >= 50000000) {
			return true;
		} else {
			return false;
		}
	}
}
