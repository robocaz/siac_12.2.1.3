package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_DETESTADOSENVIO database table.
 * 
 */
@Entity
@Table(name="TBL_DETESTADOSENVIO")
@NamedQuery(name="TblDetestadosenvio.findAll", query="SELECT t FROM TblDetestadosenvio t")
public class TblDetestadosenvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CORRENVIODIG")
	private BigDecimal fldCorrenviodig;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAESTADO")
	private Date fldFechaestado;

	@Column(name="FLD_USUARIO")
	private String fldUsuario;

	public TblDetestadosenvio() {
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCorrenviodig() {
		return this.fldCorrenviodig;
	}

	public void setFldCorrenviodig(BigDecimal fldCorrenviodig) {
		this.fldCorrenviodig = fldCorrenviodig;
	}

	public Date getFldFechaestado() {
		return this.fldFechaestado;
	}

	public void setFldFechaestado(Date fldFechaestado) {
		this.fldFechaestado = fldFechaestado;
	}

	public String getFldUsuario() {
		return this.fldUsuario;
	}

	public void setFldUsuario(String fldUsuario) {
		this.fldUsuario = fldUsuario;
	}

}