package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdDetCedulas database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdDetCedulas")
@NamedQuery(name="Tb_CeIdDetCedula.findAll", query="SELECT t FROM Tb_CeIdDetCedula t")
public class Tb_CeIdDetCedula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDDETCEDULAS_FLD_CORRCEDULA_ID_GENERATOR", sequenceName="FLD_CORRCEDULA_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDDETCEDULAS_FLD_CORRCEDULA_ID_GENERATOR")
	@Column(name="Fld_CorrCedula_Id")
	private long fld_CorrCedula_Id;

	@Column(name="Fld_AnoInsNacimiento")
	private short fld_AnoInsNacimiento;

	@Column(name="Fld_CodCedulaInf")
	private String fld_CodCedulaInf;

	@Column(name="Fld_CodCedulaSup")
	private String fld_CodCedulaSup;

	@Column(name="Fld_CodCircunsElect")
	private short fld_CodCircunsElect;

	@Column(name="Fld_CodEstado")
	private int fld_CodEstado;

	@Column(name="Fld_CodEstCivil")
	private String fld_CodEstCivil;

	@Column(name="Fld_CodNacionalidad")
	private byte fld_CodNacionalidad;

	@Column(name="Fld_CodPais")
	private short fld_CodPais;

	@Column(name="Fld_CodProfesion")
	private short fld_CodProfesion;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecNacimiento")
	private Timestamp fld_FecNacimiento;

	@Column(name="Fld_FecVencCedula")
	private Timestamp fld_FecVencCedula;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_GlosaInsNacimiento")
	private String fld_GlosaInsNacimiento;

	@Column(name="Fld_MarcaBloqueo")
	private boolean fld_MarcaBloqueo;

	@Column(name="Fld_MarcaDefuncion")
	private boolean fld_MarcaDefuncion;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_NroInscripElect")
	private short fld_NroInscripElect;

	@Column(name="Fld_NroInsNacimiento")
	private String fld_NroInsNacimiento;

	@Column(name="Fld_NroRegistroElect")
	private short fld_NroRegistroElect;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_Sexo")
	private boolean fld_Sexo;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	//bi-directional many-to-one association to Tb_CeIdEnvio
	@ManyToOne
	@JoinColumn(name="Fld_CorrEnvio")
	private Tb_CeIdEnvio tbCeIdEnvio;

	//bi-directional many-to-one association to Tb_CeIdDetErrCedula
	@OneToMany(mappedBy="tbCeIdDetCedula")
	private List<Tb_CeIdDetErrCedula> tbCeIdDetErrCedulas;

	public Tb_CeIdDetCedula() {
	}

	public long getFld_CorrCedula_Id() {
		return this.fld_CorrCedula_Id;
	}

	public void setFld_CorrCedula_Id(long fld_CorrCedula_Id) {
		this.fld_CorrCedula_Id = fld_CorrCedula_Id;
	}

	public short getFld_AnoInsNacimiento() {
		return this.fld_AnoInsNacimiento;
	}

	public void setFld_AnoInsNacimiento(short fld_AnoInsNacimiento) {
		this.fld_AnoInsNacimiento = fld_AnoInsNacimiento;
	}

	public String getFld_CodCedulaInf() {
		return this.fld_CodCedulaInf;
	}

	public void setFld_CodCedulaInf(String fld_CodCedulaInf) {
		this.fld_CodCedulaInf = fld_CodCedulaInf;
	}

	public String getFld_CodCedulaSup() {
		return this.fld_CodCedulaSup;
	}

	public void setFld_CodCedulaSup(String fld_CodCedulaSup) {
		this.fld_CodCedulaSup = fld_CodCedulaSup;
	}

	public short getFld_CodCircunsElect() {
		return this.fld_CodCircunsElect;
	}

	public void setFld_CodCircunsElect(short fld_CodCircunsElect) {
		this.fld_CodCircunsElect = fld_CodCircunsElect;
	}

	public int getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(int fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodEstCivil() {
		return this.fld_CodEstCivil;
	}

	public void setFld_CodEstCivil(String fld_CodEstCivil) {
		this.fld_CodEstCivil = fld_CodEstCivil;
	}

	public byte getFld_CodNacionalidad() {
		return this.fld_CodNacionalidad;
	}

	public void setFld_CodNacionalidad(byte fld_CodNacionalidad) {
		this.fld_CodNacionalidad = fld_CodNacionalidad;
	}

	public short getFld_CodPais() {
		return this.fld_CodPais;
	}

	public void setFld_CodPais(short fld_CodPais) {
		this.fld_CodPais = fld_CodPais;
	}

	public short getFld_CodProfesion() {
		return this.fld_CodProfesion;
	}

	public void setFld_CodProfesion(short fld_CodProfesion) {
		this.fld_CodProfesion = fld_CodProfesion;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecNacimiento() {
		return this.fld_FecNacimiento;
	}

	public void setFld_FecNacimiento(Timestamp fld_FecNacimiento) {
		this.fld_FecNacimiento = fld_FecNacimiento;
	}

	public Timestamp getFld_FecVencCedula() {
		return this.fld_FecVencCedula;
	}

	public void setFld_FecVencCedula(Timestamp fld_FecVencCedula) {
		this.fld_FecVencCedula = fld_FecVencCedula;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public String getFld_GlosaInsNacimiento() {
		return this.fld_GlosaInsNacimiento;
	}

	public void setFld_GlosaInsNacimiento(String fld_GlosaInsNacimiento) {
		this.fld_GlosaInsNacimiento = fld_GlosaInsNacimiento;
	}

	public boolean getFld_MarcaBloqueo() {
		return this.fld_MarcaBloqueo;
	}

	public void setFld_MarcaBloqueo(boolean fld_MarcaBloqueo) {
		this.fld_MarcaBloqueo = fld_MarcaBloqueo;
	}

	public boolean getFld_MarcaDefuncion() {
		return this.fld_MarcaDefuncion;
	}

	public void setFld_MarcaDefuncion(boolean fld_MarcaDefuncion) {
		this.fld_MarcaDefuncion = fld_MarcaDefuncion;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public short getFld_NroInscripElect() {
		return this.fld_NroInscripElect;
	}

	public void setFld_NroInscripElect(short fld_NroInscripElect) {
		this.fld_NroInscripElect = fld_NroInscripElect;
	}

	public String getFld_NroInsNacimiento() {
		return this.fld_NroInsNacimiento;
	}

	public void setFld_NroInsNacimiento(String fld_NroInsNacimiento) {
		this.fld_NroInsNacimiento = fld_NroInsNacimiento;
	}

	public short getFld_NroRegistroElect() {
		return this.fld_NroRegistroElect;
	}

	public void setFld_NroRegistroElect(short fld_NroRegistroElect) {
		this.fld_NroRegistroElect = fld_NroRegistroElect;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public boolean getFld_Sexo() {
		return this.fld_Sexo;
	}

	public void setFld_Sexo(boolean fld_Sexo) {
		this.fld_Sexo = fld_Sexo;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public Tb_CeIdEnvio getTbCeIdEnvio() {
		return this.tbCeIdEnvio;
	}

	public void setTbCeIdEnvio(Tb_CeIdEnvio tbCeIdEnvio) {
		this.tbCeIdEnvio = tbCeIdEnvio;
	}

	public List<Tb_CeIdDetErrCedula> getTbCeIdDetErrCedulas() {
		return this.tbCeIdDetErrCedulas;
	}

	public void setTbCeIdDetErrCedulas(List<Tb_CeIdDetErrCedula> tbCeIdDetErrCedulas) {
		this.tbCeIdDetErrCedulas = tbCeIdDetErrCedulas;
	}

	public Tb_CeIdDetErrCedula addTbCeIdDetErrCedula(Tb_CeIdDetErrCedula tbCeIdDetErrCedula) {
		getTbCeIdDetErrCedulas().add(tbCeIdDetErrCedula);
		tbCeIdDetErrCedula.setTbCeIdDetCedula(this);

		return tbCeIdDetErrCedula;
	}

	public Tb_CeIdDetErrCedula removeTbCeIdDetErrCedula(Tb_CeIdDetErrCedula tbCeIdDetErrCedula) {
		getTbCeIdDetErrCedulas().remove(tbCeIdDetErrCedula);
		tbCeIdDetErrCedula.setTbCeIdDetCedula(null);

		return tbCeIdDetErrCedula;
	}

}