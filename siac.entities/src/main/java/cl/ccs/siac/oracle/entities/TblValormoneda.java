package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_VALORMONEDA database table.
 * 
 */
@Entity
@Table(name="TBL_VALORMONEDA")
@NamedQuery(name="TblValormoneda.findAll", query="SELECT t FROM TblValormoneda t")
public class TblValormoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_VALORMONEDA")
	private BigDecimal fldValormoneda;

	//bi-directional many-to-one association to TblMoneda
	@ManyToOne
	@JoinColumn(name="FLD_CODMONEDA")
	private TblMoneda tblMoneda;

	public TblValormoneda() {
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public BigDecimal getFldValormoneda() {
		return this.fldValormoneda;
	}

	public void setFldValormoneda(BigDecimal fldValormoneda) {
		this.fldValormoneda = fldValormoneda;
	}

	public TblMoneda getTblMoneda() {
		return this.tblMoneda;
	}

	public void setTblMoneda(TblMoneda tblMoneda) {
		this.tblMoneda = tblMoneda;
	}

}