package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rut_desp database table.
 * 
 */
@Entity
@Table(name="tmp_rut_desp")
@NamedQuery(name="TmpRutDesp.findAll", query="SELECT t FROM TmpRutDesp t")
public class TmpRutDesp implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rut;

	public TmpRutDesp() {
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}