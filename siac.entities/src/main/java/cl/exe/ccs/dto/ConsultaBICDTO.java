package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ConsultaBICDTO implements Serializable {
	
	
    private String rut;
    private String apPat;
    private String apMat;
    private String nombres;
    private String nombreCompleto;
    private Integer nroAcls;
    private String fecPubAcl;
    private String fecPubAcl2;
    private String codAutenticBic;
    private List<ProtestoVigenteDTO> protVigentes;
    
    
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getApPat() {
		return apPat;
	}
	public void setApPat(String apPat) {
		this.apPat = apPat;
	}
	public String getApMat() {
		return apMat;
	}
	public void setApMat(String apMat) {
		this.apMat = apMat;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public Integer getNroAcls() {
		return nroAcls;
	}
	public void setNroAcls(Integer nroAcls) {
		this.nroAcls = nroAcls;
	}
	public String getFecPubAcl() {
		return fecPubAcl;
	}
	public void setFecPubAcl(String fecPubAcl) {
		this.fecPubAcl = fecPubAcl;
	}
	public String getFecPubAcl2() {
		return fecPubAcl2;
	}
	public void setFecPubAcl2(String fecPubAcl2) {
		this.fecPubAcl2 = fecPubAcl2;
	}
	public String getCodAutenticBic() {
		return codAutenticBic;
	}
	public void setCodAutenticBic(String codAutenticBic) {
		this.codAutenticBic = codAutenticBic;
	}
	public List<ProtestoVigenteDTO> getProtVigentes() {
		return protVigentes;
	}
	public void setProtVigentes(List<ProtestoVigenteDTO> protVigentes) {
		this.protVigentes = protVigentes;
	}
    
    
    

}
