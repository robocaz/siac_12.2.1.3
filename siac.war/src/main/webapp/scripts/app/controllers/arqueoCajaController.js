angular.module('ccs').controller('ArqueoCajaController', function($scope, ArqueoCajaResource, $location, $filter, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.campo = {};
	$scope.campo.cash = 0;
	$scope.campo.fondo = 0;
	
	$scope.bill = {};
	$scope.bill.mil20 = {};
	$scope.bill.mil10 = {};
	$scope.bill.mil5 = {};
	$scope.bill.mil2 = {};
	$scope.bill.mil = {};
	$scope.bill.tot = {};
	
	$scope.coin = {};
	$scope.coin.c500 = {};
	$scope.coin.c100 = {};
	$scope.coin.c50 = {};
	$scope.coin.c10 = {};
	$scope.coin.c5 = {};
	$scope.coin.tot = {};

	$scope.tipo = {};
	$scope.tipo.lista = {};
	$scope.docs = {};
	$scope.docs.total = 0;
	$scope.docs.lista = {};
	$scope.impr = {};

	$('#arqueocaja').on('show.bs.modal', function (e) {
		console.log("-> Arqueo Caja..");
		$timeout(function(){
			$scope.init();
		});
	});

	$scope.init = function () {
		$scope.campo.usuario = $rootScope.usuario.username;
		$scope.campo.fecha = $filter('date')(new Date(), "ddMMyyyy");
		$scope.campo.caja = null;
		$scope.campo.rev = null;
		resetArqueo();
		cargaTablas();
	}

	function checkResultados() {
		let empty = true;
		if ( !isUndefinedOrNull($scope.tipo.lista) ) {
			if ( $scope.tipo.lista.length > 0 ) {
				empty = false;
			}
		}
		if ( !isUndefinedOrNull($scope.docs.lista) ) {
			if ( $scope.docs.lista.length > 0 ) {
				empty = false;
			}
		}
		if ( empty ) {
			swal('Alerta','No se encontraron datos','error');
		}
	}

	function resetArqueo() {
		$scope.bill.mil20.cant	= 0;
		$scope.bill.mil20.total	= 0;
		$scope.bill.mil10.cant	= 0;
		$scope.bill.mil10.total	= 0;
		$scope.bill.mil5.cant	= 0;
		$scope.bill.mil5.total	= 0;
		$scope.bill.mil2.cant	= 0;
		$scope.bill.mil2.total	= 0;
		$scope.bill.mil.cant	= 0;
		$scope.bill.mil.total	= 0;
		$scope.bill.tot.cant	= 0;
		$scope.bill.tot.total	= 0;
		
		$scope.coin.c500.cant	= 0;
		$scope.coin.c500.total	= 0;
		$scope.coin.c100.cant	= 0;
		$scope.coin.c100.total	= 0;
		$scope.coin.c50.cant	= 0;
		$scope.coin.c50.total	= 0;
		$scope.coin.c10.cant	= 0;
		$scope.coin.c10.total	= 0;
		$scope.coin.c5.cant		= 0;
		$scope.coin.c5.total	= 0;
		$scope.coin.tot.cant	= 0;
		$scope.coin.tot.total	= 0;
	}
	
	function cargaTablas() {
		console.log("cargaVentasPorTipoPago");
		
		$scope.trx = {
			fechaInicio : formatFechaStr($scope.campo.fecha),
			codUsuario 	: $rootScope.usuario.username,
			codCC 		: $rootScope.usuario.centroCostoActivo.codigo
		}
		
		var successCallback = function (data, responseHeaders) {
			if ( data.idControl === 1 ) {
				$scope.tipo.lista 	= data.lista;
				$scope.tipo.trxs 	= data.totalTrxs;
				$scope.tipo.pagado 	= data.totalPagado;
				$scope.tipo.calc 	= data.totalCalc;
				$scope.tipo.diff 	= data.totalDiff;
				$scope.tipo.dctos 	= data.totalDctos;
				cargaDetalleDocs();
			}
		}
		ArqueoCajaResource.tipopago($scope.trx, successCallback, $scope.errorCallback);
	}
	
	$scope.buscaInfo = function () {
		if ( isUndefinedNullEmpty($scope.campo.fecha) ) {
			return false;
		}
		
		if ( !isValidDate($scope.campo.fecha) ) {
			$scope.campo.fecha = '';
			return false;
		}
		
		let fechaIn = getDateStrFecha($scope.campo.fecha);
		let hoy = new Date();
		if ( fechaIn > hoy ) {
			swal('Alerta','Fecha no puede ser mayor a la fecha actual','error');
			return false;
		}
		cargaTablas();
	}
	
	function cargaDetalleDocs() {
		console.log("cargaDetalleDocs");
		
		$scope.trx = {
			fechaInicio : formatFechaStr($scope.campo.fecha),
			codUsuario 	: $rootScope.usuario.username,
			codCC 		: $rootScope.usuario.centroCostoActivo.codigo
		}
		
		var successCallback = function (data, responseHeaders) {
			if ( data.idControl === 1 ) {
				$scope.docs.lista = data.lista;
				if ( isUndefinedNullEmpty(data.montoTotal) ) {
					$scope.docs.total = 0;
				} else {
					$scope.docs.total = data.montoTotal;
				}
				calcTotalArqueo();
			}
			checkResultados();
		}
		ArqueoCajaResource.condcto($scope.trx, successCallback, $scope.errorCallback);
	}
	
	$scope.calcBill = function(e) {
		//console.log("-> calcBill: " + e.currentTarget.id + " value: " + e.currentTarget.value);
		
		if ( e.currentTarget.value == '' ) {
			eval("$scope.bill." + e.currentTarget.name + ".cant = 0");
			eval("$scope.bill." + e.currentTarget.name + ".total = 0");
		} else {
			switch (e.currentTarget.id) {
				case 'mil20c':
					$scope.bill.mil20.total = $scope.bill.mil20.cant * 20000;
				break;
				case 'mil20t':
					$scope.bill.mil20.cant = $scope.bill.mil20.total / 20000;
					if ( !esEntero($scope.bill.mil20.cant) ) {
						$scope.bill.mil20.cant = 0;
						$scope.bill.mil20.total = 0;
					}
				break;
				case 'mil10c':
					$scope.bill.mil10.total = $scope.bill.mil10.cant * 10000;
				break;
				case 'mil10t':
					$scope.bill.mil10.cant = $scope.bill.mil10.total / 10000;
					if ( !esEntero($scope.bill.mil10.cant) ) {
						$scope.bill.mil10.cant = 0;
						$scope.bill.mil10.total = 0;
					}
				break;
				case 'mil5c':
					$scope.bill.mil5.total = $scope.bill.mil5.cant * 5000;
				break;
				case 'mil5t':
					$scope.bill.mil5.cant = $scope.bill.mil5.total / 5000;
					if ( !esEntero($scope.bill.mil5.cant) ) {
						$scope.bill.mil5.cant = 0;
						$scope.bill.mil5.total = 0;
					}
				break;
				case 'mil2c':
					$scope.bill.mil2.total = $scope.bill.mil2.cant * 2000;
				break;
				case 'mil2t':
					$scope.bill.mil2.cant = $scope.bill.mil2.total / 2000;
					if ( !esEntero($scope.bill.mil2.cant) ) {
						$scope.bill.mil2.cant = 0;
						$scope.bill.mil2.total = 0;
					}
				break;
				case 'milc':
					$scope.bill.mil.total = $scope.bill.mil.cant * 1000;
				break;
				case 'milt':
					$scope.bill.mil.cant = $scope.bill.mil.total / 1000;
					if ( !esEntero($scope.bill.mil.cant) ) {
						$scope.bill.mil.cant = 0;
						$scope.bill.mil.total = 0;
					}
				break;
			}
		}
		calcTotalesBill();
	};
	
	function calcTotalesBill() {
		$scope.bill.tot.cant = parseInt($scope.bill.mil20.cant);
		$scope.bill.tot.cant = $scope.bill.tot.cant + parseInt($scope.bill.mil10.cant);
		$scope.bill.tot.cant = $scope.bill.tot.cant + parseInt($scope.bill.mil5.cant);
		$scope.bill.tot.cant = $scope.bill.tot.cant + parseInt($scope.bill.mil2.cant);
		$scope.bill.tot.cant = $scope.bill.tot.cant + parseInt($scope.bill.mil.cant);
		
		$scope.bill.tot.total = parseInt($scope.bill.mil20.total);
		$scope.bill.tot.total = $scope.bill.tot.total + parseInt($scope.bill.mil10.total);
		$scope.bill.tot.total = $scope.bill.tot.total + parseInt($scope.bill.mil5.total);
		$scope.bill.tot.total = $scope.bill.tot.total + parseInt($scope.bill.mil2.total);
		$scope.bill.tot.total = $scope.bill.tot.total + parseInt($scope.bill.mil.total);
		calcCash();
	}
	
	$scope.calcCoin = function(e) {
		//console.log("-> calcCoin: " + e.currentTarget.id + " value: " + e.currentTarget.value);
		
		if ( e.currentTarget.value == '' ) {
			eval("$scope.coin." + e.currentTarget.name + ".cant = 0");
			eval("$scope.coin." + e.currentTarget.name + ".total = 0");
		} else {
			switch (e.currentTarget.id) {
				case 'c500c':
					$scope.coin.c500.total = $scope.coin.c500.cant * 500;
				break;
				case 'c500t':
					$scope.coin.c500.cant = $scope.coin.c500.total / 500;
					if ( !esEntero($scope.coin.c500.cant) ) {
						$scope.coin.c500.cant = 0;
						$scope.coin.c500.total = 0;
					}
				break;
				case 'c100c':
					$scope.coin.c100.total = $scope.coin.c100.cant * 100;
				break;
				case 'c100t':
					$scope.coin.c100.cant = $scope.coin.c100.total / 100;
					if ( !esEntero($scope.coin.c100.cant) ) {
						$scope.coin.c100.cant = 0;
						$scope.coin.c100.total = 0;
					}
				break;
				case 'c50c':
					$scope.coin.c50.total = $scope.coin.c50.cant * 50;
				break;
				case 'c50t':
					$scope.coin.c50.cant = $scope.coin.c50.total / 50;
					if ( !esEntero($scope.coin.c50.cant) ) {
						$scope.coin.c50.cant = 0;
						$scope.coin.c50.total = 0;
					}
				break;
				case 'c10c':
					$scope.coin.c10.total = $scope.coin.c10.cant * 10;
				break;
				case 'c10t':
					$scope.coin.c10.cant = $scope.coin.c10.total / 10;
					if ( !esEntero($scope.coin.c10.cant) ) {
						$scope.coin.c10.cant = 0;
						$scope.coin.c10.total = 0;
					}
				break;
				case 'c5c':
					$scope.coin.c5.total = $scope.coin.c5.cant * 5;
				break;
				case 'c5t':
					$scope.coin.c5.cant = $scope.coin.c5.total / 5;
					if ( !esEntero($scope.coin.c5.cant) ) {
						$scope.coin.c5.cant = 0;
						$scope.coin.c5.total = 0;
					}
				break;
			}
		}
		calcTotalesCoin();
	};
	
	function calcTotalesCoin() {
		$scope.coin.tot.cant = parseInt($scope.coin.c500.cant);
		$scope.coin.tot.cant = $scope.coin.tot.cant + parseInt($scope.coin.c100.cant);
		$scope.coin.tot.cant = $scope.coin.tot.cant + parseInt($scope.coin.c50.cant);
		$scope.coin.tot.cant = $scope.coin.tot.cant + parseInt($scope.coin.c10.cant);
		$scope.coin.tot.cant = $scope.coin.tot.cant + parseInt($scope.coin.c5.cant);
		
		$scope.coin.tot.total = parseInt($scope.coin.c500.total);
		$scope.coin.tot.total = $scope.coin.tot.total + parseInt($scope.coin.c100.total);
		$scope.coin.tot.total = $scope.coin.tot.total + parseInt($scope.coin.c50.total);
		$scope.coin.tot.total = $scope.coin.tot.total + parseInt($scope.coin.c10.total);
		$scope.coin.tot.total = $scope.coin.tot.total + parseInt($scope.coin.c5.total);
		calcCash();
	}
	
	function calcCash() {
		$scope.campo.cash = parseInt($scope.bill.tot.total) + parseInt($scope.coin.tot.total);
		calcTotalArqueo();
	}
	
	$scope.cambiaFondo = function () {
		calcTotalArqueo();
	}
	
	function calcTotalArqueo() {
		let cash = 0;
		if ( $scope.campo.cash != '' ) {
			cash = parseInt($scope.campo.cash);
		}
		let fondo = 0;
		if ( !isUndefinedNullEmpty($scope.campo.fondo) ) {
			fondo = parseInt($scope.campo.fondo);
		}
		let docs = 0;
		if ( $scope.tipo.dctos != '' ) {
			docs = parseInt($scope.tipo.dctos);
		}
		console.log("cash: " + cash +" fondo: " + fondo +" docs: " + docs);
		$scope.campo.total = cash - fondo + docs;
		calcDiff();
	}
	
	function calcDiff() {
		let total = 0;
		if ( $scope.campo.total != '' ) {
			total = parseInt($scope.campo.total);
		}
		let tipo = 0;
		if ( !isUndefinedNullEmpty($scope.tipo.pagado) ) {
			tipo = parseInt($scope.tipo.pagado);
		}
		console.log("total: " + total +" tipo: " + tipo);
		$scope.campo.diff = total - tipo;
	}
	
	$scope.imprimir = function() {
		console.log("-> Imprimir");
		
		if ( !validaDatos() ) {
			return false;
		}
		
		trx = {
			caja		: $scope.campo.caja,
			fecha		: $scope.campo.fecha,
			user		: $rootScope.usuario.username,
			cc			: $rootScope.usuario.centroCostoActivo.codigo,
			ccDesc		: $rootScope.usuario.centroCostoActivo.descripcion,
			rev			: $scope.campo.rev,
			billNums	: getArrayArqueo(1),
			billTots	: getArrayArqueo(2),
			coinNums	: getArrayArqueo(3),
			coinTots	: getArrayArqueo(4),
			billTotal	: $scope.bill.tot.total,
			coinTotal	: $scope.coin.tot.total,
			totDocs		: $scope.tipo.dctos,
			totCash		: $scope.campo.cash,
			fondo		: $scope.campo.fondo,
			totArqueo	: $scope.campo.total,
			diff		: $scope.campo.diff,
			obs			: $scope.campo.obs
		}
		console.log("> PDF Trx:");
		console.log(trx);
		var successCallback = function (data, responseHeaders) {
			console.log("> PDF Resp:");
			console.log(data);
			
			// Base64 String
			var base64str = data.reporte;
			
			// Decode Base64 String, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			
			for ( var i = 0; i < len; i++ ) {
				view[i] = binary.charCodeAt(i);
			}
			
			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "arqueo_caja_diario.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
		ArqueoCajaResource.pdf(trx, successCallback, $scope.errorCallback);
	};
	
	function validaDatos() {
		console.log("-> Validando datos..");
		
		if ( isUndefinedNullEmpty($scope.campo.fecha) ) {
			swal('Error','Debe ingresar una fecha','error');
			return false;
		}
		
		if ( !isValidDate($scope.campo.fecha) ) {
			swal('Error','Debe ingresar una fecha válida','error');
			return false;
		}
		
		let fechaIn = getDateStrFecha($scope.campo.fecha);
		let hoy = new Date();
		if ( fechaIn > hoy ) {
			swal('Alerta','Fecha no puede ser mayor a la fecha actual','error');
			return false;
		}
		
		return true;
	}
	
	function getDateStrFecha(strFecha) {
		let dia = strFecha.substr(0, 2);
		let mes = strFecha.substr(2, 2);
		let anio = strFecha.substr(4, 4);
		let fecha = new Date(anio +'-'+ mes +'-'+ dia);
		return fecha;
	}
	
	function getArrayArqueo(id) {
		var array = new Array();
		switch (id) {
			case 1:
				array[0] = $scope.bill.mil20.cant;
				array[1] = $scope.bill.mil10.cant;
				array[2] = $scope.bill.mil5.cant;
				array[3] = $scope.bill.mil2.cant;
				array[4] = $scope.bill.mil.cant;
			break;
			case 2:
				array[0] = $scope.bill.mil20.total;
				array[1] = $scope.bill.mil10.total;
				array[2] = $scope.bill.mil5.total;
				array[3] = $scope.bill.mil2.total;
				array[4] = $scope.bill.mil.total;
			break;
			case 3:
				array[0] = $scope.coin.c500.cant;
				array[1] = $scope.coin.c100.cant;
				array[2] = $scope.coin.c50.cant;
				array[3] = $scope.coin.c10.cant;
				array[4] = $scope.coin.c5.cant;
			break;
			case 4:
				array[0] = $scope.coin.c500.total;
				array[1] = $scope.coin.c100.total;
				array[2] = $scope.coin.c50.total;
				array[3] = $scope.coin.c10.total;
				array[4] = $scope.coin.c5.total;
			break;
		}
		return array;
	}
	
	$scope.cerrar = function() {
		$scope.campo.cash = 0;
		$scope.campo.fondo = 0;
		$scope.tipo = {};
		$scope.docs = {};
		resetArqueo();
	};

	function esEntero(numero) {
		if ( numero % 1 == 0 ) {
			return true;
		} else {
			return false;
		}
	}

	function formatFechaStr(fecha) {
		fecha = fecha.replace(/-/g, "");
		if ( isUndefinedOrNull(fecha) || !isValidDate(fecha) ) {
			return "";
		}
		let dia = fecha.substr(0, 2);
		let mes = fecha.substr(2, 2);
		let anio = fecha.substr(4, 4);
		return anio +'-'+ mes +'-'+ dia;
	}

	function isValidDate(fechaStr) {
		if ( isUndefinedNullEmpty(fechaStr) ) {
			return false;
		}
		if ( fechaStr.length < 8 ) {
			return false;
		}
		fechaStr = fechaStr.replace(/-/g, "");
		var fechaArray = fechaStr.split("");
		var dia = fechaArray[0] + fechaArray[1];
		var mes = fechaArray[2] + fechaArray[3];
		var anio = fechaArray[4] + fechaArray[5] + fechaArray[6] + fechaArray[7];
		var dateStr = mes + "/" + dia + "/" + anio;
		var d = new Date(dateStr);
		return (d && d.getDate() == Number(dia) && (d.getMonth() + 1) == mes && d.getFullYear() == Number(anio));
	}

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

	function formatRut(_value) {
		_value = cleanRut(_value);
		if ( !_value ) {
			return null;
		}
		if ( _value.length <= 1 ) {
			return _value;
		}
		var result = _value.slice(-4,-1) + '-' + _value.substr(_value.length-1);
		for ( var i=4; i < _value.length; i+=3 ) {
			result = _value.slice(-3-i,-i) + '.' + result;
		}
		return result;
	}

	function cleanRut(_value) {
		return typeof _value === 'string' ? _value.replace(/[^0-9kK]+/g,'').replace(/^0+/, '').toUpperCase() : '';
	}

});
