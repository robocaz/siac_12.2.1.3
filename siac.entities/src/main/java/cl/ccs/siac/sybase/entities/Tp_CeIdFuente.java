package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdFuentes database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdFuentes")
@NamedQuery(name="Tp_CeIdFuente.findAll", query="SELECT t FROM Tp_CeIdFuente t")
public class Tp_CeIdFuente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodFuente")
	private short fld_CodFuente;

	@Column(name="Fld_GlosaFuente")
	private String fld_GlosaFuente;

	@Column(name="Fld_TipoFuente")
	private byte fld_TipoFuente;

	public Tp_CeIdFuente() {
	}

	public short getFld_CodFuente() {
		return this.fld_CodFuente;
	}

	public void setFld_CodFuente(short fld_CodFuente) {
		this.fld_CodFuente = fld_CodFuente;
	}

	public String getFld_GlosaFuente() {
		return this.fld_GlosaFuente;
	}

	public void setFld_GlosaFuente(String fld_GlosaFuente) {
		this.fld_GlosaFuente = fld_GlosaFuente;
	}

	public byte getFld_TipoFuente() {
		return this.fld_TipoFuente;
	}

	public void setFld_TipoFuente(byte fld_TipoFuente) {
		this.fld_TipoFuente = fld_TipoFuente;
	}

}