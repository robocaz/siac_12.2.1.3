package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_VCEnvios database table.
 * 
 */
@Entity
@Table(name="Tbl_VCEnvios")
@NamedQuery(name="Tbl_VCEnvio.findAll", query="SELECT t FROM Tbl_VCEnvio t")
public class Tbl_VCEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_VCENVIOS_FLD_CORRENVIOVC_ID_GENERATOR", sequenceName="FLD_CORRENVIOVC_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_VCENVIOS_FLD_CORRENVIOVC_ID_GENERATOR")
	@Column(name="Fld_CorrEnvioVC_Id")
	private long fld_CorrEnvioVC_Id;

	@Column(name="Fld_CodEmisorCom")
	private String fld_CodEmisorCom;

	@Column(name="Fld_CodEmisorVen")
	private String fld_CodEmisorVen;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecElaboracionSolic")
	private Timestamp fld_FecElaboracionSolic;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FechaArchivo")
	private Timestamp fld_FechaArchivo;

	@Column(name="Fld_TipoEmisorCom")
	private String fld_TipoEmisorCom;

	@Column(name="Fld_TipoEmisorVen")
	private String fld_TipoEmisorVen;

	@Column(name="Fld_TipoEnvioVC")
	private byte fld_TipoEnvioVC;

	@Column(name="Fld_TotRegistrosCargados")
	private int fld_TotRegistrosCargados;

	@Column(name="Fld_TotRegistrosInformados")
	private int fld_TotRegistrosInformados;

	@Column(name="Fld_TotRegistrosPareados")
	private int fld_TotRegistrosPareados;

	@Column(name="Fld_TotRegistrosRechazados")
	private int fld_TotRegistrosRechazados;

	public Tbl_VCEnvio() {
	}

	public long getFld_CorrEnvioVC_Id() {
		return this.fld_CorrEnvioVC_Id;
	}

	public void setFld_CorrEnvioVC_Id(long fld_CorrEnvioVC_Id) {
		this.fld_CorrEnvioVC_Id = fld_CorrEnvioVC_Id;
	}

	public String getFld_CodEmisorCom() {
		return this.fld_CodEmisorCom;
	}

	public void setFld_CodEmisorCom(String fld_CodEmisorCom) {
		this.fld_CodEmisorCom = fld_CodEmisorCom;
	}

	public String getFld_CodEmisorVen() {
		return this.fld_CodEmisorVen;
	}

	public void setFld_CodEmisorVen(String fld_CodEmisorVen) {
		this.fld_CodEmisorVen = fld_CodEmisorVen;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecElaboracionSolic() {
		return this.fld_FecElaboracionSolic;
	}

	public void setFld_FecElaboracionSolic(Timestamp fld_FecElaboracionSolic) {
		this.fld_FecElaboracionSolic = fld_FecElaboracionSolic;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FechaArchivo() {
		return this.fld_FechaArchivo;
	}

	public void setFld_FechaArchivo(Timestamp fld_FechaArchivo) {
		this.fld_FechaArchivo = fld_FechaArchivo;
	}

	public String getFld_TipoEmisorCom() {
		return this.fld_TipoEmisorCom;
	}

	public void setFld_TipoEmisorCom(String fld_TipoEmisorCom) {
		this.fld_TipoEmisorCom = fld_TipoEmisorCom;
	}

	public String getFld_TipoEmisorVen() {
		return this.fld_TipoEmisorVen;
	}

	public void setFld_TipoEmisorVen(String fld_TipoEmisorVen) {
		this.fld_TipoEmisorVen = fld_TipoEmisorVen;
	}

	public byte getFld_TipoEnvioVC() {
		return this.fld_TipoEnvioVC;
	}

	public void setFld_TipoEnvioVC(byte fld_TipoEnvioVC) {
		this.fld_TipoEnvioVC = fld_TipoEnvioVC;
	}

	public int getFld_TotRegistrosCargados() {
		return this.fld_TotRegistrosCargados;
	}

	public void setFld_TotRegistrosCargados(int fld_TotRegistrosCargados) {
		this.fld_TotRegistrosCargados = fld_TotRegistrosCargados;
	}

	public int getFld_TotRegistrosInformados() {
		return this.fld_TotRegistrosInformados;
	}

	public void setFld_TotRegistrosInformados(int fld_TotRegistrosInformados) {
		this.fld_TotRegistrosInformados = fld_TotRegistrosInformados;
	}

	public int getFld_TotRegistrosPareados() {
		return this.fld_TotRegistrosPareados;
	}

	public void setFld_TotRegistrosPareados(int fld_TotRegistrosPareados) {
		this.fld_TotRegistrosPareados = fld_TotRegistrosPareados;
	}

	public int getFld_TotRegistrosRechazados() {
		return this.fld_TotRegistrosRechazados;
	}

	public void setFld_TotRegistrosRechazados(int fld_TotRegistrosRechazados) {
		this.fld_TotRegistrosRechazados = fld_TotRegistrosRechazados;
	}

}