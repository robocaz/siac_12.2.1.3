package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sysencryptkeys database table.
 * 
 */
@Entity
@Table(name="sysencryptkeys")
@NamedQuery(name="Sysencryptkey.findAll", query="SELECT s FROM Sysencryptkey s")
public class Sysencryptkey implements Serializable {
	private static final long serialVersionUID = 1L;

	private int ekalgorithm;

	private short eklen;

	private int ekpairid;

	private int ekpwdwarn;

	private byte[] eksalt;

	private int expdate;

	private int id;

	private Timestamp pwdate;

	private int status;

	private short type;

	private int uid;

	private byte[] value;

	public Sysencryptkey() {
	}

	public int getEkalgorithm() {
		return this.ekalgorithm;
	}

	public void setEkalgorithm(int ekalgorithm) {
		this.ekalgorithm = ekalgorithm;
	}

	public short getEklen() {
		return this.eklen;
	}

	public void setEklen(short eklen) {
		this.eklen = eklen;
	}

	public int getEkpairid() {
		return this.ekpairid;
	}

	public void setEkpairid(int ekpairid) {
		this.ekpairid = ekpairid;
	}

	public int getEkpwdwarn() {
		return this.ekpwdwarn;
	}

	public void setEkpwdwarn(int ekpwdwarn) {
		this.ekpwdwarn = ekpwdwarn;
	}

	public byte[] getEksalt() {
		return this.eksalt;
	}

	public void setEksalt(byte[] eksalt) {
		this.eksalt = eksalt;
	}

	public int getExpdate() {
		return this.expdate;
	}

	public void setExpdate(int expdate) {
		this.expdate = expdate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getPwdate() {
		return this.pwdate;
	}

	public void setPwdate(Timestamp pwdate) {
		this.pwdate = pwdate;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public short getType() {
		return this.type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public byte[] getValue() {
		return this.value;
	}

	public void setValue(byte[] value) {
		this.value = value;
	}

}