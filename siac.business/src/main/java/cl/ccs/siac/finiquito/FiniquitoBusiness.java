package cl.ccs.siac.finiquito;

import cl.exe.ccs.dto.FiniquitoDTO;

public interface FiniquitoBusiness {

	FiniquitoDTO consultaFiniquito(String rut);

	FiniquitoDTO guardaFiniquito(FiniquitoDTO finiquito);

}
