package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_EMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_EMISOR")
@NamedQuery(name="TblEmisor.findAll", query="SELECT t FROM TblEmisor t")
public class TblEmisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TblEmisorPK id;

	@Column(name="FLD_CORRDIRECCION")
	private BigDecimal fldCorrdireccion;

	@Column(name="FLD_EMAIL")
	private String fldEmail;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAFINVIGENCIA")
	private Date fldFechafinvigencia;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIOVIGENCIA")
	private Date fldFechainiciovigencia;

	@Column(name="FLD_FLAGVIGENCIA")
	private BigDecimal fldFlagvigencia;

	@Column(name="FLD_GLOSAEMISOR")
	private String fldGlosaemisor;

	@Column(name="FLD_RAZONSOCIAL")
	private String fldRazonsocial;

	@Column(name="FLD_SITIOWEB")
	private String fldSitioweb;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOASIGNADO")
	private String fldUsuarioasignado;

	//bi-directional many-to-many association to TblTipocredito
	@ManyToMany(mappedBy="tblEmisors")
	private List<TblTipocredito> tblTipocreditos;

	//bi-directional many-to-one association to TblTipoemisor
	@ManyToOne
	@JoinColumn(name="FLD_TIPOEMISOR")
	private TblTipoemisor tblTipoemisor;

	//bi-directional many-to-one association to TblEmisordetalle
	@OneToMany(mappedBy="tblEmisor")
	private List<TblEmisordetalle> tblEmisordetalles;

	//bi-directional many-to-one association to TblSucursal
	@OneToMany(mappedBy="tblEmisor")
	private List<TblSucursal> tblSucursals;

	public TblEmisor() {
	}

	public TblEmisorPK getId() {
		return this.id;
	}

	public void setId(TblEmisorPK id) {
		this.id = id;
	}

	public BigDecimal getFldCorrdireccion() {
		return this.fldCorrdireccion;
	}

	public void setFldCorrdireccion(BigDecimal fldCorrdireccion) {
		this.fldCorrdireccion = fldCorrdireccion;
	}

	public String getFldEmail() {
		return this.fldEmail;
	}

	public void setFldEmail(String fldEmail) {
		this.fldEmail = fldEmail;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Date getFldFechafinvigencia() {
		return this.fldFechafinvigencia;
	}

	public void setFldFechafinvigencia(Date fldFechafinvigencia) {
		this.fldFechafinvigencia = fldFechafinvigencia;
	}

	public Date getFldFechainiciovigencia() {
		return this.fldFechainiciovigencia;
	}

	public void setFldFechainiciovigencia(Date fldFechainiciovigencia) {
		this.fldFechainiciovigencia = fldFechainiciovigencia;
	}

	public BigDecimal getFldFlagvigencia() {
		return this.fldFlagvigencia;
	}

	public void setFldFlagvigencia(BigDecimal fldFlagvigencia) {
		this.fldFlagvigencia = fldFlagvigencia;
	}

	public String getFldGlosaemisor() {
		return this.fldGlosaemisor;
	}

	public void setFldGlosaemisor(String fldGlosaemisor) {
		this.fldGlosaemisor = fldGlosaemisor;
	}

	public String getFldRazonsocial() {
		return this.fldRazonsocial;
	}

	public void setFldRazonsocial(String fldRazonsocial) {
		this.fldRazonsocial = fldRazonsocial;
	}

	public String getFldSitioweb() {
		return this.fldSitioweb;
	}

	public void setFldSitioweb(String fldSitioweb) {
		this.fldSitioweb = fldSitioweb;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuarioasignado() {
		return this.fldUsuarioasignado;
	}

	public void setFldUsuarioasignado(String fldUsuarioasignado) {
		this.fldUsuarioasignado = fldUsuarioasignado;
	}

	public List<TblTipocredito> getTblTipocreditos() {
		return this.tblTipocreditos;
	}

	public void setTblTipocreditos(List<TblTipocredito> tblTipocreditos) {
		this.tblTipocreditos = tblTipocreditos;
	}

	public TblTipoemisor getTblTipoemisor() {
		return this.tblTipoemisor;
	}

	public void setTblTipoemisor(TblTipoemisor tblTipoemisor) {
		this.tblTipoemisor = tblTipoemisor;
	}

	public List<TblEmisordetalle> getTblEmisordetalles() {
		return this.tblEmisordetalles;
	}

	public void setTblEmisordetalles(List<TblEmisordetalle> tblEmisordetalles) {
		this.tblEmisordetalles = tblEmisordetalles;
	}

	public TblEmisordetalle addTblEmisordetalle(TblEmisordetalle tblEmisordetalle) {
		getTblEmisordetalles().add(tblEmisordetalle);
		tblEmisordetalle.setTblEmisor(this);

		return tblEmisordetalle;
	}

	public TblEmisordetalle removeTblEmisordetalle(TblEmisordetalle tblEmisordetalle) {
		getTblEmisordetalles().remove(tblEmisordetalle);
		tblEmisordetalle.setTblEmisor(null);

		return tblEmisordetalle;
	}

	public List<TblSucursal> getTblSucursals() {
		return this.tblSucursals;
	}

	public void setTblSucursals(List<TblSucursal> tblSucursals) {
		this.tblSucursals = tblSucursals;
	}

	public TblSucursal addTblSucursal(TblSucursal tblSucursal) {
		getTblSucursals().add(tblSucursal);
		tblSucursal.setTblEmisor(this);

		return tblSucursal;
	}

	public TblSucursal removeTblSucursal(TblSucursal tblSucursal) {
		getTblSucursals().remove(tblSucursal);
		tblSucursal.setTblEmisor(null);

		return tblSucursal;
	}

}