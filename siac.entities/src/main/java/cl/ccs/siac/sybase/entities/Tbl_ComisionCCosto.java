package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ComisionCCosto database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ComisionCCosto.findAll", query="SELECT t FROM Tbl_ComisionCCosto t")
public class Tbl_ComisionCCosto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_FecInicioComision")
	private Timestamp fld_FecInicioComision;

	@Column(name="Fld_PorcentajeComision")
	private BigDecimal fld_PorcentajeComision;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_ComisionCCosto() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public Timestamp getFld_FecInicioComision() {
		return this.fld_FecInicioComision;
	}

	public void setFld_FecInicioComision(Timestamp fld_FecInicioComision) {
		this.fld_FecInicioComision = fld_FecInicioComision;
	}

	public BigDecimal getFld_PorcentajeComision() {
		return this.fld_PorcentajeComision;
	}

	public void setFld_PorcentajeComision(BigDecimal fld_PorcentajeComision) {
		this.fld_PorcentajeComision = fld_PorcentajeComision;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}