package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_TmpAclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TmpAclBatch.findAll", query="SELECT t FROM Tbl_TmpAclBatch t")
public class Tbl_TmpAclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalidadJuridica")
	private String fld_CalidadJuridica;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodRetAcl")
	private byte fld_CodRetAcl;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrCarga_Id")
	private BigDecimal fld_CorrCarga_Id;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CostoAclaracion")
	private int fld_CostoAclaracion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_Filler")
	private String fld_Filler;

	@Column(name="Fld_FolioEnvio")
	private int fld_FolioEnvio;

	@Column(name="Fld_GlosaLocalidad")
	private String fld_GlosaLocalidad;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroOper3")
	private short fld_NroOper3;

	@Column(name="Fld_NroOperacion")
	private String fld_NroOperacion;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutLibrador")
	private String fld_RutLibrador;

	@Column(name="Fld_TarifaAclaracion")
	private int fld_TarifaAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoRegistro")
	private String fld_TipoRegistro;

	public Tbl_TmpAclBatch() {
	}

	public String getFld_CalidadJuridica() {
		return this.fld_CalidadJuridica;
	}

	public void setFld_CalidadJuridica(String fld_CalidadJuridica) {
		this.fld_CalidadJuridica = fld_CalidadJuridica;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte getFld_CodRetAcl() {
		return this.fld_CodRetAcl;
	}

	public void setFld_CodRetAcl(byte fld_CodRetAcl) {
		this.fld_CodRetAcl = fld_CodRetAcl;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrCarga_Id() {
		return this.fld_CorrCarga_Id;
	}

	public void setFld_CorrCarga_Id(BigDecimal fld_CorrCarga_Id) {
		this.fld_CorrCarga_Id = fld_CorrCarga_Id;
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public int getFld_CostoAclaracion() {
		return this.fld_CostoAclaracion;
	}

	public void setFld_CostoAclaracion(int fld_CostoAclaracion) {
		this.fld_CostoAclaracion = fld_CostoAclaracion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public String getFld_Filler() {
		return this.fld_Filler;
	}

	public void setFld_Filler(String fld_Filler) {
		this.fld_Filler = fld_Filler;
	}

	public int getFld_FolioEnvio() {
		return this.fld_FolioEnvio;
	}

	public void setFld_FolioEnvio(int fld_FolioEnvio) {
		this.fld_FolioEnvio = fld_FolioEnvio;
	}

	public String getFld_GlosaLocalidad() {
		return this.fld_GlosaLocalidad;
	}

	public void setFld_GlosaLocalidad(String fld_GlosaLocalidad) {
		this.fld_GlosaLocalidad = fld_GlosaLocalidad;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public short getFld_NroOper3() {
		return this.fld_NroOper3;
	}

	public void setFld_NroOper3(short fld_NroOper3) {
		this.fld_NroOper3 = fld_NroOper3;
	}

	public String getFld_NroOperacion() {
		return this.fld_NroOperacion;
	}

	public void setFld_NroOperacion(String fld_NroOperacion) {
		this.fld_NroOperacion = fld_NroOperacion;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutLibrador() {
		return this.fld_RutLibrador;
	}

	public void setFld_RutLibrador(String fld_RutLibrador) {
		this.fld_RutLibrador = fld_RutLibrador;
	}

	public int getFld_TarifaAclaracion() {
		return this.fld_TarifaAclaracion;
	}

	public void setFld_TarifaAclaracion(int fld_TarifaAclaracion) {
		this.fld_TarifaAclaracion = fld_TarifaAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_TipoRegistro() {
		return this.fld_TipoRegistro;
	}

	public void setFld_TipoRegistro(String fld_TipoRegistro) {
		this.fld_TipoRegistro = fld_TipoRegistro;
	}

}