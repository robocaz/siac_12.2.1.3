package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_SITUACION database table.
 * 
 */
@Entity
@Table(name="TBL_SITUACION")
@NamedQuery(name="TblSituacion.findAll", query="SELECT t FROM TblSituacion t")
public class TblSituacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_SITUACION_FLDCODSITUACION_GENERATOR", sequenceName="FLD_CODSITUACION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_SITUACION_FLDCODSITUACION_GENERATOR")
	@Column(name="FLD_CODSITUACION")
	private long fldCodsituacion;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSACORTASITUACION")
	private String fldGlosacortasituacion;

	@Column(name="FLD_GLOSASITUACION")
	private String fldGlosasituacion;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	public TblSituacion() {
	}

	public long getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(long fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosacortasituacion() {
		return this.fldGlosacortasituacion;
	}

	public void setFldGlosacortasituacion(String fldGlosacortasituacion) {
		this.fldGlosacortasituacion = fldGlosacortasituacion;
	}

	public String getFldGlosasituacion() {
		return this.fldGlosasituacion;
	}

	public void setFldGlosasituacion(String fldGlosasituacion) {
		this.fldGlosasituacion = fldGlosasituacion;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

}