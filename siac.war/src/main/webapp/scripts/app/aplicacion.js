'use strict';
var app = angular.module('ccs',[ 'ngRoute','ngResource', 'ui.slimscroll','ui.select', 'ngSanitize', 'ngAnimate', 'ui.tree', 'ngCookies', 'ui.bootstrap','ngStorage','angular.filter','ui.mask','ngTable']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/',{templateUrl:'views/home.html?nocache='+ new Date().getTime(),controller:'BienvenidoController'})
      .when('/inicio',{templateUrl:'views/home.html?nocache='+ new Date().getTime(),controller:'BienvenidoController'})
      .when('/faq',{templateUrl:'views/faq.html?nocache='+ new Date().getTime(),controller:'BienvenidoController'})
      .when('/login',{templateUrl:'views/login.html?nocache='+ new Date().getTime(),controller:'LoginController'})
      .when('/atencion',{templateUrl:'views/atencion.html?nocache='+ new Date().getTime(),controller:'TramitanteController'})
      .when('/cuadraturaindex',{templateUrl:'views/modalcuadraturaindex.html?nocache='+ new Date().getTime(),controller:'CuadraturaIndexController'})
      .when('/protesto',{templateUrl:'views/home.html?nocache='+ new Date().getTime(),controller:'BienvenidoController'})
      .when('/servicios',{templateUrl:'views/servicios.html?nocache='+ new Date().getTime(),controller:'CarroCompraController'})
      .when('/pagos',{templateUrl:'views/pagos.html?nocache='+ new Date().getTime(),controller:'PagoController'})
      .otherwise({redirectTo: '/404' });
}]);

app.run(function($rootScope, LoginResource, $location){
    $rootScope.$on('$routeChangeStart', function()
    {
        var successCallback = function(data){};

        var errorCallback = function(response) {
            $rootScope.usuario = {};
            $location.path("/login");
        };

        LoginResource.status({}, successCallback, errorCallback);
    })

});

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

   
}
]);
