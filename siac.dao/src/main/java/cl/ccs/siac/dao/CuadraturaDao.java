package cl.ccs.siac.dao;

import java.util.Date;
import java.util.List;

import cl.exe.ccs.dto.CajaInformeDTO;
import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaAclaracionDTO;
import cl.exe.ccs.dto.CuadraturaCajaDTO;
import cl.exe.ccs.dto.CuadraturaCentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaUsuarioDTO;
import cl.exe.ccs.dto.DatoBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleBoletaDTO;
import cl.exe.ccs.dto.DetalleBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleCentroCostoDTO;
import cl.exe.ccs.dto.DetalleServicioDTO;
import cl.exe.ccs.dto.PorcentajeIvaDTO;
import cl.exe.ccs.dto.ReclamoDTO;
import cl.exe.ccs.dto.ResumenMovimientoDTO;
import cl.exe.ccs.dto.UsuariosInformeDTO;

public interface CuadraturaDao {

	List<CentroCostoDTO> centrosCostoInforme(Integer centroCosto, Date desde, Date hasta);

	List<UsuariosInformeDTO> usuariosInforme(Integer centroCosto, Date desde, Date hasta);

	List<CajaInformeDTO> cajaInforme(String usuario, Integer centroCosto, Date desde, Date hasta);

	CuadraturaCentroCostoDTO cuadraturaPorCamara(Integer centroCosto, Date desde, Date hasta);

	CuadraturaUsuarioDTO cuadraturaPorUsuario(Integer centroCosto, String usuario, Date desde, Date hasta);

	DetalleCentroCostoDTO cuadraturaPorCentroCosto(Integer centroCosto, Date desde, Date hasta);

	PorcentajeIvaDTO obtienePorcentajeIva();
	
	List<CuadraturaCajaDTO> buscarCajasPorUsuario(String usuario, Integer centroCosto, Date desdeDate);
	
	List<CuadraturaCajaDTO> buscarCajasPorCajero(String usuario, Long fechaInicio);
	
	ResumenMovimientoDTO obtieneMovimientosNac(Integer centroCosto, Date fechaInicio, Date fechaDesde);
	
	ResumenMovimientoDTO obtieneMovimientos(Date fechaInicio, String usuario, Integer centroCosto, Integer corrCaja, Integer criterio );
	
	DetalleBoletaDTO obtieneDetalleBoleta(Date fechaInicio, String usuario, Integer corrCaja, Integer centroCosto, Integer criterio);
	
	DetalleBoletaFacturaDTO obtieneDetalleBoletaFactura(DetalleBoletaFacturaDTO detalle);
	
	List<DatoBoletaFacturaDTO> getDetalleBoletaModal(DetalleBoletaFacturaDTO detalle);
	
	DetalleBoletaFacturaDTO getDetalleFacturaModal(DetalleBoletaFacturaDTO detalle);
	
	DetalleServicioDTO obtieneDetalleServicio(Date fechaInicio, String usuario, Integer corrCaja,Integer centroCosto, Integer criterio, Integer ordenamiento);
	
	CuadraturaAclaracionDTO validaAclaraciones(Integer corrCaja);
	
	ReclamoDTO solicitudesReclamos(Integer corrCaja);
}
