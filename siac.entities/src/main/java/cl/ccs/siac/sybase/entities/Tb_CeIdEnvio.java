package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Tb_CeIdEnvios database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdEnvios")
@NamedQuery(name="Tb_CeIdEnvio.findAll", query="SELECT t FROM Tb_CeIdEnvio t")
public class Tb_CeIdEnvio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CEIDENVIOS_FLD_CORRENVIO_ID_GENERATOR", sequenceName="FLD_CORRENVIO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CEIDENVIOS_FLD_CORRENVIO_ID_GENERATOR")
	@Column(name="Fld_CorrEnvio_Id")
	private long fld_CorrEnvio_Id;

	@Column(name="Fld_CodAgencia")
	private short fld_CodAgencia;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodFuente")
	private short fld_CodFuente;

	@Column(name="Fld_CodProveedor")
	private short fld_CodProveedor;

	@Column(name="Fld_CodUsuarioDigitacion")
	private String fld_CodUsuarioDigitacion;

	@Column(name="Fld_CodUsuarioResp")
	private String fld_CodUsuarioResp;

	@Column(name="Fld_FecCreacionEnv")
	private Timestamp fld_FecCreacionEnv;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FecPubDiario")
	private Timestamp fld_FecPubDiario;

	@Column(name="Fld_FecRecepcionEnv")
	private Timestamp fld_FecRecepcionEnv;

	@Column(name="Fld_MarcaExtBIC")
	private boolean fld_MarcaExtBIC;

	@Column(name="Fld_MarcaExtRC")
	private boolean fld_MarcaExtRC;

	@Column(name="Fld_MarcaExtSAC")
	private boolean fld_MarcaExtSAC;

	@Column(name="Fld_MarcaPareo")
	private boolean fld_MarcaPareo;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	@Column(name="Fld_TipoFuente")
	private byte fld_TipoFuente;

	@Column(name="Fld_TotRegDigitados")
	private int fld_TotRegDigitados;

	@Column(name="Fld_TotRegInformados")
	private int fld_TotRegInformados;

	//bi-directional many-to-one association to Tb_CeIdDetBloqueoCedula
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetBloqueoCedula> tbCeIdDetBloqueoCedulas;

	//bi-directional many-to-one association to Tb_CeIdDetCedula
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetCedula> tbCeIdDetCedulas;

	//bi-directional many-to-one association to Tb_CeIdDetCtaCte
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetCtaCte> tbCeIdDetCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdDetDigNombre
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetDigNombre> tbCeIdDetDigNombres;

	//bi-directional many-to-one association to Tb_CeIdDetEstEnvio
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetEstEnvio> tbCeIdDetEstEnvios;

	//bi-directional many-to-one association to Tb_CeIdDetImpedidosCtaCte
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetImpedidosCtaCte> tbCeIdDetImpedidosCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdDetObituario
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdDetObituario> tbCeIdDetObituarios;

	//bi-directional many-to-one association to Tb_CeIdSegtoBloqueoCtaCte
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdSegtoBloqueoCtaCte> tbCeIdSegtoBloqueoCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdSegtoCtaCte
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdSegtoCtaCte> tbCeIdSegtoCtaCtes;

	//bi-directional many-to-one association to Tb_CeIdSegtoDefuncione
	@OneToMany(mappedBy="tbCeIdEnvio")
	private List<Tb_CeIdSegtoDefuncione> tbCeIdSegtoDefunciones;

	public Tb_CeIdEnvio() {
	}

	public long getFld_CorrEnvio_Id() {
		return this.fld_CorrEnvio_Id;
	}

	public void setFld_CorrEnvio_Id(long fld_CorrEnvio_Id) {
		this.fld_CorrEnvio_Id = fld_CorrEnvio_Id;
	}

	public short getFld_CodAgencia() {
		return this.fld_CodAgencia;
	}

	public void setFld_CodAgencia(short fld_CodAgencia) {
		this.fld_CodAgencia = fld_CodAgencia;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodFuente() {
		return this.fld_CodFuente;
	}

	public void setFld_CodFuente(short fld_CodFuente) {
		this.fld_CodFuente = fld_CodFuente;
	}

	public short getFld_CodProveedor() {
		return this.fld_CodProveedor;
	}

	public void setFld_CodProveedor(short fld_CodProveedor) {
		this.fld_CodProveedor = fld_CodProveedor;
	}

	public String getFld_CodUsuarioDigitacion() {
		return this.fld_CodUsuarioDigitacion;
	}

	public void setFld_CodUsuarioDigitacion(String fld_CodUsuarioDigitacion) {
		this.fld_CodUsuarioDigitacion = fld_CodUsuarioDigitacion;
	}

	public String getFld_CodUsuarioResp() {
		return this.fld_CodUsuarioResp;
	}

	public void setFld_CodUsuarioResp(String fld_CodUsuarioResp) {
		this.fld_CodUsuarioResp = fld_CodUsuarioResp;
	}

	public Timestamp getFld_FecCreacionEnv() {
		return this.fld_FecCreacionEnv;
	}

	public void setFld_FecCreacionEnv(Timestamp fld_FecCreacionEnv) {
		this.fld_FecCreacionEnv = fld_FecCreacionEnv;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FecPubDiario() {
		return this.fld_FecPubDiario;
	}

	public void setFld_FecPubDiario(Timestamp fld_FecPubDiario) {
		this.fld_FecPubDiario = fld_FecPubDiario;
	}

	public Timestamp getFld_FecRecepcionEnv() {
		return this.fld_FecRecepcionEnv;
	}

	public void setFld_FecRecepcionEnv(Timestamp fld_FecRecepcionEnv) {
		this.fld_FecRecepcionEnv = fld_FecRecepcionEnv;
	}

	public boolean getFld_MarcaExtBIC() {
		return this.fld_MarcaExtBIC;
	}

	public void setFld_MarcaExtBIC(boolean fld_MarcaExtBIC) {
		this.fld_MarcaExtBIC = fld_MarcaExtBIC;
	}

	public boolean getFld_MarcaExtRC() {
		return this.fld_MarcaExtRC;
	}

	public void setFld_MarcaExtRC(boolean fld_MarcaExtRC) {
		this.fld_MarcaExtRC = fld_MarcaExtRC;
	}

	public boolean getFld_MarcaExtSAC() {
		return this.fld_MarcaExtSAC;
	}

	public void setFld_MarcaExtSAC(boolean fld_MarcaExtSAC) {
		this.fld_MarcaExtSAC = fld_MarcaExtSAC;
	}

	public boolean getFld_MarcaPareo() {
		return this.fld_MarcaPareo;
	}

	public void setFld_MarcaPareo(boolean fld_MarcaPareo) {
		this.fld_MarcaPareo = fld_MarcaPareo;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

	public byte getFld_TipoFuente() {
		return this.fld_TipoFuente;
	}

	public void setFld_TipoFuente(byte fld_TipoFuente) {
		this.fld_TipoFuente = fld_TipoFuente;
	}

	public int getFld_TotRegDigitados() {
		return this.fld_TotRegDigitados;
	}

	public void setFld_TotRegDigitados(int fld_TotRegDigitados) {
		this.fld_TotRegDigitados = fld_TotRegDigitados;
	}

	public int getFld_TotRegInformados() {
		return this.fld_TotRegInformados;
	}

	public void setFld_TotRegInformados(int fld_TotRegInformados) {
		this.fld_TotRegInformados = fld_TotRegInformados;
	}

	public List<Tb_CeIdDetBloqueoCedula> getTbCeIdDetBloqueoCedulas() {
		return this.tbCeIdDetBloqueoCedulas;
	}

	public void setTbCeIdDetBloqueoCedulas(List<Tb_CeIdDetBloqueoCedula> tbCeIdDetBloqueoCedulas) {
		this.tbCeIdDetBloqueoCedulas = tbCeIdDetBloqueoCedulas;
	}

	public Tb_CeIdDetBloqueoCedula addTbCeIdDetBloqueoCedula(Tb_CeIdDetBloqueoCedula tbCeIdDetBloqueoCedula) {
		getTbCeIdDetBloqueoCedulas().add(tbCeIdDetBloqueoCedula);
		tbCeIdDetBloqueoCedula.setTbCeIdEnvio(this);

		return tbCeIdDetBloqueoCedula;
	}

	public Tb_CeIdDetBloqueoCedula removeTbCeIdDetBloqueoCedula(Tb_CeIdDetBloqueoCedula tbCeIdDetBloqueoCedula) {
		getTbCeIdDetBloqueoCedulas().remove(tbCeIdDetBloqueoCedula);
		tbCeIdDetBloqueoCedula.setTbCeIdEnvio(null);

		return tbCeIdDetBloqueoCedula;
	}

	public List<Tb_CeIdDetCedula> getTbCeIdDetCedulas() {
		return this.tbCeIdDetCedulas;
	}

	public void setTbCeIdDetCedulas(List<Tb_CeIdDetCedula> tbCeIdDetCedulas) {
		this.tbCeIdDetCedulas = tbCeIdDetCedulas;
	}

	public Tb_CeIdDetCedula addTbCeIdDetCedula(Tb_CeIdDetCedula tbCeIdDetCedula) {
		getTbCeIdDetCedulas().add(tbCeIdDetCedula);
		tbCeIdDetCedula.setTbCeIdEnvio(this);

		return tbCeIdDetCedula;
	}

	public Tb_CeIdDetCedula removeTbCeIdDetCedula(Tb_CeIdDetCedula tbCeIdDetCedula) {
		getTbCeIdDetCedulas().remove(tbCeIdDetCedula);
		tbCeIdDetCedula.setTbCeIdEnvio(null);

		return tbCeIdDetCedula;
	}

	public List<Tb_CeIdDetCtaCte> getTbCeIdDetCtaCtes() {
		return this.tbCeIdDetCtaCtes;
	}

	public void setTbCeIdDetCtaCtes(List<Tb_CeIdDetCtaCte> tbCeIdDetCtaCtes) {
		this.tbCeIdDetCtaCtes = tbCeIdDetCtaCtes;
	}

	public Tb_CeIdDetCtaCte addTbCeIdDetCtaCte(Tb_CeIdDetCtaCte tbCeIdDetCtaCte) {
		getTbCeIdDetCtaCtes().add(tbCeIdDetCtaCte);
		tbCeIdDetCtaCte.setTbCeIdEnvio(this);

		return tbCeIdDetCtaCte;
	}

	public Tb_CeIdDetCtaCte removeTbCeIdDetCtaCte(Tb_CeIdDetCtaCte tbCeIdDetCtaCte) {
		getTbCeIdDetCtaCtes().remove(tbCeIdDetCtaCte);
		tbCeIdDetCtaCte.setTbCeIdEnvio(null);

		return tbCeIdDetCtaCte;
	}

	public List<Tb_CeIdDetDigNombre> getTbCeIdDetDigNombres() {
		return this.tbCeIdDetDigNombres;
	}

	public void setTbCeIdDetDigNombres(List<Tb_CeIdDetDigNombre> tbCeIdDetDigNombres) {
		this.tbCeIdDetDigNombres = tbCeIdDetDigNombres;
	}

	public Tb_CeIdDetDigNombre addTbCeIdDetDigNombre(Tb_CeIdDetDigNombre tbCeIdDetDigNombre) {
		getTbCeIdDetDigNombres().add(tbCeIdDetDigNombre);
		tbCeIdDetDigNombre.setTbCeIdEnvio(this);

		return tbCeIdDetDigNombre;
	}

	public Tb_CeIdDetDigNombre removeTbCeIdDetDigNombre(Tb_CeIdDetDigNombre tbCeIdDetDigNombre) {
		getTbCeIdDetDigNombres().remove(tbCeIdDetDigNombre);
		tbCeIdDetDigNombre.setTbCeIdEnvio(null);

		return tbCeIdDetDigNombre;
	}

	public List<Tb_CeIdDetEstEnvio> getTbCeIdDetEstEnvios() {
		return this.tbCeIdDetEstEnvios;
	}

	public void setTbCeIdDetEstEnvios(List<Tb_CeIdDetEstEnvio> tbCeIdDetEstEnvios) {
		this.tbCeIdDetEstEnvios = tbCeIdDetEstEnvios;
	}

	public Tb_CeIdDetEstEnvio addTbCeIdDetEstEnvio(Tb_CeIdDetEstEnvio tbCeIdDetEstEnvio) {
		getTbCeIdDetEstEnvios().add(tbCeIdDetEstEnvio);
		tbCeIdDetEstEnvio.setTbCeIdEnvio(this);

		return tbCeIdDetEstEnvio;
	}

	public Tb_CeIdDetEstEnvio removeTbCeIdDetEstEnvio(Tb_CeIdDetEstEnvio tbCeIdDetEstEnvio) {
		getTbCeIdDetEstEnvios().remove(tbCeIdDetEstEnvio);
		tbCeIdDetEstEnvio.setTbCeIdEnvio(null);

		return tbCeIdDetEstEnvio;
	}

	public List<Tb_CeIdDetImpedidosCtaCte> getTbCeIdDetImpedidosCtaCtes() {
		return this.tbCeIdDetImpedidosCtaCtes;
	}

	public void setTbCeIdDetImpedidosCtaCtes(List<Tb_CeIdDetImpedidosCtaCte> tbCeIdDetImpedidosCtaCtes) {
		this.tbCeIdDetImpedidosCtaCtes = tbCeIdDetImpedidosCtaCtes;
	}

	public Tb_CeIdDetImpedidosCtaCte addTbCeIdDetImpedidosCtaCte(Tb_CeIdDetImpedidosCtaCte tbCeIdDetImpedidosCtaCte) {
		getTbCeIdDetImpedidosCtaCtes().add(tbCeIdDetImpedidosCtaCte);
		tbCeIdDetImpedidosCtaCte.setTbCeIdEnvio(this);

		return tbCeIdDetImpedidosCtaCte;
	}

	public Tb_CeIdDetImpedidosCtaCte removeTbCeIdDetImpedidosCtaCte(Tb_CeIdDetImpedidosCtaCte tbCeIdDetImpedidosCtaCte) {
		getTbCeIdDetImpedidosCtaCtes().remove(tbCeIdDetImpedidosCtaCte);
		tbCeIdDetImpedidosCtaCte.setTbCeIdEnvio(null);

		return tbCeIdDetImpedidosCtaCte;
	}

	public List<Tb_CeIdDetObituario> getTbCeIdDetObituarios() {
		return this.tbCeIdDetObituarios;
	}

	public void setTbCeIdDetObituarios(List<Tb_CeIdDetObituario> tbCeIdDetObituarios) {
		this.tbCeIdDetObituarios = tbCeIdDetObituarios;
	}

	public Tb_CeIdDetObituario addTbCeIdDetObituario(Tb_CeIdDetObituario tbCeIdDetObituario) {
		getTbCeIdDetObituarios().add(tbCeIdDetObituario);
		tbCeIdDetObituario.setTbCeIdEnvio(this);

		return tbCeIdDetObituario;
	}

	public Tb_CeIdDetObituario removeTbCeIdDetObituario(Tb_CeIdDetObituario tbCeIdDetObituario) {
		getTbCeIdDetObituarios().remove(tbCeIdDetObituario);
		tbCeIdDetObituario.setTbCeIdEnvio(null);

		return tbCeIdDetObituario;
	}

	public List<Tb_CeIdSegtoBloqueoCtaCte> getTbCeIdSegtoBloqueoCtaCtes() {
		return this.tbCeIdSegtoBloqueoCtaCtes;
	}

	public void setTbCeIdSegtoBloqueoCtaCtes(List<Tb_CeIdSegtoBloqueoCtaCte> tbCeIdSegtoBloqueoCtaCtes) {
		this.tbCeIdSegtoBloqueoCtaCtes = tbCeIdSegtoBloqueoCtaCtes;
	}

	public Tb_CeIdSegtoBloqueoCtaCte addTbCeIdSegtoBloqueoCtaCte(Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte) {
		getTbCeIdSegtoBloqueoCtaCtes().add(tbCeIdSegtoBloqueoCtaCte);
		tbCeIdSegtoBloqueoCtaCte.setTbCeIdEnvio(this);

		return tbCeIdSegtoBloqueoCtaCte;
	}

	public Tb_CeIdSegtoBloqueoCtaCte removeTbCeIdSegtoBloqueoCtaCte(Tb_CeIdSegtoBloqueoCtaCte tbCeIdSegtoBloqueoCtaCte) {
		getTbCeIdSegtoBloqueoCtaCtes().remove(tbCeIdSegtoBloqueoCtaCte);
		tbCeIdSegtoBloqueoCtaCte.setTbCeIdEnvio(null);

		return tbCeIdSegtoBloqueoCtaCte;
	}

	public List<Tb_CeIdSegtoCtaCte> getTbCeIdSegtoCtaCtes() {
		return this.tbCeIdSegtoCtaCtes;
	}

	public void setTbCeIdSegtoCtaCtes(List<Tb_CeIdSegtoCtaCte> tbCeIdSegtoCtaCtes) {
		this.tbCeIdSegtoCtaCtes = tbCeIdSegtoCtaCtes;
	}

	public Tb_CeIdSegtoCtaCte addTbCeIdSegtoCtaCte(Tb_CeIdSegtoCtaCte tbCeIdSegtoCtaCte) {
		getTbCeIdSegtoCtaCtes().add(tbCeIdSegtoCtaCte);
		tbCeIdSegtoCtaCte.setTbCeIdEnvio(this);

		return tbCeIdSegtoCtaCte;
	}

	public Tb_CeIdSegtoCtaCte removeTbCeIdSegtoCtaCte(Tb_CeIdSegtoCtaCte tbCeIdSegtoCtaCte) {
		getTbCeIdSegtoCtaCtes().remove(tbCeIdSegtoCtaCte);
		tbCeIdSegtoCtaCte.setTbCeIdEnvio(null);

		return tbCeIdSegtoCtaCte;
	}

	public List<Tb_CeIdSegtoDefuncione> getTbCeIdSegtoDefunciones() {
		return this.tbCeIdSegtoDefunciones;
	}

	public void setTbCeIdSegtoDefunciones(List<Tb_CeIdSegtoDefuncione> tbCeIdSegtoDefunciones) {
		this.tbCeIdSegtoDefunciones = tbCeIdSegtoDefunciones;
	}

	public Tb_CeIdSegtoDefuncione addTbCeIdSegtoDefuncione(Tb_CeIdSegtoDefuncione tbCeIdSegtoDefuncione) {
		getTbCeIdSegtoDefunciones().add(tbCeIdSegtoDefuncione);
		tbCeIdSegtoDefuncione.setTbCeIdEnvio(this);

		return tbCeIdSegtoDefuncione;
	}

	public Tb_CeIdSegtoDefuncione removeTbCeIdSegtoDefuncione(Tb_CeIdSegtoDefuncione tbCeIdSegtoDefuncione) {
		getTbCeIdSegtoDefunciones().remove(tbCeIdSegtoDefuncione);
		tbCeIdSegtoDefuncione.setTbCeIdEnvio(null);

		return tbCeIdSegtoDefuncione;
	}

}