package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the SAC_DetError database table.
 * 
 */
@Entity
@NamedQuery(name="SAC_DetError.findAll", query="SELECT s FROM SAC_DetError s")
public class SAC_DetError implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CodTabla")
	private short fld_CodTabla;

	@Column(name="Fld_CorrReg")
	private BigDecimal fld_CorrReg;

	@Column(name="Fld_GlosaError")
	private String fld_GlosaError;

	@Column(name="Fld_TipoError")
	private String fld_TipoError;

	public SAC_DetError() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public short getFld_CodTabla() {
		return this.fld_CodTabla;
	}

	public void setFld_CodTabla(short fld_CodTabla) {
		this.fld_CodTabla = fld_CodTabla;
	}

	public BigDecimal getFld_CorrReg() {
		return this.fld_CorrReg;
	}

	public void setFld_CorrReg(BigDecimal fld_CorrReg) {
		this.fld_CorrReg = fld_CorrReg;
	}

	public String getFld_GlosaError() {
		return this.fld_GlosaError;
	}

	public void setFld_GlosaError(String fld_GlosaError) {
		this.fld_GlosaError = fld_GlosaError;
	}

	public String getFld_TipoError() {
		return this.fld_TipoError;
	}

	public void setFld_TipoError(String fld_TipoError) {
		this.fld_TipoError = fld_TipoError;
	}

}