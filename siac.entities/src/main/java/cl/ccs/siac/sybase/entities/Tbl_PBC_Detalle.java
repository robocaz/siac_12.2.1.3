package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_PBC_Detalle database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PBC_Detalle.findAll", query="SELECT t FROM Tbl_PBC_Detalle t")
public class Tbl_PBC_Detalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodResolucion")
	private String fld_CodResolucion;

	@Column(name="Fld_CodRetAcl")
	private BigDecimal fld_CodRetAcl;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrControlPBC")
	private BigDecimal fld_CorrControlPBC;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FecMarcaAcl")
	private Timestamp fld_FecMarcaAcl;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_FlagAclaracion")
	private byte fld_FlagAclaracion;

	@Column(name="Fld_FlagFecAclaracion")
	private Timestamp fld_FlagFecAclaracion;

	@Column(name="Fld_FlagUsuario")
	private String fld_FlagUsuario;

	@Column(name="Fld_MarcaAcl")
	private byte fld_MarcaAcl;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NombreLibrador")
	private String fld_NombreLibrador;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_PBC_Detalle() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_CodResolucion() {
		return this.fld_CodResolucion;
	}

	public void setFld_CodResolucion(String fld_CodResolucion) {
		this.fld_CodResolucion = fld_CodResolucion;
	}

	public BigDecimal getFld_CodRetAcl() {
		return this.fld_CodRetAcl;
	}

	public void setFld_CodRetAcl(BigDecimal fld_CodRetAcl) {
		this.fld_CodRetAcl = fld_CodRetAcl;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrControlPBC() {
		return this.fld_CorrControlPBC;
	}

	public void setFld_CorrControlPBC(BigDecimal fld_CorrControlPBC) {
		this.fld_CorrControlPBC = fld_CorrControlPBC;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public Timestamp getFld_FecMarcaAcl() {
		return this.fld_FecMarcaAcl;
	}

	public void setFld_FecMarcaAcl(Timestamp fld_FecMarcaAcl) {
		this.fld_FecMarcaAcl = fld_FecMarcaAcl;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public byte getFld_FlagAclaracion() {
		return this.fld_FlagAclaracion;
	}

	public void setFld_FlagAclaracion(byte fld_FlagAclaracion) {
		this.fld_FlagAclaracion = fld_FlagAclaracion;
	}

	public Timestamp getFld_FlagFecAclaracion() {
		return this.fld_FlagFecAclaracion;
	}

	public void setFld_FlagFecAclaracion(Timestamp fld_FlagFecAclaracion) {
		this.fld_FlagFecAclaracion = fld_FlagFecAclaracion;
	}

	public String getFld_FlagUsuario() {
		return this.fld_FlagUsuario;
	}

	public void setFld_FlagUsuario(String fld_FlagUsuario) {
		this.fld_FlagUsuario = fld_FlagUsuario;
	}

	public byte getFld_MarcaAcl() {
		return this.fld_MarcaAcl;
	}

	public void setFld_MarcaAcl(byte fld_MarcaAcl) {
		this.fld_MarcaAcl = fld_MarcaAcl;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public String getFld_NombreLibrador() {
		return this.fld_NombreLibrador;
	}

	public void setFld_NombreLibrador(String fld_NombreLibrador) {
		this.fld_NombreLibrador = fld_NombreLibrador;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}