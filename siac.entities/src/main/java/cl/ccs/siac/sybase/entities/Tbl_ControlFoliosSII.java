package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlFoliosSII database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlFoliosSII.findAll", query="SELECT t FROM Tbl_ControlFoliosSII t")
public class Tbl_ControlFoliosSII implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tbl_ControlFoliosSIIPK id;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_FecAutorizacion")
	private Timestamp fld_FecAutorizacion;

	@Column(name="Fld_FolioActual")
	private int fld_FolioActual;

	@Column(name="Fld_FolioDesde")
	private int fld_FolioDesde;

	@Column(name="Fld_FolioHasta")
	private int fld_FolioHasta;

	public Tbl_ControlFoliosSII() {
	}

	public Tbl_ControlFoliosSIIPK getId() {
		return this.id;
	}

	public void setId(Tbl_ControlFoliosSIIPK id) {
		this.id = id;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public Timestamp getFld_FecAutorizacion() {
		return this.fld_FecAutorizacion;
	}

	public void setFld_FecAutorizacion(Timestamp fld_FecAutorizacion) {
		this.fld_FecAutorizacion = fld_FecAutorizacion;
	}

	public int getFld_FolioActual() {
		return this.fld_FolioActual;
	}

	public void setFld_FolioActual(int fld_FolioActual) {
		this.fld_FolioActual = fld_FolioActual;
	}

	public int getFld_FolioDesde() {
		return this.fld_FolioDesde;
	}

	public void setFld_FolioDesde(int fld_FolioDesde) {
		this.fld_FolioDesde = fld_FolioDesde;
	}

	public int getFld_FolioHasta() {
		return this.fld_FolioHasta;
	}

	public void setFld_FolioHasta(int fld_FolioHasta) {
		this.fld_FolioHasta = fld_FolioHasta;
	}

}