package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_NivelEducacional database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_NivelEducacional.findAll", query="SELECT t FROM Tbl_NivelEducacional t")
public class Tbl_NivelEducacional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaNivelEducacional")
	private String fld_GlosaNivelEducacional;

	@Column(name="Fld_NivelEducacional")
	private byte fld_NivelEducacional;

	public Tbl_NivelEducacional() {
	}

	public String getFld_GlosaNivelEducacional() {
		return this.fld_GlosaNivelEducacional;
	}

	public void setFld_GlosaNivelEducacional(String fld_GlosaNivelEducacional) {
		this.fld_GlosaNivelEducacional = fld_GlosaNivelEducacional;
	}

	public byte getFld_NivelEducacional() {
		return this.fld_NivelEducacional;
	}

	public void setFld_NivelEducacional(byte fld_NivelEducacional) {
		this.fld_NivelEducacional = fld_NivelEducacional;
	}

}