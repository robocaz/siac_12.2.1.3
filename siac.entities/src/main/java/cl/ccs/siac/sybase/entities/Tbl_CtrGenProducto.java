package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CtrGenProductos database table.
 * 
 */
@Entity
@Table(name="Tbl_CtrGenProductos")
@NamedQuery(name="Tbl_CtrGenProducto.findAll", query="SELECT t FROM Tbl_CtrGenProducto t")
public class Tbl_CtrGenProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_CorrGen_Id")
	private BigDecimal fld_CorrGen_Id;

	@Column(name="Fld_FecGeneracion")
	private Timestamp fld_FecGeneracion;

	@Column(name="Fld_FecMinGeneracion")
	private Timestamp fld_FecMinGeneracion;

	@Column(name="Fld_FlagGeneracion")
	private byte fld_FlagGeneracion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CtrGenProducto() {
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public BigDecimal getFld_CorrGen_Id() {
		return this.fld_CorrGen_Id;
	}

	public void setFld_CorrGen_Id(BigDecimal fld_CorrGen_Id) {
		this.fld_CorrGen_Id = fld_CorrGen_Id;
	}

	public Timestamp getFld_FecGeneracion() {
		return this.fld_FecGeneracion;
	}

	public void setFld_FecGeneracion(Timestamp fld_FecGeneracion) {
		this.fld_FecGeneracion = fld_FecGeneracion;
	}

	public Timestamp getFld_FecMinGeneracion() {
		return this.fld_FecMinGeneracion;
	}

	public void setFld_FecMinGeneracion(Timestamp fld_FecMinGeneracion) {
		this.fld_FecMinGeneracion = fld_FecMinGeneracion;
	}

	public byte getFld_FlagGeneracion() {
		return this.fld_FlagGeneracion;
	}

	public void setFld_FlagGeneracion(byte fld_FlagGeneracion) {
		this.fld_FlagGeneracion = fld_FlagGeneracion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}