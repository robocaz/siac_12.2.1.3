package cl.ccs.siac.certificados;

import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;

public interface CertificadosBusiness {

	public ConsultaBICDTO generaCOACBIC(String user, CertificadoDTO certificado);

	public ConsultaMOLDTO generaCOACMOL(String user, CertificadoDTO certificado, String codAutenticacionBIC);

	public ConsultaCerAclDTO generaCAA(ConsultaCerAclDTO consultaCerAclDTO);

	public CertificadoAclDTO generaCAC(ConsultaCerAclDTO consultaCerAclDTO);

	public ConsCertificadoILPDTO generaILP(ConsCertificadoILPDTO consCertificado);

	public ConsCertificadoICMDTO generaICM(ConsCertificadoICMDTO consCertificado);
	
	public TeAvisaDTO getDatosContratoTeAvisa(TeAvisaDTO contrato);
}
