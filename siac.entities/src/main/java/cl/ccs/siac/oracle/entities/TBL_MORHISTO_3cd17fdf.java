package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the "TBL_MORHISTO_3cd17fdf" database table.
 * 
 */
@Entity
@Table(name="\"TBL_MORHISTO_3cd17fdf\"")
@NamedQuery(name="TBL_MORHISTO_3cd17fdf.findAll", query="SELECT t FROM TBL_MORHISTO_3cd17fdf t")
public class TBL_MORHISTO_3cd17fdf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	@Column(name="FLD_CORRENVIO")
	private BigDecimal fldCorrenvio;

	@Column(name="FLD_CORRENVIOBAJA")
	private BigDecimal fldCorrenviobaja;

	@Column(name="FLD_CORRENVIOCARGA")
	private BigDecimal fldCorrenviocarga;

	@Column(name="FLD_CORRMOROSIDAD_ID")
	private BigDecimal fldCorrmorosidadId;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_FECHACREACION")
	private Timestamp fldFechacreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	@Column(name="FLD_USUARIOCREACION")
	private String fldUsuariocreacion;

	public TBL_MORHISTO_3cd17fdf() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}

	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public BigDecimal getFldCorrenvio() {
		return this.fldCorrenvio;
	}

	public void setFldCorrenvio(BigDecimal fldCorrenvio) {
		this.fldCorrenvio = fldCorrenvio;
	}

	public BigDecimal getFldCorrenviobaja() {
		return this.fldCorrenviobaja;
	}

	public void setFldCorrenviobaja(BigDecimal fldCorrenviobaja) {
		this.fldCorrenviobaja = fldCorrenviobaja;
	}

	public BigDecimal getFldCorrenviocarga() {
		return this.fldCorrenviocarga;
	}

	public void setFldCorrenviocarga(BigDecimal fldCorrenviocarga) {
		this.fldCorrenviocarga = fldCorrenviocarga;
	}

	public BigDecimal getFldCorrmorosidadId() {
		return this.fldCorrmorosidadId;
	}

	public void setFldCorrmorosidadId(BigDecimal fldCorrmorosidadId) {
		this.fldCorrmorosidadId = fldCorrmorosidadId;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public Timestamp getFldFechacreacion() {
		return this.fldFechacreacion;
	}

	public void setFldFechacreacion(Timestamp fldFechacreacion) {
		this.fldFechacreacion = fldFechacreacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public String getFldUsuariocreacion() {
		return this.fldUsuariocreacion;
	}

	public void setFldUsuariocreacion(String fldUsuariocreacion) {
		this.fldUsuariocreacion = fldUsuariocreacion;
	}

}