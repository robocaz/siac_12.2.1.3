package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

abstract class BoletinDataGeneric implements Serializable{

	protected String tipoDeudaLaboral;
	protected String nombreInstitucion;
	protected String rut;
	protected String nombre;
	protected Integer periodo;
	protected BigDecimal montoLaboralUTM;
	protected BigDecimal montoAdeudado;

	public String getTipoDeudaLaboral() {
		return tipoDeudaLaboral;
	}

	public void setTipoDeudaLaboral(String tipoDeudaLaboral) {
		this.tipoDeudaLaboral = tipoDeudaLaboral;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public BigDecimal getMontoLaboralUTM() {
		return montoLaboralUTM;
	}

	public void setMontoLaboralUTM(BigDecimal montoLaboralUTM) {
		this.montoLaboralUTM = montoLaboralUTM;
	}

	public BigDecimal getMontoAdeudado() {
		return montoAdeudado;
	}

	public void setMontoAdeudado(BigDecimal montoAdeudado) {
		this.montoAdeudado = montoAdeudado;
	}

}
