package cl.ccs.siac.cuadratura;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.CuadraturaDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.CajaInformeDTO;
import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaAclaracionDTO;
import cl.exe.ccs.dto.CuadraturaCajaDTO;
import cl.exe.ccs.dto.CuadraturaCentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaUsuarioDTO;
import cl.exe.ccs.dto.DatoBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleBoletaDTO;
import cl.exe.ccs.dto.DetalleBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleCentroCostoDTO;
import cl.exe.ccs.dto.DetalleServicioDTO;
import cl.exe.ccs.dto.PorcentajeIvaDTO;
import cl.exe.ccs.dto.ReclamoDTO;
import cl.exe.ccs.dto.ResumenMovimientoDTO;
import cl.exe.ccs.dto.UsuariosInformeDTO;

@Named("cuadraturaBusiness")
public class CuadraturaBusinessImpl implements CuadraturaBusiness {
	
	@Inject
	private CuadraturaDao cuadraturaDao;
	
	@Inject
	private Utils util;

	public List<CentroCostoDTO> centrosCostoInforme(Integer centroCosto, String desde, String hasta){
		
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		
		if(null==desdeDate || null==hastaDate) return null;
		
		return cuadraturaDao.centrosCostoInforme(centroCosto, desdeDate, hastaDate);
	}
	
	public List<UsuariosInformeDTO> usuariosInforme(Integer centroCosto, String desde, String hasta){
		
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		if(null==desdeDate || null==hastaDate) return null;
		return cuadraturaDao.usuariosInforme(centroCosto, desdeDate, hastaDate);
	}
	
	public List<CajaInformeDTO> cajaInforme(String usuario, Integer centroCosto, String desde, String hasta){
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		if(null==desdeDate || null==hastaDate) return null;
		return cuadraturaDao.cajaInforme(usuario, centroCosto, desdeDate, hastaDate);
	}

	
	public CuadraturaCentroCostoDTO cuadraturaPorCamara(Integer centroCosto, String desde, String hasta){
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		CuadraturaCentroCostoDTO cuadraturaCC =  cuadraturaDao.cuadraturaPorCamara(centroCosto, desdeDate, hastaDate);
		cuadraturaCC.setFechaDesde(util.formatearFecha(desdeDate.getTime()));
		cuadraturaCC.setFechaHasta(util.formatearFecha(hastaDate.getTime()));
		return cuadraturaCC;
	}

	public CuadraturaUsuarioDTO cuadraturaPorUsuario(String usuario, Integer centroCosto, String desde, String hasta){
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		CuadraturaUsuarioDTO cuadraturaUsuario =  cuadraturaDao.cuadraturaPorUsuario(centroCosto, usuario, desdeDate, hastaDate);
		cuadraturaUsuario.setFechaDesde(util.formatearFecha(desdeDate.getTime()));
		cuadraturaUsuario.setFechaHasta(util.formatearFecha(hastaDate.getTime()));
		cuadraturaUsuario.setUsuario(usuario);
		cuadraturaUsuario.setCentroCosto(centroCosto);
		return cuadraturaUsuario;

	}

	public DetalleCentroCostoDTO cuadraturaPorCentroCosto(Integer centroCosto, String desde, String hasta){
		Date desdeDate = util.formatearFechaFromForm(desde);
		Date hastaDate = util.formatearFechaFromForm(hasta);
		DetalleCentroCostoDTO detCentros = cuadraturaDao.cuadraturaPorCentroCosto(centroCosto, desdeDate, hastaDate);
		detCentros.setFechaDesde(util.formatearFecha(desdeDate.getTime()));
		detCentros.setFechaHasta(util.formatearFecha(hastaDate.getTime()));
		detCentros.setCentroCosto(centroCosto);
		return detCentros;

	}
	
	
	public PorcentajeIvaDTO obtienePorcentajeIva(){
		return cuadraturaDao.obtienePorcentajeIva();
	}
	
	public List<CuadraturaCajaDTO> buscarCajasPorUsuario(String usuario, Integer centroCosto, String fechaInicio){
		Date desdeDate = util.formatearFechaFromForm(fechaInicio);
		List<CuadraturaCajaDTO> cuadCajas = cuadraturaDao.buscarCajasPorUsuario(usuario, centroCosto, desdeDate);
		return cuadCajas;
	}
	
	public List<CuadraturaCajaDTO> buscarCajasPorCajero(String usuario, Long fechaInicio){
		return cuadraturaDao.buscarCajasPorCajero(usuario, fechaInicio);
	}
	
	
	public ResumenMovimientoDTO obtieneMovimientosNac(Integer centroCosto, String fechaInicio, String fechaDesde){
		Date desdeDate = util.formatearFechaFromForm(fechaInicio);
		Date hastaDate = util.formatearFechaFromForm(fechaDesde);
		ResumenMovimientoDTO resumen = cuadraturaDao.obtieneMovimientosNac(centroCosto, desdeDate, hastaDate);
		resumen.setFechaDesde(util.formatearFecha(desdeDate.getTime()));
		resumen.setFechaHasta(util.formatearFecha(hastaDate.getTime()));

		return resumen;
	}
	
	public ResumenMovimientoDTO obtieneMovimientos(String fechaInicio, String usuario, Integer centroCosto, Integer corrCaja, Integer criterio ){
		Date desdeDate = util.formatearFechaFromForm(fechaInicio);
		ResumenMovimientoDTO resumen = cuadraturaDao.obtieneMovimientos(desdeDate, usuario, centroCosto, corrCaja, criterio);
		resumen.setFechaDesde(util.formatearFecha(desdeDate.getTime()));
		return resumen;
	}
	
	public DetalleBoletaDTO obtieneDetalleBoleta(String fechaInicio, String usuario, Integer corrCaja, Integer centroCosto, Integer criterio){
		Date desdeDate = util.formatearFechaFromForm(fechaInicio);
		return cuadraturaDao.obtieneDetalleBoleta(desdeDate, usuario, corrCaja, centroCosto, criterio);
	}

	public DetalleBoletaFacturaDTO obtieneDetalleBoletaFactura(DetalleBoletaFacturaDTO detalle){
		return cuadraturaDao.obtieneDetalleBoletaFactura(detalle);
	}
	
	public List<DatoBoletaFacturaDTO> getDetalleBoletaModal(DetalleBoletaFacturaDTO detalle){
		return cuadraturaDao.getDetalleBoletaModal(detalle);
	}
	
	public DetalleBoletaFacturaDTO getDetalleFacturaModal(DetalleBoletaFacturaDTO detalle){
		return cuadraturaDao.getDetalleFacturaModal(detalle);
	}

	
	public DetalleServicioDTO obtieneDetalleServicio(String fechaInicio, String usuario, Integer corrCaja,Integer centroCosto, Integer criterio, Integer ordenamiento){
		Date desdeDate = util.formatearFechaFromForm(fechaInicio);
		return cuadraturaDao.obtieneDetalleServicio(desdeDate, usuario, corrCaja, centroCosto, criterio, ordenamiento);
	}
	
	public CuadraturaAclaracionDTO validaAclaraciones(Integer corrCaja){
		return cuadraturaDao.validaAclaraciones(corrCaja);
	}
	
	public ReclamoDTO solicitudesReclamos(Integer corrCaja){
		return cuadraturaDao.solicitudesReclamos(corrCaja);
	}
}
