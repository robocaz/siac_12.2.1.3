package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdEstadoPareo database table.
 * 
 */
@Entity
@NamedQuery(name="Tp_CeIdEstadoPareo.findAll", query="SELECT t FROM Tp_CeIdEstadoPareo t")
public class Tp_CeIdEstadoPareo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodPareo")
	private byte fld_CodPareo;

	@Column(name="Fld_GlosaPareo")
	private String fld_GlosaPareo;

	public Tp_CeIdEstadoPareo() {
	}

	public byte getFld_CodPareo() {
		return this.fld_CodPareo;
	}

	public void setFld_CodPareo(byte fld_CodPareo) {
		this.fld_CodPareo = fld_CodPareo;
	}

	public String getFld_GlosaPareo() {
		return this.fld_GlosaPareo;
	}

	public void setFld_GlosaPareo(String fld_GlosaPareo) {
		this.fld_GlosaPareo = fld_GlosaPareo;
	}

}