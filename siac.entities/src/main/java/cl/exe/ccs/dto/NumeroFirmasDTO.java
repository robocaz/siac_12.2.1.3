package cl.exe.ccs.dto;

import java.io.Serializable;

public class NumeroFirmasDTO extends ResponseDTO implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5592404184122558506L;
	private String tipoEmisor;
	private String codEmisor;
	private String tipoDocAclaracion;
	private String tipoDocumento;
	private Integer nroFirmas;
	private Integer usaNrosConfirmatorios;
	private Integer flagFirmaNroConf;
	private Integer flagUsoOpcional;
	
	
	
	public String getTipoEmisor() {
		return tipoEmisor;
	}
	public void setTipoEmisor(String tipoEmisor) {
		this.tipoEmisor = tipoEmisor;
	}
	public String getCodEmisor() {
		return codEmisor;
	}
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}
	public String getTipoDocAclaracion() {
		return tipoDocAclaracion;
	}
	public void setTipoDocAclaracion(String tipoDocAclaracion) {
		this.tipoDocAclaracion = tipoDocAclaracion;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Integer getNroFirmas() {
		return nroFirmas;
	}
	public void setNroFirmas(Integer nroFirmas) {
		this.nroFirmas = nroFirmas;
	}
	public Integer getUsaNrosConfirmatorios() {
		return usaNrosConfirmatorios;
	}
	public void setUsaNrosConfirmatorios(Integer usaNrosConfirmatorios) {
		this.usaNrosConfirmatorios = usaNrosConfirmatorios;
	}
	public Integer getFlagFirmaNroConf() {
		return flagFirmaNroConf;
	}
	public void setFlagFirmaNroConf(Integer flagFirmaNroConf) {
		this.flagFirmaNroConf = flagFirmaNroConf;
	}
	public Integer getFlagUsoOpcional() {
		return flagUsoOpcional;
	}
	public void setFlagUsoOpcional(Integer flagUsoOpcional) {
		this.flagUsoOpcional = flagUsoOpcional;
	}
	
	
	
	

}
