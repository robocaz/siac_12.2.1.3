package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_TIPOEMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_TIPOEMISOR")
@NamedQuery(name="TblTipoemisor.findAll", query="SELECT t FROM TblTipoemisor t")
public class TblTipoemisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_TIPOEMISOR_FLDTIPOEMISOR_GENERATOR", sequenceName="FLD_TIPOEMISOR")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_TIPOEMISOR_FLDTIPOEMISOR_GENERATOR")
	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_GLOSATIPOEMISOR")
	private String fldGlosatipoemisor;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblEmisor
	@OneToMany(mappedBy="tblTipoemisor")
	private List<TblEmisor> tblEmisors;

	public TblTipoemisor() {
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldGlosatipoemisor() {
		return this.fldGlosatipoemisor;
	}

	public void setFldGlosatipoemisor(String fldGlosatipoemisor) {
		this.fldGlosatipoemisor = fldGlosatipoemisor;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblEmisor> getTblEmisors() {
		return this.tblEmisors;
	}

	public void setTblEmisors(List<TblEmisor> tblEmisors) {
		this.tblEmisors = tblEmisors;
	}

	public TblEmisor addTblEmisor(TblEmisor tblEmisor) {
		getTblEmisors().add(tblEmisor);
		tblEmisor.setTblTipoemisor(this);

		return tblEmisor;
	}

	public TblEmisor removeTblEmisor(TblEmisor tblEmisor) {
		getTblEmisors().remove(tblEmisor);
		tblEmisor.setTblTipoemisor(null);

		return tblEmisor;
	}

}