angular.module('ccs').controller('CarroCompraController', function($scope, $rootScope, $location, $filter, CarroCompraResource, PagosResource, TransaccionResource, AclaracionResource, TramitanteResource, RutHelper, LoginResource) {

  $scope.carrocompra = {};
  $scope.servicios = [];//CarroCompraResource.cargaServicios();
  $scope._servicios = [];
  $rootScope.disableTram = true;

  
	$scope.cargaServicioLista = function(){
     var successCallback = function(data,responseHeaders){
        if(data !== null){
          $scope.servicios = data;
		  $scope._servicios = $scope.servicios;
		  $scope.validaDeclaracionUso();
        }

      };
      CarroCompraResource.cargaServicios(successCallback, $scope.errorCallback);
      		
		
    }
  
  $scope.validaServicioModal = function(servicio) {
	$rootScope.codServicio = servicio;
	
	console.log($rootScope);
	if($rootScope.usuario.caja === undefined || $rootScope.usuario.caja === null){
		$scope.getUsuario();
	}	
	
    if(servicio == 'ISA') {
      $('#informecertificado').modal('show');
    }
    else if(servicio == 'ACL') {
      $('#aclaracion').modal('show');
    }
    else if(servicio == 'ICM') {
      $('#informecomercial1').modal('show');
    }
    else if(servicio == 'ILP') {
      $('#informelistapublico').modal('show');
    }
    else if(servicio == 'ILG') {
		$rootScope.$emit('eventoLegal');
	    $scope.$digest();	
    }
    else if(servicio == 'CT1') {
      $('#certificadototal1').modal('show');
    }
    else if(servicio == 'CT2') {
      $('#certificadototal2').modal('show');
    }
    else if(servicio == 'ICT') {
      $('#consolidadoaclaraciontramite').modal('show');
    }
    else if(servicio == 'CAC') {
      $('#certificadoaclaracion4').modal('show');
    }
    else if(servicio == 'BLI') {
      $('#boletinlaboral').modal('show');
    }
    else if(servicio == 'BLR') {
      $('#boletinlaboral').modal('show');
    }
    else if(servicio == 'BLT') {
      $('#boletinlaboral').modal('show');
    }
    else if(servicio == 'ARM') {
      $('#bc_select').modal('show');
    }
    else if(servicio == 'AMM') {
      $('#bc_select').modal('show');
    }
    else if(servicio == 'ADM') {
      $('#bc_select').modal('show');
    }
    else if(servicio == 'CAA') {
      $('#tramiteagrupanterior').modal('show');
    }
    else if(servicio == 'CAR') {

      var successCallback = function(data,responseHeaders){
        if(data.idControl == 0){
          $rootScope.$emit('callEvent', data.lista, $scope.afectado);
          $scope.$digest();

        }
        else{
          swal('Alerta',"No hay Aclaraciones",'warning');
        }
      };
      AclaracionResource.consultaAclaracionProceso({rutTramitante: $rootScope.tramitante.rut, corrCaja: $rootScope.usuario.caja}, successCallback, $scope.errorCallback);
      
    }
  };

  $scope.listarServicioTramitante();

  $scope.declaracionUso = function(){
	console.log('CarroCompraController - declaracionUso');
	console.log($scope.transacciones);

	var serviciosNoAutorizados = false;
	if ( $scope.transacciones.length > 0 ) {
		for ( var t=0; t < $scope.transacciones.length; t++ ) {
			if ( $scope.transacciones[t].codServicio !== 'ISA' && $scope.transacciones[t].codServicio !== 'ICT' ) {
				serviciosNoAutorizados = true;
      }
    }
	}
	if ( serviciosNoAutorizados ) {
		swal({ title: 'Error', text: 'Existen en la grilla servicios no autorizados, para el ingreso de Declaración de Uso de la Información.',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(function () {
			$("#rutAfectadoInforme").focus();
		});
		return false;
	}
    angular.element('#declaracion').modal('show');
    angular.element("#declaracion").on('shown.bs.modal', function () {
       $rootScope.$broadcast('onDeclaracionUsoInicia', [$rootScope.tramitante, $rootScope.transacciones]);
    });
  }

  $scope.validaDeclaracionUso = function(){
	  if( $rootScope.tramitante.declaracionUso ) {
		  $scope.servicios = [$filter('propsFilter')($scope._servicios, {codServicio: 'ISA'})[0], $filter('propsFilter')($scope._servicios, {codServicio: 'ICT'})[0]];
	  }else {
		  $scope.servicios = $scope._servicios;
	  }
  }
  $scope.$on('onDeclaracionUsoTermina', function(event, args) {
	  console.log('CarroCompraController - onDeclaracionUsoTermina');
	  console.log(args);

	  // $scope.tramitante = args;
	  $rootScope.tramitante = args;

	  console.log('CarroCompraController - servicios');
	  console.log($scope.servicios);

	  $scope._servicios = $scope.servicios;

	  if( $rootScope.tramitante.declaracionUso ) {
		  $scope.servicios = [$filter('propsFilter')($scope._servicios, {codServicio: 'ISA'})[0], $filter('propsFilter')($scope._servicios, {codServicio: 'ICT'})[0]];
	  }else {
		  $scope.servicios = $scope._servicios;
	  }

    console.log($scope.servicios);

	  $scope.$applyAsync();
  });

  $scope.cancelarConfirm = function(){
    swal({
      title: 'Aviso',
      text: "¿Desea cancelar la transacción?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(function () {
      $scope.cancelarTransaccion();
    }, function(dismiss) {
      return false;
    });
  }

  $scope.cancelarTransaccion = function(){
    $scope.aceptaTransaccion = {};
    $scope.aceptaTransaccion.idCaja = $rootScope.usuario.caja;
    $scope.aceptaTransaccion.rutTramitante = $rootScope.tramitante.rut;

    var successCallback2 = function(data,responseHeaders){
      if(data.idControl == 0){
        $scope.tramitante = {};
        $rootScope.tramitante = {};
        $rootScope.disableTram = false;
        $rootScope.rutDisbale = false;
        $rootScope.transacciones = [];
        console.log("ok");
        $location.path("/atencion");
      }
    }
    PagosResource.cancelaTransaccion($scope.aceptaTransaccion, successCallback2, $scope.errorCallback);
  }

  $scope.volverTramitante = function(){
    $rootScope.rutDisbale = true;
    $rootScope.isback = true;
    $scope.rutSearch = "";
    $location.path("/atencion");
  }

  $scope.eliminarServicio = function(servicio){
    swal({
      title: 'Aviso',
      text: "¿Desea eliminar servicio?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then(function () {
      var successCallback2 = function(data,responseHeaders){
        if(data.idControl == 0){
          $scope.listarServicioTramitante();
        }
      }
      CarroCompraResource.eliminaServicios(servicio,successCallback2, $scope.errorCallback )
    }, function(dismiss) {
      return false;
    });

  }

  $scope.botonPagarEnable = function(){
    if($scope.transacionesSession.length == 0){
      $scope.tieneServicios = false;
    }
  }
  
  
  $scope.upgradeTeAvisa = function(){
	 
	 $rootScope.$emit('vigenteEvent');
	 //angular.element("#productosvigentes").modal();
    //$('#upgradeteavisa').modal('show');
  }
  
  $scope.upgradeTeAvisa();
  
  
  $scope.getUsuario = function() {
      var successCallback = function(data){
      	data.esAgencia = Number(data.centroCostoActivo.codigo) > 2300 && Number(data.centroCostoActivo.codigo) < 3000;
          $rootScope.usuario = data;
          $scope.getTramitanteSession();
      };
      LoginResource.status({}, successCallback, $scope.errorCallback);
  };
  
  $scope.getTramitanteSession = function() {
      var successCallback = function(data){
          $rootScope.tramitante = data;
          $scope.$digest();
      };
      TramitanteResource.obtenerPorCaja($rootScope.usuario.caja, successCallback, $scope.errorCallback);
  };

  //$scope.get();
 // 
   $scope.cargaServicioLista();
});
