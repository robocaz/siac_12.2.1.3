package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_TEST database table.
 * 
 */
@Entity
@Table(name="TBL_TEST")
@NamedQuery(name="TblTest.findAll", query="SELECT t FROM TblTest t")
public class TblTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_APMATERNO")
	private String fldApmaterno;

	@Column(name="FLD_APPATERNO")
	private String fldAppaterno;

	@Column(name="FLD_CODESTADO")
	private BigDecimal fldCodestado;

	@Column(name="FLD_CODMONEDA")
	private BigDecimal fldCodmoneda;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	@Column(name="FLD_COMUNA")
	private String fldComuna;

	@Column(name="FLD_CORRENVIODIG")
	private BigDecimal fldCorrenviodig;

	@Column(name="FLD_CORRMORDIG_ID")
	private BigDecimal fldCorrmordigId;

	@Column(name="FLD_DIRECCION")
	private String fldDireccion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHADIGITACION")
	private Date fldFechadigitacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAESTADO")
	private Date fldFechaestado;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAMODIFICACION")
	private Date fldFechamodificacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_FOLIOMOR")
	private BigDecimal fldFoliomor;

	@Column(name="FLD_GLOSACORTA")
	private String fldGlosacorta;

	@Column(name="FLD_MONTO")
	private BigDecimal fldMonto;

	@Column(name="FLD_MONTOSTR")
	private String fldMontostr;

	@Column(name="FLD_MOVIMIENTO")
	private BigDecimal fldMovimiento;

	@Column(name="FLD_NOMBRES")
	private String fldNombres;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_RETORNO")
	private BigDecimal fldRetorno;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_RUTRELACIONADO")
	private String fldRutrelacionado;

	@Column(name="FLD_STRFECHAVCTO")
	private String fldStrfechavcto;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	@Column(name="FLD_TIPORELACION")
	private String fldTiporelacion;

	@Column(name="FLD_USUARIODIGITACION")
	private String fldUsuariodigitacion;

	@Column(name="FLD_USUARIOMODIFICACION")
	private String fldUsuariomodificacion;

	public TblTest() {
	}

	public String getFldApmaterno() {
		return this.fldApmaterno;
	}

	public void setFldApmaterno(String fldApmaterno) {
		this.fldApmaterno = fldApmaterno;
	}

	public String getFldAppaterno() {
		return this.fldAppaterno;
	}

	public void setFldAppaterno(String fldAppaterno) {
		this.fldAppaterno = fldAppaterno;
	}

	public BigDecimal getFldCodestado() {
		return this.fldCodestado;
	}

	public void setFldCodestado(BigDecimal fldCodestado) {
		this.fldCodestado = fldCodestado;
	}

	public BigDecimal getFldCodmoneda() {
		return this.fldCodmoneda;
	}

	public void setFldCodmoneda(BigDecimal fldCodmoneda) {
		this.fldCodmoneda = fldCodmoneda;
	}

	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}

	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public String getFldComuna() {
		return this.fldComuna;
	}

	public void setFldComuna(String fldComuna) {
		this.fldComuna = fldComuna;
	}

	public BigDecimal getFldCorrenviodig() {
		return this.fldCorrenviodig;
	}

	public void setFldCorrenviodig(BigDecimal fldCorrenviodig) {
		this.fldCorrenviodig = fldCorrenviodig;
	}

	public BigDecimal getFldCorrmordigId() {
		return this.fldCorrmordigId;
	}

	public void setFldCorrmordigId(BigDecimal fldCorrmordigId) {
		this.fldCorrmordigId = fldCorrmordigId;
	}

	public String getFldDireccion() {
		return this.fldDireccion;
	}

	public void setFldDireccion(String fldDireccion) {
		this.fldDireccion = fldDireccion;
	}

	public Date getFldFechadigitacion() {
		return this.fldFechadigitacion;
	}

	public void setFldFechadigitacion(Date fldFechadigitacion) {
		this.fldFechadigitacion = fldFechadigitacion;
	}

	public Date getFldFechaestado() {
		return this.fldFechaestado;
	}

	public void setFldFechaestado(Date fldFechaestado) {
		this.fldFechaestado = fldFechaestado;
	}

	public Date getFldFechamodificacion() {
		return this.fldFechamodificacion;
	}

	public void setFldFechamodificacion(Date fldFechamodificacion) {
		this.fldFechamodificacion = fldFechamodificacion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public BigDecimal getFldFoliomor() {
		return this.fldFoliomor;
	}

	public void setFldFoliomor(BigDecimal fldFoliomor) {
		this.fldFoliomor = fldFoliomor;
	}

	public String getFldGlosacorta() {
		return this.fldGlosacorta;
	}

	public void setFldGlosacorta(String fldGlosacorta) {
		this.fldGlosacorta = fldGlosacorta;
	}

	public BigDecimal getFldMonto() {
		return this.fldMonto;
	}

	public void setFldMonto(BigDecimal fldMonto) {
		this.fldMonto = fldMonto;
	}

	public String getFldMontostr() {
		return this.fldMontostr;
	}

	public void setFldMontostr(String fldMontostr) {
		this.fldMontostr = fldMontostr;
	}

	public BigDecimal getFldMovimiento() {
		return this.fldMovimiento;
	}

	public void setFldMovimiento(BigDecimal fldMovimiento) {
		this.fldMovimiento = fldMovimiento;
	}

	public String getFldNombres() {
		return this.fldNombres;
	}

	public void setFldNombres(String fldNombres) {
		this.fldNombres = fldNombres;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public BigDecimal getFldRetorno() {
		return this.fldRetorno;
	}

	public void setFldRetorno(BigDecimal fldRetorno) {
		this.fldRetorno = fldRetorno;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldRutrelacionado() {
		return this.fldRutrelacionado;
	}

	public void setFldRutrelacionado(String fldRutrelacionado) {
		this.fldRutrelacionado = fldRutrelacionado;
	}

	public String getFldStrfechavcto() {
		return this.fldStrfechavcto;
	}

	public void setFldStrfechavcto(String fldStrfechavcto) {
		this.fldStrfechavcto = fldStrfechavcto;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public String getFldTiporelacion() {
		return this.fldTiporelacion;
	}

	public void setFldTiporelacion(String fldTiporelacion) {
		this.fldTiporelacion = fldTiporelacion;
	}

	public String getFldUsuariodigitacion() {
		return this.fldUsuariodigitacion;
	}

	public void setFldUsuariodigitacion(String fldUsuariodigitacion) {
		this.fldUsuariodigitacion = fldUsuariodigitacion;
	}

	public String getFldUsuariomodificacion() {
		return this.fldUsuariomodificacion;
	}

	public void setFldUsuariomodificacion(String fldUsuariomodificacion) {
		this.fldUsuariomodificacion = fldUsuariomodificacion;
	}

}