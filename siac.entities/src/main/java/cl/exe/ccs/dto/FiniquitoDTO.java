package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FiniquitoDTO extends ResponseDTO implements Serializable{
	
	private static final long serialVersionUID = 1338480126513950269L;
	
	private String rutAfectado;
	private BigDecimal corrFiniquito;
	private String fecIniContrato;
	private String fecFinContrato;
	private String fecFiniquito;
	private String rutEmpresa;
	private String nombreApPatEmp;
	private String nombreApMatEmp;
	private String nombreNombresEmp;
	private Integer codAfp;
	private Integer docPresentadoFnq;
	private Boolean marcaProrroga;
	private String fecDecJurada;
	private Integer retorno;
	private String mensaje;
	
	private BigDecimal corrCaja;
	private String nombreApPat;
	private String nombreApMat;
	private String nombreNombres;
	private String rutTramitante;
	private String nombreTramitante;
	private BigDecimal corrNomTram;
	private String fecUltCotizacion;
	private String fecCertCotizacion;
	private Integer estadoRespuesta;
	
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public BigDecimal getCorrFiniquito() {
		return corrFiniquito;
	}
	public void setCorrFiniquito(BigDecimal corrFiniquito) {
		this.corrFiniquito = corrFiniquito;
	}
	public String getFecIniContrato() {
		return fecIniContrato;
	}
	public void setFecIniContrato(String fecIniContrato) {
		this.fecIniContrato = fecIniContrato;
	}
	public String getFecFinContrato() {
		return fecFinContrato;
	}
	public void setFecFinContrato(String fecFinContrato) {
		this.fecFinContrato = fecFinContrato;
	}
	public String getFecFiniquito() {
		return fecFiniquito;
	}
	public void setFecFiniquito(String fecFiniquito) {
		this.fecFiniquito = fecFiniquito;
	}
	public String getRutEmpresa() {
		return rutEmpresa;
	}
	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}
	public String getNombreApPatEmp() {
		return nombreApPatEmp;
	}
	public void setNombreApPatEmp(String nombreApPatEmp) {
		this.nombreApPatEmp = nombreApPatEmp;
	}
	public String getNombreApMatEmp() {
		return nombreApMatEmp;
	}
	public void setNombreApMatEmp(String nombreApMatEmp) {
		this.nombreApMatEmp = nombreApMatEmp;
	}
	public String getNombreNombresEmp() {
		return nombreNombresEmp;
	}
	public void setNombreNombresEmp(String nombreNombresEmp) {
		this.nombreNombresEmp = nombreNombresEmp;
	}
	public Integer getCodAfp() {
		return codAfp;
	}
	public void setCodAfp(Integer codAfp) {
		this.codAfp = codAfp;
	}
	public Integer getDocPresentadoFnq() {
		return docPresentadoFnq;
	}
	public void setDocPresentadoFnq(Integer docPresentadoFnq) {
		this.docPresentadoFnq = docPresentadoFnq;
	}
	public Boolean getMarcaProrroga() {
		return marcaProrroga;
	}
	public void setMarcaProrroga(Boolean marcaProrroga) {
		this.marcaProrroga = marcaProrroga;
	}
	public String getFecDecJurada() {
		return fecDecJurada;
	}
	public void setFecDecJurada(String fecDecJurada) {
		this.fecDecJurada = fecDecJurada;
	}
	public Integer getRetorno() {
		return retorno;
	}
	public void setRetorno(Integer retorno) {
		this.retorno = retorno;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public BigDecimal getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(BigDecimal corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getNombreApPat() {
		return nombreApPat;
	}
	public void setNombreApPat(String nombreApPat) {
		this.nombreApPat = nombreApPat;
	}
	public String getNombreApMat() {
		return nombreApMat;
	}
	public void setNombreApMat(String nombreApMat) {
		this.nombreApMat = nombreApMat;
	}
	public String getNombreNombres() {
		return nombreNombres;
	}
	public void setNombreNombres(String nombreNombres) {
		this.nombreNombres = nombreNombres;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public String getNombreTramitante() {
		return nombreTramitante;
	}
	public void setNombreTramitante(String nombreTramitante) {
		this.nombreTramitante = nombreTramitante;
	}
	public BigDecimal getCorrNomTram() {
		return corrNomTram;
	}
	public void setCorrNomTram(BigDecimal corrNomTram) {
		this.corrNomTram = corrNomTram;
	}
	public String getFecUltCotizacion() {
		return fecUltCotizacion;
	}
	public void setFecUltCotizacion(String fecUltCotizacion) {
		this.fecUltCotizacion = fecUltCotizacion;
	}
	public String getFecCertCotizacion() {
		return fecCertCotizacion;
	}
	public void setFecCertCotizacion(String fecCertCotizacion) {
		this.fecCertCotizacion = fecCertCotizacion;
	}
	public Integer getEstadoRespuesta() {
		return estadoRespuesta;
	}
	public void setEstadoRespuesta(Integer estadoRespuesta) {
		this.estadoRespuesta = estadoRespuesta;
	}
	
	@Override
	public String toString() { 
		StringBuilder str = new StringBuilder();
		str.append("CorrFiniquito: " + corrFiniquito);
		str.append(" ,CorrCaja: " + corrCaja);
		str.append(" ,RutTramitante: " + rutTramitante);
		str.append(" ,RutAfectado: " + rutAfectado);
		str.append(" ,RutEmpresa: " + rutEmpresa);
		str.append(" ,nombreApPatEmp: " + nombreApPatEmp);
		str.append(" ,nombreApMatEmp: " + nombreApMatEmp);
		str.append(" ,nombreNombresEmp: " + nombreNombresEmp);
		str.append(" ,FecIniContrato: " + fecIniContrato);
		str.append(" ,FecFinContrato: " + fecFinContrato);
		str.append(" ,FecFiniquito: " + fecFiniquito);
		str.append(" ,DocPresentadoFnq: " + docPresentadoFnq);
		str.append(" ,MarcaProrroga: " + marcaProrroga);
		str.append(" ,FecDecJurada: " + fecDecJurada);
		str.append(" ,CodAfp: " + codAfp);
		str.append(" ,FecUltCotizacion: " + fecUltCotizacion);
		str.append(" ,FecCertCotizacion: " + fecCertCotizacion);
		return str.toString();
	}
	
}
