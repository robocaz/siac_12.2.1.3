package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoCartaDev database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoCartaDev.findAll", query="SELECT t FROM Tbl_TipoCartaDev t")
public class Tbl_TipoCartaDev implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoCarta")
	private String fld_GlosaTipoCarta;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoCarta")
	private short fld_TipoCarta;

	public Tbl_TipoCartaDev() {
	}

	public String getFld_GlosaTipoCarta() {
		return this.fld_GlosaTipoCarta;
	}

	public void setFld_GlosaTipoCarta(String fld_GlosaTipoCarta) {
		this.fld_GlosaTipoCarta = fld_GlosaTipoCarta;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public short getFld_TipoCarta() {
		return this.fld_TipoCarta;
	}

	public void setFld_TipoCarta(short fld_TipoCarta) {
		this.fld_TipoCarta = fld_TipoCarta;
	}

}