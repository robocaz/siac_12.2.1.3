package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_LGCONSULTA database table.
 * 
 */
@Entity
@Table(name="TBL_LGCONSULTA")
@NamedQuery(name="TblLgconsulta.findAll", query="SELECT t FROM TblLgconsulta t")
public class TblLgconsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CANTIDAD")
	private BigDecimal fldCantidad;

	@Column(name="FLD_CODTRANSACCION")
	private BigDecimal fldCodtransaccion;

	@Column(name="FLD_CORRLOGCONSULTA_ID")
	private BigDecimal fldCorrlogconsultaId;

	@Column(name="FLD_FECHATRANSACCION")
	private Timestamp fldFechatransaccion;

	@Column(name="FLD_FLAGBATCH")
	private BigDecimal fldFlagbatch;

	@Column(name="FLD_RESULTADO")
	private BigDecimal fldResultado;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_USUARIOCONSULTA")
	private String fldUsuarioconsulta;

	public TblLgconsulta() {
	}

	public BigDecimal getFldCantidad() {
		return this.fldCantidad;
	}

	public void setFldCantidad(BigDecimal fldCantidad) {
		this.fldCantidad = fldCantidad;
	}

	public BigDecimal getFldCodtransaccion() {
		return this.fldCodtransaccion;
	}

	public void setFldCodtransaccion(BigDecimal fldCodtransaccion) {
		this.fldCodtransaccion = fldCodtransaccion;
	}

	public BigDecimal getFldCorrlogconsultaId() {
		return this.fldCorrlogconsultaId;
	}

	public void setFldCorrlogconsultaId(BigDecimal fldCorrlogconsultaId) {
		this.fldCorrlogconsultaId = fldCorrlogconsultaId;
	}

	public Timestamp getFldFechatransaccion() {
		return this.fldFechatransaccion;
	}

	public void setFldFechatransaccion(Timestamp fldFechatransaccion) {
		this.fldFechatransaccion = fldFechatransaccion;
	}

	public BigDecimal getFldFlagbatch() {
		return this.fldFlagbatch;
	}

	public void setFldFlagbatch(BigDecimal fldFlagbatch) {
		this.fldFlagbatch = fldFlagbatch;
	}

	public BigDecimal getFldResultado() {
		return this.fldResultado;
	}

	public void setFldResultado(BigDecimal fldResultado) {
		this.fldResultado = fldResultado;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldUsuarioconsulta() {
		return this.fldUsuarioconsulta;
	}

	public void setFldUsuarioconsulta(String fldUsuarioconsulta) {
		this.fldUsuarioconsulta = fldUsuarioconsulta;
	}

}