package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RutAviPagoMas2Anos database table.
 * 
 */
@Entity
@Table(name="Tbl_RutAviPagoMas2Anos")
@NamedQuery(name="Tbl_RutAviPagoMas2Ano.findAll", query="SELECT t FROM Tbl_RutAviPagoMas2Ano t")
public class Tbl_RutAviPagoMas2Ano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_RUTAVIPAGOMAS2ANOS_FLD_RUTAFECTADO_GENERATOR", sequenceName="FLD_RUTAFECTADO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_RUTAVIPAGOMAS2ANOS_FLD_RUTAFECTADO_GENERATOR")
	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_NroAvisos")
	private int fld_NroAvisos;

	@Column(name="Fld_NroProtestos")
	private short fld_NroProtestos;

	public Tbl_RutAviPagoMas2Ano() {
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public int getFld_NroAvisos() {
		return this.fld_NroAvisos;
	}

	public void setFld_NroAvisos(int fld_NroAvisos) {
		this.fld_NroAvisos = fld_NroAvisos;
	}

	public short getFld_NroProtestos() {
		return this.fld_NroProtestos;
	}

	public void setFld_NroProtestos(short fld_NroProtestos) {
		this.fld_NroProtestos = fld_NroProtestos;
	}

}