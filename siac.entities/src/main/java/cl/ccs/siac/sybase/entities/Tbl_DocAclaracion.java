package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_DocAclaracion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DocAclaracion.findAll", query="SELECT t FROM Tbl_DocAclaracion t")
public class Tbl_DocAclaracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoDocAclaracion")
	private String fld_GlosaTipoDocAclaracion;

	@Column(name="Fld_Ordenamiento")
	private byte fld_Ordenamiento;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	public Tbl_DocAclaracion() {
	}

	public String getFld_GlosaTipoDocAclaracion() {
		return this.fld_GlosaTipoDocAclaracion;
	}

	public void setFld_GlosaTipoDocAclaracion(String fld_GlosaTipoDocAclaracion) {
		this.fld_GlosaTipoDocAclaracion = fld_GlosaTipoDocAclaracion;
	}

	public byte getFld_Ordenamiento() {
		return this.fld_Ordenamiento;
	}

	public void setFld_Ordenamiento(byte fld_Ordenamiento) {
		this.fld_Ordenamiento = fld_Ordenamiento;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

}