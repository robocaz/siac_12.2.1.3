angular.module('ccs').controller('ListaCajaController', function($document, $scope, CajaResource, $rootScope, $location, $filter, $timeout, LoginResource, $uibModal) {

    $scope.listaCajasAbiertas = [];

    $scope.caja = {
        id : "0"
    };

    $scope.listar = function(){
        var successCallback = function(data){
            $scope.listaCajasAbiertas = data;
        };
        CajaResource.cajasabiertas( successCallback, $scope.errorCallback);
    }
    $('#listacajas').on('show.bs.modal', function (e) {
        $timeout(function(){
          $scope.listar();
        });
    });
    $('#listacajas').on('shown.bs.modal', function (e) {
        $("input[name=group_listacaja")[0].focus();
    });

    
    
    $scope.cerrar = function(){
    	
    	$rootScope.tramitante = {};
    	$scope.tramitante = {};
    	angular.element('#listacajas').modal("hide");
    	$location.path("/inicio");
    	
    	
    }
    
    
    
    $scope.cerrarCaja = function(){
        var successCallback = function(data){
            $rootScope.usuario.caja = data.caja;
        };
        CajaResource.cerrarcajatemporal($rootScope.usuario.caja, successCallback, $scope.errorCallback);
    }

    $scope.cancelarSupervisor = function(){
        //$('.modalsupervisor-placeholder').empty();
        angular.element('#supervisor').modal("hide");
    }

    $scope.tomarcaja = function(){
        var successCallback = function(data){
            $scope.msgerror ="";
            $rootScope.usuario.caja = data.caja;
            angular.element('#listacajas').modal("hide");
            $location.path("/atencion");

        };
        CajaResource.tomarcaja($scope.listaCajasAbiertas[$scope.caja.id], successCallback, $scope.errorCallback)
    }

    $scope.aceptarSeleccion = function(){

        var caja = $scope.listaCajasAbiertas[$scope.caja.id];
        if(caja.estado==1){
            $scope.abrirModal();
            //angular.element('#supervisor').modal();
        }else if(caja.estado==2){
            $scope.tomarcaja();
        }else{
            console.log("ERROR");
        }
    }

    $scope.abrirModal = function(){
    	
		$scope.$emit('logout');

        var modalInstance = $uibModal.open({
          templateUrl: 'views/modal/modalloginsupervisor.html?nocache='+ new Date().getTime(),
          controller: 'ModalLoginSupervisorController',
          appendTo: angular.element($document[0].querySelector('#listacajas')),
          size: 'sm'
        });
        modalInstance.result.then(function (respuesta) {
            if(respuesta){
                var successCallback = function(data){
                    $rootScope.usuario.caja = data.caja;
                    angular.element('#listacajas').modal("hide");
                    $location.path("/atencion");
                };
                CajaResource.cajaseleccionada($scope.listaCajasAbiertas[$scope.caja.id].corrCaja , successCallback, $scope.errorCallback);
            }
        });
    }
});
