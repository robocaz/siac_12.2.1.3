package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_OrdenPagoTBK database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_OrdenPagoTBK.findAll", query="SELECT t FROM Tbl_OrdenPagoTBK t")
public class Tbl_OrdenPagoTBK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAutorizacionTC")
	private String fld_CodAutorizacionTC;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CodRespuesta")
	private byte fld_CodRespuesta;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_FecContableTC")
	private String fld_FecContableTC;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecExpiracionTC")
	private String fld_FecExpiracionTC;

	@Column(name="Fld_FecTransaccionTC")
	private String fld_FecTransaccionTC;

	@Column(name="Fld_HoraTransaccionTC")
	private String fld_HoraTransaccionTC;

	@Column(name="Fld_IdSesion")
	private String fld_IdSesion;

	@Column(name="Fld_IdTransaccionTC")
	private String fld_IdTransaccionTC;

	@Column(name="Fld_MacTC")
	private String fld_MacTC;

	@Column(name="Fld_MontoTransaccion")
	private BigDecimal fld_MontoTransaccion;

	@Column(name="Fld_NroCuotasTC")
	private byte fld_NroCuotasTC;

	@Column(name="Fld_NroTarjeta")
	private String fld_NroTarjeta;

	@Column(name="Fld_TipoPago")
	private String fld_TipoPago;

	@Column(name="Fld_TipoPagoTC")
	private String fld_TipoPagoTC;

	@Column(name="Fld_TipoTransaccion")
	private String fld_TipoTransaccion;

	public Tbl_OrdenPagoTBK() {
	}

	public String getFld_CodAutorizacionTC() {
		return this.fld_CodAutorizacionTC;
	}

	public void setFld_CodAutorizacionTC(String fld_CodAutorizacionTC) {
		this.fld_CodAutorizacionTC = fld_CodAutorizacionTC;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public byte getFld_CodRespuesta() {
		return this.fld_CodRespuesta;
	}

	public void setFld_CodRespuesta(byte fld_CodRespuesta) {
		this.fld_CodRespuesta = fld_CodRespuesta;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public String getFld_FecContableTC() {
		return this.fld_FecContableTC;
	}

	public void setFld_FecContableTC(String fld_FecContableTC) {
		this.fld_FecContableTC = fld_FecContableTC;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public String getFld_FecExpiracionTC() {
		return this.fld_FecExpiracionTC;
	}

	public void setFld_FecExpiracionTC(String fld_FecExpiracionTC) {
		this.fld_FecExpiracionTC = fld_FecExpiracionTC;
	}

	public String getFld_FecTransaccionTC() {
		return this.fld_FecTransaccionTC;
	}

	public void setFld_FecTransaccionTC(String fld_FecTransaccionTC) {
		this.fld_FecTransaccionTC = fld_FecTransaccionTC;
	}

	public String getFld_HoraTransaccionTC() {
		return this.fld_HoraTransaccionTC;
	}

	public void setFld_HoraTransaccionTC(String fld_HoraTransaccionTC) {
		this.fld_HoraTransaccionTC = fld_HoraTransaccionTC;
	}

	public String getFld_IdSesion() {
		return this.fld_IdSesion;
	}

	public void setFld_IdSesion(String fld_IdSesion) {
		this.fld_IdSesion = fld_IdSesion;
	}

	public String getFld_IdTransaccionTC() {
		return this.fld_IdTransaccionTC;
	}

	public void setFld_IdTransaccionTC(String fld_IdTransaccionTC) {
		this.fld_IdTransaccionTC = fld_IdTransaccionTC;
	}

	public String getFld_MacTC() {
		return this.fld_MacTC;
	}

	public void setFld_MacTC(String fld_MacTC) {
		this.fld_MacTC = fld_MacTC;
	}

	public BigDecimal getFld_MontoTransaccion() {
		return this.fld_MontoTransaccion;
	}

	public void setFld_MontoTransaccion(BigDecimal fld_MontoTransaccion) {
		this.fld_MontoTransaccion = fld_MontoTransaccion;
	}

	public byte getFld_NroCuotasTC() {
		return this.fld_NroCuotasTC;
	}

	public void setFld_NroCuotasTC(byte fld_NroCuotasTC) {
		this.fld_NroCuotasTC = fld_NroCuotasTC;
	}

	public String getFld_NroTarjeta() {
		return this.fld_NroTarjeta;
	}

	public void setFld_NroTarjeta(String fld_NroTarjeta) {
		this.fld_NroTarjeta = fld_NroTarjeta;
	}

	public String getFld_TipoPago() {
		return this.fld_TipoPago;
	}

	public void setFld_TipoPago(String fld_TipoPago) {
		this.fld_TipoPago = fld_TipoPago;
	}

	public String getFld_TipoPagoTC() {
		return this.fld_TipoPagoTC;
	}

	public void setFld_TipoPagoTC(String fld_TipoPagoTC) {
		this.fld_TipoPagoTC = fld_TipoPagoTC;
	}

	public String getFld_TipoTransaccion() {
		return this.fld_TipoTransaccion;
	}

	public void setFld_TipoTransaccion(String fld_TipoTransaccion) {
		this.fld_TipoTransaccion = fld_TipoTransaccion;
	}

}