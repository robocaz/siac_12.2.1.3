/**
 * 
 */
package cl.exe.ccs.dto.certificados;

import java.io.Serializable;

/**
 * @author emiranda
 *
 */
public class CertificadoICMDTO implements Serializable {

	private static final long serialVersionUID = 5920745781717252696L;

	private String rut;
	private Long CorrAutenticacion;
	private int numBoletinProt;
	private int pagBoletinProt;
	private String fechaProt;
	private String tipoDocumento;
	private String codEmisor;
	private int numOperDig;
	private String glosaCorta;
	private String montoProt;
	private String ciudad;
	private String glosaEmisor;
	private String nombreLiberador;
	private String fechaPublicacion;
	private String tipoDocumentoImpago;
	private String fechaAclaracion;
	private Integer marcaAclaracion;

	/**
	 * @return the rut
	 */
	public String getRut() {
		return rut;
	}

	/**
	 * @param rut
	 *            the rut to set
	 */
	public void setRut(String rut) {
		this.rut = rut;
	}

	/**
	 * @return the corrAutenticacion
	 */
	public Long getCorrAutenticacion() {
		return CorrAutenticacion;
	}

	/**
	 * @param corrAutenticacion
	 *            the corrAutenticacion to set
	 */
	public void setCorrAutenticacion(Long corrAutenticacion) {
		CorrAutenticacion = corrAutenticacion;
	}

	/**
	 * @return the numBoletinProt
	 */
	public int getNumBoletinProt() {
		return numBoletinProt;
	}

	/**
	 * @param numBoletinProt
	 *            the numBoletinProt to set
	 */
	public void setNumBoletinProt(int numBoletinProt) {
		this.numBoletinProt = numBoletinProt;
	}

	/**
	 * @return the pagBoletinProt
	 */
	public int getPagBoletinProt() {
		return pagBoletinProt;
	}

	/**
	 * @param pagBoletinProt
	 *            the pagBoletinProt to set
	 */
	public void setPagBoletinProt(int pagBoletinProt) {
		this.pagBoletinProt = pagBoletinProt;
	}

	/**
	 * @return the fechaProt
	 */
	public String getFechaProt() {
		return fechaProt;
	}

	/**
	 * @param fechaProt
	 *            the fechaProt to set
	 */
	public void setFechaProt(String fechaProt) {
		this.fechaProt = fechaProt;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the codEmisor
	 */
	public String getCodEmisor() {
		return codEmisor;
	}

	/**
	 * @param codEmisor
	 *            the codEmisor to set
	 */
	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}

	/**
	 * @return the numOperDig
	 */
	public int getNumOperDig() {
		return numOperDig;
	}

	/**
	 * @param numOperDig
	 *            the numOperDig to set
	 */
	public void setNumOperDig(int numOperDig) {
		this.numOperDig = numOperDig;
	}

	/**
	 * @return the glosaCorta
	 */
	public String getGlosaCorta() {
		return glosaCorta;
	}

	/**
	 * @param glosaCorta
	 *            the glosaCorta to set
	 */
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}

	/**
	 * @return the montoProt
	 */
	public String getMontoProt() {
		return montoProt;
	}

	/**
	 * @param montoProt
	 *            the montoProt to set
	 */
	public void setMontoProt(String montoProt) {
		this.montoProt = montoProt;
	}

	/**
	 * @return the ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * @param ciudad
	 *            the ciudad to set
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * @return the glosaEmisor
	 */
	public String getGlosaEmisor() {
		return glosaEmisor;
	}

	/**
	 * @param glosaEmisor
	 *            the glosaEmisor to set
	 */
	public void setGlosaEmisor(String glosaEmisor) {
		this.glosaEmisor = glosaEmisor;
	}

	/**
	 * @return the nombreLiberador
	 */
	public String getNombreLiberador() {
		return nombreLiberador;
	}

	/**
	 * @param nombreLiberador
	 *            the nombreLiberador to set
	 */
	public void setNombreLiberador(String nombreLiberador) {
		this.nombreLiberador = nombreLiberador;
	}

	/**
	 * @return the fechaPublicacion
	 */
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}

	/**
	 * @param fechaPublicacion
	 *            the fechaPublicacion to set
	 */
	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	/**
	 * @return the tipoDocumentoImpago
	 */
	public String getTipoDocumentoImpago() {
		return tipoDocumentoImpago;
	}

	/**
	 * @param tipoDocumentoImpago
	 *            the tipoDocumentoImpago to set
	 */
	public void setTipoDocumentoImpago(String tipoDocumentoImpago) {
		this.tipoDocumentoImpago = tipoDocumentoImpago;
	}

	/**
	 * @return the fechaAclaracion
	 */
	public String getFechaAclaracion() {
		return fechaAclaracion;
	}

	/**
	 * @param fechaAclaracion
	 *            the fechaAclaracion to set
	 */
	public void setFechaAclaracion(String fechaAclaracion) {
		this.fechaAclaracion = fechaAclaracion;
	}

	/**
	 * @return the marcaAclaracion
	 */
	public Integer getMarcaAclaracion() {
		return marcaAclaracion;
	}

	/**
	 * @param marcaAclaracion
	 *            the marcaAclaracion to set
	 */
	public void setMarcaAclaracion(Integer marcaAclaracion) {
		this.marcaAclaracion = marcaAclaracion;
	}

}
