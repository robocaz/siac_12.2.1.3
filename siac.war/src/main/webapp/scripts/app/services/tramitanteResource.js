angular.module('ccs').factory('TramitanteResource', function ($resource) {
    var resource = $resource('rest/tramitante/',  { nocache : new Date().getTime()},
    {
        'carga' :  { method : 'POST' ,url: 'rest/tramitante/carga'},
        'cargaRegiones' :  { method : 'GET' ,url: 'rest/tramitante/cargaRegiones',isArray: true },
        'cargaComunas' :  { method : 'GET' ,params:{idRegion:'idRegion'},url: 'rest/tramitante/cargaComunas',isArray: true },
        'actualizaTramitante' :  { method : 'POST', url: 'rest/tramitante/actualizatramitante', isArray: false},
        'status' :  { method : 'GET', url: 'rest/tramitante/status', isArray: false},
        'obtenerPorCaja' :  { method : 'POST', url: 'rest/tramitante/obtenertramitantecaja', isArray: false},
        'obtenerDeclaracionUso' : { method : 'POST', url: 'rest/tramitante/obtenerDeclaracionUso', isArray: false},
        'declaracionUso' :  { method : 'POST', url: 'rest/tramitante/declaracionUso', isArray: false},
        'cargaMotivos' :  { method : 'GET', url: 'rest/tramitante/cargaMotivos', isArray: true },
        'cargaDeclaracionUso' :  { method : 'POST', url: 'rest/tramitante/cargaDeclaracionUso', isArray: false}
    });
    return resource;
});
