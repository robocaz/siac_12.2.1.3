package cl.ccs.siac.solicitud;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.SolicitudDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarInDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoData;
import cl.exe.ccs.dto.SolicitudReclamoIngresoOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoIngresoInDTO;

@Named("solicitudBusiness")
public class SolicitudBusinessImpl implements SolicitudBusiness {
	
	@Inject
	private Utils utils;
	
	@Inject
	private SolicitudDao dao;
	
	@Override
	public SolicitudReclamoIngresoOutDTO ingreso(SolicitudReclamoIngresoInDTO params) {
		
		params.setRutTitular(utils.formatZeroRut(params.getRutTitular()));
		
		SolicitudReclamoIngresoOutDTO out = dao.ingreso(params);
		
		return out;
	}
	
	@Override
	public SolicitudReclamoBuscarOutDTO buscar(SolicitudReclamoBuscarInDTO params) {
		
		params.setRutTitular(utils.formatZeroRut(params.getRutTitular()));
		
		SolicitudReclamoBuscarOutDTO out = dao.buscar(params);
		
		for ( SolicitudReclamoData row : out.getLista() ) {
			row.setFechaSolicitud(utils.checkTimestampToStrFechaVista(row.getFechaSolicitud()));
			row.setFechaEstado(utils.checkTimestampToStrFechaVista(row.getFechaEstado()));
		}
		
		return out;
	}
	
	@Override
	public ResponseListDTO<String[]> historial(Integer corrSolic) {
		
		ResponseListDTO<String[]> out = dao.historial(corrSolic);
		
		for ( String[] row : out.getLista() ) {
			row[1] = utils.checkTimestampToStrFechaVista(row[1]);
		}
		
		return out;
	}

}
