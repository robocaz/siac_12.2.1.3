package cl.ccs.siac.pdf;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class CustomFooter extends PdfPageEventHelper {
    /** The header text. */
    String text;

    /** The template with the total number of pages. */
    PdfTemplate total;
    
    private Float posX;
    private Float posY;
    
    private String topText;
    
    public CustomFooter(){
        posX = 40f;
        posY = 75f;
    }
    
    
    /**
     * Allows us to change the content of the header.
     * @param header The new header String
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Creates the PdfTemplate that will hold the total number of pages.
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(
     *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30, 16);
    }

    /**
     * Adds a header to every page
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
     *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onEndPage(PdfWriter writer, Document document) {
        PdfPTable table = new PdfPTable(1);
        try {
            table.setWidths(new int[] { 24 });
            table.setTotalWidth(500);
            if (topText != null){
                table.addCell(cellCertificateText());
            }
            table.addCell(cellCertificateCode());

            table.writeSelectedRows(0, -1, posX, posY, writer.getDirectContent());
        } catch (Exception de) {
            throw new ExceptionConverter(de);
        }
    }
    

    /**
     * Fills out the total number of pages before the document is closed.
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(
     *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onCloseDocument(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                                   2, 2, 0);
    }

    public void setPosX(Float posX) {
        this.posX = posX;
    }

    public Float getPosX() {
        return posX;
    }

    public void setPosY(Float posY) {
        this.posY = posY;
    }

    public Float getPosY() {
        return posY;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getTopText() {
        return topText;
    }

    private PdfPCell cellCertificateCode() throws DocumentException {

        Font fontTextFooter = new Font();
        fontTextFooter.setStyle(Font.BOLD);
        Chunk chunkTextFooter = new Chunk(text);
        chunkTextFooter.setFont(fontTextFooter);
        PdfPCell cell = new PdfPCell();
        cell.addElement(chunkTextFooter);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private PdfPCell cellCertificateText() throws DocumentException {

        Font fontTextFooter = new Font();
        fontTextFooter.setSize(8);
        Chunk chunkTextFooter = new Chunk(topText);
        chunkTextFooter.setFont(fontTextFooter);
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.addElement(chunkTextFooter);
        return cell;
    }
}
   