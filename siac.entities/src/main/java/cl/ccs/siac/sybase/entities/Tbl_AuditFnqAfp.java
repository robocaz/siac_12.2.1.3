package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditFnqAfp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditFnqAfp.findAll", query="SELECT t FROM Tbl_AuditFnqAfp t")
public class Tbl_AuditFnqAfp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrFiniquito")
	private BigDecimal fld_CorrFiniquito;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecCertCotizacion")
	private Timestamp fld_FecCertCotizacion;

	@Column(name="Fld_FecUltCotizacion")
	private Timestamp fld_FecUltCotizacion;

	public Tbl_AuditFnqAfp() {
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrFiniquito() {
		return this.fld_CorrFiniquito;
	}

	public void setFld_CorrFiniquito(BigDecimal fld_CorrFiniquito) {
		this.fld_CorrFiniquito = fld_CorrFiniquito;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecCertCotizacion() {
		return this.fld_FecCertCotizacion;
	}

	public void setFld_FecCertCotizacion(Timestamp fld_FecCertCotizacion) {
		this.fld_FecCertCotizacion = fld_FecCertCotizacion;
	}

	public Timestamp getFld_FecUltCotizacion() {
		return this.fld_FecUltCotizacion;
	}

	public void setFld_FecUltCotizacion(Timestamp fld_FecUltCotizacion) {
		this.fld_FecUltCotizacion = fld_FecUltCotizacion;
	}

}