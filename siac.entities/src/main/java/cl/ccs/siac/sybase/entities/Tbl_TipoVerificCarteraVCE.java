package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoVerificCarteraVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoVerificCarteraVCE.findAll", query="SELECT t FROM Tbl_TipoVerificCarteraVCE t")
public class Tbl_TipoVerificCarteraVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProceso")
	private byte fld_CodProceso;

	@Column(name="Fld_CodVerificacion")
	private byte fld_CodVerificacion;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_GlosaVerificacion")
	private String fld_GlosaVerificacion;

	@Column(name="Fld_Parametro")
	private String fld_Parametro;

	public Tbl_TipoVerificCarteraVCE() {
	}

	public byte getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(byte fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public byte getFld_CodVerificacion() {
		return this.fld_CodVerificacion;
	}

	public void setFld_CodVerificacion(byte fld_CodVerificacion) {
		this.fld_CodVerificacion = fld_CodVerificacion;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_GlosaVerificacion() {
		return this.fld_GlosaVerificacion;
	}

	public void setFld_GlosaVerificacion(String fld_GlosaVerificacion) {
		this.fld_GlosaVerificacion = fld_GlosaVerificacion;
	}

	public String getFld_Parametro() {
		return this.fld_Parametro;
	}

	public void setFld_Parametro(String fld_Parametro) {
		this.fld_Parametro = fld_Parametro;
	}

}