package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CCostoAclBat database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CCostoAclBat.findAll", query="SELECT t FROM Tbl_CCostoAclBat t")
public class Tbl_CCostoAclBat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CargoContacto")
	private String fld_CargoContacto;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_DireccionContacto")
	private String fld_DireccionContacto;

	@Column(name="Fld_Flag_EnvioCentralizado")
	private boolean fld_Flag_EnvioCentralizado;

	@Column(name="Fld_FonoContacto")
	private String fld_FonoContacto;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_GlosaCCosto")
	private String fld_GlosaCCosto;

	@Column(name="Fld_NomContacto_ApMat")
	private String fld_NomContacto_ApMat;

	@Column(name="Fld_NomContacto_ApPat")
	private String fld_NomContacto_ApPat;

	@Column(name="Fld_NomContacto_Nombres")
	private String fld_NomContacto_Nombres;

	@Column(name="Fld_RazonSocialCCosto")
	private String fld_RazonSocialCCosto;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_RutContacto")
	private String fld_RutContacto;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_Ubicacion")
	private String fld_Ubicacion;

	public Tbl_CCostoAclBat() {
	}

	public String getFld_CargoContacto() {
		return this.fld_CargoContacto;
	}

	public void setFld_CargoContacto(String fld_CargoContacto) {
		this.fld_CargoContacto = fld_CargoContacto;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_DireccionContacto() {
		return this.fld_DireccionContacto;
	}

	public void setFld_DireccionContacto(String fld_DireccionContacto) {
		this.fld_DireccionContacto = fld_DireccionContacto;
	}

	public boolean getFld_Flag_EnvioCentralizado() {
		return this.fld_Flag_EnvioCentralizado;
	}

	public void setFld_Flag_EnvioCentralizado(boolean fld_Flag_EnvioCentralizado) {
		this.fld_Flag_EnvioCentralizado = fld_Flag_EnvioCentralizado;
	}

	public String getFld_FonoContacto() {
		return this.fld_FonoContacto;
	}

	public void setFld_FonoContacto(String fld_FonoContacto) {
		this.fld_FonoContacto = fld_FonoContacto;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public String getFld_GlosaCCosto() {
		return this.fld_GlosaCCosto;
	}

	public void setFld_GlosaCCosto(String fld_GlosaCCosto) {
		this.fld_GlosaCCosto = fld_GlosaCCosto;
	}

	public String getFld_NomContacto_ApMat() {
		return this.fld_NomContacto_ApMat;
	}

	public void setFld_NomContacto_ApMat(String fld_NomContacto_ApMat) {
		this.fld_NomContacto_ApMat = fld_NomContacto_ApMat;
	}

	public String getFld_NomContacto_ApPat() {
		return this.fld_NomContacto_ApPat;
	}

	public void setFld_NomContacto_ApPat(String fld_NomContacto_ApPat) {
		this.fld_NomContacto_ApPat = fld_NomContacto_ApPat;
	}

	public String getFld_NomContacto_Nombres() {
		return this.fld_NomContacto_Nombres;
	}

	public void setFld_NomContacto_Nombres(String fld_NomContacto_Nombres) {
		this.fld_NomContacto_Nombres = fld_NomContacto_Nombres;
	}

	public String getFld_RazonSocialCCosto() {
		return this.fld_RazonSocialCCosto;
	}

	public void setFld_RazonSocialCCosto(String fld_RazonSocialCCosto) {
		this.fld_RazonSocialCCosto = fld_RazonSocialCCosto;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_RutContacto() {
		return this.fld_RutContacto;
	}

	public void setFld_RutContacto(String fld_RutContacto) {
		this.fld_RutContacto = fld_RutContacto;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_Ubicacion() {
		return this.fld_Ubicacion;
	}

	public void setFld_Ubicacion(String fld_Ubicacion) {
		this.fld_Ubicacion = fld_Ubicacion;
	}

}