package cl.exe.ccs.resources;

import java.util.Properties;

public class Messages {
    /* Official from Uses Cases */
    
    private final static PropertiesManager propManager = new PropertiesManager();
    private final static Properties prop = propManager.getPropertiesMessages();
    
    public final static String SERVICE_OFFLINE = (String)prop.get("SERVICE_OFFLINE");
    
    public final static String REGISTER_TEAVISA_MAIL_SENT_OK = (String)prop.get("REGISTER_TEAVISA_MAIL_SENT_OK");
    public final static String REGISTER_TEAVISA_MAIL_SENT_ERROR = (String)prop.get("REGISTER_TEAVISA_MAIL_SENT_ERROR");
    public final static String REGISTER_TEAVISA_ALL_REGISTER_FIELDS_NECCESARY = (String)prop.get("REGISTER_TEAVISA_ALL_REGISTER_FIELDS_NECCESARY");
    public final static String REGISTER_TEAVISA_INVALID_CONFIRMATION_EMAIL = (String)prop.get("REGISTER_TEAVISA_INVALID_CONFIRMATION_EMAIL");
    public final static String REGISTER_TEAVISA_ALREADY_REGISTERED = (String)prop.get("REGISTER_TEAVISA_ALREADY_REGISTERED");

    public final static String REGISTER_CUSTOMER_INVALID_CONFIRMATION_EMAIL = (String)prop.get("REGISTER_CUSTOMER_INVALID_CONFIRMATION_EMAIL");
    public final static String REGISTER_CUSTOMER_ALL_REGISTER_FIELDS_NECCESARY = (String)prop.get("REGISTER_CUSTOMER_ALL_REGISTER_FIELDS_NECCESARY");
    public final static String REGISTER_CUSTOMER_ALREADY_REGISTERED = (String)prop.get("REGISTER_CUSTOMER_ALREADY_REGISTERED");
    public final static String REGISTER_CUSTOMER_DATABASE_GENERIC_ERROR = (String)prop.get("REGISTER_CUSTOMER_DATABASE_GENERIC_ERROR");
    public final static String REGISTER_CUSTOMER_EMAIL_BODY_TEMPLATE = (String)prop.get("REGISTER_CUSTOMER_EMAIL_BODY_TEMPLATE");

    public final static String LOGIN_NEEDS_FIELDS = (String)prop.get("LOGIN_NEEDS_FIELDS");
    public final static String LOGIN_FAILS = (String)prop.get("LOGIN_FAILS");

    public final static String ACCEPT_TERMS_AND_CONDITIONS = (String)prop.get("ACCEPT_TERMS_AND_CONDITIONS");
    public final static String ACCEPT_TERMS_AND_CONDITIONS_EMP = (String)prop.get("ACCEPT_TERMS_AND_CONDITIONS_EMP");

    public final static String FIRST_PASSWORD_CHANGE_SUCCESS = (String)prop.get("FIRST_PASSWORD_CHANGE_SUCCESS");
    public final static String FIRST_PASSWORD_CHANGE_PASSWORD_ERROR = (String)prop.get("FIRST_PASSWORD_CHANGE_PASSWORD_ERROR");
    public final static String FIRST_PASSWORD_CHANGE_MISSING_DATA = (String)prop.get("FIRST_PASSWORD_CHANGE_MISSING_DATA");
    public final static String FIRST_PASSWORD_CHANGE_NO_ACCEPTED_TERMS = (String)prop.get("FIRST_PASSWORD_CHANGE_NO_ACCEPTED_TERMS");
    public final static String FIRST_PASSWORD_CHANGE_NEW_PASSWORD_REPEATED_ERROR = (String)prop.get("FIRST_PASSWORD_CHANGE_NEW_PASSWORD_REPEATED_ERROR");
    public final static String FIRST_PASSWORD_CHANGE_PASSWORD_FORMAT_ERROR = (String)prop.get("FIRST_PASSWORD_CHANGE_PASSWORD_FORMAT_ERROR");
    public final static String FIRST_PASSWORD_CHANGE_PASSWORD_INPUT_IS_NOT_THE_PASSWORD = (String)prop.get("FIRST_PASSWORD_CHANGE_PASSWORD_INPUT_IS_NOT_THE_PASSWORD");

    public final static String CONTACT_US_MAIL_SENT_OK = (String)prop.get("CONTACT_US_MAIL_SENT_OK");
    public final static String CONTACT_US_FIELDS_FORMAT_ERROR = (String)prop.get("CONTACT_US_FIELDS_FORMAT_ERROR");

    public final static String PASSWORD_RECOVERY_SENT_OK = (String)prop.get("PASSWORD_RECOVERY_SENT_OK");
    public final static String PASSWORD_RECOVERY_NEEDS_FIELDS = (String)prop.get("PASSWORD_RECOVERY_NEEDS_FIELDS");
    public final static String PASSWORD_RECOVERY_INCORRECT_CMP = (String)prop.get("PASSWORD_RECOVERY_INCORRECT_CMP");

    public final static String PASSWORD_CHANGE_OK = (String)prop.get("PASSWORD_CHANGE_OK");
    public final static String PASSWORD_CHANGE_NEEDS_FIELDS = (String)prop.get("PASSWORD_CHANGE_NEEDS_FIELDS");
    public final static String PASSWORD_CHANGE_BAD_CONFIRMATION_PASSWORD = (String)prop.get("PASSWORD_CHANGE_BAD_CONFIRMATION_PASSWORD");
    public final static String PASSWORD_CHANGE_NEW_PASSWORD_INVALID = (String)prop.get("PASSWORD_CHANGE_NEW_PASSWORD_INVALID");
    public final static String PASSWORD_CHANGE_INCORRECT_CMP = (String)prop.get("PASSWORD_CHANGE_INCORRECT_CMP");
    public final static String PASSWORD_CHANGE_ERROR = (String)prop.get("PASSWORD_CHANGE_ERROR");

    /* Non official */

    public final static String PASSWORD_CHANGE_NEW_PASSWORD_EQUAL_OLD_PASSWORD = (String)prop.get("PASSWORD_CHANGE_NEW_PASSWORD_EQUAL_OLD_PASSWORD");

    public final static String REGISTER_CUSTOMER_OK = (String)prop.get("REGISTER_CUSTOMER_OK");

    public final static String DATABASE_GENERIC_ERROR = (String)prop.get("DATABASE_GENERIC_ERROR");

    public final static String UPDATE_USER_DATA_NEEDS_FIELDS = (String)prop.get("UPDATE_USER_DATA_NEEDS_FIELDS");
    public final static String UPDATE_USER_DATA_ANSWER_FORMAT_ERROR = (String)prop.get("UPDATE_USER_DATA_ANSWER_FORMAT_ERROR");
    public final static String UPDATE_USER_DATA_CHANGED = (String)prop.get("UPDATE_USER_DATA_CHANGED");
    public final static String UPDATE_USER_DATA_CONFIRMATION = (String)prop.get("UPDATE_USER_DATA_CONFIRMATION");
    public final static String UPDATE_USER_BAD_CONFIRMATION_EMAIL = (String)prop.get("UPDATE_USER_BAD_CONFIRMATION_EMAIL");

    public final static String FIRST_PASSWORD_CHANGE_ANSWER_FORMAT_ERROR =
        (String)prop.get("FIRST_PASSWORD_CHANGE_ANSWER_FORMAT_ERROR");
    public final static String ERROR_OCURRED = (String)prop.get("ERROR_OCURRED");
    public final static String INVALID_RUT = (String)prop.get("INVALID_RUT");
    public final static String INVALID_RUT_CHECK = (String)prop.get("INVALID_RUT_CHECK");
    public final static String INVALID_RUT_ENTERPRISE = (String)prop.get("INVALID_RUT_ENTERPRISE");
    public final static String INVALID_RUT_EXISTS = (String)prop.get("INVALID_RUT_EXISTS");
    public final static String INVALID_EMAIL = (String)prop.get("INVALID_EMAIL");

    public final static String INVALID_NAME = (String)prop.get("INVALID_NAME");
    public final static String INVALID_FIRST_LASTNAME = (String)prop.get("INVALID_FIRST_LASTNAME");
    public final static String INVALID_SECOND_LASTNAME = (String)prop.get("INVALID_SECOND_LASTNAME");

    public final static String INVALID_BIRTHDAY = (String)prop.get("INVALID_BIRTHDAY");

    public final static String INVALID_PHONE = (String)prop.get("INVALID_PHONE");
    public final static String INVALID_MOBILEPHONE = (String)prop.get("INVALID_MOBILEPHONE");

    public final static String INVALID_COMMUNE = (String)prop.get("INVALID_COMMUNE");


    public final static String CONTACT_US_MAIL_SENT_ERROR = (String)prop.get("CONTACT_US_MAIL_SENT_ERROR");

    public final static String PASSWORD_RECOVERY_SENT_ERROR = (String)prop.get("PASSWORD_RECOVERY_SENT_ERROR");

    public final static String INVALID_RUT_VALUE = (String)prop.get("INVALID_RUT_VALUE");

    public final static String CMB_DEFAULT_MESSAGE = (String)prop.get("CMB_DEFAULT_MESSAGE");

    public final static String TERMS_AND_CONDICTIONS_DEFAULT = (String)prop.get("TERMS_AND_CONDICTIONS_DEFAULT");
    public final static String TERMS_AND_CONDICTIONS_NEW = (String)prop.get("TERMS_AND_CONDICTIONS_NEW");

    public final static String SANTANDER_WAIT_MESSAGE = (String)prop.get("SANTANDER_WAIT_MESSAGE");
    public final static String TRANSBANK_WAIT_MESSAGE = (String)prop.get("TRANSBANK_WAIT_MESSAGE");

    public final static String EDIT_SHOPPING_CART_NOT_ALLOW_ACCESS_PAYMENT = (String)prop.get("EDIT_SHOPPING_CART_NOT_ALLOW_ACCESS_PAYMENT");

    public final static String GENERATE_CERTIFICATES_CREATED = (String)prop.get("GENERATE_CERTIFICATES_CREATED");
    public final static String GENERATE_CERTIFICATES_EMPTY = (String)prop.get("GENERATE_CERTIFICATES_EMPTY");
    public final static String GENERATE_CERTIFICATES_NO_PARAM = (String)prop.get("GENERATE_CERTIFICATES_NO_PARAM");
    
    public final static String EMAIL_MOBILEPHONE_REQUIRED = (String)prop.get("EMAIL_MOBILEPHONE_REQUIRED");
    public final static String NO_EXISTEN_CONTRATOS = (String)prop.get("NO_EXISTEN_CONTRATOS");
    public final static String CANCEL_SHOPPING_CAR = (String)prop.get("CANCEL_SHOPPING_CAR");

    private Messages() {
        super();
    }
}
