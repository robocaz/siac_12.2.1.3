package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalDevSolTrib database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalDevSolTrib.findAll", query="SELECT t FROM Tbl_CausalDevSolTrib t")
public class Tbl_CausalDevSolTrib implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_GlosaListaVerif")
	private String fld_GlosaListaVerif;

	@Column(name="Fld_MarcaVisada")
	private byte fld_MarcaVisada;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CausalDevSolTrib() {
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public String getFld_GlosaListaVerif() {
		return this.fld_GlosaListaVerif;
	}

	public void setFld_GlosaListaVerif(String fld_GlosaListaVerif) {
		this.fld_GlosaListaVerif = fld_GlosaListaVerif;
	}

	public byte getFld_MarcaVisada() {
		return this.fld_MarcaVisada;
	}

	public void setFld_MarcaVisada(byte fld_MarcaVisada) {
		this.fld_MarcaVisada = fld_MarcaVisada;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}