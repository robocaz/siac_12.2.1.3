package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DetectorFraudeDTO implements Serializable {

	private static final long serialVersionUID = 18382666870702170L;
	private String causal;
	private String fechaDigitacion;
	private EnListaNegra enListaNegra;
	private String rut;
	private Long corrCaja;

	public enum EnListaNegra {
    	SI("S"), NO("N");
    	
    	public String value;
    	
    	EnListaNegra(String value) {
    		this.value = value;
    	}
    }

	public String getCausal() {
		return causal;
	}

	public void setCausal(String causal) {
		this.causal = causal;
	}

	public String getFechaDigitacion() {
		return fechaDigitacion;
	}

	public void setFechaDigitacion(String fechaDigitacion) {
		this.fechaDigitacion = fechaDigitacion;
	}

	public EnListaNegra getEnListaNegra() {
		return enListaNegra;
	}

	public void setEnListaNegra(EnListaNegra enListaNegra) {
		this.enListaNegra = enListaNegra;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Long getCorrCaja() {
		return corrCaja;
	}

	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	
}
