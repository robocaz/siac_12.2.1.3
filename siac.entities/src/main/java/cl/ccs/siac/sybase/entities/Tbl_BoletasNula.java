package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BoletasNulas database table.
 * 
 */
@Entity
@Table(name="Tbl_BoletasNulas")
@NamedQuery(name="Tbl_BoletasNula.findAll", query="SELECT t FROM Tbl_BoletasNula t")
public class Tbl_BoletasNula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_FecAnulacion")
	private Timestamp fld_FecAnulacion;

	@Column(name="Fld_NroBoletaFactura")
	private int fld_NroBoletaFactura;

	public Tbl_BoletasNula() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public Timestamp getFld_FecAnulacion() {
		return this.fld_FecAnulacion;
	}

	public void setFld_FecAnulacion(Timestamp fld_FecAnulacion) {
		this.fld_FecAnulacion = fld_FecAnulacion;
	}

	public int getFld_NroBoletaFactura() {
		return this.fld_NroBoletaFactura;
	}

	public void setFld_NroBoletaFactura(int fld_NroBoletaFactura) {
		this.fld_NroBoletaFactura = fld_NroBoletaFactura;
	}

}