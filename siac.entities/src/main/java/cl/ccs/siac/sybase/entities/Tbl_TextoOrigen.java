package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_TextoOrigen database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TextoOrigen.findAll", query="SELECT t FROM Tbl_TextoOrigen t")
public class Tbl_TextoOrigen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrOrdenStr")
	private BigDecimal fld_CorrOrdenStr;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_TextoLinea1")
	private String fld_TextoLinea1;

	public Tbl_TextoOrigen() {
	}

	public BigDecimal getFld_CorrOrdenStr() {
		return this.fld_CorrOrdenStr;
	}

	public void setFld_CorrOrdenStr(BigDecimal fld_CorrOrdenStr) {
		this.fld_CorrOrdenStr = fld_CorrOrdenStr;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_TextoLinea1() {
		return this.fld_TextoLinea1;
	}

	public void setFld_TextoLinea1(String fld_TextoLinea1) {
		this.fld_TextoLinea1 = fld_TextoLinea1;
	}

}