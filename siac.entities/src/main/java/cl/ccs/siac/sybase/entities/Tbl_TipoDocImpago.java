package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoDocImpago database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_TipoDocImpago.findAll", query="SELECT t FROM Tbl_TipoDocImpago t")
public class Tbl_TipoDocImpago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_GlosaTipoDocumentoImpago")
	private String fld_GlosaTipoDocumentoImpago;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumentoImpago")
	private String fld_TipoDocumentoImpago;

	public Tbl_TipoDocImpago() {
	}

	public String getFld_GlosaTipoDocumentoImpago() {
		return this.fld_GlosaTipoDocumentoImpago;
	}

	public void setFld_GlosaTipoDocumentoImpago(String fld_GlosaTipoDocumentoImpago) {
		this.fld_GlosaTipoDocumentoImpago = fld_GlosaTipoDocumentoImpago;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumentoImpago() {
		return this.fld_TipoDocumentoImpago;
	}

	public void setFld_TipoDocumentoImpago(String fld_TipoDocumentoImpago) {
		this.fld_TipoDocumentoImpago = fld_TipoDocumentoImpago;
	}

}