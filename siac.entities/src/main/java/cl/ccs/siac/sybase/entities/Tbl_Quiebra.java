package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Quiebras database table.
 * 
 */
@Entity
@Table(name="Tbl_Quiebras")
@NamedQuery(name="Tbl_Quiebra.findAll", query="SELECT t FROM Tbl_Quiebra t")
public class Tbl_Quiebra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CausaQuiebra")
	private String fld_CausaQuiebra;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_CodTribunal")
	private String fld_CodTribunal;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_FecDeclaratoria")
	private Timestamp fld_FecDeclaratoria;

	@Column(name="Fld_FecPublicacionDiarioOfic")
	private Timestamp fld_FecPublicacionDiarioOfic;

	@Column(name="Fld_FecPublicacionFiscalia")
	private Timestamp fld_FecPublicacionFiscalia;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_GlosaSindico")
	private String fld_GlosaSindico;

	@Column(name="Fld_NroPublicacionDiarioOfic")
	private int fld_NroPublicacionDiarioOfic;

	@Column(name="Fld_RolQuiebra")
	private String fld_RolQuiebra;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_Quiebra() {
	}

	public String getFld_CausaQuiebra() {
		return this.fld_CausaQuiebra;
	}

	public void setFld_CausaQuiebra(String fld_CausaQuiebra) {
		this.fld_CausaQuiebra = fld_CausaQuiebra;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_CodTribunal() {
		return this.fld_CodTribunal;
	}

	public void setFld_CodTribunal(String fld_CodTribunal) {
		this.fld_CodTribunal = fld_CodTribunal;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public Timestamp getFld_FecDeclaratoria() {
		return this.fld_FecDeclaratoria;
	}

	public void setFld_FecDeclaratoria(Timestamp fld_FecDeclaratoria) {
		this.fld_FecDeclaratoria = fld_FecDeclaratoria;
	}

	public Timestamp getFld_FecPublicacionDiarioOfic() {
		return this.fld_FecPublicacionDiarioOfic;
	}

	public void setFld_FecPublicacionDiarioOfic(Timestamp fld_FecPublicacionDiarioOfic) {
		this.fld_FecPublicacionDiarioOfic = fld_FecPublicacionDiarioOfic;
	}

	public Timestamp getFld_FecPublicacionFiscalia() {
		return this.fld_FecPublicacionFiscalia;
	}

	public void setFld_FecPublicacionFiscalia(Timestamp fld_FecPublicacionFiscalia) {
		this.fld_FecPublicacionFiscalia = fld_FecPublicacionFiscalia;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public String getFld_GlosaSindico() {
		return this.fld_GlosaSindico;
	}

	public void setFld_GlosaSindico(String fld_GlosaSindico) {
		this.fld_GlosaSindico = fld_GlosaSindico;
	}

	public int getFld_NroPublicacionDiarioOfic() {
		return this.fld_NroPublicacionDiarioOfic;
	}

	public void setFld_NroPublicacionDiarioOfic(int fld_NroPublicacionDiarioOfic) {
		this.fld_NroPublicacionDiarioOfic = fld_NroPublicacionDiarioOfic;
	}

	public String getFld_RolQuiebra() {
		return this.fld_RolQuiebra;
	}

	public void setFld_RolQuiebra(String fld_RolQuiebra) {
		this.fld_RolQuiebra = fld_RolQuiebra;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}