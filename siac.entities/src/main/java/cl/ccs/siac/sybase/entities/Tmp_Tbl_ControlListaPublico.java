package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tmp_Tbl_ControlListaPublico database table.
 * 
 */
@Entity
@Table(name="tmp_Tbl_ControlListaPublico")
@NamedQuery(name="Tmp_Tbl_ControlListaPublico.findAll", query="SELECT t FROM Tmp_Tbl_ControlListaPublico t")
public class Tmp_Tbl_ControlListaPublico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCLP_Id")
	private BigDecimal fld_CorrCLP_Id;

	@Column(name="Fld_EstadoLP")
	private String fld_EstadoLP;

	@Column(name="Fld_FecCierre")
	private Timestamp fld_FecCierre;

	@Column(name="Fld_FecFin")
	private Timestamp fld_FecFin;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecPaginacion")
	private Timestamp fld_FecPaginacion;

	@Column(name="Fld_FecPublicacion")
	private Timestamp fld_FecPublicacion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroEnviosProt")
	private int fld_NroEnviosProt;

	@Column(name="Fld_NroImpresores")
	private short fld_NroImpresores;

	@Column(name="Fld_NroPaginas")
	private short fld_NroPaginas;

	@Column(name="Fld_NroProt")
	private int fld_NroProt;

	@Column(name="Fld_NroProtCH")
	private int fld_NroProtCH;

	@Column(name="Fld_NroProtCM")
	private int fld_NroProtCM;

	@Column(name="Fld_NroProtLT")
	private int fld_NroProtLT;

	@Column(name="Fld_NroProtPG")
	private int fld_NroProtPG;

	@Column(name="Fld_NroProtQI")
	private int fld_NroProtQI;

	@Column(name="Fld_NroProtRA")
	private int fld_NroProtRA;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoGeneracion")
	private String fld_TipoGeneracion;

	@Column(name="Fld_TotFilas")
	private short fld_TotFilas;

	@Column(name="Fld_TotRegistrosLP")
	private int fld_TotRegistrosLP;

	@Column(name="Fld_UsoLP")
	private String fld_UsoLP;

	public Tmp_Tbl_ControlListaPublico() {
	}

	public BigDecimal getFld_CorrCLP_Id() {
		return this.fld_CorrCLP_Id;
	}

	public void setFld_CorrCLP_Id(BigDecimal fld_CorrCLP_Id) {
		this.fld_CorrCLP_Id = fld_CorrCLP_Id;
	}

	public String getFld_EstadoLP() {
		return this.fld_EstadoLP;
	}

	public void setFld_EstadoLP(String fld_EstadoLP) {
		this.fld_EstadoLP = fld_EstadoLP;
	}

	public Timestamp getFld_FecCierre() {
		return this.fld_FecCierre;
	}

	public void setFld_FecCierre(Timestamp fld_FecCierre) {
		this.fld_FecCierre = fld_FecCierre;
	}

	public Timestamp getFld_FecFin() {
		return this.fld_FecFin;
	}

	public void setFld_FecFin(Timestamp fld_FecFin) {
		this.fld_FecFin = fld_FecFin;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecPaginacion() {
		return this.fld_FecPaginacion;
	}

	public void setFld_FecPaginacion(Timestamp fld_FecPaginacion) {
		this.fld_FecPaginacion = fld_FecPaginacion;
	}

	public Timestamp getFld_FecPublicacion() {
		return this.fld_FecPublicacion;
	}

	public void setFld_FecPublicacion(Timestamp fld_FecPublicacion) {
		this.fld_FecPublicacion = fld_FecPublicacion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public int getFld_NroEnviosProt() {
		return this.fld_NroEnviosProt;
	}

	public void setFld_NroEnviosProt(int fld_NroEnviosProt) {
		this.fld_NroEnviosProt = fld_NroEnviosProt;
	}

	public short getFld_NroImpresores() {
		return this.fld_NroImpresores;
	}

	public void setFld_NroImpresores(short fld_NroImpresores) {
		this.fld_NroImpresores = fld_NroImpresores;
	}

	public short getFld_NroPaginas() {
		return this.fld_NroPaginas;
	}

	public void setFld_NroPaginas(short fld_NroPaginas) {
		this.fld_NroPaginas = fld_NroPaginas;
	}

	public int getFld_NroProt() {
		return this.fld_NroProt;
	}

	public void setFld_NroProt(int fld_NroProt) {
		this.fld_NroProt = fld_NroProt;
	}

	public int getFld_NroProtCH() {
		return this.fld_NroProtCH;
	}

	public void setFld_NroProtCH(int fld_NroProtCH) {
		this.fld_NroProtCH = fld_NroProtCH;
	}

	public int getFld_NroProtCM() {
		return this.fld_NroProtCM;
	}

	public void setFld_NroProtCM(int fld_NroProtCM) {
		this.fld_NroProtCM = fld_NroProtCM;
	}

	public int getFld_NroProtLT() {
		return this.fld_NroProtLT;
	}

	public void setFld_NroProtLT(int fld_NroProtLT) {
		this.fld_NroProtLT = fld_NroProtLT;
	}

	public int getFld_NroProtPG() {
		return this.fld_NroProtPG;
	}

	public void setFld_NroProtPG(int fld_NroProtPG) {
		this.fld_NroProtPG = fld_NroProtPG;
	}

	public int getFld_NroProtQI() {
		return this.fld_NroProtQI;
	}

	public void setFld_NroProtQI(int fld_NroProtQI) {
		this.fld_NroProtQI = fld_NroProtQI;
	}

	public int getFld_NroProtRA() {
		return this.fld_NroProtRA;
	}

	public void setFld_NroProtRA(int fld_NroProtRA) {
		this.fld_NroProtRA = fld_NroProtRA;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoGeneracion() {
		return this.fld_TipoGeneracion;
	}

	public void setFld_TipoGeneracion(String fld_TipoGeneracion) {
		this.fld_TipoGeneracion = fld_TipoGeneracion;
	}

	public short getFld_TotFilas() {
		return this.fld_TotFilas;
	}

	public void setFld_TotFilas(short fld_TotFilas) {
		this.fld_TotFilas = fld_TotFilas;
	}

	public int getFld_TotRegistrosLP() {
		return this.fld_TotRegistrosLP;
	}

	public void setFld_TotRegistrosLP(int fld_TotRegistrosLP) {
		this.fld_TotRegistrosLP = fld_TotRegistrosLP;
	}

	public String getFld_UsoLP() {
		return this.fld_UsoLP;
	}

	public void setFld_UsoLP(String fld_UsoLP) {
		this.fld_UsoLP = fld_UsoLP;
	}

}