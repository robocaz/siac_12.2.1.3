package cl.ccs.siac.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ComboDTO;
import cl.exe.ccs.dto.ImpresoraDTO;
import cl.exe.ccs.dto.MonedaDTO;
import cl.exe.ccs.dto.ServicioDTO;
import cl.exe.ccs.dto.SucursalDTO;
import cl.exe.ccs.dto.TarifaDTO;

@Named("combosDao")
@Stateless
public class CombosDaoImpl implements CombosDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(CombosDaoImpl.class);

	public List<ComboDTO> obtenerRegiones() {

		List<ComboDTO> regionesCombo = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbRegiones_SW2");
     
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbRegiones_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaRegion", String.class, ParameterMode.OUT);

			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO region = new ComboDTO();
				region.setCodigo(String.valueOf(obj[0]));
				region.setDescripcion(String.valueOf(obj[1]));
				regionesCombo.add(region);
			}

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbRegiones_SW2 " + e.getMessage());
			return null;
		}
		return regionesCombo;
	}

	public List<ComboDTO> obtenerComunas(Integer idRegion) {

		List<ComboDTO> comunasCombo = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbComunasxRegion_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbComunasxRegion_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodRegion", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodComuna", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaRegion", String.class, ParameterMode.OUT);
			
			log.info("Fld_CodRegion " + idRegion);
			
			storedProcedure.setParameter("Fld_CodRegion", idRegion);

			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO comunas = new ComboDTO();
				comunas.setCodigo(String.valueOf(obj[0]));
				comunas.setDescripcion(String.valueOf(obj[1]));
				comunasCombo.add(comunas);
			}
			return comunasCombo;
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_CmbComunasxRegion_SW2 " + e.getMessage());
			return null;
		}
		

	}

	public List<ServicioDTO> obtenerServicios() {

		List<ServicioDTO> serviciosCombo = new ArrayList<ServicioDTO>();
		try {
			
			log.info("Ejecutando SQ_Servicios_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_Servicios_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaServicio", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Tipo", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodExterno", Integer.class, ParameterMode.OUT);
			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ServicioDTO servicio = new ServicioDTO();
				servicio.setCodServicio(obj[0].toString());
				servicio.setGlosaServicio(obj[1].toString());
				servicio.setTipoServicio(Integer.parseInt(obj[2].toString()));
				servicio.setCodExterno(Integer.parseInt(obj[3].toString()));
				serviciosCombo.add(servicio);

			}

		} catch (Exception e) {
			log.error("Error en el SP SQ_Servicios_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return serviciosCombo;

	}
	
	public List<TarifaDTO> obtenerTarifas() {

		List<TarifaDTO> tarifasCombo = new ArrayList<TarifaDTO>();

		try {
			
			log.info("Ejecutando SQ_ServicioTarifa_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ServicioTarifa_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaServicio", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CostoBase", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecTermino", Date.class, ParameterMode.OUT);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				TarifaDTO tarifa = new TarifaDTO();
				tarifa.setCodServicio(obj[0].toString());
				tarifa.setGlosaServicio(obj[1].toString());
				tarifa.setCostoBase(Double.parseDouble(obj[2].toString()));
				tarifasCombo.add(tarifa);
			}

			return tarifasCombo;

		} catch (Exception e) {
			log.error("Error en el SP SQ_ServicioTarifa_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ComboDTO> obtenerObservaciones() {
		List<ComboDTO> obervacionCombo = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbObservacion_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbObservacion_SW2");

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO observacion = new ComboDTO();
				observacion.setCodigo(String.valueOf(obj[0]));
				observacion.setDescripcion(String.valueOf(obj[1]));
				obervacionCombo.add(observacion);
			}

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbObservacion_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return obervacionCombo;
	}

	public List<ComboDTO> obtenerEmisores(String codigoTipoEmisor) {

		List<ComboDTO> emisores = new ArrayList<ComboDTO>();
		try {
			

			log.info("Ejecutando SQ_CmbEmisores_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbEmisores_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaEmisor", String.class, ParameterMode.OUT);
			
			log.info("Fld_TipoEmisor " + codigoTipoEmisor);
			
			storedProcedure.setParameter("Fld_TipoEmisor", codigoTipoEmisor);

			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO emisor = new ComboDTO();
				emisor.setCodigo(String.valueOf(obj[0]).toString());
				emisor.setDescripcion(String.valueOf(obj[1]).trim());
				emisores.add(emisor);
			}
			return emisores;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbEmisores_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<SucursalDTO> obtenerSucursales(ComboDTO banco) {

		List<SucursalDTO> sucursales = new ArrayList<SucursalDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbSucursales_Banco_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbSucursales_Banco_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodSucursal", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaSucursal", String.class, ParameterMode.OUT);

			log.info("Fld_CodEmisor " + banco.getCodigo());
			
			storedProcedure.setParameter("Fld_CodEmisor", banco.getCodigo());
			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				SucursalDTO sucursal = new SucursalDTO();
				sucursal.setCodEmisor(String.valueOf(obj[1]));
				sucursal.setTipoEmisor(String.valueOf(obj[0]));
				sucursal.setCodSucursal(Integer.parseInt(String.valueOf(obj[2])));
				sucursal.setGlosaSucursal(String.valueOf(obj[3]));
				sucursales.add(sucursal);
			}
			return sucursales;

		} catch (Exception e) {

			log.error("Error en el SP SQ_CmbSucursales_Banco_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ImpresoraDTO> obtenerImpresoras(Integer centroCosto) {

		List<ImpresoraDTO> impresoras = new ArrayList<ImpresoraDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbImpresoraBol_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbImpresoraBol_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_IdImpresora", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Tbl_ImpresoraBoleta", String.class, ParameterMode.OUT);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ImpresoraDTO impresora = new ImpresoraDTO();
				impresora.setCodCCosto(centroCosto);
				impresora.setIdImpresora(String.valueOf(obj[0]));
				impresora.setModeloImpresora(String.valueOf(obj[1]));

				impresoras.add(impresora);
			}
			return impresoras;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbImpresoraBol_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> obtenerTiposEmisores() {

		List<ComboDTO> emisores = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbTipoEmisor_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbTipoEmisor_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoEmisor", String.class, ParameterMode.OUT);

			
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO tipoEmisor = new ComboDTO();
				tipoEmisor.setCodigo(String.valueOf(obj[0]).toString());
				tipoEmisor.setDescripcion(String.valueOf(obj[1]).trim());
				emisores.add(tipoEmisor);
			}
			return emisores;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbTipoEmisor_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> obtenerDocsAclaracion(String tipoDoc) {

		List<ComboDTO> emisores = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbTipoDocAcl_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbTipoDocAcl_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocAclaracion", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoDocAclaracion", String.class,ParameterMode.OUT);

			log.info("Fld_TipoDocumento", tipoDoc);
			
			storedProcedure.setParameter("Fld_TipoDocumento", tipoDoc);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO docs = new ComboDTO();
				docs.setCodigo(String.valueOf(obj[0]).toString().trim());
				docs.setDescripcion(String.valueOf(obj[1]).trim());
				emisores.add(docs);
			}
			return emisores;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbTipoDocAcl_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> tiposDocumentos() {

		List<ComboDTO> documentos = new ArrayList<ComboDTO>();
		try {

			log.info("Ejecutando SQ_CmbTipoDocumento_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbTipoDocumento_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoDocumento", String.class, ParameterMode.OUT);
			
			
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO docs = new ComboDTO();
				docs.setCodigo(String.valueOf(obj[0]).toString().trim());
				docs.setDescripcion(String.valueOf(obj[1]).trim());
				documentos.add(docs);
			}
			return documentos;

		} catch (Exception e) {
			
			log.error("Error en el SP SQ_CmbTipoDocumento_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<SucursalDTO> obtenerSucursalesEmisor(String tipoEmisor, String codEmisor) {

		List<SucursalDTO> sucursales = new ArrayList<SucursalDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbSucursales_Emisor_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbSucursales_Emisor_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);

			log.info("Fld_TipoEmisor " + tipoEmisor);
			log.info("Fld_CodEmisor " + codEmisor);
			
			storedProcedure.setParameter("Fld_TipoEmisor", tipoEmisor);
			storedProcedure.setParameter("Fld_CodEmisor", codEmisor);
			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				SucursalDTO sucursal = new SucursalDTO();
				sucursal.setCodSucursal(Integer.parseInt(String.valueOf(obj[0])));
				sucursal.setGlosaSucursal(String.valueOf(obj[1]));
				sucursales.add(sucursal);
			}
			return sucursales;

		} catch (Exception e) {
			
			log.error("Error en el SP SQ_CmbSucursales_Emisor_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<MonedaDTO> obtenerMonedas() {

		List<MonedaDTO> monedas = new ArrayList<MonedaDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbMonedas_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbMonedas_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodMoneda", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaMoneda", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaCorta", String.class, ParameterMode.OUT);

			// execute SP
			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				MonedaDTO moneda = new MonedaDTO();
				moneda.setCodMoneda(Integer.parseInt(String.valueOf(obj[0])));
				moneda.setGlosaMoneda(String.valueOf(obj[1]));
				moneda.setGlosaCorta(String.valueOf(obj[2]));
				monedas.add(moneda);
			}
			return monedas;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbMonedas_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> obtenerTiposEmisoresPorDoc(String tipoDocumento) {

		List<ComboDTO> emisores = new ArrayList<ComboDTO>();
		try {
			
			log.info("Ejecutando SQ_CmbTipoEmisor_TipoDoc_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbTipoEmisor_TipoDoc_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoEmisor", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoEmisor", String.class, ParameterMode.OUT);

			log.info("Fld_TipoDocumento " + tipoDocumento);
			
			storedProcedure.setParameter("Fld_TipoDocumento", tipoDocumento);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO tipoEmisor = new ComboDTO();
				tipoEmisor.setCodigo(String.valueOf(obj[0]).toString());
				tipoEmisor.setDescripcion(String.valueOf(obj[1]).trim());
				emisores.add(tipoEmisor);
			}
			return emisores;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbTipoEmisor_TipoDoc_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> obtenerMotivosDeclaracionUso() {

		List<ComboDTO> motivos = new ArrayList<ComboDTO>();
		try {
			

			log.info("Ejecutando SQ_CmbMotivo_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CmbMotivo_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CodMotivo", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaMotivo", String.class, ParameterMode.OUT);

			List<Object[]> rs = storedProcedure.getResultList();
			for (Object[] obj : rs) {
				ComboDTO motivoDeclaracionUso = new ComboDTO();
				motivoDeclaracionUso.setCodigo(String.valueOf(obj[0]).toString());
				motivoDeclaracionUso.setDescripcion(String.valueOf(obj[1]).trim());
				motivos.add(motivoDeclaracionUso);
			}
			return motivos;

		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbMotivo_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<ComboDTO> cmbDocPresentado() {

		List<ComboDTO> combo = new ArrayList<ComboDTO>();

		try {
			log.info("Ejecutando SQ_CmbDocPreseFnq_SW2");
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_CmbDocPreseFnq_SW2");

			List<Object[]> result = spq.getResultList();

			for (Object[] obj : result) {
				ComboDTO opt = new ComboDTO();
				opt.setCodigo(String.valueOf(obj[0]));
				opt.setDescripcion(String.valueOf(obj[1]).trim());
				combo.add(opt);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en el SP SQ_CmbDocPreseFnq_SW2 " + e.getMessage());
		}
		return combo;
	}

	public List<ComboDTO> cmbBancos() {

		List<ComboDTO> combo = new ArrayList<ComboDTO>();

		try {
			log.info("Ejecutando SQ_BancosCamara_SW2");
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_BancosCamara_SW2");

			List<Object[]> result = spq.getResultList();

			for (Object[] obj : result) {
				ComboDTO opt = new ComboDTO();
				opt.setCodigo(String.valueOf(obj[0]).trim());
				opt.setDescripcion(String.valueOf(obj[1]).trim());
				combo.add(opt);
			}

		} catch (Exception e) {
			log.error("Error en el SP SQ_BancosCamara_SW2 " + e.getMessage());
		}
		return combo;
	}

	public List<ComboDTO> cmbCtasCtes(String codEmisor) {

		List<ComboDTO> combo = new ArrayList<ComboDTO>();

		try {
			log.info("Ejecutando SQ_CtasCtesDepo_SW2");
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_CtasCtesDepo_SW2");
			spq.registerStoredProcedureParameter("Fld_CodEmisor", String.class, ParameterMode.IN);

			log.info("Fld_CodEmisor " + codEmisor);
			spq.setParameter("Fld_CodEmisor", codEmisor);
			
			List<String> result = spq.getResultList();
			
			for ( String obj : result ) {
				ComboDTO opt = new ComboDTO();
				opt.setCodigo(obj.trim());
				opt.setDescripcion(opt.getCodigo());
				combo.add(opt);
			}

		} catch (Exception e) {
			log.error("Error en el SP SQ_CtasCtesDepo_SW2 " + e.getMessage());
		}
		return combo;
	}

	@Override
	public List<ComboDTO> cmbTipoSolicRecl() {
		List<ComboDTO> combo = new ArrayList<ComboDTO>();
		
		try {
			
			log.info("Ejecutando SQ_CmbTipoSolicRecl_SW2");
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_CmbTipoSolicRecl_SW2");
			
			List<Object[]> result = spq.getResultList();
			
			for ( Object[] obj : result ) {
				ComboDTO opt = new ComboDTO();
				opt.setCodigo(String.valueOf(obj[0]));
				opt.setDescripcion(String.valueOf(obj[1]).trim());
				combo.add(opt);
			}
			
		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbTipoSolicRecl_SW2 " + e.getMessage());
		}
		
		return combo;
	}

	@Override
	public List<ComboDTO> cmbCategoriaSolic(Integer idSolic) {
		List<ComboDTO> combo = new ArrayList<ComboDTO>();
		
		try {
			
			log.info("Ejecutando SQ_CmbCategoriaSolicitud_SW2");
			
			StoredProcedureQuery spq = em.createStoredProcedureQuery("SQ_CmbCategoriaSolicitud_SW2");
			spq.registerStoredProcedureParameter("Fld_TipoSolicitud", Integer.class, ParameterMode.IN);
			
			log.info("Fld_TipoSolicitud " + idSolic);
			
			spq.setParameter("Fld_TipoSolicitud", idSolic);
			
			List<Object[]> result = spq.getResultList();
			
			for ( Object[] obj : result ) {
				ComboDTO opt = new ComboDTO();
				opt.setCodigo(String.valueOf(obj[0]));
				opt.setDescripcion(String.valueOf(obj[1]).trim());
				combo.add(opt);
			}
			
		} catch (Exception e) {
			log.error("Error en el SP SQ_CmbCategoriaSolicitud_SW2 " + e.getMessage());
		}
		
		return combo;
	}

}
