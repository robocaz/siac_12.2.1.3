package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SolDetConfEnviados database table.
 * 
 */
@Entity
@Table(name="Tbl_SolDetConfEnviados")
@NamedQuery(name="Tbl_SolDetConfEnviado.findAll", query="SELECT t FROM Tbl_SolDetConfEnviado t")
public class Tbl_SolDetConfEnviado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrContrato")
	private BigDecimal fld_CorrContrato;

	@Column(name="Fld_CorrMail")
	private BigDecimal fld_CorrMail;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	public Tbl_SolDetConfEnviado() {
	}

	public BigDecimal getFld_CorrContrato() {
		return this.fld_CorrContrato;
	}

	public void setFld_CorrContrato(BigDecimal fld_CorrContrato) {
		this.fld_CorrContrato = fld_CorrContrato;
	}

	public BigDecimal getFld_CorrMail() {
		return this.fld_CorrMail;
	}

	public void setFld_CorrMail(BigDecimal fld_CorrMail) {
		this.fld_CorrMail = fld_CorrMail;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

}