package cl.ccs.siac.arqueo;

import cl.exe.ccs.dto.ArqueoCajaInDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasBolFactEmiOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasConDctoOutDTO;
import cl.exe.ccs.dto.ArqueoCajaVtasTipoPagoOutDTO;

public interface ArqueoCajaBusiness {

	ArqueoCajaVtasTipoPagoOutDTO vtasTipoPago(ArqueoCajaInDTO params);
	
	ArqueoCajaVtasConDctoOutDTO vtasConDcto(ArqueoCajaInDTO params);
	
	ArqueoCajaVtasBolFactEmiOutDTO vtasBolFactEmi(ArqueoCajaInDTO params);

}
