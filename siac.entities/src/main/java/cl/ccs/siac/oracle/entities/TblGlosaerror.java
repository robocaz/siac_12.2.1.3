package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_GLOSAERROR database table.
 * 
 */
@Entity
@Table(name="TBL_GLOSAERROR")
@NamedQuery(name="TblGlosaerror.findAll", query="SELECT t FROM TblGlosaerror t")
public class TblGlosaerror implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TblGlosaerrorPK id;

	@Column(name="FLD_DESCRIPCION")
	private String fldDescripcion;

	@Column(name="FLD_GLOSA")
	private String fldGlosa;

	public TblGlosaerror() {
	}

	public TblGlosaerrorPK getId() {
		return this.id;
	}

	public void setId(TblGlosaerrorPK id) {
		this.id = id;
	}

	public String getFldDescripcion() {
		return this.fldDescripcion;
	}

	public void setFldDescripcion(String fldDescripcion) {
		this.fldDescripcion = fldDescripcion;
	}

	public String getFldGlosa() {
		return this.fldGlosa;
	}

	public void setFldGlosa(String fldGlosa) {
		this.fldGlosa = fldGlosa;
	}

}