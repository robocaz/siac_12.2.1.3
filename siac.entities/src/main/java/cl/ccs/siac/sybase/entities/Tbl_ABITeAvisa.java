package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ABITeAvisa database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ABITeAvisa.findAll", query="SELECT t FROM Tbl_ABITeAvisa t")
public class Tbl_ABITeAvisa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CantBC")
	private short fld_CantBC;

	@Column(name="Fld_CantMC")
	private short fld_CantMC;

	@Column(name="Fld_CorrContrato")
	private BigDecimal fld_CorrContrato;

	@Column(name="Fld_CorrMail_Id")
	private BigDecimal fld_CorrMail_Id;

	@Column(name="Fld_FecEnvio")
	private Timestamp fld_FecEnvio;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_TipoMail")
	private byte fld_TipoMail;

	@Column(name="Fld_TotDetalle")
	private short fld_TotDetalle;

	public Tbl_ABITeAvisa() {
	}

	public short getFld_CantBC() {
		return this.fld_CantBC;
	}

	public void setFld_CantBC(short fld_CantBC) {
		this.fld_CantBC = fld_CantBC;
	}

	public short getFld_CantMC() {
		return this.fld_CantMC;
	}

	public void setFld_CantMC(short fld_CantMC) {
		this.fld_CantMC = fld_CantMC;
	}

	public BigDecimal getFld_CorrContrato() {
		return this.fld_CorrContrato;
	}

	public void setFld_CorrContrato(BigDecimal fld_CorrContrato) {
		this.fld_CorrContrato = fld_CorrContrato;
	}

	public BigDecimal getFld_CorrMail_Id() {
		return this.fld_CorrMail_Id;
	}

	public void setFld_CorrMail_Id(BigDecimal fld_CorrMail_Id) {
		this.fld_CorrMail_Id = fld_CorrMail_Id;
	}

	public Timestamp getFld_FecEnvio() {
		return this.fld_FecEnvio;
	}

	public void setFld_FecEnvio(Timestamp fld_FecEnvio) {
		this.fld_FecEnvio = fld_FecEnvio;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte getFld_TipoMail() {
		return this.fld_TipoMail;
	}

	public void setFld_TipoMail(byte fld_TipoMail) {
		this.fld_TipoMail = fld_TipoMail;
	}

	public short getFld_TotDetalle() {
		return this.fld_TotDetalle;
	}

	public void setFld_TotDetalle(short fld_TotDetalle) {
		this.fld_TotDetalle = fld_TotDetalle;
	}

}