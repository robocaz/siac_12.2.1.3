package cl.ccs.siac.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.servicios.PagoServicio;
import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.MorosidadVigenteDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ProtestoVigenteDTO;
import cl.exe.ccs.dto.UsuarioDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoICTPaginacionDTO;
import cl.exe.ccs.dto.certificados.CertificadoICTbcDTO;
import cl.exe.ccs.dto.certificados.CertificadoICTmcDTO;
import cl.exe.ccs.dto.certificados.CertificadoISAPaginacionDTO;
import cl.exe.ccs.dto.certificados.CertificadoISAbcDTO;
import cl.exe.ccs.dto.certificados.CertificadoISAmcDTO;

public class CertificadosUtil {

	public static String newline = System.getProperty("line.separator");
	private static final Logger LOG = LogManager.getLogger(CertificadosUtil.class);

	public static Map<String, Object> getParametrosICT(ConsultaBICDTO consultaBIC, ConsultaMOLDTO consultaMOL,
			UsuarioDTO usuario, Integer maxPorPagina) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		try{
		
		Date hoyDia = new Date();
		List<CertificadoICTbcDTO> coleccionBC = CertificadosUtil.getColeccionICTbc(consultaBIC);
		List<CertificadoICTbcDTO> coleccionBCACL = CertificadosUtil.getColeccionICTbcAcl(consultaBIC);
		List<CertificadoICTmcDTO> coleccionMC = CertificadosUtil.getColeccionICTmc(consultaMOL);

		parametros
				.put("titulo",
						(CertificadosUtil.getTipoPersonaRut(
								consultaBIC.getRut()).compareTo("N") == 0 ? "CERTIFICADO PARA FINES ESPECIALES"
										: "CERTIFICADO")
								+ CertificadosUtil.newline
								+ "INCLUYE OBLIGACIONES VENCIDAS, NO PAGADAS Y EN TRÁMITE DE ACLARACIÓN"
								+ CertificadosUtil.newline
								+ "EN BOLETÍN COMERCIAL Y BASE DE DATOS DE MOROSIDAD DE LOS SISTEMAS FINANCIERO/COMERCIAL");
		parametros.put("rut", CertificadosUtil.getRut(consultaBIC.getRut()));
		parametros.put("tipoPersona", CertificadosUtil.getTipoPersonaRut(consultaBIC.getRut()));
		parametros.put("nombre", CertificadosUtil.getNombre(consultaBIC.getRut(), consultaBIC.getNombres(),
				consultaBIC.getApPat(), consultaBIC.getApMat()));
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));

		parametros.put("totalPublicaciones",
				Integer.toString(consultaBIC.getProtVigentes().size() + consultaMOL.getListMorosidadVig().size()));
		parametros.put("totalBoletin", Integer.toString(consultaBIC.getProtVigentes().size()));
		parametros.put("totalMorosidad", Integer.toString(consultaMOL.getListMorosidadVig().size()));
		parametros.put("totalTramiteAclaracion", Integer.toString(coleccionBCACL.size()));

		// cheques, letras, cuotas, rentas
		Integer bcCheques = 0, bcLetras = 0, bcCuotas = 0, bcRentas = 0;
		for (Integer i = 0; i < consultaBIC.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consultaBIC.getProtVigentes().get(i);

			if (protesto.getTipoDocumento().compareTo("CH") == 0) {
				++bcCheques;
			}
			if (protesto.getTipoDocumento().compareTo("LT") == 0 || protesto.getTipoDocumento().compareTo("PG") == 0) {
				++bcLetras;
			}
			if (protesto.getTipoDocumento().compareTo("CM") == 0) {
				++bcCuotas;
			}
			if (protesto.getTipoDocumento().compareTo("RA") == 0) {
				++bcRentas;
			}
		}
		// bc_cheques, bc_letras, bc_cuotas, bc_rentas
		parametros.put("bc_cheques", bcCheques.toString());
		parametros.put("bc_letras", bcLetras.toString());
		parametros.put("bc_cuotas", bcCuotas.toString());
		parametros.put("bc_rentas", bcRentas.toString());

		// tarjetas, letras, pagares, otras
		Integer mcTarjetas = 0, mcLetras = 0, mcPagares = 0, mcOtras = 0;
		for (Integer i = 0; i < coleccionMC.size(); i++) {
			CertificadoICTmcDTO morosidad = coleccionMC.get(i);

			if (morosidad.getTipoCredito().compareTo("TC") == 0) {
				++mcTarjetas;
			} else if (morosidad.getTipoCredito().compareTo("LT") == 0) {
				++mcLetras;
			} else if (morosidad.getTipoCredito().compareTo("PG") == 0) {
				++mcPagares;
			} else {
				++mcOtras;
			}
		}
		// mc_tarjetas, mc_letras, mc_pagares, mc_otras
		parametros.put("mc_tarjetas", mcTarjetas.toString());
		parametros.put("mc_letras", mcLetras.toString());
		parametros.put("mc_pagares", mcPagares.toString());
		parametros.put("mc_otras", mcOtras.toString());

		parametros.put("usuario", usuario.getUsername().trim());
		parametros.put("sucursal", usuario.getCentroCostoActivo().getDescripcion().trim());
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));
		parametros.put("codigoVerificacion",
				consultaBIC.getCodAutenticBic().trim() + " - " + consultaMOL.getCodAutenticMol().trim());

		// paginacion
		// paginacion
		List<CertificadoICTPaginacionDTO> coleccionPAG = new ArrayList<CertificadoICTPaginacionDTO>();
		Integer total = new Integer(coleccionBC.size() + coleccionBCACL.size() + coleccionMC.size());
		Integer total_bc = coleccionBC.size();
		Integer total_acl = coleccionBCACL.size();
		Integer total_mc = coleccionMC.size();
		Integer maxBC = 26;
		Integer maxMC = 23;
		Integer maxBcMc = 15;
		Integer paginas = 1; // (int)Math.ceil(total.doubleValue() /
								// maxPorPagina);
		
		// bc
		Integer bc_inicio = 1;
		Integer bc_termino = bc_inicio + CertificadosUtil.getPaginas(total_bc, maxBC, 0);
		Integer bc_resto = (bc_termino * maxBC) - total_bc;
		// acl
		Integer acl_inicio = bc_resto == 0 && total_acl > 0 ? bc_termino + 1 : bc_termino;
		Integer acl_termino = acl_inicio + CertificadosUtil.getPaginas(total_acl, maxBC, bc_resto);
		Integer acl_resto = (acl_termino * maxBC) - (total_bc + total_acl);
		//acl_resto = acl_resto <= (maxBC - maxBcMc) ? 0 : acl_resto - (maxBC - maxBcMc);
		// mc
		Integer mc_inicio=0;
        if(acl_resto==1 || acl_resto==2)  
        	mc_inicio = acl_termino+1;
        else
        	mc_inicio = acl_resto<11 ? acl_termino + 1 : acl_termino;
        Integer mc_termino=0;
        if(mc_inicio==acl_termino)
        	mc_termino = mc_inicio + CertificadosUtil.getPaginas(total_mc, maxMC, acl_resto);
        else 
        	mc_termino = acl_termino + CertificadosUtil.getPaginas(total_mc, maxMC, acl_resto);
        Integer mc_resto = (mc_termino * maxMC) - total_mc;
        mc_resto = mc_resto <= (maxBC - maxBcMc) ? 0 : mc_resto - (maxBC - maxBcMc);
        if (total_mc>0)
        	mc_termino = ((mc_inicio==acl_termino) && (mc_inicio==mc_termino) && (((maxBC-acl_resto)+total_mc)>maxBcMc)) ? mc_termino + 1 : mc_termino;
        // paginas
        if(mc_termino<mc_inicio) mc_termino=mc_inicio;
        paginas = mc_termino;
		// elaboracion lista de paginas y bloques
		int sum_total_bc=total_bc;
		int sum_total_acl=total_acl;
		int sum_total_mc=total_mc;
		for (int p = 0; p < paginas; p++) {
		    int pag = p + 1;
		    int resto = 0;
		
		    CertificadoICTPaginacionDTO paginacion = new CertificadoICTPaginacionDTO();
		    paginacion.setPagina(pag);
		
		    // bc
		
			paginacion.setBc(bc_inicio <= pag && pag <= bc_termino);
			if (paginacion.isBc()) {
			    if (sum_total_bc > 0) {
					paginacion.setBc_inicio(pag == bc_inicio ? 0 : coleccionPAG.get(p - 1).getBc_termino());
					int diff_bc = total_bc - paginacion.getBc_inicio();
					diff_bc = diff_bc > maxBC ? maxBC : diff_bc;
					resto = diff_bc < maxBC ? maxBC - diff_bc : 0;
					paginacion.setBc_termino(paginacion.getBc_inicio() + diff_bc);
					if (paginacion.getBc_inicio() == paginacion.getBc_termino()) {
					    paginacion.getBc_coleccion().add(coleccionBC.get(paginacion.getBc_inicio()));
					    sum_total_bc=0;
					} else {
					    paginacion.setBc_coleccion(coleccionBC.subList(paginacion.getBc_inicio(), paginacion.getBc_termino()));
					    sum_total_bc=total_bc-paginacion.getBc_termino();
					}
			    }
			}
		
			// acl
			paginacion.setAcl(acl_inicio <= pag && pag <= acl_termino);
			if (paginacion.isAcl()) {
				if (sum_total_acl > 0) {
					paginacion.setAcl_inicio(pag == acl_inicio ? 0 : coleccionPAG.get(p - 1).getAcl_termino());
					int diff_acl = total_acl - paginacion.getAcl_inicio();
					diff_acl = diff_acl > resto && resto > 0 ? resto : (diff_acl > maxBC ? maxBC : diff_acl);
					resto = resto > 0 ? resto - diff_acl : maxBC - diff_acl;
					/*if(pag==bc_termino && bc_inicio==bc_termino){
						diff_acl = diff_acl > maxBc - paginacion.getBc_termino() ? maxBc - (paginacion.getBc_termino() - paginacion.getBc_inicio()) : diff_acl;
						resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
					}else{
						resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
						diff_acl = diff_acl > resto && resto > 0 ? resto : (diff_acl > maxBC ? maxBC : diff_acl);
					}*/
					paginacion.setAcl_termino(paginacion.getAcl_inicio() + diff_acl);
					if (paginacion.getAcl_inicio() == paginacion.getAcl_termino()) {
						paginacion.getAcl_coleccion().add(coleccionBCACL.get(paginacion.getAcl_inicio()));
						sum_total_acl=0;
					} else {
						paginacion.setAcl_coleccion(coleccionBCACL.subList(paginacion.getAcl_inicio(), paginacion.getAcl_termino()));
						if(pag==paginas && paginacion.getAcl_termino()<total_acl){
					    	paginas+=1;
					    	acl_termino+=1;
					    }
					}
				}
			}
		
			// mc
		    paginacion.setMc(mc_inicio <= pag && pag <= mc_termino);
		    if (paginacion.isMc()) {
				if (sum_total_mc > 0) {
					paginacion.setMc_inicio(pag == mc_inicio ? 0 : coleccionPAG.get(p - 1).getMc_termino());
					int diff_mc = total_mc - paginacion.getMc_inicio();
					if(pag==acl_termino && bc_inicio==acl_termino){
						diff_mc = diff_mc > maxBcMc - (paginacion.getBc_termino()+paginacion.getAcl_termino()) ? maxBcMc - ((paginacion.getBc_termino() - paginacion.getBc_inicio())+(paginacion.getAcl_termino() - paginacion.getAcl_inicio())) : diff_mc;
						resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
					}else{
						resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
						diff_mc = diff_mc > resto && resto > 0 ? resto : (diff_mc > maxMC ? maxMC : diff_mc);
					}
					    paginacion.setMc_termino(paginacion.getMc_inicio() + diff_mc);
					if (paginacion.getMc_inicio() == paginacion.getMc_termino()) {
						paginacion.getMc_coleccion().add(coleccionMC.get(paginacion.getMc_inicio()));
						sum_total_mc=0;
					} else {
					    paginacion.setMc_coleccion(coleccionMC.subList(paginacion.getMc_inicio(), paginacion.getMc_termino()));
					    sum_total_mc=total_mc-paginacion.getMc_termino();
					    if(pag==paginas && paginacion.getMc_termino()<total_mc){
					    	paginas+=1;
					    	mc_termino+=1;
					    }
					}
				}
		    }
		    coleccionPAG.add(paginacion);
		}
/*
		
*/
		parametros.put("COLECCION_PAG", coleccionPAG);

		parametros.put("TOTAL", total);
		parametros.put("TOTAL_BC", total_bc);
		parametros.put("TOTAL_ACL", total_acl);
		parametros.put("TOTAL_MC", total_mc);
		parametros.put("MAX_POR_PAGINA", maxPorPagina);
		parametros.put("PAGINAS", paginas);

		parametros.put("BC_INICIO", bc_inicio);
		parametros.put("BC_TERMINO", bc_termino);
		parametros.put("ACL_INICIO", acl_inicio);
		parametros.put("ACL_TERMINO", acl_termino);
		parametros.put("MC_INICIO", mc_inicio);
		parametros.put("MC_TERMINO", mc_termino);

		parametros.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		
		}catch(Exception ex){
			LOG.error("Error al generar Certificado ICT1: +"+ex.getLocalizedMessage());
			LOG.error("Error al generar Certificado ICT2: +"+ex);
		}

		return parametros;
	}

	public static int getPaginas(Integer total, int maxPorPagina, int resto) {
		if (total == 0) {
			return 0;
		}
		int pag = 0;
		if (resto > 0 && total > resto) {
			total -= resto;
			pag = 1;
		}
		int min = (int) Math.floor(total.doubleValue() / maxPorPagina);
		int max = (int) Math.ceil(total.doubleValue() / maxPorPagina);
		Double diff = new Double(total.doubleValue() / maxPorPagina);

		return (total == (max * maxPorPagina) ? max - 1 : min) + pag;
	}

	public static List<CertificadoICTbcDTO> getColeccionICTbc(ConsultaBICDTO consulta) {
		List<CertificadoICTbcDTO> data = new ArrayList<CertificadoICTbcDTO>();

		for (Integer i = 0; i < consulta.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consulta.getProtVigentes().get(i);

			if (protesto.getFecRecepAcl().compareTo("") == 0
					|| protesto.getFecRecepAcl().compareTo("1900-01-01 00:00:00.0") == 0) {
				CertificadoICTbcDTO boletinComercial = new CertificadoICTbcDTO();
				boletinComercial.setBoletin(new String(CertificadosUtil.getNumero(protesto.getNroBoletin(), 4) + "/"
						+ CertificadosUtil.getNumero(protesto.getPagBoletin(), 4)));
				boletinComercial.setFechaProtesto(CertificadosUtil.getFecha(protesto.getFecProt()));
				boletinComercial.setFechaRecepcion("");
				boletinComercial.setTipoDocumento(protesto.getTipoDocumento());
				boletinComercial.setTipoDocumentoImpago(protesto.getTipoDocImpago());
				boletinComercial.setNroOperacion(protesto.getNroOper4Dig().toString());
				boletinComercial.setCodEmisor(protesto.getCodEmisor());
				boletinComercial.setEmisor(protesto.getGlosaEmisor());
				boletinComercial.setMoneda(protesto.getGlosaCorta());
				boletinComercial.setMonto(CertificadosUtil
						.getMonto(BigDecimal.valueOf(Double.parseDouble(protesto.getMontoProt().toString()))));
				boletinComercial.setCiudad(protesto.getGlosaLocPub());
				boletinComercial.setLibrador(protesto.getNombreLibrador());
				data.add(boletinComercial);
			}
		}

		return data;
	}

	public static List<CertificadoICTbcDTO> getColeccionICTbcAcl(ConsultaBICDTO consulta) {
		List<CertificadoICTbcDTO> data = new ArrayList<CertificadoICTbcDTO>();

		for (Integer i = 0; i < consulta.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consulta.getProtVigentes().get(i);

			if (protesto.getFecRecepAcl().compareTo("") != 0
					&& protesto.getFecRecepAcl().compareTo("1900-01-01 00:00:00.0") != 0) {
				CertificadoICTbcDTO boletinComercial = new CertificadoICTbcDTO();
				boletinComercial.setBoletin(new String(CertificadosUtil.getNumero(protesto.getNroBoletin(), 4) + "/"
						+ CertificadosUtil.getNumero(protesto.getPagBoletin(), 4)));
				boletinComercial.setFechaProtesto(CertificadosUtil.getFecha(protesto.getFecProt()));
				boletinComercial.setFechaRecepcion(CertificadosUtil.getFecha(protesto.getFecRecepAcl()));
				boletinComercial.setTipoDocumento(protesto.getTipoDocumento());
				boletinComercial.setTipoDocumentoImpago(protesto.getTipoDocImpago());
				boletinComercial.setNroOperacion(protesto.getNroOper4Dig().toString());
				boletinComercial.setCodEmisor(protesto.getCodEmisor());
				boletinComercial.setEmisor(protesto.getGlosaEmisor());
				boletinComercial.setMoneda(protesto.getGlosaCorta());
				boletinComercial.setMonto(CertificadosUtil
						.getMonto(BigDecimal.valueOf(Double.parseDouble(protesto.getMontoProt().toString()))));
				boletinComercial.setCiudad(protesto.getGlosaLocPub());
				boletinComercial.setLibrador(protesto.getNombreLibrador());
				data.add(boletinComercial);
			}
		}

		return data;
	}

	public static List<CertificadoICTmcDTO> getColeccionICTmc(ConsultaMOLDTO consulta) {
		List<CertificadoICTmcDTO> data = new ArrayList<CertificadoICTmcDTO>();

		for (Integer i = 0; i < consulta.getListMorosidadVig().size(); i++) {
			MorosidadVigenteDTO morosidad = consulta.getListMorosidadVig().get(i);

			CertificadoICTmcDTO morosidadComercial = new CertificadoICTmcDTO();
			morosidadComercial.setFechaVencimiento(CertificadosUtil.getFecha(morosidad.getFechaVcto()));
			morosidadComercial.setTipoCredito(morosidad.getTipoCredito());
			morosidadComercial.setMoneda(morosidad.getMoneda());
			morosidadComercial.setMonto(CertificadosUtil
					.getMonto(BigDecimal.valueOf(Double.parseDouble(morosidad.getMontoDeuda().toString()))));
			morosidadComercial.setEmisor(morosidad.getNombreEmisor());

			data.add(morosidadComercial);
		}

		return data;
	}

	public static Map<String, Object> getParametrosCAC(CertificadoAclDTO certificado, UsuarioDTO usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		Integer indice = 0;

		parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "FECHA PRESENTACIÓN");
		parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
				CertificadosUtil.getFecha(certificado.getFecAclaracion()));

		parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "TIPO DE DOCUMENTO");
		parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
				certificado.getGlosaTipoDocumento().trim());

		if (certificado.getTipoDocumento().compareTo("CM") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "TIPO CRÉDITO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					certificado.getTipoDocumentoImpago().trim());
		}

		if (certificado.getTipoDocumento().compareTo("CH") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "NÚMERO DOCUMENTO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					certificado.getNroOper4Dig().toString());
		}

		parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "NOMBRE DEUDOR");
		parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
				CertificadosUtil.getNombre(certificado.getRutAfectado(), certificado.getNombre(),
						certificado.getApellidoPaterno(), certificado.getApellidoMaterno()));

		parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "R.U.T.");
		parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
				CertificadosUtil.getRut(certificado.getRutAfectado()));

		if (certificado.getTipoDocumento().compareTo("CH") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "BANCO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false), certificado.getGlosaEmisor().trim());
		}
		if (certificado.getTipoDocumento().compareTo("CM") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "INSTITUCIÓN");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false), certificado.getGlosaEmisor().trim());
		}
		if (certificado.getTipoDocumento().compareTo("LT") == 0
				|| certificado.getTipoDocumento().compareTo("PG") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "GIRADOR");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					certificado.getNombreLibrador().trim());
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "NOTARIO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false), certificado.getGlosaEmisor().trim());
		}
		if (certificado.getTipoDocumento().compareTo("RA") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "ARRENDADOR");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					certificado.getNombreLibrador().trim());
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "JUZGADO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false), certificado.getGlosaEmisor().trim());
		}

		parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "MONTO");
		parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
				CertificadosUtil.getMonto(certificado.getMontoProt()) + " " + certificado.getMoneda());

		if (certificado.getTipoDocumento().compareTo("CM") == 0
				|| certificado.getTipoDocumento().compareTo("RA") == 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "FECHA DE VENCIMIENTO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					CertificadosUtil.getFecha(certificado.getFecProt()));
		} else {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "FECHA DE PROTESTO");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					CertificadosUtil.getFecha(certificado.getFecProt()));
		}

		if (certificado.getNroBoletinProt() > 0) {
			parametros.put(CertificadosUtil.getNombreParametroCAC(++indice, true), "BOLETÍN DE PUBLICACIÓN");
			parametros.put(CertificadosUtil.getNombreParametroCAC(indice, false),
					certificado.getNroBoletinProt().toString());
		}

		parametros.put("boletinSiguiente", certificado.getPagBoletinProt().toString());
		parametros.put("fechaBoletinSiguiente", CertificadosUtil.getFecha(certificado.getFecPublicacion()));

		// ParÃƒÂƒÃ‚Â¡metros Pie PÃƒÂƒÃ‚Â¡gina Certificado CAC
		Date hoyDia = new Date();
		parametros.put("tipoPersona", CertificadosUtil.getTipoPersonaRut(certificado.getRutAfectado()));
		parametros.put("usuario", usuario.getUsername().trim());
		parametros.put("centroCosto", usuario.getCentroCostoActivo().getDescripcion().trim());
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));
		parametros.put("codigoVerificacion", certificado.getCodAutenticBic());

		return parametros;
	}

	public static Map<String, Object> getParametrosCAA(ConsultaCerAclDTO certificado, UsuarioDTO usuario,
			PersonaDTO persona) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		Integer maxPorPagina = 28;

		parametros.put("TITULO", "CERTIFICADO DE ACLARACIÓN EN TRÁMITE");
		parametros.put("RUT", CertificadosUtil.getRut(certificado.getRutAfectado()));
		// parametros.put("NOMBRE", certificado.getNombreAfectado());
		parametros.put("TIPO_PERSONA", CertificadosUtil.getTipoPersonaRut(certificado.getRutAfectado()));
		parametros.put("NOMBRE", CertificadosUtil.getNombre(persona.getRut(), persona.getNombres(),
				persona.getApellidoPaterno(), persona.getApellidoMaterno()));

		parametros.put("COLECCION", CertificadosUtil.getColeccionCAA(certificado, maxPorPagina));
		parametros.put("COLECCION_MAX_PAGINA", maxPorPagina);

		Date hoyDia = new Date();
		parametros.put("USUARIO", usuario.getUsername().trim());
		parametros.put("SUCURSAL", usuario.getCentroCostoActivo().getDescripcion().trim());
		parametros.put("FECHA", CertificadosUtil.getFecha(hoyDia));
		parametros.put("TIPO_CERTIFICADO", "5");
		parametros.put("CODIGO_VERIFICACION", certificado.getCodAutenticBic());

		return parametros;
	}

	public static List<CertificadoAclDTO> getColeccionCAA(ConsultaCerAclDTO certificado, Integer maxPorPagina) {

		List<CertificadoAclDTO> dataOriginal = certificado.getDetalle();
		List<CertificadoAclDTO> data = new ArrayList<CertificadoAclDTO>();
		Integer maxACL = maxPorPagina - 2;

		for (Integer i = 0; i < dataOriginal.size(); i++) {
			CertificadoAclDTO d = dataOriginal.get(i);

			d.setMontoProtString(CertificadosUtil.getMonto(d.getMontoProt()));
			d.setFecAclaracion(CertificadosUtil.getFecha(d.getFecAclaracion()));
			d.setFecPago(CertificadosUtil.getFecha(d.getFecPago()));
			d.setFecProt(CertificadosUtil.getFecha(d.getFecProt()));
			d.setFecPublicacion(CertificadosUtil.getFecha(d.getFecPublicacion()));
			data.add(d);
		}

		CertificadoAclDTO fechaPresentacion = new CertificadoAclDTO(
				"Fecha de Presentación : " + CertificadosUtil.getFecha(certificado.getFechaAcl()));
		CertificadoAclDTO finDetalle = new CertificadoAclDTO("Fin Detalle");
		if (data.size() <= maxACL) {
			data.add(fechaPresentacion);
			data.add(finDetalle);
		} else if (data.size() > maxACL) {
			data.add(maxPorPagina - 2, fechaPresentacion);
			data.add(maxPorPagina - 1, finDetalle);
			Integer loop = 1;
			do {
				if (data.size() > maxACL + (maxACL * loop)) {
					data.add(maxPorPagina + (maxACL * loop) + 1, fechaPresentacion);
					data.add(maxPorPagina + (maxACL * loop) + 2, finDetalle);
				} else {
					data.add(fechaPresentacion);
					data.add(finDetalle);
				}
			} while (data.size() > maxPorPagina + (maxACL * loop++));
		}
		return data;
	}

	
	public static Map<String, Object> getParametrosISA(ConsultaBICDTO consultaBIC, ConsultaMOLDTO consultaMOL,
			UsuarioDTO usuario, Integer maxPorPagina) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		try{
		
		Date hoyDia = new Date();
		List<CertificadoISAbcDTO> coleccionBC = CertificadosUtil.getColeccionISAbc(consultaBIC);
		//List<CertificadoISAbcDTO> coleccionBCACL = CertificadosUtil.getColeccionICTbcAcl(consultaBIC);
		List<CertificadoISAmcDTO> coleccionMC = CertificadosUtil.getColeccionISAmc(consultaMOL);

		parametros
				.put("titulo",
						(CertificadosUtil.getTipoPersonaRut(
								consultaBIC.getRut()).compareTo("N") == 0 ? "CERTIFICADO PARA FINES ESPECIALES"
										: "CERTIFICADO")
								+ CertificadosUtil.newline
								+ "INCLUYE OBLIGACIONES VENCIDAS Y NO PAGADAS EN BOLETÍN COMERCIAL"
								+ CertificadosUtil.newline
								+ "Y BASE DE DATOS DE MOROSIDAD DE LOS SISTEMAS FINANCIERO/COMERCIAL");
		parametros.put("rut", CertificadosUtil.getRut(consultaBIC.getRut()));
		parametros.put("tipoPersona", CertificadosUtil.getTipoPersonaRut(consultaBIC.getRut()));
		parametros.put("nombre", CertificadosUtil.getNombre(consultaBIC.getRut(), consultaBIC.getNombres(),
				consultaBIC.getApPat(), consultaBIC.getApMat()));
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));

		parametros.put("totalPublicaciones",
				Integer.toString(consultaBIC.getProtVigentes().size() + consultaMOL.getListMorosidadVig().size()));
		parametros.put("totalBoletin", Integer.toString(consultaBIC.getProtVigentes().size()));
		parametros.put("totalMorosidad", Integer.toString(consultaMOL.getListMorosidadVig().size()));
		//parametros.put("totalTramiteAclaracion", Integer.toString(coleccionBCACL.size()));

		// cheques, letras, cuotas, rentas
		Integer bcCheques = 0, bcLetras = 0, bcCuotas = 0, bcRentas = 0;
		for (Integer i = 0; i < consultaBIC.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consultaBIC.getProtVigentes().get(i);

			if (protesto.getTipoDocumento().compareTo("CH") == 0) {
				++bcCheques;
			}
			if (protesto.getTipoDocumento().compareTo("LT") == 0 || protesto.getTipoDocumento().compareTo("PG") == 0) {
				++bcLetras;
			}
			if (protesto.getTipoDocumento().compareTo("CM") == 0) {
				++bcCuotas;
			}
			if (protesto.getTipoDocumento().compareTo("RA") == 0) {
				++bcRentas;
			}
		}
		// bc_cheques, bc_letras, bc_cuotas, bc_rentas
		parametros.put("bc_cheques", bcCheques.toString());
		parametros.put("bc_letras", bcLetras.toString());
		parametros.put("bc_cuotas", bcCuotas.toString());
		parametros.put("bc_rentas", bcRentas.toString());

		// tarjetas, letras, pagares, otras
		Integer mcTarjetas = 0, mcLetras = 0, mcPagares = 0, mcOtras = 0;
		for (Integer i = 0; i < coleccionMC.size(); i++) {
			CertificadoISAmcDTO morosidad = coleccionMC.get(i);

			if (morosidad.getTipoCredito().compareTo("TC") == 0) {
				++mcTarjetas;
			} else if (morosidad.getTipoCredito().compareTo("LT") == 0) {
				++mcLetras;
			} else if (morosidad.getTipoCredito().compareTo("PG") == 0) {
				++mcPagares;
			} else {
				++mcOtras;
			}
		}
		// mc_tarjetas, mc_letras, mc_pagares, mc_otras
		parametros.put("mc_tarjetas", mcTarjetas.toString());
		parametros.put("mc_letras", mcLetras.toString());
		parametros.put("mc_pagares", mcPagares.toString());
		parametros.put("mc_otras", mcOtras.toString());

		parametros.put("usuario", usuario.getUsername().trim());
		parametros.put("sucursal", usuario.getCentroCostoActivo().getDescripcion().trim());
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));
		parametros.put("codigoVerificacion",
				consultaBIC.getCodAutenticBic().trim() + " - " + consultaMOL.getCodAutenticMol().trim());

		// paginacion
		// paginacion
		List<CertificadoISAPaginacionDTO> coleccionPAG = new ArrayList<CertificadoISAPaginacionDTO>();
		Integer total = new Integer(coleccionBC.size() + coleccionMC.size());
		Integer total_bc = coleccionBC.size();
		//Integer total_acl = coleccionBCACL.size();
		Integer total_mc = coleccionMC.size();
		Integer maxBC = 26;
		Integer maxMC = 23;
		Integer maxBcMc = 15;
		Integer paginas = 1; // (int)Math.ceil(total.doubleValue() /
								// maxPorPagina);

		// bc
		Integer bc_inicio = 1;
		Integer bc_termino = bc_inicio + CertificadosUtil.getPaginas(total_bc, maxBC, 0);
		Integer bc_resto = (bc_termino * maxBC) - total_bc;
		// acl
		//Integer acl_inicio = bc_resto == 0 && total_acl > 0 ? bc_termino + 1 : bc_termino;
		//Integer acl_termino = acl_inicio + CertificadosUtil.getPaginas(total_acl, maxBC, bc_resto);
		//Integer acl_resto = (acl_termino * maxBC) - (total_bc + total_acl);
		//acl_resto = acl_resto <= (maxBC - maxBcMc) ? 0 : acl_resto - (maxBC - maxBcMc);
		// mc
		/*Integer mc_inicio=0;
		if(bc_resto==1 || bc_resto==2)  mc_inicio = bc_termino+1;
		else
		mc_inicio = bc_resto == 0 ? bc_termino + 1 : bc_termino;
		Integer mc_termino=0;
		
		mc_termino = mc_inicio + CertificadosUtil.getPaginas(total_mc, maxMC, bc_resto);
		Integer mc_resto = (mc_termino * maxMC) - total_mc;
		//Integer mc_resto=0;
		mc_resto = mc_resto <= (maxMC - maxBcMc) ? 0 : mc_resto - (maxMC - maxBcMc);
		if (total_mc>0)mc_termino = ((mc_inicio==bc_termino) && ((maxBC-bc_resto)+total_mc>maxBcMc)) ? mc_termino + 1 : mc_termino;
		//if((mc_resto==1 || mc_resto==2))  mc_termino = mc_termino+1;
		// paginas
 		paginas = mc_termino;
		*/
		Integer mc_inicio=0;
        if(bc_resto==1 || bc_resto==2)  
        	mc_inicio = bc_termino+1;
        else if(total_mc==0 && bc_resto==11)
        	mc_inicio =  bc_termino;
        else mc_inicio = ((bc_resto<11) || (total_mc>0 && bc_resto==11)) ? bc_termino + 1 : bc_termino;
        Integer mc_termino=0;
        if(mc_inicio==bc_termino)
        	mc_termino = mc_inicio + CertificadosUtil.getPaginas(total_mc, maxMC, bc_resto);
        else 
        	mc_termino = bc_termino + CertificadosUtil.getPaginas(total_mc, maxMC, bc_resto);
        Integer mc_resto = (mc_termino * maxMC) - total_mc;
        mc_resto = mc_resto <= (maxBC - maxBcMc) ? 0 : mc_resto - (maxBC - maxBcMc);
        if (total_mc>0)
        	mc_termino = ((mc_inicio==bc_termino) && (mc_inicio==mc_termino) && (((maxBC-bc_resto)+total_mc)>maxBcMc)) ? mc_termino + 1 : mc_termino;
        // paginas
        if(mc_termino<mc_inicio) mc_termino=mc_inicio;
        paginas = mc_termino;
		// elaboracion lista de paginas y bloques
		int sum_total_bc=total_bc;
		int sum_total_mc=total_mc;
        for (int p = 0; p < paginas; p++) {
            int pag = p + 1;
            int resto = 0;

            CertificadoISAPaginacionDTO paginacion = new CertificadoISAPaginacionDTO();
            paginacion.setPagina(pag);

            // bc
            
            paginacion.setBc(bc_inicio <= pag && pag <= bc_termino);
            if (paginacion.isBc()) {
                if (sum_total_bc > 0) {
					paginacion.setBc_inicio(pag == bc_inicio ? 0 : coleccionPAG.get(p - 1).getBc_termino());
					int diff_bc = total_bc - paginacion.getBc_inicio();
					diff_bc = diff_bc > maxBC ? maxBC : diff_bc;
					resto = diff_bc < maxBC ? maxBC - diff_bc : 0;
					paginacion.setBc_termino(paginacion.getBc_inicio() + diff_bc);
					if (paginacion.getBc_inicio() == paginacion.getBc_termino()) {
					    paginacion.getBc_coleccion().add(coleccionBC.get(paginacion.getBc_inicio()));
					    sum_total_bc=0;
					} else {
					    paginacion.setBc_coleccion(coleccionBC.subList(paginacion.getBc_inicio(), paginacion.getBc_termino()));
					    sum_total_bc=total_bc-paginacion.getBc_termino();
					}
                }
            }

            // mc
            paginacion.setMc(mc_inicio <= pag && pag <= mc_termino);
            if (paginacion.isMc()) {
				if (sum_total_mc > 0) {
					paginacion.setMc_inicio(pag == mc_inicio ? 0 : coleccionPAG.get(p - 1).getMc_termino());
					int diff_mc = total_mc - paginacion.getMc_inicio();
						if(pag==bc_termino && bc_inicio==bc_termino){
							diff_mc = diff_mc > maxBcMc - paginacion.getBc_termino() ? maxBcMc - (paginacion.getBc_termino() - paginacion.getBc_inicio()) : diff_mc;
							resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
						}else{
							resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
							diff_mc = diff_mc > resto && resto > 0 ? resto : (diff_mc > maxMC ? maxMC : diff_mc);
						}
					    paginacion.setMc_termino(paginacion.getMc_inicio() + diff_mc);
					if (paginacion.getMc_inicio() == paginacion.getMc_termino()) {
						paginacion.getMc_coleccion().add(coleccionMC.get(paginacion.getMc_inicio()));
						sum_total_mc=0;
					} else {
					    paginacion.setMc_coleccion(coleccionMC.subList(paginacion.getMc_inicio(), paginacion.getMc_termino()));
					    sum_total_mc=total_mc-paginacion.getMc_termino();
					    if(pag==paginas && paginacion.getMc_termino()<total_mc){
					    	paginas+=1;
					    	mc_termino+=1;
					    }
					}
				}
            }
    
            coleccionPAG.add(paginacion);
        }
		parametros.put("COLECCION_PAG", coleccionPAG);

		parametros.put("TOTAL", total);
		parametros.put("TOTAL_BC", total_bc);
		parametros.put("TOTAL_MC", total_mc);
		parametros.put("MAX_POR_PAGINA", maxPorPagina);
		parametros.put("PAGINAS", paginas);

		parametros.put("BC_INICIO", bc_inicio);
		parametros.put("BC_TERMINO", bc_termino);
		parametros.put("MC_INICIO", mc_inicio);
		parametros.put("MC_TERMINO", mc_termino);

		parametros.put("SUBREPORT_DIR", "wlscomun/Certificados/");

		}catch(Exception ex){
			LOG.error("Error al generar Certificado ISA1: +"+ex.getLocalizedMessage());
			LOG.error("Error al generar Certificado ISA2: +"+ex);
		}
		
		return parametros;
	}
	
	public static List<CertificadoISAbcDTO> getColeccionISAbc(ConsultaBICDTO consulta) {
		List<CertificadoISAbcDTO> data = new ArrayList<CertificadoISAbcDTO>();

		for (Integer i = 0; i < consulta.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consulta.getProtVigentes().get(i);

			//if (protesto.getFecRecepAcl().compareTo("") == 0
			//		|| protesto.getFecRecepAcl().compareTo("1900-01-01 00:00:00.0") == 0) {
				CertificadoISAbcDTO boletinComercial = new CertificadoISAbcDTO();
				boletinComercial.setBoletin(new String(CertificadosUtil.getNumero(protesto.getNroBoletin(), 4) + "/"
						+ CertificadosUtil.getNumero(protesto.getPagBoletin(), 4)));
				boletinComercial.setFechaProtesto(CertificadosUtil.getFecha(protesto.getFecProt()));
				boletinComercial.setFechaRecepcion("");
				boletinComercial.setTipoDocumento(protesto.getTipoDocumento());
				boletinComercial.setTipoDocumentoImpago(protesto.getTipoDocImpago());
				boletinComercial.setNroOperacion(protesto.getNroOper4Dig().toString());
				boletinComercial.setCodEmisor(protesto.getCodEmisor());
				boletinComercial.setEmisor(protesto.getGlosaEmisor());
				boletinComercial.setMoneda(protesto.getGlosaCorta());
				boletinComercial.setMonto(CertificadosUtil
						.getMonto(BigDecimal.valueOf(Double.parseDouble(protesto.getMontoProt().toString()))));
				boletinComercial.setCiudad(protesto.getGlosaLocPub());
				boletinComercial.setLibrador(protesto.getNombreLibrador());
				data.add(boletinComercial);
			//}
		}

		return data;
	}

	public static List<CertificadoISAmcDTO> getColeccionISAmc(ConsultaMOLDTO consulta) {
		List<CertificadoISAmcDTO> data = new ArrayList<CertificadoISAmcDTO>();

		for (Integer i = 0; i < consulta.getListMorosidadVig().size(); i++) {
			MorosidadVigenteDTO morosidad = consulta.getListMorosidadVig().get(i);

			CertificadoISAmcDTO morosidadComercial = new CertificadoISAmcDTO();
			morosidadComercial.setFechaVencimiento(CertificadosUtil.getFecha(morosidad.getFechaVcto()));
			morosidadComercial.setTipoCredito(morosidad.getTipoCredito());
			morosidadComercial.setMoneda(morosidad.getMoneda());
			morosidadComercial.setMonto(CertificadosUtil
					.getMonto(BigDecimal.valueOf(Double.parseDouble(morosidad.getMontoDeuda().toString()))));
			morosidadComercial.setEmisor(morosidad.getNombreEmisor());

			data.add(morosidadComercial);
		}

		return data;
	}
	
	public static Map<String, Object> getParametrosILG(ConsultaBICDTO consultaBIC, ConsultaMOLDTO consultaMOL,
			UsuarioDTO usuario, Integer maxPorPagina) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		Date hoyDia = new Date();
		List<CertificadoISAbcDTO> coleccionBC = CertificadosUtil.getColeccionILGbc(consultaBIC);
		//List<CertificadoISAbcDTO> coleccionBCACL = CertificadosUtil.getColeccionICTbcAcl(consultaBIC);
		List<CertificadoISAmcDTO> coleccionMC = CertificadosUtil.getColeccionILGmc(consultaMOL);

		parametros.put("titulo", "");
		parametros.put("rut", CertificadosUtil.getRut(consultaBIC.getRut()));
		parametros.put("tipoPersona", CertificadosUtil.getTipoPersonaRut(consultaBIC.getRut()));
		parametros.put("nombre", CertificadosUtil.getNombre(consultaBIC.getRut(), consultaBIC.getNombres(),	consultaBIC.getApPat(), consultaBIC.getApMat()));
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));

		parametros.put("totalPublicaciones", Integer.toString(consultaBIC.getProtVigentes().size() + consultaMOL.getListMorosidadVig().size()));
		parametros.put("totalBoletin", Integer.toString(consultaBIC.getProtVigentes().size()));
		parametros.put("totalMorosidad", Integer.toString(consultaMOL.getListMorosidadVig().size()));
		//parametros.put("totalTramiteAclaracion", Integer.toString(coleccionBCACL.size()));
		// cheques, letras, cuotas, rentas
		Integer bcCheques = 0, bcLetras = 0, bcCuotas = 0, bcRentas = 0;
		for (Integer i = 0; i < consultaBIC.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consultaBIC.getProtVigentes().get(i);

			if (protesto.getTipoDocumento().compareTo("CH") == 0) {
				++bcCheques;
			}
			if (protesto.getTipoDocumento().compareTo("LT") == 0 || protesto.getTipoDocumento().compareTo("PG") == 0) {
				++bcLetras;
			}
			if (protesto.getTipoDocumento().compareTo("CM") == 0) {
				++bcCuotas;
			}
			if (protesto.getTipoDocumento().compareTo("RA") == 0) {
				++bcRentas;
			}
		}
		// bc_cheques, bc_letras, bc_cuotas, bc_rentas
		parametros.put("bc_cheques", bcCheques.toString());
		parametros.put("bc_letras", bcLetras.toString());
		parametros.put("bc_cuotas", bcCuotas.toString());
		parametros.put("bc_rentas", bcRentas.toString());

		// tarjetas, letras, pagares, otras
		Integer mcTarjetas = 0, mcLetras = 0, mcPagares = 0, mcOtras = 0;
		for (Integer i = 0; i < coleccionMC.size(); i++) {
			CertificadoISAmcDTO morosidad = coleccionMC.get(i);

			if (morosidad.getTipoCredito().compareTo("TC") == 0) {
				++mcTarjetas;
			} else if (morosidad.getTipoCredito().compareTo("LT") == 0) {
				++mcLetras;
			} else if (morosidad.getTipoCredito().compareTo("PG") == 0) {
				++mcPagares;
			} else {
				++mcOtras;
			}
		}
		// mc_tarjetas, mc_letras, mc_pagares, mc_otras
		parametros.put("mc_tarjetas", mcTarjetas.toString());
		parametros.put("mc_letras", mcLetras.toString());
		parametros.put("mc_pagares", mcPagares.toString());
		parametros.put("mc_otras", mcOtras.toString());

		parametros.put("usuario", usuario.getUsername().trim());
		parametros.put("sucursal", usuario.getCentroCostoActivo().getDescripcion().trim());
		parametros.put("fecha", CertificadosUtil.getFecha(hoyDia));
		parametros.put("codigoVerificacion", consultaBIC.getCodAutenticBic().trim() + " - " + consultaMOL.getCodAutenticMol().trim());

		// paginacion
		List<CertificadoISAPaginacionDTO> coleccionPAG = new ArrayList<CertificadoISAPaginacionDTO>();
		Integer total = new Integer(coleccionBC.size() + coleccionMC.size());
		Integer total_bc = coleccionBC.size();
		//Integer total_acl = coleccionBCACL.size();
		Integer total_mc = coleccionMC.size();
		Integer maxBC = 26;
		Integer maxMC = 23;
		Integer maxBcMc = 15;
		Integer paginas = 1; // (int)Math.ceil(total.doubleValue() /
								// maxPorPagina);
		// bc
		Integer bc_inicio = 1;
		Integer bc_termino = bc_inicio + CertificadosUtil.getPaginas(total_bc, maxBC, 0);
		Integer bc_resto = (bc_termino * maxBC) - total_bc;
		
		Integer mc_inicio=0;
        if(bc_resto==1 || bc_resto==2)  
        	mc_inicio = bc_termino+1;
        else if(total_mc==0 && bc_resto==11)
        	mc_inicio =  bc_termino;
        else mc_inicio = ((bc_resto<11) || (total_mc>0 && bc_resto==11)) ? bc_termino + 1 : bc_termino;
        Integer mc_termino=0;
        if(mc_inicio==bc_termino)
        	mc_termino = mc_inicio + CertificadosUtil.getPaginas(total_mc, maxMC, bc_resto);
        else 
        	mc_termino = bc_termino + CertificadosUtil.getPaginas(total_mc, maxMC, bc_resto);
        Integer mc_resto = (mc_termino * maxMC) - total_mc;
        mc_resto = mc_resto <= (maxBC - maxBcMc) ? 0 : mc_resto - (maxBC - maxBcMc);
        if (total_mc>0)
        	mc_termino = ((mc_inicio==bc_termino) && (mc_inicio==mc_termino) && (((maxBC-bc_resto)+total_mc)>maxBcMc)) ? mc_termino + 1 : mc_termino;
        // paginas
        if(mc_termino<mc_inicio) mc_termino=mc_inicio;
        paginas = mc_termino;
		// elaboracion lista de paginas y bloques
		int sum_total_bc=total_bc;
		int sum_total_mc=total_mc;
		for (int p = 0; p < paginas; p++) {
            int pag = p + 1;
            int resto = 0;

            CertificadoISAPaginacionDTO paginacion = new CertificadoISAPaginacionDTO();
            paginacion.setPagina(pag);

            // bc
            
            paginacion.setBc(bc_inicio <= pag && pag <= bc_termino);
            if (paginacion.isBc()) {
                if (sum_total_bc > 0) {
					paginacion.setBc_inicio(pag == bc_inicio ? 0 : coleccionPAG.get(p - 1).getBc_termino());
					int diff_bc = total_bc - paginacion.getBc_inicio();
					diff_bc = diff_bc > maxBC ? maxBC : diff_bc;
					resto = diff_bc < maxBC ? maxBC - diff_bc : 0;
					paginacion.setBc_termino(paginacion.getBc_inicio() + diff_bc);
					if (paginacion.getBc_inicio() == paginacion.getBc_termino()) {
					    paginacion.getBc_coleccion().add(coleccionBC.get(paginacion.getBc_inicio()));
					    sum_total_bc=0;
					} else {
					    paginacion.setBc_coleccion(coleccionBC.subList(paginacion.getBc_inicio(), paginacion.getBc_termino()));
					    sum_total_bc=total_bc-paginacion.getBc_termino();
					}
                }
            }

            // mc
            paginacion.setMc(mc_inicio <= pag && pag <= mc_termino);
            if (paginacion.isMc()) {
				if (sum_total_mc > 0) {
					paginacion.setMc_inicio(pag == mc_inicio ? 0 : coleccionPAG.get(p - 1).getMc_termino());
					int diff_mc = total_mc - paginacion.getMc_inicio();
						if(pag==bc_termino && bc_inicio==bc_termino){
							diff_mc = diff_mc > maxBcMc - paginacion.getBc_termino() ? maxBcMc - (paginacion.getBc_termino() - paginacion.getBc_inicio()) : diff_mc;
							resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
						}else{
							resto = resto <= (maxBC - maxBcMc) ? 0 : resto - (maxBC - maxBcMc);
							diff_mc = diff_mc > resto && resto > 0 ? resto : (diff_mc > maxMC ? maxMC : diff_mc);
						}
					    paginacion.setMc_termino(paginacion.getMc_inicio() + diff_mc);
					if (paginacion.getMc_inicio() == paginacion.getMc_termino()) {
						paginacion.getMc_coleccion().add(coleccionMC.get(paginacion.getMc_inicio()));
						sum_total_mc=0;
					} else {
					    paginacion.setMc_coleccion(coleccionMC.subList(paginacion.getMc_inicio(), paginacion.getMc_termino()));
					    sum_total_mc=total_mc-paginacion.getMc_termino();
					    if(pag==paginas && paginacion.getMc_termino()<total_mc){
					    	paginas+=1;
					    	mc_termino+=1;
					    }
					}
				}
            }
    
            coleccionPAG.add(paginacion);
        }
		parametros.put("COLECCION_PAG", coleccionPAG);

		parametros.put("TOTAL", total);
		parametros.put("TOTAL_BC", total_bc);
		parametros.put("TOTAL_MC", total_mc);
		parametros.put("MAX_POR_PAGINA", maxPorPagina);
		parametros.put("PAGINAS", paginas);

		parametros.put("BC_INICIO", bc_inicio);
		parametros.put("BC_TERMINO", bc_termino);
		parametros.put("MC_INICIO", mc_inicio);
		parametros.put("MC_TERMINO", mc_termino);

		parametros.put("SUBREPORT_DIR", "wlscomun/Certificados/");
		return parametros;
	}
	
	public static List<CertificadoISAbcDTO> getColeccionILGbc(ConsultaBICDTO consulta) {
		List<CertificadoISAbcDTO> data = new ArrayList<CertificadoISAbcDTO>();

		for (Integer i = 0; i < consulta.getProtVigentes().size(); i++) {
			ProtestoVigenteDTO protesto = consulta.getProtVigentes().get(i);

			//if (protesto.getFecRecepAcl().compareTo("") == 0
			//		|| protesto.getFecRecepAcl().compareTo("1900-01-01 00:00:00.0") == 0) {
				CertificadoISAbcDTO boletinComercial = new CertificadoISAbcDTO();
				boletinComercial.setBoletin(new String(CertificadosUtil.getNumero(protesto.getNroBoletin(), 4) + "/"
						+ CertificadosUtil.getNumero(protesto.getPagBoletin(), 4)));
				boletinComercial.setFechaProtesto(CertificadosUtil.getFecha(protesto.getFecProt()));
				boletinComercial.setFechaRecepcion("");
				boletinComercial.setTipoDocumento(protesto.getTipoDocumento());
				boletinComercial.setTipoDocumentoImpago(protesto.getTipoDocImpago());
				boletinComercial.setNroOperacion(protesto.getNroOper4Dig().toString());
				boletinComercial.setCodEmisor(protesto.getCodEmisor());
				boletinComercial.setEmisor(protesto.getGlosaEmisor());
				boletinComercial.setMoneda(protesto.getGlosaCorta());
				boletinComercial.setMonto(CertificadosUtil
						.getMonto(BigDecimal.valueOf(Double.parseDouble(protesto.getMontoProt().toString()))));
				boletinComercial.setCiudad(protesto.getGlosaLocPub());
				boletinComercial.setLibrador(protesto.getNombreLibrador());
				data.add(boletinComercial);
			//}
		}

		return data;
	}
	

	public static List<CertificadoISAmcDTO> getColeccionILGmc(ConsultaMOLDTO consulta) {
		List<CertificadoISAmcDTO> data = new ArrayList<CertificadoISAmcDTO>();

		for (Integer i = 0; i < consulta.getListMorosidadVig().size(); i++) {
			MorosidadVigenteDTO morosidad = consulta.getListMorosidadVig().get(i);

			CertificadoISAmcDTO morosidadComercial = new CertificadoISAmcDTO();
			morosidadComercial.setFechaVencimiento(CertificadosUtil.getFecha(morosidad.getFechaVcto()));
			morosidadComercial.setTipoCredito(morosidad.getTipoCredito());
			morosidadComercial.setMoneda(morosidad.getMoneda());
			morosidadComercial.setMonto(CertificadosUtil
					.getMonto(BigDecimal.valueOf(Double.parseDouble(morosidad.getMontoDeuda().toString()))));
			morosidadComercial.setEmisor(morosidad.getNombreEmisor());

			data.add(morosidadComercial);
		}

		return data;
	}
	
	public static Map<String, Object> getParametrosCAR(ConsultaCerAclDTO certificado, UsuarioDTO usuario,
			PersonaDTO persona) {
		return CertificadosUtil.getParametrosCAA(certificado, usuario, persona);
	}

	public static String getNombreParametroCAC(Integer indice, Boolean isKey) {
		return isKey ? ("L" + indice.toString() + "k") : ("L" + indice.toString() + "v");
	}

	public static String getNombreArchivoCertificado(String codigoVerificacion) {
		return codigoVerificacion.replaceAll(" ", "").replaceAll("-", "_");
	}

	public static String getFecha(String fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date laFecha = new Date();
		try {
			laFecha = formatter.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(laFecha);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		return year == 1900 && month == 0 && day == 1 ? "" : new SimpleDateFormat("dd/MM/yyyy").format(laFecha);
	}

	public static String getFecha(Long fecha) {
		Date laFecha = new Date(fecha);
		Calendar cal = Calendar.getInstance();
		cal.setTime(laFecha);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		return year == 1900 && month == 0 && day == 1 ? "" : new SimpleDateFormat("dd/MM/yyyy").format(laFecha);
	}

	public static String getFecha(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		return year == 1900 && month == 0 && day == 1 ? "" : new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	}

	public static String getNombre(String rut, String nombre, String apPat, String apMat) {
		if (CertificadosUtil.getTipoPersonaRut(rut).compareTo("J") == 0) {
			return apPat.trim() + apMat.trim() + nombre.trim();
		}
		return apPat.trim() + ", " + apMat.trim() + ", " + nombre.trim();
	}

	public static String getRut(String rut) {
		while (rut.charAt(0) == '0') {
			rut = rut.substring(1);
		}
		rut = rut.replace(".", "");
		if (!rut.contains("-")) {
			rut = CertificadosUtil.getMiles(new Integer(rut.substring(0, rut.length() - 1))) + "-"
					+ rut.charAt(rut.length() - 1);
		} else {
			rut = CertificadosUtil.getMiles(new Integer(rut.substring(0, rut.lastIndexOf('-')))) + "-"
					+ rut.charAt(rut.length() - 1);
		}
		return rut.trim().toUpperCase();
	}

	public static String getTipoPersonaRut(String rut) {
		if (rut.contains(".")) {
			rut = rut.replace(".", "");
		}
		if (rut.contains("-")) {
			rut = rut.substring(0, rut.lastIndexOf("-"));
		}
		return Integer.parseInt(rut) < 50000000 ? "N" : "J";
	}

	public static String getMonto(BigDecimal monto) {
		Locale currentLocale = Locale.getDefault();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(currentLocale);
		dfs.setDecimalSeparator(',');
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat();
		formatter.setDecimalFormatSymbols(dfs);
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
		formatter.setGroupingSize(3);

		return formatter.format(monto.doubleValue());
	}

	public static String getMiles(Integer numero) {
		Locale currentLocale = Locale.getDefault();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(currentLocale);
		dfs.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat();
		formatter.setDecimalFormatSymbols(dfs);
		formatter.setMaximumFractionDigits(0);
		formatter.setGroupingSize(3);

		return formatter.format(numero);
	}

	public static String getNumero(Integer numero, Integer digitos) {
		String retorno = numero.toString();
		while (retorno.toString().length() < digitos) {
			retorno = new String("0" + retorno);
		}
		return retorno;
	}

}
