package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ModuleFunction database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ModuleFunction.findAll", query="SELECT t FROM Tbl_ModuleFunction t")
public class Tbl_ModuleFunction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FunctionName")
	private String fld_FunctionName;

	@Column(name="Fld_ModuleName")
	private String fld_ModuleName;

	public Tbl_ModuleFunction() {
	}

	public String getFld_FunctionName() {
		return this.fld_FunctionName;
	}

	public void setFld_FunctionName(String fld_FunctionName) {
		this.fld_FunctionName = fld_FunctionName;
	}

	public String getFld_ModuleName() {
		return this.fld_ModuleName;
	}

	public void setFld_ModuleName(String fld_ModuleName) {
		this.fld_ModuleName = fld_ModuleName;
	}

}