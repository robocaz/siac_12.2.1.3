package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ProductosVendidos database table.
 * 
 */
@Entity
@Table(name="Tbl_ProductosVendidos")
@NamedQuery(name="Tbl_ProductosVendido.findAll", query="SELECT t FROM Tbl_ProductosVendido t")
public class Tbl_ProductosVendido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodProducto")
	private byte fld_CodProducto;

	@Column(name="Fld_CodSitPago")
	private byte fld_CodSitPago;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrCobroProd")
	private BigDecimal fld_CorrCobroProd;

	@Column(name="Fld_FecCancelacion")
	private Timestamp fld_FecCancelacion;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecFactura")
	private Timestamp fld_FecFactura;

	@Column(name="Fld_FecVenta")
	private Timestamp fld_FecVenta;

	@Column(name="Fld_MontoNeto")
	private BigDecimal fld_MontoNeto;

	@Column(name="Fld_MontoPago")
	private BigDecimal fld_MontoPago;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroFacturaVta")
	private BigDecimal fld_NroFacturaVta;

	@Column(name="Fld_RutCliente")
	private String fld_RutCliente;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorIva")
	private BigDecimal fld_ValorIva;

	public Tbl_ProductosVendido() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public byte getFld_CodProducto() {
		return this.fld_CodProducto;
	}

	public void setFld_CodProducto(byte fld_CodProducto) {
		this.fld_CodProducto = fld_CodProducto;
	}

	public byte getFld_CodSitPago() {
		return this.fld_CodSitPago;
	}

	public void setFld_CodSitPago(byte fld_CodSitPago) {
		this.fld_CodSitPago = fld_CodSitPago;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrCobroProd() {
		return this.fld_CorrCobroProd;
	}

	public void setFld_CorrCobroProd(BigDecimal fld_CorrCobroProd) {
		this.fld_CorrCobroProd = fld_CorrCobroProd;
	}

	public Timestamp getFld_FecCancelacion() {
		return this.fld_FecCancelacion;
	}

	public void setFld_FecCancelacion(Timestamp fld_FecCancelacion) {
		this.fld_FecCancelacion = fld_FecCancelacion;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecFactura() {
		return this.fld_FecFactura;
	}

	public void setFld_FecFactura(Timestamp fld_FecFactura) {
		this.fld_FecFactura = fld_FecFactura;
	}

	public Timestamp getFld_FecVenta() {
		return this.fld_FecVenta;
	}

	public void setFld_FecVenta(Timestamp fld_FecVenta) {
		this.fld_FecVenta = fld_FecVenta;
	}

	public BigDecimal getFld_MontoNeto() {
		return this.fld_MontoNeto;
	}

	public void setFld_MontoNeto(BigDecimal fld_MontoNeto) {
		this.fld_MontoNeto = fld_MontoNeto;
	}

	public BigDecimal getFld_MontoPago() {
		return this.fld_MontoPago;
	}

	public void setFld_MontoPago(BigDecimal fld_MontoPago) {
		this.fld_MontoPago = fld_MontoPago;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public BigDecimal getFld_NroFacturaVta() {
		return this.fld_NroFacturaVta;
	}

	public void setFld_NroFacturaVta(BigDecimal fld_NroFacturaVta) {
		this.fld_NroFacturaVta = fld_NroFacturaVta;
	}

	public String getFld_RutCliente() {
		return this.fld_RutCliente;
	}

	public void setFld_RutCliente(String fld_RutCliente) {
		this.fld_RutCliente = fld_RutCliente;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorIva() {
		return this.fld_ValorIva;
	}

	public void setFld_ValorIva(BigDecimal fld_ValorIva) {
		this.fld_ValorIva = fld_ValorIva;
	}

}