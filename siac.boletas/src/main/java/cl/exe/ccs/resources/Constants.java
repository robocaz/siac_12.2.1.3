package cl.exe.ccs.resources;

import java.util.Locale;
import java.util.Properties;


public class Constants {
    
    private final static PropertiesManager propManager = new PropertiesManager();
    private final static Properties prop = propManager.getPropertiesConstants();
    
    public final static Locale LOCALE = new Locale("es", "CL");

    // URL asignada desde init (web.xml)

    public final static String URL_PORTAL_CLIENTES = getProp("URL_PORTAL_CLIENTES");;  
    
    public final static String JNDI_ORACLE = getProp("JNDI_ORACLE");
    public final static String JNDI_SYBASE = getProp("JNDI_SYBASE");
    
    public final static Long MAX_RUT_VALUE = Long.parseLong(getProp("MAX_RUT_VALUE"));
    public final static Integer CMB_DEFAULT_VALUE= Integer.parseInt(getProp("CMB_DEFAULT_VALUE"));
    
    public final static String CODE_TE_AVISA_TOTAL = getProp("CODE_TE_AVISA_TOTAL");
    public final static String CODE_TE_AVISA_SMS = getProp("CODE_TE_AVISA_SMS");
    public final static String CODE_TE_AVISA_EMAIL = getProp("CODE_TE_AVISA_EMAIL");
    public final static String CODE_TE_AVISA_RACT = getProp("CODE_TE_AVISA_RACT");
    public final static String NOT_SPECIFIED_ACTIVITY = getProp("NOT_SPECIFIED_ACTIVITY");
    public final static String CODE_TE_AVISA_UPGRADE = getProp("CODE_TE_AVISA_UPGRADE");
    public final static String CODE_TE_AVISA_DETALLE = getProp("CODE_TE_AVISA_DETALLE");
    public final static String CODE_CERTIFICADO_UNO = getProp("CODE_CERTIFICADO_UNO");
    public final static String CODE_CERTIFICADO_DOS = getProp("CODE_CERTIFICADO_DOS");
    public final static String CODE_CERTIFICADO_TRES = getProp("CODE_CERTIFICADO_TRES");
    
    public final static String CODE_RENUEVA_RESUMEN = getProp("CODE_RENUEVA_RESUMEN");
    public final static String CODE_RENUEVA_EMAIL = getProp("CODE_RENUEVA_EMAIL");
    public final static String CODE_RENUEVA_SMS = getProp("CODE_RENUEVA_SMS");
    
    public final static String GLOSA_TE_AVISA_TOTAL = getProp("GLOSA_TE_AVISA_TOTAL");
    public final static String GLOSA_TE_AVISA_SMS = getProp("GLOSA_TE_AVISA_SMS");
    public final static String GLOSA_TE_AVISA_EMAIL = getProp("GLOSA_TE_AVISA_EMAIL");
    public final static String GLOSA_TE_AVISA_RACT = getProp("GLOSA_TE_AVISA_RACT");
    
    public final static String GLOSA_CERTIFICADO_UNO = getProp("GLOSA_CERTIFICADO_UNO");
    public final static String GLOSA_CERTIFICADO_DOS = getProp("GLOSA_CERTIFICADO_DOS");
    public final static String GLOSA_CERTIFICADO_TRES = getProp("GLOSA_CERTIFICADO_TRES");
    
    public final static String ALARMA_EMAIL = getProp("ALARMA_EMAIL");
    public final static String ALARMA_MOVIL = getProp("ALARMA_MOVIL");
    public final static String ALARMA_PREVENTIVA_TOTAL = getProp("ALARMA_PREVENTIVA_TOTAL");

    public final static String DEFAULT_LOG_FILENAME = getProp("DEFAULT_LOG_FILENAME");
    public final static String DETALLE_DEUDAS_TE_AVISA = getProp("DETALLE_DEUDAS_TE_AVISA");
    public final static String DESC_DETALLE_DEUDAS_TE_AVISA = getProp("DESC_DETALLE_DEUDAS_TE_AVISA");
    
    public final static String URL_WSDL_EMMA = getProp("URL_WSDL_EMMA");
    
    public final static String TRANSBANK_CHECK_MAC = getProp("TRANSBANK_CHECK_MAC");
    public final static String TRANSBANK_ERROR_PAGE = getProp("TRANSBANK_ERROR_PAGE");
    public final static String TRANSBANK_LOG_FILENAME = getProp("TRANSBANK_LOG_FILENAME");
    public final static String TRANSBANK_PAY_PAGE = getProp("TRANSBANK_PAY_PAGE");
    public final static String TRANSBANK_SUCCESS_PAGE = getProp("TRANSBANK_SUCCESS_PAGE");
    public final static String TRANSBANK_TRANSACTION_TYPE = getProp("TRANSBANK_TRANSACTION_TYPE");
    
    public final static Integer COST_CENTER = Integer.valueOf(getProp("COST_CENTER"));
    public final static Integer COST_CENTER_TRANSBANK = Integer.valueOf(getProp("COST_CENTER_TRANSBANK"));
    public final static Integer COST_CENTER_SANTANDER = Integer.valueOf(getProp("COST_CENTER_SANTANDER"));
    public final static Integer COST_CENTER_SERVIPAG = Integer.valueOf(getProp("COST_CENTER_SERVIPAG"));
    
    public final static Integer ORDER_CODE_ANNULLED = Integer.valueOf(getProp("ORDER_CODE_ANNULLED"));
    public final static Integer ORDER_CODE_PAID = Integer.valueOf(getProp("ORDER_CODE_PAID"));
    
    public final static String INVALID_SERVICE_CODE = getProp("INVALID_SERVICE_CODE");
    public final static String INVOICE_WS_URL = getProp("INVOICE_WS_URL");
    public final static String INVOICE_WS_USER = getProp("INVOICE_WS_USER");
    public final static String INVOICE_WS_PASSWD = getProp("INVOICE_WS_PASSWD");
    public final static String INVOICE_COST_CENTER_ACTECO = getProp("INVOICE_COST_CENTER_ACTECO");
    public final static String INVOICE_COST_CENTER_ACTIVITY = getProp("INVOICE_COST_CENTER_ACTIVITY");
    public final static String INVOICE_COST_CENTER_CITY = getProp("INVOICE_COST_CENTER_CITY");
    public final static String INVOICE_COST_CENTER_COMMUNE = getProp("INVOICE_COST_CENTER_COMMUNE");
    public final static String INVOICE_COST_CENTER_NAME = getProp("INVOICE_COST_CENTER_NAME");
    public final static String INVOICE_COST_CENTER_RUT = getProp("INVOICE_COST_CENTER_RUT");
    public final static String INVOICE_COST_CENTER_RUT_ONLY = getProp("INVOICE_COST_CENTER_RUT_ONLY");
    public final static String INVOICE_COST_CENTER_STREET_ADDRESS = getProp("INVOICE_COST_CENTER_STREET_ADDRESS");
    public final static String INVOICE_COST_CENTER_SYMBOL = getProp("INVOICE_COST_CENTER_SYMBOL");
    public final static String INVOICE_DOCUMENT_TYPE = getProp("INVOICE_DOCUMENT_TYPE");
    public final static String INVOICE_DOCUMENT_TYPE_INT = getProp("INVOICE_DOCUMENT_TYPE_INT");
    public final static String INVOICE_FILENAME_EXTENSION = getProp("INVOICE_FILENAME_EXTENSION");
    public final static String INVOICE_FILENAME_PATH = getProp("INVOICE_FILENAME_PATH");
    public final static String INVOICE_PAY_ACCOUNT_TYPE = getProp("INVOICE_PAY_ACCOUNT_TYPE");
    public final static String INVOICE_PAYMENT_METHOD = getProp("INVOICE_PAYMENT_METHOD");
    public final static String INVOICE_VERSION = getProp("INVOICE_VERSION");
    public final static String DIR_INVOICES = getProp("DIR_INVOICES");
    public final static String DIR_INVOICES_PDF = getProp("DIR_INVOICES_PDF");
    
    public final static String SANTANDER_COMMERCE_ID = getProp("SANTANDER_COMMERCE_ID");
    public final static String SANTANDER_EMISOR_COMPANY = getProp("SANTANDER_EMISOR_COMPANY");
    public final static String SANTANDER_LOG_FILENAME = getProp("SANTANDER_LOG_FILENAME");
    public final static Integer SANTANDER_TIMEOUT_SECS = Integer.valueOf(getProp("SANTANDER_TIMEOUT_SECS"));
    public final static Integer SANTANDER_INTERVAL_SECS = Integer.valueOf(getProp("SANTANDER_INTERVAL_SECS"));
    public final static String SANTANDER_SRVREC = getProp("SANTANDER_SRVREC");
    public final static String SANTANDER_CGI = getProp("SANTANDER_CGI");
    public final static String SERVICE_WITHOUT_DESCRIPTION = getProp("SERVICE_WITHOUT_DESCRIPTION");
    public final static String SYSTEM_USER = getProp("SYSTEM_USER");
    
    public final static String PAYMENT_COMPANY_TRANSBANK = getProp("PAYMENT_COMPANY_TRANSBANK");
    public final static String PAYMENT_COMPANY_SANTANDER = getProp("PAYMENT_COMPANY_SANTANDER");
    
    public final static Integer SERVICIO_CONTRATO = Integer.valueOf(getProp("SERVICIO_CONTRATO"));
    public final static Integer SERVICIO_RENOVAR = Integer.valueOf(getProp("SERVICIO_RENOVAR"));
    public final static Integer SERVICIO_UPGRADE = Integer.valueOf(getProp("SERVICIO_UPGRADE"));
    public final static Integer SERVICIO_CARTOLA = Integer.valueOf(getProp("SERVICIO_CARTOLA"));
    public final static Integer  SERVICIO_CERTIFICADO = Integer.valueOf(getProp("SERVICIO_CERTIFICADO"));
    
    public final static String DIR_CERTIFICATES = getProp("DIR_CERTIFICATES");
    public final static String DIR_CERTIFICATES_TEMP = getProp("DIR_CERTIFICATES_TEMP");
    public final static String DIR_FIRMAS = getProp("DIR_FIRMAS");
    public final static String CERTIFICATE_NAME_COAC = getProp("CERT_COAC");
    public final static String CERTIFICATE_NAME_LP = getProp("CERT_LP");
    public final static String CERTIFICATE_NAME_BL = getProp("CERT_BL");
    public final static String CERTIFICATE_NAME_DI = getProp("CERT_DI");
    
    public final static String URL_BIC = getProp("URL_BIC");
    public final static String CERT_SERVLET_VALIDA = getProp("CERT_SERVLET_VALIDA");
    
    public final static String MAIL_JNDI = getProp("MAIL_JNDI");
    public final static String MAIL_USER_KEY = getProp("MAIL_USER_KEY");
    public final static String MAIL_PASSWORD_KEY = getProp("MAIL_PASSWORD_KEY");
    public final static String MAIL_CONTACT_RECEPTOR = getProp("MAIL_CONTACT_RECEPTOR"); 
    public final static String MAIL_CONTACT_MESSAGE_BODY_TEMPLATE = getProp("MAIL_CONTACT_MESSAGE_BODY_TEMPLATE");
    public final static String MAIL_CONTACT_SUBJECT = getProp("MAIL_CONTACT_SUBJECT");
    public final static String CONTACTO_MAIL_SENT_OK = getProp("CONTACTO_MAIL_SENT_OK");
    public final static String CONTACTO_MAIL_SENT_ERROR = getProp("CONTACTO_MAIL_SENT_ERROR");
    public final static String MAIL_CONTACT_ACL_ESP_RECEPTOR = getProp("MAIL_CONTACT_ACL_ESP_RECEPTOR");
    public final static String MAIL_CONTACT_ENC_DATOS_RECEPTOR = getProp("MAIL_CONTACT_ENC_DATOS_RECEPTOR");
    public final static String MAIL_CONTACT_CLIENT_ADDRESS = getProp("MAIL_CONTACT_CLIENT_ADDRESS");
    
    public final static String URL_SITIO_PUBLICO = getProp("URL_SITIO_PUBLICO");
    public final static String URL_COMPRACERT_NAT = getProp("URL_COMPRACERT_NAT");
    public final static String URL_COMPRACERT_EMP = getProp("URL_COMPRACERT_EMP");
    public final static Integer TIPOMAIL_RECEP = Integer.valueOf(getProp("TIPOMAIL_RECEP"));
    public final static Integer TIPOMAIL_RECEP_PER = Integer.valueOf(getProp("TIPOMAIL_RECEP_PER"));
    public final static Integer TIPOMAIL_RECEP_EMP = Integer.valueOf(getProp("TIPOMAIL_RECEP_EMP"));
    public final static Integer TIPOMAIL_PUBBIC = Integer.valueOf(getProp("TIPOMAIL_PUBBIC"));
    public final static Integer TIPOMAIL_PUBBIC_PER = Integer.valueOf(getProp("TIPOMAIL_PUBBIC_PER"));
    public final static Integer TIPOMAIL_PUBBIC_EMP = Integer.valueOf(getProp("TIPOMAIL_PUBBIC_EMP"));
    public final static Integer TIPOMAIL_PUBMOL = Integer.valueOf(getProp("TIPOMAIL_PUBMOL"));
    public final static Integer TIPOMAIL_PUBMOL_PER = Integer.valueOf(getProp("TIPOMAIL_PUBMOL_PER"));
    public final static Integer TIPOMAIL_PUBMOL_EMP = Integer.valueOf(getProp("TIPOMAIL_PUBMOL_EMP"));
    
        
    public Constants() {
        super();
    }
        
    public static String getProp(String key) {

        String value = prop.getProperty(key);
        if (null == value) {
            value = "";
        }
        return value;
    } 
}
