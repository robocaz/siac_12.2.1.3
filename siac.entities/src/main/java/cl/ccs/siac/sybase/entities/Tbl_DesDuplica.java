package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DesDuplica database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_DesDuplica.findAll", query="SELECT t FROM Tbl_DesDuplica t")
public class Tbl_DesDuplica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	public Tbl_DesDuplica() {
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

}