package cl.ccs.siac.persona;

import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ResponseDTO;

public interface PersonaBusiness {
	public PersonaDTO obtener(String rut);
	public PersonaDTO actualizar(PersonaDTO personaDTO);
	public ResponseDTO verificar(String rut);
	public PersonaDTO getNombre(String rut, Long correlativo);
}
