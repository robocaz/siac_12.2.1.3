package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tmp_estboletin database table.
 * 
 */
@Entity
@Table(name="tmp_estboletin")
@NamedQuery(name="TmpEstboletin.findAll", query="SELECT t FROM TmpEstboletin t")
public class TmpEstboletin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Cantidad")
	private int cantidad;

	@Column(name="CodEmisor")
	private String codEmisor;

	@Column(name="FecPublic")
	private Timestamp fecPublic;

	@Column(name="Monto")
	private BigDecimal monto;

	@Column(name="NroBoletin")
	private short nroBoletin;

	@Column(name="TipoDoc")
	private String tipoDoc;

	@Column(name="TipoEmisor")
	private String tipoEmisor;

	@Column(name="TipoPersona")
	private String tipoPersona;

	public TmpEstboletin() {
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getCodEmisor() {
		return this.codEmisor;
	}

	public void setCodEmisor(String codEmisor) {
		this.codEmisor = codEmisor;
	}

	public Timestamp getFecPublic() {
		return this.fecPublic;
	}

	public void setFecPublic(Timestamp fecPublic) {
		this.fecPublic = fecPublic;
	}

	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public short getNroBoletin() {
		return this.nroBoletin;
	}

	public void setNroBoletin(short nroBoletin) {
		this.nroBoletin = nroBoletin;
	}

	public String getTipoDoc() {
		return this.tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getTipoEmisor() {
		return this.tipoEmisor;
	}

	public void setTipoEmisor(String tipoEmisor) {
		this.tipoEmisor = tipoEmisor;
	}

	public String getTipoPersona() {
		return this.tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

}