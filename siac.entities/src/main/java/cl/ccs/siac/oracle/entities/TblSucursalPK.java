package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TBL_SUCURSAL database table.
 * 
 */
@Embeddable
public class TblSucursalPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_TIPOEMISOR", insertable=false, updatable=false)
	private String fldTipoemisor;

	@Column(name="FLD_CODEMISOR", insertable=false, updatable=false)
	private String fldCodemisor;

	@Column(name="FLD_CODSUCURSAL")
	private String fldCodsucursal;

	public TblSucursalPK() {
	}
	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}
	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}
	public String getFldCodemisor() {
		return this.fldCodemisor;
	}
	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}
	public String getFldCodsucursal() {
		return this.fldCodsucursal;
	}
	public void setFldCodsucursal(String fldCodsucursal) {
		this.fldCodsucursal = fldCodsucursal;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TblSucursalPK)) {
			return false;
		}
		TblSucursalPK castOther = (TblSucursalPK)other;
		return 
			this.fldTipoemisor.equals(castOther.fldTipoemisor)
			&& this.fldCodemisor.equals(castOther.fldCodemisor)
			&& this.fldCodsucursal.equals(castOther.fldCodsucursal);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fldTipoemisor.hashCode();
		hash = hash * prime + this.fldCodemisor.hashCode();
		hash = hash * prime + this.fldCodsucursal.hashCode();
		
		return hash;
	}
}