package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EnviosProtPB_Vig database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EnviosProtPB_Vig.findAll", query="SELECT t FROM Tbl_EnviosProtPB_Vig t")
public class Tbl_EnviosProtPB_Vig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrEnvio")
	private BigDecimal fld_CorrEnvio;

	@Column(name="Fld_CorrOProc")
	private BigDecimal fld_CorrOProc;

	@Column(name="Fld_CorrPerRecepProt")
	private BigDecimal fld_CorrPerRecepProt;

	@Column(name="Fld_FecFlag_1")
	private Timestamp fld_FecFlag_1;

	@Column(name="Fld_FecFlag_2")
	private Timestamp fld_FecFlag_2;

	@Column(name="Fld_FecFlag_3")
	private Timestamp fld_FecFlag_3;

	@Column(name="Fld_FecFlag_4")
	private Timestamp fld_FecFlag_4;

	@Column(name="Fld_FecFlag_5")
	private Timestamp fld_FecFlag_5;

	@Column(name="Fld_FecFlag_6")
	private Timestamp fld_FecFlag_6;

	@Column(name="Fld_FecFlag_7")
	private Timestamp fld_FecFlag_7;

	@Column(name="Fld_FecFlag_8")
	private Timestamp fld_FecFlag_8;

	@Column(name="Fld_FecFlag_AsigEnvio")
	private Timestamp fld_FecFlag_AsigEnvio;

	@Column(name="Fld_FecFlag_Bol")
	private Timestamp fld_FecFlag_Bol;

	@Column(name="Fld_FecFlag_Cierre")
	private Timestamp fld_FecFlag_Cierre;

	@Column(name="Fld_FecFlag_DespOP")
	private Timestamp fld_FecFlag_DespOP;

	@Column(name="Fld_FecFlag_EjecOP")
	private Timestamp fld_FecFlag_EjecOP;

	@Column(name="Fld_FecFlag_GenOP")
	private Timestamp fld_FecFlag_GenOP;

	@Column(name="Fld_FecFlag_LP")
	private Timestamp fld_FecFlag_LP;

	@Column(name="Fld_FecFlag_RecepADestOP")
	private Timestamp fld_FecFlag_RecepADestOP;

	@Column(name="Fld_FecFlag_RecepAOrigOP")
	private Timestamp fld_FecFlag_RecepAOrigOP;

	@Column(name="Fld_FecRecepcion")
	private Timestamp fld_FecRecepcion;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_Flag_5")
	private boolean fld_Flag_5;

	@Column(name="Fld_Flag_6")
	private boolean fld_Flag_6;

	@Column(name="Fld_Flag_7")
	private boolean fld_Flag_7;

	@Column(name="Fld_Flag_8")
	private boolean fld_Flag_8;

	@Column(name="Fld_Flag_AsigEnvio")
	private boolean fld_Flag_AsigEnvio;

	@Column(name="Fld_Flag_Bol")
	private boolean fld_Flag_Bol;

	@Column(name="Fld_Flag_Cierre")
	private byte fld_Flag_Cierre;

	@Column(name="Fld_Flag_DespOP")
	private boolean fld_Flag_DespOP;

	@Column(name="Fld_Flag_EjecOP")
	private boolean fld_Flag_EjecOP;

	@Column(name="Fld_Flag_GenOp")
	private boolean fld_Flag_GenOp;

	@Column(name="Fld_Flag_LP")
	private boolean fld_Flag_LP;

	@Column(name="Fld_Flag_RecepADestOP")
	private boolean fld_Flag_RecepADestOP;

	@Column(name="Fld_Flag_RecepAOrigOP")
	private boolean fld_Flag_RecepAOrigOP;

	@Column(name="Fld_Flag_Rechazo")
	private boolean fld_Flag_Rechazo;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_NroBoletinLP")
	private short fld_NroBoletinLP;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tbl_EnviosProtPB_Vig() {
	}

	public BigDecimal getFld_CorrEnvio() {
		return this.fld_CorrEnvio;
	}

	public void setFld_CorrEnvio(BigDecimal fld_CorrEnvio) {
		this.fld_CorrEnvio = fld_CorrEnvio;
	}

	public BigDecimal getFld_CorrOProc() {
		return this.fld_CorrOProc;
	}

	public void setFld_CorrOProc(BigDecimal fld_CorrOProc) {
		this.fld_CorrOProc = fld_CorrOProc;
	}

	public BigDecimal getFld_CorrPerRecepProt() {
		return this.fld_CorrPerRecepProt;
	}

	public void setFld_CorrPerRecepProt(BigDecimal fld_CorrPerRecepProt) {
		this.fld_CorrPerRecepProt = fld_CorrPerRecepProt;
	}

	public Timestamp getFld_FecFlag_1() {
		return this.fld_FecFlag_1;
	}

	public void setFld_FecFlag_1(Timestamp fld_FecFlag_1) {
		this.fld_FecFlag_1 = fld_FecFlag_1;
	}

	public Timestamp getFld_FecFlag_2() {
		return this.fld_FecFlag_2;
	}

	public void setFld_FecFlag_2(Timestamp fld_FecFlag_2) {
		this.fld_FecFlag_2 = fld_FecFlag_2;
	}

	public Timestamp getFld_FecFlag_3() {
		return this.fld_FecFlag_3;
	}

	public void setFld_FecFlag_3(Timestamp fld_FecFlag_3) {
		this.fld_FecFlag_3 = fld_FecFlag_3;
	}

	public Timestamp getFld_FecFlag_4() {
		return this.fld_FecFlag_4;
	}

	public void setFld_FecFlag_4(Timestamp fld_FecFlag_4) {
		this.fld_FecFlag_4 = fld_FecFlag_4;
	}

	public Timestamp getFld_FecFlag_5() {
		return this.fld_FecFlag_5;
	}

	public void setFld_FecFlag_5(Timestamp fld_FecFlag_5) {
		this.fld_FecFlag_5 = fld_FecFlag_5;
	}

	public Timestamp getFld_FecFlag_6() {
		return this.fld_FecFlag_6;
	}

	public void setFld_FecFlag_6(Timestamp fld_FecFlag_6) {
		this.fld_FecFlag_6 = fld_FecFlag_6;
	}

	public Timestamp getFld_FecFlag_7() {
		return this.fld_FecFlag_7;
	}

	public void setFld_FecFlag_7(Timestamp fld_FecFlag_7) {
		this.fld_FecFlag_7 = fld_FecFlag_7;
	}

	public Timestamp getFld_FecFlag_8() {
		return this.fld_FecFlag_8;
	}

	public void setFld_FecFlag_8(Timestamp fld_FecFlag_8) {
		this.fld_FecFlag_8 = fld_FecFlag_8;
	}

	public Timestamp getFld_FecFlag_AsigEnvio() {
		return this.fld_FecFlag_AsigEnvio;
	}

	public void setFld_FecFlag_AsigEnvio(Timestamp fld_FecFlag_AsigEnvio) {
		this.fld_FecFlag_AsigEnvio = fld_FecFlag_AsigEnvio;
	}

	public Timestamp getFld_FecFlag_Bol() {
		return this.fld_FecFlag_Bol;
	}

	public void setFld_FecFlag_Bol(Timestamp fld_FecFlag_Bol) {
		this.fld_FecFlag_Bol = fld_FecFlag_Bol;
	}

	public Timestamp getFld_FecFlag_Cierre() {
		return this.fld_FecFlag_Cierre;
	}

	public void setFld_FecFlag_Cierre(Timestamp fld_FecFlag_Cierre) {
		this.fld_FecFlag_Cierre = fld_FecFlag_Cierre;
	}

	public Timestamp getFld_FecFlag_DespOP() {
		return this.fld_FecFlag_DespOP;
	}

	public void setFld_FecFlag_DespOP(Timestamp fld_FecFlag_DespOP) {
		this.fld_FecFlag_DespOP = fld_FecFlag_DespOP;
	}

	public Timestamp getFld_FecFlag_EjecOP() {
		return this.fld_FecFlag_EjecOP;
	}

	public void setFld_FecFlag_EjecOP(Timestamp fld_FecFlag_EjecOP) {
		this.fld_FecFlag_EjecOP = fld_FecFlag_EjecOP;
	}

	public Timestamp getFld_FecFlag_GenOP() {
		return this.fld_FecFlag_GenOP;
	}

	public void setFld_FecFlag_GenOP(Timestamp fld_FecFlag_GenOP) {
		this.fld_FecFlag_GenOP = fld_FecFlag_GenOP;
	}

	public Timestamp getFld_FecFlag_LP() {
		return this.fld_FecFlag_LP;
	}

	public void setFld_FecFlag_LP(Timestamp fld_FecFlag_LP) {
		this.fld_FecFlag_LP = fld_FecFlag_LP;
	}

	public Timestamp getFld_FecFlag_RecepADestOP() {
		return this.fld_FecFlag_RecepADestOP;
	}

	public void setFld_FecFlag_RecepADestOP(Timestamp fld_FecFlag_RecepADestOP) {
		this.fld_FecFlag_RecepADestOP = fld_FecFlag_RecepADestOP;
	}

	public Timestamp getFld_FecFlag_RecepAOrigOP() {
		return this.fld_FecFlag_RecepAOrigOP;
	}

	public void setFld_FecFlag_RecepAOrigOP(Timestamp fld_FecFlag_RecepAOrigOP) {
		this.fld_FecFlag_RecepAOrigOP = fld_FecFlag_RecepAOrigOP;
	}

	public Timestamp getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(Timestamp fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_Flag_5() {
		return this.fld_Flag_5;
	}

	public void setFld_Flag_5(boolean fld_Flag_5) {
		this.fld_Flag_5 = fld_Flag_5;
	}

	public boolean getFld_Flag_6() {
		return this.fld_Flag_6;
	}

	public void setFld_Flag_6(boolean fld_Flag_6) {
		this.fld_Flag_6 = fld_Flag_6;
	}

	public boolean getFld_Flag_7() {
		return this.fld_Flag_7;
	}

	public void setFld_Flag_7(boolean fld_Flag_7) {
		this.fld_Flag_7 = fld_Flag_7;
	}

	public boolean getFld_Flag_8() {
		return this.fld_Flag_8;
	}

	public void setFld_Flag_8(boolean fld_Flag_8) {
		this.fld_Flag_8 = fld_Flag_8;
	}

	public boolean getFld_Flag_AsigEnvio() {
		return this.fld_Flag_AsigEnvio;
	}

	public void setFld_Flag_AsigEnvio(boolean fld_Flag_AsigEnvio) {
		this.fld_Flag_AsigEnvio = fld_Flag_AsigEnvio;
	}

	public boolean getFld_Flag_Bol() {
		return this.fld_Flag_Bol;
	}

	public void setFld_Flag_Bol(boolean fld_Flag_Bol) {
		this.fld_Flag_Bol = fld_Flag_Bol;
	}

	public byte getFld_Flag_Cierre() {
		return this.fld_Flag_Cierre;
	}

	public void setFld_Flag_Cierre(byte fld_Flag_Cierre) {
		this.fld_Flag_Cierre = fld_Flag_Cierre;
	}

	public boolean getFld_Flag_DespOP() {
		return this.fld_Flag_DespOP;
	}

	public void setFld_Flag_DespOP(boolean fld_Flag_DespOP) {
		this.fld_Flag_DespOP = fld_Flag_DespOP;
	}

	public boolean getFld_Flag_EjecOP() {
		return this.fld_Flag_EjecOP;
	}

	public void setFld_Flag_EjecOP(boolean fld_Flag_EjecOP) {
		this.fld_Flag_EjecOP = fld_Flag_EjecOP;
	}

	public boolean getFld_Flag_GenOp() {
		return this.fld_Flag_GenOp;
	}

	public void setFld_Flag_GenOp(boolean fld_Flag_GenOp) {
		this.fld_Flag_GenOp = fld_Flag_GenOp;
	}

	public boolean getFld_Flag_LP() {
		return this.fld_Flag_LP;
	}

	public void setFld_Flag_LP(boolean fld_Flag_LP) {
		this.fld_Flag_LP = fld_Flag_LP;
	}

	public boolean getFld_Flag_RecepADestOP() {
		return this.fld_Flag_RecepADestOP;
	}

	public void setFld_Flag_RecepADestOP(boolean fld_Flag_RecepADestOP) {
		this.fld_Flag_RecepADestOP = fld_Flag_RecepADestOP;
	}

	public boolean getFld_Flag_RecepAOrigOP() {
		return this.fld_Flag_RecepAOrigOP;
	}

	public void setFld_Flag_RecepAOrigOP(boolean fld_Flag_RecepAOrigOP) {
		this.fld_Flag_RecepAOrigOP = fld_Flag_RecepAOrigOP;
	}

	public boolean getFld_Flag_Rechazo() {
		return this.fld_Flag_Rechazo;
	}

	public void setFld_Flag_Rechazo(boolean fld_Flag_Rechazo) {
		this.fld_Flag_Rechazo = fld_Flag_Rechazo;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_NroBoletinLP() {
		return this.fld_NroBoletinLP;
	}

	public void setFld_NroBoletinLP(short fld_NroBoletinLP) {
		this.fld_NroBoletinLP = fld_NroBoletinLP;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}