package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the HSS_COMPONENT_PROPERTY_VALUES database table.
 * 
 */
@Entity
@Table(name="HSS_COMPONENT_PROPERTY_VALUES")
@NamedQuery(name="HssComponentPropertyValue.findAll", query="SELECT h FROM HssComponentPropertyValue h")
public class HssComponentPropertyValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="COMPONENT_ID")
	private String componentId;

	@Column(name="PROPERTY_NAME")
	private String propertyName;

	@Column(name="PROPERTY_VALUE")
	private String propertyValue;

	public HssComponentPropertyValue() {
	}

	public String getComponentId() {
		return this.componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return this.propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

}