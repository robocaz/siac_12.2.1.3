package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_UsuarioIPAclOnline database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_UsuarioIPAclOnline.findAll", query="SELECT t FROM Tbl_UsuarioIPAclOnline t")
public class Tbl_UsuarioIPAclOnline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_IpAutoriz")
	private String fld_IpAutoriz;

	public Tbl_UsuarioIPAclOnline() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_IpAutoriz() {
		return this.fld_IpAutoriz;
	}

	public void setFld_IpAutoriz(String fld_IpAutoriz) {
		this.fld_IpAutoriz = fld_IpAutoriz;
	}

}