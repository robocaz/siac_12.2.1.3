package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutFlagProtVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutFlagProtVCE.findAll", query="SELECT t FROM Tbl_RutFlagProtVCE t")
public class Tbl_RutFlagProtVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_ExisteProtesto")
	private byte fld_ExisteProtesto;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_RutFlagProtVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public byte getFld_ExisteProtesto() {
		return this.fld_ExisteProtesto;
	}

	public void setFld_ExisteProtesto(byte fld_ExisteProtesto) {
		this.fld_ExisteProtesto = fld_ExisteProtesto;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}