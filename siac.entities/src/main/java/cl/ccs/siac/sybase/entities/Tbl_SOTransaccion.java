package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_SOTransaccion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SOTransaccion.findAll", query="SELECT t FROM Tbl_SOTransaccion t")
public class Tbl_SOTransaccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_BoletaFactura")
	private String fld_BoletaFactura;

	@Column(name="Fld_CodBanco")
	private String fld_CodBanco;

	@Column(name="Fld_CodComuna")
	private short fld_CodComuna;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrCajaSO")
	private BigDecimal fld_CorrCajaSO;

	@Column(name="Fld_CorrOffline")
	private BigDecimal fld_CorrOffline;

	@Column(name="Fld_CorrTransac")
	private BigDecimal fld_CorrTransac;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_NroBoletaFactura")
	private int fld_NroBoletaFactura;

	@Column(name="Fld_NroCheque")
	private String fld_NroCheque;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_Telefono")
	private String fld_Telefono;

	@Column(name="Fld_TipoPago")
	private String fld_TipoPago;

	@Column(name="Fld_TotalCalculado")
	private BigDecimal fld_TotalCalculado;

	@Column(name="Fld_TotalPagado")
	private BigDecimal fld_TotalPagado;

	@Column(name="Fld_TotMovimientos")
	private int fld_TotMovimientos;

	public Tbl_SOTransaccion() {
	}

	public String getFld_BoletaFactura() {
		return this.fld_BoletaFactura;
	}

	public void setFld_BoletaFactura(String fld_BoletaFactura) {
		this.fld_BoletaFactura = fld_BoletaFactura;
	}

	public String getFld_CodBanco() {
		return this.fld_CodBanco;
	}

	public void setFld_CodBanco(String fld_CodBanco) {
		this.fld_CodBanco = fld_CodBanco;
	}

	public short getFld_CodComuna() {
		return this.fld_CodComuna;
	}

	public void setFld_CodComuna(short fld_CodComuna) {
		this.fld_CodComuna = fld_CodComuna;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrCajaSO() {
		return this.fld_CorrCajaSO;
	}

	public void setFld_CorrCajaSO(BigDecimal fld_CorrCajaSO) {
		this.fld_CorrCajaSO = fld_CorrCajaSO;
	}

	public BigDecimal getFld_CorrOffline() {
		return this.fld_CorrOffline;
	}

	public void setFld_CorrOffline(BigDecimal fld_CorrOffline) {
		this.fld_CorrOffline = fld_CorrOffline;
	}

	public BigDecimal getFld_CorrTransac() {
		return this.fld_CorrTransac;
	}

	public void setFld_CorrTransac(BigDecimal fld_CorrTransac) {
		this.fld_CorrTransac = fld_CorrTransac;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public int getFld_NroBoletaFactura() {
		return this.fld_NroBoletaFactura;
	}

	public void setFld_NroBoletaFactura(int fld_NroBoletaFactura) {
		this.fld_NroBoletaFactura = fld_NroBoletaFactura;
	}

	public String getFld_NroCheque() {
		return this.fld_NroCheque;
	}

	public void setFld_NroCheque(String fld_NroCheque) {
		this.fld_NroCheque = fld_NroCheque;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public String getFld_Telefono() {
		return this.fld_Telefono;
	}

	public void setFld_Telefono(String fld_Telefono) {
		this.fld_Telefono = fld_Telefono;
	}

	public String getFld_TipoPago() {
		return this.fld_TipoPago;
	}

	public void setFld_TipoPago(String fld_TipoPago) {
		this.fld_TipoPago = fld_TipoPago;
	}

	public BigDecimal getFld_TotalCalculado() {
		return this.fld_TotalCalculado;
	}

	public void setFld_TotalCalculado(BigDecimal fld_TotalCalculado) {
		this.fld_TotalCalculado = fld_TotalCalculado;
	}

	public BigDecimal getFld_TotalPagado() {
		return this.fld_TotalPagado;
	}

	public void setFld_TotalPagado(BigDecimal fld_TotalPagado) {
		this.fld_TotalPagado = fld_TotalPagado;
	}

	public int getFld_TotMovimientos() {
		return this.fld_TotMovimientos;
	}

	public void setFld_TotMovimientos(int fld_TotMovimientos) {
		this.fld_TotMovimientos = fld_TotMovimientos;
	}

}