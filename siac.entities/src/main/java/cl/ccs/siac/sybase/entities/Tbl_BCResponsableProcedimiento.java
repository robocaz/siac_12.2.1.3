package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCResponsableProcedimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCResponsableProcedimiento.findAll", query="SELECT t FROM Tbl_BCResponsableProcedimiento t")
public class Tbl_BCResponsableProcedimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_DocResol")
	private String fld_DocResol;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_Excluido")
	private boolean fld_Excluido;

	@Column(name="Fld_FechaResol")
	private Timestamp fld_FechaResol;

	@Column(name="Fld_Jurisdiccion")
	private String fld_Jurisdiccion;

	@Column(name="Fld_NombreResponsableProc")
	private String fld_NombreResponsableProc;

	@Column(name="Fld_NroResolIncorporacion")
	private BigDecimal fld_NroResolIncorporacion;

	@Column(name="Fld_Profesion")
	private String fld_Profesion;

	@Column(name="Fld_RutResponsableProc")
	private String fld_RutResponsableProc;

	@Column(name="Fld_Suspendido")
	private boolean fld_Suspendido;

	@Column(name="Fld_TelefonoFijo")
	private String fld_TelefonoFijo;

	@Column(name="Fld_TelefonoMovil")
	private String fld_TelefonoMovil;

	@Column(name="Fld_TipoResponsableProc")
	private String fld_TipoResponsableProc;

	public Tbl_BCResponsableProcedimiento() {
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_DocResol() {
		return this.fld_DocResol;
	}

	public void setFld_DocResol(String fld_DocResol) {
		this.fld_DocResol = fld_DocResol;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public boolean getFld_Excluido() {
		return this.fld_Excluido;
	}

	public void setFld_Excluido(boolean fld_Excluido) {
		this.fld_Excluido = fld_Excluido;
	}

	public Timestamp getFld_FechaResol() {
		return this.fld_FechaResol;
	}

	public void setFld_FechaResol(Timestamp fld_FechaResol) {
		this.fld_FechaResol = fld_FechaResol;
	}

	public String getFld_Jurisdiccion() {
		return this.fld_Jurisdiccion;
	}

	public void setFld_Jurisdiccion(String fld_Jurisdiccion) {
		this.fld_Jurisdiccion = fld_Jurisdiccion;
	}

	public String getFld_NombreResponsableProc() {
		return this.fld_NombreResponsableProc;
	}

	public void setFld_NombreResponsableProc(String fld_NombreResponsableProc) {
		this.fld_NombreResponsableProc = fld_NombreResponsableProc;
	}

	public BigDecimal getFld_NroResolIncorporacion() {
		return this.fld_NroResolIncorporacion;
	}

	public void setFld_NroResolIncorporacion(BigDecimal fld_NroResolIncorporacion) {
		this.fld_NroResolIncorporacion = fld_NroResolIncorporacion;
	}

	public String getFld_Profesion() {
		return this.fld_Profesion;
	}

	public void setFld_Profesion(String fld_Profesion) {
		this.fld_Profesion = fld_Profesion;
	}

	public String getFld_RutResponsableProc() {
		return this.fld_RutResponsableProc;
	}

	public void setFld_RutResponsableProc(String fld_RutResponsableProc) {
		this.fld_RutResponsableProc = fld_RutResponsableProc;
	}

	public boolean getFld_Suspendido() {
		return this.fld_Suspendido;
	}

	public void setFld_Suspendido(boolean fld_Suspendido) {
		this.fld_Suspendido = fld_Suspendido;
	}

	public String getFld_TelefonoFijo() {
		return this.fld_TelefonoFijo;
	}

	public void setFld_TelefonoFijo(String fld_TelefonoFijo) {
		this.fld_TelefonoFijo = fld_TelefonoFijo;
	}

	public String getFld_TelefonoMovil() {
		return this.fld_TelefonoMovil;
	}

	public void setFld_TelefonoMovil(String fld_TelefonoMovil) {
		this.fld_TelefonoMovil = fld_TelefonoMovil;
	}

	public String getFld_TipoResponsableProc() {
		return this.fld_TipoResponsableProc;
	}

	public void setFld_TipoResponsableProc(String fld_TipoResponsableProc) {
		this.fld_TipoResponsableProc = fld_TipoResponsableProc;
	}

}