package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCDetPublicacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCDetPublicacion.findAll", query="SELECT t FROM Tbl_BCDetPublicacion t")
public class Tbl_BCDetPublicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private byte fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrDetPublicacion_Id")
	private BigDecimal fld_CorrDetPublicacion_Id;

	@Column(name="Fld_CorrProcedimiento")
	private BigDecimal fld_CorrProcedimiento;

	@Column(name="Fld_DocPublicado")
	private String fld_DocPublicado;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_FechaPublicacion")
	private Timestamp fld_FechaPublicacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_NomPublicacion")
	private String fld_NomPublicacion;

	@Column(name="Fld_RutResponsableProc")
	private String fld_RutResponsableProc;

	@Column(name="Fld_TipoResponsableProc")
	private String fld_TipoResponsableProc;

	public Tbl_BCDetPublicacion() {
	}

	public byte getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(byte fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrDetPublicacion_Id() {
		return this.fld_CorrDetPublicacion_Id;
	}

	public void setFld_CorrDetPublicacion_Id(BigDecimal fld_CorrDetPublicacion_Id) {
		this.fld_CorrDetPublicacion_Id = fld_CorrDetPublicacion_Id;
	}

	public BigDecimal getFld_CorrProcedimiento() {
		return this.fld_CorrProcedimiento;
	}

	public void setFld_CorrProcedimiento(BigDecimal fld_CorrProcedimiento) {
		this.fld_CorrProcedimiento = fld_CorrProcedimiento;
	}

	public String getFld_DocPublicado() {
		return this.fld_DocPublicado;
	}

	public void setFld_DocPublicado(String fld_DocPublicado) {
		this.fld_DocPublicado = fld_DocPublicado;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public Timestamp getFld_FechaPublicacion() {
		return this.fld_FechaPublicacion;
	}

	public void setFld_FechaPublicacion(Timestamp fld_FechaPublicacion) {
		this.fld_FechaPublicacion = fld_FechaPublicacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_NomPublicacion() {
		return this.fld_NomPublicacion;
	}

	public void setFld_NomPublicacion(String fld_NomPublicacion) {
		this.fld_NomPublicacion = fld_NomPublicacion;
	}

	public String getFld_RutResponsableProc() {
		return this.fld_RutResponsableProc;
	}

	public void setFld_RutResponsableProc(String fld_RutResponsableProc) {
		this.fld_RutResponsableProc = fld_RutResponsableProc;
	}

	public String getFld_TipoResponsableProc() {
		return this.fld_TipoResponsableProc;
	}

	public void setFld_TipoResponsableProc(String fld_TipoResponsableProc) {
		this.fld_TipoResponsableProc = fld_TipoResponsableProc;
	}

}