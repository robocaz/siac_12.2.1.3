package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EmisoresFTP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EmisoresFTP.findAll", query="SELECT t FROM Tbl_EmisoresFTP t")
public class Tbl_EmisoresFTP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_NombreArchivo")
	private String fld_NombreArchivo;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoProceso")
	private String fld_TipoProceso;

	public Tbl_EmisoresFTP() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_NombreArchivo() {
		return this.fld_NombreArchivo;
	}

	public void setFld_NombreArchivo(String fld_NombreArchivo) {
		this.fld_NombreArchivo = fld_NombreArchivo;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_TipoProceso() {
		return this.fld_TipoProceso;
	}

	public void setFld_TipoProceso(String fld_TipoProceso) {
		this.fld_TipoProceso = fld_TipoProceso;
	}

}