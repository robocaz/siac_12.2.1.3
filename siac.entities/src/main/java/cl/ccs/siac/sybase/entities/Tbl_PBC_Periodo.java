package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_PBC_Periodo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PBC_Periodo.findAll", query="SELECT t FROM Tbl_PBC_Periodo t")
public class Tbl_PBC_Periodo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CantidadActual")
	private int fld_CantidadActual;

	@Column(name="Fld_Periodo")
	private int fld_Periodo;

	@Column(name="Fld_Tope")
	private int fld_Tope;

	public Tbl_PBC_Periodo() {
	}

	public int getFld_CantidadActual() {
		return this.fld_CantidadActual;
	}

	public void setFld_CantidadActual(int fld_CantidadActual) {
		this.fld_CantidadActual = fld_CantidadActual;
	}

	public int getFld_Periodo() {
		return this.fld_Periodo;
	}

	public void setFld_Periodo(int fld_Periodo) {
		this.fld_Periodo = fld_Periodo;
	}

	public int getFld_Tope() {
		return this.fld_Tope;
	}

	public void setFld_Tope(int fld_Tope) {
		this.fld_Tope = fld_Tope;
	}

}