package cl.ccs.siac.transaccion;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.TransaccionDao;
import cl.ccs.siac.utils.Utils;
import cl.exe.ccs.dto.AceptaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.TransaccionDTO;
import cl.exe.ccs.dto.TransaccionGrillaDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;

@Named("transaccionBusiness")
public class TransaccionBusinessImpl implements TransaccionBusiness {

	@Inject
	private TransaccionDao transaccionDao;
	
	@Inject
	private Utils utils;
	
	
    
	@Override
	public ResponseDTO agregarServicio(TransaccionDTO transaccionDTO) {
		
		String rutParse = "";
		if(transaccionDTO.getRutAfectado() != null && !transaccionDTO.getRutAfectado().equals("")){

			rutParse = utils.formatearRUT(transaccionDTO.getRutAfectado());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			transaccionDTO.setRutAfectado(rutParse);
		}
		
		return transaccionDao.agregarServicio(transaccionDTO);
	}

	@Override
	public ResponseListDTO<TransaccionGrillaDTO> listarServicio(Long corrCaja) {
		return transaccionDao.listarServicio(corrCaja);
	}
	
	public ResponseDTO cancelarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO){
		String rutParse = "";
		if(aceptaTransaccionDTO.getRutTramitante() != null && !aceptaTransaccionDTO.getRutTramitante().equals("")){

			rutParse = utils.formatearRUT(aceptaTransaccionDTO.getRutTramitante());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			aceptaTransaccionDTO.setRutTramitante(rutParse);
		}
		return transaccionDao.cancelarTransaccion(aceptaTransaccionDTO);
	}
	
	
	public ResponseDTO aceptarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO){
		String rutParse = "";
		if(aceptaTransaccionDTO.getRutTramitante() != null && !aceptaTransaccionDTO.getRutTramitante().equals("")){

			rutParse = utils.formatearRUT(aceptaTransaccionDTO.getRutTramitante());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			aceptaTransaccionDTO.setRutTramitante(rutParse);
		}
		return transaccionDao.aceptarTransaccion(aceptaTransaccionDTO);
	}
	
	
	public ResponseListDTO<CertificadoDTO> certificadosTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO){
		String rutParse = "";
		if(aceptaTransaccionDTO.getRutTramitante() != null && !aceptaTransaccionDTO.getRutTramitante().equals("")){

			rutParse = utils.formatearRUT(aceptaTransaccionDTO.getRutTramitante());
			rutParse = rutParse.replace(".", "");
			rutParse = String.format("%11s", rutParse).replace(' ', '0');
			
			aceptaTransaccionDTO.setRutTramitante(rutParse);
		}
		return transaccionDao.certificadosTransaccion(aceptaTransaccionDTO);
	}
	
	
	public ResponseDTO eliminarServicio(Long idCaja, Long corrMov){
		return transaccionDao.eliminarServicio(idCaja, corrMov);
	}
}
