package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the TBL_ACLHISTO database table.
 * 
 */
@Entity
@Table(name="TBL_ACLHISTO")
@NamedQuery(name="TblAclhisto.findAll", query="SELECT t FROM TblAclhisto t")
public class TblAclhisto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ACLHISTO_FLDCORRACLHISTOID_GENERATOR", sequenceName="FLD_CORRACLHISTO_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ACLHISTO_FLDCORRACLHISTOID_GENERATOR")
	@Column(name="FLD_CORRACLHISTO_ID")
	private long fldCorraclhistoId;

	@Column(name="FLD_CODSITUACION")
	private BigDecimal fldCodsituacion;

	@Column(name="FLD_FECHAACLARACION")
	private Timestamp fldFechaaclaracion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAPAGO")
	private Date fldFechapago;

	@Column(name="FLD_RUTAFECTADO")
	private String fldRutafectado;

	@Column(name="FLD_USUARIOACLARACION")
	private String fldUsuarioaclaracion;

	//bi-directional many-to-one association to TblMorhisto
	@ManyToOne
	@JoinColumn(name="FLD_CORRMOROSIDAD")
	private TblMorhisto tblMorhisto;

	public TblAclhisto() {
	}

	public long getFldCorraclhistoId() {
		return this.fldCorraclhistoId;
	}

	public void setFldCorraclhistoId(long fldCorraclhistoId) {
		this.fldCorraclhistoId = fldCorraclhistoId;
	}

	public BigDecimal getFldCodsituacion() {
		return this.fldCodsituacion;
	}

	public void setFldCodsituacion(BigDecimal fldCodsituacion) {
		this.fldCodsituacion = fldCodsituacion;
	}

	public Timestamp getFldFechaaclaracion() {
		return this.fldFechaaclaracion;
	}

	public void setFldFechaaclaracion(Timestamp fldFechaaclaracion) {
		this.fldFechaaclaracion = fldFechaaclaracion;
	}

	public Date getFldFechapago() {
		return this.fldFechapago;
	}

	public void setFldFechapago(Date fldFechapago) {
		this.fldFechapago = fldFechapago;
	}

	public String getFldRutafectado() {
		return this.fldRutafectado;
	}

	public void setFldRutafectado(String fldRutafectado) {
		this.fldRutafectado = fldRutafectado;
	}

	public String getFldUsuarioaclaracion() {
		return this.fldUsuarioaclaracion;
	}

	public void setFldUsuarioaclaracion(String fldUsuarioaclaracion) {
		this.fldUsuarioaclaracion = fldUsuarioaclaracion;
	}

	public TblMorhisto getTblMorhisto() {
		return this.tblMorhisto;
	}

	public void setTblMorhisto(TblMorhisto tblMorhisto) {
		this.tblMorhisto = tblMorhisto;
	}

}