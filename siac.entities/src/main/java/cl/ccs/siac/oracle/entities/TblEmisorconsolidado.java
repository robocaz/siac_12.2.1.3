package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_EMISORCONSOLIDADO database table.
 * 
 */
@Entity
@Table(name="TBL_EMISORCONSOLIDADO")
@NamedQuery(name="TblEmisorconsolidado.findAll", query="SELECT t FROM TblEmisorconsolidado t")
public class TblEmisorconsolidado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_ORDEN")
	private BigDecimal fldOrden;

	@Column(name="FLD_RUTEMISOR")
	private String fldRutemisor;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblEmisorconsolidado() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldOrden() {
		return this.fldOrden;
	}

	public void setFldOrden(BigDecimal fldOrden) {
		this.fldOrden = fldOrden;
	}

	public String getFldRutemisor() {
		return this.fldRutemisor;
	}

	public void setFldRutemisor(String fldRutemisor) {
		this.fldRutemisor = fldRutemisor;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}