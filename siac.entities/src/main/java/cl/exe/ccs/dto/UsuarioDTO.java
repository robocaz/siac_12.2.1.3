package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UsuarioDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 4638342405617043633L;
	private String nombre;
	private CentroCostoDTO centroCostoActivo;
	private String estado;
	private String caja = "0";
	private String email;
	private String idSession;
	private String username;
	private String ipLocal;

	private Boolean validaSupervisor = false;

	private List<String> privilegios;
	private Boolean esSupervisor = false;
	private Boolean esVenta = false;
	private Boolean esAdministrador = false;
	private Boolean esConsultaprotesto = false;

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}

	public String getIdSession() {
		return idSession;
	}

	public UsuarioDTO() {
		centroCostoActivo = new CentroCostoDTO();
	}

	public UsuarioDTO(Integer idControl, String msgControl) {
		this.msgControl = msgControl;
		this.idControl = idControl;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setCentroCostoActivo(CentroCostoDTO centroCostoActivo) {
		this.centroCostoActivo = centroCostoActivo;
	}

	public CentroCostoDTO getCentroCostoActivo() {
		return centroCostoActivo;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}

	public void setCaja(String caja) {
		this.caja = caja;
	}

	public String getCaja() {
		return caja;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public Boolean getValidaSupervisor() {
		return validaSupervisor;
	}

	public void setValidaSupervisor(Boolean validaSupervisor) {
		this.validaSupervisor = validaSupervisor;
	}

	public List<String> getPrivilegios() {
		return privilegios;
	}

	public void setPrivilegios(List<String> privilegios) {
		this.privilegios = privilegios;
	}

	public Boolean getEsSupervisor() {
		if (privilegios != null) {
			return privilegios.contains("siac.supervisor");
		}
		return esSupervisor;
	}

	public Boolean getEsVenta() {
		if (privilegios != null) {
			return privilegios.contains("siac.venta");
		}
		return esVenta;
	}

	public Boolean getEsAdministrador() {
		if (privilegios != null) {
			return privilegios.contains("siac.administrador");
		}
		return esAdministrador;

	}

	public Boolean getEsConsultaprotesto() {
		if (privilegios != null) {
			return privilegios.contains("siac.consultaprot");
		}
		return esConsultaprotesto;
	}

	public String getIpLocal() {
		return ipLocal;
	}

	public void setIpLocal(String ipLocal) {
		this.ipLocal = ipLocal;
	}

}
