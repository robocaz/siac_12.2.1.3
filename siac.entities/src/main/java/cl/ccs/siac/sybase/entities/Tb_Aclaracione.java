package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_Aclaraciones database table.
 * 
 */
@Entity
@Table(name="Tb_Aclaraciones")
@NamedQuery(name="Tb_Aclaracione.findAll", query="SELECT t FROM Tb_Aclaracione t")
public class Tb_Aclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Acl_CodCliente")
	private String acl_CodCliente;

	@Column(name="Acl_CodSucursal")
	private String acl_CodSucursal;

	@Column(name="Acl_FecAcla")
	private Timestamp acl_FecAcla;

	@Column(name="Acl_FecExpi")
	private Timestamp acl_FecExpi;

	@Column(name="Acl_FecVenc")
	private Timestamp acl_FecVenc;

	@Column(name="Acl_Moneda")
	private String acl_Moneda;

	@Column(name="Acl_Monto")
	private BigDecimal acl_Monto;

	@Column(name="Acl_NumDocu")
	private short acl_NumDocu;

	@Column(name="Acl_Rut")
	private String acl_Rut;

	@Column(name="Acl_TimeStamp")
	private byte[] acl_TimeStamp;

	@Column(name="Acl_TipoDocu")
	private String acl_TipoDocu;

	@Column(name="Acl_TipoInfo")
	private String acl_TipoInfo;

	@Column(name="Acl_TipoRelacion")
	private String acl_TipoRelacion;

	public Tb_Aclaracione() {
	}

	public String getAcl_CodCliente() {
		return this.acl_CodCliente;
	}

	public void setAcl_CodCliente(String acl_CodCliente) {
		this.acl_CodCliente = acl_CodCliente;
	}

	public String getAcl_CodSucursal() {
		return this.acl_CodSucursal;
	}

	public void setAcl_CodSucursal(String acl_CodSucursal) {
		this.acl_CodSucursal = acl_CodSucursal;
	}

	public Timestamp getAcl_FecAcla() {
		return this.acl_FecAcla;
	}

	public void setAcl_FecAcla(Timestamp acl_FecAcla) {
		this.acl_FecAcla = acl_FecAcla;
	}

	public Timestamp getAcl_FecExpi() {
		return this.acl_FecExpi;
	}

	public void setAcl_FecExpi(Timestamp acl_FecExpi) {
		this.acl_FecExpi = acl_FecExpi;
	}

	public Timestamp getAcl_FecVenc() {
		return this.acl_FecVenc;
	}

	public void setAcl_FecVenc(Timestamp acl_FecVenc) {
		this.acl_FecVenc = acl_FecVenc;
	}

	public String getAcl_Moneda() {
		return this.acl_Moneda;
	}

	public void setAcl_Moneda(String acl_Moneda) {
		this.acl_Moneda = acl_Moneda;
	}

	public BigDecimal getAcl_Monto() {
		return this.acl_Monto;
	}

	public void setAcl_Monto(BigDecimal acl_Monto) {
		this.acl_Monto = acl_Monto;
	}

	public short getAcl_NumDocu() {
		return this.acl_NumDocu;
	}

	public void setAcl_NumDocu(short acl_NumDocu) {
		this.acl_NumDocu = acl_NumDocu;
	}

	public String getAcl_Rut() {
		return this.acl_Rut;
	}

	public void setAcl_Rut(String acl_Rut) {
		this.acl_Rut = acl_Rut;
	}

	public byte[] getAcl_TimeStamp() {
		return this.acl_TimeStamp;
	}

	public void setAcl_TimeStamp(byte[] acl_TimeStamp) {
		this.acl_TimeStamp = acl_TimeStamp;
	}

	public String getAcl_TipoDocu() {
		return this.acl_TipoDocu;
	}

	public void setAcl_TipoDocu(String acl_TipoDocu) {
		this.acl_TipoDocu = acl_TipoDocu;
	}

	public String getAcl_TipoInfo() {
		return this.acl_TipoInfo;
	}

	public void setAcl_TipoInfo(String acl_TipoInfo) {
		this.acl_TipoInfo = acl_TipoInfo;
	}

	public String getAcl_TipoRelacion() {
		return this.acl_TipoRelacion;
	}

	public void setAcl_TipoRelacion(String acl_TipoRelacion) {
		this.acl_TipoRelacion = acl_TipoRelacion;
	}

}