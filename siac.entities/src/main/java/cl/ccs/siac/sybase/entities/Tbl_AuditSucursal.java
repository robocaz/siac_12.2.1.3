package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_AuditSucursal database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AuditSucursal.findAll", query="SELECT t FROM Tbl_AuditSucursal t")
public class Tbl_AuditSucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodLocalidad")
	private int fld_CodLocalidad;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecEliminacion")
	private Timestamp fld_FecEliminacion;

	@Column(name="Fld_GlosaSucursal")
	private String fld_GlosaSucursal;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_UsrCreacion")
	private String fld_UsrCreacion;

	@Column(name="Fld_UsrEliminacion")
	private String fld_UsrEliminacion;

	public Tbl_AuditSucursal() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public int getFld_CodLocalidad() {
		return this.fld_CodLocalidad;
	}

	public void setFld_CodLocalidad(int fld_CodLocalidad) {
		this.fld_CodLocalidad = fld_CodLocalidad;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecEliminacion() {
		return this.fld_FecEliminacion;
	}

	public void setFld_FecEliminacion(Timestamp fld_FecEliminacion) {
		this.fld_FecEliminacion = fld_FecEliminacion;
	}

	public String getFld_GlosaSucursal() {
		return this.fld_GlosaSucursal;
	}

	public void setFld_GlosaSucursal(String fld_GlosaSucursal) {
		this.fld_GlosaSucursal = fld_GlosaSucursal;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public String getFld_UsrCreacion() {
		return this.fld_UsrCreacion;
	}

	public void setFld_UsrCreacion(String fld_UsrCreacion) {
		this.fld_UsrCreacion = fld_UsrCreacion;
	}

	public String getFld_UsrEliminacion() {
		return this.fld_UsrEliminacion;
	}

	public void setFld_UsrEliminacion(String fld_UsrEliminacion) {
		this.fld_UsrEliminacion = fld_UsrEliminacion;
	}

}