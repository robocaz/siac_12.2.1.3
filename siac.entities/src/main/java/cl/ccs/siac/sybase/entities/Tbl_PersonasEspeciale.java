package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_PersonasEspeciales database table.
 * 
 */
@Entity
@Table(name="Tbl_PersonasEspeciales")
@NamedQuery(name="Tbl_PersonasEspeciale.findAll", query="SELECT t FROM Tbl_PersonasEspeciale t")
public class Tbl_PersonasEspeciale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CiudadPersona")
	private String fld_CiudadPersona;

	@Column(name="Fld_DireccionPersona")
	private String fld_DireccionPersona;

	@Column(name="Fld_FonoContacto")
	private double fld_FonoContacto;

	@Column(name="Fld_Nombre_ApMat")
	private String fld_Nombre_ApMat;

	@Column(name="Fld_Nombre_ApPat")
	private String fld_Nombre_ApPat;

	@Column(name="Fld_Nombre_Nombres")
	private String fld_Nombre_Nombres;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	public Tbl_PersonasEspeciale() {
	}

	public String getFld_CiudadPersona() {
		return this.fld_CiudadPersona;
	}

	public void setFld_CiudadPersona(String fld_CiudadPersona) {
		this.fld_CiudadPersona = fld_CiudadPersona;
	}

	public String getFld_DireccionPersona() {
		return this.fld_DireccionPersona;
	}

	public void setFld_DireccionPersona(String fld_DireccionPersona) {
		this.fld_DireccionPersona = fld_DireccionPersona;
	}

	public double getFld_FonoContacto() {
		return this.fld_FonoContacto;
	}

	public void setFld_FonoContacto(double fld_FonoContacto) {
		this.fld_FonoContacto = fld_FonoContacto;
	}

	public String getFld_Nombre_ApMat() {
		return this.fld_Nombre_ApMat;
	}

	public void setFld_Nombre_ApMat(String fld_Nombre_ApMat) {
		this.fld_Nombre_ApMat = fld_Nombre_ApMat;
	}

	public String getFld_Nombre_ApPat() {
		return this.fld_Nombre_ApPat;
	}

	public void setFld_Nombre_ApPat(String fld_Nombre_ApPat) {
		this.fld_Nombre_ApPat = fld_Nombre_ApPat;
	}

	public String getFld_Nombre_Nombres() {
		return this.fld_Nombre_Nombres;
	}

	public void setFld_Nombre_Nombres(String fld_Nombre_Nombres) {
		this.fld_Nombre_Nombres = fld_Nombre_Nombres;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

}