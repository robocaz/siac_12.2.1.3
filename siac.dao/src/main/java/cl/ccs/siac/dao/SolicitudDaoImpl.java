package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarInDTO;
import cl.exe.ccs.dto.SolicitudReclamoBuscarOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoIngresoOutDTO;
import cl.exe.ccs.dto.SolicitudReclamoData;
import cl.exe.ccs.dto.SolicitudReclamoIngresoInDTO;

@Named("solicitudDao")
@Stateless
public class SolicitudDaoImpl implements SolicitudDao {
	
	@PersistenceContext(unitName = "sybase")
	private EntityManager em;
	
	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;
	
	private static final Logger LOG = LogManager.getLogger(SolicitudDaoImpl.class);
	
	@Override
	public SolicitudReclamoIngresoOutDTO ingreso(SolicitudReclamoIngresoInDTO params) {
		LOG.debug("> DAO: ingreso - SP: SI_SolicitudReclamo_SW2");
		
		SolicitudReclamoIngresoOutDTO out = new SolicitudReclamoIngresoOutDTO();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SI_SolicitudReclamo_SW2");
			
			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(7, BigDecimal.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(8, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(9, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(10, Integer.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(11, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(12, String.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(13, BigDecimal.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(14, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(15, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getCorrCaja());
			spq.setParameter(2, params.getRutTitular());
			spq.setParameter(3, params.getCorrNombre());
			spq.setParameter(4, params.getEmail());
			spq.setParameter(5, params.getCodComuna());
			spq.setParameter(6, params.getDireccion());
			spq.setParameter(7, Long.valueOf(params.getTelefono()));
			spq.setParameter(8, params.getCantDoc());
			spq.setParameter(9, params.getTipoSolicitud());
			spq.setParameter(10, params.getCodCategoria());
			spq.setParameter(11, params.getUsuario());
			spq.setParameter(12, params.getTexto());
			LOG.debug("> Params: " + params.toString());
			
			spq.execute();
			
			out.setCorrSolic(((BigDecimal) spq.getOutputParameterValue(13)).intValue());
			out.setIdControl((Integer) spq.getOutputParameterValue(14));
			out.setMsgControl(spq.getOutputParameterValue(15).toString());
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public SolicitudReclamoBuscarOutDTO buscar(SolicitudReclamoBuscarInDTO params) {
		LOG.debug("> DAO: buscar - SP: SQ_BuscaSolicReclamo_SW2");
		
		SolicitudReclamoBuscarOutDTO out = new SolicitudReclamoBuscarOutDTO();
		List<SolicitudReclamoData> lista = new ArrayList<SolicitudReclamoData>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_BuscaSolicReclamo_SW2");
			
			spq.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(2, Timestamp.class, ParameterMode.IN);
			spq.registerStoredProcedureParameter(3, BigDecimal.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, params.getRutTitular());
			if ( params.getFecha() != null && !params.getFecha().isEmpty() ) {
				spq.setParameter(2, Timestamp.valueOf(params.getFecha()));
			}
			if ( params.getCorrCaja() != null ) {
				spq.setParameter(3, params.getCorrCaja());
			}
			LOG.debug("> Params: " + params);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			for ( Object row : result ) {
				Object[] data = (Object[]) row;
				SolicitudReclamoData obj = new SolicitudReclamoData();
				
				obj.setCorrSolicitudReclamo(Integer.valueOf(data[0].toString()));
				obj.setRutTitular(data[1].toString());
				obj.setNombreApePaterno(data[2].toString());
				obj.setNombreApeMaterno(data[3].toString());
				obj.setNombres(data[4].toString());
				obj.setDireccion(data[5].toString());
				obj.setEmail(data[6].toString());
				obj.setTelefono(Long.valueOf(((BigDecimal) data[7]).longValue()));
				obj.setTipoSolicitud((Integer) data[8]);
				obj.setGlosaTipoSolicitud(data[9].toString());
				obj.setCantDoc((Integer) data[10]);
				obj.setCodCategoria((Integer) data[11]);
				obj.setGlosaCategoria(data[12].toString());
				obj.setCodComuna((Integer) data[13]);
				obj.setCodRegion((Integer) data[14]);
				obj.setGlosaComuna(data[15].toString());
				obj.setCodEstado((Integer) data[16]);
				obj.setGlosaEstado(data[17].toString());
				obj.setCorrCaja(Integer.valueOf(data[18].toString()));
				obj.setCorrNombre(Integer.valueOf(data[19].toString()));
				obj.setFechaEstado(data[20].toString());
				obj.setUsuarioEstado(data[21].toString());
				obj.setGlosaCC(data[22].toString());
				obj.setFechaSolicitud(data[23].toString());
				obj.setUsuarioCreacion(data[24].toString());
				obj.setPdf(data[25].toString());
				
				lista.add(obj);
			}
			
			out.setLista(lista);
			
			out.setIdControl((Integer) spq.getOutputParameterValue(4));
			out.setMsgControl(spq.getOutputParameterValue(5).toString());
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}
	
	@Override
	public ResponseListDTO<String[]> historial(Integer corrSolic) {
		LOG.debug("> DAO: historial - SP: SQ_HistorialSR_SW2");
		
		ResponseListDTO<String[]> out = new ResponseListDTO<>();
		List<String[]> lista = new ArrayList<String[]>();
		
		try {
			StoredProcedureQuery spq = emlink.createStoredProcedureQuery("SQ_HistorialSR_SW2");
			
			spq.registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN);
			
			spq.registerStoredProcedureParameter(2, Integer.class, ParameterMode.OUT);
			spq.registerStoredProcedureParameter(3, String.class, ParameterMode.OUT);
			
			spq.setParameter(1, corrSolic);
			LOG.debug("> corrSolic: " + corrSolic);
			
			spq.execute();
			
			List<Object[]> result = spq.getResultList();
			
			for ( Object row : result ) {
				Object[] data = (Object[]) row;
				
				String[] fila = new String[3];
				fila[0] = data[0].toString();
				fila[1] = data[1].toString();
				fila[2] = data[2].toString();
				
				lista.add(fila);
			}
			
			out.setLista(lista);
			
			out.setIdControl((Integer) spq.getOutputParameterValue(2));
			out.setMsgControl(spq.getOutputParameterValue(3).toString());
			
		} catch (Exception e) {
			out.setIdControl(-1);
			out.setMsgControl(e.getMessage());
			LOG.error(e.getMessage());
		}
		
		return out;
	}

}
