package cl.ccs.siac.dao;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;

public interface UsuarioDao {
	
	UsuarioDTO obtenerUsuario(String username);
	
	UsuarioDTO esSupervisor (String usuario, String password);
	
	ResponseDTO cambiarClave (String usuario, String password, String passwordAux);

}
