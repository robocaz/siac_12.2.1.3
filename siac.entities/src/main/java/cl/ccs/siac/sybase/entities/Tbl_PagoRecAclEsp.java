package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_PagoRecAclEsp database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PagoRecAclEsp.findAll", query="SELECT t FROM Tbl_PagoRecAclEsp t")
public class Tbl_PagoRecAclEsp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_CorrVigNC")
	private BigDecimal fld_CorrVigNC;

	@Column(name="Fld_FlagCursada")
	private boolean fld_FlagCursada;

	@Column(name="Fld_FolioNro")
	private short fld_FolioNro;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_RutFirma2")
	private String fld_RutFirma2;

	public Tbl_PagoRecAclEsp() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public BigDecimal getFld_CorrVigNC() {
		return this.fld_CorrVigNC;
	}

	public void setFld_CorrVigNC(BigDecimal fld_CorrVigNC) {
		this.fld_CorrVigNC = fld_CorrVigNC;
	}

	public boolean getFld_FlagCursada() {
		return this.fld_FlagCursada;
	}

	public void setFld_FlagCursada(boolean fld_FlagCursada) {
		this.fld_FlagCursada = fld_FlagCursada;
	}

	public short getFld_FolioNro() {
		return this.fld_FolioNro;
	}

	public void setFld_FolioNro(short fld_FolioNro) {
		this.fld_FolioNro = fld_FolioNro;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public String getFld_RutFirma2() {
		return this.fld_RutFirma2;
	}

	public void setFld_RutFirma2(String fld_RutFirma2) {
		this.fld_RutFirma2 = fld_RutFirma2;
	}

}