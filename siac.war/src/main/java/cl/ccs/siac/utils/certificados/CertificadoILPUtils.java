package cl.ccs.siac.utils.certificados;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.utils.CertificadosUtil;
import cl.exe.ccs.dto.certificados.CertificadoILPDTO;
import cl.exe.ccs.dto.certificados.CertificadoILPPaginacionDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;

public class CertificadoILPUtils {

	private static final Logger LOG = LogManager.getLogger(CertificadoILPUtils.class);

	public static Map<String, Object> getParametrosILP(ConsCertificadoILPDTO consCertificadoILPDTO) {
		Map<String, Object> parametros = new HashMap<String, Object>();

		SimpleDateFormat of = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {

			parametros.put("nroBoletin", consCertificadoILPDTO.getNumeroBoletin());
			parametros.put("fecha", df.format(of.parse(consCertificadoILPDTO.getFechaPubLP())).toString());

			parametros.put("pRut",
					CertificadosUtil.getRut(consCertificadoILPDTO.getRutAfectado().toUpperCase().trim()));
			
			
            if(!CertificadosUtil.getTipoPersonaRut(consCertificadoILPDTO.getRutAfectado()).equalsIgnoreCase("J")){
                parametros.put("pNombre",
                            (((consCertificadoILPDTO.getNombres() != null)
                                            ? consCertificadoILPDTO.getNombres().toUpperCase().trim() : "")
                                            + " "
                                            + ((consCertificadoILPDTO.getAppPat() != null)
                                                            ? consCertificadoILPDTO.getAppPat().toUpperCase().trim() : "")
                                            + " " + ((consCertificadoILPDTO.getAppMat() != null)
                                                            ? consCertificadoILPDTO.getAppMat().toUpperCase().trim() : "")));
            }else{
                parametros.put("pNombre",
                            (((consCertificadoILPDTO.getNombres() != null)
                                            ? ((consCertificadoILPDTO.getAppPat() != null)
                                                            ? consCertificadoILPDTO.getAppPat().toUpperCase().trim() : "")
                                            + ((consCertificadoILPDTO.getAppMat() != null)
                                                            ? consCertificadoILPDTO.getAppMat().toUpperCase().trim() : "")
                                            + consCertificadoILPDTO.getNombres().toUpperCase().trim() : "")));
            }
            
            
//			parametros.put("pNombre",
//					(((consCertificadoILPDTO.getNombres() != null)
//							? consCertificadoILPDTO.getNombres().toUpperCase().trim() : "")
//							+ " "
//							+ ((consCertificadoILPDTO.getAppPat() != null)
//									? consCertificadoILPDTO.getAppPat().toUpperCase().trim() : "")
//							+ " " + ((consCertificadoILPDTO.getAppMat() != null)
//									? consCertificadoILPDTO.getAppMat().toUpperCase().trim() : "")));
			
			
			parametros.put("cantDocs", consCertificadoILPDTO.getCertILPResult().size());
			parametros.put("user", consCertificadoILPDTO.getCodUsuario());
			parametros.put("sucursal", consCertificadoILPDTO.getSucursal());
			parametros.put("codVerificacion", consCertificadoILPDTO.getCodAutenticBic());
			parametros.put("documentos", formatData(consCertificadoILPDTO.getCertILPResult()));
			parametros.put("nroCheques", countByType(consCertificadoILPDTO.getCertILPResult(), "CH"));
			parametros.put("nroLetras", (countByType(consCertificadoILPDTO.getCertILPResult(), "LT"))
					+ (countByType(consCertificadoILPDTO.getCertILPResult(), "PG")));
			parametros.put("nroCuotas", countByType(consCertificadoILPDTO.getCertILPResult(), "CM"));
			parametros.put("nroRentas", countByType(consCertificadoILPDTO.getCertILPResult(), "RA"));

			// Paginacion ILP
			List<CertificadoILPDTO> certificados = formatData(consCertificadoILPDTO.getCertILPResult());
			List<CertificadoILPPaginacionDTO> paginado = new ArrayList<CertificadoILPPaginacionDTO>();

			Integer totalBc = certificados.size();
			Integer maxBC = 26;
			Integer paginas = 1;

			// bc
			Integer bc_inicio = 1;
			Integer bc_termino = bc_inicio + CertificadosUtil.getPaginas(totalBc, maxBC, 0);
			Integer bc_resto = (bc_termino * maxBC) - totalBc;

			paginas = bc_termino;

			// elaboracion lista de paginas y bloques
			for (int p = 0; p < paginas; p++) {
				int pag = p + 1;
				int resto = 0;

				CertificadoILPPaginacionDTO paginacionILP = new CertificadoILPPaginacionDTO();
				paginacionILP.setPagina(pag);

				// bc
				if (totalBc > 0) {
					paginacionILP.setBc_inicio(pag == bc_inicio ? 0 : paginado.get(p - 1).getBc_termino());
					int diff_bc = totalBc - paginacionILP.getBc_inicio();
					diff_bc = diff_bc > maxBC ? maxBC : diff_bc;
					resto = diff_bc < maxBC ? maxBC - diff_bc : 0;
					paginacionILP.setBc_termino(paginacionILP.getBc_inicio() + diff_bc);
					if (paginacionILP.getBc_inicio() == paginacionILP.getBc_termino()) {
						paginacionILP.getBc_coleccion().add(certificados.get(paginacionILP.getBc_inicio()));
					} else {
						paginacionILP.setBc_coleccion(
								certificados.subList(paginacionILP.getBc_inicio(), paginacionILP.getBc_termino()));
					}
				}

				paginado.add(paginacionILP);
			}

			parametros.put("COLECCION_PAG", paginado);

			parametros.put("TOTAL_BC", totalBc);
			parametros.put("MAX_POR_PAGINA", maxBC);
			parametros.put("PAGINAS", paginas);

			parametros.put("BC_INICIO", bc_inicio);
			parametros.put("BC_TERMINO", bc_termino);

			parametros.put("SUBREPORT_DIR", "wlscomun/Certificados/");

		} catch (Exception e) {
			LOG.error("Error formando parametros para certificado ILP.");
			e.printStackTrace();
		}

		return parametros;

	}

	private static Integer countByType(List<CertificadoILPDTO> certILPResult, String type) {
		int count = 0;
		for (CertificadoILPDTO c : certILPResult) {
			if (c.getTipoDocumento().trim().equals(type)) {
				count++;
			}
		}

		return count;
	}

	private static List<CertificadoILPDTO> formatData(List<CertificadoILPDTO> certILPResult) {
		List<CertificadoILPDTO> resultSet = new ArrayList<CertificadoILPDTO>();
		for (CertificadoILPDTO c : certILPResult) {
			CertificadoILPDTO cert = new CertificadoILPDTO();
			cert.setCiudad(c.getCiudad().trim());
			cert.setCodEmisor(c.getCodEmisor().trim());
			cert.setFechaProt(c.getFechaProt());
			cert.setFechaPublicacion(c.getFechaPublicacion());
			cert.setGlosaCorta(c.getGlosaCorta().trim());
			cert.setGlosaEmisor(c.getGlosaEmisor().trim());
			cert.setMontoProt(CertificadosUtil.getMonto(new BigDecimal(c.getMontoProt())));
			cert.setNombreLiberador(c.getNombreLiberador().trim());
			cert.setNumBoletinProt(c.getNumBoletinProt());
			cert.setNumOperDig(c.getNumOperDig());
			cert.setPagBoletinProt(c.getPagBoletinProt());
			cert.setRut(CertificadosUtil.getRut(c.getRut().trim()));
			cert.setTipoDocumento(c.getTipoDocumento().trim());
			cert.setTipoDocumentoImpago(c.getTipoDocumentoImpago().trim());
			resultSet.add(cert);
		}

		return resultSet;
	}

}
