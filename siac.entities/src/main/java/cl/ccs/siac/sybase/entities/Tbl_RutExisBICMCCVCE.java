package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutExisBICMCCVCE database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutExisBICMCCVCE.findAll", query="SELECT t FROM Tbl_RutExisBICMCCVCE t")
public class Tbl_RutExisBICMCCVCE implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut")
	private BigDecimal fld_CorrRut;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_SiNoMorosidad")
	private String fld_SiNoMorosidad;

	@Column(name="Fld_SiNoProtesto")
	private String fld_SiNoProtesto;

	public Tbl_RutExisBICMCCVCE() {
	}

	public BigDecimal getFld_CorrRut() {
		return this.fld_CorrRut;
	}

	public void setFld_CorrRut(BigDecimal fld_CorrRut) {
		this.fld_CorrRut = fld_CorrRut;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_SiNoMorosidad() {
		return this.fld_SiNoMorosidad;
	}

	public void setFld_SiNoMorosidad(String fld_SiNoMorosidad) {
		this.fld_SiNoMorosidad = fld_SiNoMorosidad;
	}

	public String getFld_SiNoProtesto() {
		return this.fld_SiNoProtesto;
	}

	public void setFld_SiNoProtesto(String fld_SiNoProtesto) {
		this.fld_SiNoProtesto = fld_SiNoProtesto;
	}

}