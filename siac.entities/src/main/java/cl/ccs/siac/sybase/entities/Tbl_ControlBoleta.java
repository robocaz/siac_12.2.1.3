package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_ControlBoleta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlBoleta.findAll", query="SELECT t FROM Tbl_ControlBoleta t")
public class Tbl_ControlBoleta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_NroBoletaActual")
	private int fld_NroBoletaActual;

	@Column(name="Fld_NroBoletaDesde")
	private int fld_NroBoletaDesde;

	@Column(name="Fld_NroBoletaHasta")
	private int fld_NroBoletaHasta;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_ControlBoleta() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public int getFld_NroBoletaActual() {
		return this.fld_NroBoletaActual;
	}

	public void setFld_NroBoletaActual(int fld_NroBoletaActual) {
		this.fld_NroBoletaActual = fld_NroBoletaActual;
	}

	public int getFld_NroBoletaDesde() {
		return this.fld_NroBoletaDesde;
	}

	public void setFld_NroBoletaDesde(int fld_NroBoletaDesde) {
		this.fld_NroBoletaDesde = fld_NroBoletaDesde;
	}

	public int getFld_NroBoletaHasta() {
		return this.fld_NroBoletaHasta;
	}

	public void setFld_NroBoletaHasta(int fld_NroBoletaHasta) {
		this.fld_NroBoletaHasta = fld_NroBoletaHasta;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}