package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServicioDTO implements Serializable{

	private String codServicio;
	private String glosaServicio;
	private Integer tipoServicio;
	private Integer codExterno;
	
	
	
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getGlosaServicio() {
		return glosaServicio;
	}
	public void setGlosaServicio(String glosaServicio) {
		this.glosaServicio = glosaServicio;
	}
	public Integer getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(Integer tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public Integer getCodExterno() {
		return codExterno;
	}
	public void setCodExterno(Integer codExterno) {
		this.codExterno = codExterno;
	}
	
	
	
}
