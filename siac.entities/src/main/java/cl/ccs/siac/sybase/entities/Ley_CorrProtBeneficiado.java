package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Ley_CorrProtBeneficiados database table.
 * 
 */
@Entity
@Table(name="Ley_CorrProtBeneficiados")
@NamedQuery(name="Ley_CorrProtBeneficiado.findAll", query="SELECT l FROM Ley_CorrProtBeneficiado l")
public class Ley_CorrProtBeneficiado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_FlagProcesado")
	private byte fld_FlagProcesado;

	public Ley_CorrProtBeneficiado() {
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public byte getFld_FlagProcesado() {
		return this.fld_FlagProcesado;
	}

	public void setFld_FlagProcesado(byte fld_FlagProcesado) {
		this.fld_FlagProcesado = fld_FlagProcesado;
	}

}