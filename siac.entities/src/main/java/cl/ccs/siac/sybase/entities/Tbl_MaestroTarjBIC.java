package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_MaestroTarjBIC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_MaestroTarjBIC.findAll", query="SELECT t FROM Tbl_MaestroTarjBIC t")
public class Tbl_MaestroTarjBIC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_Corr_Id")
	private BigDecimal fld_Corr_Id;

	@Column(name="Fld_FecGeneracion")
	private Timestamp fld_FecGeneracion;

	public Tbl_MaestroTarjBIC() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_Corr_Id() {
		return this.fld_Corr_Id;
	}

	public void setFld_Corr_Id(BigDecimal fld_Corr_Id) {
		this.fld_Corr_Id = fld_Corr_Id;
	}

	public Timestamp getFld_FecGeneracion() {
		return this.fld_FecGeneracion;
	}

	public void setFld_FecGeneracion(Timestamp fld_FecGeneracion) {
		this.fld_FecGeneracion = fld_FecGeneracion;
	}

}