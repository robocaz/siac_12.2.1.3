package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ESTACL database table.
 * 
 */
@Entity
@Table(name="ESTACL")
@NamedQuery(name="Estacl.findAll", query="SELECT e FROM Estacl e")
public class Estacl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CalJur")
	private String fld_CalJur;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_FecRecepcion")
	private String fld_FecRecepcion;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_NroReg")
	private int fld_NroReg;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TotalMtoProt")
	private BigDecimal fld_TotalMtoProt;

	@Column(name="Fld_Tramo")
	private int fld_Tramo;

	public Estacl() {
	}

	public String getFld_CalJur() {
		return this.fld_CalJur;
	}

	public void setFld_CalJur(String fld_CalJur) {
		this.fld_CalJur = fld_CalJur;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public String getFld_FecRecepcion() {
		return this.fld_FecRecepcion;
	}

	public void setFld_FecRecepcion(String fld_FecRecepcion) {
		this.fld_FecRecepcion = fld_FecRecepcion;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public int getFld_NroReg() {
		return this.fld_NroReg;
	}

	public void setFld_NroReg(int fld_NroReg) {
		this.fld_NroReg = fld_NroReg;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public BigDecimal getFld_TotalMtoProt() {
		return this.fld_TotalMtoProt;
	}

	public void setFld_TotalMtoProt(BigDecimal fld_TotalMtoProt) {
		this.fld_TotalMtoProt = fld_TotalMtoProt;
	}

	public int getFld_Tramo() {
		return this.fld_Tramo;
	}

	public void setFld_Tramo(int fld_Tramo) {
		this.fld_Tramo = fld_Tramo;
	}

}