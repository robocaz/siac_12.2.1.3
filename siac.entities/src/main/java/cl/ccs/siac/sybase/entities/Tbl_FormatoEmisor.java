package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_FormatoEmisor database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FormatoEmisor.findAll", query="SELECT t FROM Tbl_FormatoEmisor t")
public class Tbl_FormatoEmisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_FormaEnvio")
	private String fld_FormaEnvio;

	@Column(name="Fld_FormatoProtesto")
	private short fld_FormatoProtesto;

	@Column(name="Fld_LargoReg")
	private short fld_LargoReg;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_FormatoEmisor() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_FormaEnvio() {
		return this.fld_FormaEnvio;
	}

	public void setFld_FormaEnvio(String fld_FormaEnvio) {
		this.fld_FormaEnvio = fld_FormaEnvio;
	}

	public short getFld_FormatoProtesto() {
		return this.fld_FormatoProtesto;
	}

	public void setFld_FormatoProtesto(short fld_FormatoProtesto) {
		this.fld_FormatoProtesto = fld_FormatoProtesto;
	}

	public short getFld_LargoReg() {
		return this.fld_LargoReg;
	}

	public void setFld_LargoReg(short fld_LargoReg) {
		this.fld_LargoReg = fld_LargoReg;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}