package cl.ccs.siac.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransaccionServicioDTO implements Serializable {

	
	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = -7399532442109692382L;
	private Long corrCaja;
	private String tipoServicio;
	private String tipoDocAclaracion;
	private String rutAfectado;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String costoServicio;
	private String tipoDocumento;
	private String moneda;
	private String montoDocumento;
	private String numeroOperacion;
	private String fechaProtesto;
	private String emisor;
	private String correlativoProtesto;
	private String correlativoTransaccion;
	private String correlativoMovimiento;
	private String codServicio;
	private String correlativoPersona;
	private String boletaFactura;
	private String numeroBoletaFactura;
	private String titular;
	private String estadoMovimiento;
	
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getTipoDocAclaracion() {
		return tipoDocAclaracion;
	}
	public void setTipoDocAclaracion(String tipoDocAclaracion) {
		this.tipoDocAclaracion = tipoDocAclaracion;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCostoServicio() {
		return costoServicio;
	}
	public void setCostoServicio(String costoServicio) {
		this.costoServicio = costoServicio;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMontoDocumento() {
		return montoDocumento;
	}
	public void setMontoDocumento(String montoDocumento) {
		this.montoDocumento = montoDocumento;
	}
	public String getNumeroOperacion() {
		return numeroOperacion;
	}
	public void setNumeroOperacion(String numeroOperacion) {
		this.numeroOperacion = numeroOperacion;
	}
	public String getFechaProtesto() {
		return fechaProtesto;
	}
	public void setFechaProtesto(String fechaProtesto) {
		this.fechaProtesto = fechaProtesto;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getCorrelativoProtesto() {
		return correlativoProtesto;
	}
	public void setCorrelativoProtesto(String correlativoProtesto) {
		this.correlativoProtesto = correlativoProtesto;
	}
	public String getCorrelativoTransaccion() {
		return correlativoTransaccion;
	}
	public void setCorrelativoTransaccion(String correlativoTransaccion) {
		this.correlativoTransaccion = correlativoTransaccion;
	}
	public String getCorrelativoMovimiento() {
		return correlativoMovimiento;
	}
	public void setCorrelativoMovimiento(String correlativoMovimiento) {
		this.correlativoMovimiento = correlativoMovimiento;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getCorrelativoPersona() {
		return correlativoPersona;
	}
	public void setCorrelativoPersona(String correlativoPersona) {
		this.correlativoPersona = correlativoPersona;
	}
	public String getBoletaFactura() {
		return boletaFactura;
	}
	public void setBoletaFactura(String boletaFactura) {
		this.boletaFactura = boletaFactura;
	}
	public String getNumeroBoletaFactura() {
		return numeroBoletaFactura;
	}
	public void setNumeroBoletaFactura(String numeroBoletaFactura) {
		this.numeroBoletaFactura = numeroBoletaFactura;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getEstadoMovimiento() {
		return estadoMovimiento;
	}
	public void setEstadoMovimiento(String estadoMovimiento) {
		this.estadoMovimiento = estadoMovimiento;
	}	
    
    
}
