package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CorrPrescritos database table.
 * 
 */
@Entity
@Table(name="Tbl_CorrPrescritos")
@NamedQuery(name="Tbl_CorrPrescrito.findAll", query="SELECT t FROM Tbl_CorrPrescrito t")
public class Tbl_CorrPrescrito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_Estado")
	private byte fld_Estado;

	public Tbl_CorrPrescrito() {
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public byte getFld_Estado() {
		return this.fld_Estado;
	}

	public void setFld_Estado(byte fld_Estado) {
		this.fld_Estado = fld_Estado;
	}

}