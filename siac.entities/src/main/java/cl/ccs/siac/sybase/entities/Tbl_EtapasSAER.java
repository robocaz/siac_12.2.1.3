package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_EtapasSAER database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EtapasSAER.findAll", query="SELECT t FROM Tbl_EtapasSAER t")
public class Tbl_EtapasSAER implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEtapa")
	private short fld_CodEtapa;

	@Column(name="Fld_GlosaEtapa")
	private String fld_GlosaEtapa;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_EtapasSAER() {
	}

	public short getFld_CodEtapa() {
		return this.fld_CodEtapa;
	}

	public void setFld_CodEtapa(short fld_CodEtapa) {
		this.fld_CodEtapa = fld_CodEtapa;
	}

	public String getFld_GlosaEtapa() {
		return this.fld_GlosaEtapa;
	}

	public void setFld_GlosaEtapa(String fld_GlosaEtapa) {
		this.fld_GlosaEtapa = fld_GlosaEtapa;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}