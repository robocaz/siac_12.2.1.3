package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_CobroCertifInfCom database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CobroCertifInfCom.findAll", query="SELECT t FROM Tbl_CobroCertifInfCom t")
public class Tbl_CobroCertifInfCom implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodServicio")
	private String fld_CodServicio;

	@Column(name="Fld_CostoBase")
	private BigDecimal fld_CostoBase;

	@Column(name="Fld_CostoPorRegistro")
	private BigDecimal fld_CostoPorRegistro;

	@Column(name="Fld_FecInicio")
	private Timestamp fld_FecInicio;

	@Column(name="Fld_FecTermino")
	private Timestamp fld_FecTermino;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_CobroCertifInfCom() {
	}

	public String getFld_CodServicio() {
		return this.fld_CodServicio;
	}

	public void setFld_CodServicio(String fld_CodServicio) {
		this.fld_CodServicio = fld_CodServicio;
	}

	public BigDecimal getFld_CostoBase() {
		return this.fld_CostoBase;
	}

	public void setFld_CostoBase(BigDecimal fld_CostoBase) {
		this.fld_CostoBase = fld_CostoBase;
	}

	public BigDecimal getFld_CostoPorRegistro() {
		return this.fld_CostoPorRegistro;
	}

	public void setFld_CostoPorRegistro(BigDecimal fld_CostoPorRegistro) {
		this.fld_CostoPorRegistro = fld_CostoPorRegistro;
	}

	public Timestamp getFld_FecInicio() {
		return this.fld_FecInicio;
	}

	public void setFld_FecInicio(Timestamp fld_FecInicio) {
		this.fld_FecInicio = fld_FecInicio;
	}

	public Timestamp getFld_FecTermino() {
		return this.fld_FecTermino;
	}

	public void setFld_FecTermino(Timestamp fld_FecTermino) {
		this.fld_FecTermino = fld_FecTermino;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}