package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Facturas_9d28c980 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_Facturas_9d28c980.findAll", query="SELECT t FROM Tbl_Facturas_9d28c980 t")
public class Tbl_Facturas_9d28c980 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodOrigen")
	private String fld_CodOrigen;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_Giro")
	private String fld_Giro;

	@Column(name="Fld_MontoBruto")
	private BigDecimal fld_MontoBruto;

	@Column(name="Fld_NroFactura")
	private int fld_NroFactura;

	@Column(name="Fld_RazonSocial")
	private String fld_RazonSocial;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_Ubicacion")
	private String fld_Ubicacion;

	public Tbl_Facturas_9d28c980() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodOrigen() {
		return this.fld_CodOrigen;
	}

	public void setFld_CodOrigen(String fld_CodOrigen) {
		this.fld_CodOrigen = fld_CodOrigen;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public String getFld_Giro() {
		return this.fld_Giro;
	}

	public void setFld_Giro(String fld_Giro) {
		this.fld_Giro = fld_Giro;
	}

	public BigDecimal getFld_MontoBruto() {
		return this.fld_MontoBruto;
	}

	public void setFld_MontoBruto(BigDecimal fld_MontoBruto) {
		this.fld_MontoBruto = fld_MontoBruto;
	}

	public int getFld_NroFactura() {
		return this.fld_NroFactura;
	}

	public void setFld_NroFactura(int fld_NroFactura) {
		this.fld_NroFactura = fld_NroFactura;
	}

	public String getFld_RazonSocial() {
		return this.fld_RazonSocial;
	}

	public void setFld_RazonSocial(String fld_RazonSocial) {
		this.fld_RazonSocial = fld_RazonSocial;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_Ubicacion() {
		return this.fld_Ubicacion;
	}

	public void setFld_Ubicacion(String fld_Ubicacion) {
		this.fld_Ubicacion = fld_Ubicacion;
	}

}