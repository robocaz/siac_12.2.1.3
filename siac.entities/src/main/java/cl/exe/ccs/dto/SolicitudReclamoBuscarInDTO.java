package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoBuscarInDTO implements Serializable {

	private static final long serialVersionUID = -8239113961219501300L;

	private String rutTitular;
	private String fecha;
	private Integer corrCaja;

	public String getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(String rutTitular) {
		this.rutTitular = rutTitular;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Integer getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Integer corrCaja) {
		this.corrCaja = corrCaja;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SolicitudReclamoBuscarInDTO [rutTitular=");
		builder.append(rutTitular);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", corrCaja=");
		builder.append(corrCaja);
		builder.append("]");
		return builder.toString();
	}

}
