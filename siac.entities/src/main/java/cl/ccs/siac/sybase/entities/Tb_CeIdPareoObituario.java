package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tb_CeIdPareoObituarios database table.
 * 
 */
@Entity
@Table(name="Tb_CeIdPareoObituarios")
@NamedQuery(name="Tb_CeIdPareoObituario.findAll", query="SELECT t FROM Tb_CeIdPareoObituario t")
public class Tb_CeIdPareoObituario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodRetorno")
	private byte fld_CodRetorno;

	@Column(name="Fld_MarcaDefuncion")
	private boolean fld_MarcaDefuncion;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	//bi-directional many-to-one association to Tb_CeIdDetObituario
	@ManyToOne
	@JoinColumn(name="Fld_CorrObituario")
	private Tb_CeIdDetObituario tbCeIdDetObituario;

	public Tb_CeIdPareoObituario() {
	}

	public byte getFld_CodRetorno() {
		return this.fld_CodRetorno;
	}

	public void setFld_CodRetorno(byte fld_CodRetorno) {
		this.fld_CodRetorno = fld_CodRetorno;
	}

	public boolean getFld_MarcaDefuncion() {
		return this.fld_MarcaDefuncion;
	}

	public void setFld_MarcaDefuncion(boolean fld_MarcaDefuncion) {
		this.fld_MarcaDefuncion = fld_MarcaDefuncion;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public Tb_CeIdDetObituario getTbCeIdDetObituario() {
		return this.tbCeIdDetObituario;
	}

	public void setTbCeIdDetObituario(Tb_CeIdDetObituario tbCeIdDetObituario) {
		this.tbCeIdDetObituario = tbCeIdDetObituario;
	}

}