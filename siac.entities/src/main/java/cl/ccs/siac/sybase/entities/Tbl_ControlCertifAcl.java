package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ControlCertifAcl database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlCertifAcl.findAll", query="SELECT t FROM Tbl_ControlCertifAcl t")
public class Tbl_ControlCertifAcl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrTransaccion")
	private BigDecimal fld_CorrTransaccion;

	public Tbl_ControlCertifAcl() {
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrTransaccion() {
		return this.fld_CorrTransaccion;
	}

	public void setFld_CorrTransaccion(BigDecimal fld_CorrTransaccion) {
		this.fld_CorrTransaccion = fld_CorrTransaccion;
	}

}