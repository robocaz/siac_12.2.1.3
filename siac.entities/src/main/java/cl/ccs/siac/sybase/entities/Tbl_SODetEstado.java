package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_SODetEstado database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SODetEstado.findAll", query="SELECT t FROM Tbl_SODetEstado t")
public class Tbl_SODetEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodUsuarioEjecucion")
	private String fld_CodUsuarioEjecucion;

	@Column(name="Fld_CorrOffline")
	private BigDecimal fld_CorrOffline;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	public Tbl_SODetEstado() {
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodUsuarioEjecucion() {
		return this.fld_CodUsuarioEjecucion;
	}

	public void setFld_CodUsuarioEjecucion(String fld_CodUsuarioEjecucion) {
		this.fld_CodUsuarioEjecucion = fld_CodUsuarioEjecucion;
	}

	public BigDecimal getFld_CorrOffline() {
		return this.fld_CorrOffline;
	}

	public void setFld_CorrOffline(BigDecimal fld_CorrOffline) {
		this.fld_CorrOffline = fld_CorrOffline;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

}