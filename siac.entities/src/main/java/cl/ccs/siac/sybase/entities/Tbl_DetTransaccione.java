package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_DetTransacciones database table.
 * 
 */
@Entity
@Table(name="Tbl_DetTransacciones")
@NamedQuery(name="Tbl_DetTransaccione.findAll", query="SELECT t FROM Tbl_DetTransaccione t")
public class Tbl_DetTransaccione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_BoletaFactura")
	private String fld_BoletaFactura;

	@Column(name="Fld_BoletaNula")
	private String fld_BoletaNula;

	@Column(name="Fld_CodBanco")
	private String fld_CodBanco;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrTransaccion_id")
	private BigDecimal fld_CorrTransaccion_id;

	@Column(name="Fld_FonoSolicitante")
	private String fld_FonoSolicitante;

	@Column(name="Fld_MontoCobradoSistema")
	private BigDecimal fld_MontoCobradoSistema;

	@Column(name="Fld_MontoIngresadoCajera")
	private BigDecimal fld_MontoIngresadoCajera;

	@Column(name="Fld_NroBoletaFactura")
	private int fld_NroBoletaFactura;

	@Column(name="Fld_NroCheque")
	private String fld_NroCheque;

	@Column(name="Fld_RutSolicitante")
	private String fld_RutSolicitante;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoPago")
	private String fld_TipoPago;

	@Column(name="Fld_TotFaltante")
	private BigDecimal fld_TotFaltante;

	@Column(name="Fld_TotSobrante")
	private BigDecimal fld_TotSobrante;

	@Column(name="Fld_Ubicacion")
	private String fld_Ubicacion;

	public Tbl_DetTransaccione() {
	}

	public String getFld_BoletaFactura() {
		return this.fld_BoletaFactura;
	}

	public void setFld_BoletaFactura(String fld_BoletaFactura) {
		this.fld_BoletaFactura = fld_BoletaFactura;
	}

	public String getFld_BoletaNula() {
		return this.fld_BoletaNula;
	}

	public void setFld_BoletaNula(String fld_BoletaNula) {
		this.fld_BoletaNula = fld_BoletaNula;
	}

	public String getFld_CodBanco() {
		return this.fld_CodBanco;
	}

	public void setFld_CodBanco(String fld_CodBanco) {
		this.fld_CodBanco = fld_CodBanco;
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrTransaccion_id() {
		return this.fld_CorrTransaccion_id;
	}

	public void setFld_CorrTransaccion_id(BigDecimal fld_CorrTransaccion_id) {
		this.fld_CorrTransaccion_id = fld_CorrTransaccion_id;
	}

	public String getFld_FonoSolicitante() {
		return this.fld_FonoSolicitante;
	}

	public void setFld_FonoSolicitante(String fld_FonoSolicitante) {
		this.fld_FonoSolicitante = fld_FonoSolicitante;
	}

	public BigDecimal getFld_MontoCobradoSistema() {
		return this.fld_MontoCobradoSistema;
	}

	public void setFld_MontoCobradoSistema(BigDecimal fld_MontoCobradoSistema) {
		this.fld_MontoCobradoSistema = fld_MontoCobradoSistema;
	}

	public BigDecimal getFld_MontoIngresadoCajera() {
		return this.fld_MontoIngresadoCajera;
	}

	public void setFld_MontoIngresadoCajera(BigDecimal fld_MontoIngresadoCajera) {
		this.fld_MontoIngresadoCajera = fld_MontoIngresadoCajera;
	}

	public int getFld_NroBoletaFactura() {
		return this.fld_NroBoletaFactura;
	}

	public void setFld_NroBoletaFactura(int fld_NroBoletaFactura) {
		this.fld_NroBoletaFactura = fld_NroBoletaFactura;
	}

	public String getFld_NroCheque() {
		return this.fld_NroCheque;
	}

	public void setFld_NroCheque(String fld_NroCheque) {
		this.fld_NroCheque = fld_NroCheque;
	}

	public String getFld_RutSolicitante() {
		return this.fld_RutSolicitante;
	}

	public void setFld_RutSolicitante(String fld_RutSolicitante) {
		this.fld_RutSolicitante = fld_RutSolicitante;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoPago() {
		return this.fld_TipoPago;
	}

	public void setFld_TipoPago(String fld_TipoPago) {
		this.fld_TipoPago = fld_TipoPago;
	}

	public BigDecimal getFld_TotFaltante() {
		return this.fld_TotFaltante;
	}

	public void setFld_TotFaltante(BigDecimal fld_TotFaltante) {
		this.fld_TotFaltante = fld_TotFaltante;
	}

	public BigDecimal getFld_TotSobrante() {
		return this.fld_TotSobrante;
	}

	public void setFld_TotSobrante(BigDecimal fld_TotSobrante) {
		this.fld_TotSobrante = fld_TotSobrante;
	}

	public String getFld_Ubicacion() {
		return this.fld_Ubicacion;
	}

	public void setFld_Ubicacion(String fld_Ubicacion) {
		this.fld_Ubicacion = fld_Ubicacion;
	}

}