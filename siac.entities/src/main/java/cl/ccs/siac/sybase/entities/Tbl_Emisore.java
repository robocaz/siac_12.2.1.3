package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Emisores database table.
 * 
 */
@Entity
@Table(name="Tbl_Emisores")
@NamedQuery(name="Tbl_Emisore.findAll", query="SELECT t FROM Tbl_Emisore t")
public class Tbl_Emisore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Calidad")
	private BigDecimal fld_Calidad;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodUsuarioAsigEnvio")
	private String fld_CodUsuarioAsigEnvio;

	@Column(name="Fld_FecInicioVigencia")
	private Timestamp fld_FecInicioVigencia;

	@Column(name="Fld_FecTerminoVigencia")
	private Timestamp fld_FecTerminoVigencia;

	@Column(name="Fld_FlagVigencia")
	private boolean fld_FlagVigencia;

	@Column(name="Fld_GlosaEmisor")
	private String fld_GlosaEmisor;

	@Column(name="Fld_GlosaLarga")
	private String fld_GlosaLarga;

	@Column(name="Fld_RutEmisor")
	private String fld_RutEmisor;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TotNrosConfirmatAGen")
	private int fld_TotNrosConfirmatAGen;

	@Column(name="Fld_TotNrosConfirmatorios")
	private int fld_TotNrosConfirmatorios;

	@Column(name="Fld_UsaNrosConfirmatorios")
	private boolean fld_UsaNrosConfirmatorios;

	public Tbl_Emisore() {
	}

	public BigDecimal getFld_Calidad() {
		return this.fld_Calidad;
	}

	public void setFld_Calidad(BigDecimal fld_Calidad) {
		this.fld_Calidad = fld_Calidad;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public String getFld_CodUsuarioAsigEnvio() {
		return this.fld_CodUsuarioAsigEnvio;
	}

	public void setFld_CodUsuarioAsigEnvio(String fld_CodUsuarioAsigEnvio) {
		this.fld_CodUsuarioAsigEnvio = fld_CodUsuarioAsigEnvio;
	}

	public Timestamp getFld_FecInicioVigencia() {
		return this.fld_FecInicioVigencia;
	}

	public void setFld_FecInicioVigencia(Timestamp fld_FecInicioVigencia) {
		this.fld_FecInicioVigencia = fld_FecInicioVigencia;
	}

	public Timestamp getFld_FecTerminoVigencia() {
		return this.fld_FecTerminoVigencia;
	}

	public void setFld_FecTerminoVigencia(Timestamp fld_FecTerminoVigencia) {
		this.fld_FecTerminoVigencia = fld_FecTerminoVigencia;
	}

	public boolean getFld_FlagVigencia() {
		return this.fld_FlagVigencia;
	}

	public void setFld_FlagVigencia(boolean fld_FlagVigencia) {
		this.fld_FlagVigencia = fld_FlagVigencia;
	}

	public String getFld_GlosaEmisor() {
		return this.fld_GlosaEmisor;
	}

	public void setFld_GlosaEmisor(String fld_GlosaEmisor) {
		this.fld_GlosaEmisor = fld_GlosaEmisor;
	}

	public String getFld_GlosaLarga() {
		return this.fld_GlosaLarga;
	}

	public void setFld_GlosaLarga(String fld_GlosaLarga) {
		this.fld_GlosaLarga = fld_GlosaLarga;
	}

	public String getFld_RutEmisor() {
		return this.fld_RutEmisor;
	}

	public void setFld_RutEmisor(String fld_RutEmisor) {
		this.fld_RutEmisor = fld_RutEmisor;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public int getFld_TotNrosConfirmatAGen() {
		return this.fld_TotNrosConfirmatAGen;
	}

	public void setFld_TotNrosConfirmatAGen(int fld_TotNrosConfirmatAGen) {
		this.fld_TotNrosConfirmatAGen = fld_TotNrosConfirmatAGen;
	}

	public int getFld_TotNrosConfirmatorios() {
		return this.fld_TotNrosConfirmatorios;
	}

	public void setFld_TotNrosConfirmatorios(int fld_TotNrosConfirmatorios) {
		this.fld_TotNrosConfirmatorios = fld_TotNrosConfirmatorios;
	}

	public boolean getFld_UsaNrosConfirmatorios() {
		return this.fld_UsaNrosConfirmatorios;
	}

	public void setFld_UsaNrosConfirmatorios(boolean fld_UsaNrosConfirmatorios) {
		this.fld_UsaNrosConfirmatorios = fld_UsaNrosConfirmatorios;
	}

}