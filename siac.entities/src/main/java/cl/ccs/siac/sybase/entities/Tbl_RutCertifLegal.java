package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RutCertifLegal database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutCertifLegal.findAll", query="SELECT t FROM Tbl_RutCertifLegal t")
public class Tbl_RutCertifLegal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_RutCertifLegal() {
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}