package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_PreguntaSeguridad database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_PreguntaSeguridad.findAll", query="SELECT t FROM Tbl_PreguntaSeguridad t")
public class Tbl_PreguntaSeguridad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodPregunta")
	private byte fld_CodPregunta;

	@Column(name="Fld_GlosaPregunta")
	private String fld_GlosaPregunta;

	public Tbl_PreguntaSeguridad() {
	}

	public byte getFld_CodPregunta() {
		return this.fld_CodPregunta;
	}

	public void setFld_CodPregunta(byte fld_CodPregunta) {
		this.fld_CodPregunta = fld_CodPregunta;
	}

	public String getFld_GlosaPregunta() {
		return this.fld_GlosaPregunta;
	}

	public void setFld_GlosaPregunta(String fld_GlosaPregunta) {
		this.fld_GlosaPregunta = fld_GlosaPregunta;
	}

}