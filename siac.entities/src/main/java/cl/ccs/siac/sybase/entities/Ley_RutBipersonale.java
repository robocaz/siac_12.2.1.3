package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ley_RutBipersonales database table.
 * 
 */
@Entity
@Table(name="Ley_RutBipersonales")
@NamedQuery(name="Ley_RutBipersonale.findAll", query="SELECT l FROM Ley_RutBipersonale l")
public class Ley_RutBipersonale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FlagBeneficiado")
	private byte fld_FlagBeneficiado;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public Ley_RutBipersonale() {
	}

	public byte getFld_FlagBeneficiado() {
		return this.fld_FlagBeneficiado;
	}

	public void setFld_FlagBeneficiado(byte fld_FlagBeneficiado) {
		this.fld_FlagBeneficiado = fld_FlagBeneficiado;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}