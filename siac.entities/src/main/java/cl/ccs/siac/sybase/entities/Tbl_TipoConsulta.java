package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_TipoConsultas database table.
 * 
 */
@Entity
@Table(name="Tbl_TipoConsultas")
@NamedQuery(name="Tbl_TipoConsulta.findAll", query="SELECT t FROM Tbl_TipoConsulta t")
public class Tbl_TipoConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodConsulta")
	private byte fld_CodConsulta;

	@Column(name="Fld_GlosaConsulta")
	private String fld_GlosaConsulta;

	public Tbl_TipoConsulta() {
	}

	public byte getFld_CodConsulta() {
		return this.fld_CodConsulta;
	}

	public void setFld_CodConsulta(byte fld_CodConsulta) {
		this.fld_CodConsulta = fld_CodConsulta;
	}

	public String getFld_GlosaConsulta() {
		return this.fld_GlosaConsulta;
	}

	public void setFld_GlosaConsulta(String fld_GlosaConsulta) {
		this.fld_GlosaConsulta = fld_GlosaConsulta;
	}

}