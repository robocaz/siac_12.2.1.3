package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_DetFirmas database table.
 * 
 */
@Entity
@Table(name="Tbl_DetFirmas")
@NamedQuery(name="Tbl_DetFirma.findAll", query="SELECT t FROM Tbl_DetFirma t")
public class Tbl_DetFirma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CargoFirma")
	private String fld_CargoFirma;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAutoriza")
	private BigDecimal fld_CorrAutoriza;

	@Column(name="Fld_CorrFirma_Id")
	private BigDecimal fld_CorrFirma_Id;

	@Column(name="Fld_DirecFirma")
	private String fld_DirecFirma;

	@Column(name="Fld_DivisionCC")
	private String fld_DivisionCC;

	@Column(name="Fld_EmailFirma")
	private String fld_EmailFirma;

	@Column(name="Fld_FaxFirma")
	private String fld_FaxFirma;

	@Column(name="Fld_FecCreacion")
	private Timestamp fld_FecCreacion;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecVcto")
	private Timestamp fld_FecVcto;

	@Column(name="Fld_Flag1")
	private boolean fld_Flag1;

	@Column(name="Fld_Flag2")
	private boolean fld_Flag2;

	@Column(name="Fld_Flag3")
	private boolean fld_Flag3;

	@Column(name="Fld_FlagAutorizaFirma")
	private boolean fld_FlagAutorizaFirma;

	@Column(name="Fld_FlagCertifAcl")
	private boolean fld_FlagCertifAcl;

	@Column(name="Fld_FlagCertifAclEsp")
	private boolean fld_FlagCertifAclEsp;

	@Column(name="Fld_FlagCertifRectif")
	private boolean fld_FlagCertifRectif;

	@Column(name="Fld_FlagEnviaProt")
	private boolean fld_FlagEnviaProt;

	@Column(name="Fld_FonoFirma")
	private String fld_FonoFirma;

	@Column(name="Fld_MatFirma")
	private String fld_MatFirma;

	@Column(name="Fld_NomFirma")
	private String fld_NomFirma;

	@Column(name="Fld_PatFirma")
	private String fld_PatFirma;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_DetFirma() {
	}

	public String getFld_CargoFirma() {
		return this.fld_CargoFirma;
	}

	public void setFld_CargoFirma(String fld_CargoFirma) {
		this.fld_CargoFirma = fld_CargoFirma;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAutoriza() {
		return this.fld_CorrAutoriza;
	}

	public void setFld_CorrAutoriza(BigDecimal fld_CorrAutoriza) {
		this.fld_CorrAutoriza = fld_CorrAutoriza;
	}

	public BigDecimal getFld_CorrFirma_Id() {
		return this.fld_CorrFirma_Id;
	}

	public void setFld_CorrFirma_Id(BigDecimal fld_CorrFirma_Id) {
		this.fld_CorrFirma_Id = fld_CorrFirma_Id;
	}

	public String getFld_DirecFirma() {
		return this.fld_DirecFirma;
	}

	public void setFld_DirecFirma(String fld_DirecFirma) {
		this.fld_DirecFirma = fld_DirecFirma;
	}

	public String getFld_DivisionCC() {
		return this.fld_DivisionCC;
	}

	public void setFld_DivisionCC(String fld_DivisionCC) {
		this.fld_DivisionCC = fld_DivisionCC;
	}

	public String getFld_EmailFirma() {
		return this.fld_EmailFirma;
	}

	public void setFld_EmailFirma(String fld_EmailFirma) {
		this.fld_EmailFirma = fld_EmailFirma;
	}

	public String getFld_FaxFirma() {
		return this.fld_FaxFirma;
	}

	public void setFld_FaxFirma(String fld_FaxFirma) {
		this.fld_FaxFirma = fld_FaxFirma;
	}

	public Timestamp getFld_FecCreacion() {
		return this.fld_FecCreacion;
	}

	public void setFld_FecCreacion(Timestamp fld_FecCreacion) {
		this.fld_FecCreacion = fld_FecCreacion;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecVcto() {
		return this.fld_FecVcto;
	}

	public void setFld_FecVcto(Timestamp fld_FecVcto) {
		this.fld_FecVcto = fld_FecVcto;
	}

	public boolean getFld_Flag1() {
		return this.fld_Flag1;
	}

	public void setFld_Flag1(boolean fld_Flag1) {
		this.fld_Flag1 = fld_Flag1;
	}

	public boolean getFld_Flag2() {
		return this.fld_Flag2;
	}

	public void setFld_Flag2(boolean fld_Flag2) {
		this.fld_Flag2 = fld_Flag2;
	}

	public boolean getFld_Flag3() {
		return this.fld_Flag3;
	}

	public void setFld_Flag3(boolean fld_Flag3) {
		this.fld_Flag3 = fld_Flag3;
	}

	public boolean getFld_FlagAutorizaFirma() {
		return this.fld_FlagAutorizaFirma;
	}

	public void setFld_FlagAutorizaFirma(boolean fld_FlagAutorizaFirma) {
		this.fld_FlagAutorizaFirma = fld_FlagAutorizaFirma;
	}

	public boolean getFld_FlagCertifAcl() {
		return this.fld_FlagCertifAcl;
	}

	public void setFld_FlagCertifAcl(boolean fld_FlagCertifAcl) {
		this.fld_FlagCertifAcl = fld_FlagCertifAcl;
	}

	public boolean getFld_FlagCertifAclEsp() {
		return this.fld_FlagCertifAclEsp;
	}

	public void setFld_FlagCertifAclEsp(boolean fld_FlagCertifAclEsp) {
		this.fld_FlagCertifAclEsp = fld_FlagCertifAclEsp;
	}

	public boolean getFld_FlagCertifRectif() {
		return this.fld_FlagCertifRectif;
	}

	public void setFld_FlagCertifRectif(boolean fld_FlagCertifRectif) {
		this.fld_FlagCertifRectif = fld_FlagCertifRectif;
	}

	public boolean getFld_FlagEnviaProt() {
		return this.fld_FlagEnviaProt;
	}

	public void setFld_FlagEnviaProt(boolean fld_FlagEnviaProt) {
		this.fld_FlagEnviaProt = fld_FlagEnviaProt;
	}

	public String getFld_FonoFirma() {
		return this.fld_FonoFirma;
	}

	public void setFld_FonoFirma(String fld_FonoFirma) {
		this.fld_FonoFirma = fld_FonoFirma;
	}

	public String getFld_MatFirma() {
		return this.fld_MatFirma;
	}

	public void setFld_MatFirma(String fld_MatFirma) {
		this.fld_MatFirma = fld_MatFirma;
	}

	public String getFld_NomFirma() {
		return this.fld_NomFirma;
	}

	public void setFld_NomFirma(String fld_NomFirma) {
		this.fld_NomFirma = fld_NomFirma;
	}

	public String getFld_PatFirma() {
		return this.fld_PatFirma;
	}

	public void setFld_PatFirma(String fld_PatFirma) {
		this.fld_PatFirma = fld_PatFirma;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}