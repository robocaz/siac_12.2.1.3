package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_RutSolCertif_WEB database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RutSolCertif_WEB.findAll", query="SELECT t FROM Tbl_RutSolCertif_WEB t")
public class Tbl_RutSolCertif_WEB implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrCaja")
	private BigDecimal fld_CorrCaja;

	@Column(name="Fld_CorrMovimiento")
	private BigDecimal fld_CorrMovimiento;

	@Column(name="Fld_CorrRutSolic_Id")
	private BigDecimal fld_CorrRutSolic_Id;

	@Column(name="Fld_CorrSolicitud")
	private BigDecimal fld_CorrSolicitud;

	@Column(name="Fld_EnvioEmail")
	private boolean fld_EnvioEmail;

	@Column(name="Fld_EnvioFax")
	private boolean fld_EnvioFax;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipoServicio")
	private String fld_TipoServicio;

	public Tbl_RutSolCertif_WEB() {
	}

	public BigDecimal getFld_CorrCaja() {
		return this.fld_CorrCaja;
	}

	public void setFld_CorrCaja(BigDecimal fld_CorrCaja) {
		this.fld_CorrCaja = fld_CorrCaja;
	}

	public BigDecimal getFld_CorrMovimiento() {
		return this.fld_CorrMovimiento;
	}

	public void setFld_CorrMovimiento(BigDecimal fld_CorrMovimiento) {
		this.fld_CorrMovimiento = fld_CorrMovimiento;
	}

	public BigDecimal getFld_CorrRutSolic_Id() {
		return this.fld_CorrRutSolic_Id;
	}

	public void setFld_CorrRutSolic_Id(BigDecimal fld_CorrRutSolic_Id) {
		this.fld_CorrRutSolic_Id = fld_CorrRutSolic_Id;
	}

	public BigDecimal getFld_CorrSolicitud() {
		return this.fld_CorrSolicitud;
	}

	public void setFld_CorrSolicitud(BigDecimal fld_CorrSolicitud) {
		this.fld_CorrSolicitud = fld_CorrSolicitud;
	}

	public boolean getFld_EnvioEmail() {
		return this.fld_EnvioEmail;
	}

	public void setFld_EnvioEmail(boolean fld_EnvioEmail) {
		this.fld_EnvioEmail = fld_EnvioEmail;
	}

	public boolean getFld_EnvioFax() {
		return this.fld_EnvioFax;
	}

	public void setFld_EnvioFax(boolean fld_EnvioFax) {
		this.fld_EnvioFax = fld_EnvioFax;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public String getFld_TipoServicio() {
		return this.fld_TipoServicio;
	}

	public void setFld_TipoServicio(String fld_TipoServicio) {
		this.fld_TipoServicio = fld_TipoServicio;
	}

}