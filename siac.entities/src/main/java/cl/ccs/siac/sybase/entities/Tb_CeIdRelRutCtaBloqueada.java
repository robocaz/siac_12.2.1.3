package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdRelRutCtaBloqueada database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdRelRutCtaBloqueada.findAll", query="SELECT t FROM Tb_CeIdRelRutCtaBloqueada t")
public class Tb_CeIdRelRutCtaBloqueada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_RutPersona")
	private String fld_RutPersona;

	//bi-directional many-to-one association to Tb_CeIdCtaCteBloqueada
	@ManyToOne
	@JoinColumn(name="Fld_CorrCtaCteBlq")
	private Tb_CeIdCtaCteBloqueada tbCeIdCtaCteBloqueada;

	public Tb_CeIdRelRutCtaBloqueada() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public String getFld_RutPersona() {
		return this.fld_RutPersona;
	}

	public void setFld_RutPersona(String fld_RutPersona) {
		this.fld_RutPersona = fld_RutPersona;
	}

	public Tb_CeIdCtaCteBloqueada getTbCeIdCtaCteBloqueada() {
		return this.tbCeIdCtaCteBloqueada;
	}

	public void setTbCeIdCtaCteBloqueada(Tb_CeIdCtaCteBloqueada tbCeIdCtaCteBloqueada) {
		this.tbCeIdCtaCteBloqueada = tbCeIdCtaCteBloqueada;
	}

}