package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCArbitro database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCArbitro.findAll", query="SELECT t FROM Tbl_BCArbitro t")
public class Tbl_BCArbitro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Direccion")
	private String fld_Direccion;

	@Column(name="Fld_DocResol")
	private String fld_DocResol;

	@Column(name="Fld_Email")
	private String fld_Email;

	@Column(name="Fld_FechaResol")
	private Timestamp fld_FechaResol;

	@Column(name="Fld_FecVigencia")
	private Timestamp fld_FecVigencia;

	@Column(name="Fld_NombreArbitro")
	private String fld_NombreArbitro;

	@Column(name="Fld_NroResolIncorporacion")
	private BigDecimal fld_NroResolIncorporacion;

	@Column(name="Fld_RutArbitro")
	private String fld_RutArbitro;

	@Column(name="Fld_TelefonoFijo")
	private BigDecimal fld_TelefonoFijo;

	@Column(name="Fld_TelefonoMovil")
	private BigDecimal fld_TelefonoMovil;

	@Column(name="Fld_Vigencia")
	private boolean fld_Vigencia;

	public Tbl_BCArbitro() {
	}

	public String getFld_Direccion() {
		return this.fld_Direccion;
	}

	public void setFld_Direccion(String fld_Direccion) {
		this.fld_Direccion = fld_Direccion;
	}

	public String getFld_DocResol() {
		return this.fld_DocResol;
	}

	public void setFld_DocResol(String fld_DocResol) {
		this.fld_DocResol = fld_DocResol;
	}

	public String getFld_Email() {
		return this.fld_Email;
	}

	public void setFld_Email(String fld_Email) {
		this.fld_Email = fld_Email;
	}

	public Timestamp getFld_FechaResol() {
		return this.fld_FechaResol;
	}

	public void setFld_FechaResol(Timestamp fld_FechaResol) {
		this.fld_FechaResol = fld_FechaResol;
	}

	public Timestamp getFld_FecVigencia() {
		return this.fld_FecVigencia;
	}

	public void setFld_FecVigencia(Timestamp fld_FecVigencia) {
		this.fld_FecVigencia = fld_FecVigencia;
	}

	public String getFld_NombreArbitro() {
		return this.fld_NombreArbitro;
	}

	public void setFld_NombreArbitro(String fld_NombreArbitro) {
		this.fld_NombreArbitro = fld_NombreArbitro;
	}

	public BigDecimal getFld_NroResolIncorporacion() {
		return this.fld_NroResolIncorporacion;
	}

	public void setFld_NroResolIncorporacion(BigDecimal fld_NroResolIncorporacion) {
		this.fld_NroResolIncorporacion = fld_NroResolIncorporacion;
	}

	public String getFld_RutArbitro() {
		return this.fld_RutArbitro;
	}

	public void setFld_RutArbitro(String fld_RutArbitro) {
		this.fld_RutArbitro = fld_RutArbitro;
	}

	public BigDecimal getFld_TelefonoFijo() {
		return this.fld_TelefonoFijo;
	}

	public void setFld_TelefonoFijo(BigDecimal fld_TelefonoFijo) {
		this.fld_TelefonoFijo = fld_TelefonoFijo;
	}

	public BigDecimal getFld_TelefonoMovil() {
		return this.fld_TelefonoMovil;
	}

	public void setFld_TelefonoMovil(BigDecimal fld_TelefonoMovil) {
		this.fld_TelefonoMovil = fld_TelefonoMovil;
	}

	public boolean getFld_Vigencia() {
		return this.fld_Vigencia;
	}

	public void setFld_Vigencia(boolean fld_Vigencia) {
		this.fld_Vigencia = fld_Vigencia;
	}

}