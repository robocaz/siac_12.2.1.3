package cl.exe.ccs.dto;

import java.io.Serializable;

public class BoletinDataEmpleador extends BoletinDataDetalle implements Comparable<BoletinDataEmpleador>, Serializable {

	@Override
	public int compareTo(BoletinDataEmpleador o) {
		final int val = super.montoLaboral.compareTo(o.montoLaboral);
		if ( val < 0 ) {
			return -1;
		}
		if ( val > 0 ) {
			return 1;
		}
		if ( periodo < o.periodo ) {
			return -1;
		}
		if ( periodo > o.periodo ) {
			return 1;
		}
		return 0;
	}

}
