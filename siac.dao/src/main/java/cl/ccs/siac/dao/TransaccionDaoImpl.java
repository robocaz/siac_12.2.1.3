package cl.ccs.siac.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.AceptaTransaccionDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;
import cl.exe.ccs.dto.TransaccionDTO;
import cl.exe.ccs.dto.TransaccionGrillaDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;

@Named("transaccionDao")
@Stateless
public class TransaccionDaoImpl implements TransaccionDao {

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	private static final Logger log = LogManager.getLogger(TransaccionDaoImpl.class);


	public ResponseDTO agregarServicio(TransaccionDTO transaccionDTO) {
		transaccionDTO.setIdControl(1);
		try {

			
			log.info("Ejecutando SI_ServiciosTramitante_SW2");			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_ServiciosTramitante_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrProt", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrAcl", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", transaccionDTO.getCorrCaja());
			storedProcedure.setParameter("Fld_CodServicio", transaccionDTO.getCodServicio());
			storedProcedure.setParameter("Fld_RutTramitante", transaccionDTO.getRutTramitante());
			storedProcedure.setParameter("Fld_RutAfectado", transaccionDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrNombre", transaccionDTO.getCorrNombre());
			storedProcedure.setParameter("Fld_CorrProt", transaccionDTO.getCorrProtesto());
			storedProcedure.setParameter("Fld_CorrAcl", transaccionDTO.getCorrAclaracion());

			storedProcedure.execute();

			transaccionDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			transaccionDTO.setIdControl(0);

		} catch (Exception e) {

			log.error("Error en el SP SI_ServiciosTramitante_SW2 " + e.getMessage());
			transaccionDTO.setIdControl(1);
			e.printStackTrace();

		}
		return transaccionDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.TransaccionDao#listarServicio(java.lang.Long)
	 */
	public ResponseListDTO<TransaccionGrillaDTO> listarServicio(Long corrCaja) {

		ResponseListDTO<TransaccionGrillaDTO> respuesta = new ResponseListDTO<TransaccionGrillaDTO>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {

			log.info("Ejecutando SQ_ServiciosTramitante_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ServiciosTramitante_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoPago", String.class, ParameterMode.OUT);
			storedProcedure.setParameter("Fld_CorrCaja", corrCaja);

			storedProcedure.execute();

			List<TransaccionGrillaDTO> lista = new ArrayList<TransaccionGrillaDTO>();

			List<Object[]> listaCajas = storedProcedure.getResultList();

			for (Object item : listaCajas) {
				Object[] datos = (Object[]) item;
				TransaccionGrillaDTO grilla = new TransaccionGrillaDTO();
				grilla.setTipoServicio(datos[0].toString());
				grilla.setCorrCaja(corrCaja);
				grilla.setTipoDocAclaracion(datos[1].toString());
				grilla.setRutAfectado(datos[2].toString());
				grilla.setApellidoPaterno(datos[3].toString());
				grilla.setApellidoMaterno(datos[4].toString());
				grilla.setNombres(datos[5].toString());
				grilla.setCostoServicio(datos[6].toString());
				grilla.setTipoDocumento(datos[7].toString());
				grilla.setMoneda(datos[8].toString());
				grilla.setMontoDocumento(datos[9].toString());
				grilla.setNumeroOperacion(datos[10].toString());
				if (datos[11].toString().substring(0, 10).equals("1900-01-01")) {
					grilla.setFechaProtesto("");
				} else {
					Date date = formatter.parse(datos[11].toString());
					grilla.setFechaProtesto(formatter.format(date));
				}
				grilla.setEmisor(datos[12].toString());
				grilla.setCorrelativoProtesto(datos[13].toString());
				grilla.setCorrelativoTransaccion(datos[14].toString());
				grilla.setCorrelativoMovimiento(datos[15].toString());
				grilla.setCodServicio(datos[16].toString());
				grilla.setCorrelativoPersona(datos[17].toString());
				grilla.setBoletaFactura(datos[18].toString());
				grilla.setNumeroBoletaFactura(datos[19].toString());
				if (datos[20] == null) {
					grilla.setTitular("");
				} else {
					grilla.setTitular(datos[20].toString());
				}
				if (datos[21] == null) {
					grilla.setEstadoMovimiento("");
				} else {
					grilla.setEstadoMovimiento(datos[21].toString());
				}
				grilla.setCodExterno(datos[22].toString().trim());
				grilla.setGlosaBoleta(datos[23].toString().trim());
				lista.add(grilla);
			}

			respuesta.setLista(lista);

			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_TipoPago").toString());

		} catch (Exception e) {
			log.error("Error en el SP SQ_ServiciosTramitante_SW2 " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return respuesta;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.TransaccionDao#cancelarTransaccion(cl.exe.ccs.dto.
	 * AceptaTransaccionDTO)
	 */
	public ResponseDTO cancelarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_EliTransacTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", aceptaTransaccionDTO.getIdCaja());
			storedProcedure.setParameter("Fld_RutTramitante", aceptaTransaccionDTO.getRutTramitante());

			storedProcedure.execute();

			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

		} catch (Exception e) {
			respuesta.setMsgControl("Error en cancelarTransaccion");
			respuesta.setIdControl(1);
			e.printStackTrace();

		}
		return respuesta;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.TransaccionDao#aceptarTransaccion(cl.exe.ccs.dto.
	 * AceptaTransaccionDTO)
	 */
	public ResponseDTO aceptarTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO) {

		ResponseDTO respuesta = new ResponseDTO();

		try {

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SP_AceptaTransacTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_NombrePC", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", aceptaTransaccionDTO.getIdCaja());
			storedProcedure.setParameter("Fld_RutTramitante", aceptaTransaccionDTO.getRutTramitante());
			storedProcedure.setParameter("Fld_NombrePC", aceptaTransaccionDTO.getNombreMaquina());

			storedProcedure.execute();

			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

		} catch (Exception e) {
			aceptaTransaccionDTO.setIdControl(1);
			e.printStackTrace();

		}
		return respuesta;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cl.ccs.siac.dao.TransaccionDao#certificadosTransaccion(cl.exe.ccs.dto.
	 * AceptaTransaccionDTO)
	 */
	@SuppressWarnings("unchecked")
	public ResponseListDTO<CertificadoDTO> certificadosTransaccion(AceptaTransaccionDTO aceptaTransaccionDTO) {
		ResponseListDTO<CertificadoDTO> respuesta = new ResponseListDTO<CertificadoDTO>();
		try {

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_MovTempCertif_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutTramitante", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", aceptaTransaccionDTO.getIdCaja());
			storedProcedure.setParameter("Fld_RutTramitante", aceptaTransaccionDTO.getRutTramitante());

			boolean result = storedProcedure.execute();

			if (result) {
				List<CertificadoDTO> lista = new ArrayList<CertificadoDTO>();

				List<Object[]> listaCer = storedProcedure.getResultList();

				for (Object item : listaCer) {
					Object[] datos = (Object[]) item;
					CertificadoDTO grilla = new CertificadoDTO();
					grilla.setCodServicio(datos[0].toString());
					grilla.setRutAfectado(datos[1].toString());
					grilla.setCorrMovimiento(Long.parseLong(datos[2].toString()));
					grilla.setCorrTransaccion(Long.parseLong(datos[3].toString()));
					grilla.setTipoDocumento(datos[4].toString());
					grilla.setNumeroBoleta(Long.parseLong(datos[5].toString()));
					grilla.setGlosaServicio(datos[6].toString());
					grilla.setFlagImprime(Integer.parseInt(datos[7].toString()));
					lista.add(grilla);
				}

				respuesta.setLista(lista);

				respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
				respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			} else {
				respuesta = null;
			}

		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}
		return respuesta;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.TransaccionDao#eliminarServicio(java.lang.Long,
	 * java.lang.Long)
	 */
	public ResponseDTO eliminarServicio(Long idCaja, Long corrMov) {

		ResponseDTO respuesta = new ResponseDTO();
		try {

			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SD_MovimientoTemp_SW2");

			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_TempCorrMov", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			storedProcedure.setParameter("Fld_CorrCaja", idCaja);
			storedProcedure.setParameter("Fld_TempCorrMov", corrMov);

			storedProcedure.execute();

			respuesta.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			respuesta.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

		} catch (Exception e) {
			e.printStackTrace();

		}
		return respuesta;

	}
}
