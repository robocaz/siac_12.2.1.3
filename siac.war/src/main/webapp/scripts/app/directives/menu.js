'use strict';

angular.module('sitwar').directive('menu',function(){
	return {
		restrict: 'E',
		templateUrl : 'menu.html?nocache='+ new Date().getTime(),
		scope: true
	};
});