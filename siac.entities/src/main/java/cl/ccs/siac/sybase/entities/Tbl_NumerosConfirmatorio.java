package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_NumerosConfirmatorios database table.
 * 
 */
@Entity
@Table(name="Tbl_NumerosConfirmatorios")
@NamedQuery(name="Tbl_NumerosConfirmatorio.findAll", query="SELECT t FROM Tbl_NumerosConfirmatorio t")
public class Tbl_NumerosConfirmatorio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrVigNC")
	private BigDecimal fld_CorrVigNC;

	@Column(name="Fld_FecMarcaUso")
	private Timestamp fld_FecMarcaUso;

	@Column(name="Fld_FolioNro")
	private short fld_FolioNro;

	@Column(name="Fld_MarcaUso")
	private boolean fld_MarcaUso;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_Usuario")
	private String fld_Usuario;

	public Tbl_NumerosConfirmatorio() {
	}

	public BigDecimal getFld_CorrVigNC() {
		return this.fld_CorrVigNC;
	}

	public void setFld_CorrVigNC(BigDecimal fld_CorrVigNC) {
		this.fld_CorrVigNC = fld_CorrVigNC;
	}

	public Timestamp getFld_FecMarcaUso() {
		return this.fld_FecMarcaUso;
	}

	public void setFld_FecMarcaUso(Timestamp fld_FecMarcaUso) {
		this.fld_FecMarcaUso = fld_FecMarcaUso;
	}

	public short getFld_FolioNro() {
		return this.fld_FolioNro;
	}

	public void setFld_FolioNro(short fld_FolioNro) {
		this.fld_FolioNro = fld_FolioNro;
	}

	public boolean getFld_MarcaUso() {
		return this.fld_MarcaUso;
	}

	public void setFld_MarcaUso(boolean fld_MarcaUso) {
		this.fld_MarcaUso = fld_MarcaUso;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public String getFld_Usuario() {
		return this.fld_Usuario;
	}

	public void setFld_Usuario(String fld_Usuario) {
		this.fld_Usuario = fld_Usuario;
	}

}