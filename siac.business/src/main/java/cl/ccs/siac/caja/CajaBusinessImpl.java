package cl.ccs.siac.caja;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.CajaDao;
import cl.exe.ccs.dto.CajaDTO;
import cl.exe.ccs.dto.CajaPendienteDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("cajaBusiness")
public class CajaBusinessImpl implements CajaBusiness {
	
	@Inject
	private CajaDao cajaDao;
	
	@Override
	public CajaDTO cantidadAbiertas(String usuario, String codCentroCosto) {
		return cajaDao.cantidadAbiertas(usuario, codCentroCosto);
	}

	@Override
	public ResponseListDTO<CajaDTO> listarCajasAbiertas(String usuario, String codCentroCosto) {
		return cajaDao.listarCajasAbiertas(usuario, codCentroCosto);
	}

	@Override
	public CajaDTO aperturaCaja(String usuario, String codCentroCosto) {
		return cajaDao.aperturaCaja(usuario, codCentroCosto);
	}
	
	@Override
	public String tomarCaja(String CorrCaja, byte[] timeStamp, String username){
		return cajaDao.tomarCaja(CorrCaja, timeStamp, username);
	}
	
	@Override
	public Boolean cerrarCajaTemporal(String CorrCaja, String usuario){
		return cajaDao.cerrarCajaTemporal(CorrCaja, usuario);
	}
	
	@Override
	public Boolean cerrarCajaDefinitivo(String CorrCaja, String usuario){
		return cajaDao.cerrarCajaDefinitivo(CorrCaja, usuario);
	}
	
	public ResponseListDTO<CajaDTO> listarCajasTodas(String usuario){
		return cajaDao.listarCajasTodas( usuario);
	}
	
	@Override
	public ResponseListDTO<CajaDTO> listarCajasDelDiaPorPrivilegio(String codCentroCosto, String usuario, int tipoUsuario) {
		return cajaDao.listarCajasDelDiaPorPrivilegio(codCentroCosto, usuario, tipoUsuario);
	}

	
	public CajaPendienteDTO cajasAbiertaPendiente(String usuario){
		return cajaDao.cajasAbiertaPendiente(usuario);
	}
	
	public CajaDTO numeroCajasAbiertas(String usuario, String codCentroCosto){
		return cajaDao.numeroCajasAbiertas(usuario, codCentroCosto);
	}
}
