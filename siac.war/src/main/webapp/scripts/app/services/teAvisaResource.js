angular.module('ccs').factory('TeAvisaResource', function ($resource) {
    var resource = $resource('rest/teavisa',{
        nocache : new Date().getTime()
    },{
        'contratar' :   {  method : 'POST', isArray : false, url: 'rest/teavisa/contratar'},
        'validaContrato' :   {  method : 'GET', params:{rut:'rut'}, isArray : false, url: 'rest/teavisa/validacontrato'},
        'buscaContratos' :   {  method : 'GET', isArray : true, params:{rut:'rut'}, url: 'rest/teavisa/buscacontratos'},
        'buscaContratosRenovacion' :   {  method : 'GET', isArray : true, params:{rut:'rut', criterio: 'criterio', correlativo:'correlativo'}, url: 'rest/teavisa/buscacontratosrenovacion'},
        'cargaSucursales' :   {  method : 'GET', params:{tipoEmisor:'tipoEmisor', codEmisor:'codEmisor'}, isArray : true, url: 'rest/aclaracion/sucursales'},
        'tiposDocumentos' :   {  method : 'GET', isArray : true, url: 'rest/aclaracion/tiposdocumentos'}

    });
    return resource;
});
