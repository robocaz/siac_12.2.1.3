package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TBL_PALABRASINVALIDAS database table.
 * 
 */
@Entity
@Table(name="TBL_PALABRASINVALIDAS")
@NamedQuery(name="TblPalabrasinvalida.findAll", query="SELECT t FROM TblPalabrasinvalida t")
public class TblPalabrasinvalida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODIGO")
	private BigDecimal fldCodigo;

	@Column(name="FLD_PALABRA")
	private String fldPalabra;

	@Column(name="FLD_TIPOVALIDACION")
	private BigDecimal fldTipovalidacion;

	public TblPalabrasinvalida() {
	}

	public BigDecimal getFldCodigo() {
		return this.fldCodigo;
	}

	public void setFldCodigo(BigDecimal fldCodigo) {
		this.fldCodigo = fldCodigo;
	}

	public String getFldPalabra() {
		return this.fldPalabra;
	}

	public void setFldPalabra(String fldPalabra) {
		this.fldPalabra = fldPalabra;
	}

	public BigDecimal getFldTipovalidacion() {
		return this.fldTipovalidacion;
	}

	public void setFldTipovalidacion(BigDecimal fldTipovalidacion) {
		this.fldTipovalidacion = fldTipovalidacion;
	}

}