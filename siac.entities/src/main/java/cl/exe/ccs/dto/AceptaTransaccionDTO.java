package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AceptaTransaccionDTO extends ResponseDTO implements Serializable{

	private Long idCaja;
	private String rutTramitante;
	private Long corrNombre;
	private String nombreMaquina; 
	
	
	public Long getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(Long idCaja) {
		this.idCaja = idCaja;
	}
	public String getRutTramitante() {
		return rutTramitante;
	}
	public void setRutTramitante(String rutTramitante) {
		this.rutTramitante = rutTramitante;
	}
	public Long getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Long corrNombre) {
		this.corrNombre = corrNombre;
	}
	public String getNombreMaquina() {
		return nombreMaquina;
	}
	public void setNombreMaquina(String nombreMaquina) {
		this.nombreMaquina = nombreMaquina;
	}
	
	
	
	
	
	
}
