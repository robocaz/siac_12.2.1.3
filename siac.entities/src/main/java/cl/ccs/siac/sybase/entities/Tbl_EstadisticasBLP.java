package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EstadisticasBLP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_EstadisticasBLP.findAll", query="SELECT t FROM Tbl_EstadisticasBLP t")
public class Tbl_EstadisticasBLP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Cantidad")
	private int fld_Cantidad;

	@Column(name="Fld_CorrEstadist_Id")
	private BigDecimal fld_CorrEstadist_Id;

	@Column(name="Fld_Fecha")
	private Timestamp fld_Fecha;

	@Column(name="Fld_LPoBOL")
	private String fld_LPoBOL;

	@Column(name="Fld_MontoPlano")
	private BigDecimal fld_MontoPlano;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_PagMax")
	private short fld_PagMax;

	@Column(name="Fld_PagMin")
	private short fld_PagMin;

	@Column(name="Fld_TipoRegBol")
	private String fld_TipoRegBol;

	public Tbl_EstadisticasBLP() {
	}

	public int getFld_Cantidad() {
		return this.fld_Cantidad;
	}

	public void setFld_Cantidad(int fld_Cantidad) {
		this.fld_Cantidad = fld_Cantidad;
	}

	public BigDecimal getFld_CorrEstadist_Id() {
		return this.fld_CorrEstadist_Id;
	}

	public void setFld_CorrEstadist_Id(BigDecimal fld_CorrEstadist_Id) {
		this.fld_CorrEstadist_Id = fld_CorrEstadist_Id;
	}

	public Timestamp getFld_Fecha() {
		return this.fld_Fecha;
	}

	public void setFld_Fecha(Timestamp fld_Fecha) {
		this.fld_Fecha = fld_Fecha;
	}

	public String getFld_LPoBOL() {
		return this.fld_LPoBOL;
	}

	public void setFld_LPoBOL(String fld_LPoBOL) {
		this.fld_LPoBOL = fld_LPoBOL;
	}

	public BigDecimal getFld_MontoPlano() {
		return this.fld_MontoPlano;
	}

	public void setFld_MontoPlano(BigDecimal fld_MontoPlano) {
		this.fld_MontoPlano = fld_MontoPlano;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public short getFld_PagMax() {
		return this.fld_PagMax;
	}

	public void setFld_PagMax(short fld_PagMax) {
		this.fld_PagMax = fld_PagMax;
	}

	public short getFld_PagMin() {
		return this.fld_PagMin;
	}

	public void setFld_PagMin(short fld_PagMin) {
		this.fld_PagMin = fld_PagMin;
	}

	public String getFld_TipoRegBol() {
		return this.fld_TipoRegBol;
	}

	public void setFld_TipoRegBol(String fld_TipoRegBol) {
		this.fld_TipoRegBol = fld_TipoRegBol;
	}

}