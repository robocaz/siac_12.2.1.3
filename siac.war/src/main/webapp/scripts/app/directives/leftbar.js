'use strict';
angular.module('ccs').directive('leftbar',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/leftbar.html?nocache='+ new Date().getTime()
    };
});
