package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tb_CeIdDetErrImpedCtaCte database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetErrImpedCtaCte.findAll", query="SELECT t FROM Tb_CeIdDetErrImpedCtaCte t")
public class Tb_CeIdDetErrImpedCtaCte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	//bi-directional many-to-one association to Tb_CeIdDetImpedidosCtaCte
	@ManyToOne
	@JoinColumn(name="Fld_CorrImpedido")
	private Tb_CeIdDetImpedidosCtaCte tbCeIdDetImpedidosCtaCte;

	public Tb_CeIdDetErrImpedCtaCte() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public Tb_CeIdDetImpedidosCtaCte getTbCeIdDetImpedidosCtaCte() {
		return this.tbCeIdDetImpedidosCtaCte;
	}

	public void setTbCeIdDetImpedidosCtaCte(Tb_CeIdDetImpedidosCtaCte tbCeIdDetImpedidosCtaCte) {
		this.tbCeIdDetImpedidosCtaCte = tbCeIdDetImpedidosCtaCte;
	}

}