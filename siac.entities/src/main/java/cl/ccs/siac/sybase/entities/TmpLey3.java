package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tmp_ley3 database table.
 * 
 */
@Entity
@Table(name="tmp_ley3")
@NamedQuery(name="TmpLey3.findAll", query="SELECT t FROM TmpLey3 t")
public class TmpLey3 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CantBIC")
	private short cantBIC;

	@Column(name="CantMol")
	private short cantMol;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="SumaBIC")
	private BigDecimal sumaBIC;

	@Column(name="SumaMol")
	private BigDecimal sumaMol;

	@Column(name="SumaTotal")
	private BigDecimal sumaTotal;

	public TmpLey3() {
	}

	public short getCantBIC() {
		return this.cantBIC;
	}

	public void setCantBIC(short cantBIC) {
		this.cantBIC = cantBIC;
	}

	public short getCantMol() {
		return this.cantMol;
	}

	public void setCantMol(short cantMol) {
		this.cantMol = cantMol;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public BigDecimal getSumaBIC() {
		return this.sumaBIC;
	}

	public void setSumaBIC(BigDecimal sumaBIC) {
		this.sumaBIC = sumaBIC;
	}

	public BigDecimal getSumaMol() {
		return this.sumaMol;
	}

	public void setSumaMol(BigDecimal sumaMol) {
		this.sumaMol = sumaMol;
	}

	public BigDecimal getSumaTotal() {
		return this.sumaTotal;
	}

	public void setSumaTotal(BigDecimal sumaTotal) {
		this.sumaTotal = sumaTotal;
	}

}