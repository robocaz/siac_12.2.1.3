package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tb_CeIdDetErrDigDef database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdDetErrDigDef.findAll", query="SELECT t FROM Tb_CeIdDetErrDigDef t")
public class Tb_CeIdDetErrDigDef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCampo")
	private short fld_CodCampo;

	@Column(name="Fld_CodError")
	private short fld_CodError;

	@Column(name="Fld_CorrDigDef")
	private BigDecimal fld_CorrDigDef;

	public Tb_CeIdDetErrDigDef() {
	}

	public short getFld_CodCampo() {
		return this.fld_CodCampo;
	}

	public void setFld_CodCampo(short fld_CodCampo) {
		this.fld_CodCampo = fld_CodCampo;
	}

	public short getFld_CodError() {
		return this.fld_CodError;
	}

	public void setFld_CodError(short fld_CodError) {
		this.fld_CodError = fld_CodError;
	}

	public BigDecimal getFld_CorrDigDef() {
		return this.fld_CorrDigDef;
	}

	public void setFld_CorrDigDef(BigDecimal fld_CorrDigDef) {
		this.fld_CorrDigDef = fld_CorrDigDef;
	}

}