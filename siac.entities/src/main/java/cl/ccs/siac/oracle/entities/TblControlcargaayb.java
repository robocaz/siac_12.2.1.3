package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_CONTROLCARGAAYB database table.
 * 
 */
@Entity
@Table(name="TBL_CONTROLCARGAAYB")
@NamedQuery(name="TblControlcargaayb.findAll", query="SELECT t FROM TblControlcargaayb t")
public class TblControlcargaayb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_CODPROCESO")
	private BigDecimal fldCodproceso;

	@Column(name="FLD_CORRCONTROLCARGA_ID")
	private BigDecimal fldCorrcontrolcargaId;

	@Column(name="FLD_CORRENVIOCARGA")
	private BigDecimal fldCorrenviocarga;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAINICIO")
	private Date fldFechainicio;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHATERMINO")
	private Date fldFechatermino;

	@Column(name="FLD_REGCORRECTOS")
	private BigDecimal fldRegcorrectos;

	@Column(name="FLD_REGERRONEOS")
	private BigDecimal fldRegerroneos;

	@Column(name="FLD_REGLEIDOS")
	private BigDecimal fldRegleidos;

	@Column(name="FLD_REGLEIDOSBAJAS")
	private BigDecimal fldRegleidosbajas;

	@Column(name="FLD_REGLEIDOSCARGA")
	private BigDecimal fldRegleidoscarga;

	@Column(name="FLD_REGPROCALTAS")
	private BigDecimal fldRegprocaltas;

	@Column(name="FLD_REGPROCBAJASACL")
	private BigDecimal fldRegprocbajasacl;

	@Column(name="FLD_REGPROCBAJASBLQ")
	private BigDecimal fldRegprocbajasblq;

	@Column(name="FLD_REGPROCBAJASNORMAL")
	private BigDecimal fldRegprocbajasnormal;

	public TblControlcargaayb() {
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public BigDecimal getFldCodproceso() {
		return this.fldCodproceso;
	}

	public void setFldCodproceso(BigDecimal fldCodproceso) {
		this.fldCodproceso = fldCodproceso;
	}

	public BigDecimal getFldCorrcontrolcargaId() {
		return this.fldCorrcontrolcargaId;
	}

	public void setFldCorrcontrolcargaId(BigDecimal fldCorrcontrolcargaId) {
		this.fldCorrcontrolcargaId = fldCorrcontrolcargaId;
	}

	public BigDecimal getFldCorrenviocarga() {
		return this.fldCorrenviocarga;
	}

	public void setFldCorrenviocarga(BigDecimal fldCorrenviocarga) {
		this.fldCorrenviocarga = fldCorrenviocarga;
	}

	public Date getFldFechainicio() {
		return this.fldFechainicio;
	}

	public void setFldFechainicio(Date fldFechainicio) {
		this.fldFechainicio = fldFechainicio;
	}

	public Date getFldFechatermino() {
		return this.fldFechatermino;
	}

	public void setFldFechatermino(Date fldFechatermino) {
		this.fldFechatermino = fldFechatermino;
	}

	public BigDecimal getFldRegcorrectos() {
		return this.fldRegcorrectos;
	}

	public void setFldRegcorrectos(BigDecimal fldRegcorrectos) {
		this.fldRegcorrectos = fldRegcorrectos;
	}

	public BigDecimal getFldRegerroneos() {
		return this.fldRegerroneos;
	}

	public void setFldRegerroneos(BigDecimal fldRegerroneos) {
		this.fldRegerroneos = fldRegerroneos;
	}

	public BigDecimal getFldRegleidos() {
		return this.fldRegleidos;
	}

	public void setFldRegleidos(BigDecimal fldRegleidos) {
		this.fldRegleidos = fldRegleidos;
	}

	public BigDecimal getFldRegleidosbajas() {
		return this.fldRegleidosbajas;
	}

	public void setFldRegleidosbajas(BigDecimal fldRegleidosbajas) {
		this.fldRegleidosbajas = fldRegleidosbajas;
	}

	public BigDecimal getFldRegleidoscarga() {
		return this.fldRegleidoscarga;
	}

	public void setFldRegleidoscarga(BigDecimal fldRegleidoscarga) {
		this.fldRegleidoscarga = fldRegleidoscarga;
	}

	public BigDecimal getFldRegprocaltas() {
		return this.fldRegprocaltas;
	}

	public void setFldRegprocaltas(BigDecimal fldRegprocaltas) {
		this.fldRegprocaltas = fldRegprocaltas;
	}

	public BigDecimal getFldRegprocbajasacl() {
		return this.fldRegprocbajasacl;
	}

	public void setFldRegprocbajasacl(BigDecimal fldRegprocbajasacl) {
		this.fldRegprocbajasacl = fldRegprocbajasacl;
	}

	public BigDecimal getFldRegprocbajasblq() {
		return this.fldRegprocbajasblq;
	}

	public void setFldRegprocbajasblq(BigDecimal fldRegprocbajasblq) {
		this.fldRegprocbajasblq = fldRegprocbajasblq;
	}

	public BigDecimal getFldRegprocbajasnormal() {
		return this.fldRegprocbajasnormal;
	}

	public void setFldRegprocbajasnormal(BigDecimal fldRegprocbajasnormal) {
		this.fldRegprocbajasnormal = fldRegprocbajasnormal;
	}

}