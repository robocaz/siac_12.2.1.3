'use strict';
angular.module('ccs').directive('modalanularboletafactura',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalanularboletafactura.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalaperturacaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalaperturacaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcambiarcontrasena',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcambiarcontrasena.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcambioimpresora',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcambioimpresora.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcentrocosto',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcentrocosto.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcerrarcaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcerrarcaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalfactura',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalfactura.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalfraude',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalfraude.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalfraudeaclaracion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalfraudeaclaracion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalinforme',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinforme.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalaclaracion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalaclaracion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modallistacaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modallistacaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalmodificarmediopago',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalmodificarmediopago.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalmodpagocheque',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalmodpagocheque.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalmodpagoredcompra',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalmodpagoredcompra.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcambiarcaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcambiarcaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalmodpagotarjetacredito',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalmodpagotarjetacredito.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('pagovalevista',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagovista.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalmodpagovalevista',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalmodpagovalevista.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalpagocheque',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagocheque.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcarropagocheque',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagocheque.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modalpagotarjetacredito',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagotarjetacredito.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalpagoredcompra',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagoredcompra.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalpagovista',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpagovista.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalrangoboleta',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalrangoboleta.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modalrangoboletaedicion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalrangoboletaedicion.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modalreporteboleta',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalreporteboleta.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaltblresuldocumento',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaltblresuldocumento.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalvalidacion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalvalidacion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfirmarcambiarcaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfirmarcambiarcaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfirmarsalircarrocompra',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfirmarsalircarrocompra.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfirmarcierrecaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfirmarcierrecaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modallistacierrecaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modallistacierrecaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalobservacion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalobservacion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfirmarcierresesion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfirmarcierresesion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfirmarcierrecajassesion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfirmarcierrecajassesion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modallistacierrecajasesion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modallistacierrecajasesion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalaceptatransaccion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalaceptatransaccion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalboleta',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalboleta.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalboletareimp',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalboletareprint.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalpdflistado',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalpdflistado.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalreimprimir',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalreimprimir.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalreemboletafactura',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalreemboletafactura.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalreemboletafacturadescarga',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalreemboletafacturadescarga.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalverfirma',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalverfirma.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('imgfirma',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalfirma.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modalcargando',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcargando.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalfintransaccion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalfintransaccion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modallistatarifas',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modallistatarifas.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalinformecomercial1',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinformecomercial1.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modallistapublico',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modallistapublico.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalinformelegal',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinformelegal.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcertificadototal1',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcertificadototal1.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcertificadototal2',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcertificadototal2.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconsolidadoaclaraciontramite',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconsolidadoaclaraciontramite.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalinformeaclaracionproceso',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinformeaclaracionproceso.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalinformeaclaracionproceso2',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinformeaclaracionproceso2.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalprotestosencontrados',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalprotestosencontrados.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcuotasmorosasanteriores',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcuotasmorosasanteriores.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalventacartera',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalventacartera.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalaclaraciontramiteagrupant',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalaclaraciontramiteagrupant.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcertificadoaclaraciontramite',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcertificadoaclaraciontramite.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcertificadoaclaracion4',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcertificadoaclaracion4.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldeclaraciondeuso',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldeclaraciondeuso.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalbusquedaprotestos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalbusquedaprotestos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleprotesto',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleprotesto.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleaclaracion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleaclaracion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalboletinlaboral',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalboletinlaboral.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalproductosvigentes',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalproductosvigentes.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalupgradeteavisa',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalupgradeteavisa.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalingresofiniquitos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalingresofiniquitos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalbcteavisa',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalbcteavisa.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalboletincontrato',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalboletincontrato.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalboletinrenovacion',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalboletinrenovacion.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalbcbusqueda',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalbcbusqueda.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalsolicitudreclamos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalsolicitudreclamos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalseleccionformulario',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalseleccionformulario.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalingresosolicitudreclamos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalingresosolicitudreclamos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalbusquedasolicitudreclamos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalbusquedasolicitudreclamos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalestadosolicitud',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalestadosolicitud.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalarqueocaja',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalarqueocaja.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalingresodepositos',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalingresodepositos.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalconfiguraciones',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalconfiguraciones.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcuadratura',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcuadratura.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalcuadraturaindex',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalcuadraturaindex.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleboleta',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleboleta.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleboletaindex',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleboletaindex.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modaldetalletransaccionboleta',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalletransaccionboleta.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalletransaccionboletaindex',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalletransaccionboletaindex.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modaldetalletransaccionfactura',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalletransaccionfactura.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalletransaccionfacturaindex',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalletransaccionfacturaindex.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleservicios',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleservicios.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleserviciosindex',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleserviciosindex.html?nocache='+ new Date().getTime()
    };
});


angular.module('ccs').directive('modalinformecc',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalinformecc.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modalreportecamara',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modalreportecamara.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetallecc',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetallecc.html?nocache='+ new Date().getTime()
    };
});

angular.module('ccs').directive('modaldetalleusuario',function(){
    return {
        restrict: 'EA',
        templateUrl : 'views/modal/modaldetalleusuario.html?nocache='+ new Date().getTime()
    };
});