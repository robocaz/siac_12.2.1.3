package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_SistemaConsLey20575 database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_SistemaConsLey20575.findAll", query="SELECT t FROM Tbl_SistemaConsLey20575 t")
public class Tbl_SistemaConsLey20575 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodResponsable")
	private short fld_CodResponsable;

	@Column(name="Fld_CodSistema")
	private short fld_CodSistema;

	@Column(name="Fld_GlosaSistema")
	private String fld_GlosaSistema;

	public Tbl_SistemaConsLey20575() {
	}

	public short getFld_CodResponsable() {
		return this.fld_CodResponsable;
	}

	public void setFld_CodResponsable(short fld_CodResponsable) {
		this.fld_CodResponsable = fld_CodResponsable;
	}

	public short getFld_CodSistema() {
		return this.fld_CodSistema;
	}

	public void setFld_CodSistema(short fld_CodSistema) {
		this.fld_CodSistema = fld_CodSistema;
	}

	public String getFld_GlosaSistema() {
		return this.fld_GlosaSistema;
	}

	public void setFld_GlosaSistema(String fld_GlosaSistema) {
		this.fld_GlosaSistema = fld_GlosaSistema;
	}

}