package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tp_CeIdProfesiones database table.
 * 
 */
@Entity
@Table(name="Tp_CeIdProfesiones")
@NamedQuery(name="Tp_CeIdProfesione.findAll", query="SELECT t FROM Tp_CeIdProfesione t")
public class Tp_CeIdProfesione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodProfesion")
	private short fld_CodProfesion;

	@Column(name="Fld_GlosaProfesion")
	private String fld_GlosaProfesion;

	public Tp_CeIdProfesione() {
	}

	public short getFld_CodProfesion() {
		return this.fld_CodProfesion;
	}

	public void setFld_CodProfesion(short fld_CodProfesion) {
		this.fld_CodProfesion = fld_CodProfesion;
	}

	public String getFld_GlosaProfesion() {
		return this.fld_GlosaProfesion;
	}

	public void setFld_GlosaProfesion(String fld_GlosaProfesion) {
		this.fld_GlosaProfesion = fld_GlosaProfesion;
	}

}