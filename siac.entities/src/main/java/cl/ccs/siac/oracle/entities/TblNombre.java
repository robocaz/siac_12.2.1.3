package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the TBL_NOMBRE database table.
 * 
 */
@Entity
@Table(name="TBL_NOMBRE")
@NamedQuery(name="TblNombre.findAll", query="SELECT t FROM TblNombre t")
public class TblNombre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_NOMBRE_FLDCORRNOMBREID_GENERATOR", sequenceName="FLD_CORRNOMBRE_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_NOMBRE_FLDCORRNOMBREID_GENERATOR")
	@Column(name="FLD_CORRNOMBRE_ID")
	private long fldCorrnombreId;

	@Column(name="FLD_APMATERNO")
	private String fldApmaterno;

	@Column(name="FLD_APPATERNO")
	private String fldAppaterno;

	@Column(name="FLD_CALIDADJURIDICA")
	private String fldCalidadjuridica;

	@Column(name="FLD_CODFUENTE")
	private BigDecimal fldCodfuente;

	@Column(name="FLD_FECHAACTUALIZACION")
	private Timestamp fldFechaactualizacion;

	@Column(name="FLD_NOMBRES")
	private String fldNombres;

	@Column(name="FLD_RUTPERSONA")
	private String fldRutpersona;

	@Column(name="FLD_TIPONOMBRE")
	private BigDecimal fldTiponombre;

	@Column(name="FLD_USUARIOACTUALIZACION")
	private String fldUsuarioactualizacion;

	//bi-directional many-to-one association to TblRutmorhisto
	@OneToMany(mappedBy="tblNombre")
	private List<TblRutmorhisto> tblRutmorhistos;

	//bi-directional many-to-one association to TblRutmorosidad
	@OneToMany(mappedBy="tblNombre")
	private List<TblRutmorosidad> tblRutmorosidads;

	public TblNombre() {
	}

	public long getFldCorrnombreId() {
		return this.fldCorrnombreId;
	}

	public void setFldCorrnombreId(long fldCorrnombreId) {
		this.fldCorrnombreId = fldCorrnombreId;
	}

	public String getFldApmaterno() {
		return this.fldApmaterno;
	}

	public void setFldApmaterno(String fldApmaterno) {
		this.fldApmaterno = fldApmaterno;
	}

	public String getFldAppaterno() {
		return this.fldAppaterno;
	}

	public void setFldAppaterno(String fldAppaterno) {
		this.fldAppaterno = fldAppaterno;
	}

	public String getFldCalidadjuridica() {
		return this.fldCalidadjuridica;
	}

	public void setFldCalidadjuridica(String fldCalidadjuridica) {
		this.fldCalidadjuridica = fldCalidadjuridica;
	}

	public BigDecimal getFldCodfuente() {
		return this.fldCodfuente;
	}

	public void setFldCodfuente(BigDecimal fldCodfuente) {
		this.fldCodfuente = fldCodfuente;
	}

	public Timestamp getFldFechaactualizacion() {
		return this.fldFechaactualizacion;
	}

	public void setFldFechaactualizacion(Timestamp fldFechaactualizacion) {
		this.fldFechaactualizacion = fldFechaactualizacion;
	}

	public String getFldNombres() {
		return this.fldNombres;
	}

	public void setFldNombres(String fldNombres) {
		this.fldNombres = fldNombres;
	}

	public String getFldRutpersona() {
		return this.fldRutpersona;
	}

	public void setFldRutpersona(String fldRutpersona) {
		this.fldRutpersona = fldRutpersona;
	}

	public BigDecimal getFldTiponombre() {
		return this.fldTiponombre;
	}

	public void setFldTiponombre(BigDecimal fldTiponombre) {
		this.fldTiponombre = fldTiponombre;
	}

	public String getFldUsuarioactualizacion() {
		return this.fldUsuarioactualizacion;
	}

	public void setFldUsuarioactualizacion(String fldUsuarioactualizacion) {
		this.fldUsuarioactualizacion = fldUsuarioactualizacion;
	}

	public List<TblRutmorhisto> getTblRutmorhistos() {
		return this.tblRutmorhistos;
	}

	public void setTblRutmorhistos(List<TblRutmorhisto> tblRutmorhistos) {
		this.tblRutmorhistos = tblRutmorhistos;
	}

	public TblRutmorhisto addTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().add(tblRutmorhisto);
		tblRutmorhisto.setTblNombre(this);

		return tblRutmorhisto;
	}

	public TblRutmorhisto removeTblRutmorhisto(TblRutmorhisto tblRutmorhisto) {
		getTblRutmorhistos().remove(tblRutmorhisto);
		tblRutmorhisto.setTblNombre(null);

		return tblRutmorhisto;
	}

	public List<TblRutmorosidad> getTblRutmorosidads() {
		return this.tblRutmorosidads;
	}

	public void setTblRutmorosidads(List<TblRutmorosidad> tblRutmorosidads) {
		this.tblRutmorosidads = tblRutmorosidads;
	}

	public TblRutmorosidad addTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().add(tblRutmorosidad);
		tblRutmorosidad.setTblNombre(this);

		return tblRutmorosidad;
	}

	public TblRutmorosidad removeTblRutmorosidad(TblRutmorosidad tblRutmorosidad) {
		getTblRutmorosidads().remove(tblRutmorosidad);
		tblRutmorosidad.setTblNombre(null);

		return tblRutmorosidad;
	}

}