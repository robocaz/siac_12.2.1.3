package cl.exe.ccs.filtros;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

/**
 * Servlet Filter implementation class CcsFilter
 */
@WebFilter("/CcsFilter")
public class CcsFilter implements Filter {

	private static final String CCS_GUID = "ccs_guid";

	/**
	 * Default constructor.
	 */
	public CcsFilter() {
		super();
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {

	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final String principal = this.getPrincipal(request);

		if (principal != null) {
			response.reset();
			response.resetBuffer();
			chain.doFilter(request, response);
		} else {
			final PrintWriter out = response.getWriter();
			response.setContentType(MediaType.APPLICATION_JSON);
			out.println("{\"success\":false, \"redirectToLogin\":true \"}");
		}

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

	}

	private String getPrincipal(final ServletRequest servletRequest) {
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		String userLogin = null;

		if (request.getSession() != null) {
			userLogin = (String) request.getSession().getAttribute(CCS_GUID);
		}

		return userLogin;
	}

}
