package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Vi_BolLabTrabajador database table.
 * 
 */
@Entity
@NamedQuery(name="Vi_BolLabTrabajador.findAll", query="SELECT v FROM Vi_BolLabTrabajador v")
public class Vi_BolLabTrabajador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAcreedor")
	private String fld_CodAcreedor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodInfraccion")
	private String fld_CodInfraccion;

	@Column(name="Fld_CorrBolLabTrab_Id")
	private int fld_CorrBolLabTrab_Id;

	@Column(name="Fld_CorrDeudaLab")
	private BigDecimal fld_CorrDeudaLab;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_MontoDeudaLaboral")
	private BigDecimal fld_MontoDeudaLaboral;

	@Column(name="Fld_MontoDeudaLabUTM")
	private BigDecimal fld_MontoDeudaLabUTM;

	@Column(name="Fld_NombreTrabajador")
	private String fld_NombreTrabajador;

	@Column(name="Fld_NroBolLaboral")
	private short fld_NroBolLaboral;

	@Column(name="Fld_PagBolLaboral")
	private int fld_PagBolLaboral;

	@Column(name="Fld_Periodo")
	private int fld_Periodo;

	@Column(name="Fld_RutInfractor")
	private String fld_RutInfractor;

	@Column(name="Fld_RutTrabajador")
	private String fld_RutTrabajador;

	public Vi_BolLabTrabajador() {
	}

	public String getFld_CodAcreedor() {
		return this.fld_CodAcreedor;
	}

	public void setFld_CodAcreedor(String fld_CodAcreedor) {
		this.fld_CodAcreedor = fld_CodAcreedor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public String getFld_CodInfraccion() {
		return this.fld_CodInfraccion;
	}

	public void setFld_CodInfraccion(String fld_CodInfraccion) {
		this.fld_CodInfraccion = fld_CodInfraccion;
	}

	public int getFld_CorrBolLabTrab_Id() {
		return this.fld_CorrBolLabTrab_Id;
	}

	public void setFld_CorrBolLabTrab_Id(int fld_CorrBolLabTrab_Id) {
		this.fld_CorrBolLabTrab_Id = fld_CorrBolLabTrab_Id;
	}

	public BigDecimal getFld_CorrDeudaLab() {
		return this.fld_CorrDeudaLab;
	}

	public void setFld_CorrDeudaLab(BigDecimal fld_CorrDeudaLab) {
		this.fld_CorrDeudaLab = fld_CorrDeudaLab;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public BigDecimal getFld_MontoDeudaLaboral() {
		return this.fld_MontoDeudaLaboral;
	}

	public void setFld_MontoDeudaLaboral(BigDecimal fld_MontoDeudaLaboral) {
		this.fld_MontoDeudaLaboral = fld_MontoDeudaLaboral;
	}

	public BigDecimal getFld_MontoDeudaLabUTM() {
		return this.fld_MontoDeudaLabUTM;
	}

	public void setFld_MontoDeudaLabUTM(BigDecimal fld_MontoDeudaLabUTM) {
		this.fld_MontoDeudaLabUTM = fld_MontoDeudaLabUTM;
	}

	public String getFld_NombreTrabajador() {
		return this.fld_NombreTrabajador;
	}

	public void setFld_NombreTrabajador(String fld_NombreTrabajador) {
		this.fld_NombreTrabajador = fld_NombreTrabajador;
	}

	public short getFld_NroBolLaboral() {
		return this.fld_NroBolLaboral;
	}

	public void setFld_NroBolLaboral(short fld_NroBolLaboral) {
		this.fld_NroBolLaboral = fld_NroBolLaboral;
	}

	public int getFld_PagBolLaboral() {
		return this.fld_PagBolLaboral;
	}

	public void setFld_PagBolLaboral(int fld_PagBolLaboral) {
		this.fld_PagBolLaboral = fld_PagBolLaboral;
	}

	public int getFld_Periodo() {
		return this.fld_Periodo;
	}

	public void setFld_Periodo(int fld_Periodo) {
		this.fld_Periodo = fld_Periodo;
	}

	public String getFld_RutInfractor() {
		return this.fld_RutInfractor;
	}

	public void setFld_RutInfractor(String fld_RutInfractor) {
		this.fld_RutInfractor = fld_RutInfractor;
	}

	public String getFld_RutTrabajador() {
		return this.fld_RutTrabajador;
	}

	public void setFld_RutTrabajador(String fld_RutTrabajador) {
		this.fld_RutTrabajador = fld_RutTrabajador;
	}

}