package cl.exe.ccs.enums;

/**
 * @author emiranda
 *
 */
public enum TipoCertificado {

	ISA("ISA"), ILP("ILP"), ILG("ILG"), CT1("CT1"), CT2("CT2"), CT3("CT3"), CAC("CAC"), BLI("BLI"), BLR("BLR"), BLT("BLT"), CAA(
			"CAA"), ICM("ICM"), CAR("CAR"), ICT("ICT"), ARM("ARM"), AMM("AMM"), ADM("ADM"), RAM("RAM"), RMM("RMM"), RDM("RDM"), AUM("AUM");

	private final String tipoCert;

	/**
	 * @param tipoCert
	 */
	private TipoCertificado(final String tipoCert) {
		this.tipoCert = tipoCert;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return tipoCert;
	}

}
