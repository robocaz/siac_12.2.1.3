package cl.ccs.siac.tramitante;

import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.TramitanteCajaDTO;
import cl.exe.ccs.dto.TramitanteDTO;

public interface TramitanteBusiness {
 
	
	public TramitanteDTO obtenerTramitante(String rut);
	
	public TramitanteDTO actualizarTramitante(TramitanteDTO tramitanteDTO);
	
	public TramitanteDTO obtenerTramitante(Long corrCaja);
	
	public TramitanteCajaDTO obtenerDeclaracionUso(TramitanteCajaDTO tramitanteCajaDTO);
	
	public ResponseDTO declaracionUso(TramitanteCajaDTO tramitanteCajaDTO);
	
	public TramitanteCajaDTO obtieneDatosDeUso(TramitanteCajaDTO tramitanteCajaDTO);

}
