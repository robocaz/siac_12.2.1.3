package cl.exe.ccs.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InicioConfigDTO extends ResponseDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 276601115258625999L;
	private boolean emiteBoleta;
	private boolean emiteFactura;
	private boolean biometria;
	private String puertoImpresora;
	
	
	
	
	
	public InicioConfigDTO() {
		super();
	}
	public InicioConfigDTO(boolean emiteBoleta, boolean emiteFactura, boolean biometria, String puertoImpresora) {
		super();
		this.emiteBoleta = emiteBoleta;
		this.emiteFactura = emiteFactura;
		this.biometria = biometria;
		this.puertoImpresora = puertoImpresora;
	}
	public boolean isEmiteBoleta() {
		return emiteBoleta;
	}
	public void setEmiteBoleta(boolean emiteBoleta) {
		this.emiteBoleta = emiteBoleta;
	}
	public boolean isEmiteFactura() {
		return emiteFactura;
	}
	public void setEmiteFactura(boolean emiteFactura) {
		this.emiteFactura = emiteFactura;
	}
	public boolean isBiometria() {
		return biometria;
	}
	public void setBiometria(boolean biometria) {
		this.biometria = biometria;
	}
	public String getPuertoImpresora() {
		return puertoImpresora;
	}
	public void setPuertoImpresora(String puertoImpresora) {
		this.puertoImpresora = puertoImpresora;
	}
	
	
	
	
	
	
	
	
	

}
