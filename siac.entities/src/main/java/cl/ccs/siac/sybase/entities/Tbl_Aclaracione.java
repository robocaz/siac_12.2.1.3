package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_Aclaraciones database table.
 * 
 */
@Entity
@Table(name="Tbl_Aclaraciones")
@NamedQuery(name="Tbl_Aclaracione.findAll", query="SELECT t FROM Tbl_Aclaracione t")
public class Tbl_Aclaracione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodEstadoAcl")
	private short fld_CodEstadoAcl;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_CodSucursal")
	private BigDecimal fld_CodSucursal;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CodUsuarioModif")
	private String fld_CodUsuarioModif;

	@Column(name="Fld_CorrAcl_Id")
	private BigDecimal fld_CorrAcl_Id;

	@Column(name="Fld_CorrEnvioAcl")
	private BigDecimal fld_CorrEnvioAcl;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	@Column(name="Fld_DigitoVerificador")
	private String fld_DigitoVerificador;

	@Column(name="Fld_FecDigitacion")
	private Timestamp fld_FecDigitacion;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_FecPago")
	private Timestamp fld_FecPago;

	@Column(name="Fld_FecProt")
	private Timestamp fld_FecProt;

	@Column(name="Fld_Flag_1")
	private boolean fld_Flag_1;

	@Column(name="Fld_Flag_2")
	private boolean fld_Flag_2;

	@Column(name="Fld_Flag_3")
	private boolean fld_Flag_3;

	@Column(name="Fld_Flag_4")
	private boolean fld_Flag_4;

	@Column(name="Fld_Flag_5")
	private boolean fld_Flag_5;

	@Column(name="Fld_Flag_Origen")
	private boolean fld_Flag_Origen;

	@Column(name="Fld_MarcaAclaracion")
	private byte fld_MarcaAclaracion;

	@Column(name="Fld_MarcaReaceptacion")
	private boolean fld_MarcaReaceptacion;

	@Column(name="Fld_MontoCancelado")
	private int fld_MontoCancelado;

	@Column(name="Fld_MontoProt")
	private BigDecimal fld_MontoProt;

	@Column(name="Fld_NroBoletinAcl")
	private short fld_NroBoletinAcl;

	@Column(name="Fld_NroConfirmatorio")
	private int fld_NroConfirmatorio;

	@Column(name="Fld_NroLote")
	private double fld_NroLote;

	@Column(name="Fld_NroOper4Dig")
	private short fld_NroOper4Dig;

	@Column(name="Fld_PagBoletinAcl")
	private short fld_PagBoletinAcl;

	@Column(name="Fld_PtjePareo")
	private BigDecimal fld_PtjePareo;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	@Column(name="Fld_RutFirma")
	private String fld_RutFirma;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TipCert")
	private byte fld_TipCert;

	@Column(name="Fld_TipoDocAclaracion")
	private String fld_TipoDocAclaracion;

	@Column(name="Fld_TipoDocumento")
	private String fld_TipoDocumento;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	@Column(name="Fld_TipoPareo")
	private short fld_TipoPareo;

	@Column(name="Fld_TomoBoletinAcl")
	private short fld_TomoBoletinAcl;

	public Tbl_Aclaracione() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public short getFld_CodEstadoAcl() {
		return this.fld_CodEstadoAcl;
	}

	public void setFld_CodEstadoAcl(short fld_CodEstadoAcl) {
		this.fld_CodEstadoAcl = fld_CodEstadoAcl;
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public BigDecimal getFld_CodSucursal() {
		return this.fld_CodSucursal;
	}

	public void setFld_CodSucursal(BigDecimal fld_CodSucursal) {
		this.fld_CodSucursal = fld_CodSucursal;
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public String getFld_CodUsuarioModif() {
		return this.fld_CodUsuarioModif;
	}

	public void setFld_CodUsuarioModif(String fld_CodUsuarioModif) {
		this.fld_CodUsuarioModif = fld_CodUsuarioModif;
	}

	public BigDecimal getFld_CorrAcl_Id() {
		return this.fld_CorrAcl_Id;
	}

	public void setFld_CorrAcl_Id(BigDecimal fld_CorrAcl_Id) {
		this.fld_CorrAcl_Id = fld_CorrAcl_Id;
	}

	public BigDecimal getFld_CorrEnvioAcl() {
		return this.fld_CorrEnvioAcl;
	}

	public void setFld_CorrEnvioAcl(BigDecimal fld_CorrEnvioAcl) {
		this.fld_CorrEnvioAcl = fld_CorrEnvioAcl;
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

	public String getFld_DigitoVerificador() {
		return this.fld_DigitoVerificador;
	}

	public void setFld_DigitoVerificador(String fld_DigitoVerificador) {
		this.fld_DigitoVerificador = fld_DigitoVerificador;
	}

	public Timestamp getFld_FecDigitacion() {
		return this.fld_FecDigitacion;
	}

	public void setFld_FecDigitacion(Timestamp fld_FecDigitacion) {
		this.fld_FecDigitacion = fld_FecDigitacion;
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public Timestamp getFld_FecPago() {
		return this.fld_FecPago;
	}

	public void setFld_FecPago(Timestamp fld_FecPago) {
		this.fld_FecPago = fld_FecPago;
	}

	public Timestamp getFld_FecProt() {
		return this.fld_FecProt;
	}

	public void setFld_FecProt(Timestamp fld_FecProt) {
		this.fld_FecProt = fld_FecProt;
	}

	public boolean getFld_Flag_1() {
		return this.fld_Flag_1;
	}

	public void setFld_Flag_1(boolean fld_Flag_1) {
		this.fld_Flag_1 = fld_Flag_1;
	}

	public boolean getFld_Flag_2() {
		return this.fld_Flag_2;
	}

	public void setFld_Flag_2(boolean fld_Flag_2) {
		this.fld_Flag_2 = fld_Flag_2;
	}

	public boolean getFld_Flag_3() {
		return this.fld_Flag_3;
	}

	public void setFld_Flag_3(boolean fld_Flag_3) {
		this.fld_Flag_3 = fld_Flag_3;
	}

	public boolean getFld_Flag_4() {
		return this.fld_Flag_4;
	}

	public void setFld_Flag_4(boolean fld_Flag_4) {
		this.fld_Flag_4 = fld_Flag_4;
	}

	public boolean getFld_Flag_5() {
		return this.fld_Flag_5;
	}

	public void setFld_Flag_5(boolean fld_Flag_5) {
		this.fld_Flag_5 = fld_Flag_5;
	}

	public boolean getFld_Flag_Origen() {
		return this.fld_Flag_Origen;
	}

	public void setFld_Flag_Origen(boolean fld_Flag_Origen) {
		this.fld_Flag_Origen = fld_Flag_Origen;
	}

	public byte getFld_MarcaAclaracion() {
		return this.fld_MarcaAclaracion;
	}

	public void setFld_MarcaAclaracion(byte fld_MarcaAclaracion) {
		this.fld_MarcaAclaracion = fld_MarcaAclaracion;
	}

	public boolean getFld_MarcaReaceptacion() {
		return this.fld_MarcaReaceptacion;
	}

	public void setFld_MarcaReaceptacion(boolean fld_MarcaReaceptacion) {
		this.fld_MarcaReaceptacion = fld_MarcaReaceptacion;
	}

	public int getFld_MontoCancelado() {
		return this.fld_MontoCancelado;
	}

	public void setFld_MontoCancelado(int fld_MontoCancelado) {
		this.fld_MontoCancelado = fld_MontoCancelado;
	}

	public BigDecimal getFld_MontoProt() {
		return this.fld_MontoProt;
	}

	public void setFld_MontoProt(BigDecimal fld_MontoProt) {
		this.fld_MontoProt = fld_MontoProt;
	}

	public short getFld_NroBoletinAcl() {
		return this.fld_NroBoletinAcl;
	}

	public void setFld_NroBoletinAcl(short fld_NroBoletinAcl) {
		this.fld_NroBoletinAcl = fld_NroBoletinAcl;
	}

	public int getFld_NroConfirmatorio() {
		return this.fld_NroConfirmatorio;
	}

	public void setFld_NroConfirmatorio(int fld_NroConfirmatorio) {
		this.fld_NroConfirmatorio = fld_NroConfirmatorio;
	}

	public double getFld_NroLote() {
		return this.fld_NroLote;
	}

	public void setFld_NroLote(double fld_NroLote) {
		this.fld_NroLote = fld_NroLote;
	}

	public short getFld_NroOper4Dig() {
		return this.fld_NroOper4Dig;
	}

	public void setFld_NroOper4Dig(short fld_NroOper4Dig) {
		this.fld_NroOper4Dig = fld_NroOper4Dig;
	}

	public short getFld_PagBoletinAcl() {
		return this.fld_PagBoletinAcl;
	}

	public void setFld_PagBoletinAcl(short fld_PagBoletinAcl) {
		this.fld_PagBoletinAcl = fld_PagBoletinAcl;
	}

	public BigDecimal getFld_PtjePareo() {
		return this.fld_PtjePareo;
	}

	public void setFld_PtjePareo(BigDecimal fld_PtjePareo) {
		this.fld_PtjePareo = fld_PtjePareo;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

	public String getFld_RutFirma() {
		return this.fld_RutFirma;
	}

	public void setFld_RutFirma(String fld_RutFirma) {
		this.fld_RutFirma = fld_RutFirma;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public byte getFld_TipCert() {
		return this.fld_TipCert;
	}

	public void setFld_TipCert(byte fld_TipCert) {
		this.fld_TipCert = fld_TipCert;
	}

	public String getFld_TipoDocAclaracion() {
		return this.fld_TipoDocAclaracion;
	}

	public void setFld_TipoDocAclaracion(String fld_TipoDocAclaracion) {
		this.fld_TipoDocAclaracion = fld_TipoDocAclaracion;
	}

	public String getFld_TipoDocumento() {
		return this.fld_TipoDocumento;
	}

	public void setFld_TipoDocumento(String fld_TipoDocumento) {
		this.fld_TipoDocumento = fld_TipoDocumento;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

	public short getFld_TipoPareo() {
		return this.fld_TipoPareo;
	}

	public void setFld_TipoPareo(short fld_TipoPareo) {
		this.fld_TipoPareo = fld_TipoPareo;
	}

	public short getFld_TomoBoletinAcl() {
		return this.fld_TomoBoletinAcl;
	}

	public void setFld_TomoBoletinAcl(short fld_TomoBoletinAcl) {
		this.fld_TomoBoletinAcl = fld_TomoBoletinAcl;
	}

}