package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_FechasPeriodo database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_FechasPeriodo.findAll", query="SELECT t FROM Tbl_FechasPeriodo t")
public class Tbl_FechasPeriodo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodEmisor")
	private String fld_CodEmisor;

	@Column(name="Fld_FecInicioCalculada")
	private Timestamp fld_FecInicioCalculada;

	@Column(name="Fld_FecInicioInformado")
	private Timestamp fld_FecInicioInformado;

	@Column(name="Fld_FecTerminoCalculada")
	private Timestamp fld_FecTerminoCalculada;

	@Column(name="Fld_FecTerminoInformado")
	private Timestamp fld_FecTerminoInformado;

	@Column(name="Fld_TipoEmisor")
	private String fld_TipoEmisor;

	public Tbl_FechasPeriodo() {
	}

	public String getFld_CodEmisor() {
		return this.fld_CodEmisor;
	}

	public void setFld_CodEmisor(String fld_CodEmisor) {
		this.fld_CodEmisor = fld_CodEmisor;
	}

	public Timestamp getFld_FecInicioCalculada() {
		return this.fld_FecInicioCalculada;
	}

	public void setFld_FecInicioCalculada(Timestamp fld_FecInicioCalculada) {
		this.fld_FecInicioCalculada = fld_FecInicioCalculada;
	}

	public Timestamp getFld_FecInicioInformado() {
		return this.fld_FecInicioInformado;
	}

	public void setFld_FecInicioInformado(Timestamp fld_FecInicioInformado) {
		this.fld_FecInicioInformado = fld_FecInicioInformado;
	}

	public Timestamp getFld_FecTerminoCalculada() {
		return this.fld_FecTerminoCalculada;
	}

	public void setFld_FecTerminoCalculada(Timestamp fld_FecTerminoCalculada) {
		this.fld_FecTerminoCalculada = fld_FecTerminoCalculada;
	}

	public Timestamp getFld_FecTerminoInformado() {
		return this.fld_FecTerminoInformado;
	}

	public void setFld_FecTerminoInformado(Timestamp fld_FecTerminoInformado) {
		this.fld_FecTerminoInformado = fld_FecTerminoInformado;
	}

	public String getFld_TipoEmisor() {
		return this.fld_TipoEmisor;
	}

	public void setFld_TipoEmisor(String fld_TipoEmisor) {
		this.fld_TipoEmisor = fld_TipoEmisor;
	}

}