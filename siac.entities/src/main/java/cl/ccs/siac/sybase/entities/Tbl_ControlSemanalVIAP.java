package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlSemanalVIAP database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlSemanalVIAP.findAll", query="SELECT t FROM Tbl_ControlSemanalVIAP t")
public class Tbl_ControlSemanalVIAP implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_CorrVirtual_Id")
	private BigDecimal fld_CorrVirtual_Id;

	@Column(name="Fld_FecInicioVirtual")
	private Timestamp fld_FecInicioVirtual;

	@Column(name="Fld_FecTerminoVirtual")
	private Timestamp fld_FecTerminoVirtual;

	@Column(name="Fld_FlagGeneracion")
	private byte fld_FlagGeneracion;

	@Column(name="Fld_NroBoletin")
	private short fld_NroBoletin;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_TotProtProcesados")
	private int fld_TotProtProcesados;

	@Column(name="Fld_TotProtRechazados")
	private int fld_TotProtRechazados;

	@Column(name="Fld_TotProtVigentes")
	private int fld_TotProtVigentes;

	@Column(name="Fld_TotRutProcesados")
	private int fld_TotRutProcesados;

	public Tbl_ControlSemanalVIAP() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public BigDecimal getFld_CorrVirtual_Id() {
		return this.fld_CorrVirtual_Id;
	}

	public void setFld_CorrVirtual_Id(BigDecimal fld_CorrVirtual_Id) {
		this.fld_CorrVirtual_Id = fld_CorrVirtual_Id;
	}

	public Timestamp getFld_FecInicioVirtual() {
		return this.fld_FecInicioVirtual;
	}

	public void setFld_FecInicioVirtual(Timestamp fld_FecInicioVirtual) {
		this.fld_FecInicioVirtual = fld_FecInicioVirtual;
	}

	public Timestamp getFld_FecTerminoVirtual() {
		return this.fld_FecTerminoVirtual;
	}

	public void setFld_FecTerminoVirtual(Timestamp fld_FecTerminoVirtual) {
		this.fld_FecTerminoVirtual = fld_FecTerminoVirtual;
	}

	public byte getFld_FlagGeneracion() {
		return this.fld_FlagGeneracion;
	}

	public void setFld_FlagGeneracion(byte fld_FlagGeneracion) {
		this.fld_FlagGeneracion = fld_FlagGeneracion;
	}

	public short getFld_NroBoletin() {
		return this.fld_NroBoletin;
	}

	public void setFld_NroBoletin(short fld_NroBoletin) {
		this.fld_NroBoletin = fld_NroBoletin;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public int getFld_TotProtProcesados() {
		return this.fld_TotProtProcesados;
	}

	public void setFld_TotProtProcesados(int fld_TotProtProcesados) {
		this.fld_TotProtProcesados = fld_TotProtProcesados;
	}

	public int getFld_TotProtRechazados() {
		return this.fld_TotProtRechazados;
	}

	public void setFld_TotProtRechazados(int fld_TotProtRechazados) {
		this.fld_TotProtRechazados = fld_TotProtRechazados;
	}

	public int getFld_TotProtVigentes() {
		return this.fld_TotProtVigentes;
	}

	public void setFld_TotProtVigentes(int fld_TotProtVigentes) {
		this.fld_TotProtVigentes = fld_TotProtVigentes;
	}

	public int getFld_TotRutProcesados() {
		return this.fld_TotRutProcesados;
	}

	public void setFld_TotRutProcesados(int fld_TotRutProcesados) {
		this.fld_TotRutProcesados = fld_TotRutProcesados;
	}

}