package cl.exe.ccs.dto;

import java.io.Serializable;

public class ConsultaCAADTO extends ResponseDTO implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -4635359944376894486L;
	private Long fecAclaracion;
    private String rutAfectado;
    private Long corrCaja;
    private Integer nroBoletaFactura;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private Long corrTransaccion;
    private Integer cantidad;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public Long getFecAclaracion() {
		return fecAclaracion;
	}
	public void setFecAclaracion(Long fecAclaracion) {
		this.fecAclaracion = fecAclaracion;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Integer getNroBoletaFactura() {
		return nroBoletaFactura;
	}
	public void setNroBoletaFactura(Integer nroBoletaFactura) {
		this.nroBoletaFactura = nroBoletaFactura;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public Long getCorrTransaccion() {
		return corrTransaccion;
	}
	public void setCorrTransaccion(Long corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
}
