package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_LocPublicacion database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_LocPublicacion.findAll", query="SELECT t FROM Tbl_LocPublicacion t")
public class Tbl_LocPublicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodCCosto")
	private short fld_CodCCosto;

	@Column(name="Fld_CodLocPub")
	private int fld_CodLocPub;

	@Column(name="Fld_CodRegion")
	private byte fld_CodRegion;

	@Column(name="Fld_GlosaLocPub")
	private String fld_GlosaLocPub;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_LocPublicacion() {
	}

	public short getFld_CodCCosto() {
		return this.fld_CodCCosto;
	}

	public void setFld_CodCCosto(short fld_CodCCosto) {
		this.fld_CodCCosto = fld_CodCCosto;
	}

	public int getFld_CodLocPub() {
		return this.fld_CodLocPub;
	}

	public void setFld_CodLocPub(int fld_CodLocPub) {
		this.fld_CodLocPub = fld_CodLocPub;
	}

	public byte getFld_CodRegion() {
		return this.fld_CodRegion;
	}

	public void setFld_CodRegion(byte fld_CodRegion) {
		this.fld_CodRegion = fld_CodRegion;
	}

	public String getFld_GlosaLocPub() {
		return this.fld_GlosaLocPub;
	}

	public void setFld_GlosaLocPub(String fld_GlosaLocPub) {
		this.fld_GlosaLocPub = fld_GlosaLocPub;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}