package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_ControlFoliosSII database table.
 * 
 */
@Embeddable
public class Tbl_ControlFoliosSIIPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrFolio_Id")
	private long fld_CorrFolio_Id;

	@Column(name="Fld_TipoDTE")
	private byte fld_TipoDTE;

	public Tbl_ControlFoliosSIIPK() {
	}
	public long getFld_CorrFolio_Id() {
		return this.fld_CorrFolio_Id;
	}
	public void setFld_CorrFolio_Id(long fld_CorrFolio_Id) {
		this.fld_CorrFolio_Id = fld_CorrFolio_Id;
	}
	public byte getFld_TipoDTE() {
		return this.fld_TipoDTE;
	}
	public void setFld_TipoDTE(byte fld_TipoDTE) {
		this.fld_TipoDTE = fld_TipoDTE;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_ControlFoliosSIIPK)) {
			return false;
		}
		Tbl_ControlFoliosSIIPK castOther = (Tbl_ControlFoliosSIIPK)other;
		return 
			(this.fld_CorrFolio_Id == castOther.fld_CorrFolio_Id)
			&& (this.fld_TipoDTE == castOther.fld_TipoDTE);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.fld_CorrFolio_Id ^ (this.fld_CorrFolio_Id >>> 32)));
		hash = hash * prime + ((int) this.fld_TipoDTE);
		
		return hash;
	}
}