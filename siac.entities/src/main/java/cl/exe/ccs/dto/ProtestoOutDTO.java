package cl.exe.ccs.dto;

import java.io.Serializable;
import java.util.List;

public class ProtestoOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 5802690892086134924L;

	private String nombreAfectado;
	private Integer totReg;
	private List<ProtestoData> lista;

	public String getNombreAfectado() {
		return nombreAfectado;
	}

	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}

	public Integer getTotReg() {
		return totReg;
	}

	public void setTotReg(Integer totReg) {
		this.totReg = totReg;
	}

	public List<ProtestoData> getLista() {
		return lista;
	}

	public void setLista(List<ProtestoData> lista) {
		this.lista = lista;
	}

	@Override
	public String toString() { 
		final StringBuilder str = new StringBuilder();
		str.append("\n nombreAfectado: " + nombreAfectado);
		str.append(", totReg: " + totReg);
		str.append(", retorno: " + super.idControl);
		str.append(", mensaje: " + super.msgControl);
		str.append(", lista: " + lista);
		return str.toString();
	}

}
