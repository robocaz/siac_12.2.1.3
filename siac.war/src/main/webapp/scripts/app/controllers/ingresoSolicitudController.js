angular.module('ccs').controller('IngresoSolicitudController', function($scope, PersonaResource, TramitanteResource, SolicitudResource, $location, $filter, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.show = false;
	$scope.afectado = {};
	$scope.opcion = '1';
	$scope.ingr = {};
	$scope.ingr.regiones = {};
	$scope.ingr.comunas = {};
	$scope.busq = {};
	$scope.lista = {};
	$scope.numero = '';
	$scope.lstHist = {};
	$scope.impr = {};
	$scope.impr.opc = '1';
	
	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);
	
	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;
	$scope.esRutEmpJuridico = false;
	$scope.otros = false;

	$('#solicitudreclamos').on('show.bs.modal', function (e) {
		$timeout(function(){
			if ( !existeCaja() ) {
				swal('Atención','Debe iniciar una caja y cargar data de tramitante válida previo al proceso','warning');
				$('#solicitudreclamos').modal('hide');
				return;
			}
			if ( !existeTramitante() ) {
				swal('Atención','Debe tener un tramitante para realizar esta operación.','warning');
				$('#solicitudreclamos').modal('hide');
				return;
			}
			$scope.init();
		});
	});

	$scope.limpiarAfectado = function() {
		$scope.afectado.apellidoPaterno = null;
		$scope.afectado.apellidoMaterno = null;
		$scope.afectado.nombres 		= null;
		$scope.afectado.nombreafectado	= null;
		$scope.esrutjuridico = false;
		$scope.editable = false;
	};

	function existeCaja() {
		if ( isUndefinedNullEmpty($rootScope.usuario.caja) || $rootScope.usuario.caja === '0' ) {
			$scope.show = false;
		} else {
			$scope.show = true;
		}
		return $scope.show;
	}

	function existeTramitante() {
		if ( isUndefinedOrNull($rootScope.tramitante) || isUndefinedNullEmpty($rootScope.tramitante.rut) || 
				isUndefinedNullEmpty($rootScope.tramitante.nombres) ) {
			$scope.show = false;
		} else {
			$scope.show = true;
		}
		return $scope.show;
	}

	$scope.init = function () {
		console.log("-> Solicitud..");
		console.log("Correlativo caja: " + $rootScope.tramitante.corrCaja);
		console.log($rootScope.tramitante);
		$("#rutAfectadoInforme").focus();
		$scope.impr.opc = '1';
		cargaRegiones();
		$scope.cargaComunas();
		cargaTipoSolicitudes();
		$scope.cargaCategSolicitudes();
	}

	function cargaRegiones() {
		var successCallback = function(data,responseHeaders){
			$scope.ingr.regiones = data;
		}
		TramitanteResource.cargaRegiones(successCallback, $scope.errorCallback);
	}
	
	$scope.cargaComunas = function() {
		if ( !isUndefinedOrNull($scope.ingr.region) && !isUndefinedNullEmpty($scope.ingr.region.codigo) ) {
			var successCallback = function(data,responseHeaders){
				$scope.ingr.comunas = data;
				if ( !isUndefinedOrNull($scope.afectado.comuna) && !isUndefinedNullEmpty($scope.afectado.comuna.codigo) ) {
					$scope.ingr.comuna = $filter('filter')($scope.ingr.comunas, {codigo: $scope.afectado.comuna.codigo})[0];
				}
			}
			TramitanteResource.cargaComunas({idRegion: $scope.ingr.region.codigo}, successCallback, $scope.errorCallback);
		} else {
			$scope.ingr.comunas = {};
			$scope.ingr.comuna = {};
		}
	};
	
	function cargaTipoSolicitudes() {
		var successCallback = function (data, responseHeaders) {
			$scope.ingr.cmbTipo = data;
		}
		SolicitudResource.cmbtipo(successCallback, $scope.errorCallback);
	}
	
	$scope.cargaCategSolicitudes = function() {
		if ( !isUndefinedOrNull($scope.ingr.tipoSol) && !isUndefinedNullEmpty($scope.ingr.tipoSol.codigo) ) {
			var successCallback = function(data,responseHeaders){
				$scope.ingr.cmbCateg = data;
				$scope.ingr.categoria = data[0];
				$scope.checkOtros();
			}
			SolicitudResource.cmbcateg({idSolic: $scope.ingr.tipoSol.codigo}, successCallback, $scope.errorCallback);
		} else {
			$scope.ingr.cmbCateg = {};
		}
	};

	$scope.checkOtros = function() {
		if ( !isUndefinedOrNull($scope.ingr.categoria) && !isUndefinedNullEmpty($scope.ingr.categoria.descripcion) ) {
			let desc = $scope.ingr.categoria.descripcion.toUpperCase();
			if ( desc === "OTRO" ) {
				$scope.otros = true;
				return;
			} else {
				$scope.ingr.obs = '';
			}
		}
		$scope.otros = false;
	};

	$scope.aceptar = function() {
		switch($scope.opcion) {
		case '1':
			if ( validaAfectado() ) {
				poblarIngreso();
				angular.element("#ingresosolicitudreclamos").modal();
			}
			break;
		case '2':
			if ( validaAfectado() ) {
				buscarSolicitudes();
				angular.element("#busquedasolicitudreclamos").modal();
			}
			break;
		case '3':
			if ( validaAfectado() ) {
				//poblarIngreso();
				angular.element("#seleccionformulario").modal();
			}
			break;
		}
	};

	function validaAfectado() {
		
		if ( isUndefinedNullEmpty($scope.afectado.rut) ) {
			swal('Error','Debe ingresar Rut','error');
			return false;
		}
		if ( $scope.esrutjuridico ) {
			if ( isUndefinedNullEmpty($scope.afectado.nombreafectado) ) {
				swal('Error','Debe ingresar Nombre Afectado','error');
				return false;
			}
		} else {
			if ( isUndefinedNullEmpty($scope.afectado.apellidoPaterno) ) {
				swal('Error','Debe ingresar Apellido Paterno','error');
				return false;
			}
			if ( isUndefinedNullEmpty($scope.afectado.nombres) ) {
				swal('Error','Debe ingresar Nombres','error');
				return false;
			}
		}
		
		return true;
	}

	function esValido() {
		if ( $scope.esvalido ) {
			return true;
		} else {
			swal('Error','Debe ingresar un Afectado','error');
		}
		return false;
	}

	function poblarIngreso() {
		console.log("Poblar afectado..");
		$scope.ingr.rut = $scope.afectado.rut;
		$scope.ingr.apellidop = $scope.afectado.apellidoPaterno;
		$scope.ingr.apellidom = $scope.afectado.apellidoMaterno;
		$scope.ingr.nombres = $scope.afectado.nombres;
		$scope.ingr.email = $scope.afectado.email;
		$scope.ingr.telefono = $scope.afectado.telefono;
		if ( !isUndefinedOrNull($scope.afectado.region) && !isUndefinedNullEmpty($scope.afectado.region.codigo) ) {
			$scope.ingr.region = $filter('filter')($scope.ingr.regiones, {codigo: $scope.afectado.region.codigo})[0];
			$scope.cargaComunas();
		} else {
			$scope.ingr.region = {};
		}
		$scope.ingr.direccion = $scope.afectado.direccion;
	}

	function buscarSolicitudes() {
		console.log("> Buscando..");
		
		$scope.busq.rut = $scope.afectado.rut;
		
		if ( $scope.esrutjuridico ) {
			$scope.busq.nombre = $scope.afectado.apellidoPaterno + $scope.afectado.apellidoMaterno + $scope.afectado.nombres;
		} else {
			$scope.busq.apellidop = $scope.afectado.apellidoPaterno;
			$scope.busq.apellidom = $scope.afectado.apellidoMaterno;
			$scope.busq.nombres = $scope.afectado.nombres;
		}
		
		$scope.trx = {
			rutTitular : $scope.afectado.rut
		}
		
		console.log("> Data Trx: ");
		console.log($scope.trx);
		
		var successCallback = function(data, responseHeaders) {
			console.log("-> Data Resp: ");
			console.log(data);
			$scope.lista = data.lista;
		};
		SolicitudResource.buscar($scope.trx, successCallback, $scope.errorCallback);
	}

	$scope.cerrar = function(flag) {
		switch(flag) {
		case 0:
			reset();
			break;
		case 1:
			angular.element("#ingresosolicitudreclamos").modal("hide");
			resetIngreso();
			break;
		case 2:
			angular.element("#busquedasolicitudreclamos").modal("hide");
			break;
		case 21:
			angular.element("#estadosolicitud").modal("hide");
			break;
		case 3:
			$scope.impr.opc = '1';
			angular.element("#seleccionformulario").modal("hide");
			break;
		}
	};

	function reset() {
		$scope.limpiarAfectado();
		$scope.afectado.rut = null;
		$scope.esvalido = false;
		$scope.opcion = '1';
		$scope.show = false;
		$scope.ingr = {};
		$scope.busq = {};
		$scope.lista = {};
		$scope.numero = '';
		$scope.lstHist = {};
		$scope.impr = {};
	}

	function resetIngreso() {
		$scope.ingr.incluyedoc = false;
		$scope.ingr.cantdocs = '';
		$scope.ingr.categoria = null;
		$scope.ingr.tipoSol = null;
		$scope.ingr.obs = '';
	}

	$scope.buscaAfectado = function() {
		$scope.limpiarAfectado();
		$scope.esvalido = false;
		
		if ( $scope.afectado.rut === undefined ) {
			return;
		}
		if ( $scope.afectado.rut === null ) {
			$scope.afectado.rut = undefined;
			swal({ title:'Error', text:'Rut no es válido', type:'error', showCloseButton:true , confirmButtonText:'cerrar'}).then(
				function() {
					$("#rutAfectadoInforme").focus();
				});
			return;
		}
		
		if ( esRutValido($scope.afectado.rut) ) {
			
			if ( !esRutJuridico($scope.afectado.rut) ) {
				$scope.esrutjuridico = false;
			} else {
				$scope.esrutjuridico = true;
			}
			$scope.obtenerAfectado();
			
		} else {
			swal({ title:'Error', text:'Rut no es válido', type:'error', showCloseButton:true, confirmButtonText:'cerrar'});
			$scope.afectado.rut = null;
		}
	}

	$scope.obtenerAfectado = function(){
		var successCallback = function(data,responseHeaders){
			$scope.esvalido = true;
			console.log("> Corr. afectado: " + data.correlativo);
			if ( data.correlativo == "0" ) {
				$scope.editable = true;
				$scope.afectado.apellidoPaterno = null;
				$scope.afectado.apellidoMaterno = null;
				$scope.afectado.nombres 		= null;
				$scope.afectado.nombreafectado	= null;
			} else {
				$scope.afectado = data;
				$scope.editable = false;
				if ( $scope.esrutjuridico ) {
					$scope.afectado.nombreafectado = data.apellidoPaterno + data.apellidoMaterno + data.nombres;
				} else {
					$scope.afectado.apellidoPaterno = data.apellidoPaterno;
					$scope.afectado.apellidoMaterno = data.apellidoMaterno;
					$scope.afectado.nombres 		= data.nombres;
				}
			}
			
			if ( $scope.editable ) {
				swal('Alerta','Afectado no encontrado','error');
				$('#rutAfectadoInforme').focus();
			} else {
				$scope.obtenerAfectadoTramitante();
				$('#ap_pat_pag').focus();
			}
			
		};
		PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
	};

	$scope.obtenerAfectadoTramitante = function(){
		var successCallback = function(data,responseHeaders){
			if ( data.correlativo == "0" ) {
				$scope.afectado.email = null;
				$scope.afectado.telefono = null;
				$scope.afectado.region = {};
				$scope.afectado.comuna = {};
				$scope.afectado.direccion = null;
			} else {
				$scope.afectado.email = data.email;
				$scope.afectado.telefono = data.telefono;
				$scope.afectado.region = data.region;
				$scope.afectado.comuna = data.comuna;
				$scope.afectado.direccion = data.direccion;
			}
		};
		TramitanteResource.carga({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
	};

	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
		$scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
		$scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
		$scope.afectado.nombres 		= $rootScope.tramitante.nombres;
		$scope.esvalido = true;
		$scope.buscaAfectado();
	};

	function validaDatos() {
		console.log("-> Validando datos..");
		
		if ( isUndefinedNullEmpty($scope.ingr.email) ) {
			swal('Error','Debe ingresar Email','error');
			return false;
		}
		if ( !validateEmail($scope.ingr.email) ) {
			swal('Error','Email inválido','error');
			return false;
		}
		if ( isUndefinedNullEmptyTrim($scope.ingr.telefono) ) {
			swal('Alerta','Debe ingresar Teléfono','error');
			return false;
		}
		if ( isUndefinedOrNull($scope.ingr.region) || isUndefinedNullEmpty($scope.ingr.region.codigo) ) {
			swal('Alerta','Debe ingresar Región','error');
			return false;
		}
		if ( isUndefinedOrNull($scope.ingr.comuna) || isUndefinedNullEmpty($scope.ingr.comuna.codigo) ) {
			swal('Alerta','Debe ingresar Comuna','error');
			return false;
		}
		if ( isUndefinedNullEmpty($scope.ingr.direccion) ) {
			swal('Alerta','Debe ingresar Dirección','error');
			return false;
		}
		if ( $scope.ingr.incluyedoc ) {
			if ( isUndefinedNullEmpty($scope.ingr.cantdocs) ) {
				swal('Error','Debe ingresar cantidad de documentación','error');
				return false;
			}
			if ( parseInt($scope.ingr.cantdocs) < 1 ) {
				swal('Error','Cantidad de documentación no puede ser menor a 1','error');
				return false;
			}
			if ( parseInt($scope.ingr.cantdocs) > 255 ) {
				swal('Error','Cantidad de documentación no puede ser mayor a 255','error');
				return false;
			}
		}
		if ( isUndefinedOrNull($scope.ingr.tipoSol) ) {
			swal('Alerta','Seleccione Tipo Solicitud','error');
			return false;
		}
		if ( isUndefinedOrNull($scope.ingr.categoria) ) {
			swal('Alerta','Seleccione Categoría Solicitud','error');
			return false;
		}
		if ( $scope.otros ) {
			if ( isUndefinedNullEmpty($scope.ingr.obs) ) {
				swal('Error','Debe ingresar comentario','error');
				return false;
			}
		}
		
		return true;
	}

	$scope.ingresar = function() {
		
		if ( !validaDatos() ) {
			return;
		}
		
		swal({
			title: 'Alerta',
			text: "¿Está seguro de ingresar la solicitud?",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Aceptar',
			cancelButtonText: 'Cancelar'
			
		}).then( function() {
			
			if ( !$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso && $scope.afectado.rut != $rootScope.tramitante.rut ) {
				$scope.authSupervisor();
			} else {
				$scope.guardar();
			}
			$scope.$digest();
			
		}, function (dismiss) {
			return;
		})
		
	};

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#ingresosolicitudreclamos')),
			size: 'sm'
		});
		modalInstance.result.then(function (respuesta) {
			if(respuesta){
				$scope.guardar();
			}
		});
	};

	$scope.guardar = function(){
		// if($scope.editable){
		$scope.guardarPersona();
		//	}else{
		//$scope.guardarServicio();
		//	}
	}

	$scope.guardarPersona = function(){
		if ( $scope.esrutjuridico ) {
			$scope.afectado.apellidoPaterno = $scope.afectado.nombreafectado.substring(0, 25);
			$scope.afectado.apellidoMaterno = $scope.afectado.nombreafectado.substring(25, 50);
			$scope.afectado.nombres 		= $scope.afectado.nombreafectado.substring(50, 75);
		}
		console.log("> Guarda afectado");
		console.log($scope.afectado);
		
		var successCallback = function(data,responseHeaders){
			console.log("< Guarda afectado");
			console.log(data);
			
			if ( data.correlativo == "0" ) {
				swal('Alerta','No se ha actualizado el afectado','error');
			} else {
				console.log("Correlativo afectado: " + data.correlativo);
				$scope.afectado.corrNombre = data.correlativo;
				guardarServicio();
			}
		};
		PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
	};

	function guardarServicio() {
		
		if ( isUndefinedNullEmpty($rootScope.tramitante.corrCaja) ) {
			console.log("Correlativo de caja: " + $rootScope.tramitante.corrCaja);
			console.log($rootScope.tramitante);
			swal('Error','No existe correlativo de caja','error');
			return;
		}
		
		var trx = {
			corrCaja		: parseInt($rootScope.tramitante.corrCaja),
			rutTitular		: $scope.ingr.rut,
			corrNombre		: $scope.afectado.corrNombre,
			email			: $scope.ingr.email,
			codComuna		: $scope.ingr.comuna.codigo,
			direccion		: $scope.ingr.direccion,
			tipoSolicitud	: $scope.ingr.tipoSol.codigo,
			usuario			: $rootScope.usuario.username,
			texto			: $scope.ingr.tipoSol.descripcion + ": "
		}
		if ( !isUndefinedNullEmptyTrim($scope.ingr.telefono) ) {
			trx.telefono = $scope.ingr.telefono;
		} else {
			trx.telefono = '0';
		}
		
		if ( $scope.ingr.incluyedoc ) {
			trx.cantDoc = $scope.ingr.cantdocs;
		} else {
			trx.cantDoc = 0;
		}
		
		if ( !isUndefinedOrNull($scope.ingr.categoria) ) {
			trx.codCategoria = $scope.ingr.categoria.codigo;
		}
		
		if ( $scope.otros ) {
			trx.texto = trx.texto + $scope.ingr.obs;
		} else {
			trx.texto = trx.texto + $scope.ingr.categoria.descripcion;
		}
		
		console.log("> Guarda trx");
		console.log(trx);
		
		var successCallback = function(data, responseHeaders) {
			console.log("< Guarda trx");
			console.log(data);
			
			if ( data.idControl === 0 ) {
				swal({
					title: 'Operación Exitosa',
					text: "Ingreso realizado exitosamente",
					type: 'success',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Continuar'
				}).then(function() {
					imprComprobante(data.corrSolic);
					$scope.$digest();
				})
			} else {
				swal('Error', 'Ocurrió un error al procesar su petición \n' + data.msgControl, 'error');
			}
			
		};
		SolicitudResource.ingreso(trx, successCallback, $scope.errorCallback);
	}

	$scope.historial = function(corrSolic) {
		buscarHistorial(corrSolic);
		angular.element("#estadosolicitud").modal();
	};

	function buscarHistorial(corrSolic) {
		console.log("-> buscarDetalle: " + corrSolic);
		
		var successCallback = function(data, responseHeaders) {
			console.log("-> Detalle Resp: ");
			console.log(data);
			$scope.numero = corrSolic;
			$scope.lstHist = data.lista;
		};
		SolicitudResource.historial(corrSolic, successCallback, $scope.errorCallback);
	}

	function imprComprobante(corrSolic) {
		console.log("> Impr Compr: " + corrSolic);
		
		$scope.trx = {
				numero		: corrSolic,
				tituRut		: formatRut($scope.ingr.rut),
				tituFono	: $scope.ingr.telefono,
				tituEmail	: $scope.ingr.email,
				tituDirec	: $scope.ingr.direccion,
				tituComuna	: $scope.ingr.comuna.descripcion,
				tramNombre	: $rootScope.tramitante.nombres + " " + $rootScope.tramitante.apellidoPaterno + " " + $rootScope.tramitante.apellidoMaterno,
				tramRut		: formatRut($rootScope.tramitante.rut),
				tramFono	: $rootScope.tramitante.telefono,
				tramEmail	: $rootScope.tramitante.email,
				tramDirec	: $rootScope.tramitante.direccion,
				tramComuna	: $rootScope.tramitante.comuna.descripcion.replace(/\s+$/,''),
				texto		: $scope.ingr.tipoSol.descripcion + ": ",
				codUser		: $rootScope.usuario.username
		}
		
		if ( $scope.esrutjuridico ) {
			$scope.trx.tituNombre = $scope.afectado.nombreafectado;
		} else {
			$scope.trx.tituNombre = $scope.ingr.nombres + " " + $scope.ingr.apellidop + " " + $scope.ingr.apellidom;
		}
		
		if ( $scope.ingr.incluyedoc ) {
			$scope.trx.cantDoc = $scope.ingr.cantdocs;
		} else {
			$scope.trx.cantDoc = 0;
		}
		
		if ( $scope.otros ) {
			$scope.trx.texto = $scope.trx.texto + $scope.ingr.obs;
		} else {
			$scope.trx.texto = $scope.trx.texto + $scope.ingr.categoria.descripcion;
		}
		
		console.log("-> Data Trx: ");
		console.log($scope.trx);
		
		var successCallback = function (data, responseHeaders) {
			console.log("> PDF Resp:");
			console.log(data);
			
			// Base64 String
			var base64str = data.reporte;
			
			// Decode Base64 String, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			
			for ( var i = 0; i < len; i++ ) {
				view[i] = binary.charCodeAt(i);
			}
			
			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "SR_"+corrSolic+".pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
		SolicitudResource.compr($scope.trx, successCallback, $scope.errorCallback);
	}
	
	$scope.imprimir = function() {
		console.log("> Impr Form: " + $scope.impr.opc);
		
		$scope.ingr.region = $filter('filter')($scope.ingr.regiones, {codigo: $scope.afectado.region.codigo})[0];
		
		if ( !isUndefinedOrNull($scope.ingr.region) && !isUndefinedNullEmpty($scope.ingr.region.codigo) ) {
			cargaListaComunas();
		} else {
			$scope.ingr.comuna = {};
			$scope.ingr.comuna.descripcion = '';
			generaForm();
		}
	};
	
	function cargaListaComunas() {
		
		var successCallback = function(data,responseHeaders){
			$scope.ingr.comunas = data;
			if ( !isUndefinedOrNull($scope.afectado.comuna) && !isUndefinedNullEmpty($scope.afectado.comuna.codigo) ) {
				console.log("filter");
				$scope.ingr.comuna = $filter('filter')($scope.ingr.comunas, {codigo: $scope.afectado.comuna.codigo})[0];
			}
			generaForm();
		}
		TramitanteResource.cargaComunas({idRegion: $scope.ingr.region.codigo}, successCallback, $scope.errorCallback);
	}
	
	function generaForm() {
		
		trx = {
			numero		: $scope.impr.opc,
			tramNombre	: $rootScope.tramitante.nombres + " " + $rootScope.tramitante.apellidoPaterno + " " + $rootScope.tramitante.apellidoMaterno,
			tramRut		: formatRut($rootScope.tramitante.rut),
			tramFono	: $rootScope.tramitante.telefono,
			tramEmail	: $rootScope.tramitante.email,
			tramDirec	: $rootScope.tramitante.direccion,
			tramComuna	: $rootScope.tramitante.comuna.descripcion.replace(/\s+$/,'')
		}
		
		console.log("-> Data Trx: ");
		console.log(trx);
		
		var successCallback = function (data, responseHeaders) {
			console.log("> PDF Resp:");
			console.log(data);
			
			// Base64 String
			var base64str = data.reporte;
			
			// Decode Base64 String, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			
			for ( var i = 0; i < len; i++ ) {
				view[i] = binary.charCodeAt(i);
			}
			
			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "form_solicrecl.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
		SolicitudResource.form(trx, successCallback, $scope.errorCallback);
	}

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

	function isUndefinedNullEmptyTrim(val) {
		if ( isUndefinedNullEmpty(val) || val.replace(/\s+$/,'') == "" ) {
			return true;
		}
		return false;
	}

	function esRutValido(rut) {
		var rutNumerico	= (rut == undefined) ? 0 : parseInt(rut.slice(0,rut.length-1).trim());
		if ( RutHelper.validate(rut) && rutNumerico > rutMinimo ) {
			return true;
		} else {
			return false;
		}
	}

	function esRutJuridico(rut) {
		var rutNumerico = (rut == undefined) ? 0 : parseInt(rut.slice(0, rut.length-1).trim());
		if ( rutNumerico >= rutJuridico ) {
			return true;
		}
		return  false;
	}

	$scope.cleanDocs = function() {
		if ( !$scope.ingr.incluyedoc ) {
			$scope.ingr.cantdocs = null;
		}
	};

	function formatRut(_value) {
		_value = cleanRut(_value);
		if ( !_value ) {
			return null;
		}
		if ( _value.length <= 1 ) {
			return _value;
		}
		var result = _value.slice(-4,-1) + '-' + _value.substr(_value.length-1);
		for ( var i=4; i < _value.length; i+=3 ) {
			result = _value.slice(-3-i,-i) + '.' + result;
		}
		return result;
	}

	function cleanRut(_value) {
		return typeof _value === 'string' ? _value.replace(/[^0-9kK]+/g,'').replace(/^0+/, '').toUpperCase() : '';
	}

	function validateEmail(email) {
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		return reg.test(email);
	}

	$scope.noCeros = function(valor) {
		if ( parseInt(valor, 11) === 0) {
			swal({ title:'Error', text:"Número Inválido", type:'error', showCloseButton:true, confirmButtonText:'Cerrar'});
			$scope.ingr.telefono = "";
		}
	}

});
