package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_RetornoAclBatch database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_RetornoAclBatch.findAll", query="SELECT t FROM Tbl_RetornoAclBatch t")
public class Tbl_RetornoAclBatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodRetAcl")
	private short fld_CodRetAcl;

	@Column(name="Fld_GlosaRetAcl")
	private String fld_GlosaRetAcl;

	public Tbl_RetornoAclBatch() {
	}

	public short getFld_CodRetAcl() {
		return this.fld_CodRetAcl;
	}

	public void setFld_CodRetAcl(short fld_CodRetAcl) {
		this.fld_CodRetAcl = fld_CodRetAcl;
	}

	public String getFld_GlosaRetAcl() {
		return this.fld_GlosaRetAcl;
	}

	public void setFld_GlosaRetAcl(String fld_GlosaRetAcl) {
		this.fld_GlosaRetAcl = fld_GlosaRetAcl;
	}

}