package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_RutNoBeneficiados database table.
 * 
 */
@Entity
@Table(name="Tbl_RutNoBeneficiados")
@NamedQuery(name="Tbl_RutNoBeneficiado.findAll", query="SELECT t FROM Tbl_RutNoBeneficiado t")
public class Tbl_RutNoBeneficiado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecModificacion")
	private Timestamp fld_FecModificacion;

	@Column(name="Fld_Flag1")
	private boolean fld_Flag1;

	@Column(name="Fld_RutAfectado")
	private String fld_RutAfectado;

	public Tbl_RutNoBeneficiado() {
	}

	public Timestamp getFld_FecModificacion() {
		return this.fld_FecModificacion;
	}

	public void setFld_FecModificacion(Timestamp fld_FecModificacion) {
		this.fld_FecModificacion = fld_FecModificacion;
	}

	public boolean getFld_Flag1() {
		return this.fld_Flag1;
	}

	public void setFld_Flag1(boolean fld_Flag1) {
		this.fld_Flag1 = fld_Flag1;
	}

	public String getFld_RutAfectado() {
		return this.fld_RutAfectado;
	}

	public void setFld_RutAfectado(String fld_RutAfectado) {
		this.fld_RutAfectado = fld_RutAfectado;
	}

}