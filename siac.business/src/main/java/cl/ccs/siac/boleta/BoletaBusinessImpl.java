package cl.ccs.siac.boleta;

import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.BoletaDao;
import cl.exe.ccs.dto.BoletaDTO;
import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ControlBoletaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

@Named("boletaBusiness")
public class BoletaBusinessImpl implements BoletaBusiness{

	@Inject
	private BoletaDao boletaDao;
	
    public BoletaDTO obtenerBoleta(BoletaDTO boletaDTO){
        return boletaDao.obtenerBoleta(boletaDTO);
    }
    
    public BoletaDTO obtenerBoletaTransaccion(BoletaDTO boletaDTO){
    	return boletaDao.obtenerBoletaTransaccion(boletaDTO);
    }

    public ResponseListDTO<Integer> obtenerBoletasFolioCero(BoletaDTO boletaDTO){
        return boletaDao.obtenerBoletasFolioCero(boletaDTO);
    }

	public ResponseDTO consultaBoleta(ControlBoletaDTO boletaDTO){
		
		ResponseDTO respuesta = new ResponseDTO();
		respuesta = boletaDao.consultaBoleta(boletaDTO);
		return respuesta;
		
	}

	public ControlBoletaDTO inicializaBoleta(ControlBoletaDTO boletaDTO){
		return boletaDao.inicializaBoleta(boletaDTO);
	}
	
	public ResponseDTO actualizaRangoBoleta(BoletaTransaccionDTO boletaDTO){
		return boletaDao.actualizaRangoBoleta(boletaDTO);
	}
	
	public ResponseDTO anularBoleta(ControlBoletaDTO boletaDTO){
		return boletaDao.anularBoleta(boletaDTO);
	}
}
