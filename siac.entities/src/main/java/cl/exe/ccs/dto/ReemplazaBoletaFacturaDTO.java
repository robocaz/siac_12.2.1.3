package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReemplazaBoletaFacturaDTO extends ResponseDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5010242991330156156L;
	
	private Long codCCosto;
	private Long corrCaja;
	private Long corrTransaccion;
	private Integer numeroBoleta;
	private Integer numeroFactura;
	private Long fecEmision;
	private String rutAfectado;
	private Long corrNombre;
	private BigDecimal montoBruto;
	private String codUsuario;
	private String codOrigen;
	private String ordenCompra;
	
	public Long getCodCCosto() {
		return codCCosto;
	}
	public void setCodCCosto(Long codCCosto) {
		this.codCCosto = codCCosto;
	}
	public Long getCorrCaja() {
		return corrCaja;
	}
	public void setCorrCaja(Long corrCaja) {
		this.corrCaja = corrCaja;
	}
	public Long getCorrTransaccion() {
		return corrTransaccion;
	}
	public void setCorrTransaccion(Long corrTransaccion) {
		this.corrTransaccion = corrTransaccion;
	}
	public Integer getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(Integer numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public Integer getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(Integer numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public Long getFecEmision() {
		return fecEmision;
	}
	public void setFecEmision(Long fecEmision) {
		this.fecEmision = fecEmision;
	}
	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public Long getCorrNombre() {
		return corrNombre;
	}
	public void setCorrNombre(Long corrNombre) {
		this.corrNombre = corrNombre;
	}
	public BigDecimal getMontoBruto() {
		return montoBruto;
	}
	public void setMontoBruto(BigDecimal montoBruto) {
		this.montoBruto = montoBruto;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodOrigen() {
		return codOrigen;
	}
	public void setCodOrigen(String codOrigen) {
		this.codOrigen = codOrigen;
	}
	public String getOrdenCompra() { return ordenCompra; }
	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	
}
