package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Tbl_LogConsultas database table.
 * 
 */
@Embeddable
public class Tbl_LogConsultaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Fld_FecConsulta")
	private java.util.Date fld_FecConsulta;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_Transaccion")
	private short fld_Transaccion;

	@Column(name="Fld_CorrConsulta_Id")
	private long fld_CorrConsulta_Id;

	public Tbl_LogConsultaPK() {
	}
	public java.util.Date getFld_FecConsulta() {
		return this.fld_FecConsulta;
	}
	public void setFld_FecConsulta(java.util.Date fld_FecConsulta) {
		this.fld_FecConsulta = fld_FecConsulta;
	}
	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}
	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}
	public short getFld_Transaccion() {
		return this.fld_Transaccion;
	}
	public void setFld_Transaccion(short fld_Transaccion) {
		this.fld_Transaccion = fld_Transaccion;
	}
	public long getFld_CorrConsulta_Id() {
		return this.fld_CorrConsulta_Id;
	}
	public void setFld_CorrConsulta_Id(long fld_CorrConsulta_Id) {
		this.fld_CorrConsulta_Id = fld_CorrConsulta_Id;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Tbl_LogConsultaPK)) {
			return false;
		}
		Tbl_LogConsultaPK castOther = (Tbl_LogConsultaPK)other;
		return 
			this.fld_FecConsulta.equals(castOther.fld_FecConsulta)
			&& this.fld_CodUsuario.equals(castOther.fld_CodUsuario)
			&& (this.fld_Transaccion == castOther.fld_Transaccion)
			&& (this.fld_CorrConsulta_Id == castOther.fld_CorrConsulta_Id);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fld_FecConsulta.hashCode();
		hash = hash * prime + this.fld_CodUsuario.hashCode();
		hash = hash * prime + ((int) this.fld_Transaccion);
		hash = hash * prime + ((int) (this.fld_CorrConsulta_Id ^ (this.fld_CorrConsulta_Id >>> 32)));
		
		return hash;
	}
}