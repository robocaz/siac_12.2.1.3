angular.module('ccs').controller('IngresoDepositosController', function($scope, PersonaResource, DepositosResource, $location, $filter, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.tab1 = true;
	$scope.campo = {};
	$scope.campo.banco = {};
	$scope.campo.ctaCte = {};
	$scope.lstDepos = {};

	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;

	$('#ingresodepositos').on('show.bs.modal', function (e) {
		$timeout(function(){
			$scope.init();
		});
	});

	$scope.init = function() {
		$scope.tab1 = true;
		$scope.campo.centroCosto = obtenerCentroCosto();
		$scope.campo.nombreTitular = "CAMARA DE COMERCIO DE SANTIAGO";
		loadCmbsBancosCtas();
		$scope.campo.tipoDepo = 'EFC';
	}
	
	function obtenerCentroCosto() {
		if ( isUndefinedOrNull($rootScope.usuario.centroCostoActivo) ) {
			return null;
		}
		var cod = $rootScope.usuario.centroCostoActivo.codigo;
		var desc = $rootScope.usuario.centroCostoActivo.descripcion;
		return cod + " - " + desc.trim();
	}
	
	function loadCmbsBancosCtas() {
		var successCallback = function (data, responseHeaders) {
			$scope.campo.cmbBancos = data;
			$scope.campo.banco = data[0];
			loadCmbCtasCtes();
		}
		DepositosResource.cmbbancos(successCallback, $scope.errorCallback);
	}
	
	function loadCmbCtasCtes() {
		var successCallback = function (data, responseHeaders) {
			$scope.campo.cmbCtasCtes = data;
			$scope.campo.ctaCte = data[0];
		}
		DepositosResource.cmbctasctes({codEmisor: $scope.campo.banco.codigo}, successCallback, $scope.errorCallback);
	}
	
	$scope.esFecha = function(fechaStr, campoFecha) {
		if ( isUndefinedNullEmpty(fechaStr) ) {
			return;
		}
		if ( !isValidDate(fechaStr) ) {
			errorFechaInvalida(campoFecha);
		}
	}

	function isUndefinedOrNull(val) {
		return angular.isUndefined(val) || val === null;
	}

	function isUndefinedNullEmpty(val) {
		if ( isUndefinedOrNull(val) || val == "" ) {
			return true;
		}
		return false;
	}

	function isValidDate(fechaStr) {
		if ( isUndefinedNullEmpty(fechaStr) ) {
			return false;
		}
		if ( fechaStr.length < 8 ) {
			return false;
		}
		fechaStr = fechaStr.replace(/-/g, "");
		var fechaArray = fechaStr.split("");
		var dia = fechaArray[0] + fechaArray[1];
		var mes = fechaArray[2] + fechaArray[3];
		var anio = fechaArray[4] + fechaArray[5] + fechaArray[6] + fechaArray[7];
		var dateStr = mes + "/" + dia + "/" + anio;
		var d = new Date(dateStr);
		return (d && d.getDate() == Number(dia) && (d.getMonth() + 1) == mes && d.getFullYear() == Number(anio));
	}

	function errorFechaInvalida(campoFecha) {
		switch(campoFecha) {
		case 1:
			$scope.campo.fechaDepo = null;
			swal('Alerta','Fecha Deposito Inválida','error');
			$("#fecha_deposito").focus();
			break;
		case 2:
			$scope.campo.fechaDesde = null;
			swal('Alerta','Fecha Desde Inválida','error');
			$("#rango_desde").focus();
			break;
		case 3:
			$scope.campo.fechaHasta = null;
			swal('Alerta','Fecha Hasta Inválida','error');
			$("#rango_hasta").focus();
			break;
		}
	}

	$scope.buscaDepositante = function() {
		if ( !$scope.validaRut($scope.campo.rutDepo) ) {
			$scope.campo.rutDepo = undefined;
			$scope.campo.nombreDepo = undefined;
			return;
		}
		buscaTramitante($scope.campo.rutDepo);
	};

	function buscaTramitante(rut) {
		let trx = {
			rut	: rut
		}
		var successCallback = function(data, responseHeaders) {
			if ( data.correlativo === 0 ) {
				swal('Alerta','El Rut ingresado no posee Nombre Asociado, por favor comunicarse con Operaciones para que ingrese el Nombre de este Rut','error');
				$scope.campo.nombreDepo = null;
			} else {
				if ( !esRutJuridico(rut) ) {
					$scope.campo.nombreDepo = data.nombres + " " + data.apellidoPaterno + " " + data.apellidoMaterno;
				} else {
					$scope.campo.nombreDepo = data.apellidoPaterno + data.apellidoMaterno + data.nombres;
				}
			}
		};
		PersonaResource.obtenerPersona(trx, successCallback, $scope.errorCallback);
	}

	function esRutJuridico(rut) {
		var rutNumerico = (rut == undefined) ? 0 : parseInt(rut.slice(0, rut.length-1).trim());
		if ( rutNumerico >= rutJuridico ) {
			return true;
		}
		return  false;
	}

	$scope.validaRut = function(rut) {
		if ( rut === undefined ) {
			return false;
		}
		if ( rut === null ) {
			swal('Error','Rut no es válido','error');
			return false;
		}
		if ( !esRutValido(rut) ) {
			swal('Error','Rut no es válido','error');
			return false;
		}
		return true;
	};

	function esRutValido(rut) {
		var rutNumerico = (rut == undefined) ? 0 : parseInt(rut.slice(0,rut.length-1).trim());
		if ( RutHelper.validate(rut) && rutNumerico > rutMinimo ) {
			return true;
		} else {
			return false;
		}
	}

	$scope.buscar = function() {
		if ( !validaFechas() ) {
			console.log("-> Fechas inválidas");
			return;
		}
		
		$scope.params = {
			codigoCC	: $rootScope.usuario.centroCostoActivo.codigo,
			fechaDesde	: formatFechaStr($scope.campo.fechaDesde),
			fechaHasta	: formatFechaStr($scope.campo.fechaHasta)
		}
		var successCallback = function (data, responseHeaders) {
			$scope.lstDepos = data;
			if ( data == null || data.length == 0 ){
				swal('Alerta','No existen registros para el rango de fechas seleccionado','error');
			}
		}
		DepositosResource.consulta($scope.params, successCallback, $scope.errorCallback);
	};

	function validaFechas() {
		
		if ( !validaFecha($scope.campo.fechaDesde, 2) ) {
			return false;
		}
		var fechaDesde = getDateStrFecha($scope.campo.fechaDesde);
		var hoy = new Date();
		if ( fechaDesde > hoy ) {
			swal('Alerta','Fecha Desde no puede ser mayor a la fecha actual','error');
			return false;
		}
		
		if ( !validaFecha($scope.campo.fechaHasta, 3) ) {
			return false;
		}
		var fechaHasta = getDateStrFecha($scope.campo.fechaHasta);
		if ( fechaHasta > hoy ) {
			swal('Alerta','Fecha Hasta no puede ser mayor a la fecha actual','error');
			return false;
		}
		
		if ( fechaHasta < fechaDesde ) {
			swal('Alerta','Fecha Hasta no puede ser menor a Fecha Desde','error');
			return false;
		}
		
		return true;
	}

	function getOptionFromCode(cod) {
		for (var i=0; i < $scope.campo.ctaCte.length; i++) {
			var opt = $scope.campo.ctaCte[i];
			if ( opt.codigo == cod ) {
				return opt;
			}
		}
		return null;
	}

	$scope.guardarDatos = function() {
		if ( $scope.validaDatos() ) {
			$scope.guardar();
		}
	};

	$scope.validaDatos = function() {
		console.log("-> Validando datos..");
		
		if ( $scope.campo.banco === null || $scope.campo.banco === undefined ) {
			swal('Alerta','Debe seleccionar un Banco','error');
			$("#select_banco").focus();
			return false;
		}
		if ( $scope.campo.ctaCte === null || $scope.campo.ctaCte === undefined ) {
			swal('Alerta','Debe seleccionar un N° Cuenta','error');
			$("#num_cuenta").focus();
			return false;
		}
		console.log($scope.campo.tipoDepo);
		if ( isUndefinedNullEmpty($scope.campo.tipoDepo) ) {
			swal('Alerta','Debe seleccionar el tipo de Depósito','error');
			return false;
		}
		if ( isUndefinedNullEmpty($scope.campo.numeroDepo) ) {
			swal('Alerta','Debe ingresar N° Depósito','error');
			return false;
		}
		if ( !validaFecha($scope.campo.fechaDepo, 1) ) {
			return false;
		}
		var fechaDeposito = getDateStrFecha($scope.campo.fechaDepo);
		var hoy = new Date();
		if ( fechaDeposito > hoy ) {
			swal('Alerta','Fecha Deposito no puede ser mayor a la fecha actual','error');
			return false;
		}
		
		if ( isUndefinedNullEmpty($scope.campo.rutDepo) ) {
			$scope.campo.rutDepo = undefined;
			swal('Alerta','Debe ingresar Rut','error');
			return false;
		}
		if ( isUndefinedNullEmpty($scope.campo.nombreDepo) ) {
			swal('Alerta','Debe ingresar un depositante','error');
			return false;
		}
		if ( isUndefinedNullEmpty($scope.campo.totalDepo) ) {
			swal('Alerta','Debe ingresar Total Depósito','error');
			return false;
		}
		if ( parseInt($scope.campo.totalDepo) == 0 ) {
			$scope.campo.totalDepo = "0";
			swal('Alerta','Ingrese un monto total mayor a 0','error');
			return false;
		}
		
		return true;
	};

	function validaFecha(fecha, campoFecha) {
		if ( isUndefinedNullEmpty(fecha) ) {
			errorFechaNula(campoFecha);
			return false;
		}
		if ( !isValidDate(fecha) ) {
			errorFechaInvalida(campoFecha);
			return false;
		}
		return true;
	}

	function errorFechaNula(campoFecha) {
		switch(campoFecha) {
		case 1:
			swal('Alerta','Ingrese Fecha Deposito','error');
			$("#fecha_deposito").focus();
			break;
		case 2:
			swal('Alerta','Ingrese Fecha Desde','error');
			$("#rango_desde").focus();
			break;
		case 3:
			swal('Alerta','Ingrese Fecha Hasta','error');
			$("#rango_hasta").focus();
			break;
		}
	}

	function getDateStrFecha(strFecha) {
		let dia = strFecha.substr(0, 2);
		let mes = strFecha.substr(2, 2);
		let anio = strFecha.substr(4, 4);
		let fecha = new Date(anio +'-'+ mes +'-'+ dia);
		return fecha;
	}

	$scope.guardar = function(){
		$scope.guardarServicio();
	}

	$scope.guardarServicio = function() {
		$scope.trx = {
			codigoCC			: $rootScope.usuario.centroCostoActivo.codigo,
			fecRegDeposito		: formatFechaStr($scope.campo.fechaDepo),
			codUser				: $rootScope.usuario.username,
			rutPersona			: $scope.campo.rutDepo,
			codEmisor			: $scope.campo.banco.codigo,
			nroDepositoAux		: $scope.campo.numeroDepo,
			nroCtaCteAux		: $scope.campo.ctaCte.codigo,
			tipoPago			: $scope.campo.tipoDepo,
			montoDepositoAux	: $scope.campo.totalDepo
		}
		
		console.log("-> Data Trx:");
		console.log($scope.trx);
		var successCallback = function(data,responseHeaders){
			console.log("-> Data Resp:");
			console.log(data);
			if ( data.idControl === 0 ) {
				swal({
					title: 'Operación Exitosa',
					text: data.msgControl,
					type: 'success',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Continuar'
				}).then(function() {
					
				})
			} else if ( data.idControl === 1 ) {
				swal('Error', data.msgControl, 'error');
			} else {
				swal('Error', 'Ocurrió un error al procesar su petición', 'error');
			}
		};
		DepositosResource.guarda($scope.trx, successCallback, $scope.errorCallback);
	}

	function formatFechaStr(fecha) {
		fecha = fecha.replace(/-/g, "");
		if ( isUndefinedOrNull(fecha) || !isValidDate(fecha) ) {
			return "";
		}
		let dia = fecha.substr(0, 2);
		let mes = fecha.substr(2, 2);
		let anio = fecha.substr(4, 4);
		return anio +'-'+ mes +'-'+ dia;
	}

	$scope.cerrarDepo = function() {
		if ( isUndefinedNullEmpty($scope.campo.depoSelec) ) {
			swal('Alerta','Debe seleccionar registro','error');
			return;
		}
		
		let row = $filter('filter')($scope.lstDepos, {corrCuentaId: $scope.campo.depoSelec})[0];
		
		if ( row.flagCierreFinal ) {
			swal('Alerta','Deposito ya cerrado','error');
			return;
		}
		
		swal({
			title: 'Alerta',
			text: "¿Está seguro que desea realizar el cierre del depósito?",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
			
		}).then( function() {
			cerrarDeposito();
		}, function (dismiss) {
			
		})
	};

	function cerrarDeposito() {
		var successCallback = function(data,responseHeaders){
			console.log("-> Data Resp:");
			console.log(data);
			if ( data.idControl === 0 ) {
				updateListaDepositos($scope.campo.depoSelec);
				swal({
					title: 'Operación Exitosa',
					text: data.msgControl,
					type: 'success',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Continuar'
				}).then(function() {
					
				})
			} else {
				swal('Error', 'Ocurrió un error al procesar su petición', 'error');
			}
			
		};
		DepositosResource.cerrar($scope.campo.depoSelec, successCallback, $scope.errorCallback);
	}

	function updateListaDepositos(corrCuentaId) {
		for ( var i=0; i < $scope.lstDepos.length; i++ ) {
			var row = $scope.lstDepos[i];
			if ( row.corrCuentaId == corrCuentaId ) {
				row.flagCierreFinal = true;
				row.fecCierreFinal = formatoFechaStr(new Date());
				break;
			}
		}
	}

	function formatoFechaStr(fecha) {
		let mes = String(fecha.getMonth() + 1);
		let dia = String(fecha.getDate());
		const anio = String(fecha.getFullYear());
		
		if ( mes.length < 2 ) { mes = '0' + mes; }
		if ( dia.length < 2 ) { dia = '0' + dia; }
		
		return dia +"-"+ mes +"-"+ anio;
	}

	$scope.limpiar = function() {
		$scope.campo.fechaDesde = null;
		$scope.campo.fechaHasta = null;
		$scope.lstDepos = {};
	}

	$scope.cerrar = function() {
		$scope.campo = {};
		$scope.lstDepos = {};
		$scope.esvalido = false;
		$scope.editable = false;
		$scope.tab1 = true;
		angular.element("#ingresodepositos").modal('hide');
	}

	$scope.imprimir = function () {
		
		$scope.trx = {
			fechaDesde	: formatoFecha($scope.campo.fechaDesde),
			fechaHasta	: formatoFecha($scope.campo.fechaHasta),
			usuario		: $rootScope.usuario.username,
			centroCosto	: $scope.campo.centroCosto,
			totalDepos	: $scope.lstDepos.length,
			depositos	: $scope.lstDepos
		}
		
		console.log("> Data Trx:");
		console.log($scope.trx);
		var successCallback = function (data, responseHeaders) {
			console.log("> PDF response:");
			console.log(data);
			
			// Base64 String
			var base64str = data.reporte;
			
			// Decode Base64 String, remove space for IE compatibility
			var binary = atob(base64str.replace(/\s/g, ''));
			var len = binary.length;
			var buffer = new ArrayBuffer(len);
			var view = new Uint8Array(buffer);
			
			for ( var i = 0; i < len; i++ ) {
				view[i] = binary.charCodeAt(i);
			}
			
			var blob = new Blob( [view], { type: "application/pdf" });
			saveAs(blob, "listado_depositos.pdf");
			//var url = URL.createObjectURL(blob);
			//window.location = url;
		}
		DepositosResource.pdf($scope.trx, successCallback, $scope.errorCallback);
	}

	function formatoFecha(strFecha) {
		strFecha = strFecha.replace(/-/g, "");
		let dia = strFecha.substr(0, 2);
		let mes = strFecha.substr(2, 2);
		let anio = strFecha.substr(4, 4);
		return  dia +'/'+ mes +'/'+ anio;
	}

});
