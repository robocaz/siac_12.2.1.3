package cl.exe.ccs.dto;

import java.io.Serializable;

public class SolicitudReclamoIngresoOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 6911127445481336099L;

	private Integer corrSolic;

	public Integer getCorrSolic() {
		return corrSolic;
	}

	public void setCorrSolic(Integer corrSolic) {
		this.corrSolic = corrSolic;
	}

}
