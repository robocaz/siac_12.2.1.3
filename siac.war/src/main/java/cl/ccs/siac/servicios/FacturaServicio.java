package cl.ccs.siac.servicios;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.security.Principal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.ws.WebServiceFeature;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.ccs.siac.factura.FacturaBusiness;
import cl.ccs.siac.utils.FacturaUtil;
import cl.ccs.siac.utils.Utils;
import cl.ccs.siac.utils.WatchTimer;
import cl.exe.ccs.dto.ConsultaRangoBolFacDTO;
import cl.exe.ccs.dto.FacturaDTO;
import cl.exe.ccs.dto.FacturaTransaccionDTO;
import cl.exe.ccs.dto.ReemplazaBoletaFacturaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.resources.Constants;
import webfenixpublico.webservices.EmitirDTE.DTE;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Detalle;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Detalle.CdgItem;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Encabezado;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Encabezado.Emisor;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Encabezado.IdDoc;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Encabezado.Receptor;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Encabezado.Totales;
import webfenixpublico.webservices.EmitirDTE.DTE.Documento.Referencia;
import webfenixpublico.webservices.ExportacionInf;
import webfenixpublico.webservices.MedioPagoType;
import webfenixpublico.webservices.ResultadoInf;
import webfenixpublico.webservices.WSEmision;

@Path("/factura")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class FacturaServicio {

	private static final Logger log = LogManager.getLogger(FacturaServicio.class);

	@Inject
	private FacturaBusiness facturaBusiness;

	@POST
	@Path("/consultar")
	public Response consultar(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: consultar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			// ResponseListDTO<FacturaDTO> responseList =
			// facturaBusiness.consultaFacturas(factura);
			// return Response.ok(responseList).build();
			return Response.ok(facturaBusiness.consultaFacturas(factura)).build();
		} catch (Exception ex) {
			log.error("Metodo consultar - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/grabar")
	public Response grabar(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: grabar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			// factura.setRutFacturacion(FacturaUtil.getRutWS(factura.getRutFacturacion()));
			FacturaDTO response = facturaBusiness.grabaFactura(factura);
			return Response.ok(response).build();
		} catch (Exception ex) {
			log.error("Metodo grabar - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/eliminar")
	public Response eliminar(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: eliminar - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			FacturaDTO response = facturaBusiness.eliminaFactura(factura);
			return Response.ok(response).build();
		} catch (Exception ex) {
			log.error("Metodo eliminar - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/actualizarTransaccion")
	public Response actualizarTransaccion(@Context HttpServletRequest request, FacturaDTO factura) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: actualizarTransaccion - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(facturaBusiness.actualizarTransaccion(factura)).build();
		} catch (Exception ex) {
			log.error("Metodo actualizarTransaccion - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/consultaRangoBF")
	public Response consultaRangoBF(@Context HttpServletRequest request, ConsultaRangoBolFacDTO consultaRango) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: consultaRangoBF - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(facturaBusiness.consultaRangoBF(consultaRango)).build();
		} catch (Exception ex) {
			log.error("Metodo consultaRangoBF - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/folio")
	public Response folio(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: folio - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(facturaBusiness.folioFactura(factura)).build();

			/*
			 * factura.setNumeroFactura(Long.parseLong("2567"));
			 * factura.setIdControl(0); factura.setMsgControl(""); return
			 * Response.ok(factura).build();
			 */
		} catch (Exception ex) {
			log.info("Metodo folio - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/emitir")
	public Response emitir(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: emitir - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {

			log.info("Factura.emitir");

			ResponseDTO response = new ResponseDTO();
			
			String archivo2 = FacturaUtil.getFileName(String.valueOf(factura.getNumeroFactura()), "00", false);
			

			File pdfFactura2 = new File(Constants.DIR_INVOICES_PDF + '/'  + archivo2 + ".pdf");
			if (pdfFactura2.exists()) {
			    response.setIdControl(0);
			    response.setMsgControl(FacturaUtil.getFileName(String.valueOf(factura.getNumeroFactura()), "00", false));
			    return Response.ok(response).build();
			}
			/*
			 * ResponseDTO response = new ResponseDTO();
			 * response.setIdControl(0);
			 * response.setMsgControl("BC700178200332567020175709"); return
			 * Response.ok(response).build();
			 */

			String retornar = "00,11";

			DTE dte = new DTE();
			Documento doc = new Documento();
			Encabezado encabezado = new Encabezado();

			IdDoc idDoc = new IdDoc();
			idDoc.setTipoDTE(Constants.INVOICE_DOCUMENT_TYPE_INT);
			if(factura.getNumeroFactura() != null) {
				idDoc.setFolio(factura.getNumeroFactura().intValue());
			}
			else {
				response.setIdControl(1);
				response.setMsgControl("Error: No se pudo obtener folio");
			}
			
			idDoc.setMedioPago(MedioPagoType.fromValue(FacturaUtil.getMedioPago(factura.getMedioPago())));
			//idDoc.setFchEmis(FacturaUtil.getFecha());
			idDoc.setFchEmis(FacturaUtil.getFechaSinHora());
			idDoc.setFmaPago("1");
			encabezado.setIdDoc(idDoc);

			Emisor emisor = new Emisor();
			emisor.setRznSoc(Constants.INVOICE_COST_CENTER_NAME);
			emisor.setRUTEmisor(Constants.INVOICE_COST_CENTER_RUT);
			emisor.setGiroEmis(Constants.INVOICE_COST_CENTER_ACTIVITY);
			emisor.getActeco().add(Integer.parseInt(Constants.INVOICE_COST_CENTER_ACTECO));
			emisor.setDirOrigen(Constants.INVOICE_COST_CENTER_STREET_ADDRESS);
			emisor.setCiudadOrigen(Constants.INVOICE_COST_CENTER_CITY);
			emisor.setCmnaOrigen(Constants.INVOICE_COST_CENTER_COMMUNE);
			encabezado.setEmisor(emisor);

			log.info("RUTRecep (raw): " + factura.getRutFacturacion());
			log.info("RUTRecep: " + FacturaUtil.getRutWS(factura.getRutFacturacion()));

			Receptor receptor = new Receptor();
			receptor.setRUTRecep(FacturaUtil.getRutWS(factura.getRutFacturacion()));
			if(FacturaUtil.getTipoPersonaRut(factura.getRutFacturacion()).equals("J")){
				receptor.setRznSocRecep(factura.getRazonSocial().trim());
			}else{
				receptor.setRznSocRecep(factura.getNombres() + " " + factura.getApellidoPaterno() + " " +  factura.getApellidoMaterno());
			}
			receptor.setGiroRecep(factura.getGiro().trim());
			receptor.setDirRecep(factura.getDireccion().trim());
			receptor.setCmnaRecep(factura.getComunaDesc().trim());
			receptor.setContacto(FacturaUtil.getTramitante(factura.getTramitante()));
			receptor.setCorreoRecep(factura.getTramitante().getEmail().trim());
			receptor.setCiudadRecep(factura.getComunaDesc().trim());
			
			encabezado.setReceptor(receptor);

			// TODO OC parámetros configurables
			if (factura.getOrdenCompra().compareTo("") != 0) {
				Referencia referencia = new Referencia();
				referencia.setNroLinRef(1);
				referencia.setFolioRef(factura.getOrdenCompra());
				referencia.setTpoDocRef("801");
				referencia.setFchRef(FacturaUtil.getFecha());
				doc.getReferencia().add(referencia);
			}

			log.info("RUTSolicita (raw): " + factura.getTramitante().getRut());
			log.info("RUTSolicita: " + FacturaUtil.getRutWS(factura.getTramitante().getRut()));
			encabezado.setRUTSolicita(FacturaUtil.getRutWS(factura.getTramitante().getRut()));

			Integer montoNeto = 0;
			Integer indexTransaccion = 1;
			ResponseDTO responseIVA = facturaBusiness.obtenerIVA();
			Float iva = Float.parseFloat(responseIVA.getMsgControl());
			
			for (FacturaTransaccionDTO transaccion : factura.getTransacciones()) {
				Detalle detalle = new Detalle();

				if (transaccion.getCostoServicio().compareTo(BigDecimal.ZERO) == 0) {
					/*
					 * <Detalle> <NroLinDet>2</NroLinDet> <IndExe>2</IndExe>
					 * <NmbItem>No Facturable</NmbItem> <DscItem>No
					 * Facturable</DscItem> <MontoItem>0</MontoItem> </Detalle>
					 */
					detalle.setNroLinDet(indexTransaccion);
					detalle.setQtyItem(BigDecimal.valueOf(Long.parseLong(transaccion.getCantidadServicio().toString())));
					detalle.setUnmdItem("UND");
					detalle.setIndExe("2");
					detalle.setNmbItem(transaccion.getTipoServicio());
					// detalle.setDscItem(transaccion.getTipoServicio());
					detalle.setMontoItem(transaccion.getCostoServicio().intValue() * transaccion.getCantidadServicio());

				} else {

					detalle.setNroLinDet(indexTransaccion);
					detalle.setQtyItem(BigDecimal.valueOf(Long.parseLong(transaccion.getCantidadServicio().toString())));

					CdgItem item = new CdgItem();
					item.setTpoCodigo("INT1");
					item.setVlrCodigo(transaccion.getCodExterno());
					detalle.getCdgItem().add(item);

					detalle.setNmbItem(transaccion.getTipoServicio());
					
					detalle.setPrcItem(new BigDecimal(Math.round(transaccion.getCostoServicio().intValue() / (1 + (iva/100)))));
					Float montoNetoItem = (transaccion.getCostoServicio().intValue() / (1 + (iva/100)));
					//Float montoIva = transaccion.getCostoServicio().intValue() - montoIva;
					detalle.setMontoItem(Math.round(montoNetoItem * transaccion.getCantidadServicio()));
					// detalle.setMontoItem(Integer.parseInt(detalle.getPrcItem().multiply(detalle.getQtyItem()).toString()));

				}

				montoNeto += detalle.getMontoItem();
				doc.getDetalle().add(detalle);
				indexTransaccion += 1;
			}



			Totales totales = new Totales();
			totales.setMntNeto(montoNeto);
			totales.setMntExe(0);
			// totales.setTasaIVA(BigDecimal.valueOf(Long.parseLong("19")));
			totales.setTasaIVA(BigDecimal.valueOf(iva));
			totales.setIVA(FacturaUtil.getIVA(iva, totales.getMntNeto()));
			totales.setMntTotal(totales.getMntNeto() + totales.getIVA());
			totales.setVlrPagar(totales.getMntNeto() + totales.getIVA());
			
			System.out.println(totales.getMntNeto());
			System.out.println(totales.getIVA());
			System.out.println(totales.getVlrPagar());

			encabezado.setTotales(totales);

			doc.setEncabezado(encabezado);
			dte.setDocumento(doc);
			WatchTimer watch = new WatchTimer();
			watch.start();
			WSEmision ws = new WSEmision(new URL(Constants.INVOICE_WS_URL));
			ResultadoInf retorno = ws.getWSEmisionSoap().emitirDTE(Constants.INVOICE_WS_USER,Constants.INVOICE_WS_PASSWD, dte, retornar);
			watch.stop();
			log.info("Tiempo WS FacturaServicio.Emitir= "+watch.getElapsedTimeSecs());
			// ResultadoInf retorno =
			// ws.getWSEmisionSoap12().emitirDTE(Constants.INVOICE_WS_USER,
			// Constants.INVOICE_WS_PASSWD, dte, retornar);

			log.info("retornar " + retornar);

			log.info("retorno " + retorno.getResultado());
			log.info("fechaHora " + retorno.getFechaHora());
			log.info("descripcion " + retorno.getDescripcion());
			log.info("detalle " + retorno.getDetalle());
			log.info("tipoDTE " + retorno.getTipoDTE());
			log.info("folio " + retorno.getFolio());
			log.info("rutEmisor " + retorno.getRutEmisor());

			//ResponseDTO response = new ResponseDTO();

			if (retorno.getResultado().compareTo("0") == 0) {
				response.setIdControl(0);
				String mensaje = "";

				for (ExportacionInf exportacion : retorno.getPDF().getExportacionInf()) {

					String file = FacturaUtil.getFileName(String.valueOf(retorno.getFolio()), exportacion.getModo());

					byte[] data = Base64.decodeBase64(exportacion.getPDFBase64());
					OutputStream stream = new FileOutputStream(Constants.DIR_INVOICES_PDF + '/' + file);
					stream.write(data);

					mensaje = mensaje + "\n " + Constants.URL_SITIO_PUBLICO + '/' + Constants.DIR_INVOICES_PDF + '/' + file;

				}

				response.setMsgControl(FacturaUtil.getFileName(String.valueOf(retorno.getFolio()), "00", false));

			} 
			else if(retorno.getResultado().compareTo("4") == 0) {
				
				File pdfFactura = new File(Constants.DIR_INVOICES_PDF + '/'  + archivo2 + ".pdf");
				if (pdfFactura.exists()) {
				    response.setIdControl(0);
				    response.setMsgControl(FacturaUtil.getFileName(String.valueOf(factura.getNumeroFactura()), "00", false));
				    return Response.ok(response).build();
				}else {
					response.setIdControl(1);
					response.setMsgControl(retorno.getDescripcion() + ": " + retorno.getDetalle());
				}
			}
			else {
				response.setIdControl(1);
				response.setMsgControl(retorno.getDescripcion() + ": " + retorno.getDetalle());
			}

			return Response.ok(response).build();
		} catch (Exception ex) {
			log.error("Metodo emitir - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Produces("application/pdf")
	@Consumes("application/json")
	@Path("/descargar/{fileName}")
	public Response descargar(@Context HttpServletRequest request, @PathParam("fileName") String nombreArchivo) throws Exception {

		File file = new File(Constants.DIR_INVOICES_PDF + "/" + nombreArchivo);
		ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment;filename=" + file.getName());
		return response.build();

	}

	@POST
	@Path("/rut")
	public Response rut(@Context HttpServletRequest request, FacturaDTO factura) {

		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: rut - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(facturaBusiness.consultaRut(factura)).build();
		} catch (Exception ex) {
			log.error("Metodo rut  - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/reemplazarBoleta")
	public Response reemplazarBoleta(@Context HttpServletRequest request, ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: reemplazarBoleta - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResponseDTO respuesta = facturaBusiness.reemplazarBoleta(reemplazaBoletaFacturaDTO);
			return Response.ok(respuesta).build();
		} catch (Exception ex) {
			log.error("Metodo reemplazarBoleta - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/reemplazarBoletaCero")
	public Response reemplazarBoletaCero(@Context HttpServletRequest request,
			ReemplazaBoletaFacturaDTO reemplazaBoletaFacturaDTO) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: reemplazarBoletaCero - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			return Response.ok(facturaBusiness.reemplazarBoletaCero(reemplazaBoletaFacturaDTO)).build();
		} catch (Exception ex) {
			log.error("Metodo reemplazarBoletaCero - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Produces("application/json")
	@Path("/obtenerIVA")
	public Response obtenerIVA(@Context HttpServletRequest request) {
		Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal == null || userPrincipal.getName() == null) {
			log.error("Error en el metodo: obtenerIVA - Sesion expiro");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		try {
			ResponseDTO response = facturaBusiness.obtenerIVA();
			return Response.ok(response).build();

		} catch (Exception ex) {
			log.error("Metodo obtenerIVA - Message: Error del Sistema " + ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
