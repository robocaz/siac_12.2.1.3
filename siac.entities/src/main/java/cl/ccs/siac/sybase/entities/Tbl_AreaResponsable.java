package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_AreaResponsable database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_AreaResponsable.findAll", query="SELECT t FROM Tbl_AreaResponsable t")
public class Tbl_AreaResponsable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAreaResponsable")
	private String fld_CodAreaResponsable;

	@Column(name="Fld_GlosaAreaResponsable")
	private String fld_GlosaAreaResponsable;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	public Tbl_AreaResponsable() {
	}

	public String getFld_CodAreaResponsable() {
		return this.fld_CodAreaResponsable;
	}

	public void setFld_CodAreaResponsable(String fld_CodAreaResponsable) {
		this.fld_CodAreaResponsable = fld_CodAreaResponsable;
	}

	public String getFld_GlosaAreaResponsable() {
		return this.fld_GlosaAreaResponsable;
	}

	public void setFld_GlosaAreaResponsable(String fld_GlosaAreaResponsable) {
		this.fld_GlosaAreaResponsable = fld_GlosaAreaResponsable;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

}