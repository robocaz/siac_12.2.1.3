package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_VtlValorMoneda database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_VtlValorMoneda.findAll", query="SELECT t FROM Tbl_VtlValorMoneda t")
public class Tbl_VtlValorMoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodMoneda")
	private byte fld_CodMoneda;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorMoneda")
	private BigDecimal fld_ValorMoneda;

	public Tbl_VtlValorMoneda() {
	}

	public byte getFld_CodMoneda() {
		return this.fld_CodMoneda;
	}

	public void setFld_CodMoneda(byte fld_CodMoneda) {
		this.fld_CodMoneda = fld_CodMoneda;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorMoneda() {
		return this.fld_ValorMoneda;
	}

	public void setFld_ValorMoneda(BigDecimal fld_ValorMoneda) {
		this.fld_ValorMoneda = fld_ValorMoneda;
	}

}