package cl.ccs.siac.usuario;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.inject.Named;

import cl.ccs.siac.dao.UsuarioDao;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.UsuarioDTO;

@Named("usuarioBusiness")
@Singleton
public class UsuarioBusinessImpl implements UsuarioBusiness {

	@Inject
	private UsuarioDao usuarioDao;

	public UsuarioDTO obtenerUsuario(String username) {

		UsuarioDTO usuarioDTO = new UsuarioDTO();
		usuarioDTO = usuarioDao.obtenerUsuario(username);
		return usuarioDTO;

	}

	public UsuarioDTO esSupervisor(String usuario, String password) {
		return usuarioDao.esSupervisor(usuario, password);
	}
	
	public ResponseDTO cambiarClave (String usuario, String password, String passwordAux){
		return usuarioDao.cambiarClave(usuario, password, passwordAux);
	}

}