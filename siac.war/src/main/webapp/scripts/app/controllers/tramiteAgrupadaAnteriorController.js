angular.module('ccs').controller('TramiteAgrupadaAnteriorController', function($scope, AclaracionResource, TransaccionResource, TramitanteResource, PersonaResource, $location, RutHelper, $rootScope, $uibModal, $document, $cookies, $timeout) {

	$scope.afectado = {};
	$scope.items = [];
  $scope.corrTransacciones = [];


	var rutMinimo	= parseInt(1000);
	var rutJuridico = parseInt(50000000);

	$scope.esvalido = false;
	$scope.editable = false;
	$scope.esrutjuridico = false;

	$scope.getTramitante = function(){
		$scope.limpiarAfectado();
	};

	$scope.esRutValido = function(){
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(RutHelper.validate($scope.afectado.rut) && rutNumerico > rutMinimo ){
			return true;
		}else{
			return false;
		}
	};

	$scope.esRutJuridico = function (rut) {
		var rutNumerico	= $scope.afectado.rut==undefined ?  0 :  parseInt( $scope.afectado.rut.slice(0, $scope.afectado.rut.length-1).trim());
		if(rutNumerico >= rutJuridico) {
			/*Es Juridico*/
			$scope.esrutjuridico = true;
		} else {
			$scope.esrutjuridico = false;
		}
		return $scope.esrutjuridico ;
	};

	$scope.cargaTramitante = function() {
		$scope.afectado.rut = $rootScope.tramitante.rut;
    $scope.afectado.apellidoPaterno = $rootScope.tramitante.apellidoPaterno;
    $scope.afectado.apellidoMaterno = $rootScope.tramitante.apellidoMaterno;
    $scope.afectado.nombres 		= $rootScope.tramitante.nombres;
    /*if( typeof($scope.afectado.fecha) === 'undefined' || $scope.afectado.fecha === null || $scope.afectado.fecha === '' ) {
	    var hoy = new Date();
	    $scope.afectado.fecha = hoy.getFullYear() + '-' + (Number(hoy.getMonth())+1) + '-' + hoy.getDate();
    }*/
    $scope.esvalido = true;

    $scope.buscaAfectado("tramitante");
	}

	$scope.limpiarAfectado = function(){
	    $scope.afectado.apellidoPaterno = null;
	    $scope.afectado.apellidoMaterno = null;
	    $scope.afectado.nombres 		= null;
	    $scope.afectado.nombreafectado = null;
	    $scope.items = [];
	    $scope.corrTransacciones = [];
	    $scope.esrutjuridico = false;
	    $scope.editable = false;
	};

	$scope.limpiarFormulario = function(){
		$scope.limpiarAfectado();
		$scope.afectado.rut = '';
		$scope.afectado.fecha = '';
	};

  $scope.buscaAfectado = function (tipoPersona) {
    $scope.limpiarAfectado();
    $scope.esvalido = false;

    if($scope.afectado.rut === undefined ){
      return;
    }

    if($scope.afectado.rut === null ){
      $scope.afectado.rut  = undefined;
      swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
        function () {
          $("#rutAfectadoInforme").focus();
        });
      return;
    }

    if($scope.esRutValido()){
      if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
        swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
      }else{
        if(tipoPersona == "tramitante"){
          $scope.obtenerAfectadoTramitante();
        }
        else{
          $scope.obtenerAfectado();
        }
      }
    }else{
      swal({ title: 'Error', text: 'Rut no es válido',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'});
      $scope.afectado.rut = null;
    }
  };

  $scope.obtenerAfectado = function(){
    var successCallback = function(data,responseHeaders){
      $scope.esvalido = true;
      if(data.correlativo == "0"){
        $scope.editable = true;
        swal('Alerta','Tramitante no encontrado','error');

      } else {
        $scope.editable = false;
        if($scope.esrutjuridico){
          $scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
        }else{
          $scope.afectado.apellidoPaterno = data.apellidoPaterno;
          $scope.afectado.apellidoMaterno = data.apellidoMaterno;
          $scope.afectado.nombres 		= data.nombres;
        }
        //$scope.obtenerLista();
        $('#ap_pat_pag').focus();
      }
    };
    PersonaResource.obtenerPersona($scope.afectado, successCallback, $scope.errorCallback);
  };


  $scope.obtenerAfectadoTramitante = function(){
    var successCallback = function(data,responseHeaders){
      $scope.esvalido = true;
      if(data.correlativo == "0"){
        $scope.editable = true;
        swal('Alerta','Tramitante no encontrado','error');

      } else {
        $scope.afectado.corrNombre = data.correlativo;
        $scope.editable = false;
        if($scope.esrutjuridico){
          $scope.afectado.nombreafectado = data.apellidoPaterno +  data.apellidoMaterno + data.nombres;
        }else{
          $scope.afectado.apellidoPaterno = data.apellidoPaterno;
          $scope.afectado.apellidoMaterno = data.apellidoMaterno;
          $scope.afectado.nombres 		= data.nombres;
        }
        //$scope.obtenerLista();
        $('#ap_pat_pag').focus();
      }
    };
    TramitanteResource.carga({rut: $scope.afectado.rut}, successCallback, $scope.errorCallback);
  };

	$scope.obtenerLista = function(){

		if( typeof($scope.afectado.fecha) === 'undefined' || $scope.afectado.fecha === null || $scope.afectado.fecha === '' || !$scope.esFecha($scope.afectado.fecha) ){
	      swal({ title: 'Error', text: 'Fecha no es válida',type: 'error',  showCloseButton: true, confirmButtonText: 'cerrar'}).then(
	        function () {
	          $("#fech_acl").focus();
	        });
	      return;
	    }

		// Rut
		if( typeof($scope.afectado.rut) === 'undefined' || $scope.afectado.rut === null || $scope.afectado.rut.trim() === '' || !$scope.esRutValido() ) {
	      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
    // Declaración de Uso
    if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
      swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
      return;
    }
		// Apellido Paterno
		if( !$scope.esRutJuridico() && ( typeof($scope.afectado.apellidoPaterno) === 'undefined' || $scope.afectado.apellidoPaterno === null || $scope.afectado.apellidoPaterno.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Apellido Paterno', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
		// Nombres
		if( !$scope.esRutJuridico() && ( typeof($scope.afectado.nombres) === 'undefined' || $scope.afectado.nombres === null || $scope.afectado.nombres.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }
		// Razon Social
		if( $scope.esRutJuridico() && ( typeof($scope.afectado.nombreafectado) === 'undefined' || $scope.afectado.nombreafectado === null || $scope.afectado.nombreafectado.trim() === '' ) ) {
	      swal({ title: 'Error', text: 'Debe ingresar el Nombre Afectado', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	      return;
	    }

		//var dateParts = $scope.afectado.fecha.split('-');
		//var dateString = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];

		fechaArray = $scope.afectado.fecha.split("");
        var fechaString = fechaArray[4] + fechaArray[5] + fechaArray[6] + fechaArray[7] + "-" + fechaArray[2] + fechaArray[3] + "-" + fechaArray[0] + fechaArray[1];
	    var fecha = new Date(fechaString + "T12:00:00.000Z");

	    console.log('La Fecha! ' + fecha.toString());
	  // var fecha = new Date($scope.afectado.fecha);

	  var consultaRutFecha = {
	      fecAclaracion: fecha.getTime(),
	      rutAfectado: RutHelper.getRutSP($scope.afectado.rut)
	    };
		var successConsultaRutFechaCallback = function(dataConsultaRutFecha){
	      if( dataConsultaRutFecha.idControl !== 0 ) {
	        swal({ title: 'Error', text: ( dataConsultaRutFecha.msgControl === ' ' ? 'No se logró obtener aclaraciones!' : dataConsultaRutFecha.msgControl ), type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
	        return;
	      }
	      $scope.items = dataConsultaRutFecha.lista;
		};
    AclaracionResource.consultaRutFecha(consultaRutFecha, successConsultaRutFechaCallback, $scope.errorCallback);
	};

  $scope.aceptar = function () {
    console.log('aceptar');
    console.log($scope.corrTransacciones);

    // Rut
    if( typeof($scope.afectado.rut) === 'undefined' || $scope.afectado.rut === null || $scope.afectado.rut.trim() === '' || !$scope.esRutValido() ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    // Declaración de Uso
    if($scope.esRutJuridico() && $rootScope.tramitante.declaracionUso){
      swal('Error','Al tener documento Declaración de Uso asociado, solo puede solicitar certificados de personas naturales.','error');
      return;
    }
    // Apellido Paterno
    if( !$scope.esRutJuridico() && ( typeof($scope.afectado.apellidoPaterno) === 'undefined' || $scope.afectado.apellidoPaterno === null || $scope.afectado.apellidoPaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Apellido Paterno', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    // Nombres
    if( !$scope.esRutJuridico() && ( typeof($scope.afectado.nombres) === 'undefined' || $scope.afectado.nombres === null || $scope.afectado.nombres.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
    // Razon Social
    if( $scope.esRutJuridico() && ( typeof($scope.afectado.nombreafectado) === 'undefined' || $scope.afectado.nombreafectado === null || $scope.afectado.nombreafectado.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Nombre Afectado', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    $scope.corrTransacciones = [];
    for(var i=0; i < $scope.items.length; i++) {
      var radio = $('#corrTransaccion'+i).val();
      if( $('#corrTransaccion'+i).prop('checked') ) {
    	var cantidad = $('#cantidad'+i).val();
        if( cantidad === null || cantidad === '' || isNaN(cantidad) || parseInt(cantidad) <= 0 ) {
          nro = i + 1;
          swal({ title: 'Error', text: 'Debe ingresar una cantidad válida para el registro #' + nro, type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
          return;
        }else {
          $scope.corrTransacciones.push({corrTransaccion: radio, cantidad: cantidad});
        }
      }
    }

    if( $scope.corrTransacciones.length <= 0 ){
      swal({ title: 'Error', text: 'Debe seleccionar al menos un registro', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    $scope.validacionCertificado();
  };


  $scope.validacionCertificado = function () {
	  
	$scope.$emit('logout');
	  
	// Rut
	if( typeof($scope.afectado.rut) === 'undefined' || $scope.afectado.rut === null || $scope.afectado.rut.trim() === '' || !$scope.esRutValido() ) {
      swal({ title: 'Error', text: 'Debe ingresar un Rut válido', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
	// Apellido Paterno
	if( !$scope.esRutJuridico() && ( typeof($scope.afectado.apellidoPaterno) === 'undefined' || $scope.afectado.apellidoPaterno === null || $scope.afectado.apellidoPaterno.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Apellido Paterno', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
	// Nombres
	if( !$scope.esRutJuridico() && ( typeof($scope.afectado.nombres) === 'undefined' || $scope.afectado.nombres === null || $scope.afectado.nombres.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Nombre', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }
	// Razon Social
	if( $scope.esRutJuridico() && ( typeof($scope.afectado.nombreafectado) === 'undefined' || $scope.afectado.nombreafectado === null || $scope.afectado.nombreafectado.trim() === '' ) ) {
      swal({ title: 'Error', text: 'Debe ingresar el Nombre Afectado', type: 'error', showCloseButton: true, confirmButtonText: 'cerrar'});
      return;
    }

    if(!$scope.esrutjuridico && !$rootScope.tramitante.declaracionUso && $scope.afectado.rut != $rootScope.tramitante.rut){
      $scope.authSupervisor();
    }else{
      $scope.guardarPersona();
    }
  };

	$scope.authSupervisor = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/modal/modalloginsupervisor.html',
			controller: 'ModalLoginSupervisorController',
			appendTo: angular.element($document[0].querySelector('#tramiteagrupanterior')),
			size: 'sm'
		  });
		  modalInstance.result.then(function (respuesta) {
			  if(respuesta){
				  $scope.guardarPersona();
			  }
		  });
	};

  $scope.onCheckCambia = function(index) {
	  var radio = $('#corrTransaccion'+index).val();
      if( $('#corrTransaccion'+index).prop('checked') ) {
    	$('#cantidad'+index).val('1');
      }
  };

  $scope.guardar = function(){
    var largo = Number($scope.corrTransacciones.length - 1);
    for(var i=0; i < $scope.corrTransacciones.length; i++) {
      var trans = i;
      var cantidad = Number($scope.corrTransacciones[i].cantidad);

      for(var j=0; j < cantidad; j++) {
	      $scope.transaccion = {
	        codServicio : 'CAA',
	        rutAfectado : $scope.afectado.rut,
	        corrNombre: $scope.afectado.corrNombre,
	        corrAclaracion: Number($scope.corrTransacciones[i].corrTransaccion)
	      };
	      var successCallback = function(data){
	        $scope.listarServicioTramitante();

	        if( trans === largo ) {
	          // $('#tramiteagrupanterior').modal('hide');
            $("#fech_acl").focus();

	          swal({
	            title: 'Operación Exitosa',
	            text: "Actualización realizada exitosamente",
	            type: 'success',
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Continuar'
	          }).then(function () {
	            if (! $scope.esrutjuridico && !$rootScope.tramitante.declaracionUso ) {
	              swal('Atención', 'Recuerde guardar copia de la Cedúla de Identidad Vigente', 'warning');
	            }
	          });
	        }
	      };
	      TransaccionResource.agregarservicio($scope.transaccion, successCallback, $scope.errorCallback);
      }
    }
  };

  $scope.guardarPersona = function(){

    if($scope.esrutjuridico){
      $scope.afectado.apellidoPaterno = $scope.afectado.nombreafectado.substring(0, 25);
      $scope.afectado.apellidoMaterno = $scope.afectado.nombreafectado.substring(25, 50);
      $scope.afectado.nombres 		= $scope.afectado.nombreafectado.substring(50, 75);
    }

    var successCallback = function(data,responseHeaders){

      if(data.correlativo == "0"){
        swal('Alerta','No se ha actualizado el tramitante','error');
      } else{
        $scope.afectado.corrNombre = data.correlativo;
        $scope.guardar();
      }
    };
    PersonaResource.actualizar($scope.afectado, successCallback, $scope.errorCallback);
  };

	$scope.isValidDate =  function(date) {
		console.log('validando fecha: ' + date);
		var dateParts = date.split('-');
		//var dateString = Number(dateParts[2]) + "-" + Number(dateParts[1]) + "-" + Number(dateParts[0]);
		var dateString = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
		console.log('dateString: ' + dateString);
	    /*var d = new Date(dateString);
	    console.log(d);
	    return (d &&  d.getDate() == Number(dateParts[0])  && (d.getMonth() + 1) == Number(dateParts[1]) && d.getFullYear() == Number(dateParts[2]));*/

		var d = new Date(dateString);
		if ( Object.prototype.toString.call(d) === "[object Date]" ) {
		  // it is a date
		  if ( isNaN( d.getTime() ) ) {  // d.valueOf() could also work
		    // date is not valid
			  return false;
		  }
		  else {
		    // date is valid
			return true;
		  }
		}

		return false;
	};

	$scope.esFecha = function(fecha){
		console.log('esFecha? ' + fecha);
		dateArray = fecha.split("");
        var fechaString = dateArray[0] + dateArray[1] + "-" + dateArray[2] + dateArray[3] + "-" + dateArray[4] + dateArray[5] + dateArray[6] + dateArray[7];
        return $scope.isValidDate(fechaString);
 		/*if(!$scope.isValidDate(fechaString)){
			$scope.afectado.fecha = "";
			swal('Alerta','La fecha ingresada es Inválida','error');
			$("#fech_acl").focus();
			return false;
 		}
 		return true;*/
	};

	$('#tramiteagrupanterior').on('show.bs.modal', function (e) {
		$timeout(function(){
			$scope.afectado.rut = undefined;
			$scope.afectado.fecha = getStrFechaDate(new Date());
			$scope.getTramitante();
			angular.element("#fech_acl").focus();
		});
	});

	function getStrFechaDate(date) {
		var dia = Number(date.getDate()) < 10 ? '0' + date.getDate() : date.getDate();
		var mes = Number(date.getMonth()) < 9 ? '0'+ Number(date.getMonth()+1) : Number(date.getMonth()+1);
		var anio = date.getFullYear();
		return dia +''+ mes +''+ anio;
	}

});
