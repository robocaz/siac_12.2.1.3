package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tmp_rutbol database table.
 * 
 */
@Entity
@Table(name="tmp_rutbol")
@NamedQuery(name="TmpRutbol.findAll", query="SELECT t FROM TmpRutbol t")
public class TmpRutbol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	public TmpRutbol() {
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

}