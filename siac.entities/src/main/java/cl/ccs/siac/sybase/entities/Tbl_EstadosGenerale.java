package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_EstadosGenerales database table.
 * 
 */
@Entity
@Table(name="Tbl_EstadosGenerales")
@NamedQuery(name="Tbl_EstadosGenerale.findAll", query="SELECT t FROM Tbl_EstadosGenerale t")
public class Tbl_EstadosGenerale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodAplicacion")
	private String fld_CodAplicacion;

	@Column(name="Fld_CodEstado")
	private short fld_CodEstado;

	@Column(name="Fld_CodProceso")
	private byte fld_CodProceso;

	@Column(name="Fld_FecEstado")
	private Timestamp fld_FecEstado;

	@Column(name="Fld_GlosaEstado")
	private String fld_GlosaEstado;

	@Column(name="Fld_NroRegistros")
	private short fld_NroRegistros;

	@Column(name="Fld_Timestamp")
	private byte[] fld_Timestamp;

	@Column(name="Fld_ValorEstado")
	private BigDecimal fld_ValorEstado;

	@Column(name="Fld_ValorString")
	private String fld_ValorString;

	public Tbl_EstadosGenerale() {
	}

	public String getFld_CodAplicacion() {
		return this.fld_CodAplicacion;
	}

	public void setFld_CodAplicacion(String fld_CodAplicacion) {
		this.fld_CodAplicacion = fld_CodAplicacion;
	}

	public short getFld_CodEstado() {
		return this.fld_CodEstado;
	}

	public void setFld_CodEstado(short fld_CodEstado) {
		this.fld_CodEstado = fld_CodEstado;
	}

	public byte getFld_CodProceso() {
		return this.fld_CodProceso;
	}

	public void setFld_CodProceso(byte fld_CodProceso) {
		this.fld_CodProceso = fld_CodProceso;
	}

	public Timestamp getFld_FecEstado() {
		return this.fld_FecEstado;
	}

	public void setFld_FecEstado(Timestamp fld_FecEstado) {
		this.fld_FecEstado = fld_FecEstado;
	}

	public String getFld_GlosaEstado() {
		return this.fld_GlosaEstado;
	}

	public void setFld_GlosaEstado(String fld_GlosaEstado) {
		this.fld_GlosaEstado = fld_GlosaEstado;
	}

	public short getFld_NroRegistros() {
		return this.fld_NroRegistros;
	}

	public void setFld_NroRegistros(short fld_NroRegistros) {
		this.fld_NroRegistros = fld_NroRegistros;
	}

	public byte[] getFld_Timestamp() {
		return this.fld_Timestamp;
	}

	public void setFld_Timestamp(byte[] fld_Timestamp) {
		this.fld_Timestamp = fld_Timestamp;
	}

	public BigDecimal getFld_ValorEstado() {
		return this.fld_ValorEstado;
	}

	public void setFld_ValorEstado(BigDecimal fld_ValorEstado) {
		this.fld_ValorEstado = fld_ValorEstado;
	}

	public String getFld_ValorString() {
		return this.fld_ValorString;
	}

	public void setFld_ValorString(String fld_ValorString) {
		this.fld_ValorString = fld_ValorString;
	}

}