package cl.ccs.siac.cuadratura;

import java.util.List;

import cl.exe.ccs.dto.CajaInformeDTO;
import cl.exe.ccs.dto.CentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaAclaracionDTO;
import cl.exe.ccs.dto.CuadraturaCajaDTO;
import cl.exe.ccs.dto.CuadraturaCentroCostoDTO;
import cl.exe.ccs.dto.CuadraturaUsuarioDTO;
import cl.exe.ccs.dto.DatoBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleBoletaDTO;
import cl.exe.ccs.dto.DetalleBoletaFacturaDTO;
import cl.exe.ccs.dto.DetalleCentroCostoDTO;
import cl.exe.ccs.dto.DetalleServicioDTO;
import cl.exe.ccs.dto.PorcentajeIvaDTO;
import cl.exe.ccs.dto.ReclamoDTO;
import cl.exe.ccs.dto.ResumenMovimientoDTO;
import cl.exe.ccs.dto.UsuariosInformeDTO;

public interface CuadraturaBusiness {

	
	public List<CentroCostoDTO> centrosCostoInforme(Integer centroCosto, String desde, String hasta);
	
	public List<UsuariosInformeDTO> usuariosInforme(Integer centroCosto, String desde, String hasta);
	
	public List<CajaInformeDTO> cajaInforme(String usuario, Integer centroCosto, String desde, String hasta);
	
	public CuadraturaCentroCostoDTO cuadraturaPorCamara(Integer centroCosto, String desde, String hasta);
	
	public CuadraturaUsuarioDTO cuadraturaPorUsuario(String usuario, Integer centroCosto, String desde, String hasta);
	
	public DetalleCentroCostoDTO cuadraturaPorCentroCosto(Integer centroCosto, String desde, String hasta);
	
	public PorcentajeIvaDTO obtienePorcentajeIva();
	
	public List<CuadraturaCajaDTO> buscarCajasPorUsuario(String usuario, Integer centroCosto, String fechaInicio);
	
	public List<CuadraturaCajaDTO> buscarCajasPorCajero(String usuario, Long fechaInicio);

	public ResumenMovimientoDTO obtieneMovimientosNac(Integer centroCosto, String fechaInicio, String fechaDesde);
	
	public ResumenMovimientoDTO obtieneMovimientos(String fechaInicio, String usuario, Integer centroCosto, Integer corrCaja, Integer criterio );
	
	public DetalleBoletaDTO obtieneDetalleBoleta(String fechaInicio, String usuario, Integer corrCaja, Integer centroCosto, Integer criterio);
	
	public DetalleBoletaFacturaDTO obtieneDetalleBoletaFactura(DetalleBoletaFacturaDTO detalle);
	
	public List<DatoBoletaFacturaDTO> getDetalleBoletaModal(DetalleBoletaFacturaDTO detalle);
	
	public DetalleBoletaFacturaDTO getDetalleFacturaModal(DetalleBoletaFacturaDTO detalle);
	
	public DetalleServicioDTO obtieneDetalleServicio(String fechaInicio, String usuario, Integer corrCaja,Integer centroCosto, Integer criterio, Integer ordenamiento);
	
	public CuadraturaAclaracionDTO validaAclaraciones(Integer corrCaja);
	
	public ReclamoDTO solicitudesReclamos(Integer corrCaja);
}
