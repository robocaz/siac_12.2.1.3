package cl.exe.ccs.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProtestoData implements Serializable{

	private String rutAfectado;
	private String glosaCorta;
	private String tipoDocumento;
	private String glosaMoneda;
	private BigDecimal montoProt;
	private String fecProt;
	private String fecPago;
	private Integer nroOper4Dig;
	private Integer nroBoletin;
	private String glosaEstado;
	private BigDecimal corrProt;
	private BigDecimal corrAclId;
	private String fecVcto;

	public String getRutAfectado() {
		return rutAfectado;
	}
	public void setRutAfectado(String rutAfectado) {
		this.rutAfectado = rutAfectado;
	}
	public String getGlosaCorta() {
		return glosaCorta;
	}
	public void setGlosaCorta(String glosaCorta) {
		this.glosaCorta = glosaCorta;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getGlosaMoneda() {
		return glosaMoneda;
	}
	public void setGlosaMoneda(String glosaMoneda) {
		this.glosaMoneda = glosaMoneda;
	}
	public BigDecimal getMontoProt() {
		return montoProt;
	}
	public void setMontoProt(BigDecimal montoProt) {
		this.montoProt = montoProt;
	}
	public String getFecProt() {
		return fecProt;
	}
	public void setFecProt(String fecProt) {
		this.fecProt = fecProt;
	}
	public String getFecPago() {
		return fecPago;
	}
	public void setFecPago(String fecPago) {
		this.fecPago = fecPago;
	}
	public Integer getNroOper4Dig() {
		return nroOper4Dig;
	}
	public void setNroOper4Dig(Integer nroOper4Dig) {
		this.nroOper4Dig = nroOper4Dig;
	}
	public Integer getNroBoletin() {
		return nroBoletin;
	}
	public void setNroBoletin(Integer nroBoletin) {
		this.nroBoletin = nroBoletin;
	}
	public String getGlosaEstado() {
		return glosaEstado;
	}
	public void setGlosaEstado(String glosaEstado) {
		this.glosaEstado = glosaEstado;
	}
	public BigDecimal getCorrProt() {
		return corrProt;
	}
	public void setCorrProt(BigDecimal corrProt) {
		this.corrProt = corrProt;
	}
	public BigDecimal getCorrAclId() {
		return corrAclId;
	}
	public void setCorrAclId(BigDecimal corrAclId) {
		this.corrAclId = corrAclId;
	}
	public String getFecVcto() {
		return fecVcto;
	}
	public void setFecVcto(String fecVcto) {
		this.fecVcto = fecVcto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProtestoData [rutAfectado=");
		builder.append(rutAfectado);
		builder.append(", glosaCorta=");
		builder.append(glosaCorta);
		builder.append(", tipoDocumento=");
		builder.append(tipoDocumento);
		builder.append(", glosaMoneda=");
		builder.append(glosaMoneda);
		builder.append(", montoProt=");
		builder.append(montoProt);
		builder.append(", fecProt=");
		builder.append(fecProt);
		builder.append(", fecPago=");
		builder.append(fecPago);
		builder.append(", nroOper4Dig=");
		builder.append(nroOper4Dig);
		builder.append(", nroBoletin=");
		builder.append(nroBoletin);
		builder.append(", glosaEstado=");
		builder.append(glosaEstado);
		builder.append(", corrProt=");
		builder.append(corrProt);
		builder.append(", corrAclId=");
		builder.append(corrAclId);
		builder.append(", fecVcto=");
		builder.append(fecVcto);
		builder.append("]");
		return builder.toString();
	}

}
