package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_BCRut database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_BCRut.findAll", query="SELECT t FROM Tbl_BCRut t")
public class Tbl_BCRut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_FecBIC")
	private Timestamp fld_FecBIC;

	@Column(name="Fld_FecIngreso")
	private Timestamp fld_FecIngreso;

	@Column(name="Fld_FecMOL")
	private Timestamp fld_FecMOL;

	@Column(name="Fld_Nombre")
	private String fld_Nombre;

	@Column(name="Fld_RolCausa")
	private String fld_RolCausa;

	@Column(name="Fld_Rut")
	private String fld_Rut;

	@Column(name="Fld_TipoProcedimiento")
	private String fld_TipoProcedimiento;

	@Column(name="Fld_Tribunal")
	private String fld_Tribunal;

	@Column(name="Fld_VeedorLiquidador")
	private String fld_VeedorLiquidador;

	public Tbl_BCRut() {
	}

	public Timestamp getFld_FecBIC() {
		return this.fld_FecBIC;
	}

	public void setFld_FecBIC(Timestamp fld_FecBIC) {
		this.fld_FecBIC = fld_FecBIC;
	}

	public Timestamp getFld_FecIngreso() {
		return this.fld_FecIngreso;
	}

	public void setFld_FecIngreso(Timestamp fld_FecIngreso) {
		this.fld_FecIngreso = fld_FecIngreso;
	}

	public Timestamp getFld_FecMOL() {
		return this.fld_FecMOL;
	}

	public void setFld_FecMOL(Timestamp fld_FecMOL) {
		this.fld_FecMOL = fld_FecMOL;
	}

	public String getFld_Nombre() {
		return this.fld_Nombre;
	}

	public void setFld_Nombre(String fld_Nombre) {
		this.fld_Nombre = fld_Nombre;
	}

	public String getFld_RolCausa() {
		return this.fld_RolCausa;
	}

	public void setFld_RolCausa(String fld_RolCausa) {
		this.fld_RolCausa = fld_RolCausa;
	}

	public String getFld_Rut() {
		return this.fld_Rut;
	}

	public void setFld_Rut(String fld_Rut) {
		this.fld_Rut = fld_Rut;
	}

	public String getFld_TipoProcedimiento() {
		return this.fld_TipoProcedimiento;
	}

	public void setFld_TipoProcedimiento(String fld_TipoProcedimiento) {
		this.fld_TipoProcedimiento = fld_TipoProcedimiento;
	}

	public String getFld_Tribunal() {
		return this.fld_Tribunal;
	}

	public void setFld_Tribunal(String fld_Tribunal) {
		this.fld_Tribunal = fld_Tribunal;
	}

	public String getFld_VeedorLiquidador() {
		return this.fld_VeedorLiquidador;
	}

	public void setFld_VeedorLiquidador(String fld_VeedorLiquidador) {
		this.fld_VeedorLiquidador = fld_VeedorLiquidador;
	}

}