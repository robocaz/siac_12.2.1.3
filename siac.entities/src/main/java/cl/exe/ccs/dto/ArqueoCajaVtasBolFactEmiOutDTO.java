package cl.exe.ccs.dto;

import java.io.Serializable;

public class ArqueoCajaVtasBolFactEmiOutDTO extends ResponseDTO implements Serializable{

	private static final long serialVersionUID = 7881370185022411706L;

	private Integer totBolNulas;
	private Integer totBolEmitidas;
	private Integer totFacEmitidas;
	private Integer totFacNulas;
	private Integer totFolCeroNulo;
	private Integer totFolCeroEmit;

	public Integer getTotBolNulas() {
		return totBolNulas;
	}
	public void setTotBolNulas(Integer totBolNulas) {
		this.totBolNulas = totBolNulas;
	}
	public Integer getTotBolEmitidas() {
		return totBolEmitidas;
	}
	public void setTotBolEmitidas(Integer totBolEmitidas) {
		this.totBolEmitidas = totBolEmitidas;
	}
	public Integer getTotFacEmitidas() {
		return totFacEmitidas;
	}
	public void setTotFacEmitidas(Integer totFacEmitidas) {
		this.totFacEmitidas = totFacEmitidas;
	}
	public Integer getTotFacNulas() {
		return totFacNulas;
	}
	public void setTotFacNulas(Integer totFacNulas) {
		this.totFacNulas = totFacNulas;
	}
	public Integer getTotFolCeroNulo() {
		return totFolCeroNulo;
	}
	public void setTotFolCeroNulo(Integer totFolCeroNulo) {
		this.totFolCeroNulo = totFolCeroNulo;
	}
	public Integer getTotFolCeroEmit() {
		return totFolCeroEmit;
	}
	public void setTotFolCeroEmit(Integer totFolCeroEmit) {
		this.totFolCeroEmit = totFolCeroEmit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArqueoCajaVtasBolFactEmiOutDTO [totBolNulas=");
		builder.append(totBolNulas);
		builder.append(", totBolEmitidas=");
		builder.append(totBolEmitidas);
		builder.append(", totFacEmitidas=");
		builder.append(totFacEmitidas);
		builder.append(", totFacNulas=");
		builder.append(totFacNulas);
		builder.append(", totFolCeroNulo=");
		builder.append(totFolCeroNulo);
		builder.append(", totFolCeroEmit=");
		builder.append(totFolCeroEmit);
		builder.append(", msgControl=");
		builder.append(msgControl);
		builder.append(", idControl=");
		builder.append(idControl);
		builder.append("]");
		return builder.toString();
	}

}
