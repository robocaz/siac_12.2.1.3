package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_DETAUTENTICACION database table.
 * 
 */
@Entity
@Table(name="TBL_DETAUTENTICACION")
@NamedQuery(name="TblDetautenticacion.findAll", query="SELECT t FROM TblDetautenticacion t")
public class TblDetautenticacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECACLARACION")
	private Date fldFecaclaracion;

	@Temporal(TemporalType.DATE)
	@Column(name="FLD_FECHAVCTO")
	private Date fldFechavcto;

	@Column(name="FLD_GLOSACORTAMONEDA")
	private String fldGlosacortamoneda;

	@Column(name="FLD_MONTODEUDA")
	private BigDecimal fldMontodeuda;

	@Column(name="FLD_NOMBREDEUDOR")
	private String fldNombredeudor;

	@Column(name="FLD_NOMBREEMISOR")
	private String fldNombreemisor;

	@Column(name="FLD_NRODOCUMENTO")
	private BigDecimal fldNrodocumento;

	@Column(name="FLD_TIPOCREDITO")
	private String fldTipocredito;

	@Column(name="FLD_TIPODOCUMENTO")
	private String fldTipodocumento;

	//bi-directional many-to-one association to TblCodautenticacion
	@ManyToOne
	@JoinColumn(name="FLD_CORRAUTENTICACION")
	private TblCodautenticacion tblCodautenticacion;

	public TblDetautenticacion() {
	}

	public Date getFldFecaclaracion() {
		return this.fldFecaclaracion;
	}

	public void setFldFecaclaracion(Date fldFecaclaracion) {
		this.fldFecaclaracion = fldFecaclaracion;
	}

	public Date getFldFechavcto() {
		return this.fldFechavcto;
	}

	public void setFldFechavcto(Date fldFechavcto) {
		this.fldFechavcto = fldFechavcto;
	}

	public String getFldGlosacortamoneda() {
		return this.fldGlosacortamoneda;
	}

	public void setFldGlosacortamoneda(String fldGlosacortamoneda) {
		this.fldGlosacortamoneda = fldGlosacortamoneda;
	}

	public BigDecimal getFldMontodeuda() {
		return this.fldMontodeuda;
	}

	public void setFldMontodeuda(BigDecimal fldMontodeuda) {
		this.fldMontodeuda = fldMontodeuda;
	}

	public String getFldNombredeudor() {
		return this.fldNombredeudor;
	}

	public void setFldNombredeudor(String fldNombredeudor) {
		this.fldNombredeudor = fldNombredeudor;
	}

	public String getFldNombreemisor() {
		return this.fldNombreemisor;
	}

	public void setFldNombreemisor(String fldNombreemisor) {
		this.fldNombreemisor = fldNombreemisor;
	}

	public BigDecimal getFldNrodocumento() {
		return this.fldNrodocumento;
	}

	public void setFldNrodocumento(BigDecimal fldNrodocumento) {
		this.fldNrodocumento = fldNrodocumento;
	}

	public String getFldTipocredito() {
		return this.fldTipocredito;
	}

	public void setFldTipocredito(String fldTipocredito) {
		this.fldTipocredito = fldTipocredito;
	}

	public String getFldTipodocumento() {
		return this.fldTipodocumento;
	}

	public void setFldTipodocumento(String fldTipodocumento) {
		this.fldTipodocumento = fldTipodocumento;
	}

	public TblCodautenticacion getTblCodautenticacion() {
		return this.tblCodautenticacion;
	}

	public void setTblCodautenticacion(TblCodautenticacion tblCodautenticacion) {
		this.tblCodautenticacion = tblCodautenticacion;
	}

}