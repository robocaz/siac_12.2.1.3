package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tbl_CausalAclConsulta database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CausalAclConsulta.findAll", query="SELECT t FROM Tbl_CausalAclConsulta t")
public class Tbl_CausalAclConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CierreAutomatico")
	private boolean fld_CierreAutomatico;

	@Column(name="Fld_CodCausal")
	private byte fld_CodCausal;

	@Column(name="Fld_GlosaCausal")
	private String fld_GlosaCausal;

	@Column(name="Fld_TipoAclCons")
	private String fld_TipoAclCons;

	public Tbl_CausalAclConsulta() {
	}

	public boolean getFld_CierreAutomatico() {
		return this.fld_CierreAutomatico;
	}

	public void setFld_CierreAutomatico(boolean fld_CierreAutomatico) {
		this.fld_CierreAutomatico = fld_CierreAutomatico;
	}

	public byte getFld_CodCausal() {
		return this.fld_CodCausal;
	}

	public void setFld_CodCausal(byte fld_CodCausal) {
		this.fld_CodCausal = fld_CodCausal;
	}

	public String getFld_GlosaCausal() {
		return this.fld_GlosaCausal;
	}

	public void setFld_GlosaCausal(String fld_GlosaCausal) {
		this.fld_GlosaCausal = fld_GlosaCausal;
	}

	public String getFld_TipoAclCons() {
		return this.fld_TipoAclCons;
	}

	public void setFld_TipoAclCons(String fld_TipoAclCons) {
		this.fld_TipoAclCons = fld_TipoAclCons;
	}

}