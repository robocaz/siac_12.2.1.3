package cl.ccs.siac.dao;

import cl.exe.ccs.dto.BoletaDTO;
import cl.exe.ccs.dto.BoletaTransaccionDTO;
import cl.exe.ccs.dto.ControlBoletaDTO;
import cl.exe.ccs.dto.ResponseDTO;
import cl.exe.ccs.dto.ResponseListDTO;

public interface BoletaDao {

    public BoletaDTO obtenerBoleta(BoletaDTO boletaDTO);
    public BoletaDTO obtenerBoletaTransaccion(BoletaDTO boletaDTO);
    public ResponseListDTO<Integer> obtenerBoletasFolioCero(BoletaDTO boletaDTO);
	public ResponseDTO consultaBoleta(ControlBoletaDTO boletaDTO);
	public ControlBoletaDTO inicializaBoleta(ControlBoletaDTO boletaDTO);
	public ResponseDTO actualizaRangoBoleta(BoletaTransaccionDTO boletaDTO);
	public ResponseDTO anularBoleta(ControlBoletaDTO boletaDTO);
	
}
