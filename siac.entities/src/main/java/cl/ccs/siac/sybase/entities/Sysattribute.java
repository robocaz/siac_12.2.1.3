package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sysattributes database table.
 * 
 */
@Entity
@Table(name="sysattributes")
@NamedQuery(name="Sysattribute.findAll", query="SELECT s FROM Sysattribute s")
public class Sysattribute implements Serializable {
	private static final long serialVersionUID = 1L;

	private short attribute;

	@Column(name="char_value")
	private String charValue;

	@Column(name="class")
	private short class_;

	private String comments;

	@Lob
	@Column(name="image_value")
	private byte[] imageValue;

	@Column(name="int_value")
	private int intValue;

	private int object;

	@Column(name="object_cinfo")
	private String objectCinfo;

	@Column(name="object_cinfo2")
	private String objectCinfo2;

	@Column(name="object_datetime")
	private Object objectDatetime;

	@Column(name="object_info1")
	private int objectInfo1;

	@Column(name="object_info2")
	private int objectInfo2;

	@Column(name="object_info3")
	private int objectInfo3;

	@Column(name="object_type")
	private String objectType;

	@Lob
	@Column(name="text_value")
	private String textValue;

	public Sysattribute() {
	}

	public short getAttribute() {
		return this.attribute;
	}

	public void setAttribute(short attribute) {
		this.attribute = attribute;
	}

	public String getCharValue() {
		return this.charValue;
	}

	public void setCharValue(String charValue) {
		this.charValue = charValue;
	}

	public short getClass_() {
		return this.class_;
	}

	public void setClass_(short class_) {
		this.class_ = class_;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public byte[] getImageValue() {
		return this.imageValue;
	}

	public void setImageValue(byte[] imageValue) {
		this.imageValue = imageValue;
	}

	public int getIntValue() {
		return this.intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public int getObject() {
		return this.object;
	}

	public void setObject(int object) {
		this.object = object;
	}

	public String getObjectCinfo() {
		return this.objectCinfo;
	}

	public void setObjectCinfo(String objectCinfo) {
		this.objectCinfo = objectCinfo;
	}

	public String getObjectCinfo2() {
		return this.objectCinfo2;
	}

	public void setObjectCinfo2(String objectCinfo2) {
		this.objectCinfo2 = objectCinfo2;
	}

	public Object getObjectDatetime() {
		return this.objectDatetime;
	}

	public void setObjectDatetime(Object objectDatetime) {
		this.objectDatetime = objectDatetime;
	}

	public int getObjectInfo1() {
		return this.objectInfo1;
	}

	public void setObjectInfo1(int objectInfo1) {
		this.objectInfo1 = objectInfo1;
	}

	public int getObjectInfo2() {
		return this.objectInfo2;
	}

	public void setObjectInfo2(int objectInfo2) {
		this.objectInfo2 = objectInfo2;
	}

	public int getObjectInfo3() {
		return this.objectInfo3;
	}

	public void setObjectInfo3(int objectInfo3) {
		this.objectInfo3 = objectInfo3;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getTextValue() {
		return this.textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}

}