package cl.exe.ccs.dto;

import java.io.Serializable;

public class VentaCarteraDTO extends ResponseDTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1249561950422397924L;
	
	private Integer flagVentaCartera;
	private String mensaje;
	private Integer flagDespliegue;
	
	
	public Integer getFlagVentaCartera() {
		return flagVentaCartera;
	}
	public void setFlagVentaCartera(Integer flagVentaCartera) {
		this.flagVentaCartera = flagVentaCartera;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getFlagDespliegue() {
		return flagDespliegue;
	}
	public void setFlagDespliegue(Integer flagDespliegue) {
		this.flagDespliegue = flagDespliegue;
	}
	
	
}
