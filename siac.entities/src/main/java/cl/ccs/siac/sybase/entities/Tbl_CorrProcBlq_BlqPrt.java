package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CorrProcBlq_BlqPrt database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_CorrProcBlq_BlqPrt.findAll", query="SELECT t FROM Tbl_CorrProcBlq_BlqPrt t")
public class Tbl_CorrProcBlq_BlqPrt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CorrProt")
	private BigDecimal fld_CorrProt;

	public Tbl_CorrProcBlq_BlqPrt() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public BigDecimal getFld_CorrProt() {
		return this.fld_CorrProt;
	}

	public void setFld_CorrProt(BigDecimal fld_CorrProt) {
		this.fld_CorrProt = fld_CorrProt;
	}

}