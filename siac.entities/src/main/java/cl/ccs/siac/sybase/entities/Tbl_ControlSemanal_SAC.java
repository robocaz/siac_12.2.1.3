package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_ControlSemanal_SAC database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlSemanal_SAC.findAll", query="SELECT t FROM Tbl_ControlSemanal_SAC t")
public class Tbl_ControlSemanal_SAC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrRut_Id")
	private BigDecimal fld_CorrRut_Id;

	@Column(name="Fld_MontoCH")
	private BigDecimal fld_MontoCH;

	@Column(name="Fld_MontoCM")
	private BigDecimal fld_MontoCM;

	@Column(name="Fld_MontoLT")
	private BigDecimal fld_MontoLT;

	@Column(name="Fld_MontoPG")
	private BigDecimal fld_MontoPG;

	@Column(name="Fld_RutOriginal")
	private String fld_RutOriginal;

	@Column(name="Fld_TotRegCH")
	private BigDecimal fld_TotRegCH;

	@Column(name="Fld_TotRegCM")
	private BigDecimal fld_TotRegCM;

	@Column(name="Fld_TotRegLT")
	private BigDecimal fld_TotRegLT;

	@Column(name="Fld_TotRegPG")
	private BigDecimal fld_TotRegPG;

	public Tbl_ControlSemanal_SAC() {
	}

	public BigDecimal getFld_CorrRut_Id() {
		return this.fld_CorrRut_Id;
	}

	public void setFld_CorrRut_Id(BigDecimal fld_CorrRut_Id) {
		this.fld_CorrRut_Id = fld_CorrRut_Id;
	}

	public BigDecimal getFld_MontoCH() {
		return this.fld_MontoCH;
	}

	public void setFld_MontoCH(BigDecimal fld_MontoCH) {
		this.fld_MontoCH = fld_MontoCH;
	}

	public BigDecimal getFld_MontoCM() {
		return this.fld_MontoCM;
	}

	public void setFld_MontoCM(BigDecimal fld_MontoCM) {
		this.fld_MontoCM = fld_MontoCM;
	}

	public BigDecimal getFld_MontoLT() {
		return this.fld_MontoLT;
	}

	public void setFld_MontoLT(BigDecimal fld_MontoLT) {
		this.fld_MontoLT = fld_MontoLT;
	}

	public BigDecimal getFld_MontoPG() {
		return this.fld_MontoPG;
	}

	public void setFld_MontoPG(BigDecimal fld_MontoPG) {
		this.fld_MontoPG = fld_MontoPG;
	}

	public String getFld_RutOriginal() {
		return this.fld_RutOriginal;
	}

	public void setFld_RutOriginal(String fld_RutOriginal) {
		this.fld_RutOriginal = fld_RutOriginal;
	}

	public BigDecimal getFld_TotRegCH() {
		return this.fld_TotRegCH;
	}

	public void setFld_TotRegCH(BigDecimal fld_TotRegCH) {
		this.fld_TotRegCH = fld_TotRegCH;
	}

	public BigDecimal getFld_TotRegCM() {
		return this.fld_TotRegCM;
	}

	public void setFld_TotRegCM(BigDecimal fld_TotRegCM) {
		this.fld_TotRegCM = fld_TotRegCM;
	}

	public BigDecimal getFld_TotRegLT() {
		return this.fld_TotRegLT;
	}

	public void setFld_TotRegLT(BigDecimal fld_TotRegLT) {
		this.fld_TotRegLT = fld_TotRegLT;
	}

	public BigDecimal getFld_TotRegPG() {
		return this.fld_TotRegPG;
	}

	public void setFld_TotRegPG(BigDecimal fld_TotRegPG) {
		this.fld_TotRegPG = fld_TotRegPG;
	}

}