package cl.ccs.siac.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.exe.ccs.dto.ConsultaBICDTO;
import cl.exe.ccs.dto.ConsultaCerAclDTO;
import cl.exe.ccs.dto.ConsultaMOLDTO;
import cl.exe.ccs.dto.MorosidadVigenteDTO;
import cl.exe.ccs.dto.PersonaDTO;
import cl.exe.ccs.dto.ProtestoVigenteDTO;
import cl.exe.ccs.dto.TeAvisaDTO;
import cl.exe.ccs.dto.certificados.CertificadoAclDTO;
import cl.exe.ccs.dto.certificados.CertificadoDTO;
import cl.exe.ccs.dto.certificados.CertificadoICMDTO;
import cl.exe.ccs.dto.certificados.CertificadoILPDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoICMDTO;
import cl.exe.ccs.dto.certificados.ConsCertificadoILPDTO;

/**
 * @author rbocaz
 *
 */
@Named("certificadoDao")
@Stateless
public class CertificadoDaoImpl implements CertificadoDao {

	private static final Logger LOG = LogManager.getLogger(CertificadoDaoImpl.class);

	@PersistenceContext(unitName = "sybase")
	private EntityManager em;

	@PersistenceContext(unitName = "sybase-link")
	private EntityManager emlink;

	@PersistenceContext(unitName = "orcl-link")
	private EntityManager emora;

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.CertificadoDao#generaCOACBIC(java.lang.String,
	 * cl.exe.ccs.dto.CertificadoDTO)
	 */
	@SuppressWarnings("unchecked")
	public ConsultaBICDTO generaCOACBIC(String user, CertificadoDTO certificado) {

		ConsultaBICDTO consultaBic = new ConsultaBICDTO();
		String resultado = "";
		try {
			
			
			LOG.info("Ejecutando SQ_ConsInfBicPB_ADF");
			
			StoredProcedureQuery storedProcedure = emlink.createStoredProcedureQuery("SQ_ConsInfBicPB_ADF");
			storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(4, BigDecimal.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(8, Boolean.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(9, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(10, Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(11, Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(12, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(13, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(14, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(15, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(16, Object.class, ParameterMode.OUT);

			storedProcedure.setParameter(1, user);
			storedProcedure.setParameter(2, certificado.getRutAfectado());
			if(certificado.getCodServicio().equals("ILG")){
				storedProcedure.setParameter(3, 15);
			}
			else{
				storedProcedure.setParameter(3, 2);
			}
			storedProcedure.setParameter(4, certificado.getCorrMovimiento());

			Boolean execute = storedProcedure.execute();

			List<Object[]> listaCer = storedProcedure.getResultList();

			if (execute) {
				consultaBic.setApPat(storedProcedure.getOutputParameterValue(5).toString());
				consultaBic.setApMat(storedProcedure.getOutputParameterValue(6).toString());
				consultaBic.setNombres(storedProcedure.getOutputParameterValue(7).toString());
				consultaBic.setNroAcls(Integer.parseInt(storedProcedure.getOutputParameterValue(9).toString()));
				consultaBic.setFecPubAcl(storedProcedure.getOutputParameterValue(10).toString());
				consultaBic.setFecPubAcl2(storedProcedure.getOutputParameterValue(11).toString());
				consultaBic.setCodAutenticBic(storedProcedure.getOutputParameterValue(12).toString());
				if (storedProcedure.getOutputParameterValue(15) != null) {
					resultado = storedProcedure.getOutputParameterValue(15).toString();
				}
			} else {
				LOG.error("Error en el SP SQ_ConsInfBicPB_ADF ");
			}

			List<ProtestoVigenteDTO> lista = new ArrayList<ProtestoVigenteDTO>();

			for (Object item : listaCer) {
				System.out.println(item);
				Object[] datos = (Object[]) item;
				ProtestoVigenteDTO grilla = new ProtestoVigenteDTO();
				grilla.setNroBoletin(Integer.parseInt(datos[1].toString()));
				grilla.setPagBoletin(Integer.parseInt(datos[2].toString()));
				// //protVigentes.setFecVcto(rs.getString(5));
				grilla.setFecProt(datos[4].toString());
				grilla.setTipoDocumento(datos[5].toString());
				grilla.setTipoDocImpago(datos[6].toString());
				grilla.setNroOper4Dig(Integer.parseInt(datos[7].toString()));
				grilla.setCodEmisor(datos[8].toString());
				grilla.setGlosaCorta(datos[9].toString());
				grilla.setMontoProt(Float.parseFloat(datos[10].toString()));
				grilla.setGlosaEmisor(datos[11].toString());
				grilla.setNombreLibrador(datos[12].toString());
				grilla.setGlosaLocPub(datos[13].toString());
				grilla.setFecRecepAcl(datos[15].toString());
				grilla.setMarcaAcl(datos[14].toString());
				lista.add(grilla);
			}

			consultaBic.setProtVigentes(lista);

		} catch (Exception e) {
			LOG.error("Error en el SP SQ_ConsInfBicPB_ADF " + e.getMessage());
			e.printStackTrace();
		}

		return consultaBic;

	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.CertificadoDao#generaCOACMOL(java.lang.String,
	 * cl.exe.ccs.dto.CertificadoDTO, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public ConsultaMOLDTO generaCOACMOL(String user, CertificadoDTO certificado, String codAutenticacionBIC) {

		ConsultaMOLDTO consultaMOLDTO = new ConsultaMOLDTO();
		try {
			
			LOG.info("Ejecutando MOL.PKG_Certificados.SvcConsInfMol2");
			
			StoredProcedureQuery storedProcedure = emora.createStoredProcedureQuery("MOL.PKG_Certificados.SvcConsInfMol2");
			storedProcedure.registerStoredProcedureParameter("pCodUsuarioSiac", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pRutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pCodAutenticBic", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pGrabarDatos", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pNroPagina", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pFilasXPag", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("pCodAutenticMol", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pReturn", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pMensaje", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pRegFilas", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("pRegistro", void.class, ParameterMode.REF_CURSOR);

			storedProcedure.setParameter("pCodUsuarioSiac", user);
			storedProcedure.setParameter("pRutAfectado", certificado.getRutAfectado());
			storedProcedure.setParameter("pCodAutenticBic", codAutenticacionBIC);
			storedProcedure.setParameter("pGrabarDatos", 0);
			storedProcedure.setParameter("pNroPagina", 1);
			storedProcedure.setParameter("pFilasXPag", 5000);

			storedProcedure.execute();

			Integer retorno = Integer.parseInt(storedProcedure.getOutputParameterValue("pReturn").toString());
			String mensaje = storedProcedure.getOutputParameterValue("pMensaje").toString();
			consultaMOLDTO.setCodAutenticMol(storedProcedure.getOutputParameterValue("pCodAutenticMol").toString());
			consultaMOLDTO.setCantRegistros(Integer.parseInt(storedProcedure.getOutputParameterValue("pRegFilas").toString()));

			List<MorosidadVigenteDTO> listaMorosidad = new ArrayList<MorosidadVigenteDTO>();
			List<Object[]> listaMor = storedProcedure.getResultList();
			if (listaMor != null) {

				for (Object item : listaMor) {
					Object[] datos = (Object[]) item;
					MorosidadVigenteDTO morVigentes = new MorosidadVigenteDTO();
					morVigentes.setTipoCredito(datos[1].toString());
					morVigentes.setMoneda(datos[2].toString());
					morVigentes.setMontoDeuda(Float.parseFloat(datos[3].toString()));
					morVigentes.setFechaVcto(datos[4].toString());
					morVigentes.setNombreEmisor(datos[5].toString());
					listaMorosidad.add(morVigentes);
				}

			}
			consultaMOLDTO.setListMorosidadVig(listaMorosidad);

		} catch (Exception e) {
			LOG.error("Error en el SP MOL.PKG_Certificados.SvcConsInfMol2 " + e.getMessage());
			e.printStackTrace();
		}

		return consultaMOLDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.CertificadoDao#generaCAA(cl.exe.ccs.dto.
	 * ConsultaCerAclDTO)
	 */
	@SuppressWarnings("unchecked")
	public ConsultaCerAclDTO generaCAA(ConsultaCerAclDTO consultaCerAclDTO) {

		try {
			
			LOG.info("Ejecutando SI_DetalleCerCAA_SW2");
			
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SI_DetalleCerCAA_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransac", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrNombre", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrMovimiento", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodServicio", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_FechaAcl", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodAutenticBic", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			LOG.info("Fld_CorrCaja " + consultaCerAclDTO.getCorrCaja());
			LOG.info("Fld_CorrTransac " + consultaCerAclDTO.getCorrTransac());
			LOG.info("Fld_RutAfectado " + consultaCerAclDTO.getRutAfectado());
			LOG.info("Fld_CorrNombre " + consultaCerAclDTO.getCorrNombre());
			LOG.info("Fld_CodUsuario " + consultaCerAclDTO.getCodUsuario());
			LOG.info("Fld_CorrMovimiento " + consultaCerAclDTO.getCorrMovimiento());
			LOG.info("Fld_CodServicio " + consultaCerAclDTO.getCodServicio());
			
			
			storedProcedure.setParameter("Fld_CorrCaja", consultaCerAclDTO.getCorrCaja());
			storedProcedure.setParameter("Fld_CorrTransac", consultaCerAclDTO.getCorrTransac());
			storedProcedure.setParameter("Fld_RutAfectado", consultaCerAclDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrNombre", consultaCerAclDTO.getCorrNombre());
			storedProcedure.setParameter("Fld_CodUsuario", consultaCerAclDTO.getCodUsuario());
			storedProcedure.setParameter("Fld_CorrMovimiento", consultaCerAclDTO.getCorrMovimiento());
			storedProcedure.setParameter("Fld_CodServicio", consultaCerAclDTO.getCodServicio());

			storedProcedure.execute();

			consultaCerAclDTO.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			consultaCerAclDTO.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));

			if (consultaCerAclDTO.getIdControl() == 0) {

				consultaCerAclDTO.setFechaAcl(storedProcedure.getOutputParameterValue("Fld_FechaAcl").toString());
				consultaCerAclDTO.setCodAutenticBic(storedProcedure.getOutputParameterValue("Fld_CodAutenticBic").toString());

				List<CertificadoAclDTO> lista = new ArrayList<CertificadoAclDTO>();
				List<Object[]> listaSP = storedProcedure.getResultList();
				LOG.info("Vamos a comenzar con CAA");
				if (listaSP != null) {
					for (Object item : listaSP) {
						Object[] datos = (Object[]) item;
						CertificadoAclDTO certificadoACL = new CertificadoAclDTO();

						certificadoACL.setCorrAutenticacion(Long.parseLong(datos[0].toString()));
						certificadoACL.setNroBoletinProt(Integer.parseInt(datos[1].toString()));
						certificadoACL.setPagBoletinProt(Integer.parseInt(datos[2].toString()));
						certificadoACL.setFecPublicacion(datos[3].toString());
						certificadoACL.setFecProt(datos[4].toString());
						certificadoACL.setTipoDocumento(datos[5].toString().trim());
						certificadoACL.setTipoDocumentoImpago(datos[6].toString().trim());
						certificadoACL.setNroOper4Dig(Integer.parseInt(datos[7].toString()));
						certificadoACL.setCodEmisor(datos[8].toString().trim());
						certificadoACL.setGlosaCorta(datos[9].toString().trim());
						certificadoACL.setMontoProt(BigDecimal.valueOf(Double.parseDouble(datos[10].toString())));
						certificadoACL.setGlosaEmisor(datos[11].toString().trim());
						certificadoACL.setNombreLibrador(datos[12].toString().trim());
						certificadoACL.setCiudad(datos[13].toString().trim());
						certificadoACL.setFecAclaracion(datos[14].toString());
						certificadoACL.setFecPago(datos[15].toString());

						lista.add(certificadoACL);
					}
				}
				consultaCerAclDTO.setDetalle(lista);

			}

		} catch (Exception e) {
			consultaCerAclDTO.setIdControl(1);
			LOG.error("Error en el SP SI_DetalleCerCAA_SW2 " + e.getMessage());
			e.printStackTrace();
		}

		return consultaCerAclDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.ccs.siac.dao.CertificadoDao#generaCAC(cl.exe.ccs.dto.
	 * ConsultaCerAclDTO)
	 */
	public CertificadoAclDTO generaCAC(ConsultaCerAclDTO consultaCerAclDTO) {

		CertificadoAclDTO certificado = new CertificadoAclDTO();

		try {
			
			LOG.info("Ejecutando SQ_CertificadoAcl_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_CertificadoAcl_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrMovimiento", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CodAutenticBic", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletin", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecProt", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoDocumento", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoDocImpago", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroOper4Dig", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaMoneda", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_MontoProt", BigDecimal.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaEmisor", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NombreLibrador", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecAclaracion", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletinAcl", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPubAcl", Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_GlosaTipoDocumento", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			
			LOG.info("Fld_RutAfectado " + consultaCerAclDTO.getRutAfectado());
			LOG.info("Fld_CorrMovimiento " + consultaCerAclDTO.getCorrMovimiento());
			LOG.info("Fld_CodUsuario  " + consultaCerAclDTO.getCodUsuario());

			
			storedProcedure.setParameter("Fld_RutAfectado", consultaCerAclDTO.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrMovimiento", consultaCerAclDTO.getCorrMovimiento());
			storedProcedure.setParameter("Fld_CodUsuario", consultaCerAclDTO.getCodUsuario());

			storedProcedure.execute();

			certificado.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				certificado.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

			if (certificado.getIdControl() == 0) {
				certificado.setRutAfectado(consultaCerAclDTO.getRutAfectado());
				certificado.setCodAutenticBic(storedProcedure.getOutputParameterValue("Fld_CodAutenticBic").toString());
				certificado.setNombre(storedProcedure.getOutputParameterValue("Fld_Nombre_Nombres").toString());
				certificado.setApellidoPaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApPat").toString());
				certificado.setApellidoMaterno(storedProcedure.getOutputParameterValue("Fld_Nombre_ApMat").toString());
				certificado.setNroBoletinProt(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletin").toString()));
				certificado.setFecProt(storedProcedure.getOutputParameterValue("Fld_FecProt").toString());
				certificado.setTipoDocumento(storedProcedure.getOutputParameterValue("Fld_TipoDocumento").toString().trim());
				certificado.setTipoDocumentoImpago(storedProcedure.getOutputParameterValue("Fld_GlosaTipoDocImpago").toString());
				certificado.setNroOper4Dig(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroOper4Dig").toString()));
				certificado.setMoneda(storedProcedure.getOutputParameterValue("Fld_GlosaMoneda").toString());
				certificado.setMontoProt(BigDecimal.valueOf(Double.parseDouble(storedProcedure.getOutputParameterValue("Fld_MontoProt").toString())));
				certificado.setGlosaEmisor(storedProcedure.getOutputParameterValue("Fld_GlosaEmisor").toString());
				certificado.setNombreLibrador(storedProcedure.getOutputParameterValue("Fld_NombreLibrador").toString());
				certificado.setFecAclaracion(storedProcedure.getOutputParameterValue("Fld_FecAclaracion").toString());
				certificado.setPagBoletinProt(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletinAcl").toString()));
				certificado.setFecPublicacion(storedProcedure.getOutputParameterValue("Fld_FecPubAcl").toString());
				certificado.setGlosaTipoDocumento(storedProcedure.getOutputParameterValue("Fld_GlosaTipoDocumento").toString());
			}

		} catch (Exception e) {
			certificado.setIdControl(1);
			LOG.error("Error en el SP SQ_CertificadoAcl_SW2 " + e.getMessage());
			e.printStackTrace();
		}

		return certificado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ConsCertificadoILPDTO generaILP(ConsCertificadoILPDTO consCertificado) {
		ConsCertificadoILPDTO consCertificadoReturn = consCertificado;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		try {
			
			LOG.info("Ejecutando SQ_ConLisPuBicPB_ADF");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_ConLisPuBicPB_ADF");
			storedProcedure.registerStoredProcedureParameter("Fld_CodUsuario", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_RutAfectado", String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrMovimiento", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApPat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_ApMat", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Nombre_Nombres", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_TipoNombre", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_NroBoletinLP", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_FecPublicacionLP", Timestamp.class,ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_CodAutenticBic", String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Resultado", Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);

			
			LOG.info("Fld_CodUsuario " + consCertificadoReturn.getCodUsuario());
			LOG.info("Fld_RutAfectado " + consCertificadoReturn.getRutAfectado());
			LOG.info("Fld_CorrMovimiento " + consCertificadoReturn.getCorrMovimiento());
			
			
			storedProcedure.setParameter("Fld_CodUsuario", consCertificadoReturn.getCodUsuario());
			storedProcedure.setParameter("Fld_RutAfectado", consCertificadoReturn.getRutAfectado());
			storedProcedure.setParameter("Fld_CorrMovimiento", consCertificadoReturn.getCorrMovimiento());

			storedProcedure.execute();

			consCertificadoReturn.setResultado(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Resultado").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				consCertificadoReturn.setMensaje(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}

			if (consCertificadoReturn.getResultado() == 0) {
				consCertificadoReturn.setRutAfectado(consCertificadoReturn.getRutAfectado());
				consCertificadoReturn.setCodAutenticBic(storedProcedure.getOutputParameterValue("Fld_CodAutenticBic").toString());
				consCertificadoReturn.setNombres(storedProcedure.getOutputParameterValue("Fld_Nombre_Nombres").toString());
				consCertificadoReturn.setAppPat(storedProcedure.getOutputParameterValue("Fld_Nombre_ApPat").toString());
				consCertificadoReturn.setAppMat(storedProcedure.getOutputParameterValue("Fld_Nombre_ApMat").toString());
				consCertificadoReturn.setTipoNombre(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_TipoNombre").toString()));
				consCertificadoReturn.setNumeroBoletin(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_NroBoletinLP").toString()));
				consCertificadoReturn.setFechaPubLP(sdf.format(storedProcedure.getOutputParameterValue("Fld_FecPublicacionLP")));

				List<Object[]> result = storedProcedure.getResultList();
				List<CertificadoILPDTO> certILPs = new ArrayList<CertificadoILPDTO>();

				for (Object cert : result) {
					Object[] datos = (Object[]) cert;
					CertificadoILPDTO certificadoILPDTO = new CertificadoILPDTO();
					certificadoILPDTO.setRut(consCertificado.getRutAfectado());
					certificadoILPDTO.setNumBoletinProt(Integer.parseInt(datos[0].toString()));
					certificadoILPDTO.setPagBoletinProt(Integer.parseInt(datos[1].toString()));
					certificadoILPDTO.setFechaProt(sdf.format(datos[2]));
					certificadoILPDTO.setTipoDocumento(datos[3].toString());
					certificadoILPDTO.setCodEmisor(datos[4].toString());
					certificadoILPDTO.setNumOperDig(Integer.parseInt(datos[5].toString()));
					certificadoILPDTO.setGlosaCorta(datos[6].toString());
					certificadoILPDTO.setMontoProt(datos[7].toString());
					certificadoILPDTO.setCiudad(datos[8].toString());
					certificadoILPDTO.setGlosaEmisor(datos[9].toString());
					certificadoILPDTO.setNombreLiberador(datos[10].toString());
					certificadoILPDTO.setFechaPublicacion(sdf.format(datos[11]));
					certificadoILPDTO.setTipoDocumentoImpago(datos[12].toString());
					certILPs.add(certificadoILPDTO);
				}

				consCertificadoReturn.setCertILPResult(certILPs);

			}

		} catch (Exception e) {
			consCertificadoReturn.setResultado(1);
			LOG.error("Error en el SP SQ_ConLisPuBicPB_ADF " + e.getMessage());
			e.printStackTrace();
		}

		return consCertificadoReturn;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ConsCertificadoICMDTO generaICM(ConsCertificadoICMDTO consCertificado) {

		ConsCertificadoICMDTO consCertificadoICMReturn = consCertificado;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {
			
			
			LOG.info("Ejecutando SQ_ConsInfBicPB_ADF");
			
			StoredProcedureQuery storedProcedure = emlink.createStoredProcedureQuery("SQ_ConsInfBicPB_ADF");
			storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(4, Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter(5, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(6, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(8, Boolean.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(9, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(10, Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(11, Timestamp.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(12, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(13, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(14, Integer.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(15, String.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter(16, Object.class, ParameterMode.OUT);

			storedProcedure.setParameter(1, consCertificadoICMReturn.getCodUsuario());
			storedProcedure.setParameter(2, consCertificadoICMReturn.getRutAfectado());
			storedProcedure.setParameter(3, consCertificadoICMReturn.getTipoConsulta());
			storedProcedure.setParameter(4, consCertificadoICMReturn.getCorrMovimiento());

			Boolean execute = storedProcedure.execute();
			List<Object[]> result = storedProcedure.getResultList();

			consCertificadoICMReturn.setResultado(Integer.parseInt(storedProcedure.getOutputParameterValue(14).toString()));
			if (storedProcedure.getOutputParameterValue(15) != null) {
				consCertificadoICMReturn.setMensaje(storedProcedure.getOutputParameterValue(15).toString());
			}

			if (consCertificadoICMReturn.getResultado() == 0) {
				consCertificadoICMReturn.setRutAfectado(consCertificadoICMReturn.getRutAfectado());
				consCertificadoICMReturn.setCodAutenticBic(storedProcedure.getOutputParameterValue(12).toString());
				consCertificadoICMReturn.setNombres(storedProcedure.getOutputParameterValue(7).toString());
				consCertificadoICMReturn.setAppPat(storedProcedure.getOutputParameterValue(5).toString());
				consCertificadoICMReturn.setAppMat(storedProcedure.getOutputParameterValue(6).toString());
				consCertificadoICMReturn.setTipoNombre(Boolean.valueOf(storedProcedure.getOutputParameterValue(8).toString()));
				consCertificadoICMReturn.setNumeroAcls(Integer.parseInt(storedProcedure.getOutputParameterValue(9).toString()));
				consCertificadoICMReturn.setFechaPubAcls(sdf.format(storedProcedure.getOutputParameterValue(10)));
				consCertificadoICMReturn.setFechaPubAcls2(sdf.format(storedProcedure.getOutputParameterValue(11)));
				consCertificadoICMReturn.setFilas(Integer.parseInt(storedProcedure.getOutputParameterValue(13).toString()));

				List<CertificadoICMDTO> certICMs = new ArrayList<CertificadoICMDTO>();

				for (Object cert : result) {
					Object[] datos = (Object[]) cert;
					CertificadoICMDTO certificadoICMDTO = new CertificadoICMDTO();
					certificadoICMDTO.setRut(consCertificado.getRutAfectado());
					certificadoICMDTO.setCorrAutenticacion(Long.parseLong(datos[0].toString()));
					certificadoICMDTO.setNumBoletinProt(Integer.parseInt(datos[1].toString()));
					certificadoICMDTO.setPagBoletinProt(Integer.parseInt(datos[2].toString()));
					certificadoICMDTO.setFechaPublicacion(sdf.format(datos[3]));
					certificadoICMDTO.setFechaProt(sdf.format(datos[4]));
					certificadoICMDTO.setTipoDocumento(datos[5].toString());
					certificadoICMDTO.setTipoDocumentoImpago(datos[6].toString());
					certificadoICMDTO.setNumOperDig(Integer.parseInt(datos[7].toString()));
					certificadoICMDTO.setCodEmisor(datos[8].toString());
					certificadoICMDTO.setGlosaCorta(datos[9].toString());
					certificadoICMDTO.setMontoProt(datos[10].toString());
					certificadoICMDTO.setGlosaEmisor(datos[11].toString());
					certificadoICMDTO.setNombreLiberador(datos[12].toString());
					certificadoICMDTO.setCiudad(datos[13].toString());
					certificadoICMDTO.setMarcaAclaracion(Integer.parseInt((datos[14]).toString()));
					certificadoICMDTO.setFechaAclaracion(sdf.format(datos[15]));
					certICMs.add(certificadoICMDTO);
				}

				consCertificadoICMReturn.setCertICMResult(certICMs);

			}

		} catch (Exception e) {
			consCertificadoICMReturn.setResultado(1);
			LOG.error("Error en el SP SQ_ConsInfBicPB_ADF " + e.getMessage());
			e.printStackTrace();
		}

		return consCertificadoICMReturn;

	}
	
	
	public TeAvisaDTO getDatosContratoTeAvisa(TeAvisaDTO contrato){
		
		try {
			
			LOG.info("Ejecutando SQ_DatosContratoTA_SW2");
			
			StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("SQ_DatosContratoTA_SW2");
			storedProcedure.registerStoredProcedureParameter("Fld_CorrCaja", Integer.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrMovimiento", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_CorrTransaccion", Long.class, ParameterMode.IN);
			storedProcedure.registerStoredProcedureParameter("Fld_Retorno", Long.class, ParameterMode.OUT);
			storedProcedure.registerStoredProcedureParameter("Fld_Mensaje", String.class, ParameterMode.OUT);
			
			storedProcedure.setParameter("Fld_CorrCaja", contrato.getCorrCaja());
			storedProcedure.setParameter("Fld_CorrMovimiento", contrato.getCorrMovimiento());
			storedProcedure.setParameter("Fld_CorrTransaccion", contrato.getCorrTransaccion());

			boolean execute = storedProcedure.execute();
			List<Object[]> result = storedProcedure.getResultList();

			for (Object cert : result) {
				Object[] datos = (Object[]) cert;
				contrato.setCorrContrato(Long.parseLong(datos[0].toString()));
				PersonaDTO persTeAvisa = new PersonaDTO();
				persTeAvisa.setCorrelativo(Long.parseLong(datos[2].toString()));
				contrato.setPersTeAvisa(persTeAvisa);
				PersonaDTO contratante = new PersonaDTO();
				contratante.setCorrelativo(Long.parseLong(datos[3].toString()));
				contrato.setContratante(contratante);
				contrato.setDireccion(datos[4].toString());
				contrato.setEmail(datos[5].toString());
				contratante.setRut(datos[6].toString());
				persTeAvisa.setRut(datos[7].toString());
				contrato.setTelcel(datos[8].toString());
				contrato.setTelfijo(datos[9].toString());
				contrato.setServcorreo(Boolean.parseBoolean(datos[10].toString()));
				contrato.setServsms(Boolean.parseBoolean(datos[11].toString()));
				contratante.setApellidoPaterno(datos[12].toString());
				contratante.setApellidoMaterno(datos[13].toString());
				contratante.setNombres(datos[14].toString());
			}
			
			

			contrato.setIdControl(Integer.parseInt(storedProcedure.getOutputParameterValue("Fld_Retorno").toString()));
			if (storedProcedure.getOutputParameterValue("Fld_Mensaje") != null) {
				contrato.setMsgControl(storedProcedure.getOutputParameterValue("Fld_Mensaje").toString());
			}
			
			

		
		} catch (Exception e) {
			LOG.error("Error en el SP SQ_DatosContratoTA_SW2 " + e.getMessage());
			contrato.setIdControl(1);
			e.printStackTrace();
		}
		return contrato;
		
	}

}
