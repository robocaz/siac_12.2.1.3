package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the HSS_COMPONENT database table.
 * 
 */
@Entity
@Table(name="HSS_COMPONENT")
@NamedQuery(name="HssComponent.findAll", query="SELECT h FROM HssComponent h")
public class HssComponent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="COMPONENT_ID")
	private String componentId;

	@Column(name="COMPONENT_NAME")
	private String componentName;

	@Column(name="COMPONENT_TYPE_ID")
	private BigDecimal componentTypeId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATE_DT")
	private Date createDt;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATE_DT")
	private Date lastUpdateDt;

	public HssComponent() {
	}

	public String getComponentId() {
		return this.componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return this.componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public BigDecimal getComponentTypeId() {
		return this.componentTypeId;
	}

	public void setComponentTypeId(BigDecimal componentTypeId) {
		this.componentTypeId = componentTypeId;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Date getLastUpdateDt() {
		return this.lastUpdateDt;
	}

	public void setLastUpdateDt(Date lastUpdateDt) {
		this.lastUpdateDt = lastUpdateDt;
	}

}