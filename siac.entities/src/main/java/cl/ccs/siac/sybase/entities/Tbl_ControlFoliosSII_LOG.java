package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Tbl_ControlFoliosSII_LOG database table.
 * 
 */
@Entity
@NamedQuery(name="Tbl_ControlFoliosSII_LOG.findAll", query="SELECT t FROM Tbl_ControlFoliosSII_LOG t")
public class Tbl_ControlFoliosSII_LOG implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CodUsuario")
	private String fld_CodUsuario;

	@Column(name="Fld_FecEmision")
	private Timestamp fld_FecEmision;

	@Column(name="Fld_FolioActual")
	private int fld_FolioActual;

	@Column(name="Fld_TipoDTE")
	private byte fld_TipoDTE;

	public Tbl_ControlFoliosSII_LOG() {
	}

	public String getFld_CodUsuario() {
		return this.fld_CodUsuario;
	}

	public void setFld_CodUsuario(String fld_CodUsuario) {
		this.fld_CodUsuario = fld_CodUsuario;
	}

	public Timestamp getFld_FecEmision() {
		return this.fld_FecEmision;
	}

	public void setFld_FecEmision(Timestamp fld_FecEmision) {
		this.fld_FecEmision = fld_FecEmision;
	}

	public int getFld_FolioActual() {
		return this.fld_FolioActual;
	}

	public void setFld_FolioActual(int fld_FolioActual) {
		this.fld_FolioActual = fld_FolioActual;
	}

	public byte getFld_TipoDTE() {
		return this.fld_TipoDTE;
	}

	public void setFld_TipoDTE(byte fld_TipoDTE) {
		this.fld_TipoDTE = fld_TipoDTE;
	}

}