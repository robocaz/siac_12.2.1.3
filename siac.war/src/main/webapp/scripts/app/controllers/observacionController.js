angular.module('ccs').controller('ObservacionController', function($rootScope, $scope,  $location, $cookies, $timeout, ObservacionResource, $filter ) {

    $scope.observacionmodel = {
            model : undefined
    };
    $scope.listarobservaciones = [];
    $scope.listarobservacionestramitante = [];
    $scope.observaciontramitante = {
        'codigoObservacion' : 0,
        'glosaObservacion' : ''
    };

    $scope.insertarObservacion = function(){
        $scope.listarobservacionestramitante.push({
            rutTramitante : $scope.tramitante.rut,
            codigoObservacion : $scope.observacionmodel.model.codigo,
            glosaObservacion : $scope.observacionmodel.model.descripcion,
            usuario : $rootScope.usuario.username,
            codigoCentroCosto : $rootScope.usuario.centroCostoActivo.codigo,
            glosaCentroCosto : $rootScope.usuario.centroCostoActivo.descripcion,
            fechaCreacion : $filter('date')(new Date(), 'd/M/yy h:mm a'),
            esNuevo : true
        });
    }

    $('#observacion').on('show.bs.modal', function (e) {
        $timeout(function(){
            $scope.observacionmodel = {
                    model : undefined
            };
            //$scope.getObservacion();
            $scope.listarobservacionescombo();
            $scope.listarObservacionesTramitante();
        });
    });

    $scope.listarobservacionescombo = function(){
        var successCallback = function(data){
            $scope.listarobservaciones = data;
        };
        ObservacionResource.listarobservaciones(successCallback, $scope.errorCallback);
    };

    $scope.listarObservacionesTramitante= function(){
        var successCallback = function(data){
            $scope.listarobservacionestramitante = data;
        };
        ObservacionResource.listarobservacionestramitante(successCallback, $scope.errorCallback);
    }

    $scope.setFocus = function(){
    	$("div[focus-on=listaObs] .ui-select-focusser").focus();
    }
    $scope.guardarObservaciones = function(){
        var contarSolicitud = 0;
        var contarExitosa = 0;
		$scope.setFocus();
        if($scope.observacionmodel.model !== undefined){
        	swal('Alerta','Debe seleccionar y agregar una observación','error');
			
        	//$("#listaObs").focus();
        	return false;
        }
        for(x=0; x< $scope.listarobservacionestramitante.length; x++){
        	var obs = $scope.listarobservacionestramitante[x];
            if(obs.esNuevo){
                contarSolicitud = contarSolicitud +1;
                var successCallback = function(data){
                    contarExitosa = contarExitosa +1;
                    if(contarSolicitud==contarExitosa){
                        angular.element('#observacion').modal("hide");
                    }
                };
                var errorCallback = function(data){
                    console.log("ERROR");
                };
                ObservacionResource.crear(new Number($scope.listarobservacionestramitante[x].codigoObservacion), successCallback, $scope.errorCallback);
            }
        }
	    angular.element('#observacion').modal("hide"); 
    }

    $scope.agregarObservacion = function(){
        existe = false;
        if($scope.observacionmodel.model!=undefined){
            for(x=0; x< $scope.listarobservacionestramitante.length; x++){
                if($scope.listarobservacionestramitante[x].codigoObservacion == $scope.observacionmodel.model.codigo){
                    existe=true;
                }
            }
            if(existe){
                swal('Alerta','Ya se seleccionó esa observación','error');
            }else{
                $scope.insertarObservacion();
                $scope.observacionmodel = {
			            model : undefined
			    };

            }
        }else{
            swal('Alerta','Debe ingresar un Tipo Observación','error');
        }

    }

});
