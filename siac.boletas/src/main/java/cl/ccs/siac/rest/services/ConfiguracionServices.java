package cl.ccs.siac.rest.services;

import java.io.FileWriter;
import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import cl.ccs.siac.printer.CargaConfig;
import cl.ccs.siac.printer.Impresion;
import cl.exe.ccs.dto.InicioConfigDTO;

@Path("/config")
@RequestScoped
public class ConfiguracionServices {

	@Inject
	private CargaConfig cargaConfigImpl;
	
	@Inject
	private Impresion utilImpresion;

	private static final Logger LOG = LogManager.getLogger(ConfiguracionServices.class);

	@GET
	@Path("/inicio")
	@Produces(MediaType.APPLICATION_JSON)
	public Response configuracion() {

		InicioConfigDTO configuracion = new InicioConfigDTO();
   
		configuracion = cargaConfigImpl.cargaConfiguracion();
		if (configuracion.getIdControl() != null) {
			return Response.ok(configuracion).build();
		} else {
			return Response.status(404).entity(null).build();
		}

	}

	@POST
	@Path("/confinicial")
	@Produces(MediaType.APPLICATION_JSON)
	public Response configuracionInicial(@Context HttpServletRequest request, InicioConfigDTO configuracion) {

		Gson gson = new Gson();

		configuracion.setPuertoImpresora("COM"+configuracion.getPuertoImpresora());
		String json = gson.toJson(configuracion);

		FileWriter writer;
		try {
			writer = new FileWriter("./configuracion.json");
			writer.write(json);
			writer.close();
			return Response.ok(configuracion).build();

		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			return Response.status(404).entity(null).build();
		}
	}

	@GET
	@Path("/emiteboleta")
	@Produces(MediaType.APPLICATION_JSON)
	public Response emisionBoleta(@Context HttpServletRequest request, @QueryParam("emiteboleta") boolean emiteBoleta) {

		Gson gson = new Gson();
		InicioConfigDTO configuracion = new InicioConfigDTO();

		configuracion = cargaConfigImpl.cargaConfiguracion();
		configuracion.setEmiteBoleta(emiteBoleta);

		String json = gson.toJson(configuracion);

		FileWriter writer;
		try {
			writer = new FileWriter("./configuracion.json");
			writer.write(json);
			writer.close();
			return Response.ok(configuracion).build();

		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			return Response.status(404).entity(null).build();
		}
	}

	@GET
	@Path("/emitefactura")
	@Produces(MediaType.APPLICATION_JSON)
	public Response emisionFactura(@Context HttpServletRequest request, @QueryParam("emitefactura") boolean emiteFactura) {

		Gson gson = new Gson();
		InicioConfigDTO configuracion = new InicioConfigDTO();

		configuracion = cargaConfigImpl.cargaConfiguracion();
		configuracion.setEmiteFactura(emiteFactura);

		String json = gson.toJson(configuracion);

		FileWriter writer;
		try {
			writer = new FileWriter("./configuracion.json");
			writer.write(json);
			writer.close();
			return Response.ok(configuracion).build();

		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			return Response.status(404).entity(null).build();
		}
	}

	@GET
	@Path("/emitebiometria")
	@Produces(MediaType.APPLICATION_JSON)
	public Response emisionBiometria(@Context HttpServletRequest request,
			@QueryParam("emitebiometria") boolean emiteBiometria) {

		Gson gson = new Gson();
		InicioConfigDTO configuracion = new InicioConfigDTO();

		configuracion = cargaConfigImpl.cargaConfiguracion();
		configuracion.setBiometria(emiteBiometria);

		String json = gson.toJson(configuracion);

		FileWriter writer;
		try {
			writer = new FileWriter("./configuracion.json");
			writer.write(json);
			writer.close();
			return Response.ok(configuracion).build();

		} catch (IOException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			return Response.status(404).entity(null).build();
		}
	}
	
	
	@GET
	@Path("/prueba")
	@Produces(MediaType.APPLICATION_JSON)
	public Response pruebaBoletera(@Context HttpServletRequest request) {
		
		try{
			utilImpresion.imprimeBoletaPrueba();
			return Response.ok("OK").build();
		}
		catch(Exception e){
			LOG.error(e.getMessage());
			return Response.status(404).entity(null).build();
		}


	}
}
