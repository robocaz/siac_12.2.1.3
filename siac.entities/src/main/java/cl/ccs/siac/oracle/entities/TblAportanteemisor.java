package cl.ccs.siac.oracle.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TBL_APORTANTEEMISOR database table.
 * 
 */
@Entity
@Table(name="TBL_APORTANTEEMISOR")
@NamedQuery(name="TblAportanteemisor.findAll", query="SELECT t FROM TblAportanteemisor t")
public class TblAportanteemisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLD_APORTANTE")
	private String fldAportante;

	@Column(name="FLD_CODEMISOR")
	private String fldCodemisor;

	@Column(name="FLD_TIPOEMISOR")
	private String fldTipoemisor;

	public TblAportanteemisor() {
	}

	public String getFldAportante() {
		return this.fldAportante;
	}

	public void setFldAportante(String fldAportante) {
		this.fldAportante = fldAportante;
	}

	public String getFldCodemisor() {
		return this.fldCodemisor;
	}

	public void setFldCodemisor(String fldCodemisor) {
		this.fldCodemisor = fldCodemisor;
	}

	public String getFldTipoemisor() {
		return this.fldTipoemisor;
	}

	public void setFldTipoemisor(String fldTipoemisor) {
		this.fldTipoemisor = fldTipoemisor;
	}

}