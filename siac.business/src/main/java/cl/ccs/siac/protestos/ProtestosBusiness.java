package cl.ccs.siac.protestos;

import cl.exe.ccs.dto.ProtestoOutDTO;
import cl.exe.ccs.dto.ProtestoTramitanteDTO;
import cl.exe.ccs.dto.ProtestoAclaracionDTO;
import cl.exe.ccs.dto.ProtestoDetalleDTO;
import cl.exe.ccs.dto.ProtestoInDTO;

public interface ProtestosBusiness {

	ProtestoOutDTO buscar(ProtestoInDTO params);
	
	ProtestoDetalleDTO detalle(Integer corrProt);
	
	ProtestoAclaracionDTO aclaracion(Integer corrAcl);
	
	ProtestoTramitanteDTO tramitante(String rut);
	
}
