package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the Tb_CeIdCtrBIC database table.
 * 
 */
@Entity
@NamedQuery(name="Tb_CeIdCtrBIC.findAll", query="SELECT t FROM Tb_CeIdCtrBIC t")
public class Tb_CeIdCtrBIC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_Accion")
	private String fld_Accion;

	@Column(name="Fld_CodUsuarioAct")
	private String fld_CodUsuarioAct;

	@Column(name="Fld_Correlativo")
	private BigDecimal fld_Correlativo;

	@Column(name="Fld_FecActualizacion")
	private Timestamp fld_FecActualizacion;

	@Column(name="Fld_FecProceso")
	private Timestamp fld_FecProceso;

	@Column(name="Fld_FlagProceso")
	private int fld_FlagProceso;

	@Column(name="Fld_TipoEnvio")
	private byte fld_TipoEnvio;

	public Tb_CeIdCtrBIC() {
	}

	public String getFld_Accion() {
		return this.fld_Accion;
	}

	public void setFld_Accion(String fld_Accion) {
		this.fld_Accion = fld_Accion;
	}

	public String getFld_CodUsuarioAct() {
		return this.fld_CodUsuarioAct;
	}

	public void setFld_CodUsuarioAct(String fld_CodUsuarioAct) {
		this.fld_CodUsuarioAct = fld_CodUsuarioAct;
	}

	public BigDecimal getFld_Correlativo() {
		return this.fld_Correlativo;
	}

	public void setFld_Correlativo(BigDecimal fld_Correlativo) {
		this.fld_Correlativo = fld_Correlativo;
	}

	public Timestamp getFld_FecActualizacion() {
		return this.fld_FecActualizacion;
	}

	public void setFld_FecActualizacion(Timestamp fld_FecActualizacion) {
		this.fld_FecActualizacion = fld_FecActualizacion;
	}

	public Timestamp getFld_FecProceso() {
		return this.fld_FecProceso;
	}

	public void setFld_FecProceso(Timestamp fld_FecProceso) {
		this.fld_FecProceso = fld_FecProceso;
	}

	public int getFld_FlagProceso() {
		return this.fld_FlagProceso;
	}

	public void setFld_FlagProceso(int fld_FlagProceso) {
		this.fld_FlagProceso = fld_FlagProceso;
	}

	public byte getFld_TipoEnvio() {
		return this.fld_TipoEnvio;
	}

	public void setFld_TipoEnvio(byte fld_TipoEnvio) {
		this.fld_TipoEnvio = fld_TipoEnvio;
	}

}