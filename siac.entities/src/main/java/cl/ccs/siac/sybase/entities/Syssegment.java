package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the syssegments database table.
 * 
 */
@Entity
@Table(name="syssegments")
@NamedQuery(name="Syssegment.findAll", query="SELECT s FROM Syssegment s")
public class Syssegment implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;

	private short segment;

	private short status;

	public Syssegment() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getSegment() {
		return this.segment;
	}

	public void setSegment(short segment) {
		this.segment = segment;
	}

	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

}