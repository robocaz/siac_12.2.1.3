package cl.ccs.siac.sybase.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Tbl_CorrNomPrescritos database table.
 * 
 */
@Entity
@Table(name="Tbl_CorrNomPrescritos")
@NamedQuery(name="Tbl_CorrNomPrescrito.findAll", query="SELECT t FROM Tbl_CorrNomPrescrito t")
public class Tbl_CorrNomPrescrito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Fld_CorrNombre")
	private BigDecimal fld_CorrNombre;

	public Tbl_CorrNomPrescrito() {
	}

	public BigDecimal getFld_CorrNombre() {
		return this.fld_CorrNombre;
	}

	public void setFld_CorrNombre(BigDecimal fld_CorrNombre) {
		this.fld_CorrNombre = fld_CorrNombre;
	}

}